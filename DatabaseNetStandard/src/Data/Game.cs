﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace DatabaseNetStandard.Data
{
    public abstract class GameBase
    {
        public abstract DateTime Date { get; protected set; }
        public abstract string DateFormated { get; }
        public abstract Color ResultColor { get; }
        public abstract Championship Championship { get; set; }
        public abstract Season Season { get; set; }
        public abstract string GetDayOrPhase { get; }
        public abstract string OppName { get; }
        public abstract string OppSmallName { get; }
        public abstract string PlayerSmallName { get; }
        public abstract string Result { get; }

        public abstract string ResultA_B { get; }
        public abstract int PointsCurrentPlayer { get; }
        public abstract int OppId { get; }
        public abstract int SetCurrentPlayer { set; }
        public abstract bool IsDouble { get; }
    }
    public class Game : GameBase
    {
        public Game(int id, int idPlayerA, int idPlayerB, int setsA, int setsB, int idPlayerWinner, DateTime date, Championship championship, int idCalculation, int pointsPlayerA, int pointsPlayerB, Phase phase, TeamGame teamGame, bool insertedManually)
        {
            Id = id;
            IdPlayerA = idPlayerA;
            IdPlayerB = idPlayerB;
            SetsA = setsA;
            SetsB = setsB;
            IdPlayerWinner = idPlayerWinner;
            Date = date;
            Championship = championship;
            IdCalculation = idCalculation;
            PointsPlayerA = pointsPlayerA;
            PointsPlayerB = pointsPlayerB;
            Phase = phase;
            
            if (Phase == null)
                Phase = Database.Instance.phases.SingleOrDefault(x => x.Name == "N/A");

            TeamGame = teamGame;
            InsertedManually = insertedManually;
            teamGame?.singularGames.Add(this);
            teamGame?.AddResult(IdPlayerWinner == IdPlayerA ? true : false);
            teamGame?.SetDayOrPhase(this);
            try
            {
                Season = Database.Instance.seasons.SingleOrDefault(x => x.StartDate <= Date && x.EndDate >= Date && x.IsSingleSeason);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public override bool IsDouble => false;
        public int Id { get; private set; }
        public int IdPlayerA { get; private set; }
        public int IdPlayerB { get; private set; }
        public int SetsA { get; private set; }
        public int SetsB { get; private set; }

        public override string ResultA_B => $"{SetsA}-{SetsB}";

        public Player PlayerA
        {
            get
            {
                return Database.Instance.players.SingleOrDefault(x => x.Id == IdPlayerA);
            }
        }
        public Player PlayerB
        {
            get
            {
                return Database.Instance.players.SingleOrDefault(x => x.Id == IdPlayerB);
            }
        }

        public string PlayerAName
        {
            get
            {
                return Database.Instance.players.SingleOrDefault(x => x.Id == IdPlayerA)?.Name;
            }
        }
        public string PlayerBName
        {
            get
            {
                return Database.Instance.players.SingleOrDefault(x => x.Id == IdPlayerB)?.Name;
            }
        }
        public int SetsCurrentPlayer
        {
            get
            {
                if (currentPlayer == -1)
                    return -1;

                if (currentPlayer == IdPlayerA)
                    return SetsA;
                else
                    return SetsB;
            }
        }

        public int SetsOppPlayer
        {
            get
            {
                if (currentPlayer == -1)
                    return -1;

                if (currentPlayer == IdPlayerA)
                    return SetsB;
                else
                    return SetsA;
            }
        }

        public int IdPlayerWinner { get; private set; }

        public bool PlayerWinnerCurrentClub
        {
            get
            {
                if (currentClub == -1)
                    return false;

                if (IdPlayerWinner == IdPlayerA)
                {
                    if (currentClub == TeamGame.ClubA.Id)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else if (IdPlayerWinner == IdPlayerB)
                {
                    if (currentClub == TeamGame.ClubB.Id)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }

                return false;
            }
        }

        public bool ClubAWinner
        {
            get
            {
                if (currentClub == -1)
                    return false;

                if (IdPlayerWinner == IdPlayerA)
                    return true;
                else
                    return false;
            }
        }
        public bool ClubBWinner
        {
            get
            {
                if (currentClub == -1)
                    return false;

                if (IdPlayerWinner == IdPlayerB)
                    return true;
                else
                    return false;
            }
        }

        public override string Result
        {
            get
            {
                if (currentPlayer == -1)
                    return $"{SetsA} - {SetsB}";

                if (currentPlayer == IdPlayerA)
                    return $"{SetsA} - {SetsB}";
                else
                    return $"{SetsB} - {SetsA}";
            }
        }

        public override int PointsCurrentPlayer
        {
            get
            {
                if (currentPlayer == -1)
                    return -1;

                if (currentPlayer == IdPlayerA)
                    return PointsPlayerA;
                else
                    return PointsPlayerB;
            }
        }

        public override Color ResultColor
        {
            get
            {
                if (currentPlayer == -1)
                    return Color.White;
                if (currentPlayer == IdPlayerWinner)
                    return Color.DarkOliveGreen;
                else
                    return Color.MediumVioletRed;
            }
        }

        public override string OppName
        {
            get
            {
                if (currentPlayer == -1)
                    return "N/A";

                if (currentPlayer == IdPlayerA)
                    return Database.Instance.players.Single(x => x.Id == IdPlayerB).Name;
                else
                    return Database.Instance.players.Single(x => x.Id == IdPlayerA).Name;
            }
        }

        public string PlayerName
        {
            get
            {
                if (currentPlayer == -1)
                    return "N/A";

                if (currentPlayer == IdPlayerA)
                    return Database.Instance.players.Single(x => x.Id == IdPlayerA).Name;
                else
                    return Database.Instance.players.Single(x => x.Id == IdPlayerB).Name;
            }
        }

        public override int OppId
        {
            get
            {
                if (currentPlayer == -1)
                    return -1;

                if (currentPlayer == IdPlayerA)
                    return IdPlayerB;
                else
                    return IdPlayerA;
            }
        }

        public int PlayerId
        {
            get
            {
                if (currentPlayer == -1)
                    return -1;

                if (currentPlayer == IdPlayerA)
                    return Database.Instance.players.Single(x => x.Id == IdPlayerA).Id;
                else
                    return Database.Instance.players.Single(x => x.Id == IdPlayerB).Id;
            }
        }

        private int currentPlayer = -1;
        public override int SetCurrentPlayer
        {
            set
            {
                currentPlayer = value;
            }
        }

        private int currentClub = -1;
        public int SetCurrentClub
        {
            set
            {
                currentClub = value;
                if (TeamGame == null)
                    return;

                if (TeamGame.ClubA.Id == value)
                {
                    currentClub = TeamGame.ClubA.Id;
                }
                else
                {
                    currentClub = TeamGame.ClubB.Id;
                }
            }
        }

        public int CurrentPlayerClubId
        {
            get; private set;
        }

        public int IdClubWinner
        {
            get
            {
                if (TeamGame == null)
                    return -1;

                if (IdPlayerWinner == IdPlayerA)
                    return TeamGame.ClubA.Id;
                else if (IdPlayerWinner == IdPlayerA)
                    return TeamGame.ClubB.Id;

                return -1;
            }
        }

        public override Season Season
        {
            get; set;
        }

        public override DateTime Date { get; protected set; }
        public override string DateFormated { get { return Date.ToString("dd-MM-yyyy"); } }
        public override Championship Championship { get; set; }
        public int IdCalculation { get; private set; }
        public int PointsPlayerA { get; private set; }
        public int PointsPlayerB { get; private set; }
        public Phase Phase { get; private set; }
        public TeamGame TeamGame { get; private set; }
        public bool InsertedManually { get; private set; }
        public override string GetDayOrPhase { get { return TeamGame != null ? TeamGame.DayOrPhase : Phase != null ? Phase.NameShort : "N/A"; } }
        public override string OppSmallName
        {
            get
            {
                if (currentPlayer == -1)
                    return "N/A";

                if (currentPlayer == IdPlayerA)
                    return Database.Instance.players.Single(x => x.Id == IdPlayerB).SmallName;
                else
                    return Database.Instance.players.Single(x => x.Id == IdPlayerA).SmallName;
            }
        }

        public override string PlayerSmallName
        {
            get
            {
                if (currentPlayer == -1)
                    return "N/A";

                if (currentPlayer == IdPlayerA)
                    return Database.Instance.players.Single(x => x.Id == IdPlayerA).SmallName;
                else
                    return Database.Instance.players.Single(x => x.Id == IdPlayerB).SmallName;
            }
        }
    }
}
