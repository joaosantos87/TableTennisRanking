﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DatabaseNetStandard.Data
{
    public class ChampTable
    {
        public int Pos { get; set; }
        public Club Club { get; set; }
        public string ClubTableName { get; set; }
        public int VictoryFourZero { get; set; }
        public int VictoryFourOne { get; set; }
        public int VictoryThreeTwo { get; set; }
        public int LoseThreeTwo { get; set; }
        public int LoseFourOne { get; set; }
        public int LoseFourZero { get; set; }
        public int VictoryThreeOne { get; set; }
        public int VictoryThreeZero { get; set; }
        public int LoseThreeOne { get; set; }
        public int LoseThreeZero { get; set; }
        public int Victories
        {
            get
            {
                return VictoryFourZero + VictoryFourOne + VictoryThreeTwo + VictoryThreeOne + VictoryThreeZero;
            }
        }
        public int Losts
        {
            get
            {
                return LoseThreeTwo + LoseFourOne + LoseFourZero + LoseThreeZero + LoseThreeOne;
            }
        }
        public int Games
        {
            get
            {
                return Victories + Losts;
            }
        }

        public int Points_17_Now
        {
            get
            {
                return VictoryFourZero*4 + VictoryFourOne*4 + VictoryThreeTwo*3 + LoseThreeTwo*1 + LoseFourOne*0 + LoseFourZero *0;
            }
        }

        public int Points_13_14
        {
            get
            {
                return (VictoryThreeZero + VictoryThreeTwo + VictoryThreeOne) * 3 + (LoseThreeOne + LoseThreeTwo + LoseThreeZero)*1;
            }
        }
        public int Points_14_16
        {
            get
            {
                return VictoryFourZero * 4 + VictoryFourOne * 4 + VictoryThreeTwo * 3 + LoseThreeTwo * 2 + LoseFourOne * 1 + LoseFourZero * 1;
            }
        }

        public int Points_All
        {
            get
            {
                if (season == null)
                    return Points_17_Now;
                else if (season.StartDate.Year == 2013)
                    return Points_13_14;
                else if (season.StartDate.Year == 2014)
                    return Points_14_16;
                else
                    return Points_17_Now;
            }
        }
        Season season;
        public Season SetSeason
        {
            set { season = value; }
        }

        public int PG
        {
            get
            {
                return VictoryFourZero * 4 + VictoryFourOne * 4 + VictoryThreeTwo * 3 + LoseThreeTwo * 2 + LoseFourOne + LoseFourZero * 0 + VictoryThreeZero *3 + VictoryThreeOne*3 + LoseThreeZero * 0 + LoseThreeOne * 1;
            }
        }

        public int PP
        {
            get
            {
                return VictoryFourZero * 0 + VictoryFourOne * 1 + VictoryThreeTwo * 2 + LoseThreeTwo * 3 + LoseFourOne * 4 + LoseFourZero *4 + LoseThreeOne * 3 + LoseThreeZero * 3 + VictoryThreeOne * 1 + VictoryThreeZero * 0;
            }
        }
    }
}
