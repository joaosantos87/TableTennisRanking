﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace DatabaseNetStandard.Data
{
    public class DoubleGame : GameBase
    {
        public DoubleGame(int id, int doubleAidPlayerA, int doubleAidPlayerB, int doubleBidPlayerA, int doubleBidPlayerB, int setsA, int setsB, int idPlayerWinnerA, int idPlayerWinnerB, DateTime date, Championship championship, int pointsDoubleAPlayerA, int pointsDoubleAPlayerB, int pointsDoubleBPlayerA, int pointsDoubleBPlayerB, Phase phase, TeamGame teamGame, bool insertedManually)
        {
            Id = id;
            DoubleAIdPlayerA = doubleAidPlayerA;
            DoubleAIdPlayerB = doubleAidPlayerB;
            DoubleBIdPlayerA = doubleBidPlayerA;
            DoubleBIdPlayerB = doubleBidPlayerB;
            SetsA = setsA;
            SetsB = setsB;
            IdPlayerWinnerA = idPlayerWinnerA;
            IdPlayerWinnerB= idPlayerWinnerB;
            Date = date;
            Championship = championship;
            PointsDoubleAPlayerA = pointsDoubleAPlayerA;
            PointsDoubleAPlayerB = pointsDoubleAPlayerB;
            PointsDoubleBPlayerA = pointsDoubleBPlayerA;
            PointsDoubleBPlayerB = pointsDoubleBPlayerB;
            Phase = phase;
            
            if (Phase == null)
                Phase = Database.Instance.phases.SingleOrDefault(x => x.Name == "N/A");

            TeamGame = teamGame;
            InsertedManually = insertedManually;

            teamGame?.doubleGames.Add(this);
            teamGame?.AddResult(IdPlayerWinnerA == DoubleAIdPlayerA || IdPlayerWinnerB == DoubleAIdPlayerB ? true : false);

            Season = Database.Instance.seasons.SingleOrDefault(x => x.StartDate <= Date && x.EndDate >= Date && x.IsSingleSeason);
        }
        public override Season Season
        {
            get; set;
        }

        public int Id { get; private set; }
        public int DoubleAIdPlayerA { get; private set; }
        public int DoubleAIdPlayerB { get; private set; }
        public int DoubleBIdPlayerA { get; private set; }
        public int DoubleBIdPlayerB { get; private set; }
        public int IdPlayerWinnerA { get; private set; }
        public int IdPlayerWinnerB { get; private set; }
        public int SetsA { get; private set; }
        public int SetsB { get; private set; }
        public override bool IsDouble => true;

        public override string ResultA_B => $"{SetsA}-{SetsB}";

        public string DisplayDoubleA
        {
            get
            {
                var plA = Database.Instance.players.SingleOrDefault(x => x.Id == DoubleAIdPlayerA);
                var plB = Database.Instance.players.SingleOrDefault(x => x.Id == DoubleAIdPlayerB);

                var plAName = plA != null ? plA.SmallName : "N/A";
                var plBName = plB != null ? plB.SmallName : "N/A";

                return $"{plAName} / {plBName}";
            }
        }
        public string DisplayDoubleA_Ids
        {
            get
            {
                var plA = Database.Instance.players.SingleOrDefault(x => x.Id == DoubleAIdPlayerA);
                var plB = Database.Instance.players.SingleOrDefault(x => x.Id == DoubleAIdPlayerB);

                var plA_id = plA != null ? plA.Id : -1;
                var plB_id = plB != null ? plB.Id : -1;

                return $"{plA_id} / {plB_id}";
            }
        }

        public string DoubleANamePlayerA
        {
            get
            {
                return Database.Instance.players.SingleOrDefault(x => x.Id == DoubleAIdPlayerA)?.Name;
            }
        }
        public string DoubleANamePlayerB
        {
            get
            {
                return Database.Instance.players.SingleOrDefault(x => x.Id == DoubleAIdPlayerB)?.Name;
            }
        }

        public string DisplayDoubleB
        {
            get
            {
                var plA = Database.Instance.players.SingleOrDefault(x => x.Id == DoubleBIdPlayerA);
                var plB = Database.Instance.players.SingleOrDefault(x => x.Id == DoubleBIdPlayerB);

                var plAName = plA != null ? plA.SmallName : "N/A";
                var plBName = plB != null ? plB.SmallName : "N/A";

                return $"{plAName} / {plBName}";
            }
        }
        public string DoubleBNamePlayerA
        {
            get
            {
                return Database.Instance.players.SingleOrDefault(x => x.Id == DoubleBIdPlayerA)?.Name;
            }
        }
        public string DoubleBNamePlayerB
        {
            get
            {
                return Database.Instance.players.SingleOrDefault(x => x.Id == DoubleBIdPlayerB)?.Name;
            }
        }

        public int SetsCurrentPlayerA
        {
            get
            {
                if (currentPlayerA == -1)
                    return -1;

                if (currentPlayerA == DoubleAIdPlayerA)
                    return SetsA;
                else
                    return SetsB;
            }
        }
        public int SetsCurrentPlayerB
        {
            get
            {
                if (currentPlayerB == -1)
                    return -1;

                if (currentPlayerB == DoubleAIdPlayerB)
                    return SetsA;
                else
                    return SetsB;
            }
        }

        public int SetsOppPlayerA
        {
            get
            {
                if (currentPlayerA == -1)
                    return -1;

                if (currentPlayerA == DoubleAIdPlayerA)
                    return SetsB;
                else
                    return SetsA;
            }
        }

        public int SetsOppPlayerB
        {
            get
            {
                if (currentPlayerB == -1)
                    return -1;

                if (currentPlayerB == DoubleAIdPlayerB)
                    return SetsB;
                else
                    return SetsA;
            }
        }

        public bool PlayerAWinnerCurrentClub
        {
            get
            {
                if (currentClub == -1)
                    return false;

                if (IdPlayerWinnerA == DoubleAIdPlayerA)
                {
                    if (currentClub == TeamGame.ClubA.Id)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else if (IdPlayerWinnerA == DoubleBIdPlayerA)
                {
                    if (currentClub == TeamGame.ClubB.Id)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }

                return false;
            }
        }

        public bool PlayerBWinnerCurrentClub
        {
            get
            {
                if (currentClub == -1)
                    return false;

                if (IdPlayerWinnerB == DoubleAIdPlayerB)
                {
                    if (currentClub == TeamGame.ClubA.Id)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else if (IdPlayerWinnerB == DoubleBIdPlayerB)
                {
                    if (currentClub == TeamGame.ClubB.Id)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }

                return false;
            }
        }

        public bool ClubAWinner
        {
            get
            {
                if (currentClub == -1)
                    return false;

                if (IdPlayerWinnerA == DoubleAIdPlayerA)
                    return true;
                else
                    return false;
            }
        }
        public bool ClubBWinner
        {
            get
            {
                if (currentClub == -1)
                    return false;

                if (IdPlayerWinnerA == DoubleBIdPlayerA)
                    return true;
                else
                    return false;
            }
        }



        public override string Result
        {
            get
            {
                if (currentPlayer != -1)
                {
                    if (currentPlayer == DoubleAIdPlayerA || currentPlayer == DoubleAIdPlayerB)
                        return $"{SetsA} - {SetsB}";
                    else
                        return $"{SetsB} - {SetsA}";
                }
                if (currentPlayerA == -1)
                    return $"{SetsA} - {SetsB}";

                if (currentPlayerA == DoubleAIdPlayerA)
                    return $"{SetsA} - {SetsB}";
                else
                    return $"{SetsB} - {SetsA}";
            }
        }

        public int PointsCurrentPlayerA
        {
            get
            {
                if (currentPlayerA == -1)
                    return -1;

                if (currentPlayerA == IdPlayerWinnerA)
                    return PointsDoubleAPlayerA;
                else
                    return PointsDoubleBPlayerA;
            }
        }

        public int PointsCurrentPlayerB
        {
            get
            {
                if (currentPlayerB == -1)
                    return -1;

                if (currentPlayerB == IdPlayerWinnerB)
                    return PointsDoubleAPlayerB;
                else
                    return PointsDoubleBPlayerB;
            }
        }

        public override Color ResultColor
        {
            get
            {
                if(currentPlayer!=-1)
                {
                    if(currentPlayer == IdPlayerWinnerA || currentPlayer == IdPlayerWinnerB)
                        return Color.DarkOliveGreen;
                    else
                        return Color.MediumVioletRed;
                }

                if (currentPlayerA == -1)
                    return Color.White;
                if (currentPlayerA == IdPlayerWinnerA)
                    return Color.DarkOliveGreen;
                else
                    return Color.MediumVioletRed;
            }
        }

        public string PartnerName
        {
            get
            {
                if(currentPlayer == DoubleAIdPlayerA)
                    return Database.Instance.players.Single(x => x.Id == DoubleAIdPlayerB).SmallName;

                if (currentPlayer == DoubleAIdPlayerB)
                    return Database.Instance.players.Single(x => x.Id == DoubleAIdPlayerA).SmallName;

                if (currentPlayer == DoubleBIdPlayerA)
                    return Database.Instance.players.Single(x => x.Id == DoubleBIdPlayerB).SmallName;

                if (currentPlayer == DoubleBIdPlayerB)
                    return Database.Instance.players.Single(x => x.Id == DoubleBIdPlayerA).SmallName;

                return "N/A";
            }
        }
        public override string OppName
        {
            get
            {
                if (currentPlayer == DoubleAIdPlayerA || currentPlayer == DoubleAIdPlayerB)
                {
                    return Database.Instance.players.Single(x => x.Id == DoubleBIdPlayerA).Name + " / " + Database.Instance.players.Single(x => x.Id == DoubleBIdPlayerB).Name;
                }
                else
                {
                    return Database.Instance.players.Single(x => x.Id == DoubleAIdPlayerA).Name + " / " + Database.Instance.players.Single(x => x.Id == DoubleAIdPlayerB).Name;
                }
            }
        }
        public override string OppSmallName
        {
            get
            {
                if (currentPlayer == DoubleAIdPlayerA || currentPlayer == DoubleAIdPlayerB)
                {
                    return Database.Instance.players.Single(x => x.Id == DoubleBIdPlayerA).SmallName + " / " + Database.Instance.players.Single(x => x.Id == DoubleBIdPlayerB).SmallName;
                }
                else
                {
                    return Database.Instance.players.Single(x => x.Id == DoubleAIdPlayerA).SmallName + " / " + Database.Instance.players.Single(x => x.Id == DoubleAIdPlayerB).SmallName;
                }
            }
        }

        public override string PlayerSmallName
        {
            get
            {
                if (currentPlayer == DoubleAIdPlayerA || currentPlayer == DoubleAIdPlayerB)
                {
                    return Database.Instance.players.Single(x => x.Id == DoubleAIdPlayerA).SmallName + " / " + Database.Instance.players.Single(x => x.Id == DoubleAIdPlayerB).SmallName;
                }
                else
                {
                    return Database.Instance.players.Single(x => x.Id == DoubleBIdPlayerA).SmallName + " / " + Database.Instance.players.Single(x => x.Id == DoubleBIdPlayerB).SmallName;
                }
            }
        }

        public string OppNameA
        {
            get
            {
                if (currentPlayerA == -1)
                    return "N/A";

                if (currentPlayerA == DoubleAIdPlayerA)
                    return Database.Instance.players.Single(x => x.Id == DoubleBIdPlayerA).Name;
                else
                    return Database.Instance.players.Single(x => x.Id == DoubleAIdPlayerA).Name;
            }
        }
        public string OppNameB
        {
            get
            {
                if (currentPlayerB == -1)
                    return "N/A";

                if (currentPlayerB == DoubleAIdPlayerB)
                    return Database.Instance.players.Single(x => x.Id == DoubleBIdPlayerB).Name;
                else
                    return Database.Instance.players.Single(x => x.Id == DoubleAIdPlayerB).Name;
            }
        }

        public string PlayerNameA
        {
            get
            {
                if (currentPlayerA == -1)
                    return "N/A";

                if (currentPlayerA == DoubleAIdPlayerA)
                    return Database.Instance.players.Single(x => x.Id == DoubleAIdPlayerA).Name;
                else
                    return Database.Instance.players.Single(x => x.Id == DoubleBIdPlayerA).Name;
            }
        }
        public string PlayerNameB
        {
            get
            {
                if (currentPlayerA == -1)
                    return "N/A";

                if (currentPlayerA == DoubleAIdPlayerB)
                    return Database.Instance.players.Single(x => x.Id == DoubleAIdPlayerB).Name;
                else
                    return Database.Instance.players.Single(x => x.Id == DoubleBIdPlayerB).Name;
            }
        }

        public int OppIdA
        {
            get
            {
                if (currentPlayerA == -1)
                    return -1;

                if (currentPlayerA == DoubleAIdPlayerA)
                    return DoubleBIdPlayerA;
                else
                    return DoubleAIdPlayerA;
            }
        }
        public int OppIdB
        {
            get
            {
                if (currentPlayerB == -1)
                    return -1;

                if (currentPlayerB == DoubleAIdPlayerB)
                    return DoubleBIdPlayerB;
                else
                    return DoubleAIdPlayerB;
            }
        }

        public int PlayerIdA
        {
            get
            {
                if (currentPlayerA == -1)
                    return -1;

                if (currentPlayerA == DoubleAIdPlayerA)
                    return Database.Instance.players.Single(x => x.Id == DoubleAIdPlayerA).Id;
                else
                    return Database.Instance.players.Single(x => x.Id == DoubleBIdPlayerA).Id;
            }
        }
        public int PlayerIdB
        {
            get
            {
                if (currentPlayerB == -1)
                    return -1;

                if (currentPlayerA == DoubleAIdPlayerB)
                    return Database.Instance.players.Single(x => x.Id == DoubleAIdPlayerB).Id;
                else
                    return Database.Instance.players.Single(x => x.Id == DoubleBIdPlayerB).Id;
            }
        }

        private int currentPlayer = -1;
        public override int SetCurrentPlayer
        {
            set { currentPlayer = value; }
        }

        private int currentPlayerA = -1;
        public int SetCurrentPlayerA
        {
            set
            {
                currentPlayerA = value;
            }
        }
        private int currentPlayerB = -1;
        public int SetCurrentPlayerB
        {
            set
            {
                currentPlayerB = value;
            }
        }

        private int currentClub = -1;
        public int SetCurrentClub
        {
            set
            {
                currentClub = value;
                if (TeamGame == null)
                    return;

                if (TeamGame.ClubA.Id == value)
                {
                    currentClub = TeamGame.ClubA.Id;
                }
                else
                {
                    currentClub = TeamGame.ClubB.Id;
                }
            }
        }

        public int CurrentPlayerAClubId
        {
            get; private set;
        }

        public int CurrentPlayerBClubId
        {
            get; private set;
        }

        public int IdClubWinnerA
        {
            get
            {
                if (TeamGame == null)
                    return -1;

                if (IdPlayerWinnerA == DoubleAIdPlayerA)
                    return TeamGame.ClubA.Id;
                else if (IdPlayerWinnerA == DoubleBIdPlayerA)
                    return TeamGame.ClubB.Id;

                return -1;
            }
        }
        public int IdClubWinnerB
        {
            get
            {
                if (TeamGame == null)
                    return -1;

                if (IdPlayerWinnerB == DoubleAIdPlayerB)
                    return TeamGame.ClubA.Id;
                else if (IdPlayerWinnerB == DoubleBIdPlayerB)
                    return TeamGame.ClubB.Id;

                return -1;
            }
        }

        public override DateTime Date { get; protected set; }
        public override string DateFormated { get { return Date.ToString("dd-MM-yyyy"); } }
        public override Championship Championship { get; set; }
        public int IdCalculation { get; private set; }
        public int PointsDoubleAPlayerA { get; private set; }
        public int PointsDoubleAPlayerB { get; private set; }
        public int PointsDoubleBPlayerA { get; private set; }
        public int PointsDoubleBPlayerB { get; private set; }
        public Phase Phase { get; private set; }
        public TeamGame TeamGame { get; private set; }
        public bool InsertedManually { get; private set; }

        public override string GetDayOrPhase
        {
            get
            {
                var dayOrPhase = TeamGame != null ? TeamGame.DayOrPhase : Phase != null ? Phase.NameShort : "N/A";

                if (dayOrPhase.ToLower() == "n/a")
                    return dayOrPhase;

                dayOrPhase += $"{Environment.NewLine}+ {PartnerName}";
                return dayOrPhase;
            }
        }


        public override int PointsCurrentPlayer
        {
            get
            {
                if (currentPlayer == DoubleAIdPlayerA || currentPlayer == DoubleAIdPlayerB)
                    return PointsDoubleAPlayerA;
                else
                    return PointsDoubleBPlayerA;
            }
        }

        public override int OppId => -1;
    }
}
