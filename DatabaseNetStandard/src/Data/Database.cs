﻿//using DatabaseNetStandard.src.Data;
//using DatabaseNetStandard.src.Data.DataSet1TableAdapters;
using MySqlConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Threading.Tasks;
using System.Diagnostics;

namespace DatabaseNetStandard.Data
{
    public class TournamentType
    {
        public int Id { get; private set; }
        public string Name { get; private set; }
        public float Weight { get; private set; }
        public bool AllYears { get; private set; }
        public bool IsChampionship { get; private set; }

        public TournamentType(int id, string name, float weight, bool allYears, bool isChampionship)
        {
            Id = id;
            Name = name;
            Weight = weight;
            AllYears = allYears;
            IsChampionship = isChampionship;
        }
    }
    public class Level
    {
        public int Id { get; private set; }
        public string Name { get; private set; }
        public Level(int id, string name)
        {
            Id = id;
            Name = name;
        }
    }

    public class Championship
    {
        public int Id { get; private set; }
        public string Name { get; private set; }
        public TournamentType TournamentType { get; private set; }
        public DateTime Date { get; private set; }
        public string DateFormated { get { return Date.ToString("dd/MM/yyyy"); } }
        public Level Level { get; private set; }
        public string NameAutomaticInsertion { get; private set; }
        public string NameShort { get; private set; }
        public string DisplayNameTable { get { return NameShort != "" ? NameShort : Name; } }

        public string DisplayNameAutomatic_Name { get { return NameAutomaticInsertion != "" ? NameAutomaticInsertion : Name; } }
        public override string ToString()
        {
            return Name;
        }

        public Championship(int id, string name, TournamentType tournamentType, DateTime date, Level level, string nameAutomaticInsertion, string nameShort)
        {
            Id = id;
            Name = name;
            TournamentType = tournamentType;
            Date = date;
            Level = level;
            NameAutomaticInsertion = nameAutomaticInsertion;
            NameShort = nameShort;
        }
    }

    public class Phase
    {
        public int Id { get; private set; }
        public string Name { get; private set; }
        public string NameShort { get; private set; }
        public int Order { get; private set; }
        public int NumGames { get; private set; }
        public Phase(int id, string name, string nameShort, int order, int numGames)
        {
            Id = id;
            Name = name;
            NameShort = nameShort;
            Order = order;
            NumGames = numGames;
        }

        public override string ToString()
        {
            return Name;
        }
    }

    public class TeamGame
    {
        public int Id { get; private set; }
        public DateTime Date { get; private set; }
        public string DateFormated { get { return Date.ToString("dd-MM-yyyy"); } }
        public Championship Championship { get; private set; }
        public int Day { get; private set; }
        public Club ClubA { get; private set; }
        public string ClubANameAppend { get; private set; }
        public Club ClubB { get; private set; }
        public string ClubBNameAppend { get; private set; }

        /// <summary>
        /// ClubA name and the subsuquent letter if necessary
        /// </summary>
        public string ClubADisplayName
        {
            get
            {
                var append = ClubANameAppend != "" ? $" '{ClubANameAppend}'" : "";
                return $"{ClubA.DisplayName}{append}";
            }
        }
        /// <summary>
        /// ClubB name and the subsuquent letter if necessary
        /// </summary>
        public string ClubBDisplayName
        {
            get
            {
                var append = ClubBNameAppend != "" ? $" '{ClubBNameAppend}'" : "";
                return $"{ClubB.DisplayName}{append}";
            }
        }

        public string CurrentClubDisplayName
        {
            get
            {
                if (currentClub == -1)
                    return "N/A";

                if (currentClub == ClubA.Id)
                    return ClubADisplayName;
                else
                    return ClubBDisplayName;
            }
        }

        public int ResultClubA
        {
            get;
            set;
        }

        public int ResultClubB
        {
            get;
            set;
        }

        public string OppClubName
        {
            get
            {
                if (currentClub == -1)
                    return "N/A";

                if (currentClub == ClubA.Id)
                    return Database.Instance.clubs.Single(x => x.Id == ClubB.Id).Name;
                else
                    return Database.Instance.clubs.Single(x => x.Id == ClubA.Id).Name;
            }
        }


        public string Result
        {
            get
            {
                if (currentClub == -1)
                    return $"{ResultClubA} - {ResultClubB}";

                if (currentClub == ClubA.Id)
                    return $"{ResultClubA} - {ResultClubB}";
                else
                    return $"{ResultClubB} - {ResultClubA}";
            }
        }

        public string Result_ClubA_ClubB
        {
            get { return $"{ResultClubA} - {ResultClubB}"; }
        }

        public int OppClubId
        {
            get
            {
                if (currentClub == -1)
                    return -1;

                if (currentClub == ClubA.Id)
                    return ClubB.Id;
                else
                    return ClubA.Id;
            }
        }

        private int currentClub = -1;
        public int SetCurrentClub
        {
            set
            {
                currentClub = value;

                if (ClubA.Id == value)
                    currentClub = ClubA.Id;
                else
                    currentClub = ClubB.Id;
            }
        }
        public Color ResultColor
        {
            get
            {
                if (currentClub == -1)
                    return Color.White;


                if (currentClub == ClubA.Id)
                {
                    if (ResultClubA > ResultClubB)
                        return Color.DarkOliveGreen;
                    else if (ResultClubB > ResultClubA)
                        return Color.MediumVioletRed;
                }
                else if (currentClub == ClubB.Id)
                {
                    if (ResultClubB > ResultClubA)
                        return Color.DarkOliveGreen;
                    else if (ResultClubA > ResultClubB)
                        return Color.MediumVioletRed;
                }

                return Color.White;
            }
        }

        public int ClubWinner
        {
            get
            {
                if (ResultClubA > ResultClubB)
                    return ClubA.Id;
                else if(ResultClubB > ResultClubA)
                    return ClubB.Id;
                return -1;
            }
        }

        public bool CurrentClubWinner
        {
            get
            {
                if (currentClub == ClubWinner)
                    return true;
                else
                    return false;
            }
        }
        public Club CurrentClub
        {
            get
            {
                if (currentClub == ClubA.Id)
                    return ClubA;
                else
                    return ClubB;
            }
        }

        public Season Season
        {
            get; private set;
        }

        public List<Game> singularGames;
        public List<DoubleGame> doubleGames;

        

        public TeamGame(int id, DateTime date, Championship championship, int day, Club clubA, string clubANameAppend, Club clubB, string clubBNameAppend)
        {
            Id = id;
            Date = date;
            Championship = championship;
            Day = day;
            DayOrPhase = day.ToString();
            ClubA = clubA;
            ClubANameAppend = clubANameAppend;
            ClubB = clubB;
            ClubBNameAppend = clubBNameAppend;
            singularGames = new List<Game>();
            doubleGames = new List<DoubleGame>();
            try
            {
                Season = Database.Instance.seasons.SingleOrDefault(x => x.StartDate <= Date && x.EndDate >= Date && x.Name.ToLower() != "todasepocas" && !x.Name.ToLower().Contains("ate"));
            }
            catch (Exception ex)
            {
                throw;
            }
            if(Season==null)
            {
                Console.WriteLine("test");
            }
        }

        public void SetDayOrPhase(Game game)
        {
            DayOrPhase = Day > 0 ? Day.ToString() + " J" : game.Phase.NameShort;
            DayOrPhaseComplete = Day > 0 ? Day.ToString() + " J" : game.Phase.Name;
        }

        public string DayOrPhase
        {
            get; private set;
        }
        public string DayOrPhaseComplete
        {
            get; private set;
        }

        public void AddResult(bool playerAWinner)
        {
            if (playerAWinner)
                ResultClubA++;
            else
                ResultClubB++;

            //ResultClubA = games.Count(x => x.IdPlayerWinner == x.IdPlayerA);
            //ResultClubB = games.Count(x => x.IdPlayerWinner == x.IdPlayerB);
        }

        public int CurrentClubResult
        {
            get
            {
                if (currentClub == -1)
                    return -1;

                if (currentClub == ClubA.Id)
                    return ResultClubA;
                else if (currentClub == ClubB.Id)
                    return ResultClubB;

                return -1;
            }
        }
        public int OppClubResult
        {
            get
            {
                if (currentClub == -1)
                    return -1;

                if (currentClub == ClubA.Id)
                    return ResultClubB;
                else if (currentClub == ClubB.Id)
                    return ResultClubA;

                return -1;
            }
        }
    }

    public class VsPlayer : Player
    {
        public int VsGamesWon { get; set; }
        public int VsGamesLost { get; set; }
        public string VsSummary { get { return $"{VsGamesWon}-{VsGamesLost}"; } }
        public Color BackgroundColor
        {
            get
            {
                if (VsGamesWon > VsGamesLost)
                    return Color.DarkOliveGreen;
                else if (VsGamesLost > VsGamesWon)
                    return Color.MediumVioletRed;
                return Color.Transparent;
            }
        }

        //public VsPlayer(int id, string name, Club club, int currentPoints, int currentGamesWon, int currentRank, string licenceNo, int currentGamesLost, string nameAutomaticInsertion)
        //    :base(id, name, club, currentPoints, currentGamesWon, currentRank, licenceNo, currentGamesLost, nameAutomaticInsertion)
        //{

        //}

        public VsPlayer(Player p)
            :base(p.Id, p.Name, p.Club, p.CurrentPoints, p.CurrentGamesWon, p.CurrentRank, p.LicenceNo, p.CurrentGamesLost, p.NameAutomaticInsertion, p.BasePoints, p.DominantHand, p.GameStyle)
        {

        }
    }

    public class DominantHand
    {
        public int Id { get; private set; }
        public string Text_En { get; private set; }
        public string Text_Pt { get; private set; }

        public DominantHand(int id, string text_en, string text_pt)
        {
            Id = id;
            Text_En = text_en;
            Text_Pt = text_pt;
        }
    }

    public class GameStyle
    {
        public int Id { get; private set; }
        public string Style { get; private set; }
        public GameStyle (int id, string style)
        {
            Id = id;
            Style = style;
        }
    }

    public class Player
    {
        public int Id { get; private set; }
        public string Name { get; private set; }
        public Club Club { get; private set; }
        public int CurrentPoints { get; private set; }
        public int CurrentRank { get; private set; }
        public string LicenceNo { get; private set; }
        public int CurrentGamesWon { get; private set; }
        public int CurrentGamesLost { get; private set; }
        public string NameAutomaticInsertion { get; private set; }
        public int BasePoints { get; private set; }
        public DominantHand DominantHand { get; private set; }
        public GameStyle GameStyle { get; private set; }
        public string SmallName { get; private set; }

        public string DisplayName { get { return NameAutomaticInsertion != "" ? NameAutomaticInsertion : Name; } }

        public string GamesSummary { get { return $"{CurrentGamesWon} - {CurrentGamesLost} ({((CurrentGamesWon*1.0f)/(CurrentGamesWon+CurrentGamesLost)).ToString("0 %")})"; } }

        public override string ToString()
        {
            return Name;
        }

        public Player(int id, string name, Club club, int currentPoints, int currentGamesWon, int currentRank, string licenceNo, int currentGamesLost, string nameAutomaticInsertion, int basePoints, DominantHand dominantHand, GameStyle gameStyle)
        {
            Id = id;
            Name = name;
            Club = club;
            CurrentPoints = currentPoints;
            CurrentRank = currentRank;
            LicenceNo = licenceNo;
            CurrentGamesWon = currentGamesWon;
            CurrentGamesLost = currentGamesLost;
            NameAutomaticInsertion = nameAutomaticInsertion;
            BasePoints = basePoints;
            DominantHand = dominantHand;
            GameStyle = gameStyle;
            var split = DisplayName.Split(' ').ToList();
            if (split.Count > 1)
                SmallName = split[0] + " " + split[split.Count() - 1];
            else
                SmallName = DisplayName;
        }

        public override bool Equals(object obj)
        {
            return (obj as Player).Id == Id;
        }
    }

    public class Club
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Association Association { get; private set; }
        public string Shortname { get; private set; }
        public string NameAutomaticInsertion { get; private set; }
        public string DisplayName { get { return NameAutomaticInsertion != "" ? NameAutomaticInsertion : Name; } }
        
        public string GamesSummary
        {
            get
            {
                if (TeamGames == null || TeamGames.Count == 0)
                    return "N/A";

                var numWins = TeamGames.Count(x => x.ClubWinner == Id);
                var numLoses = TeamGames.Count(x => x.ClubWinner!=-1 && x.ClubWinner != Id);

                return $"{numWins} - {numLoses}";
            }
        }
        public List<TeamGame> TeamGames { get; set; }

        public Club(int id, string name, Association association, string shortname, string nameAutomaticInsertion)
        {
            Id = id;
            Name = name;
            Association = association;
            Shortname = shortname;
            NameAutomaticInsertion = nameAutomaticInsertion;
        }
    }

    public class Association
    {
        public int Id { get; private set; }
        public string Name { get; private set; }
        public bool IsReal { get; private set; }

        public Association(int id, string name, bool isReal)
        {
            Id = id;
            Name = name;
            IsReal = isReal;
        }

        public override string ToString()
        {
            return Name;
        }

    }

    public class Season
    {
        public int Id { get; private set; }
        public string Name { get; private set; }
        public DateTime StartDate { get; private set; }
        public DateTime EndDate { get; private set; }
        public string EndDateStr { get { return EndDate.ToString("dd/MM/yyyy"); } }
        public bool IsSingleSeason { get; private set; }
        public Season(int id, string name, DateTime startDate, DateTime endDate, bool isSingleSeason)
        {
            Id = id;
            Name = name;
            StartDate = startDate;
            EndDate = endDate;
            IsSingleSeason = isSingleSeason;
        }
        public override string ToString()
        {
            return Name;
        }
    }

    public class Ranking
    {
        public int Id { get; private set; }
        public Player Player { get; private set; }
        public Championship Championship { get; private set; }
        public Season Season { get; private set; }
        public int NumGames { get; private set; }
        public int NumWins { get; private set; }
        public int NumLoses { get; private set; }
        public int Rank { get; private set; }
        public int Points { get; private set; }
        public Club Club { get; private set; }
        public string GamesSummary { get { return $"{NumWins} - {NumLoses}"; } }

        public string GamesSummarySeason { get; set; }
        public string InfoText { get; set; }
        public int Difference { get; set; }
        public string DifferenceStr
        {
            get { return Difference > 0 ? $"+ {Difference}" : $"{Difference}"; }
        }
        public Color DifferenceColor
        {
            get
            {
                if (Difference >= 0)
                    return Color.DarkOliveGreen;
                else
                    return Color.MediumVioletRed;
            }
        }
        public Ranking(int id, Player player, Championship championship, Season season, int numGames, int numWins, int numLoses, int rank, int points, Club club)
        {
            Id = id;
            Player = player;
            Championship = championship;
            Season = season;
            NumGames = numGames;
            NumWins = numWins;
            NumLoses = numLoses;
            Rank = rank;
            Points = points;
            Club = club;
        }

        public void UpdateGames(int numWins, int numLoses)
        {
            NumWins = numWins;
            NumLoses = numLoses;
        }
    }

    public class Database
    {
        private static Database instance;
        private string connection = "server=69.10.59.194;user id=st9767_tabletennisranking;port=3306;database=st9767_tabletennisranking;persistsecurityinfo=True;password=123456";
        private bool isConnected = false;
        private MySqlConnection conn;

        public List<Player> players = new List<Player>();
        public List<Club> clubs = new List<Club>();
        public List<Association> associations = new List<Association>();
        public List<Game> games = new List<Game>();
        public List<DoubleGame> doubleGames = new List<DoubleGame>();
        public List<Championship> championships = new List<Championship>();
        public List<Phase> phases = new List<Phase>();
        public List<TeamGame> teamGames = new List<TeamGame>();
        public List<TournamentType> tournamentTypes = new List<TournamentType>();
        public List<Level> levels = new List<Level>();
        public List<Season> seasons = new List<Season>();
        public List<Ranking> rankings = new List<Ranking>();
        public List<DominantHand> dominantHands = new List<DominantHand>();
        public List<GameStyle> gameStyles = new List<GameStyle>();

        public static Database Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Database();
                    instance.isConnected = false;
                    instance.conn = new MySqlConnection(instance.connection);
                    instance.Initialization();
                }

                return instance;
            }
        }

        public static void LoadDatabaseIfNotLoaded()
        {
            Trace.WriteLine($"Loading database: {Instance}");
        }

        private void Initialization()
        {
            Stopwatch s1 = Stopwatch.StartNew();
            conn.Open();
            var a = Task.Factory.StartNew(() =>
            {
                var query = "SELECT * FROM associations";
                var cmd = new MySqlCommand(query, conn);
                var returnVal = new MySqlDataAdapter(query, conn);
                var rdr = cmd.ExecuteReader();
                if (rdr.HasRows)
                {
                    while (rdr.Read())
                    {
                        var id = Convert.ToInt32(rdr[0]);
                        var name = rdr[1].ToString();
                        var isReal = Convert.ToBoolean(rdr[2]);
                        associations.Add(new Association(id, name, isReal));
                    }
                }
                rdr.Close();


                query = "SELECT * FROM season";
                cmd = new MySqlCommand(query, conn);
                returnVal = new MySqlDataAdapter(query, conn);
                rdr = cmd.ExecuteReader();
                if (rdr.HasRows)
                {
                    while (rdr.Read())
                    {
                        var id = Convert.ToInt32(rdr[0]);
                        var name = rdr[1].ToString();
                        var startDate = DateTime.Parse(rdr[2].ToString());
                        var endDate = DateTime.Parse(rdr[3].ToString());
                        var isSingleSeason = Convert.ToBoolean(rdr[4]);
                        seasons.Add(new Season(id, name, startDate, endDate, isSingleSeason));
                    }
                }
                rdr.Close();

                query = "SELECT * FROM clubs";
                cmd = new MySqlCommand(query, conn);
                returnVal = new MySqlDataAdapter(query, conn);
                rdr = cmd.ExecuteReader();
                if (rdr.HasRows)
                {
                    while (rdr.Read())
                    {
                        var id = Convert.ToInt32(rdr[0]);
                        var name = rdr[1].ToString();
                        var association = associations.SingleOrDefault(x => x.Id == Convert.ToInt32(rdr[3]));
                        if (association == null || association.Id == 0)
                            association = associations.Single(x => x.Name.ToLower() == "faltadefinir");
                        var shortname = rdr[4].ToString();
                        var nameAutomaticInsertion = rdr[5].ToString();
                        clubs.Add(new Club(id, name, association, shortname, nameAutomaticInsertion));
                    }
                }
                rdr.Close();

                query = "SELECT * FROM dominantHand";
                cmd = new MySqlCommand(query, conn);
                returnVal = new MySqlDataAdapter(query, conn);

                rdr = cmd.ExecuteReader();

                if (rdr.HasRows)
                {
                    while (rdr.Read())
                    {
                        var id = Convert.ToInt32(rdr[0]);
                        var hand_en = rdr[1].ToString();
                        var hand_pt = rdr[2].ToString();
                        dominantHands.Add(new DominantHand(id, hand_en, hand_pt));
                    }
                }
                rdr.Close();

                query = "SELECT * FROM gamestyle";
                cmd = new MySqlCommand(query, conn);
                returnVal = new MySqlDataAdapter(query, conn);

                rdr = cmd.ExecuteReader();

                if (rdr.HasRows)
                {
                    while (rdr.Read())
                    {
                        var id = Convert.ToInt32(rdr[0]);
                        var style = rdr[1].ToString();
                        gameStyles.Add(new GameStyle(id, style));
                    }
                }
                rdr.Close();

                query = "SELECT * FROM player";
                cmd = new MySqlCommand(query, conn);
                returnVal = new MySqlDataAdapter(query, conn);

                rdr = cmd.ExecuteReader();

                if (rdr.HasRows)
                {
                    while (rdr.Read())
                    {
                        var id = Convert.ToInt32(rdr[0]);
                        var name = rdr[1].ToString();
                        var club = clubs.SingleOrDefault(x => x.Id == Convert.ToInt32(rdr[3]));
                        var currentPoints = Convert.ToInt32(rdr[6]);
                        var currentGamesWon = Convert.ToInt32(rdr[7]);
                        var currentRank = Convert.ToInt32(rdr[8]);
                        var licenceNo = rdr[9].ToString();
                        var currentGamesLost = Convert.ToInt32(rdr[10]);
                        var nameAutomaticInsertion = rdr[11].ToString();
                        var basePoints = Convert.ToInt32(rdr[12]);
                        var dominantHand = dominantHands.SingleOrDefault(x => x.Id == Convert.ToInt32(rdr[13]));
                        var gameStyle = gameStyles.SingleOrDefault(x => x.Id == Convert.ToInt32(rdr[14]));
                        players.Add(new Player(id, name, club, currentPoints, currentGamesWon, currentRank, licenceNo, currentGamesLost, nameAutomaticInsertion, basePoints, dominantHand, gameStyle));
                    }
                }
                rdr.Close();

                query = "SELECT * FROM tournamenttype";
                cmd = new MySqlCommand(query, conn);
                returnVal = new MySqlDataAdapter(query, conn);
                rdr = cmd.ExecuteReader();
                if (rdr.HasRows)
                {
                    while (rdr.Read())
                    {
                        var id = Convert.ToInt32(rdr[0]);
                        var name = rdr[1].ToString();
                        var weight = Convert.ToSingle(rdr[2]);
                        var allYears = Convert.ToBoolean(rdr[3]);
                        var isChampionship = Convert.ToBoolean(rdr[4]);

                        tournamentTypes.Add(new TournamentType(id, name, weight, allYears, isChampionship));
                    }
                }
                rdr.Close();

                query = "SELECT * FROM levels";
                cmd = new MySqlCommand(query, conn);
                returnVal = new MySqlDataAdapter(query, conn);
                rdr = cmd.ExecuteReader();
                if (rdr.HasRows)
                {
                    while (rdr.Read())
                    {
                        var id = Convert.ToInt32(rdr[0]);
                        var name = rdr[1].ToString();

                        levels.Add(new Level(id, name));
                    }
                }
                rdr.Close();


                query = "SELECT * FROM championships";
                cmd = new MySqlCommand(query, conn);
                returnVal = new MySqlDataAdapter(query, conn);
                rdr = cmd.ExecuteReader();
                if (rdr.HasRows)
                {
                    while (rdr.Read())
                    {
                        var id = Convert.ToInt32(rdr[0]);
                        var name = rdr[1].ToString();
                        var tournamentType = tournamentTypes.SingleOrDefault(x => x.Id == Convert.ToInt32(rdr[2]));
                        var date = DateTime.Parse(rdr[3].ToString());
                        var level = levels.SingleOrDefault(x => x.Id == Convert.ToInt32(rdr[4]));
                        var nameAutomaticInsertion = rdr[5].ToString();
                        var nameShort = rdr[6].ToString();

                        championships.Add(new Championship(id, name, tournamentType, date, level, nameAutomaticInsertion, nameShort));
                    }
                }
                rdr.Close();

                query = "SELECT * FROM teamgame";
                cmd = new MySqlCommand(query, conn);
                returnVal = new MySqlDataAdapter(query, conn);
                rdr = cmd.ExecuteReader();
                if (rdr.HasRows)
                {
                    while (rdr.Read())
                    {
                        var id = Convert.ToInt32(rdr[0]);
                        var date = DateTime.Parse(rdr[1].ToString());
                        var champ = championships.SingleOrDefault(x => x.Id == Convert.ToInt32(rdr[2]));
                        var day = Convert.ToInt32(rdr[3]);
                        var clubA = clubs.SingleOrDefault(x => x.Id == Convert.ToInt32(rdr[4]));
                        var clubAappend = rdr[5].ToString();
                        var clubB = clubs.SingleOrDefault(x => x.Id == Convert.ToInt32(rdr[6]));
                        var clubBappend = rdr[7].ToString();

                        teamGames.Add(new TeamGame(id, date, champ, day, clubA, clubAappend, clubB, clubBappend));
                    }
                }
                rdr.Close();

                query = "SELECT * FROM phase";
                cmd = new MySqlCommand(query, conn);
                returnVal = new MySqlDataAdapter(query, conn);
                rdr = cmd.ExecuteReader();
                if (rdr.HasRows)
                {
                    while (rdr.Read())
                    {
                        var id = Convert.ToInt32(rdr[0]);
                        var name = rdr[1].ToString();
                        var nameShort = rdr[2].ToString();
                        var order = Convert.ToInt32(rdr[3]);
                        var numGames = Convert.ToInt32(rdr[4]);

                        phases.Add(new Phase(id, name, nameShort, order, numGames));
                    }
                }
                rdr.Close();

                query = "SELECT * FROM games";
                cmd = new MySqlCommand(query, conn);
                returnVal = new MySqlDataAdapter(query, conn);
                rdr = cmd.ExecuteReader();
                if (rdr.HasRows)
                {
                    while (rdr.Read())
                    {
                        var id = Convert.ToInt32(rdr[0]);
                        var idPlayerA = Convert.ToInt32(rdr[1]);
                        var idPlayerB = Convert.ToInt32(rdr[2]);
                        var setsA = Convert.ToInt32(rdr[3]);
                        var setsB = Convert.ToInt32(rdr[4]);
                        var idPlayerWinner = Convert.ToInt32(rdr[5]);
                        var date = DateTime.Parse(rdr[6].ToString());
                        var champ = championships.SingleOrDefault(x => x.Id == Convert.ToInt32(rdr[7]));
                        var idCalculation = Convert.ToInt32(rdr[8]);
                        var pointsPlayerA = Convert.ToInt32(rdr[9]);
                        var pointsPlayerB = Convert.ToInt32(rdr[10]);
                        var phase = phases.SingleOrDefault(x => x.Id == Convert.ToInt32(rdr[11]));
                        var teamGame = Convert.ToInt32(rdr[12]) != 0 ? teamGames.SingleOrDefault(x => x.Id == Convert.ToInt32(rdr[12])) : null;
                        var insertedManually = Convert.ToBoolean(rdr[13]);

                        games.Add(new Game(id, idPlayerA, idPlayerB, setsA, setsB, idPlayerWinner, date, champ, idCalculation, pointsPlayerA, pointsPlayerB, phase, teamGame, insertedManually));
                    }
                }
                rdr.Close();

                query = "SELECT * FROM doublegames";
                cmd = new MySqlCommand(query, conn);
                returnVal = new MySqlDataAdapter(query, conn);
                rdr = cmd.ExecuteReader();
                if (rdr.HasRows)
                {
                    while (rdr.Read())
                    {
                        var id = Convert.ToInt32(rdr[0]);
                        var doubleAplayerA = Convert.ToInt32(rdr[1]);
                        var doubleAplayerB = Convert.ToInt32(rdr[2]);
                        var doubleBplayerA = Convert.ToInt32(rdr[3]);
                        var doubleBplayerB = Convert.ToInt32(rdr[4]);
                        var setsA = Convert.ToInt32(rdr[5]);
                        var setsB = Convert.ToInt32(rdr[6]);
                        var idPlayerWinnerA = Convert.ToInt32(rdr[7]);
                        var idPlayerWinnerB = Convert.ToInt32(rdr[8]);
                        var date = DateTime.Parse(rdr[9].ToString());
                        var champ = championships.SingleOrDefault(x => x.Id == Convert.ToInt32(rdr[10]));
                        var pointsDoubleAPlayerA = Convert.ToInt32(rdr[11]);
                        var pointsDoubleAPlayerB = Convert.ToInt32(rdr[12]);
                        var pointsDoubleBPlayerA = Convert.ToInt32(rdr[13]);
                        var pointsDoubleBPlayerB = Convert.ToInt32(rdr[14]);
                        var phase = phases.SingleOrDefault(x => x.Id == Convert.ToInt32(rdr[15]));
                        var teamGame = Convert.ToInt32(rdr[16]) != 0 ? teamGames.SingleOrDefault(x => x.Id == Convert.ToInt32(rdr[16])) : null;
                        var insertedManually = Convert.ToBoolean(rdr[17]);

                        doubleGames.Add(new DoubleGame(id, doubleAplayerA, doubleAplayerB, doubleBplayerA, doubleBplayerB, setsA, setsB, idPlayerWinnerA, idPlayerWinnerB, date, champ, pointsDoubleAPlayerA, pointsDoubleAPlayerB, pointsDoubleBPlayerA, pointsDoubleBPlayerB, phase, teamGame, insertedManually));
                    }
                }
                rdr.Close();

                query = "SELECT * FROM ranking";
                cmd = new MySqlCommand(query, conn);
                returnVal = new MySqlDataAdapter(query, conn);
                rdr = cmd.ExecuteReader();
                if (rdr.HasRows)
                {
                    while (rdr.Read())
                    {
                        var id = Convert.ToInt32(rdr[0]);
                        var player = players.SingleOrDefault(x => x.Id == Convert.ToInt32(rdr[1]));
                        var champ = championships.SingleOrDefault(x => x.Id == Convert.ToInt32(rdr[2]));
                        var season = seasons.SingleOrDefault(x => x.Id == Convert.ToInt32(rdr[3]));
                        var numGames = Convert.ToInt32(rdr[4]);
                        var numWins = Convert.ToInt32(rdr[5]);
                        var numLoses = Convert.ToInt32(rdr[6]);
                        var rank = Convert.ToInt32(rdr[7]);
                        var points = Convert.ToInt32(rdr[8]);
                        var club = clubs.SingleOrDefault(x => x.Id == Convert.ToInt32(rdr[9]));

                        rankings.Add(new Ranking(id, player, champ, season, numGames, numWins, numLoses, rank, points, club));
                    }
                }
                rdr.Close();
            });
            a.Wait();
            s1.Stop();
            Console.WriteLine($"Time to load database: {s1.ElapsedMilliseconds}ms");

            foreach (var c in clubs)
                c.TeamGames = teamGames.Where(x => x.ClubA.Id == c.Id || x.ClubB.Id == c.Id).ToList();

            //var allChamps = Database.Instance.rankings.Select(x => x.Championship).Distinct();
            //var allSeason = Database.Instance.rankings.Select(x => x.Season).Distinct();

            //foreach(var champ in allChamps)
            //{
            //    foreach(var season in allSeason)
            //    {
            //        var data = rankings.Where(x => x.Championship.Id == champ.Id && x.Season.Id == season.Id);
            //        if (data.Count() == 0)
            //            continue;
            //        var maxRank = data.Max(x => x.Rank);
            //        for(int i=0; i<maxRank; i++)
            //        {
            //            var analyze = data.Where(x => x.Rank == i);
            //            if(analyze.Count()>1)
            //            {
            //                var id = analyze.First().Id;
            //                var player = analyze.First().Player.Id;
            //                var numGames = analyze.First().NumGames;
            //                var numWins = analyze.First().NumWins;
            //                var numLosses = analyze.First().NumLoses;
            //                var points = analyze.First().Points;

            //                foreach(var b in analyze.Skip(1))
            //                {
            //                    if(b.Player.Id == player && b.NumGames == numGames && b.NumWins == numWins && b.NumLoses == numLosses && b.Points == points)
            //                    {
            //                        var query = $"DELETE FROM ranking WHERE `id` = {b.Id}";
            //                        var cmd = new MySqlCommand(query, conn);

            //                        var result = cmd.ExecuteNonQuery();

            //                    }
            //                }
            //            }

            //        }
            //    }
            //}

        }

    }

}