﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DatabaseNetStandard.Data
{
    public class JornadaTable
    {
        public string DayFormated { get { return $"Jornada {Day}"; } }
        public int Day { get; set; }
        public int Height { get; set; }
        public List<JornadaRow> Games { get; set; }
    }

    public class JornadaRow
    {
        public int TeamGameId { get; set; }
        public DateTime Date { get; set; }
        public string DateFormated { get; set; }
        public Club ClubA { get; set; }
        public string ClubAFormated { get; set; }
        public Club ClubB { get; set; }
        public string ClubBFormated { get; set; }
        public string Result { get; set; }
        
    }
}
