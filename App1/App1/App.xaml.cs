﻿using App1.Services;
using App1.Views;
using System;
using System.Linq;
using System.Threading.Tasks;
using TenisMesaBaseDados.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App1
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            DependencyService.Register<MockDataStore>();

            SplashPage.DatabaseLoaded += SplashPage_DatabaseLoaded;
            MainPage = new SplashPage();
            //MainPage = new AppShell();
        }

        private void SplashPage_DatabaseLoaded(object sender, EventArgs e)
        {
            Device.BeginInvokeOnMainThread(() => Application.Current.MainPage = new AppShell());
        }

        private void App_DatabaseLoaded(object sender, EventArgs e)
        {
        }

        protected override void OnStart()
        {
            Task.Factory.StartNew(() =>
                (MainPage as SplashPage).LoadDatabase()
            );
            // Handle when your app starts
            //MainPage = new SplashPage();
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }

        
    }
}
