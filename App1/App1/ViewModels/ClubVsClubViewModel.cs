﻿using DatabaseNetStandard.Data;
using Prism.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;
using System.Linq;
using System.Globalization;
using System.ComponentModel;
using App1.Views;

namespace App1.ViewModels
{
    public class ClubVsClubViewModel : BaseViewModel
    {
        public ClubVsClubViewModel()
        {
            ItemChampionshipCommand = new DelegateCommand(ItemChampionshipClicked);
            ItemPlayerCommand = new DelegateCommand<object>(ItemPlayerClicked);
            ItemTappedCommand = new DelegateCommand(ItemTapped);
        }

        public DelegateCommand ItemTappedCommand { get; private set; }
        public Game SelectedItem { get; set; }
        void ItemTapped()
        {
            // Here you can do whatever you want, this will be executed when
            // user clicks on item in ListView, you will have a value of tapped
            // item in SlectedProfile property
            var page = new RankingPage();
            var season = Database.Instance.seasons.SingleOrDefault(x => SelectedItem.Date > x.StartDate && SelectedItem.Date < x.EndDate && x.Name.ToLower() != "todasepocas");
            if (season == null)
                season = Database.Instance.seasons.SingleOrDefault(x =>  x.Name.ToLower() == "todasepocas");
            page.FillRanking(season, SelectedItem.Championship);
            Application.Current.MainPage.Navigation.PushAsync(page);
        }

        Club c1;
        public Club Club1
        {
            get
            { 
                return c1; 
            }
            set
            {
                c1 = value;
                OnPropertyChanged(nameof(Club1));
            }
        }
        Club c2;
        public Club Club2
        {
            get
            {
                return c2;
            }
            set
            {
                c2 = value;
                OnPropertyChanged(nameof(Club2));
            }
        }


        void ItemChampionshipClicked()
        {

        }


        void ItemPlayerClicked(object id_object)
        {
            int id = Convert.ToInt32(id_object);
            var p = Database.Instance.players.SingleOrDefault(x => x.Id == id);
            if (p == null)
                return;
            Application.Current.MainPage.Navigation.PushAsync(new PlayerTabbedPage(p));
        }


        public DelegateCommand ItemChampionshipCommand { get; private set; }
        public DelegateCommand<object> ItemPlayerCommand { get; private set; }

        public void DefineClubs(Club c1, Club c2)
        {
            if (c1 == null || c2 == null)
                return;

            if (Club1 == null || Club2==null || Club1.Id != c1.Id || Club2.Id != c2.Id)
            {
                Club1 = c1;
                Club2 = c2;
            }

            var c1Name = Club1.NameAutomaticInsertion != "" ? Club1.NameAutomaticInsertion : Club1.Name;
            var c2Name = Club2.NameAutomaticInsertion != "" ? Club2.NameAutomaticInsertion : Club2.Name;
            Title = $"{c1Name} VS {c2Name}";
            FillResults();
        }

        private void FillResults()
        {
            var teamGames = Database.Instance.teamGames.Where(x=> (x.ClubA.Id == c1.Id && x.ClubB.Id == c2.Id) || (x.ClubB.Id == c1.Id && x.ClubA.Id == c2.Id)).ToList();

            var teamGamesIds = teamGames.Select(x => x.Id);

            var games = Database.Instance.games.Where(x => teamGamesIds.Contains(x.TeamGame.Id)).ToList();

            //we set player1 as the defined player
            games.ForEach(x => x.SetCurrentClub = Club1.Id);

            var allGames = teamGames.Count * 1.0f;
            var gamesWonThreeZero = games.Count(x => x.SetsCurrentPlayer == 3 && x.SetsOppPlayer == 0);
            NumGamesWonThreeZeroText = $"{gamesWonThreeZero} ({((gamesWonThreeZero / allGames)).ToString("0 %")})";

            var gamesWonThreeOne = games.Count(x => x.SetsCurrentPlayer == 3 && x.SetsOppPlayer == 1);
            NumGamesWonThreeOneText = $"{gamesWonThreeOne} ({((gamesWonThreeOne / allGames)).ToString("0 %")})";

            var gamesWonThreeTwo = games.Count(x => x.SetsCurrentPlayer == 3 && x.SetsOppPlayer == 2);
            NumGamesWonThreeTwoText = $"{gamesWonThreeTwo} ({((gamesWonThreeTwo / allGames)).ToString("0 %")})";



            var gamesLostThreeZero = games.Count(x => x.SetsCurrentPlayer == 0 && x.SetsOppPlayer == 3);
            NumGamesLostThreeZeroText = $"{gamesLostThreeZero} ({((gamesLostThreeZero / allGames)).ToString("0 %")})";

            var gamesLostThreeOne = games.Count(x => x.SetsCurrentPlayer == 1 && x.SetsOppPlayer == 3);
            NumGamesLostThreeOneText = $"{gamesLostThreeOne} ({((gamesLostThreeOne / allGames)).ToString("0 %")})";


            var gamesLostThreeTwo = games.Count(x => x.SetsCurrentPlayer == 2 && x.SetsOppPlayer == 3);
            NumGamesLostThreeTwoText = $"{gamesLostThreeTwo} ({((gamesLostThreeTwo / allGames)).ToString("0 %")})";

            var gamesWon = games.Count(x => x.IdClubWinner == c1.Id);
            NumGamesWon = $"{gamesWon} ({(gamesWon / allGames).ToString("0%")})";

            var gamesLost = games.Count(x => x.IdClubWinner == c2.Id);
            NumGamesLost = $"{gamesLost} ({(gamesLost / allGames).ToString("0%")})";

            Results = new ObservableCollection<Game>(games.OrderByDescending(x => x.Date));
        }
        private string numGamesWonThreeZeroText;
        public string NumGamesWonThreeZeroText
        {
            get { return numGamesWonThreeZeroText; }
            set { SetProperty<string>(ref numGamesWonThreeZeroText, value, nameof(NumGamesWonThreeZeroText)); }
        }

        private string numGamesWon;
        public string NumGamesWon
        {
            get { return numGamesWon; }
            set { SetProperty<string>(ref numGamesWon, value, nameof(NumGamesWon)); }
        }
        private string numGamesLost;
        public string NumGamesLost
        {
            get { return numGamesLost; }
            set { SetProperty<string>(ref numGamesLost, value, nameof(NumGamesLost)); }
        }

        private string numGamesWonThreeOneText;
        public string NumGamesWonThreeOneText
        {
            get { return numGamesWonThreeOneText; }
            set { SetProperty<string>(ref numGamesWonThreeOneText, value, nameof(NumGamesWonThreeOneText)); }
        }


        private string numGamesWonThreeTwoText;
        public string NumGamesWonThreeTwoText
        {
            get { return numGamesWonThreeTwoText; }
            set { SetProperty<string>(ref numGamesWonThreeTwoText, value, nameof(NumGamesWonThreeTwoText)); }
        }


        private string numGamesLostThreeZeroText;
        public string NumGamesLostThreeZeroText
        {
            get { return numGamesLostThreeZeroText; }
            set { SetProperty<string>(ref numGamesLostThreeZeroText, value, nameof(NumGamesLostThreeZeroText)); }
        }


        private string numGamesLostThreeOneText;
        public string NumGamesLostThreeOneText
        {
            get { return numGamesLostThreeOneText; }
            set { SetProperty<string>(ref numGamesLostThreeOneText, value, nameof(NumGamesLostThreeOneText)); }
        }


        private string numGamesLostThreeTwoText;
        public string NumGamesLostThreeTwoText
        {
            get { return numGamesLostThreeTwoText; }
            set { SetProperty<string>(ref numGamesLostThreeTwoText, value, nameof(NumGamesLostThreeTwoText)); }
        }
        private ObservableCollection<Game> results = new ObservableCollection<Game>();

        public ObservableCollection<Game> Results
        {
            get { return results; }
            set { SetProperty(ref results, value); }
        }

    }
}