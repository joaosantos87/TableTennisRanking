﻿using DatabaseNetStandard.Data;
using Prism.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;
using System.Linq;
using System.Globalization;
using System.ComponentModel;
using App1.Views;

namespace App1.ViewModels
{
    public class ClubPlayersHistoricViewModel : BaseViewModel
    {
        public ClubPlayersHistoricViewModel()
        {
            ItemTappedCommand = new DelegateCommand(ItemTapped);
            ItemPlayerCommand = new DelegateCommand<object>(ItemPlayerClicked);
            ItemChampionshipCommand = new DelegateCommand<object>(ItemChampionshipClicked);
            ItemRankCommand = new DelegateCommand<object>(ItemRankClicked);
        }
        public DelegateCommand<object> ItemRankCommand { get; private set; }
        public DelegateCommand<object> ItemPlayerCommand { get; private set; }
        public DelegateCommand<object> ItemChampionshipCommand { get; private set; }
        void ItemRankClicked(object id_object)
        {
            int id = Convert.ToInt32(id_object);
            var r = Database.Instance.rankings.SingleOrDefault(x => x.Id == id);
            if (r == null)
                return;

            var page = new RankingPage();
            page.FillRanking(r.Season, r.Championship);
            Application.Current.MainPage.Navigation.PushAsync(page);
        }
        void ItemPlayerClicked(object id_object)
        {
            int id = Convert.ToInt32(id_object);
            var p = Database.Instance.players.SingleOrDefault(x => x.Id == id);
            if (p == null)
                return;
            Application.Current.MainPage.Navigation.PushAsync(new PlayerTabbedPage(p));
        }

        void ItemChampionshipClicked(object id_object)
        {
            int id = Convert.ToInt32(id_object);
            var champ = Database.Instance.championships.SingleOrDefault(x => x.Id == id);
            if (champ == null)
                return;
            Application.Current.MainPage.Navigation.PushAsync(new ChampionshipTabbedPage(champ));
        }

        public List<Season> Seasons
        {
            get
            {
                return seasons;
            }
            set
            {
                SetProperty<List<Season>>(ref seasons, value);
            }
        }
        public List<Championship> Championships
        {
            get
            {
                return championships;
            }
            set
            {
                SetProperty<List<Championship>>(ref championships, value);
            }
        }

        private Season season;
        public Season SelectedSeason
        {
            get
            {
                return season;
            }
            set
            {
                if (season != value)
                {
                    season = value;
                    FetchClubPlayersHistoric();
                    OnPropertyChanged(nameof(SelectedSeason));
                }
            }
        }
        private Championship championship;
        public Championship SelectedChampionship
        {
            get
            {
                return championship;
            }
            set
            {
                if (championship != value)
                {
                    championship = value;
                    FetchClubPlayersHistoric();
                    OnPropertyChanged(nameof(Championship));
                }
            }
        }

        public DelegateCommand ItemTappedCommand { get; private set; }
        void ItemTapped()
        {
            // Here you can do whatever you want, this will be executed when
            // user clicks on item in ListView, you will have a value of tapped
            // item in SlectedProfile property
            var page = new RankingPage();
            page.FillRanking(SelectedItem.Season, SelectedItem.Championship);
            Application.Current.MainPage.Navigation.PushAsync(page);
        }
        


        public Ranking SelectedItem { get; set; }
        private Club c;
        public Club Club
        {
            get
            { 
                return c; 
            }
            set
            {
                c = value;
                OnPropertyChanged(nameof(Club));
            }
        }


        public void DefineClub(Club c)
        {
            if (c == null)
                return;

            if (Club == null || Club.Id != c.Id)
            {
                Club = c;
                FetchClubPlayersHistoric();
            }
        }

        private void FetchClubPlayersHistoric()
        {
            ClubPlayersHistoric = new ObservableCollection<Ranking>(Database.Instance.rankings.Where(x => x.Club.Id == Club.Id && (championship!=null ? x.Championship.Id == championship.Id : x.Championship.Name.ToLower() != "todoscampeonatos") && (season!=null ?x.Season.Id == season.Id : x.Season.IsSingleSeason)).OrderByDescending(x=>x.Season.StartDate).ThenByDescending(x=>x.Championship.TournamentType.Weight));

            if(season==null)
                Seasons = clubPlayersHistoric.Where(x=>x.Season.IsSingleSeason).Select(x => x.Season).Distinct().OrderByDescending(x => x.StartDate).ToList();

            if (championship == null)
                Championships = clubPlayersHistoric.Select(x => x.Championship).Distinct().OrderByDescending(x => x.TournamentType.Weight).ToList();
        }

        private ObservableCollection<Ranking> clubPlayersHistoric = new ObservableCollection<Ranking>();
        private List<Season> seasons;
        private List<Championship> championships;

        public ObservableCollection<Ranking> ClubPlayersHistoric
        {
            get { return clubPlayersHistoric; }
            set { SetProperty(ref clubPlayersHistoric, value); }
        }
    }
}