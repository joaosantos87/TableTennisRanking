﻿using App1.Views;
using DatabaseNetStandard.Data;
using Prism.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace App1.ViewModels
{
    public class ChampionshipsViewModel : BaseViewModel
    {
        private bool isChampionship;
        public ChampionshipsViewModel()
        {
            ItemChampionshipCommand = new DelegateCommand<object>(ItemChampionshipClicked);

            ItemTappedCommand = new DelegateCommand(ItemTapped);
            SearchChampionshipCommand = new DelegateCommand(UpdateSearchChampionship);
            Search = "";
            UpdateSearchChampionship();
        }

        public bool SetIsYearlyChampionshipType
        {
            set
            {
                isChampionship = value;
                if(isChampionship)
                    Title = "Campeonatos";
                else
                    Title = "Torneios";

                UpdateSearchChampionship();
            }
        }

        public string Search { get; set; }

        public Championship Selected { get; set; }

        public DelegateCommand<object> ItemChampionshipCommand { get; private set; }
        public DelegateCommand SearchChampionshipCommand { get; private set; }
        public DelegateCommand ItemTappedCommand { get; private set; }

        private ObservableCollection<Championship> championships = new ObservableCollection<Championship>();

        public ObservableCollection<Championship> Championships
        {
            get { return championships; }
            set { SetProperty(ref championships, value); }
        }

        void ItemChampionshipClicked(object id)
        {
            var idInt = Convert.ToInt32(id);
            var SelectedChampionship = Database.Instance.championships.Single(x => x.Id == idInt);
            if(SelectedChampionship.TournamentType.IsChampionship)
                Application.Current.MainPage.Navigation.PushAsync(new ChampionshipTabbedPage(SelectedChampionship));
            else
                Application.Current.MainPage.Navigation.PushAsync(new TournamentTabbedPage(SelectedChampionship));
        }

        void ItemTapped()
        {
            // Here you can do whatever you want, this will be executed when
            // user clicks on item in ListView, you will have a value of tapped
            // item in SlectedProfile property
            //Application.Current.MainPage.Navigation.PushAsync(new PlayerDetailPage(SelectedPlayer));

            var SelectedChampionship = Selected;

            if (SelectedChampionship.TournamentType.IsChampionship)
                Application.Current.MainPage.Navigation.PushAsync(new ChampionshipTabbedPage(SelectedChampionship));
            else
                Application.Current.MainPage.Navigation.PushAsync(new TournamentTabbedPage(SelectedChampionship));
        }

        private void UpdateSearchChampionship()
        {
            Championships = ListChampionships(Search);
        }

        private ObservableCollection<Championship> ListChampionships(string input)
        {
            var champ = new List<Championship>();
            var champsWithGames = Database.Instance.games.Select(x => x.Championship.Id).ToList();
            champsWithGames.AddRange(Database.Instance.doubleGames.Select(x => x.Championship.Id));
            champsWithGames = champsWithGames.Distinct().ToList();

            //if (input == "")
            //{
            //    //var tg = Database.Instance.games.Where(x => x.Championship.TournamentType.IsChampionship == isChampionship && x.Championship.TournamentType.Weight > 0);
            //    champ = Database.Instance.championships.Where(x => x.TournamentType.IsChampionship == isChampionship && x.TournamentType.Weight > 0).ToList();
            //}
            //else
            //{
            //var tg = Database.Instance.games.Where(x => x.Championship.TournamentType.IsChampionship == isChampionship && x.Championship.Name.ToLower().Contains(input) || x.Championship.NameAutomaticInsertion.ToLower().Contains(input));
            champ = Database.Instance.championships.Where(x => champsWithGames.Contains(x.Id) &&  x.TournamentType.IsChampionship == isChampionship && x.TournamentType.Weight > 0 && (x.Name.ToLower().Contains(input) || x.NameAutomaticInsertion.ToLower().Contains(input))).ToList();
            //}
            if (isChampionship)
                champ = champ.OrderByDescending(x => x.TournamentType.Weight).ToList();
            else
                champ = champ.OrderByDescending(x => x.Date).ToList();

            return new ObservableCollection<Championship>(champ.Select(x => x).Distinct());
        }
    }
}