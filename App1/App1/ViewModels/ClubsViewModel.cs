﻿using App1.Views;
using DatabaseNetStandard.Data;
using Prism.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace App1.ViewModels
{
    public class ClubsViewModel : BaseViewModel
    {
        public ClubsViewModel()
        {
            Title = "Clubes";
            ItemClubCommand = new DelegateCommand<object>(ItemClubClicked);
            if (Associations.Count == 0)
                Associations = Database.Instance.associations;

            ItemTappedCommand = new DelegateCommand(ItemTapped);
            SearchClubCommand = new DelegateCommand(UpdateSearchClub);
            Search = "";
            UpdateSearchClub();
        }
        public string Search { get; set; }

        public Club Selected { get; set; }

        public DelegateCommand<object> ItemClubCommand { get; private set; }
        public DelegateCommand SearchClubCommand { get; private set; }
        public DelegateCommand ItemTappedCommand { get; private set; }

        private ObservableCollection<Club> clubs = new ObservableCollection<Club>();

        public ObservableCollection<Club> Clubs
        {
            get { return clubs; }
            set { SetProperty(ref clubs, value); }
        }

        void ItemClubClicked(object id)
        {
            var idInt = Convert.ToInt32(id);
            var SelectedClub = Database.Instance.clubs.Single(x => x.Id == idInt);
            // Here you can do whatever you want, this will be executed when
            // user clicks on item in ListView, you will have a value of tapped
            // item in SlectedProfile property
            //Application.Current.MainPage.Navigation.PushAsync(new PlayerDetailPage(SelectedPlayer));
            Application.Current.MainPage.Navigation.PushAsync(new ClubTabbedPage(SelectedClub));
        }
        void ItemTapped()
        {
            // Here you can do whatever you want, this will be executed when
            // user clicks on item in ListView, you will have a value of tapped
            // item in SlectedProfile property
            //Application.Current.MainPage.Navigation.PushAsync(new PlayerDetailPage(SelectedPlayer));
            Application.Current.MainPage.Navigation.PushAsync(new ClubTabbedPage(Selected));
        }

        private List<Association> associations = new List<Association>();
        public List<Association> Associations
        {
            get
            {
                return associations;
            }
            set
            {
                associations = value;
                OnPropertyChanged(nameof(Associations));
            }
        }
        private Association selectedAssociation;
        public Association SelectedAssociation
        {
            get { return selectedAssociation; }
            set
            {
                if (selectedAssociation == value)
                    return;

                if (value == null)
                    return;

                selectedAssociation = value;
                OnPropertyChanged(nameof(SelectedAssociation));
                UpdateAssocicationClubs();
            }
        }
        private void UpdateAssocicationClubs()
        {
            Clubs = new ObservableCollection<Club>(Database.Instance.clubs.Where(x => x.Association.Id == SelectedAssociation.Id));
        }

        private void UpdateSearchClub()
        {
            Clubs = SearchName(Search);
        }

        private ObservableCollection<Club> SearchName(string input)
        {
            if (input == "")
            {
                return new ObservableCollection<Club>(Database.Instance.clubs);
            }
            else
            {
                return new ObservableCollection<Club>(Database.Instance.clubs.Where(x => x.Name.ToLower().Contains(input.ToLower()) || x.NameAutomaticInsertion.ToLower().Contains(input.ToLower()) || x.Association.Name.Contains(input.ToLower()) || x.Id.ToString().Equals(input)));
            }
        }
    }
}