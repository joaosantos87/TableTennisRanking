﻿using App1.Views;
using DatabaseNetStandard.Data;
using Prism.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace App1.ViewModels
{
    public class PlayerVsPlayerBaseViewModel : BaseViewModel
    {
        public PlayerVsPlayerBaseViewModel()
        {
            Title = "VS ...";
            //Jogadores = new ObservableCollection<Player>(Database.Instance.players.OrderByDescending(x=>x.CurrentPoints));
            ItemTappedCommand = new DelegateCommand(ItemTapped);
            SearchPlayerCommand = new DelegateCommand(UpdateSearchPlayer);
            currentPlayers = Database.Instance.players.Where(x => x.Name.ToLower() != "bye").OrderByDescending(x => x.CurrentPoints).ToList();
        }

        public DelegateCommand ItemTappedCommand { get; private set; }

        public DelegateCommand SearchPlayerCommand { get; private set; }

        private ObservableCollection<VsPlayer> jogadores = new ObservableCollection<VsPlayer>();

        public ObservableCollection<VsPlayer> Jogadores
        {
            get { return jogadores; }
            set { SetProperty(ref jogadores, value); }
        }

        public Player SelectedPlayer { get; set; }
        private string search = "";
        public string Search
        {
            get { return search; }
            set { SetProperty(ref search, value); }
        }

        // Method which will be executed using our command
        void ItemTapped()
        {
            // Here you can do whatever you want, this will be executed when
            // user clicks on item in ListView, you will have a value of tapped
            // item in SlectedProfile property
            //Application.Current.MainPage.Navigation.PushAsync(new PlayerDetailPage(SelectedPlayer));

            var page = new PlayerVsPlayerPage();
            page.SetPlayers(Player, SelectedPlayer);

            Application.Current.MainPage.Navigation.PushAsync(page);
        }

        private void UpdateSearchPlayer()
        {
            Jogadores = SearchName(Search);
        }
        List<Game> games = new List<Game>();
        List<Player> currentPlayers = new List<Player>();
        private ObservableCollection<VsPlayer> SearchName(string input)
        {
            if(input!="")
                currentPlayers = currentPlayers.Where(x => x.Name.ToLower().Contains(input)).ToList();

            List<VsPlayer> vsPlayers = new List<VsPlayer>();

            foreach(var cp in currentPlayers)
            {
                var vsGames = games.Where(x => x.OppId == cp.Id).ToList();
                if (vsGames.Count == 0)
                    continue;

                var curPlayerWon = vsGames.Count(x => x.IdPlayerWinner == Player.Id);
                var curPlayerLost = vsGames.Count - curPlayerWon;

                vsPlayers.Add(
                    new VsPlayer(cp)
                    {
                        VsGamesWon = curPlayerWon,
                        VsGamesLost = curPlayerLost
                    }
                );
            }

            return new ObservableCollection<VsPlayer>(vsPlayers);
        }

        Player p;
        public Player Player
        {
            get
            {
                return p;
            }
            set
            {
                p = value;
                OnPropertyChanged(nameof(Player));
            }
        }

        public void DefinePlayer(Player p)
        {
            if (p == null)
                return;

            if (Player == null || Player.Id != p.Id)
            {
                Player = p;
            }
            games = Database.Instance.games.Where(x => x.PlayerA?.Id == Player.Id || x.PlayerB?.Id == Player.Id).ToList();
            games.ForEach(x => x.SetCurrentPlayer = Player.Id);
            UpdateSearchPlayer();
        }
    }
}