﻿using DatabaseNetStandard.Data;
using Prism.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;
using System.Linq;
using System.Globalization;
using System.ComponentModel;
using App1.Views;

namespace App1.ViewModels
{
    public class PlayerHistoricViewModel : BaseViewModel
    {
        public PlayerHistoricViewModel()
        {
            ItemTappedCommand = new DelegateCommand(ItemTapped);
            ItemChampionshipCommand = new DelegateCommand<object>(ItemChampionshipClicked);
            ItemClubCommand = new DelegateCommand<object>(ItemClubClicked);
            ItemRankCommand = new DelegateCommand<object>(ItemRankClicked);
        }

        public DelegateCommand ItemTappedCommand { get; private set; }
        public DelegateCommand<object> ItemChampionshipCommand { get; private set; }
        public DelegateCommand<object> ItemClubCommand { get; private set; }
        public DelegateCommand<object> ItemRankCommand { get; private set; }
        void ItemTapped()
        {
            // Here you can do whatever you want, this will be executed when
            // user clicks on item in ListView, you will have a value of tapped
            // item in SlectedProfile property
            var page = new RankingPage();
            page.FillRanking(SelectedItem.Season, SelectedItem.Championship);
            Application.Current.MainPage.Navigation.PushAsync(page);
        }
        void ItemRankClicked(object id_object)
        {
            int id = Convert.ToInt32(id_object);
            var rank = Database.Instance.rankings.SingleOrDefault(x => x.Id == id);
            if (rank == null)
                return;
            var rp = new RankingPage();
            rp.FillRanking(rank.Season, rank.Championship);
            Application.Current.MainPage.Navigation.PushAsync(rp);
        }
        void ItemChampionshipClicked(object id_object)
        {
            int id = Convert.ToInt32(id_object);
            var champ = Database.Instance.championships.SingleOrDefault(x => x.Id == id);
            if (champ == null)
                return;
            Application.Current.MainPage.Navigation.PushAsync(new ChampionshipTabbedPage(champ));
        }
        void ItemClubClicked(object id_object)
        {
            int id = Convert.ToInt32(id_object);
            var c = Database.Instance.clubs.SingleOrDefault(x => x.Id == id);
            if (c == null)
                return;
            Application.Current.MainPage.Navigation.PushAsync(new ClubTabbedPage(c));
        }
        public Ranking SelectedItem { get; set; }
        Player p;
        public Player Player 
        {
            get
            { 
                return p; 
            }
            set
            {
                p = value;
                OnPropertyChanged(nameof(Player));
            }
        }


        public void DefinePlayer(Player p)
        {
            if (p == null)
                return;

            if (Player == null || Player.Id != p.Id)
            {
                Player = p;
                FetchPlayerHistoric();
            }
        }

        private void FetchPlayerHistoric()
        {
            PlayerHistoric = new ObservableCollection<Ranking>(Database.Instance.rankings.Where(x => x.Player.Id == Player.Id && x.Season.IsSingleSeason).OrderByDescending(x=>x.Season.StartDate));
        }

        private ObservableCollection<Ranking> playerHistoric = new ObservableCollection<Ranking>();

        public ObservableCollection<Ranking> PlayerHistoric
        {
            get { return playerHistoric; }
            set { SetProperty(ref playerHistoric, value); }
        }
    }
}