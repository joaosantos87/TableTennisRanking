﻿using DatabaseNetStandard.Data;
using Prism.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;
using System.Linq;
using System.Globalization;
using System.ComponentModel;
using App1.Views;

namespace App1.ViewModels
{
    public class TournamentEliminatoryViewModel : BaseViewModel
    {
        public TournamentEliminatoryViewModel()
        {
            ItemPlayerCommand = new DelegateCommand<object>(ItemPlayerClicked);
        }

        public DelegateCommand<object> ItemPlayerCommand { get; private set; }

        void ItemPlayerClicked(object id_object)
        {
            int id = Convert.ToInt32(id_object);
            var p = Database.Instance.players.SingleOrDefault(x => x.Id == id);
            if (p == null)
                return;
            Application.Current.MainPage.Navigation.PushAsync(new PlayerTabbedPage(p));
        }

        Championship c;
        public Championship Championship
        {
            get
            { 
                return c; 
            }
            set
            {
                c = value;
                OnPropertyChanged(nameof(Championship));
            }
        }

        public void DefineChampionship(Championship c)
        {
            if (c == null)
                return;

            Championship = c;
            Title = Championship.NameAutomaticInsertion != "" ? Championship.NameAutomaticInsertion : Championship.Name;
            GetChampionshipResults();
        }

        private List<Phase> phases = new List<Phase>();
        public List<Phase> Phases
        {
            get
            {
                return phases;
            }
            set
            {
                phases = value;
                OnPropertyChanged(nameof(Phases));
            }
        }
        private Phase selectedPhase { get; set; }
        public Phase SelectedPhase
        {
            get { return selectedPhase; }
            set
            {
                if (selectedPhase == value)
                    return;

                //if (value == null)
                //    return;

                selectedPhase = value;
                OnPropertyChanged(nameof(SelectedPhase));
                GetChampionshipResults();
            }
        }

        private List<Season> seasons = new List<Season>();
        public List<Season> Seasons
        {
            get
            {
                return seasons;
            }
            set
            {
                seasons = value;
                OnPropertyChanged(nameof(Seasons));
            }
        }
        private Season selectedSeason { get; set; }
        public Season SelectedSeason
        {
            get { return selectedSeason; }
            set
            {
                if (selectedSeason == value)
                    return;

                //if (value == null)
                //    return;

                selectedSeason = value;
                OnPropertyChanged(nameof(SelectedSeason));
                GetChampionshipResults();
            }
        }

        private void GetChampionshipResults()
        {
            var groupStagePhase = Database.Instance.phases.SingleOrDefault(x => x.Name.ToLower().Contains("grupo"));

            var g = Database.Instance.games.Where(x => x.Championship.Id == Championship.Id && (SelectedSeason != null ? x?.Season?.Id == SelectedSeason.Id : true) && x.Phase.Id != groupStagePhase.Id && (SelectedPhase != null ? x?.Phase?.Id == SelectedPhase.Id : true)).OrderByDescending(x => x?.Phase?.NumGames).ToList();

            if (Seasons.Count == 0)
                Seasons = Database.Instance.seasons.OrderByDescending(x => x.StartDate).ToList();


            Games = new ObservableCollection<Game>(g);

            if (SelectedSeason == null)
            {
                Seasons = games.Select(x => x.Season).Distinct().OrderByDescending(x => x?.StartDate).ToList();
                if(seasons.Count > 0)
                    SelectedSeason = seasons.First();
            }

            if (SelectedPhase == null)
            {
                Phases = g.Where(x => x.Phase.Id != groupStagePhase.Id).Select(x => x.Phase).Distinct().OrderByDescending(x => x.NumGames).ToList();
            }
        }

        private ObservableCollection<Game> games = new ObservableCollection<Game>();

        public ObservableCollection<Game> Games
        {
            get { return games; }
            set { SetProperty(ref games, value); }
        }

    }
}