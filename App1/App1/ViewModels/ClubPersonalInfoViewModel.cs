﻿using DatabaseNetStandard.Data;
using Prism.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;
using System.Linq;
using System.Globalization;
using System.ComponentModel;
using App1.Views;

namespace App1.ViewModels
{
    public class ClubPersonalInfoViewModel : BaseViewModel
    {
        public ClubPersonalInfoViewModel()
        {

        }

        Club c;
        public Club Club
        {
            get
            { 
                return c; 
            }
            set
            {
                c = value;
                OnPropertyChanged(nameof(Club));
            }
        }

        public void DefineClub(Club c)
        {
            if (c == null)
                return;

            if (Club == null || Club.Id != c.Id)
            {
                Club = c;
            }
            
            Title = Club.NameAutomaticInsertion != "" ? Club.NameAutomaticInsertion : Club.Name;
            FillResults();
        }

        public DelegateCommand<object> ItemVSCommand { get; private set; }
        void ItemVsClicked(object oppId)
        {
            int id = Convert.ToInt32(oppId);
            var oppClub = Database.Instance.clubs.SingleOrDefault(x => x.Id == id);
            if (oppClub == null)
                return;
            var page = new ClubVsClubPage();
            page.SetClubs(Club, oppClub);
            Application.Current.MainPage.Navigation.PushAsync(page);
        }

        private void FillResults()
        {
            var teamGames = Database.Instance.teamGames.Where(x => x.ClubA.Id == c.Id || x.ClubB.Id == c.Id).ToList();
            var teamGamesIds = teamGames.Select(x => x.Id);

            var games = Database.Instance.games.Where(x => x.TeamGame!=null && teamGamesIds.Contains(x.TeamGame.Id)).ToList();

            games.ForEach(x => x.SetCurrentClub = Club.Id);
            var allGames = games.Count * 1.0f;

            games = games.OrderByDescending(x => x.PointsCurrentPlayer).ToList();

            int numGames = 3;
            if (games.Count < numGames)
                numGames = games.Count;

            var all = games.Take(numGames).ToList();
            all.AddRange(games.Skip(games.Count - numGames).Take(numGames).Reverse());

            Results = new ObservableCollection<Game>(all);
        }
        
        private ObservableCollection<Game> results = new ObservableCollection<Game>();

        public ObservableCollection<Game> Results
        {
            get { return results; }
            set { SetProperty(ref results, value); }
        }

    }
}