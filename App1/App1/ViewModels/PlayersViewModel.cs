﻿using App1.Views;
using DatabaseNetStandard.Data;
using Prism.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace App1.ViewModels
{
    public class PlayersViewModel : BaseViewModel
    {
        public PlayersViewModel()
        {
            Title = "Jogadores";
            Jogadores = new ObservableCollection<Player>(Database.Instance.players.OrderByDescending(x=>x.CurrentPoints));
            ItemTappedCommand = new DelegateCommand(ItemTapped);
            SearchPlayerCommand = new DelegateCommand(UpdateSearchPlayer);
        }

        public DelegateCommand ItemTappedCommand { get; private set; }

        public DelegateCommand SearchPlayerCommand { get; private set; }

        private ObservableCollection<Player> jogadores = new ObservableCollection<Player>();

        public ObservableCollection<Player> Jogadores
        {
            get { return jogadores; }
            set { SetProperty(ref jogadores, value); }
        }

        public Player SelectedPlayer { get; set; }
        public string Search { get; set; }

        // Method which will be executed using our command
        void ItemTapped()
        {
            // Here you can do whatever you want, this will be executed when
            // user clicks on item in ListView, you will have a value of tapped
            // item in SlectedProfile property
            //Application.Current.MainPage.Navigation.PushAsync(new PlayerDetailPage(SelectedPlayer));
            Application.Current.MainPage.Navigation.PushAsync(new PlayerTabbedPage(SelectedPlayer));
        }

        private void UpdateSearchPlayer()
        {
            Jogadores = SearchName(Search);
        }

        private ObservableCollection<Player> SearchName(string input)
        {
            return new ObservableCollection<Player>(Database.Instance.players.Where(x => x.Name.ToLower() != "bye" && x.Name.ToLower().Contains(input)));
        }

    }
}