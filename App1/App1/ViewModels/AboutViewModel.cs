﻿using DatabaseNetStandard.Data;
using System;
using System.Linq;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace App1.ViewModels
{
    public class AboutViewModel : BaseViewModel
    {
        public AboutViewModel()
        {
            Title = "Tenis de Mesa Base de Dados";
            var totalIndividualGames = Database.Instance.games.Count;
            var totalDoubleGames = Database.Instance.doubleGames.Count;
            var teamGames = Database.Instance.teamGames.Count;
            var championships = Database.Instance.championships.Where(x => x.TournamentType.IsChampionship);
            var tournaments = Database.Instance.championships.Where(x => !x.TournamentType.IsChampionship).OrderBy(x=>x.Date);
            string champ = "";
            string tourn = "";

            foreach(var c in championships)
            {
                var games = Database.Instance.teamGames.Where(x => x.Championship.Id == c.Id).ToList();
                if (games.Count > 0)
                {
                    champ += $"{c.DisplayNameAutomatic_Name}: {Environment.NewLine}" +
                        $"- Id: {c.Id}{Environment.NewLine}" +
                        $"- Jogos: {games.Count}{Environment.NewLine}{Environment.NewLine}";
                }
            }
            foreach (var c in tournaments)
            {
                var games = Database.Instance.games.Where(x => x.Championship.Id == c.Id).ToList();
                tourn += $"{c.DisplayNameAutomatic_Name}: {Environment.NewLine}" +
                    $"- Id: {c.Id}{Environment.NewLine}" +
                    $"- Data: {c.DateFormated}{Environment.NewLine}" +
                    $"- Jogos: {games.Count}{Environment.NewLine}{Environment.NewLine}";
            }

            ApplicationSummary = $"" +
                $"Total jogos: {totalIndividualGames + totalDoubleGames}{Environment.NewLine}" +
                $"- Individuais: {totalIndividualGames} {Environment.NewLine}" +
                $"- Pares: {totalDoubleGames}{Environment.NewLine}{Environment.NewLine}" +
                $"Campeonatos: { Environment.NewLine}" +
                $"- {champ}{ Environment.NewLine}" +
                $"Torneios: {Environment.NewLine}" +
                $"- {tourn}";
        }

        public ICommand OpenWebCommand { get; }

        private string applicationSummary = "";
        public string ApplicationSummary
        { 
            get { return applicationSummary; }
            set { SetProperty<string>(ref applicationSummary, value); }
        }
    }
}