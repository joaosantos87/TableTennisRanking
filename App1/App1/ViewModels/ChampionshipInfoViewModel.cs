﻿using DatabaseNetStandard.Data;
using Prism.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;
using System.Linq;
using System.Globalization;
using System.ComponentModel;
using App1.Views;

namespace App1.ViewModels
{
    public class ChampionshipInfoViewModel : BaseViewModel
    {
        public ChampionshipInfoViewModel()
        {
            ItemResultCommand = new DelegateCommand<object>(ItemResultClicked);
            ItemPlayerCommand = new DelegateCommand<object>(ItemPlayerClicked);
        }

        public DelegateCommand<object> ItemResultCommand { get; private set; }
        public DelegateCommand<object> ItemPlayerCommand { get; private set; }

        void ItemResultClicked(object teamGameId)
        {
            int id = Convert.ToInt32(teamGameId);
            var tg = Database.Instance.teamGames.SingleOrDefault(x => x.Id == id);
            if (tg == null)
                return;
            var page = new TeamGameResultPage();
            page.SetTeamGame(tg);
            Application.Current.MainPage.Navigation.PushAsync(page);
        }

        void ItemPlayerClicked(object id_object)
        {
            int id = Convert.ToInt32(id_object);
            var p = Database.Instance.players.SingleOrDefault(x => x.Id == id);
            if (p == null)
                return;
            Application.Current.MainPage.Navigation.PushAsync(new PlayerTabbedPage(p));
        }


        private int numberGames = 0;
        public int NumberGames
        {
            get { return numberGames; }
            set { SetProperty<int>(ref numberGames, value); }
        }
        private int numberPlayers = 0;
        public int NumberPlayers
        {
            get { return numberPlayers; }
            set { SetProperty<int>(ref numberPlayers, value); }
        }

        Championship c;
        public Championship Championship
        {
            get
            { 
                return c; 
            }
            set
            {
                c = value;
                OnPropertyChanged(nameof(Championship));
            }
        }
        private List<Season> seasons = new List<Season>();
        public List<Season> Seasons
        {
            get
            {
                return seasons;
            }
            set
            {
                seasons = value;
                OnPropertyChanged(nameof(Seasons));
            }
        }
        private Season selectedSeason { get; set; }
        public Season SelectedSeason
        {
            get { return selectedSeason; }
            set
            {
                if (selectedSeason == value)
                    return;

                if (value == null)
                    return;

                selectedSeason = value;
                OnPropertyChanged(nameof(SelectedSeason));
                GetChampionshipResults();
            }
        }

        public void DefineChampionship(Championship c)
        {
            if (c == null)
                return;

            Championship = c;
            
            Title = Championship.NameAutomaticInsertion != "" ? Championship.NameAutomaticInsertion : Championship.Name;
            GetChampionshipResults();
            SelectedSeason = seasons.First();
        }

        public DelegateCommand<object> ItemVSCommand { get; private set; }

        void ItemVsClicked(object oppId)
        {
            //int id = Convert.ToInt32(oppId);
            //var oppClub = Database.Instance.clubs.SingleOrDefault(x => x.Id == id);
            //if (oppClub == null)
            //    return;
            //var page = new ClubVsClubPage();
            //page.SetClubs(Club, oppClub);
            //Application.Current.MainPage.Navigation.PushAsync(page);
        }

        private void GetChampionshipResults()
        {
            //if (Seasons.Count == 0)
            //    Seasons = Database.Instance.seasons.OrderByDescending(x => x.StartDate).ToList();

            //var games = Database.Instance.games.Where(x => x.Championship.Id == Championship.Id && (SelectedSeason != null ? SelectedSeason.StartDate < x.Date && x.Date < SelectedSeason.EndDate : true)).OrderByDescending(x => x.Date).ToList();

            var games = new List<GameBase>();
            games = Database.Instance.games.Where(x => x.Championship.Id == Championship.Id && (SelectedSeason != null ? x?.Season?.Id == SelectedSeason.Id : true)).Select(x => (GameBase)x).OrderByDescending(x => x.Date).ToList();

            games.AddRange(Database.Instance.doubleGames.Where(x => x.Championship.Id == Championship.Id && (SelectedSeason != null ? x?.Season?.Id == SelectedSeason.Id : true)).Select(x => (GameBase)x).OrderByDescending(x => x.Date));

            var data = games.Select(a => (a is DoubleGame) ? (a as DoubleGame).DoubleANamePlayerA : (a as Game).PlayerAName).Distinct().ToList();
            data.AddRange(games.Select(a => (a is DoubleGame) ? (a as DoubleGame).DoubleANamePlayerB : (a as Game).PlayerAName).Distinct());
            data.AddRange(games.Select(a => (a is DoubleGame) ? (a as DoubleGame).DoubleBNamePlayerA : (a as Game).PlayerBName).Distinct());
            data.AddRange(games.Select(a => (a is DoubleGame) ? (a as DoubleGame).DoubleBNamePlayerB : (a as Game).PlayerBName).Distinct());
            data = data.Distinct().ToList();

            if (SelectedSeason == null)
            {
                Seasons = games.Select(x => x.Season).Distinct().OrderByDescending(x => x?.StartDate).ToList();
            }

            //var data = games.Select(a => a..PlayerAName).Distinct().ToList();
            //data.AddRange(games.Select(a => a.PlayerBName).Distinct());
            //data = data.Distinct().ToList();

            NumberGames = games.Count;
            NumberPlayers = data.Count;

            var tournamentDate = games.First().Date;
            //This needs to be like this to fetch the ranking immediatetly before the tournament occours
            var usefulDate = new DateTime(tournamentDate.Year - 1, tournamentDate.Month, tournamentDate.Day);

            var season = Database.Instance.seasons.SingleOrDefault(x => x.StartDate < usefulDate && x.EndDate > usefulDate && x.IsSingleSeason);

            var runningSeason = Database.Instance.seasons.SingleOrDefault(x => x.EndDate == season?.EndDate && !x.IsSingleSeason);

            Players = new ObservableCollection<Ranking>(Database.Instance.rankings.Where(x => data.Contains(x.Player.Name) && x.Season.Id == runningSeason?.Id && x.Championship.Name.ToLower() == "todoscampeonatos").OrderByDescending(x => x.Points));

            if (season != null)
            {
                var currentSeasonResults = Database.Instance.rankings.Where(x => x.Season.Id == SelectedSeason?.Id && x.Championship.Id == Championship.Id);
                var currentSeasonGames = games;
                if (SelectedSeason != null)
                {
                    currentSeasonGames = games.Where(x => x.Season == SelectedSeason).ToList();
                }

                foreach (var p in Players)
                {
                    var a = currentSeasonResults.Where(x => x.Player.Id == p.Player.Id);
                    foreach(var b in a)
                    {
                        
                    }
                    var cp = currentSeasonResults.SingleOrDefault(x => x.Player.Id == p.Player.Id);

                    //var playerGames = games.Where(x => x.PlayerA.Id == p.Player.Id || x.PlayerB.Id == p.Player.Id).ToList();
                    //playerGames.ForEach(x => x.SetCurrentPlayer = p.Player.Id);
                    //var curTournamentRes = playerGames.Sum(x => x.PointsCurrentPlayer);

                    if (cp == null)
                    {
                        p.UpdateGames(-1, -1);
                        p.Difference = 0;
                    }
                    else
                    {
                        var g = currentSeasonGames.Where(x => (x is Game) ? (x as Game).IdPlayerA == p.Player.Id || (x as Game).IdPlayerB == p.Player.Id : (x as DoubleGame).DoubleAIdPlayerA == p.Player.Id || (x as DoubleGame).DoubleAIdPlayerB == p.Player.Id || (x as DoubleGame).DoubleBIdPlayerA == p.Player.Id || (x as DoubleGame).DoubleBIdPlayerB == p.Player.Id).ToList();
                        g.ForEach(x => x.SetCurrentPlayer = p.Player.Id);

                        p.UpdateGames(cp.NumWins, cp.NumLoses);
                        p.Difference = g.Sum(x=>x.PointsCurrentPlayer);
                    }

                }
                Players = new ObservableCollection<Ranking>(Players.OrderByDescending(x => x.Points).ThenBy(x => x.Rank));
            }
        }
        
        private ObservableCollection<Ranking> players = new ObservableCollection<Ranking>();

        public ObservableCollection<Ranking> Players
        {
            get { return players; }
            set { SetProperty(ref players, value); }
        }

    }
}