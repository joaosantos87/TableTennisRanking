﻿using DatabaseNetStandard.Data;
using Prism.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;
using System.Linq;
using System.Globalization;
using System.ComponentModel;
using App1.Views;

namespace App1.ViewModels
{
    public class TeamGameResultViewModel : BaseViewModel
    {
        public TeamGameResultViewModel()
        {
            ItemChampionshipCommand = new DelegateCommand(ItemChampionshipClicked);
            ItemPlayerCommand = new DelegateCommand<object>(ItemPlayerClicked);
            ItemDoublePlayerCommand = new DelegateCommand<object>(ItemDoublePlayerClicked);
            ItemTappedCommand = new DelegateCommand(ItemTapped);
        }
        private int _height;
        public int Height
        {
            get { return _height; }
            set
            {
                _height = value;
                OnPropertyChanged("Height");
            }
        }
        public DelegateCommand ItemTappedCommand { get; private set; }
        public Game SelectedItem { get; set; }
        void ItemTapped()
        {
            // Here you can do whatever you want, this will be executed when
            // user clicks on item in ListView, you will have a value of tapped
            // item in SlectedProfile property
            var page = new RankingPage();
            var season = Database.Instance.seasons.SingleOrDefault(x => SelectedItem.Date > x.StartDate && SelectedItem.Date < x.EndDate && x.Name.ToLower() != "todasepocas");
            if (season == null)
                season = Database.Instance.seasons.SingleOrDefault(x =>  x.Name.ToLower() == "todasepocas");
            page.FillRanking(season, SelectedItem.Championship);
            Application.Current.MainPage.Navigation.PushAsync(page);
        }

        Club c1;
        public Club Club1
        {
            get
            { 
                return c1; 
            }
            set
            {
                c1 = value;
                OnPropertyChanged(nameof(Club1));
            }
        }
        Club c2;
        public Club Club2
        {
            get
            {
                return c2;
            }
            set
            {
                c2 = value;
                OnPropertyChanged(nameof(Club2));
            }
        }


        void ItemChampionshipClicked()
        {

        }


        void ItemPlayerClicked(object id_object)
        {
            int id = Convert.ToInt32(id_object);
            var p = Database.Instance.players.SingleOrDefault(x => x.Id == id);
            if (p == null)
                return;
            Application.Current.MainPage.Navigation.PushAsync(new PlayerTabbedPage(p));
        }

        void ItemDoublePlayerClicked(object id_object)
        {
            return;
            
            int id = Convert.ToInt32(id_object);
            var p = Database.Instance.players.SingleOrDefault(x => x.Id == id);
            if (p == null)
                return;
            Application.Current.MainPage.Navigation.PushAsync(new PlayerTabbedPage(p));
        }

        public DelegateCommand ItemChampionshipCommand { get; private set; }
        public DelegateCommand<object> ItemPlayerCommand { get; private set; }
        public DelegateCommand<object> ItemDoublePlayerCommand { get; private set; }

        public void SetTeamGame(TeamGame tg)
        {
            Tg = tg;

            Club1 = tg.ClubA;
            Club2 = tg.ClubB;

            Club1_Name = Club1 == null ? "N/A" : tg.ClubADisplayName;
            Club2_Name = Club2 == null ? "N/A" : tg.ClubBDisplayName;

            Title = $"{Club1_Name} VS {Club2_Name}";
            FillResults(tg);
        }

        private TeamGame tg;
        public TeamGame Tg
        {
            get { return tg; }
            set { SetProperty<TeamGame>(ref tg, value); }
        }
        private string club1_name = "";
        private string club2_name = "";
        public string Club1_Name
        {
            get { return club1_name; }
            set 
            {
                SetProperty<string>(ref club1_name, value);
            }
        }
        public string Club2_Name
        {
            get { return club2_name; }
            set
            {
                SetProperty<string>(ref club2_name, value);
            }
        }

        private void FillResults(TeamGame tg)
        {
            var singleGames = Database.Instance.games.Where(x => x.TeamGame?.Id == tg.Id).ToList();
            var doubleGames = Database.Instance.doubleGames.Where(x => x.TeamGame?.Id == tg.Id).ToList();



            SinglesResults = new ObservableCollection<Game>(singleGames.OrderByDescending(x => x.Date));

            DoublesResults = new ObservableCollection<DoubleGame>(doubleGames.OrderByDescending(x => x.Date));

            Height = (DoublesResults.Count * 60) + 20;

        }

        private ObservableCollection<Game> singlesResults = new ObservableCollection<Game>();
        private ObservableCollection<DoubleGame> doublesResults = new ObservableCollection<DoubleGame>();

        public ObservableCollection<Game> SinglesResults
        {
            get { return singlesResults; }
            set { SetProperty(ref singlesResults, value); }
        }

        public ObservableCollection<DoubleGame> DoublesResults
        {
            get { return doublesResults; }
            set { SetProperty(ref doublesResults, value); }
        }

    }
}