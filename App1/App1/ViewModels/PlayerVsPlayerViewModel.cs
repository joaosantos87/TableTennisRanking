﻿using DatabaseNetStandard.Data;
using Prism.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;
using System.Linq;
using System.Globalization;
using System.ComponentModel;
using App1.Views;

namespace App1.ViewModels
{
    public class PlayerVsPlayerViewModel : BaseViewModel
    {
        public PlayerVsPlayerViewModel()
        {
            ItemChampionshipCommand = new DelegateCommand(ItemChampionshipClicked);
            ItemPlayerCommand = new DelegateCommand<object>(ItemPlayerClicked);
            ItemTappedCommand = new DelegateCommand(ItemTapped);
        }

        public DelegateCommand ItemTappedCommand { get; private set; }
        public Game SelectedItem { get; set; }
        void ItemTapped()
        {
            // Here you can do whatever you want, this will be executed when
            // user clicks on item in ListView, you will have a value of tapped
            // item in SlectedProfile property
            var page = new RankingPage();
            var season = Database.Instance.seasons.SingleOrDefault(x => SelectedItem.Date > x.StartDate && SelectedItem.Date < x.EndDate && x.IsSingleSeason);
            if (season == null)
                season = Database.Instance.seasons.SingleOrDefault(x =>  x.Name.ToLower() == "todasepocas");
            page.FillRanking(season, SelectedItem.Championship);
            Application.Current.MainPage.Navigation.PushAsync(page);
        }

        Player p1;
        public Player Player1
        {
            get
            { 
                return p1; 
            }
            set
            {
                p1 = value;
                OnPropertyChanged(nameof(Player1));
            }
        }
        Player p2;
        public Player Player2
        {
            get
            {
                return p2;
            }
            set
            {
                p2 = value;
                OnPropertyChanged(nameof(Player2));
            }
        }


        void ItemChampionshipClicked()
        {

        }


        void ItemPlayerClicked(object id_object)
        {
            int id = Convert.ToInt32(id_object);
            var p = Database.Instance.players.SingleOrDefault(x => x.Id == id);
            if (p == null)
                return;
            Application.Current.MainPage.Navigation.PushAsync(new PlayerTabbedPage(p));
        }


        public DelegateCommand ItemChampionshipCommand { get; private set; }
        public DelegateCommand<object> ItemPlayerCommand { get; private set; }

        public void DefinePlayers(Player p1, Player p2)
        {
            if (p1 == null || p2 == null)
                return;

            if (Player1 == null || Player2==null || Player1.Id != p1.Id || Player2.Id != p2.Id)
            {
                Player1 = p1;
                Player2 = p2;
            }

            var p1Name = Player1.NameAutomaticInsertion != "" ? Player1.NameAutomaticInsertion : Player1.Name;
            var p2Name = Player2.NameAutomaticInsertion != "" ? Player2.NameAutomaticInsertion : Player2.Name;
            Title = $"{p1Name} VS {p2Name}";
            FillResults();
        }

        private void FillResults()
        {
            var games = Database.Instance.games.Where(x => (x.IdPlayerA == p1.Id && x.IdPlayerB == p2.Id) || (x.IdPlayerB == p1.Id && x.IdPlayerA == p2.Id)).ToList();


            //we set player1 as the defined player
            games.ForEach(x => x.SetCurrentPlayer = Player1.Id);

            var allGames = games.Count * 1.0f;
            var gamesWonThreeZero = games.Count(x => x.SetsCurrentPlayer == 3 && x.SetsOppPlayer == 0);
            NumGamesWonThreeZeroText = $"{gamesWonThreeZero} ({((gamesWonThreeZero / allGames)).ToString("0 %")})";

            var gamesWonThreeOne = games.Count(x => x.SetsCurrentPlayer == 3 && x.SetsOppPlayer == 1);
            NumGamesWonThreeOneText = $"{gamesWonThreeOne} ({((gamesWonThreeOne / allGames)).ToString("0 %")})";

            var gamesWonThreeTwo = games.Count(x => x.SetsCurrentPlayer == 3 && x.SetsOppPlayer == 2);
            NumGamesWonThreeTwoText = $"{gamesWonThreeTwo} ({((gamesWonThreeTwo / allGames)).ToString("0 %")})";



            var gamesLostThreeZero = games.Count(x => x.SetsCurrentPlayer == 0 && x.SetsOppPlayer == 3);
            NumGamesLostThreeZeroText = $"{gamesLostThreeZero} ({((gamesLostThreeZero / allGames)).ToString("0 %")})";

            var gamesLostThreeOne = games.Count(x => x.SetsCurrentPlayer == 1 && x.SetsOppPlayer == 3);
            NumGamesLostThreeOneText = $"{gamesLostThreeOne} ({((gamesLostThreeOne / allGames)).ToString("0 %")})";


            var gamesLostThreeTwo = games.Count(x => x.SetsCurrentPlayer == 2 && x.SetsOppPlayer == 3);
            NumGamesLostThreeTwoText = $"{gamesLostThreeTwo} ({((gamesLostThreeTwo / allGames)).ToString("0 %")})";

            var gamesWon = games.Count(x => x.IdPlayerWinner == p1.Id);
            NumGamesWon = $"{gamesWon} ({(gamesWon / allGames).ToString("0%")})";

            var gamesLost = games.Count(x => x.IdPlayerWinner == p2.Id);
            NumGamesLost = $"{gamesLost} ({(gamesLost / allGames).ToString("0%")})";

            Results = new ObservableCollection<Game>(games.OrderByDescending(x => x.Date));
        }
        private string numGamesWonThreeZeroText;
        public string NumGamesWonThreeZeroText
        {
            get { return numGamesWonThreeZeroText; }
            set { SetProperty<string>(ref numGamesWonThreeZeroText, value, nameof(NumGamesWonThreeZeroText)); }
        }

        private string numGamesWon;
        public string NumGamesWon
        {
            get { return numGamesWon; }
            set { SetProperty<string>(ref numGamesWon, value, nameof(NumGamesWon)); }
        }
        private string numGamesLost;
        public string NumGamesLost
        {
            get { return numGamesLost; }
            set { SetProperty<string>(ref numGamesLost, value, nameof(NumGamesLost)); }
        }

        private string numGamesWonThreeOneText;
        public string NumGamesWonThreeOneText
        {
            get { return numGamesWonThreeOneText; }
            set { SetProperty<string>(ref numGamesWonThreeOneText, value, nameof(NumGamesWonThreeOneText)); }
        }


        private string numGamesWonThreeTwoText;
        public string NumGamesWonThreeTwoText
        {
            get { return numGamesWonThreeTwoText; }
            set { SetProperty<string>(ref numGamesWonThreeTwoText, value, nameof(NumGamesWonThreeTwoText)); }
        }


        private string numGamesLostThreeZeroText;
        public string NumGamesLostThreeZeroText
        {
            get { return numGamesLostThreeZeroText; }
            set { SetProperty<string>(ref numGamesLostThreeZeroText, value, nameof(NumGamesLostThreeZeroText)); }
        }


        private string numGamesLostThreeOneText;
        public string NumGamesLostThreeOneText
        {
            get { return numGamesLostThreeOneText; }
            set { SetProperty<string>(ref numGamesLostThreeOneText, value, nameof(NumGamesLostThreeOneText)); }
        }


        private string numGamesLostThreeTwoText;
        public string NumGamesLostThreeTwoText
        {
            get { return numGamesLostThreeTwoText; }
            set { SetProperty<string>(ref numGamesLostThreeTwoText, value, nameof(NumGamesLostThreeTwoText)); }
        }
        private ObservableCollection<Game> results = new ObservableCollection<Game>();

        public ObservableCollection<Game> Results
        {
            get { return results; }
            set { SetProperty(ref results, value); }
        }

    }
}