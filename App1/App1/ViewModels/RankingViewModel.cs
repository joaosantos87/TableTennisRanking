﻿using App1.Views;
using DatabaseNetStandard.Data;
using Prism.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace App1.ViewModels
{
    public class RankingViewModel : BaseViewModel
    {
        public RankingViewModel()
        {
            Title = $"Ranking ELO";
            ItemTappedCommand = new DelegateCommand(ItemTapped);
            SearchPlayerCommand = new DelegateCommand(UpdateSearchPlayer);
            ItemClubCommand = new DelegateCommand<object>(ItemClubClicked);
            FilterVisible = true;
        }

        public RankingViewModel(Season season, Championship championship)
        {
            FillRanking(season, championship);
            FilterVisible = true;
        }

        public void FillRanking(Season season, Championship championship)
        {
            if(Seasons.Count==0)
                Seasons = Database.Instance.seasons.OrderByDescending(x=>x.StartDate).ToList();

            if(Championships.Count==0)
                Championships = Database.Instance.rankings.Select(x=>x.Championship).Distinct().OrderByDescending(x => x.TournamentType.Weight).ToList();

            SelectedSeason = season;
            SelectedChampionship = championship;
            FillRanking();
        }

        private ObservableCollection<Ranking> FillRanking()
        {
            if (SelectedChampionship == null || SelectedSeason == null)
                return Ranking;

            if (rankingUpdated)
                return Ranking;

            Ranking = new ObservableCollection<Ranking>(Database.Instance.rankings.Where(x => x.Player.Name.ToLower()!="bye" && x.Player.Name.ToLower() != "fc" && x.Championship.Id == SelectedChampionship.Id && x.Season.Id == SelectedSeason.Id).OrderBy(x=>x.Rank).ToList());
            rankingUpdated = true;
            return Ranking;
        }
        private List<Season> seasons = new List<Season>();
        public List<Season> Seasons
        {
            get
            {
                return seasons;
            }
            set
            {
                seasons = value;
                OnPropertyChanged(nameof(Seasons));
            }
        }
        private List<Championship> championships = new List<Championship>();
        public List<Championship> Championships
        {
            get
            {
                return championships;
            }
            set
            {
                championships = value;
                OnPropertyChanged(nameof(Championships));
            }
        }
        private bool rankingUpdated = false;
        public DelegateCommand ItemTappedCommand { get; private set; }

        public DelegateCommand SearchPlayerCommand { get; private set; }

        private ObservableCollection<Ranking> ranking = new ObservableCollection<Ranking>();

        public ObservableCollection<Ranking> Ranking
        {
            get { return ranking; }
            set { SetProperty(ref ranking, value); }
        }

        public Ranking Selected { get; set; }

        private Season selectedSeason { get; set; }
        public Season SelectedSeason
        {
            get { return selectedSeason; }
            set
            {
                if (selectedSeason == value)
                    return;

                if (value == null)
                    return;

                rankingUpdated = false;
                selectedSeason = value;
                OnPropertyChanged(nameof(SelectedSeason));
                FillRanking();
            }
        }

        private Championship selectedChampionship { get; set; }
        public Championship SelectedChampionship
        {
            get { return selectedChampionship; }
            set
            {
                if (selectedChampionship == value)
                    return;

                if (value == null)
                    return;

                rankingUpdated = false;
                selectedChampionship = value;
                OnPropertyChanged(nameof(SelectedChampionship));
                FillRanking();
            }
        }
        public string Search { get; set; }

        // Method which will be executed using our command
        void ItemTapped()
        {
            // Here you can do whatever you want, this will be executed when
            // user clicks on item in ListView, you will have a value of tapped
            // item in SlectedProfile property
            //Application.Current.MainPage.Navigation.PushAsync(new PlayerDetailPage(SelectedPlayer));
            Application.Current.MainPage.Navigation.PushAsync(new PlayerTabbedPage(Selected.Player));
        }


        public DelegateCommand<object> ItemClubCommand { get; private set; }

        void ItemClubClicked(object id)
        {
            var idInt = Convert.ToInt32(id);
            var SelectedClub = Database.Instance.clubs.Single(x => x.Id == idInt);
            // Here you can do whatever you want, this will be executed when
            // user clicks on item in ListView, you will have a value of tapped
            // item in SlectedProfile property
            //Application.Current.MainPage.Navigation.PushAsync(new PlayerDetailPage(SelectedPlayer));
            Application.Current.MainPage.Navigation.PushAsync(new ClubTabbedPage(SelectedClub));
        }

        private void UpdateSearchPlayer()
        {
            Ranking = SearchName(Search);
        }

        private ObservableCollection<Ranking> SearchName(string input)
        {
            if (input == "")
            {
                rankingUpdated = false;
                return FillRanking();
            }
            else
            {
                //SelectedSeason = seasons.SingleOrDefault(x => x.Name.ToLower().Equals("todasepocas"));
                //selectedChampionship = championships.SingleOrDefault(x => x.Name.ToLower() == "todoscampeonatos");
                rankingUpdated = false;
                FillRanking();
                var inputs = input.Split(' ');
                return new ObservableCollection<Ranking>(Ranking.Where(x => x.Player.Name.ToLower() != "bye" && x.Player.Name.ToLower() != "fc" && (inputs.All(a=>x.Player.Name.ToLower().Contains(a)) || inputs.All(a => x.Player.NameAutomaticInsertion.ToLower().Contains(a))) || x.Player.LicenceNo.Contains(input)));
            }
        }


        private bool filterVisible;
        public bool FilterVisible
        {
            get { return filterVisible; }

            set
            {
                filterVisible = value;
                if (filterVisible)
                    FiltrosText = "Esconder Filtros";
                else
                    FiltrosText = "Mostrar Filtros";
                OnPropertyChanged(nameof(FilterVisible));
            }
        }

        string filtrosText = "";
        public string FiltrosText
        {
            get { return filtrosText; }

            set
            {
                filtrosText = value;
                OnPropertyChanged(nameof(FiltrosText));
            }
        }

    }
}