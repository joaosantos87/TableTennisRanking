﻿using DatabaseNetStandard.Data;
using Prism.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;
using System.Linq;
using System.Globalization;
using System.ComponentModel;
using App1.Views;

namespace App1.ViewModels
{
    public class ClubGamesViewModel : BaseViewModel
    {
        public ClubGamesViewModel()
        {
            ItemTappedCommand = new DelegateCommand(ItemTapped);
            ItemVSCommand = new DelegateCommand<object>(ItemVsClicked);
            ItemResultCommand = new DelegateCommand<object>(ItemResultClicked);
            ItemClubCommand = new DelegateCommand<object>(ItemClubClicked);
            ItemChampionshipCommand = new DelegateCommand<object>(ItemChampionshipClicked);
            FilterVisible = true;
        }


        Club c;
        public Club Club 
        {
            get
            { 
                return c; 
            }
            set
            {
                c = value;
                OnPropertyChanged(nameof(Club));
            }
        }
        private bool filterVisible;
        public bool FilterVisible
        {
            get { return filterVisible; }

            set
            {
                filterVisible = value;
                if (filterVisible)
                    FiltrosText = "Esconder Filtros";
                else
                    FiltrosText = "Mostrar Filtros";
                OnPropertyChanged(nameof(FilterVisible));
            }
        }
        string filtrosText = "";
        public string FiltrosText
        {
            get { return filtrosText; }

            set
            {
                filtrosText = value;
                OnPropertyChanged(nameof(FiltrosText));
            }
        }

        public DelegateCommand ItemTappedCommand { get; private set; }
        public DelegateCommand<object> ItemResultCommand { get; private set; }
        public DelegateCommand<object> ItemClubCommand { get; private set; }
        public DelegateCommand<object> ItemVSCommand { get; private set; }
        public DelegateCommand<object> ItemChampionshipCommand { get; private set; }
        void ItemTapped()
        {
            // Here you can do whatever you want, this will be executed when
            // user clicks on item in ListView, you will have a value of tapped
            // item in SlectedProfile property
            
        }

        void ItemVsClicked(object oppId)
        {
            return;
            int id = Convert.ToInt32(oppId);
            var oppClub = Database.Instance.clubs.SingleOrDefault(x => x.Id == id);
            if (oppClub == null)
                return;
            var page = new ClubVsClubPage();
            page.SetClubs(Club, oppClub);
            Application.Current.MainPage.Navigation.PushAsync(page);
        }
        void ItemResultClicked(object teamGameId)
        {
            int id = Convert.ToInt32(teamGameId);
            var tg = Database.Instance.teamGames.SingleOrDefault(x => x.Id == id);
            if (tg == null)
                return;
            var page = new TeamGameResultPage();
            page.SetTeamGame(tg);
            Application.Current.MainPage.Navigation.PushAsync(page);
        }

        void ItemChampionshipClicked(object id)
        {
            if (!Int32.TryParse(id.ToString(), out int id_int))
                return;

            var tg = Database.Instance.teamGames.SingleOrDefault(x => x.Id == id_int);
            if (tg == null)
                return;

            if (tg.Championship.TournamentType.IsChampionship)
                Application.Current.MainPage.Navigation.PushAsync(new ChampionshipTabbedPage(SelectedChampionship));
            else
                Application.Current.MainPage.Navigation.PushAsync(new TournamentTabbedPage(SelectedChampionship));
        }

        public List<Season> Seasons
        {
            get
            {
                return seasons;
            }
            set
            {
                SetProperty<List<Season>>(ref seasons, value);
            }
        }
        public List<Championship> Championships
        {
            get
            {
                return championships;
            }
            set
            {
                SetProperty<List<Championship>>(ref championships, value);
            }
        }

        private Season season;
        public Season SelectedSeason
        {
            get
            {
                return season;
            }
            set
            {
                if (season != value)
                {
                    season = value;
                    FetchClubPlayersHistoric();
                    OnPropertyChanged(nameof(SelectedSeason));
                }
            }
        }
        private Championship championship;
        public Championship SelectedChampionship
        {
            get
            {
                return championship;
            }
            set
            {
                if (championship != value)
                {
                    championship = value;
                    FetchClubPlayersHistoric();
                    OnPropertyChanged(nameof(Championship));
                }
            }
        }
        private TeamGame selectedGame;
        public TeamGame SelectedGame
        {
            get { return selectedGame; }
            set { SetProperty(ref selectedGame, value); }
        }

        private void FetchClubPlayersHistoric()
        {
            var tg = Database.Instance.teamGames.Where(x => (x.ClubA.Id == Club.Id || x.ClubB.Id == Club.Id) && (championship != null ? x.Championship.Id == championship.Id : x.Championship.Name.ToLower() != "todoscampeonatos") && (season != null ? x.Date >= season.StartDate && x.Date <=season.EndDate : true)).OrderByDescending(x => x.Date).ToList();
            tg.ForEach(x => x.SetCurrentClub = Club.Id);

            tg.RemoveAll(x => x.ResultClubA == 0 && x.ResultClubB == 0);

            Results = new ObservableCollection<TeamGame>(tg);

            var tmp = new List<Season>();
            foreach(var t in tg)
            {
                var curSeason = Database.Instance.seasons.Single(x => x.StartDate < t.Date && x.EndDate > t.Date && x.IsSingleSeason);
                if (tmp.Contains(curSeason))
                    continue;
                tmp.Add(curSeason);
            }

            if (season == null)
                Seasons = tmp;

            if (championship == null)
                Championships = tg.Select(x => x.Championship).Distinct().OrderByDescending(x => x.TournamentType.Weight).ToList();
        }

        private List<Season> seasons;
        private List<Championship> championships;

        void ItemClubClicked(object id_object)
        {
            int id = Convert.ToInt32(id_object);
            var c = Database.Instance.clubs.SingleOrDefault(x => x.Id == id);
            if (c == null)
                return;
            Application.Current.MainPage.Navigation.PushAsync(new ClubTabbedPage(c));
        }

        public void DefineClub(Club c)
        {
            if (c == null)
                return;

            if (Club == null || Club.Id != c.Id)
            {
                Club = c;
            }

            FetchClubPlayersHistoric();
        }

        private void FetchClubGames()
        {

        }

        private ObservableCollection<TeamGame> results = new ObservableCollection<TeamGame>();

        public ObservableCollection<TeamGame> Results
        {
            get { return results; }
            set { SetProperty(ref results, value); }
        }
    }
}