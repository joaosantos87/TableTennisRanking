﻿using DatabaseNetStandard.Data;
using Prism.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;
using System.Linq;
using System.Globalization;
using System.ComponentModel;
using App1.Views;

namespace App1.ViewModels
{
    public class ChampionshipGamesViewModel : BaseViewModel
    {
        public ChampionshipGamesViewModel()
        {
            ItemResultCommand = new DelegateCommand<object>(ItemResultClicked);
            ItemClubCommand = new DelegateCommand<object>(ItemClubClicked);
        }

        public DelegateCommand<object> ItemResultCommand { get; private set; }
        public DelegateCommand<object> ItemClubCommand { get; private set; }

        void ItemResultClicked(object teamGameId)
        {
            int id = Convert.ToInt32(teamGameId);
            var tg = Database.Instance.teamGames.SingleOrDefault(x => x.Id == id);
            if (tg == null)
                return;
            var page = new TeamGameResultPage();
            page.SetTeamGame(tg);
            Application.Current.MainPage.Navigation.PushAsync(page);
        }

        void ItemClubClicked(object id_object)
        {
            int id = Convert.ToInt32(id_object);
            var c = Database.Instance.clubs.SingleOrDefault(x => x.Id == id);
            if (c == null)
                return;
            Application.Current.MainPage.Navigation.PushAsync(new ClubTabbedPage(c));
        }

        Championship c;
        public Championship Championship
        {
            get
            { 
                return c; 
            }
            set
            {
                c = value;
                OnPropertyChanged(nameof(Championship));
            }
        }

        public void DefineChampionship(Championship c)
        {
            if (c == null)
                return;

            Championship = c;
            Title = Championship.NameAutomaticInsertion != "" ? Championship.NameAutomaticInsertion : Championship.Name;
            GetChampionshipResults();
            if(seasons.Count > 0)
                SelectedSeason = seasons.First();
        }

        private List<Season> seasons = new List<Season>();
        public List<Season> Seasons
        {
            get
            {
                return seasons;
            }
            set
            {
                seasons = value;
                OnPropertyChanged(nameof(Seasons));
            }
        }
        private Season selectedSeason { get; set; }
        public Season SelectedSeason
        {
            get { return selectedSeason; }
            set
            {
                if (selectedSeason == value)
                    return;

                if (value == null)
                    return;

                selectedSeason = value;
                OnPropertyChanged(nameof(SelectedSeason));
                GetChampionshipResults();
            }
        }

        public DelegateCommand<object> ItemVSCommand { get; private set; }

        void ItemVsClicked(object oppId)
        {
            //int id = Convert.ToInt32(oppId);
            //var oppClub = Database.Instance.clubs.SingleOrDefault(x => x.Id == id);
            //if (oppClub == null)
            //    return;
            //var page = new ClubVsClubPage();
            //page.SetClubs(Club, oppClub);
            //Application.Current.MainPage.Navigation.PushAsync(page);
        }

        private void GetChampionshipResults()
        {
            if (Seasons.Count == 0)
                Seasons = Database.Instance.seasons.OrderByDescending(x => x.StartDate).ToList();

            var teamGames = Database.Instance.teamGames.Where(x => x.Day>0 && x.Championship.Id == Championship.Id && (SelectedSeason != null ? x?.Season.Id == SelectedSeason.Id : true)).OrderByDescending(x => x.Date).ToList();

            if (SelectedSeason == null)
            {
                Seasons = teamGames.Select(x => x.Season).Distinct().OrderByDescending(x => x?.StartDate).ToList();
            }

            var days = teamGames.Select(x => x.Day).Distinct().OrderBy(x=>x).ToList();
            List<JornadaTable> jornadaTable = new List<JornadaTable>();
            foreach (var d in days)
            {
                var jornadaDayRows = new List<JornadaRow>();
                var games = teamGames.Where(x => x.Day == d).ToList();
                foreach (var game in games)
                {
                    var jornadaTableRow = new JornadaRow()
                    {
                        TeamGameId = game.Id,
                        Date = game.Date,
                        ClubA = game.ClubA,
                        ClubAFormated = game.ClubADisplayName,
                        ClubB = game.ClubB,
                        ClubBFormated = game.ClubBDisplayName,
                        DateFormated = game.DateFormated,
                        Result = game.Result_ClubA_ClubB
                    };
                    jornadaDayRows.Add(jornadaTableRow);
                }
                jornadaTable.Add(new JornadaTable() 
                {
                    Day = d,
                    Games = jornadaDayRows,
                    Height = jornadaDayRows.Count * 60 + 10
            });
            }

            JornadaTable = new ObservableCollection<JornadaTable>(jornadaTable);

        }

        public int GamesHeight
        {
            get { return gamesHeight; }
            set { SetProperty<int>(ref gamesHeight, value); }
        }
        
        private ObservableCollection<JornadaTable> jornadaTable = new ObservableCollection<JornadaTable>();
        private int gamesHeight;

        public ObservableCollection<JornadaTable> JornadaTable
        {
            get { return jornadaTable; }
            set { SetProperty(ref jornadaTable, value); }
        }

    }
}