﻿using DatabaseNetStandard.Data;
using Prism.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;
using System.Linq;
using System.Globalization;
using System.ComponentModel;
using App1.Views;

namespace App1.ViewModels
{
    public class PlayerGamesViewModel : BaseViewModel
    {
        public PlayerGamesViewModel()
        {
            ItemTappedCommand = new DelegateCommand(ItemTapped);
            ItemVSCommand = new DelegateCommand<object>(ItemVsClicked);
            ItemChampionshipCommand = new DelegateCommand(ItemChampionshipClicked);
            ItemPlayerCommand = new DelegateCommand<object>(ItemPlayerClicked);
            FilterVisible = true;
        }

        private List<Season> seasons;
        private List<Championship> championships;

        public List<Season> Seasons
        {
            get
            {
                return seasons;
            }
            set
            {
                SetProperty<List<Season>>(ref seasons, value);
            }
        }
        public List<Championship> Championships
        {
            get
            {
                return championships;
            }
            set
            {
                SetProperty<List<Championship>>(ref championships, value);
            }
        }

        private Season season;
        public Season SelectedSeason
        {
            get
            {
                return season;
            }
            set
            {
                if (season != value)
                {
                    season = value;
                    FetchPlayerGamesHistoric();
                    OnPropertyChanged(nameof(SelectedSeason));
                }
            }
        }
        private Championship championship;
        public Championship SelectedChampionship
        {
            get
            {
                return championship;
            }
            set
            {
                if (championship != value)
                {
                    championship = value;
                    FetchPlayerGamesHistoric();
                    OnPropertyChanged(nameof(Championship));
                }
            }
        }


        Player p;
        public Player Player 
        {
            get
            { 
                return p; 
            }
            set
            {
                p = value;
                OnPropertyChanged(nameof(Player));
            }
        }
        private bool filterVisible;
        public bool FilterVisible
        {
            get { return filterVisible; }

            set
            {
                filterVisible = value;
                if (filterVisible)
                    FiltrosText = "Esconder Filtros";
                else
                    FiltrosText = "Mostrar Filtros";
                OnPropertyChanged(nameof(FilterVisible));
            }
        }
        string filtrosText = "";
        public string FiltrosText
        {
            get { return filtrosText; }

            set
            {
                filtrosText = value;
                OnPropertyChanged(nameof(FiltrosText));
            }
        }

        public DelegateCommand ItemTappedCommand { get; private set; }
        public DelegateCommand<object> ItemVSCommand { get; private set; }
        public DelegateCommand ItemChampionshipCommand { get; private set; }
        public DelegateCommand<object> ItemPlayerCommand { get; private set; }
        void ItemTapped()
        {
            // Here you can do whatever you want, this will be executed when
            // user clicks on item in ListView, you will have a value of tapped
            // item in SlectedProfile property
            
        }

        void ItemVsClicked(object oppId)
        {
            int id = Convert.ToInt32(oppId);
            var oppPlayer = Database.Instance.players.SingleOrDefault(x => x.Id == id);
            if (oppPlayer == null)
                return;
            var page = new PlayerVsPlayerPage();
            page.SetPlayers(Player, oppPlayer);
            Application.Current.MainPage.Navigation.PushAsync(page);
        }

        void ItemChampionshipClicked()
        {

        }


        void ItemPlayerClicked(object id_object)
        {
            int id = Convert.ToInt32(id_object);
            var p = Database.Instance.players.SingleOrDefault(x => x.Id == id);
            if (p == null)
                return;
            Application.Current.MainPage.Navigation.PushAsync(new PlayerTabbedPage(p));
        }

        public void DefinePlayer(Player p)
        {
            if (p == null)
                return;

            if (Player == null || Player.Id != p.Id)
            {
                Player = p;
            }
            FetchPlayerGamesHistoric();
        }

        private void FetchPlayerGamesHistoric()
        {
            var g = Database.Instance.games.Where(x => x.OppName.ToLower() != "bye" && x.OppName.ToLower() != "fc" && (x.IdPlayerA == p.Id || x.IdPlayerB == p.Id) && (championship != null ? x.Championship.Id == championship.Id : x.Championship.Name.ToLower() != "todoscampeonatos") && (season != null ? (x.Date >= season.StartDate && x.Date <= season.EndDate) : true)).ToList();
            g.ForEach(a => a.SetCurrentPlayer = p.Id);

            var dg = Database.Instance.doubleGames.Where(x=> (x.OppNameA.ToLower() != "bye" || x.OppNameB.ToLower()!="bye") && (x.OppNameA.ToLower() != "fc" || x.OppNameB.ToLower() != "fc") &&
            ((x.DoubleAIdPlayerA == p.Id || x.DoubleAIdPlayerB == p.Id) || (x.DoubleBIdPlayerA == p.Id || x.DoubleBIdPlayerB == p.Id)) && (championship != null ? x.Championship.Id == championship.Id : x.Championship.Name.ToLower() != "todoscampeonatos") && (season != null ? (x.Date >= season.StartDate && x.Date <= season.EndDate) : true)).ToList();
            dg.ForEach(a =>
            {
                a.SetCurrentPlayer = p.Id;
            });

            List<GameBase> all = new List<GameBase>();
            all.AddRange(g);
            all.AddRange(dg);

            Results = new ObservableCollection<GameBase>(all.OrderByDescending(x => x.Date).ThenByDescending(x=> Database.Instance.phases.SingleOrDefault(a=>a.NameShort == x.GetDayOrPhase)?.Order));

            var tmp = new List<Season>();
            foreach (var t in g)
            {
                var curSeason = Database.Instance.seasons.SingleOrDefault(x => x.StartDate < t.Date && x.EndDate > t.Date && x.IsSingleSeason);
                if (curSeason==null || tmp.Contains(curSeason))
                    continue;
                tmp.Add(curSeason);
            }

            if (season == null)
                Seasons = tmp.OrderByDescending(x=>x.StartDate).ToList();

            if (championship == null)
                Championships = g.Select(x => x.Championship).Distinct().OrderByDescending(x => x.TournamentType.Weight).ToList();
        }

        private ObservableCollection<GameBase> results = new ObservableCollection<GameBase>();

        public ObservableCollection<GameBase> Results
        {
            get { return results; }
            set { SetProperty(ref results, value); }
        }
    }
}