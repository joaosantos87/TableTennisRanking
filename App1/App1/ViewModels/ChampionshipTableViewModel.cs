﻿using DatabaseNetStandard.Data;
using Prism.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;
using System.Linq;
using System.Globalization;
using System.ComponentModel;
using App1.Views;

namespace App1.ViewModels
{
    public class ChampionshipTableViewModel : BaseViewModel
    {
        public ChampionshipTableViewModel()
        {
            ItemClubCommand = new DelegateCommand<object>(ItemClubClicked);
        }
        
        public DelegateCommand<object> ItemClubCommand { get; private set; }

        void ItemClubClicked(object id_object)
        {
            int id = Convert.ToInt32(id_object);
            var c = Database.Instance.clubs.SingleOrDefault(x => x.Id == id);
            if (c == null)
                return;
            Application.Current.MainPage.Navigation.PushAsync(new ClubTabbedPage(c));
        }

        Championship c;
        public Championship Championship
        {
            get
            { 
                return c; 
            }
            set
            {
                c = value;
                OnPropertyChanged(nameof(Championship));
            }
        }

        public void DefineChampionship(Championship c)
        {
            if (c == null)
                return;

            Championship = c;

            Title = Championship.NameAutomaticInsertion != "" ? Championship.NameAutomaticInsertion : Championship.Name;
            GetChampionshipResults();
            SelectedSeason = seasons!=null&& seasons.Count > 0 ? seasons.First() : null;
        }

        public DelegateCommand<object> ItemVSCommand { get; private set; }

        void ItemVsClicked(object oppId)
        {
            //int id = Convert.ToInt32(oppId);
            //var oppClub = Database.Instance.clubs.SingleOrDefault(x => x.Id == id);
            //if (oppClub == null)
            //    return;
            //var page = new ClubVsClubPage();
            //page.SetClubs(Club, oppClub);
            //Application.Current.MainPage.Navigation.PushAsync(page);
        }

        private List<Season> seasons = new List<Season>();
        public List<Season> Seasons
        {
            get
            {
                return seasons;
            }
            set
            {
                seasons = value;
                OnPropertyChanged(nameof(Seasons));
            }
        }
        private Season selectedSeason { get; set; }
        public Season SelectedSeason
        {
            get { return selectedSeason; }
            set
            {
                if (selectedSeason == value)
                    return;

                //if (value == null)
                //    return;

                selectedSeason = value;
                OnPropertyChanged(nameof(SelectedSeason));
                GetChampionshipResults();
            }
        }

        private void GetChampionshipResults()
        {
            if (Seasons.Count == 0)
                Seasons = Database.Instance.seasons.OrderByDescending(x => x.StartDate).ToList();

            var teamGames = Database.Instance.teamGames.Where(x => x.Championship.Id == Championship.Id && (SelectedSeason != null ? x?.Season.Id == SelectedSeason.Id : true)).OrderByDescending(x => x.Date).ToList();

            if (SelectedSeason == null)
            {
                Seasons = teamGames.Select(x => x.Season).Distinct().OrderByDescending(x => x?.StartDate).ToList();
            }

            var clubs = teamGames.Select(x => x.ClubA).Distinct().ToList();
            clubs.AddRange(teamGames.Select(x => x.ClubB).Distinct().Distinct());
            clubs = clubs.Distinct().ToList();
            List<ChampTable> champTable = new List<ChampTable>();
            foreach (var c in clubs)
            {
                var games = teamGames.Where(x => x.ClubA.Id == c.Id || x.ClubB.Id == c.Id).ToList();
                games.ForEach(x => x.SetCurrentClub = c.Id);

                games.ForEach(x => x.singularGames.ForEach(a => a.SetCurrentClub = c.Id));
                games.ForEach(x => x.doubleGames.ForEach(a => a.SetCurrentClub = c.Id));
                var champTableRow = new ChampTable()
                {
                    Club = c,
                    ClubTableName = games.First().CurrentClubDisplayName,
                    VictoryFourZero = games.Count(x => x.CurrentClubResult == 4 && x.OppClubResult == 0),
                    VictoryFourOne = games.Count(x => x.CurrentClubResult == 4 && x.OppClubResult == 1),
                    VictoryThreeTwo = games.Count(x => x.CurrentClubResult == 3 && x.OppClubResult == 2),
                    LoseThreeTwo = games.Count(x => x.CurrentClubResult == 2 && x.OppClubResult == 3),
                    LoseFourOne = games.Count(x => x.CurrentClubResult == 1 && x.OppClubResult == 4),
                    LoseFourZero = games.Count(x => x.CurrentClubResult == 0 && x.OppClubResult == 4),
                    VictoryThreeOne = games.Count(x => x.CurrentClubResult == 3 && x.OppClubResult == 1),
                    VictoryThreeZero = games.Count(x => x.CurrentClubResult == 3 && x.OppClubResult == 0),
                    LoseThreeZero = games.Count(x => x.CurrentClubResult == 0 && x.OppClubResult == 3),
                    LoseThreeOne = games.Count(x => x.CurrentClubResult == 1 && x.OppClubResult == 3),
                    SetSeason = SelectedSeason
                };
                champTable.Add(champTableRow);
            }
            champTable = champTable.OrderByDescending(x => x.Points_All).ThenByDescending(x=>(x.PG-x.PP)).ToList();

            for (int i = 0; i < champTable.Count; i++)
                champTable[i].Pos = i + 1;

            ChampTable = new ObservableCollection<ChampTable>(champTable);

        }
        
        private ObservableCollection<ChampTable> champTable = new ObservableCollection<ChampTable>();

        public ObservableCollection<ChampTable> ChampTable
        {
            get { return champTable; }
            set { SetProperty(ref champTable, value); }
        }

    }
}