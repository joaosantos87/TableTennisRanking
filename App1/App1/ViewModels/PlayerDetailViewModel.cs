﻿using DatabaseNetStandard.Data;
using Prism.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;
using System.Linq;
using System.Globalization;
using System.ComponentModel;
using App1.Views;

namespace App1.ViewModels
{
    public class PlayerDetailViewModel : BaseViewModel
    {
        public PlayerDetailViewModel()
        {
            StartDate = DateTime.MinValue;
            EndDate = DateTime.Now;
            ItemTappedCommand = new DelegateCommand(ItemTapped);
            ItemVSCommand = new DelegateCommand(ItemVsClicked);
            ItemChampionshipCommand = new DelegateCommand(ItemChampionshipClicked);
            ItemPlayerCommand = new DelegateCommand<object>(ItemPlayerClicked);
            FilterVisible = true;
            PersonalInfoVisible = true;
            GamesHistoricVisible = true;
            SeasonsHistoricVisible = true;
        }

        private DateTime startDate;
        public DateTime StartDate
        {
            get
            { return startDate; }
            set
            {
                if (startDate != value)
                {
                    startDate = value;
                    DefinePlayer(p);
                    OnPropertyChanged(nameof(StartDate));
                }
            }
        }

        private DateTime endDate;
        public DateTime EndDate
        {
            get
            { 
                return endDate; 
            }
            set
            {
                if (endDate != value)
                {
                    endDate = value;
                    DefinePlayer(p);
                    OnPropertyChanged(nameof(EndDate));
                }
            }
        }

        Player p;
        public Player Player 
        {
            get
            { 
                return p; 
            }
            set
            {
                p = value;
                OnPropertyChanged(nameof(Player));
            }
        }
        private bool filterVisible;
        public bool FilterVisible
        {
            get { return filterVisible; }

            set
            {
                filterVisible = value;
                if (filterVisible)
                    FiltrosText = "Esconder Filtros";
                else
                    FiltrosText = "Mostrar Filtros";
                OnPropertyChanged(nameof(FilterVisible));
            }
        }
        private bool gamesHistoricVisible;
        public bool GamesHistoricVisible
        {
            get { return gamesHistoricVisible; }

            set
            {
                gamesHistoricVisible = value;
                if (gamesHistoricVisible)
                    GamesHistoricText = "Esconder Detalhe Jogos";
                else
                    GamesHistoricText = "Mostrar Detalhe Jogos";
                OnPropertyChanged(nameof(GamesHistoricVisible));
            }
        }

        private bool personalInfoVisible;
        public bool PersonalInfoVisible
        {
            get { return personalInfoVisible; }

            set
            {
                personalInfoVisible = value;
                if (personalInfoVisible)
                    PersonalInfoText = "Esconder Informacao Pessoal";
                else
                    PersonalInfoText = "Mostrar Informacao Pessoal";
                OnPropertyChanged(nameof(PersonalInfoVisible));
            }
        }

        private bool seasonsHistoricVisible;
        public bool SeasonsHistoricVisible
        {
            get { return seasonsHistoricVisible; }

            set
            {
                seasonsHistoricVisible = value;
                if (seasonsHistoricVisible)
                    SeasonsHistoricText = "Esconder Historico";
                else
                    SeasonsHistoricText = "Mostrar Historico";
                OnPropertyChanged(nameof(SeasonsHistoricVisible));
            }
        }

        string filtrosText = "";
        public string FiltrosText
        {
            get { return filtrosText; }

            set
            {
                filtrosText = value;
                OnPropertyChanged(nameof(FiltrosText));
            }
        }
        string personalInfoText = "";
        public string PersonalInfoText
        {
            get { return personalInfoText; }

            set
            {
                personalInfoText = value;
                OnPropertyChanged(nameof(PersonalInfoText));
            }
        }

        string gamesHistoricText = "";
        public string GamesHistoricText
        {
            get { return gamesHistoricText; }

            set
            {
                gamesHistoricText = value;
                OnPropertyChanged(nameof(GamesHistoricText));
            }
        }

        string seasonsHistoricText = "";
        public string SeasonsHistoricText
        {
            get { return seasonsHistoricText; }

            set
            {
                seasonsHistoricText = value;
                OnPropertyChanged(nameof(SeasonsHistoricText));
            }
        }

        public DelegateCommand ItemTappedCommand { get; private set; }
        public DelegateCommand ItemVSCommand { get; private set; }
        public DelegateCommand ItemChampionshipCommand { get; private set; }
        public DelegateCommand<object> ItemPlayerCommand { get; private set; }
        void ItemTapped()
        {
            // Here you can do whatever you want, this will be executed when
            // user clicks on item in ListView, you will have a value of tapped
            // item in SlectedProfile property
            
        }

        void ItemVsClicked()
        {
            // Here you can do whatever you want, this will be executed when
            // user clicks on item in ListView, you will have a value of tapped
            // item in SlectedProfile property

        }

        void ItemChampionshipClicked()
        {

        }


        void ItemPlayerClicked(object id_object)
        {
            int id = Convert.ToInt32(id_object);
            var p = Database.Instance.players.SingleOrDefault(x => x.Id == id);
            if (p == null)
                return;
            Application.Current.MainPage.Navigation.PushAsync(new PlayerDetailPage(p));
        }

        public void DefinePlayer(Player p)
        {
            if (p == null)
                return;

            if (Player == null || Player.Id != p.Id)
            {
                Player = p;
                FetchPlayerHistoric();
            }

            Title = this.Player.NameAutomaticInsertion != "" ? this.Player.NameAutomaticInsertion : this.Player.Name;
            var g = Database.Instance.games.Where(x => x.Date >= StartDate && x.Date <= EndDate && ( x.IdPlayerA == p.Id || x.IdPlayerB == p.Id)).OrderByDescending(x => x.Date).ToList();
            g.ForEach(a => a.SetCurrentPlayer = p.Id);
            Results = new ObservableCollection<Game>(g);
            if (g.Count > 0)
            {
                StartDate = g.Last().Date;
                EndDate = g[0].Date;
            }
        }

        private void FetchPlayerHistoric()
        {
            PlayerHistoric = new ObservableCollection<Ranking>(Database.Instance.rankings.Where(x => x.Player.Id == Player.Id).OrderByDescending(x=>x.Season.StartDate));

        }

        private ObservableCollection<Game> results = new ObservableCollection<Game>();

        public ObservableCollection<Game> Results
        {
            get { return results; }
            set { SetProperty(ref results, value); }
        }

        private ObservableCollection<Ranking> playerHistoric = new ObservableCollection<Ranking>();

        public ObservableCollection<Ranking> PlayerHistoric
        {
            get { return playerHistoric; }
            set { SetProperty(ref playerHistoric, value); }
        }

        public ICommand GetOppNameCommand
        {
            get
            {
                return new Command((e) =>
                {
                    var item = (e as Player);
                    //if(item)
                    // delete logic on item
                });
            }
        }
        public ICommand GetResult { get; }
    }
}