﻿using DatabaseNetStandard.Data;
using Prism.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;
using System.Linq;
using System.Globalization;
using System.ComponentModel;
using App1.Views;

namespace App1.ViewModels
{
    public class TournamentInfoViewModel : BaseViewModel
    {
        public TournamentInfoViewModel()
        {
            ItemPlayerCommand = new DelegateCommand<object>(ItemPlayerClicked);
        }

        public DelegateCommand<object> ItemPlayerCommand { get; private set; }

        void ItemPlayerClicked(object id_object)
        {
            int id = Convert.ToInt32(id_object);
            var c = Database.Instance.players.SingleOrDefault(x => x.Id == id);
            if (c == null)
                return;
            Application.Current.MainPage.Navigation.PushAsync(new PlayerTabbedPage(c));
        }


        private int numberGames = 0;
        public int NumberGames
        {
            get { return numberGames; }
            set { SetProperty<int>(ref numberGames, value); }
        }
        private int numberPlayers = 0;
        public int NumberPlayers
        {
            get { return numberPlayers; }
            set { SetProperty<int>(ref numberPlayers, value); }
        }

        Championship c;
        public Championship Championship
        {
            get
            { 
                return c; 
            }
            set
            {
                c = value;
                OnPropertyChanged(nameof(Championship));
            }
        }
        private List<Season> seasons = new List<Season>();
        public List<Season> Seasons
        {
            get
            {
                return seasons;
            }
            set
            {
                seasons = value;
                OnPropertyChanged(nameof(Seasons));
            }
        }
        private Season selectedSeason { get; set; }
        public Season SelectedSeason
        {
            get { return selectedSeason; }
            set
            {
                if (selectedSeason == value)
                    return;

                if (value == null)
                    return;

                selectedSeason = value;
                OnPropertyChanged(nameof(SelectedSeason));
                GetChampionshipResults();
            }
        }

        public void DefineChampionship(Championship c)
        {
            if (c == null)
                return;

            Championship = c;
            
            Title = Championship.NameAutomaticInsertion != "" ? Championship.NameAutomaticInsertion : Championship.Name;
            GetChampionshipResults();
        }

        public DelegateCommand<object> ItemVSCommand { get; private set; }

        void ItemVsClicked(object oppId)
        {
            //int id = Convert.ToInt32(oppId);
            //var oppClub = Database.Instance.clubs.SingleOrDefault(x => x.Id == id);
            //if (oppClub == null)
            //    return;
            //var page = new ClubVsClubPage();
            //page.SetClubs(Club, oppClub);
            //Application.Current.MainPage.Navigation.PushAsync(page);
        }

        private void GetChampionshipResults()
        {
            if (Seasons.Count == 0)
                Seasons = Database.Instance.seasons.OrderByDescending(x => x.StartDate).ToList();

            var games = new List<GameBase>();
            games = Database.Instance.games.Where(x => x.Championship.Id == Championship.Id && (SelectedSeason!=null ? x?.Season?.Id == SelectedSeason.Id : true)).Select(x=>(GameBase)x).OrderByDescending(x=>x.Date).ToList();

            games.AddRange(Database.Instance.doubleGames.Where(x => x.Championship.Id == Championship.Id && (SelectedSeason != null ? x?.Season?.Id == SelectedSeason.Id : true)).Select(x => (GameBase)x).OrderByDescending(x => x.Date));

            var data = games.Select(a => (a is DoubleGame) ? (a as DoubleGame).DoubleANamePlayerA : (a as Game).PlayerAName).Distinct().ToList();
            data.AddRange(games.Select(a => (a is DoubleGame) ? (a as DoubleGame).DoubleANamePlayerB : (a as Game).PlayerAName).Distinct());
            data.AddRange(games.Select(a => (a is DoubleGame) ? (a as DoubleGame).DoubleBNamePlayerA : (a as Game).PlayerBName).Distinct());
            data.AddRange(games.Select(a => (a is DoubleGame) ? (a as DoubleGame).DoubleBNamePlayerB : (a as Game).PlayerBName).Distinct());
            data = data.Distinct().ToList();

            NumberGames = games.Count;
            NumberPlayers = data.Count;

            if (numberGames == 0)
                return;

            var tournamentDate = games.First().Date;
            //This needs to be like this to fetch the ranking immediatetly before the tournament occours
            var usefulDate = new DateTime(tournamentDate.Year - 1, tournamentDate.Month, tournamentDate.Day);

            var season = Database.Instance.seasons.SingleOrDefault(x => x.StartDate < usefulDate && x.EndDate > usefulDate && x.IsSingleSeason);

            var runningSeason = Database.Instance.seasons.SingleOrDefault(x => x.EndDate == season?.EndDate && !x.IsSingleSeason);

            Players = new ObservableCollection<Ranking>(Database.Instance.rankings.Where(x=>data.Contains(x.Player.Name) && x.Season.Id == runningSeason?.Id && x.Championship.Name.ToLower()=="todoscampeonatos").OrderByDescending(x=>x.Points));

            //if(Players.Count==0)
            //{
            //    var tmpList = new List<Ranking>();
            //    foreach(var d in data)
            //    {
            //        var players = d.Split('/');
            //        var plA = Database.Instance.players.SingleOrDefault(x => x.SmallName == players[0]);
            //        var plB = Database.Instance.players.SingleOrDefault(x => x.SmallName == players[1]);
            //        tmpList.Add(new Ranking(0, 
            //    }
            //}

            if (SelectedSeason == null)
            {
                Seasons = games.Select(x => x.Season).Distinct().OrderByDescending(x => x?.StartDate).ToList();
                SelectedSeason = seasons.First();
            }

            if (season != null)
            {
                var currentTournamentResults = new List<GameBase>();
                currentTournamentResults = Database.Instance.games.Where(x => x.Season.Id == SelectedSeason?.Id && x.Championship.Id == Championship.Id).Select(x=>x as GameBase).ToList();
                if (currentTournamentResults.Count > 0)
                {
                    foreach (var p in Players)
                    {
                        var cp = currentTournamentResults.Where(x => (x as Game).PlayerA.Id == p.Player.Id || (x as Game).PlayerB.Id == p.Player.Id).OrderBy(x => (x as Game).Phase.Order).ToList();
                        cp.ForEach(x => x.SetCurrentPlayer = p.Player.Id);
                        var curTournamentRes = cp.Sum(x => x.PointsCurrentPlayer);
                        p.Difference = curTournamentRes;
                        var numWins = cp.Count(x => (x as Game).IdPlayerWinner == p.Player.Id);
                        var total = cp.Count;
                        var numLoses = total - numWins;
                        if (cp == null)
                            p.UpdateGames(-1, -1);
                        else
                        {
                            p.UpdateGames(numWins, numLoses);
                            var lastGame = cp.LastOrDefault(x => (x as Game).IdPlayerWinner != p.Player.Id || (x as Game).Phase.Order == 8) as Game;
                            if (lastGame != null && lastGame.Phase.Order == 8)
                                p.InfoText = lastGame.IdPlayerWinner == p.Player.Id ? "3º" : "4º";
                            else
                                p.InfoText = lastGame == null ? "Vencedor" : lastGame.Phase.Name;
                        }
                    }

                    Players = new ObservableCollection<Ranking>(Players.OrderByDescending(x => x.Points).ThenBy(x => x.Rank));
                }
            }
        }
        //private string result = "";
        //public string Result
        //{
        //    get { return result; }
        //    set { SetProperty<String>(ref result, value); }
        //}
        
        private ObservableCollection<Ranking> players = new ObservableCollection<Ranking>();

        public ObservableCollection<Ranking> Players
        {
            get { return players; }
            set { SetProperty(ref players, value); }
        }

    }
}