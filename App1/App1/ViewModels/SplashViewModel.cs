﻿using DatabaseNetStandard.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace TenisMesaBaseDados.ViewModels
{
    public class SplashViewModel
    {
        public SplashViewModel()
        {

        }

        private void NavigateToMainPage()
        {
            //Application.Current.MainPage = new MainPage();
        }

        public void LoadDatabase()
        {
            // Load data from the database
            Database.LoadDatabaseIfNotLoaded();
        }
    }
}
