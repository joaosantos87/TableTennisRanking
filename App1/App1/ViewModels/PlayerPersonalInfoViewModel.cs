﻿using DatabaseNetStandard.Data;
using Prism.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;
using System.Linq;
using System.Globalization;
using System.ComponentModel;
using App1.Views;

namespace App1.ViewModels
{
    public class PlayerPersonalInfoViewModel : BaseViewModel
    {
        public PlayerPersonalInfoViewModel()
        {
            ItemVSCommand = new DelegateCommand<object>(ItemVsClicked);
            ItemTappedCommand = new DelegateCommand(ItemTapped);
            ItemPlayerCommand = new DelegateCommand<object>(ItemPlayerClicked);
            ItemChampionshipCommand = new DelegateCommand<object>(ItemChampionshipClicked);
        }

        public DelegateCommand<object> ItemChampionshipCommand { get; private set; }
        public DelegateCommand ItemTappedCommand { get; private set; }
        public DelegateCommand<object> ItemPlayerCommand { get; private set; }
        void ItemPlayerClicked(object id_object)
        {
            int id = Convert.ToInt32(id_object);
            var p = Database.Instance.players.SingleOrDefault(x => x.Id == id);
            if (p == null)
                return;
            Application.Current.MainPage.Navigation.PushAsync(new PlayerTabbedPage(p));
        }
        void ItemChampionshipClicked(object id)
        {
            var idInt = Convert.ToInt32(id);
            var SelectedChampionship = Database.Instance.championships.Single(x => x.Id == idInt);
            Application.Current.MainPage.Navigation.PushAsync(new ChampionshipTabbedPage(SelectedChampionship));
        }
        void ItemTapped()
        {
            // Here you can do whatever you want, this will be executed when
            // user clicks on item in ListView, you will have a value of tapped
            // item in SlectedProfile property

        }

        Player p;
        public Player Player
        {
            get
            { 
                return p; 
            }
            set
            {
                p = value;
                OnPropertyChanged(nameof(Player));
            }
        }

        public void DefinePlayer(Player p)
        {
            if (p == null)
                return;

            if (Player == null || Player.Id != p.Id)
            {
                Player = p;
            }
            
            Title = Player.NameAutomaticInsertion != "" ? Player.NameAutomaticInsertion : Player.Name;
            FillResults();
        }

        public DelegateCommand<object> ItemVSCommand { get; private set; }
        void ItemVsClicked(object oppId)
        {
            int id = Convert.ToInt32(oppId);
            var oppPlayer = Database.Instance.players.SingleOrDefault(x => x.Id == id);
            if (oppPlayer == null)
                return;
            var page = new PlayerVsPlayerPage();
            page.SetPlayers(Player, oppPlayer);
            Application.Current.MainPage.Navigation.PushAsync(page);
        }

        private void FillResults()
        {
            var games = Database.Instance.games.Where(x => x.OppName.ToLower() != "bye" && x.OppName.ToLower() != "fc" && (x.IdPlayerA == p.Id || x.IdPlayerB == p.Id)).ToList();

            games.ForEach(x => x.SetCurrentPlayer = Player.Id);
            var allGames = games.Count * 1.0f;
            var gamesWonThreeZero = games.Count(x => x.SetsCurrentPlayer == 3 && x.SetsOppPlayer == 0);
            NumGamesWonThreeZeroText = $"{gamesWonThreeZero} ({((gamesWonThreeZero / allGames)).ToString("0 %")})";

            var gamesWonThreeOne = games.Count(x => x.SetsCurrentPlayer == 3 && x.SetsOppPlayer == 1);
            NumGamesWonThreeOneText = $"{gamesWonThreeOne} ({((gamesWonThreeOne / allGames)).ToString("0 %")})";

            var gamesWonThreeTwo = games.Count(x => x.SetsCurrentPlayer == 3 && x.SetsOppPlayer == 2);
            NumGamesWonThreeTwoText = $"{gamesWonThreeTwo} ({((gamesWonThreeTwo / allGames)).ToString("0 %")})";



            var gamesLostThreeZero = games.Count(x => x.SetsCurrentPlayer == 0 && x.SetsOppPlayer == 3);
            NumGamesLostThreeZeroText = $"{gamesLostThreeZero} ({((gamesLostThreeZero / allGames)).ToString("0 %")})";

            var gamesLostThreeOne = games.Count(x => x.SetsCurrentPlayer == 1 && x.SetsOppPlayer == 3);
            NumGamesLostThreeOneText = $"{gamesLostThreeOne} ({((gamesLostThreeOne / allGames)).ToString("0 %")})";


            var gamesLostThreeTwo = games.Count(x => x.SetsCurrentPlayer == 2 && x.SetsOppPlayer == 3);
            NumGamesLostThreeTwoText = $"{gamesLostThreeTwo} ({((gamesLostThreeTwo / allGames)).ToString("0 %")})";

            games = games.OrderByDescending(x => x.PointsCurrentPlayer).ToList();

            int numGames = 3;
            if (games.Count < numGames)
                numGames = games.Count;

            var all = games.Take(numGames).ToList();
            all.AddRange(games.Skip(games.Count - numGames).Take(numGames).Reverse());

            Results = new ObservableCollection<Game>(all);
        }
        private string numGamesWonThreeZeroText;
        public string NumGamesWonThreeZeroText
        {
            get { return numGamesWonThreeZeroText; }
            set { SetProperty<string>(ref numGamesWonThreeZeroText, value, nameof(NumGamesWonThreeZeroText)); }
        }


        private string numGamesWonThreeOneText;
        public string NumGamesWonThreeOneText
        {
            get { return numGamesWonThreeOneText; }
            set { SetProperty<string>(ref numGamesWonThreeOneText, value, nameof(NumGamesWonThreeOneText)); }
        }


        private string numGamesWonThreeTwoText;
        public string NumGamesWonThreeTwoText
        {
            get { return numGamesWonThreeTwoText; }
            set { SetProperty<string>(ref numGamesWonThreeTwoText, value, nameof(NumGamesWonThreeTwoText)); }
        }


        private string numGamesLostThreeZeroText;
        public string NumGamesLostThreeZeroText
        {
            get { return numGamesLostThreeZeroText; }
            set { SetProperty<string>(ref numGamesLostThreeZeroText, value, nameof(NumGamesLostThreeZeroText)); }
        }


        private string numGamesLostThreeOneText;
        public string NumGamesLostThreeOneText
        {
            get { return numGamesLostThreeOneText; }
            set { SetProperty<string>(ref numGamesLostThreeOneText, value, nameof(NumGamesLostThreeOneText)); }
        }


        private string numGamesLostThreeTwoText;
        public string NumGamesLostThreeTwoText
        {
            get { return numGamesLostThreeTwoText; }
            set { SetProperty<string>(ref numGamesLostThreeTwoText, value, nameof(NumGamesLostThreeTwoText)); }
        }
        private ObservableCollection<Game> results = new ObservableCollection<Game>();

        public ObservableCollection<Game> Results
        {
            get { return results; }
            set { SetProperty(ref results, value); }
        }

    }
}