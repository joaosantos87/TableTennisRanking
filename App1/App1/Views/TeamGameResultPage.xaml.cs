﻿using App1.ViewModels;
using DatabaseNetStandard.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App1.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TeamGameResultPage : ContentPage
    {
        public TeamGameResultPage()
        {
            InitializeComponent();
            Title = "Jogo de equipa";
        }

        public void SetTeamGame(TeamGame tg)
        {
            (BindingContext as TeamGameResultViewModel).SetTeamGame(tg);
        }
    }
}