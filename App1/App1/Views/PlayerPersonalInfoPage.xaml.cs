﻿using App1.ViewModels;
using DatabaseNetStandard.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App1.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PlayerPersonalInfoPage : ContentPage
    {
        public PlayerPersonalInfoPage()
        {
            InitializeComponent();
            Title = "Info";
        }

        public void SetPlayer(Player p)
        {
            (BindingContext as PlayerPersonalInfoViewModel).DefinePlayer(p);
        }
    }
}