﻿using App1.ViewModels;
using DatabaseNetStandard.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App1.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ClubVsClubPage : ContentPage
    {
        public ClubVsClubPage()
        {
            InitializeComponent();
            Title = "Clube VS Clube";
        }

        public void SetClubs(Club c1, Club c2)
        {
            (BindingContext as ClubVsClubViewModel).DefineClubs(c1, c2);
        }
    }
}