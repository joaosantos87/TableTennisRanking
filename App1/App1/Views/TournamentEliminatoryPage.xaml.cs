﻿using App1.ViewModels;
using DatabaseNetStandard.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App1.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TournamentEliminatoryPage : ContentPage
    {
        public TournamentEliminatoryPage()
        {
            InitializeComponent();
            Title = "Eliminatorias";
        }

        public void SetChampionship(Championship c)
        {
            (BindingContext as TournamentEliminatoryViewModel).DefineChampionship(c);
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            seasonPicker.SelectedIndex = -1;
        }

        private void Button1_Clicked(object sender, EventArgs e)
        {
            phasePicker.SelectedIndex = -1;
        }
    }
}