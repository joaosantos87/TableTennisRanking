﻿using App1.ViewModels;
using DatabaseNetStandard.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App1.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PlayerGamesPage : ContentPage
    {
        public PlayerGamesPage()
        {
            InitializeComponent();
            Title = "Jogos";
        }

        public void SetPlayer(Player p)
        {
            (BindingContext as PlayerGamesViewModel).DefinePlayer(p);
        }

        private void filterBtn_Clicked(object sender, EventArgs e)
        {
            (BindingContext as PlayerGamesViewModel).FilterVisible = !(BindingContext as PlayerGamesViewModel).FilterVisible;
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            seasonPicker.SelectedIndex = -1;
        }

        private void Button_Clicked_1(object sender, EventArgs e)
        {
            champPicker.SelectedIndex = -1;
        }

        private void advBtn_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if(sender is Button)
            {
                var btn = (Button)sender;
                if (btn.Text == null)
                    return;
                
                
            }
            if(sender is Label)
            {
                var btn = (Label)sender; 
                if (btn.Text == null)
                    return;
                else
                {
                    btn.MinimumHeightRequest = btn.MinimumHeightRequest;
                }
            }
        }
        int id = 0;
        Season selectedSeason;
        Championship selectedChampionship;
        private void ViewCell_BindingContextChanged(object sender, EventArgs e)
        {
            if ((BindingContext as PlayerGamesViewModel).SelectedSeason != selectedSeason)
            {
                id = 0;
                selectedSeason = (BindingContext as PlayerGamesViewModel).SelectedSeason;
            }

            if ((BindingContext as PlayerGamesViewModel).SelectedChampionship != selectedChampionship)
            {
                id = 0;
                selectedChampionship = (BindingContext as PlayerGamesViewModel).SelectedChampionship;
            }

            var viewCell = sender as ViewCell;

            if (id < (BindingContext as PlayerGamesViewModel).Results.Count)
            {

                if ((BindingContext as PlayerGamesViewModel).Results[id].IsDouble)
                    viewCell.Height = viewCell.RenderHeight + 30;

                id++;
            }

            //// Roughly calculate the height based on the number of chars
            //if (MultinlineLabel.Length > 75)
            //    this.Height = 110;
            //else if (MultinlineLabel.Length > 60)
            //    this.Height = 80;
            //else if (MultinlineLabel.Length > 30)
            //    this.Height = 60;
            //else
            //    this.Height = 40;
        }
    }
}