﻿using App1.ViewModels;
using DatabaseNetStandard.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App1.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ChampionshipTablePage : ContentPage
    {
        public ChampionshipTablePage()
        {
            InitializeComponent();
            Title = "Tabela";
        }

        public void SetChampionship(Championship c)
        {
            (BindingContext as ChampionshipTableViewModel).DefineChampionship(c);
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            seasonPicker.SelectedIndex = -1;
        }
    }
}