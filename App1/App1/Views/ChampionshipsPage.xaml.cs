﻿using App1.ViewModels;
using DatabaseNetStandard.Data;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App1.Views
{
    public partial class ChampionshipsPage : ContentPage
    {
        public ChampionshipsPage()
        {
            InitializeComponent();
            (BindingContext as ChampionshipsViewModel).SetIsYearlyChampionshipType = true;
        }
    }
}