﻿using App1.ViewModels;
using DatabaseNetStandard.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App1.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ChampionshipGamesPage : ContentPage
    {
        public ChampionshipGamesPage()
        {
            InitializeComponent();
            Title = "Jornadas";
        }

        public void SetChampionship(Championship c)
        {
            (BindingContext as ChampionshipGamesViewModel).DefineChampionship(c);
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            seasonPicker.SelectedIndex = -1;
        }
    }
}