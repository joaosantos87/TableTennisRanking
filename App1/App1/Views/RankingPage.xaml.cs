﻿using App1.ViewModels;
using DatabaseNetStandard.Data;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App1.Views
{
    public partial class RankingPage : ContentPage
    {
        public RankingPage()
        {
            InitializeComponent();
            (BindingContext as RankingViewModel).FillRanking(Database.Instance.seasons.Single(x => x.Name.ToLower() == "todasepocas"),
            Database.Instance.championships.Single(x => x.Name.ToLower() == "todoscampeonatos"));
        }

        public void FillRanking(Season season, Championship championship)
        {
            (BindingContext as RankingViewModel).FillRanking(season, championship);
        }

        private void jogadoresView_ItemTapped(object sender, ItemTappedEventArgs e)
        {

        }

        private void filterBtn_Clicked(object sender, EventArgs e)
        {
            (BindingContext as RankingViewModel).FilterVisible = !(BindingContext as RankingViewModel).FilterVisible;
        }

        private void Button_Clicked(object sender, EventArgs e)
        {

        }
    }
}