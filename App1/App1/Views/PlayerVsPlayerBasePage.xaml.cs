﻿using App1.ViewModels;
using DatabaseNetStandard.Data;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App1.Views
{
    public partial class PlayerVsPlayerBasePage : ContentPage
    {
        public PlayerVsPlayerBasePage()
        {
            InitializeComponent();
        }

        public void SetPlayer(Player p)
        {
            (BindingContext as PlayerVsPlayerBaseViewModel).DefinePlayer(p);
        }

        private void Entry_Completed(object sender, EventArgs e)
        {
            //string input = search.Text;
            //jogadoresView.ItemsSource = SearchName(input).OrderByDescending(x => x.CurrentPoints);
        }

        //public ObservableCollection<Player> SearchName(string input)
        //{
        //    return new ObservableCollection<Player>(Database.Instance.players.Where(x => x.Name.ToLower().Contains(input)));
        //}

        private void jogadoresView_ItemTapped(object sender, ItemTappedEventArgs e)
        {

        }
    }
}