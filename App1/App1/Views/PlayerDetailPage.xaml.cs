﻿using App1.ViewModels;
using DatabaseNetStandard.Data;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App1.Views
{
    public partial class PlayerDetailPage : ContentPage
    {
        public PlayerDetailPage(Player p)
        {
            InitializeComponent();
            (BindingContext as PlayerDetailViewModel).DefinePlayer(p);
            Title = p.NameAutomaticInsertion != "" ? p.NameAutomaticInsertion : p.Name;
            //(BindingContext as PlayerDetailViewModel).FilterVisible = true;
        }

        private void DatePicker_DateSelected(object sender, DateChangedEventArgs e)
        {
            //(BindingContext as PlayerDetailViewModel).StartDate = e.NewDate;
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            (BindingContext as PlayerDetailViewModel).PersonalInfoVisible = !(BindingContext as PlayerDetailViewModel).PersonalInfoVisible;
        }

        private void filterBtn_Clicked(object sender, EventArgs e)
        {
            (BindingContext as PlayerDetailViewModel).FilterVisible = !(BindingContext as PlayerDetailViewModel).FilterVisible;
        }

        private void Button_Clicked_1(object sender, EventArgs e)
        {
            (BindingContext as PlayerDetailViewModel).GamesHistoricVisible = !(BindingContext as PlayerDetailViewModel).GamesHistoricVisible;
        }

        private void Button_Clicked_2(object sender, EventArgs e)
        {

            (BindingContext as PlayerDetailViewModel).SeasonsHistoricVisible = !(BindingContext as PlayerDetailViewModel).SeasonsHistoricVisible;
        }


        //protected override void OnCreate(Bundle savedInstanceState)
        //{
        //    base.OnCreate(savedInstanceState);
        //    Xamarin.Essentials.Platform.Init(this, savedInstanceState);

        //    v1 = this.LayoutInflater.Inflate(Resource.Layout.content_main, null);
        //    Button button = v1.FindViewById<Button>(Resource.Id.button1);
        //    button.Click += Button_Click;


        //    v2 = this.LayoutInflater.Inflate(Resource.Layout.XMLFile1, null);
        //    Button button2 = v2.FindViewById<Button>(Resource.Id.button2);
        //    button2.Click += Button2_Click;

        //    SetContentView(v1);

        //}

    }
}