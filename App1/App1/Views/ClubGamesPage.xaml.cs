﻿using App1.ViewModels;
using DatabaseNetStandard.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App1.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ClubGamesPage : ContentPage
    {
        public ClubGamesPage()
        {
            InitializeComponent();
            Title = "Jogos";
        }

        public void SetClub(Club c)
        {
            (BindingContext as ClubGamesViewModel).DefineClub(c);
        }

        private void filterBtn_Clicked(object sender, EventArgs e)
        {
            (BindingContext as ClubGamesViewModel).FilterVisible = !(BindingContext as ClubGamesViewModel).FilterVisible;
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            seasonPicker.SelectedIndex = -1;
        }

        private void Button_Clicked_1(object sender, EventArgs e)
        {
            champPicker.SelectedIndex = -1;
        }
    }
}