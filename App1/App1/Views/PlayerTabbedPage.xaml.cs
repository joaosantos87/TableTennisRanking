﻿using DatabaseNetStandard.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App1.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PlayerTabbedPage : TabbedPage
    {
        public PlayerTabbedPage(Player p)
        {
            InitializeComponent();
            Title = p.NameAutomaticInsertion != "" ? p.NameAutomaticInsertion : p.Name;
            playerPersonalInfo.SetPlayer(p);
            playerRating.SetPlayer(p);
            playerHistoric.SetPlayer(p);
            playerGames.SetPlayer(p);
            PlayerVsPlayerBase.SetPlayer(p);
        }
    }
}