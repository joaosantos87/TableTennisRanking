﻿using DatabaseNetStandard.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App1.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TournamentTabbedPage : TabbedPage
    {
        public TournamentTabbedPage(Championship c)
        {
            InitializeComponent();
            Title = c.NameAutomaticInsertion != "" ? c.NameAutomaticInsertion : c.Name;

            tournamentInfoPage.SetChampionship(c);
            tournamentGroupStagePage.SetChampionship(c);
            tournamentEliminatoryPage.SetChampionship(c);
        }
    }
}