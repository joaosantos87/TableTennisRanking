﻿using App1.ViewModels;
using DatabaseNetStandard.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App1.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ClubPlayersHistoricPage : ContentPage
    {
        public ClubPlayersHistoricPage()
        {
            InitializeComponent();
            Title = "Jogadores";
        }

        public void SetClub(Club c)
        {
            (BindingContext as ClubPlayersHistoricViewModel).DefineClub(c);
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            seasonPicker.SelectedIndex = -1;
        }

        private void Button_Clicked_1(object sender, EventArgs e)
        {
            champPicker.SelectedIndex = -1;
        }
    }
}