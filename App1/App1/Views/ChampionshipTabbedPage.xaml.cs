﻿using DatabaseNetStandard.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App1.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ChampionshipTabbedPage : TabbedPage
    {
        public ChampionshipTabbedPage(Championship c)
        {
            InitializeComponent();
            Title = c.NameAutomaticInsertion != "" ? c.NameAutomaticInsertion : c.Name;
            
            championshipInfoPage.SetChampionship(c);
            championshipTablePage.SetChampionship(c);
            championshipGamesPage.SetChampionship(c);
        }
    }
}