﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TenisMesaBaseDados.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TenisMesaBaseDados.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SplashPage : ContentPage
    {
        public static event EventHandler DatabaseLoaded;
        public SplashPage()
        {
            InitializeComponent();
            BindingContext = new SplashViewModel();
        }

        public void LoadDatabase()
        {
            (BindingContext as SplashViewModel).LoadDatabase();
            DatabaseLoaded?.Invoke(this, new EventArgs());
        }

    }
}