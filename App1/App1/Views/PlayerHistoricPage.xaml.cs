﻿using App1.ViewModels;
using DatabaseNetStandard.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App1.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PlayerHistoricPage : ContentPage
    {
        public PlayerHistoricPage()
        {
            InitializeComponent();
            Title = "Historico";
        }

        public void SetPlayer(Player p)
        {
            (BindingContext as PlayerHistoricViewModel).DefinePlayer(p);
        }
    }
}