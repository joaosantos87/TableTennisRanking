﻿using App1.ViewModels;
using DatabaseNetStandard.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App1.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PlayerVsPlayerPage : ContentPage
    {
        public PlayerVsPlayerPage()
        {
            InitializeComponent();
            Title = "Jogador VS Jogador";
        }

        public void SetPlayers(Player p1, Player p2)
        {
            (BindingContext as PlayerVsPlayerViewModel).DefinePlayers(p1, p2);
        }
    }
}