﻿using App1.ViewModels;
using DatabaseNetStandard.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App1.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PlayerRatingPage : ContentPage
    {
        public PlayerRatingPage()
        {
            InitializeComponent();
            Title = "Rating";
        }

        public void SetPlayer(Player p)
        {
            (BindingContext as PlayerRatingViewModel).DefinePlayer(p);
        }
    }
}