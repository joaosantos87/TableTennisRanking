﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Windows.Forms;
using TableTennis.Database;
using static TableTennis.Database.DataSet2;

namespace TableTennis
{
    public static class Utilities
    {
        public static seasonRow EpocaActual
        {
            get
            {
                var seasons = FilesAccess.db.seasons.OrderByDescending(x => x.endDate);
                foreach (var s in seasons)
                {
                    if (s.name == "TodasEpocas")
                        continue;

                    if (s.endDate > DateTime.Now && s.startDate < DateTime.Now)
                    {
                        return s;
                    }
                }
                return null;
            }
        }
        public static seasonRow TodasEpocas
        {
            get { return FilesAccess.db.seasons.Single(x => x.name == "TodasEpocas"); }
        }
        public static championshipsRow TodosCampeonatos
        {
            get { return FilesAccess.db.championships.Single(x => x.Name == "TodosCampeonatos"); }
        }

        public static phaseRow Phase_NA
        {

            get { return FilesAccess.db.phase.Single(x => x.phase == "N/A"); }
        }

        public static Tuple<bool, bool> IsSimilarName(string name1, string name2)
        {
            return new Tuple<bool, bool>(false, false);
        }

        /// <summary>
        /// Compute the distance between two strings.
        /// </summary>
        public static int LevenshteinDistance(string s, string t)
        {
            s = s.ToUpper();
            t = t.ToUpper();
            int n = s.Length;
            int m = t.Length;
            int[,] d = new int[n + 1, m + 1];

            // Step 1
            if (n == 0)
            {
                return m;
            }

            if (m == 0)
            {
                return n;
            }

            // Step 2
            for (int i = 0; i <= n; d[i, 0] = i++)
            {
            }

            for (int j = 0; j <= m; d[0, j] = j++)
            {
            }

            // Step 3
            for (int i = 1; i <= n; i++)
            {
                //Step 4
                for (int j = 1; j <= m; j++)
                {
                    // Step 5
                    int cost = (t[j - 1] == s[i - 1]) ? 0 : 1;

                    // Step 6
                    d[i, j] = Math.Min(
                        Math.Min(d[i - 1, j] + 1, d[i, j - 1] + 1),
                        d[i - 1, j - 1] + cost);
                }
            }
            // Step 7
            return d[n, m];
        }

        public static bool ContainsWord(string s, string t)
        {
            //have to improve to check if the two words are contained

            s = s.ToUpper();
            t = t.ToUpper();
            string[] newNameArray = s.Split(' ');
            string[] nameInListArray = t.Split(' ');

            for (int i = 0; i < newNameArray.Length; i++)
            {
                if (t.Contains(newNameArray[i]))
                    return true;
            }

            for (int i = 0; i < nameInListArray.Length; i++)
            {
                if (s.Contains(nameInListArray[i]))
                    return true;
            }

            return false;
        }

        public static int ByePlayer
        {
            get { return FilesAccess.db.player.Single(x => x.Name.ToLower() == "bye").Id; }
        }

        public static int FCPlayer
        {
            get 
            {
                if(FilesAccess.db.player.Count(x => x.Name.ToLower() == "fc") > 0)
                    return FilesAccess.db.player.Single(x => x.Name.ToLower() == "fc").Id;

                return ByePlayer;
            }
        }

        public static int ByeClub
        {
            get { return FilesAccess.db.clubs.Single(x => x.Name == "Bye").Id; }
        }

        public static int PlayerToBeDefined
        {
            get { return FilesAccess.db.player.Single(x => x.Name.Contains("TBD")).Id; }
        }

        public static int OpenTournamentId
        {
            get { return FilesAccess.db.tournamentType.Single(x => x.name == "Torneio Aberto").id; }
        }

        public static List<int> AllOpenTournaments
        {
            get 
            {
                List<championshipsRow> champsRows = ((championshipsRow[])FilesAccess.db.championships.Select("idTournamentType=" + OpenTournamentId)).ToList();
                List<int> output = new List<int>();
                for(int i=0; i<champsRows.Count;i++)
                    output.Add(champsRows[i].Id);
                return output;
            }
        }

        public static bool IsChampOpenTournament(List<int> champs)
        {
            if (champs == null)
                return false;

            for (int i = 0; i < champs.Count; i++)
            {
                if (AllOpenTournaments.Contains(champs[i]))
                    return true;
            }
            return false;
        }

        public static int OpenLevelId
        {
            get { return FilesAccess.db.levels.Single(x => x.Name == "Seniores").id; }
        }

        public static int Phase_Group
        {
            get { return FilesAccess.db.phase.Single(x => x.phase == "Fase Grupos").id; }
        }

        public static string GetResult(this teamgameRow row)
        {
            var countA = GetScoreA(row);
            var countB = GetScoreB(row);

            if (countA > countB)
                return $"{countA} - {countB}";
            else
                return $"{countB} - {countA}";
        }

        public static string GetResult(this teamgameRow row, int currentTeam)
        {
            var countA = GetScoreA(row);
            var countB = GetScoreB(row);
            
            if (row.ClubAId == currentTeam)
                return $"{countA} - {countB}";
            else
                return $"{countB} - {countA}";
        }

        public static int GetScoreA(this teamgameRow row)
        {
            var gamesSingle = FilesAccess.db.games.Where(x => x.IdTeamGame == row.Id);
            var gamesDouble = FilesAccess.db.doubleGames.Where(x => x.IdTeamGame == row.Id);

            int countA = gamesSingle.Count(x => x.IdPlayerA == x.IdPlayerWinner) +
                    gamesDouble.Count(x =>
                        x.DoubleA_PlayerAId == x.IdPlayerAWinner || x.DoubleA_PlayerBId == x.IdPlayerAWinner ||
                        x.DoubleA_PlayerAId == x.IdPlayerBWinner || x.DoubleA_PlayerBId == x.IdPlayerBWinner);
            return countA;
        }

        public static int GetScoreB(this teamgameRow row)
        {
            var gamesSingle = FilesAccess.db.games.Where(x => x.IdTeamGame == row.Id);
            var gamesDouble = FilesAccess.db.doubleGames.Where(x => x.IdTeamGame == row.Id);

            int countB = gamesSingle.Count(x => x.IdPlayerB == x.IdPlayerWinner) +
                    gamesDouble.Count(x =>
                        x.DoubleB_PlayerAId == x.IdPlayerAWinner || x.DoubleB_PlayerBId == x.IdPlayerAWinner ||
                        x.DoubleB_PlayerAId == x.IdPlayerBWinner || x.DoubleB_PlayerBId == x.IdPlayerBWinner);
                        
            return countB;
        }

        public static int GetWinner(this teamgameRow row)
        {
            var countA = GetScoreA(row);
            var countB = GetScoreB(row);

            return countA > countB ? row.ClubAId : countB > countA ? row.ClubBId : 0;
        }
        
        public static int DoubleNumberGamesTogether(int champId, int idA, int idB)
        {
            var games = FilesAccess.db.doubleGames.ToList();

            if (champId != FilesAccess.db.championships.Single(x => x.Name == "TodosCampeonatos").Id)
                games = FilesAccess.db.doubleGames.Where(x => x.IdChampionship == champId).ToList();

            var count = games.Count(x => 
                (x.DoubleA_PlayerAId == idA && x.DoubleA_PlayerBId == idB) ||
                (x.DoubleA_PlayerAId == idB && x.DoubleA_PlayerBId == idA) ||
                (x.DoubleB_PlayerAId == idA && x.DoubleB_PlayerBId == idB) ||
                (x.DoubleB_PlayerAId == idB && x.DoubleB_PlayerBId == idA));

            return count;
        }
        
        public static int DoubleNumberGamesTogetherWon(int champId, int idA, int idB)
        {
            var games = FilesAccess.db.doubleGames.ToList();

            if (champId != FilesAccess.db.championships.Single(x => x.Name == "TodosCampeonatos").Id)
                games = FilesAccess.db.doubleGames.Where(x => x.IdChampionship == champId).ToList();

            var count = games.Count(x =>
                (((x.DoubleA_PlayerAId == idA && x.DoubleA_PlayerBId == idB) || (x.DoubleA_PlayerAId == idB && x.DoubleA_PlayerBId == idA)) &&
                ((x.IdPlayerAWinner == x.DoubleA_PlayerAId && x.IdPlayerBWinner == x.DoubleA_PlayerBId) || (x.IdPlayerAWinner == x.DoubleA_PlayerBId && x.IdPlayerBWinner == x.DoubleA_PlayerAId))) ||
                ((x.DoubleB_PlayerAId == idA && x.DoubleB_PlayerBId == idB) || (x.DoubleB_PlayerAId == idB && x.DoubleB_PlayerBId == idA)) &&
                ((x.IdPlayerAWinner == x.DoubleB_PlayerAId && x.IdPlayerBWinner == x.DoubleB_PlayerBId) || (x.IdPlayerAWinner == x.DoubleB_PlayerBId && x.IdPlayerBWinner == x.DoubleB_PlayerAId)));

            return count;
        }

        public static int DoubleNumberGamesTogetherLost(int champId, int idA, int idB)
        {
            var games = FilesAccess.db.doubleGames.ToList();

            if (champId != FilesAccess.db.championships.Single(x => x.Name == "TodosCampeonatos").Id)
                games = FilesAccess.db.doubleGames.Where(x => x.IdChampionship == champId).ToList();

            var count = games.Count(x =>
                (((x.DoubleA_PlayerAId == idA && x.DoubleA_PlayerBId == idB) || (x.DoubleA_PlayerAId == idB && x.DoubleA_PlayerBId == idA)) &&
                ((x.IdPlayerAWinner == x.DoubleB_PlayerAId && x.IdPlayerBWinner == x.DoubleB_PlayerBId) || (x.IdPlayerAWinner == x.DoubleB_PlayerBId && x.IdPlayerBWinner == x.DoubleB_PlayerAId))) ||
                ((x.DoubleB_PlayerAId == idA && x.DoubleB_PlayerBId == idB) || (x.DoubleB_PlayerAId == idB && x.DoubleB_PlayerBId == idA)) &&
                ((x.IdPlayerAWinner == x.DoubleA_PlayerAId && x.IdPlayerBWinner == x.DoubleA_PlayerBId) || (x.IdPlayerAWinner == x.DoubleA_PlayerBId && x.IdPlayerBWinner == x.DoubleA_PlayerAId)));

            return count;
        }


        public static int DoubleNumberGames(int champId, int idA)
        {
            var games = FilesAccess.db.doubleGames.ToList();

            if (champId != FilesAccess.db.championships.Single(x => x.Name == "TodosCampeonatos").Id)
                games = FilesAccess.db.doubleGames.Where(x => x.IdChampionship == champId).ToList();

            var count = games.Count(x =>
                (x.DoubleA_PlayerAId == idA || x.DoubleA_PlayerBId == idA) ||
                (x.DoubleB_PlayerAId == idA || x.DoubleB_PlayerBId == idA));

            return count;
        }

        public static int DoubleNumberGamesWon(int champId, int idA)
        {
            var games = FilesAccess.db.doubleGames.ToList();

            if (champId != FilesAccess.db.championships.Single(x => x.Name == "TodosCampeonatos").Id)
                games = FilesAccess.db.doubleGames.Where(x => x.IdChampionship == champId).ToList();

            var count = games.Count(x =>
                ((x.DoubleA_PlayerAId == idA || x.DoubleA_PlayerBId == idA) && (x.IdPlayerAWinner==idA || x.IdPlayerBWinner==idA)) ||
                ((x.DoubleB_PlayerAId == idA || x.DoubleB_PlayerBId == idA) && (x.IdPlayerBWinner == idA || x.IdPlayerBWinner == idA)));

            return count;
        }

        public static int DoubleNumberGamesLost(int champId, int idA)
        {
            var games = FilesAccess.db.doubleGames.ToList();

            if (champId != FilesAccess.db.championships.Single(x => x.Name == "TodosCampeonatos").Id)
                games = FilesAccess.db.doubleGames.Where(x => x.IdChampionship == champId).ToList();

            var count = games.Count(x =>
                ((x.DoubleA_PlayerAId == idA || x.DoubleA_PlayerBId == idA) && (x.IdPlayerAWinner != idA && x.IdPlayerBWinner != idA)) ||
                ((x.DoubleB_PlayerAId == idA || x.DoubleB_PlayerBId == idA) && (x.IdPlayerBWinner != idA && x.IdPlayerBWinner != idA)));

            return count;
        }

        public static List<doublegamesRow> DoublesGamesTogether(int champId, int idA, int idB)
        {
            var games = FilesAccess.db.doubleGames.ToList();

            if (champId != FilesAccess.db.championships.Single(x => x.Name == "TodosCampeonatos").Id)
                games = FilesAccess.db.doubleGames.Where(x => x.IdChampionship == champId).ToList();
            
            return DoublesGamesTogether(games, idA, idB);
        }

        public static List<doublegamesRow> DoublesGamesTogether(IEnumerable<doublegamesRow> data, int idA, int idB)
        {
            return data.Where(x =>
                    (x.DoubleA_PlayerAId == idA && x.DoubleA_PlayerBId == idB) || (x.DoubleA_PlayerAId == idB && x.DoubleA_PlayerBId == idA) ||
                    (x.DoubleB_PlayerAId == idA && x.DoubleB_PlayerBId == idB) || (x.DoubleB_PlayerAId == idB && x.DoubleB_PlayerBId == idA)).ToList();
        }

        public static int GetRankPoints(rankingRow rank)
        {
            if (rank != null)
                return rank.points;
            return 0;
        }
        public static int GetRankPlace(rankingRow rank)
        {
            if (rank != null)
                return rank.rank;
            return 0;
        }

    }
}
