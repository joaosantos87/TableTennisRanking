﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace TableTennis.Reports
{
    public class PdfPlayerGames : PdfReport
    {
        public PdfPlayerGames(string title, List<IPlayerGameReport> playerGames)
        {
            if (!Directory.Exists("PlayerGames"))
                Directory.CreateDirectory("PlayerGames");

            string filename = title;
            if (title.Count() > 248)
                filename = title.Split(' ')[0];

            filename += "_" + DateTime.Now.ToString("[yyyy-MM-dd_HH-mm-ss]");
            string path = "PlayerGames" + Path.DirectorySeparatorChar + filename + ".pdf";

            CreateDocument(path, title, playerGames[0].WidthSize);

            InsertRow(playerGames[0].Header, P3b);

            for (int i = 0; i < playerGames.Count; i++)
                InsertRow(playerGames[i].Data, P3, new BaseColor(playerGames[i].Color));

            Close();
        }
    }

    public class PlayerGame : IPlayerGameReport
    {
        public override string[] Data
        {
            get { return new string[] { date,championship, opponent, result, points }; }
        }

        public override string[] Header
        {
            get { return new string[] { "Date", "Championship", "Opponent", "Result", "Rating Points" }; }
        }

        public override int[] WidthSize
        {
            get { return new int[] { 100, 210, 210, 70, 70 }; }
        }

        public PlayerGame(object _date, object _name, object _championship, object _opponent, object _result, object _points, System.Drawing.Color _color)
        {
            date = Convert.ToDateTime(_date).ToString("dd/MM/yyyy");
            name = _name.ToString();
            championship = _championship.ToString();
            opponent = _opponent.ToString();
            result = _result.ToString();
            points = _points.ToString();
            color = _color;
        }
    }

    public class PlayerTournamentGame : IPlayerGameReport
    {
        public override string[] Data
        {
            get { return new string[] { date, championship, result, pointsA, pointsB }; }
        }

        public override string[] Header
        {
            get { return new string[] { "Data", "Torneio", "Resultado", "Pts A", "Pts B" }; }
        }

        public override int[] WidthSize
        {
            get { return new int[] { 100, 100, 290, 50, 50 }; }
        }

        public PlayerTournamentGame(object _date, object _championship, object _result, object _pointsA, object _pointsB, System.Drawing.Color _color)
        {
            date = Convert.ToDateTime(_date).ToString("dd/MM/yyyy");
            championship = _championship.ToString();
            result = _result.ToString();
            pointsA = _pointsA.ToString();
            pointsB = _pointsB.ToString();
            color = _color;
        }
    }

    public abstract class IPlayerGameReport
    {
        public string date;
        public string name;
        public string championship;
        public string opponent;
        public string result;
        public string points;
        public string pointsA;
        public string pointsB;
        public System.Drawing.Color color;

        public System.Drawing.Color Color { get { return color; } }

        public abstract string[] Data { get; }
        public abstract string[] Header { get; }
        public abstract int[] WidthSize { get; }
    }
}
