﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using TableTennis.Database;
using static TableTennis.Database.DataSet2;

namespace TableTennis.Reports
{
    public sealed class PdfRanking : PdfReport
    {
        string title;
        List<RankPlayer> rankPlayers;

        public PdfRanking(string title, List<RankPlayer> rankPlayer, List<Tuple<DateTime, championshipsRow>> allTournaments)
        {
            this.title = title;
            this.rankPlayers = rankPlayer;
            
            CreateDocument();
            
            InsertRow(rankPlayers[0].Header, P3b);

            for (int i = 0; i < rankPlayers.Count; i++)
                InsertRow(rankPlayers[i].Data, P3);

            //AddParagraph("\nProvas incluidas neste ranking:\n");

            //InsertRow(new string[] { "Data", "Torneio" }, P3b, columnsWidth: new int[] { 80, 300 });

            //for (int i = 0; i < allTournaments.Count; i++)
            //{
            //    InsertRow(
            //        new string[]{ allTournaments[i].Item1.ToShortDateString(), 
            //            allTournaments[i].Item2.Name},
            //            P3);
            //}
            Close();

        }

        public void CreateDocument()
        {
            if (!Directory.Exists("Reports"))
                Directory.CreateDirectory("Ranking");

            string filename = title;
            if (title.Count() > 248)
                filename = title.Split(' ')[0];

            filename += "_" + DateTime.Now.ToString("[yyyy-MM-dd_HH-mm-ss]");
            string path = "Ranking" + Path.DirectorySeparatorChar + filename + ".pdf";

            CreateDocument(path, "Ranking - " + title, rankPlayers[0].WidthSize);
        }
    }

    public class RankPlayer
    {
        string name;
        string licence;
        string club;
        string points;
        string rankingGlobal;
        string rankingLocal;
        string association;
        string gamesWon;
        string gamesLost;

        string numGames
        {
            get { return (GamesWon + GamesLost).ToString(); }
        }

        public string[] Data
        {
            get { return new string[] { rankingLocal + " [" + rankingGlobal + "]", licence, name, club, points, GamesWon+" - "+GamesLost }; }
        }

        public int PPG
        {
            get { return (Convert.ToInt32(points) - 1000) / Convert.ToInt32(numGames); }
        }

        public int GamesWon
        {
            get { return Convert.ToInt32(gamesWon); }
        }

        public int GamesLost
        {
            get { return Convert.ToInt32(gamesLost); }
        }

        public string[] Header
        {
            get { return new string[] { "Ranking", "Licenca", "Nome",  "Clube", "Pontos Rating", "Vitorias-Derrotas" }; }
        }

        public int[] WidthSize
        {
            get
            {
                return new int[] { 80, 80, 250, 250, 80, 80 };
            }
        }

        public RankPlayer(object _ranking, object _rankingLocal, object licence, object _name, object _club, object _association, object _points, object _gamesWon, object _gamesLost)
        {
            name = _name.ToString();
            this.licence = licence.ToString();
            if(_club!=null)
                club = _club.ToString();
            else
                club = FilesAccess.db.clubs[0].Name;
            points = _points.ToString();
            gamesWon = _gamesWon.ToString();
            gamesLost = _gamesLost.ToString();
            rankingGlobal = _ranking.ToString();
            rankingLocal = _rankingLocal.ToString();
            association = _association.ToString();
        }
    }
}
