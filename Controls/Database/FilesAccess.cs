﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using System.Data;
using static TableTennis.Database.DataSet2;
using TableTennis.Database.DataSet2TableAdapters;
using System.Data.SqlClient;

namespace TableTennis.Database
{
    public class FilesAccess
    {
        private static FilesAccess _db;
        public static FilesAccess db
        {
            get
            {
                if (_db == null)
                {
                    _db = new FilesAccess();
                    MySqlDatabase.Instance.ToString();
                }
                return _db;
            }
        }

        public calculationsDataTable calculations = new calculationsDataTable();
        public championshipsDataTable championships = new championshipsDataTable();
        public clubsDataTable clubs = new clubsDataTable();
        public playerleaguetournamentDataTable playerLeagueTournament = new playerleaguetournamentDataTable();
        public playerclubsDataTable playerClubs = new playerclubsDataTable();
        public playerDataTable player = new playerDataTable();
        public pointsbreakdownDataTable pointsBreakDown = new pointsbreakdownDataTable();
        public ratingchartDataTable ratingChart = new ratingchartDataTable();
        public rankingDataTable ranking = new rankingDataTable();
        public associationsDataTable associations = new associationsDataTable();
        public levelsDataTable levels = new levelsDataTable();
        public tournamenttypeDataTable tournamentType = new tournamenttypeDataTable();
        public playerinscriptionleagueDataTable playerInscriptionLeague = new playerinscriptionleagueDataTable();
        public gamestyleDataTable gameStyle = new gamestyleDataTable();
        public handshakeDataTable handShake = new handshakeDataTable();
        public tournamentgamesDataTable tournamentGames = new tournamentgamesDataTable();
        public phaseDataTable phase = new phaseDataTable();
        public tournamentgroupDataTable tournamentGroup = new tournamentgroupDataTable();
        public playertourngroupDataTable playerTournGroup = new playertourngroupDataTable();
        public seasonDataTable seasons = new seasonDataTable();
        public playerclubseasonDataTable playerclubseason = new playerclubseasonDataTable();
        public gamesDataTable games = new gamesDataTable();
        public teamgameDataTable teamGames = new teamgameDataTable();
        public doublegamesDataTable doubleGames = new doublegamesDataTable();

        public void BackupDatabase()
        {
            if (!System.IO.Directory.Exists("DatabaseBackup"))
                System.IO.Directory.CreateDirectory("DatabaseBackup");

            string file = "";

            using (MySqlCommand cmd = new MySqlCommand())
            {
                using (MySqlBackup mb = new MySqlBackup(cmd))
                {
                    cmd.Connection = MySqlDatabase.Instance.Conn;
                    MySqlDatabase.Instance.Conn.Open();
                    file = @"DatabaseBackup/Backup" + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".sql";
                    mb.ExportToFile(file);
                    MySqlDatabase.Instance.Conn.Close();
                }
            }
        }

        public bool UpdatePlayerTournamentGroup()
        {
            try
            {
                string sQuery = "Select * from playerTournGroup";

                MySqlDataAdapter myDA = new MySqlDataAdapter(sQuery, MySqlDatabase.Instance.Conn);

                MySqlCommandBuilder cmb = new MySqlCommandBuilder(myDA);
                myDA.Update(playerTournGroup);
                return true;
            }
            catch { return false; }
        }

        public bool InsertTournamentGroup(tournamentgroupRow insert)
        {
            try
            {
                using (MySqlCommand command = new MySqlCommand())
                {
                    string sQuery = "INSERT INTO tournamentGroup (champId, groupId) VALUES        (@p1, @p2)";

                    command.Connection = MySqlDatabase.Instance.Conn;
                    command.CommandType = CommandType.Text;
                    command.CommandText = sQuery;
                    command.Parameters.AddWithValue("@p1", insert.champId);
                    command.Parameters.AddWithValue("@p2", insert.groupId);
                    MySqlDatabase.Instance.Conn.Open();
                    command.ExecuteNonQuery();

                    //TODO: Need to put this two lines in all the inserted rows;
                    FilesAccess.db.tournamentGroup.Last().id = Convert.ToInt32(command.LastInsertedId);
                    tournamentGroup.AcceptChanges();

                    MySqlDatabase.Instance.Conn.Close();
                    return true;
                }
            }
            catch{return false;}
        }
        
        internal void InsertRanking(rankingRow rankingRow)
        {
            rankingTableAdapter rankingDatatable = new rankingTableAdapter();
            
            rankingDatatable.Insert(
                rankingRow.idPlayer,
                rankingRow.idChamp,
                rankingRow.idSeason,
                rankingRow.numGames,
                rankingRow.numWins,
                rankingRow.numLosses,
                rankingRow.rank,
                rankingRow.points,
                rankingRow.idClub);
            
            rankingRow.id = (int)rankingDatatable.Adapter.InsertCommand.LastInsertedId;
        }

        internal void UpdateRanking()
        {
            string query = "SELECT * FROM ranking";
            var cmd = new MySqlCommand(query, MySqlDatabase.Instance.Conn);
            var returnVal = new MySqlDataAdapter(query, MySqlDatabase.Instance.Conn);
            returnVal.Fill(FilesAccess.db.ranking);
        }

        internal void UpdateRanking(rankingRow rankRow)
        {
            string sQuery = $"SELECT * FROM ranking WHERE id={rankRow.id}";
            MySqlDataAdapter myDA = new MySqlDataAdapter(sQuery, MySqlDatabase.Instance.Conn);
            MySqlCommandBuilder cmb = new MySqlCommandBuilder(myDA);
            myDA.Update(ranking);
        }

        internal void DeleteRanking()
        {
            MySqlCommand cmd = new MySqlCommand("DELETE FROM ranking", MySqlDatabase.Instance.Conn);
            MySqlDatabase.Instance.Conn.Open();
            cmd.ExecuteNonQuery();
            MySqlDatabase.Instance.Conn.Close();
            ranking.Clear();
        }

        internal void DeleteRanking(rankingRow rankRow)
        {
            MySqlCommand cmd = new MySqlCommand($"DELETE FROM ranking WHERE id = {rankRow.id}", MySqlDatabase.Instance.Conn);
            MySqlDatabase.Instance.Conn.Open();
            cmd.ExecuteNonQuery();
            MySqlDatabase.Instance.Conn.Close();
            ranking.RemoverankingRow(rankRow);
        }

        internal void InsertTeamGame(teamgameRow teamgameRow)
        {
            teamgameTableAdapter teamGameDatatable = new teamgameTableAdapter();
            teamGameDatatable.Insert(
                teamgameRow.Date,
                teamgameRow.IdChampionship,
                teamgameRow.Day,
                teamgameRow.ClubAId,
                teamgameRow.clubAnameAppend,
                teamgameRow.ClubBId,
                teamgameRow.clubBnameAppend);

            teamgameRow.Id = (int)teamGameDatatable.Adapter.InsertCommand.LastInsertedId;
        }

        internal void UpdateTeamGame(teamgameRow row)
        {
            string sQuery = $"SELECT * FROM teamgame WHERE id={row.Id}";
            MySqlDataAdapter myDA = new MySqlDataAdapter(sQuery, MySqlDatabase.Instance.Conn);
            MySqlCommandBuilder cmb = new MySqlCommandBuilder(myDA);
            myDA.Update(teamGames);
        }


        public bool UpdateTournamentGroup()
        {
            try
            {
                string sQuery = "Select * from tournamentGroup";

                MySqlDataAdapter myDA = new MySqlDataAdapter(sQuery, MySqlDatabase.Instance.Conn);

                MySqlCommandBuilder cmb = new MySqlCommandBuilder(myDA);
                myDA.Update(tournamentGroup);
                return true;
            }
            catch { return false; }
        }

        public bool InsertTournamentGame(tournamentgamesRow insert)
        {
            using (MySqlCommand command = new MySqlCommand())
            {
                string sQuery = "INSERT INTO tournamentGames (championshipId, roundId, Time, playerAid, playerBid, groupId, gameId, set0plA, set0plB, set1plA, set1plB, set2plA, set2plB, idPhase, dependPlA, dependPlB) VALUES        (@p1, @p2, @p3, @p4, @p5, @p6, @p7, @p8, @p9, @p10, @p11, @p12, @p13, @p14, @p15, @p16)";

                command.Connection = MySqlDatabase.Instance.Conn;
                command.CommandType = CommandType.Text;
                command.CommandText = sQuery;
                command.Parameters.AddWithValue("@p1", insert.championshipId);
                command.Parameters.AddWithValue("@p2", insert.roundId);
                command.Parameters.AddWithValue("@p3", insert.Time);
                command.Parameters.AddWithValue("@p4", insert.playerAid);
                command.Parameters.AddWithValue("@p5", insert.playerBid);
                command.Parameters.AddWithValue("@p6", insert.groupId);
                command.Parameters.AddWithValue("@p7", insert.gameId);
                command.Parameters.AddWithValue("@p8", insert.set0plA);
                command.Parameters.AddWithValue("@p9", insert.set0plB);
                command.Parameters.AddWithValue("@p10", insert.set1plA);
                command.Parameters.AddWithValue("@p11", insert.set1plB);
                command.Parameters.AddWithValue("@p12", insert.set2plA);
                command.Parameters.AddWithValue("@p13", insert.set2plB);
                command.Parameters.AddWithValue("@p14", insert.idPhase);
                command.Parameters.AddWithValue("@p15", insert.dependPlA);
                command.Parameters.AddWithValue("@p16", insert.dependPlB);
                MySqlDatabase.Instance.Conn.Open();
                command.ExecuteNonQuery();

                //TODO: Need to put this two lines in all the inserted rows;
                FilesAccess.db.tournamentGames.Last().id = Convert.ToInt32(command.LastInsertedId);
                tournamentGames.AcceptChanges();

                MySqlDatabase.Instance.Conn.Close();
                return true;
            }
            return false;
        }

        public bool UpdateTournamentGames()
        {
            try
            {
                string sQuery = "Select * from tournamentGames";

                MySqlDataAdapter myDA = new MySqlDataAdapter(sQuery, MySqlDatabase.Instance.Conn);

                MySqlCommandBuilder cmb = new MySqlCommandBuilder(myDA);
                myDA.Update(tournamentGames);
                return true;
            }
            catch { return false; }
        }

        public bool InsertTournamentInscription(playerinscriptionleagueRow insert)
        {
            using (MySqlCommand command = new MySqlCommand())
            {
                string sQuery = "INSERT INTO PlayerInscriptionLeague (playerId, email, gameStyleId, handshakeId, racketwood, racketFH, racketBH, currentPoints, currentGamesWon, currentRank, currentGamesLost) VALUES        (@p1, @p2, @p3, @p4, @p5, @p6, @p7, @p8, @p9, @p10, @p11)";

                command.Connection = MySqlDatabase.Instance.Conn;
                command.CommandType = CommandType.Text;
                command.CommandText = sQuery;
                command.Parameters.AddWithValue("@p1", insert.playerId);
                command.Parameters.AddWithValue("@p2", insert.email);
                command.Parameters.AddWithValue("@p3", insert.gameStyleId);
                command.Parameters.AddWithValue("@p4", insert.handshakeId);
                command.Parameters.AddWithValue("@p5", insert.racketwood);
                command.Parameters.AddWithValue("@p6", insert.racketFH);
                command.Parameters.AddWithValue("@p7", insert.racketBH);
                command.Parameters.AddWithValue("@p8", insert.currentPoints);
                command.Parameters.AddWithValue("@p9", insert.currentGamesWon);
                command.Parameters.AddWithValue("@p10", insert.currentRank);
                command.Parameters.AddWithValue("@p11", insert.currentGamesLost);
                MySqlDatabase.Instance.Conn.Open();
                command.ExecuteNonQuery();

                //TODO: Need to put this two lines in all the inserted rows;
                FilesAccess.db.playerInscriptionLeague.Last().id = Convert.ToInt32(command.LastInsertedId);
                playerInscriptionLeague.AcceptChanges();

                MySqlDatabase.Instance.Conn.Close();
                return true;
            }
            return false;
        }

        public bool UpdateLeagueInscription()
        {
            try
            {
                string sQuery = "Select * from PlayerInscriptionLeague";

                MySqlDataAdapter myDA = new MySqlDataAdapter(sQuery, MySqlDatabase.Instance.Conn);

                MySqlCommandBuilder cmb = new MySqlCommandBuilder(myDA);
                myDA.Update(playerInscriptionLeague);
                return true;
            }
            catch { return false; }
        }

        public bool InsertPlayerChamp(playerleaguetournamentRow insert)
        {
            using (MySqlCommand command = new MySqlCommand())
            {
                string sQuery = "INSERT INTO PlayerLeagueTournament (playerId, championshipId, confirmInscription,currentPoints, currentGamesWon, currentRank, currentGamesLost) VALUES        (@p1, @p2, @p3, @p4, @p5, @p6, @p7)";

                command.Connection = MySqlDatabase.Instance.Conn;
                command.CommandType = CommandType.Text;
                command.CommandText = sQuery;
                command.Parameters.AddWithValue("@p1", insert.playerId);
                command.Parameters.AddWithValue("@p2", insert.championshipId);
                command.Parameters.AddWithValue("@p3", insert.confirmInscription);
                command.Parameters.AddWithValue("@p4", insert.currentPoints);
                command.Parameters.AddWithValue("@p5", insert.currentGamesWon);
                command.Parameters.AddWithValue("@p6", insert.currentRank);
                command.Parameters.AddWithValue("@p7", insert.currentGamesLost);
                MySqlDatabase.Instance.Conn.Open();
                command.ExecuteNonQuery();

                //TODO: Need to put this two lines in all the inserted rows;
                FilesAccess.db.playerLeagueTournament.Last().id = Convert.ToInt32(command.LastInsertedId);
                playerLeagueTournament.AcceptChanges();

                MySqlDatabase.Instance.Conn.Close();
                return true;
            }
            return false;
        }

        public bool UpdatePlayerLeagueTournament()
        {
            try
            {
                string sQuery = "Select * from PlayerLeagueTournament";

                MySqlDataAdapter myDA = new MySqlDataAdapter(sQuery, MySqlDatabase.Instance.Conn);

                MySqlCommandBuilder cmb = new MySqlCommandBuilder(myDA);
                myDA.Update(playerLeagueTournament);
                //playerChamps.Clear();
                //myDA.Fill(playerChamps);
                return true;
            }
            catch { return false; }
        }

        public void InsertAssociation()
        {
            string sQuery = "Select * from Associations";

            MySqlDataAdapter myDA = new MySqlDataAdapter(sQuery, MySqlDatabase.Instance.Conn);

            MySqlCommandBuilder cmb = new MySqlCommandBuilder(myDA);
            myDA.Update(associations);
            associations.Clear();
            myDA.Fill(associations);
        }

        public void UpdatePlayer()
        {
            string sQuery = "Select * from player WHERE 0=1";

            MySqlDataAdapter myDA = new MySqlDataAdapter(sQuery, MySqlDatabase.Instance.Conn);

            MySqlCommandBuilder cmb = new MySqlCommandBuilder(myDA);
            myDA.Update(player);
            //player.Clear();
            //myDA.Fill(player);
            //try
            //{
            //    new DataSet1TableAdapters.playerTableAdapter().Update(player);
            //    player = new DataSet1TableAdapters.playerTableAdapter().GetData();
            //}
            //catch(Exception ex) {
            //    System.Windows.Forms.MessageBox.Show(ex.Message);
            //}
        }

        public void AddPlayer()
        {
            string sQuery = "Select * from player where 0 = 1";

            MySqlDataAdapter myDA = new MySqlDataAdapter(sQuery, MySqlDatabase.Instance.Conn);

            MySqlCommandBuilder cmb = new MySqlCommandBuilder(myDA);
            myDA.Update(player);
            player.Clear();
            new MySqlDataAdapter("Select * from player", MySqlDatabase.Instance.Conn).Fill(player);
        }

        public void UpdateClub()
        {
            string sQuery = "Select * from clubs";

            MySqlDataAdapter myDA = new MySqlDataAdapter(sQuery, MySqlDatabase.Instance.Conn);

            MySqlCommandBuilder cmb = new MySqlCommandBuilder(myDA);
            myDA.Update(clubs);
            clubs.Clear();
            myDA.Fill(clubs);
            //new DataSet1TableAdapters.clubsTableAdapter().Update(clubs);
            //clubs = new DataSet1TableAdapters.clubsTableAdapter().GetData();
        }

        public bool UpdatePlayerClubSeason()
        {

            try
            {
                string sQuery = "Select * from playerClubSeason WHERE 0=1";

                MySqlDataAdapter myDA = new MySqlDataAdapter(sQuery, MySqlDatabase.Instance.Conn);

                MySqlCommandBuilder cmb = new MySqlCommandBuilder(myDA);
                myDA.Update(playerclubseason);
                return true;
            }
            catch { return false; }
        }

        public bool UpdateChamp()
        {
            try
            {
                string sQuery = "Select * from championships";

                MySqlDataAdapter myDA = new MySqlDataAdapter(sQuery, MySqlDatabase.Instance.Conn);

                MySqlCommandBuilder cmb = new MySqlCommandBuilder(myDA);
                myDA.Update(championships);
                championships.Clear();
                myDA.Fill(championships);
                return true;
            }
            catch { return false; }
        }

        public void InsertClub(clubsRow insert)
        {
            using (MySqlCommand command = new MySqlCommand())
            {

                string sQuery = "INSERT INTO clubs (name, photo, AssociationId) VALUES        (@p1, @p2, @p3)";

                command.Connection = MySqlDatabase.Instance.Conn;
                command.CommandType = CommandType.Text;
                command.CommandText = sQuery;
                command.Parameters.AddWithValue("@p1", insert.Name);
                command.Parameters.AddWithValue("@p2", insert.Photo);
                command.Parameters.AddWithValue("@p3", insert.AssociationId);
                MySqlDatabase.Instance.Conn.Open();
                command.ExecuteNonQuery();
                MySqlDatabase.Instance.Conn.Close();
            }
        }

        public void InsertGame(gamesRow insert)
        {
            using (MySqlCommand command = new MySqlCommand())
            {
                string sQuery = "INSERT INTO games (IdPlayerA, IdPlayerB, SetsA, SetsB, IdPlayerWinner, Date, IdChamp, IdCalculation, pointsPlayerA, pointsPlayerB, idPhase, IdTeamGame) VALUES        (@p1, @p2, @p3, @p4, @p5, @p6, @p7, @p8, @p9, @p10, @p11, @p12)";

                command.Connection = MySqlDatabase.Instance.Conn;
                command.CommandType = CommandType.Text;
                command.CommandText = sQuery;
                command.Parameters.AddWithValue("@p1", insert.IdPlayerA);
                command.Parameters.AddWithValue("@p2", insert.IdPlayerB);
                command.Parameters.AddWithValue("@p3", insert.SetsA);
                command.Parameters.AddWithValue("@p4", insert.SetsB);
                command.Parameters.AddWithValue("@p5", insert.IdPlayerWinner);
                command.Parameters.AddWithValue("@p6", insert.Date);
                command.Parameters.AddWithValue("@p7", insert.IdChamp);
                command.Parameters.AddWithValue("@p8", insert.IdCalculation);
                command.Parameters.AddWithValue("@p9", insert.pointsPlayerA);
                command.Parameters.AddWithValue("@p10", insert.pointsPlayerB);
                command.Parameters.AddWithValue("@p11", insert.idPhase);
                command.Parameters.AddWithValue("@p12", insert.IdTeamGame);
                MySqlDatabase.Instance.Conn.Open();
                command.ExecuteNonQuery();

                //TODO: Need to put this two lines in all the inserted rows;
                FilesAccess.db.games.Last().id = Convert.ToInt32(command.LastInsertedId);
                games.AcceptChanges();

                MySqlDatabase.Instance.Conn.Close();
            }

        }


        internal void InsertDoubleGame(doublegamesRow insert)
        {
            //doublegamesTableAdapter doublegameDatatable = new doublegamesTableAdapter();
            //doublegameDatatable.Insert(
            //    doublegameRow.DoubleA_PlayerAId,
            //    doublegameRow.DoubleA_PlayerBId,
            //    doublegameRow.DoubleB_PlayerAId,
            //    doublegameRow.DoubleB_PlayerBId,
            //    doublegameRow.SetsA,
            //    doublegameRow.SetsB,
            //    doublegameRow.SetsA > doublegameRow.SetsB ? doublegameRow.DoubleA_PlayerAId : doublegameRow.DoubleB_PlayerAId,
            //    doublegameRow.SetsA > doublegameRow.SetsB ? doublegameRow.DoubleA_PlayerBId : doublegameRow.DoubleB_PlayerBId,
            //    doublegameRow.Date,
            //    doublegameRow.IdChampionship,
            //    0,
            //    0,
            //    0,
            //    0,
            //    doublegameRow.phaseId,
            //    doublegameRow.IdTeamGame);

            //doublegameRow.Id = (int)doublegameDatatable.Adapter.InsertCommand.LastInsertedId;


            using (MySqlCommand command = new MySqlCommand())
            {
                string sQuery = "INSERT INTO doublegames (DoubleA_PlayerAId, DoubleA_PlayerBId, DoubleB_PlayerAId, DoubleB_PlayerBId, SetsA, SetsB, IdPlayerAWinner, IdPlayerBWinner, Date, IdChampionship, PointsDoubleA_PlayerA, PointsDoubleA_PlayerB, PointsDoubleB_PlayerA, PointsDoubleB_PlayerB, phaseId, IdTeamGame) VALUES        (@p1, @p2, @p3, @p4, @p5, @p6, @p7, @p8, @p9, @p10, @p11, @p12, @p13,@p14,@p15,@p16)";

                command.Connection = MySqlDatabase.Instance.Conn;
                command.CommandType = CommandType.Text;
                command.CommandText = sQuery;
                command.Parameters.AddWithValue("@p1", insert.DoubleA_PlayerAId);
                command.Parameters.AddWithValue("@p2", insert.DoubleA_PlayerBId);
                command.Parameters.AddWithValue("@p3", insert.DoubleB_PlayerAId);
                command.Parameters.AddWithValue("@p4", insert.DoubleB_PlayerBId);
                command.Parameters.AddWithValue("@p5", insert.SetsA);
                command.Parameters.AddWithValue("@p6", insert.SetsB);
                command.Parameters.AddWithValue("@p7", insert.IdPlayerAWinner);
                command.Parameters.AddWithValue("@p8", insert.IdPlayerBWinner);
                command.Parameters.AddWithValue("@p9", insert.Date);
                command.Parameters.AddWithValue("@p10", insert.IdChampionship);
                command.Parameters.AddWithValue("@p11", insert.PointsDoubleA_PlayerA);
                command.Parameters.AddWithValue("@p12", insert.PointsDoubleA_PlayerB);
                command.Parameters.AddWithValue("@p13", insert.PointsDoubleB_PlayerA);
                command.Parameters.AddWithValue("@p14", insert.PointsDoubleB_PlayerB);
                command.Parameters.AddWithValue("@p15", insert.phaseId);
                command.Parameters.AddWithValue("@p16", insert.IdTeamGame);
                MySqlDatabase.Instance.Conn.Open();
                command.ExecuteNonQuery();

                //TODO: Need to put this two lines in all the inserted rows;
                FilesAccess.db.doubleGames.Last().Id = Convert.ToInt32(command.LastInsertedId);
                doubleGames.AcceptChanges();

                MySqlDatabase.Instance.Conn.Close();
            }
        }

        public bool UpdateDoubleGames()
        {
            try
            {
                string sQuery = "Select * from doublegames WHERE 0=1";
                MySqlDataAdapter myDA = new MySqlDataAdapter(sQuery, MySqlDatabase.Instance.Conn);
                MySqlCommandBuilder cmb = new MySqlCommandBuilder(myDA);
                if(myDA.Update(doubleGames) > 0)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
                return false;
            }
        }

        public bool UpdateGames()
        {
            try
            {
                string sQuery = "Select * from games WHERE 0=1";
                MySqlDataAdapter myDA = new MySqlDataAdapter(sQuery, MySqlDatabase.Instance.Conn);
                MySqlCommandBuilder cmb = new MySqlCommandBuilder(myDA);
                if(myDA.Update(games)>0)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
                return false;
            }
        }
        
        public void Dispose()
        {
            calculations.Dispose();
            championships.Dispose();
            clubs.Dispose();
            games.Dispose();
            playerLeagueTournament.Dispose();
            playerClubs.Dispose();
            pointsBreakDown.Dispose();
            ratingChart.Dispose();
        }

        public void Dispose(bool dispose)
        {
            if (!dispose)
                return;

            Dispose();
        }
    }
}
