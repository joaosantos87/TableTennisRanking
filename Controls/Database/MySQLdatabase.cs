﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using System.Data;
using System.Windows.Forms;
using TableTennis.Properties;
using System.Threading.Tasks;

namespace TableTennis.Database
{
    public class MySqlDatabase
    {
        ///Localhost
        //string server = "localhost";
        //string databaseName = "tabletennisranking";
        //string username = "root";
        //string password = "";

        ///http://www.db4free.net/
        //string server = "db4free.net";
        //string databaseName = "tt_ranking";
        //string username = "joaosantos";
        //string password = "123456";

        ///Appharbor -> 26-01-16 not working -> 27-01-16 working
        string server = "6c145453-406b-4799-8b6f-a53e017254fd.mysql.sequelizer.com";
        string databaseName = "db6c145453406b47998b6fa53e017254fd";
        string username = "hjigjfhflfqbypac";
        string password = "Ck6Nr6JH6buAj8gGnNN7AtqrrXHE86pdHBzyanVwTv7UiMXfeuRtsSfi3MvAveJa";

        public event EventHandler DatabaseInitialized;

        MySqlConnection conn = null;

        public MySqlConnection Conn
        {
            get { return conn; }
        }

        private static MySqlDatabase instance;

        public static MySqlDatabase Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new MySqlDatabase();
                    instance.Initalization();
                    instance.PushData();
                }

                return instance;
            }
        }

        private bool PushData()
        {
            return false;
            //return true;
            //string file1 = "";

            //using (MySqlCommand cmd = new MySqlCommand())
            //{
            //    using (MySqlBackup mb = new MySqlBackup(cmd))
            //    {
            //        cmd.Connection = conn;
            //        conn.Open();
            //        file1 = "test.sql";
            //        mb.ExportToFile(file1);
            //        conn.Close();
            //    }
            //}

            string constring = "server=6c145453-406b-4799-8b6f-a53e017254fd.mysql.sequelizer.com;database=db6c145453406b47998b6fa53e017254fd;uid=hjigjfhflfqbypac;pwd=wa86VDPo8Hypkxtv7HB4QBVRnYJu6eBcArjp4maTNgrVVcbcZsewhgx3nqR64uq3";
            string file = "test.sql";
            using (MySqlConnection appHarborCon = new MySqlConnection(constring))
            {
                appHarborCon.Open();
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    using (MySqlBackup mb = new MySqlBackup(cmd))
                    {
                        cmd.Connection = appHarborCon;
                        conn.Open();
                        mb.ImportFromFile(file);
                        conn.Close();
                    }
                }
            }
            //Console.WriteLine("MySQL version : {0}", conn.ServerVersion);
            return true;
        }

        private void ForceInitialization()
        {
            Initalization();
        }

        public void CheckForceInitialization()
        {
            var query = "SELECT * FROM games";
            var cmd = new MySqlCommand(query, conn);
            var returnVal = new MySqlDataAdapter(query, conn);
            int numGames = FilesAccess.db.games.Count;
            returnVal.Fill(FilesAccess.db.games);
            if (FilesAccess.db.games.Count != numGames)
                ForceInitialization();
        }

        private void Initalization()
        {
            Task.Factory.StartNew(() =>
            {
                //string cs = "server=" + server + ";uid=" + username + ";password=" + password + ";database=" + databaseName + ";" + "port=3306";
                //string cs = "server=MYSQL6001.site4now.net;database=db_a4119d_rank;uid=a4119d_rank;password=a12345678";
                string cs = Settings.Default.db_a4119d_rankConnectionString1;

                bool initializationGood = false;
                System.Diagnostics.Stopwatch s1 = new System.Diagnostics.Stopwatch();

                try
                {
                    s1.Start();
                    conn = new MySqlConnection(cs);
                    conn.Open();
                    Console.WriteLine("Connection open...");
                    Console.WriteLine("MySQL version : {0}", conn.ServerVersion);

                    //TODO: test if filling async everything works well

                    string query = "SELECT * FROM associations";
                    MySqlCommand cmd = new MySqlCommand(query, conn);
                    MySqlDataAdapter returnVal = new MySqlDataAdapter(query, conn);
                    returnVal.Fill(FilesAccess.db.associations);
                    Console.WriteLine("... read from associations");

                    query = "SELECT * FROM season";
                    returnVal = new MySqlDataAdapter(query, conn);
                    returnVal.Fill(FilesAccess.db.seasons);
                    Console.WriteLine("... read from season");

                    query = "SELECT * FROM gamestyle";
                    cmd = new MySqlCommand(query, conn);
                    returnVal = new MySqlDataAdapter(query, conn);
                    returnVal.Fill(FilesAccess.db.gameStyle);
                    Console.WriteLine("... read from gamestyle");

                    query = "SELECT * FROM handshake";
                    cmd = new MySqlCommand(query, conn);
                    returnVal = new MySqlDataAdapter(query, conn);
                    returnVal.Fill(FilesAccess.db.handShake);
                    Console.WriteLine("... read from handshake");

                    query = "SELECT * FROM playerinscriptionleague";
                    cmd = new MySqlCommand(query, conn);
                    returnVal = new MySqlDataAdapter(query, conn);
                    returnVal.Fill(FilesAccess.db.playerInscriptionLeague);
                    Console.WriteLine("... read from playerinscriptionleague");

                    query = "SELECT * FROM levels";
                    cmd = new MySqlCommand(query, conn);
                    returnVal = new MySqlDataAdapter(query, conn);
                    returnVal.Fill(FilesAccess.db.levels);
                    Console.WriteLine("... read from levels");

                    query = "SELECT * FROM tournamenttype";
                    cmd = new MySqlCommand(query, conn);
                    returnVal = new MySqlDataAdapter(query, conn);
                    returnVal.Fill(FilesAccess.db.tournamentType);
                    Console.WriteLine("... read from tournamenttype");

                    query = "SELECT * FROM calculations";
                    cmd = new MySqlCommand(query, conn);
                    returnVal = new MySqlDataAdapter(query, conn);
                    returnVal.Fill(FilesAccess.db.calculations);
                    Console.WriteLine("... read from calculations");

                    query = "SELECT * FROM championships";
                    cmd = new MySqlCommand(query, conn);
                    returnVal = new MySqlDataAdapter(query, conn);
                    returnVal.Fill(FilesAccess.db.championships);
                    Console.WriteLine("... read from championships");

                    query = "SELECT * FROM clubs";
                    cmd = new MySqlCommand(query, conn);
                    returnVal = new MySqlDataAdapter(query, conn);
                    returnVal.Fill(FilesAccess.db.clubs);
                    Console.WriteLine("... read from clubs");

                    query = "SELECT * FROM player";
                    cmd = new MySqlCommand(query, conn);
                    returnVal = new MySqlDataAdapter(query, conn);
                    returnVal.Fill(FilesAccess.db.player);
                    Console.WriteLine("... read from player");

                    query = "SELECT * FROM teamgame";
                    cmd = new MySqlCommand(query, conn);
                    returnVal = new MySqlDataAdapter(query, conn);
                    returnVal.Fill(FilesAccess.db.teamGames);
                    Console.WriteLine("... read from teamgame");

                    query = "SELECT * FROM doublegames";
                    cmd = new MySqlCommand(query, conn);
                    returnVal = new MySqlDataAdapter(query, conn);
                    returnVal.Fill(FilesAccess.db.doubleGames);
                    Console.WriteLine("... read from doublegames");

                    query = "SELECT * FROM playerclubseason";
                    returnVal = new MySqlDataAdapter(query, conn);
                    returnVal.Fill(FilesAccess.db.playerclubseason);
                    Console.WriteLine("... read from playerclubseason");

                    query = "SELECT * FROM tournamentgames";
                    cmd = new MySqlCommand(query, conn);
                    returnVal = new MySqlDataAdapter(query, conn);
                    returnVal.Fill(FilesAccess.db.tournamentGames);
                    Console.WriteLine("... read from tournamentgames");

                    query = "SELECT * FROM tournamentgroup";
                    cmd = new MySqlCommand(query, conn);
                    returnVal = new MySqlDataAdapter(query, conn);
                    returnVal.Fill(FilesAccess.db.tournamentGroup);
                    Console.WriteLine("... read from tournamentgroup");

                    query = "SELECT * FROM playertourngroup";
                    cmd = new MySqlCommand(query, conn);
                    returnVal = new MySqlDataAdapter(query, conn);
                    returnVal.Fill(FilesAccess.db.playerTournGroup);
                    Console.WriteLine("... read from playertourngroup");

                    query = "SELECT * FROM playerleaguetournament";
                    cmd = new MySqlCommand(query, conn);
                    returnVal = new MySqlDataAdapter(query, conn);
                    returnVal.Fill(FilesAccess.db.playerLeagueTournament);
                    Console.WriteLine("... read from playerleaguetournament");

                    query = "SELECT * FROM playerclubs";
                    cmd = new MySqlCommand(query, conn);
                    returnVal = new MySqlDataAdapter(query, conn);
                    returnVal.Fill(FilesAccess.db.playerClubs);
                    Console.WriteLine("... read from playerclubs");

                    query = "SELECT * FROM phase";
                    cmd = new MySqlCommand(query, conn);
                    returnVal = new MySqlDataAdapter(query, conn);
                    returnVal.Fill(FilesAccess.db.phase);
                    Console.WriteLine("... read from phase");

                    query = "SELECT * FROM games";
                    cmd = new MySqlCommand(query, conn);
                    returnVal = new MySqlDataAdapter(query, conn);
                    returnVal.Fill(FilesAccess.db.games);
                    Console.WriteLine("... read from games");

                    query = "SELECT * FROM pointsbreakdown";
                    cmd = new MySqlCommand(query, conn);
                    returnVal = new MySqlDataAdapter(query, conn);
                    returnVal.Fill(FilesAccess.db.pointsBreakDown);
                    Console.WriteLine("... read from pointsbreakdown");

                    query = "SELECT * FROM ratingchart";
                    cmd = new MySqlCommand(query, conn);
                    returnVal = new MySqlDataAdapter(query, conn);
                    returnVal.Fill(FilesAccess.db.ratingChart);
                    Console.WriteLine("... read from ratingchart");

                    query = "SELECT * FROM ranking";
                    cmd = new MySqlCommand(query, conn);
                    returnVal = new MySqlDataAdapter(query, conn);
                    FilesAccess.db.ranking.Clear();
                    returnVal.Fill(FilesAccess.db.ranking);
                    Console.WriteLine("... read from ranking");

                    //Check if this waits for all the fill to be done
                    //Task.WaitAll();
                    s1.Stop();
                    initializationGood = true;
                }
                catch (MySqlException ex)
                {
                    System.Windows.Forms.MessageBox.Show(ex.ToString());
                    Console.WriteLine("Error: {0}", ex.ToString());
                }
                finally
                {
                    if (conn != null)
                        conn.Close();
                    DatabaseInitialized?.Invoke(this, new EventArgs());
                }
                if (!initializationGood)
                {
                    System.Windows.Forms.MessageBox.Show("Application will now close");
                    System.Environment.Exit(0);
                }
            });
        }
    }
}
