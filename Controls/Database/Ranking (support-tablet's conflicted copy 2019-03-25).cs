﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using static TableTennis.Database.DataSet2;

namespace TableTennis.Database
{
    public class Ranking
    {
        public List<Tuple<DateTime, championshipsRow>> championshipsIncluded = new List<Tuple<DateTime, championshipsRow>>();

        private List<playerRow> players;

        private static Ranking instance;
        public static Ranking Instance
        {
            get
            {
                if (instance == null)
                    instance = new Ranking();

                return instance;
            }
        }

        public List<playerRow> Players
        {
            get
            {
                if (players == null)
                    return UpdateRanking(new DateTime(1970, 1, 1), DateTime.Now);
                
                return players;
            }
        }
        public List<playerRow> GenerateRanking()
        {
            return FillRanking(new DateTime(1970,1,1), DateTime.Now);
        }

        public List<playerRow> GenerateRanking(DateTime start, DateTime end, List<int> champ = null, List<int> association = null)
        {
            return FillRanking(start, end, champ, association);
        }

        public List<playerRow> UpdateRanking(DateTime start, DateTime end, List<int> champ = null, List<int> association = null)
        {
            players = FillRanking(start, end, champ, association);
            return players;
        }

        private List<playerRow> FillRanking(DateTime start, DateTime end, List<int> champ = null, List<int> association = null)
        {
            var localPlayers = new List<playerRow>();
            string notByePlayer = " IdPlayerA <>" + Utilities.ByePlayer + " AND IdPlayerB <>" + Utilities.ByePlayer;

            gamesRow[] allGames;
            List<doublegamesRow> allGamesDouble = new List<doublegamesRow>();

            allGames = (gamesRow[])FilesAccess.db.games.Select("Date >= '" + start + "' AND Date <= '" + end + "' AND (" + notByePlayer + ")");
            allGamesDouble = FilesAccess.db.doubleGames.Where(x => x.Date >= start && x.Date <= end && x.DoubleA_PlayerAId != Utilities.ByePlayer && x.DoubleB_PlayerBId != Utilities.ByePlayer).ToList();

            if (champ != null)
            {
                if (champ.Count > 0)
                {
                    List<gamesRow> allQueries = new List<gamesRow>();
                    List<doublegamesRow> allQueriesDouble = new List<doublegamesRow>();
                    string selectedQuery = "";
                    for (int i = 0; i < champ.Count; i++)
                    {
                        selectedQuery += "IdChamp='";
                        selectedQuery += champ[i] + "' OR ";
                        allQueries.AddRange(allGames.Where(x => x.IdChamp == champ[i]).ToList());
                        allQueriesDouble.AddRange(allGamesDouble.Where(x => x.IdChampionship == champ[i]).ToList());
                    }
                    selectedQuery += "IdChamp='";
                    selectedQuery += "-1'";

                    allGames = allQueries.ToArray();
                }
            }

            allGames = allGames.OrderBy(x => x.Date).ToArray();
            allGamesDouble = allGamesDouble.OrderBy(x => x.Date).ToList();

            for (int i = 0; i < FilesAccess.db.player.Count; i++)
            {
                localPlayers.Add(FilesAccess.db.player[i]);
                localPlayers[i].currentPoints = 1000;
                localPlayers[i].currentGamesLost = 0;
                localPlayers[i].currentGamesWon = 0;
                localPlayers[i].currentRank = 0;
            }

            localPlayers = CalculateRankGames(allGames,allGamesDouble, localPlayers);

            for (int i = 0; i < localPlayers.Count; i++)
            {
                if (localPlayers[i].currentGamesLost + localPlayers[i].currentGamesWon == 0)
                    localPlayers[i].currentPoints = -1;
            }

            if (Utilities.IsChampOpenTournament(champ))
            {
                playerRow[] temp = FilesAccess.db.player.OrderByDescending(x => x.currentPoints).ToArray();
                int rank = 1;
                for (int i = 0; i < temp.Length; i++)
                {
                    //Check if the player is part of the league
                    if (FilesAccess.db.playerInscriptionLeague.Count(x => x.playerId == temp[i].Id) > 0)
                    {
                        localPlayers.Single(x => x.Id == temp[i].Id).currentRank = (rank);
                        rank++;
                    }
                }

                if (champ.Count > 1)
                {
                    for (int i = 0; i < FilesAccess.db.playerInscriptionLeague.Count; i++)
                    {
                        playerinscriptionleagueRow plilRow = FilesAccess.db.playerInscriptionLeague[i];
                        FilesAccess.db.playerInscriptionLeague[i].currentPoints = localPlayers.Single(x => x.Id == plilRow.playerId).currentPoints;
                        FilesAccess.db.playerInscriptionLeague[i].currentGamesWon = localPlayers.Single(x => x.Id == plilRow.playerId).currentGamesWon;
                        FilesAccess.db.playerInscriptionLeague[i].currentGamesLost = localPlayers.Single(x => x.Id == plilRow.playerId).currentGamesLost;

                        FilesAccess.db.playerInscriptionLeague[i].currentRank = localPlayers.Single(x => x.Id == plilRow.playerId).currentRank;
                    }

                    FilesAccess.db.UpdateLeagueInscription();
                }
                else
                {
                    List<playerleaguetournamentRow> champsRow = ((playerleaguetournamentRow[])FilesAccess.db.playerLeagueTournament.Select("championshipId=" + champ[0])).ToList();
                    for (int i = 0; i < champsRow.Count; i++)
                    {
                        playerleaguetournamentRow plilRow = champsRow[i];
                        champsRow[i].currentPoints = localPlayers.Single(x => x.Id == plilRow.playerId).currentPoints;
                        champsRow[i].currentGamesWon = localPlayers.Single(x => x.Id == plilRow.playerId).currentGamesWon;
                        champsRow[i].currentGamesLost = localPlayers.Single(x => x.Id == plilRow.playerId).currentGamesLost;
                        champsRow[i].currentRank = localPlayers.Single(x => x.Id == plilRow.playerId).currentRank;
                    }
                    FilesAccess.db.UpdatePlayerLeagueTournament();
                }
            }
            else
            {
                //Do the generic ranking:
                localPlayers = localPlayers.OrderByDescending(x => x.currentPoints).ToList();
                for (int i = 0; i < localPlayers.Count; i++)
                {
                    //localPlayers.Single(x => x.Id == temp[i].Id).currentRank = (i + 1);
                    localPlayers[i].currentRank = (i + 1);
                }
            }
            return localPlayers.Where(x => x.currentGamesLost + x.currentGamesWon > 0).ToList();
        }

        private List<playerRow> CalculateRankGames(gamesRow[] allGamesSingle, List<doublegamesRow> allGamesDouble, List<playerRow> localPlayers)
        {
            //create a class that is the same in both, and use the select, union to allow to have all in the same object
            var allGames1 = from g in allGamesSingle
                            select new ListGames()
                            {
                                Id = g.id,
                                Date = g.Date,
                                IdPlayerA = g.IdPlayerA,
                                IdPlayerB = g.IdPlayerB,
                                IdPlayerWinner = g.IdPlayerWinner,
                                IdChamp = g.IdChamp,
                                PointsPlayerA = g.pointsPlayerA,
                                PointsPlayerB = g.pointsPlayerB,
                                IsDouble = false,
                            };

            var allGames2 = from g in allGamesDouble
                            select new ListGames()
                            {
                                Id = g.Id,
                                Date = g.Date,
                                IdPlayerA = g.DoubleA_PlayerAId << 16 | g.DoubleA_PlayerBId,
                                IdPlayerB = g.DoubleB_PlayerAId << 16 | g.DoubleB_PlayerBId,
                                IdPlayerWinner = g.IdPlayerAWinner << 16 | g.IdPlayerBWinner,
                                IdChamp = g.IdChampionship,
                                PointsPlayerA = g.PointsDoubleA_PlayerA << 16 | g.PointsDoubleA_PlayerB,
                                PointsPlayerB = g.PointsDoubleB_PlayerA << 16 | g.PointsDoubleB_PlayerB,
                                IsDouble = true,
                            };

            var allGames = allGames1.Union(allGames2).OrderBy(x => x.Date).ToList();

            for (int i = 0; i < allGames.Count; i++)
            {
                if (allGames[i].IdPlayerA > -1 && allGames[i].IdPlayerB > -1 && allGames[i].IdChamp > -1)
                {
                    try
                    {
                        if (allGames[i].IsDouble)
                        {
                            playerRow doubleAplayerA = localPlayers.Single(x => x.Id == allGames[i].IdPlayerA >> 16);
                            playerRow doubleAplayerB = localPlayers.Single(x => x.Id == (allGames[i].IdPlayerA & 0xFFFF));

                            playerRow doubleBplayerA = localPlayers.Single(x => x.Id == allGames[i].IdPlayerB >> 16);
                            playerRow doubleBplayerB = localPlayers.Single(x => x.Id == (allGames[i].IdPlayerB & 0xFFFF));
                            
                            DoCalculation(
                                allGames[i], 
                                new List<playerRow>() { doubleAplayerA, doubleAplayerB }, 
                                new List<playerRow>() { doubleBplayerA, doubleBplayerB });

                            var tmp = allGamesDouble.Single(x => x.Id == allGames[i].Id);
                            tmp.PointsDoubleA_PlayerA = allGames[i].PointsPlayerA / 2;
                            tmp.PointsDoubleA_PlayerB = allGames[i].PointsPlayerA / 2;

                            tmp.PointsDoubleB_PlayerA = allGames[i].PointsPlayerB / 2;
                            tmp.PointsDoubleB_PlayerB = allGames[i].PointsPlayerB / 2;
                        }
                        else
                        {
                            playerRow playerA = localPlayers.Single(x => x.Id == allGames[i].IdPlayerA);
                            playerRow playerB = localPlayers.Single(x => x.Id == allGames[i].IdPlayerB);
                            DoCalculation(
                                allGames[i], 
                                new List<playerRow>() { playerA },
                                new List<playerRow>() { playerB });

                            var tmp = allGamesSingle.Single(x => x.id == allGames[i].Id);
                            tmp.pointsPlayerA = allGames[i].PointsPlayerA;
                            tmp.pointsPlayerB = allGames[i].PointsPlayerB;
                        }

                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
            }



            return localPlayers;
        }

        private void DoCalculation(ListGames currentGame, List<playerRow> playerA, List<playerRow> playerB)
        {
            Tuple<int, int> pointsToDistribuite = AwardPoints(currentGame.IdPlayerWinner, playerA, playerB);
            championshipsRow champRow = FilesAccess.db.championships.Single(x => x.Id == currentGame.IdChamp);
            float weight = FilesAccess.db.tournamentType.Single(x => x.id == champRow.idTournamentType).weight;

            int pointsWinner = Convert.ToInt32(pointsToDistribuite.Item2 * weight);
            int pointsLoser = Convert.ToInt32(pointsToDistribuite.Item2 / 2 * 1);

            int playerAId = playerA[0].Id;

            if (playerA.Count > 1)
                playerAId = playerA[0].Id << 16 | playerA[1].Id;

            if (playerAId == currentGame.IdPlayerWinner)
            {
                playerA.ForEach(x => x.currentPoints += pointsWinner);
                playerB.ForEach(x => x.currentPoints -= pointsLoser);
                currentGame.PointsPlayerA = pointsWinner;
                currentGame.PointsPlayerB = -pointsLoser;
                playerA.ForEach(x => x.currentGamesWon++);
                playerB.ForEach(x => x.currentGamesLost++);
            }
            else
            {
                playerB.ForEach(x => x.currentPoints += pointsWinner);
                playerA.ForEach(x => x.currentPoints -= pointsLoser);
                currentGame.PointsPlayerB = pointsWinner;
                currentGame.PointsPlayerA = -pointsLoser;
                playerB.ForEach(x => x.currentGamesWon++);
                playerA.ForEach(x => x.currentGamesLost++);
            }

            if (!championshipsIncluded.Contains(new Tuple<DateTime, championshipsRow>(currentGame.Date, champRow)))
                championshipsIncluded.Add(new Tuple<DateTime, championshipsRow>(currentGame.Date, champRow));
        }

        private bool IsWinnerExpected(long winnerId, List<playerRow> playerA, List<playerRow> playerB)
        {
            var currentPointsA = playerA.Sum(x => x.currentPoints);
            var currentPointsB = playerB.Sum(x => x.currentPoints);

            int playerAId = playerA[0].Id;

            if(playerA.Count > 1)
                playerAId = playerA[0].Id << 16 | playerA[1].Id;

            int playerBId = playerB[0].Id;

            if (playerB.Count > 1)
                playerBId = playerB[0].Id << 16 | playerB[1].Id;

            if (currentPointsA >= currentPointsB && winnerId == playerAId ||
                currentPointsB >= currentPointsA && winnerId == playerBId)
                return true;
            else
                return false;
        }

        private Tuple<int, int> AwardPoints(long winnerId, List<playerRow> playerA, List<playerRow> playerB)
        {
            List<Tuple<int, int>> pointsExpected = new List<Tuple<int, int>>();
            List<Tuple<int, int>> pointsUnexpected = new List<Tuple<int, int>>();

            List<Tuple<int, int, int>> preConfiguredPoints = new List<Tuple<int, int, int>>();
            #region OriginalPoints
            //preConfiguredPoints.Add(new Tuple<int, int, int>(-1, 8, 8));
            //preConfiguredPoints.Add(new Tuple<int, int, int>(1, 8, 8));
            //preConfiguredPoints.Add(new Tuple<int, int, int>(12, 8, 8));
            //preConfiguredPoints.Add(new Tuple<int, int, int>(37, 7, 10));
            //preConfiguredPoints.Add(new Tuple<int, int, int>(62, 6, 13));
            //preConfiguredPoints.Add(new Tuple<int, int, int>(87, 5, 16));
            //preConfiguredPoints.Add(new Tuple<int, int, int>(112, 4, 20));
            //preConfiguredPoints.Add(new Tuple<int, int, int>(137, 3, 25));
            //preConfiguredPoints.Add(new Tuple<int, int, int>(162, 2, 30));
            //preConfiguredPoints.Add(new Tuple<int, int, int>(187, 2, 35));
            //preConfiguredPoints.Add(new Tuple<int, int, int>(212, 1, 40));
            //preConfiguredPoints.Add(new Tuple<int, int, int>(237, 1, 45));
            //preConfiguredPoints.Add(new Tuple<int, int, int>(238, 0, 50));
            #endregion

            preConfiguredPoints.Add(new Tuple<int, int, int>(-1, 8, 8));
            preConfiguredPoints.Add(new Tuple<int, int, int>(1, 8, 8));
            preConfiguredPoints.Add(new Tuple<int, int, int>(20, 7, 12));
            preConfiguredPoints.Add(new Tuple<int, int, int>(50, 6, 17));
            preConfiguredPoints.Add(new Tuple<int, int, int>(75, 5, 15));
            preConfiguredPoints.Add(new Tuple<int, int, int>(100, 4, 17));
            preConfiguredPoints.Add(new Tuple<int, int, int>(150, 4, 22));
            preConfiguredPoints.Add(new Tuple<int, int, int>(225, 3, 29));
            preConfiguredPoints.Add(new Tuple<int, int, int>(300, 2, 36));
            preConfiguredPoints.Add(new Tuple<int, int, int>(400, 1, 45));
            preConfiguredPoints.Add(new Tuple<int, int, int>(500, 1, 54));
            preConfiguredPoints.Add(new Tuple<int, int, int>(800, 1, 82));
            preConfiguredPoints.Add(new Tuple<int, int, int>(1000, 1, 100));

            for (int i = 0; i < preConfiguredPoints.Count; i++)
            {
                pointsExpected.Add(new Tuple<int, int>(preConfiguredPoints[i].Item1, preConfiguredPoints[i].Item2));
                pointsUnexpected.Add(new Tuple<int, int>(preConfiguredPoints[i].Item1, preConfiguredPoints[i].Item3));
            }

            #region Backup points
            //pointsExpected.Add(new Tuple<int, int>(-1, 9));
            //pointsExpected.Add(new Tuple<int, int>(1, 9));
            //pointsExpected.Add(new Tuple<int, int>(26, 8));
            //pointsExpected.Add(new Tuple<int, int>(51, 7));
            //pointsExpected.Add(new Tuple<int, int>(101, 6));
            //pointsExpected.Add(new Tuple<int, int>(151, 5));
            //pointsExpected.Add(new Tuple<int, int>(201, 4));
            //pointsExpected.Add(new Tuple<int, int>(301, 3));
            //pointsExpected.Add(new Tuple<int, int>(401, 2));
            //pointsExpected.Add(new Tuple<int, int>(501, 1));
            //pointsExpected.Add(new Tuple<int, int>(750, 0));

            //pointsUnexpected.Add(new Tuple<int, int>(-1, 10));
            //pointsUnexpected.Add(new Tuple<int, int>(0, 10));
            //pointsUnexpected.Add(new Tuple<int, int>(25, 12));
            //pointsUnexpected.Add(new Tuple<int, int>(50, 14));
            //pointsUnexpected.Add(new Tuple<int, int>(100, 16));
            //pointsUnexpected.Add(new Tuple<int, int>(150, 20));
            //pointsUnexpected.Add(new Tuple<int, int>(200, 24));
            //pointsUnexpected.Add(new Tuple<int, int>(300, 28));
            //pointsUnexpected.Add(new Tuple<int, int>(400, 32));
            //pointsUnexpected.Add(new Tuple<int, int>(500, 36));
            //pointsUnexpected.Add(new Tuple<int, int>(749, 40));
            #endregion

            var currentPointsPlayerA = playerA.Sum(x => x.currentPoints * (1.0d / playerA.Count));
            var currentPointsPlayerB = playerB.Sum(x => x.currentPoints * (1.0d / playerA.Count));

            double dif = Math.Abs(currentPointsPlayerA - currentPointsPlayerB);
            for (int i = pointsExpected.Count-1; i > -1; i--)
            {
                if (dif > pointsExpected[i].Item1)
                {
                    if (IsWinnerExpected(winnerId, playerA, playerB))
                    {
                        return pointsExpected[i];
                    }
                    else
                        return pointsUnexpected[i];
                }
            }
            return new Tuple<int, int>(0, 0);
        }
    }
}
