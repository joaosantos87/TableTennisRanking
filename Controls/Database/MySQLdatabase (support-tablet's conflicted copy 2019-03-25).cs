﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using System.Data;
using System.Windows.Forms;
using TableTennis.Properties;

namespace TableTennis.Database
{
    public class MySqlDatabase
    {
        ///Localhost
        //string server = "localhost";
        //string databaseName = "tabletennisranking";
        //string username = "root";
        //string password = "";

        ///http://www.db4free.net/
        //string server = "db4free.net";
        //string databaseName = "tt_ranking";
        //string username = "joaosantos";
        //string password = "123456";

        ///Appharbor -> 26-01-16 not working -> 27-01-16 working
        string server = "6c145453-406b-4799-8b6f-a53e017254fd.mysql.sequelizer.com";
        string databaseName = "db6c145453406b47998b6fa53e017254fd";
        string username = "hjigjfhflfqbypac";
        string password = "Ck6Nr6JH6buAj8gGnNN7AtqrrXHE86pdHBzyanVwTv7UiMXfeuRtsSfi3MvAveJa";

        MySqlConnection conn = null;

        public MySqlConnection Conn
        {
            get { return conn; }
        }

        private static MySqlDatabase instance;
        public static MySqlDatabase Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new MySqlDatabase();
                    instance.Initalization();
                    instance.PushData();

                    //calculate the ranking
                    //new Ranking(new DateTime(2000, 1, 1), DateTime.Now);
                }
                return instance;
            }
        }

        private bool PushData()
        {
            return false;
            //return true;
            //string file1 = "";

            //using (MySqlCommand cmd = new MySqlCommand())
            //{
            //    using (MySqlBackup mb = new MySqlBackup(cmd))
            //    {
            //        cmd.Connection = conn;
            //        conn.Open();
            //        file1 = "test.sql";
            //        mb.ExportToFile(file1);
            //        conn.Close();
            //    }
            //}

            string constring = "server=6c145453-406b-4799-8b6f-a53e017254fd.mysql.sequelizer.com;database=db6c145453406b47998b6fa53e017254fd;uid=hjigjfhflfqbypac;pwd=wa86VDPo8Hypkxtv7HB4QBVRnYJu6eBcArjp4maTNgrVVcbcZsewhgx3nqR64uq3";
            string file = "test.sql";
            using (MySqlConnection appHarborCon = new MySqlConnection(constring))
            {
                appHarborCon.Open();
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    using (MySqlBackup mb = new MySqlBackup(cmd))
                    {
                        cmd.Connection = appHarborCon;
                        conn.Open();
                        mb.ImportFromFile(file);
                        conn.Close();
                    }
                }
            }
            //Console.WriteLine("MySQL version : {0}", conn.ServerVersion);
            return true;
        }

        public void ForceInitialization()
        {
            Initalization();
        }

        private void Initalization()
        {
            //string cs = "server=" + server + ";uid=" + username + ";password=" + password + ";database=" + databaseName + ";" + "port=3306";
            //string cs = "server=MYSQL6001.site4now.net;database=db_a4119d_rank;uid=a4119d_rank;password=a12345678";
            string cs = Settings.Default.db_a4119d_rankConnectionString;

            bool initializationGood = false;

            try
            {
                conn = new MySqlConnection(cs);
                conn.Open();
                Console.WriteLine("Connection open...");
                Console.WriteLine("MySQL version : {0}", conn.ServerVersion);

                string query = "SELECT * FROM Associations";
                MySqlCommand cmd = new MySqlCommand(query, conn);
                MySqlDataAdapter returnVal = new MySqlDataAdapter(query, conn);
                returnVal.Fill(FilesAccess.db.associations);

                query = "SELECT * FROM season";
                returnVal = new MySqlDataAdapter(query, conn);
                returnVal.Fill(FilesAccess.db.seasons);


                query = "SELECT * FROM GameStyle";
                cmd = new MySqlCommand(query, conn);
                returnVal = new MySqlDataAdapter(query, conn);
                returnVal.Fill(FilesAccess.db.gameStyle);

                query = "SELECT * FROM handshake";
                cmd = new MySqlCommand(query, conn);
                returnVal = new MySqlDataAdapter(query, conn);
                returnVal.Fill(FilesAccess.db.handShake);

                query = "SELECT * FROM PlayerInscriptionLeague";
                cmd = new MySqlCommand(query, conn);
                returnVal = new MySqlDataAdapter(query, conn);
                returnVal.Fill(FilesAccess.db.playerInscriptionLeague);

                query = "SELECT * FROM Levels";
                cmd = new MySqlCommand(query, conn);
                returnVal = new MySqlDataAdapter(query, conn);
                returnVal.Fill(FilesAccess.db.levels);

                query = "SELECT * FROM TournamentType";
                cmd = new MySqlCommand(query, conn);
                returnVal = new MySqlDataAdapter(query, conn);
                returnVal.Fill(FilesAccess.db.tournamentType);

                query = "SELECT * FROM calculations";
                cmd = new MySqlCommand(query, conn);
                returnVal = new MySqlDataAdapter(query, conn);
                returnVal.Fill(FilesAccess.db.calculations);

                query = "SELECT * FROM championships";
                cmd = new MySqlCommand(query, conn);
                returnVal = new MySqlDataAdapter(query, conn);
                returnVal.Fill(FilesAccess.db.championships);

                query = "SELECT * FROM clubs";
                cmd = new MySqlCommand(query, conn);
                returnVal = new MySqlDataAdapter(query, conn);
                returnVal.Fill(FilesAccess.db.clubs);

                query = "SELECT * FROM player";
                cmd = new MySqlCommand(query, conn);
                returnVal = new MySqlDataAdapter(query, conn);
                returnVal.Fill(FilesAccess.db.player);

                query = "SELECT * FROM teamgame";
                cmd = new MySqlCommand(query, conn);
                returnVal = new MySqlDataAdapter(query, conn);
                returnVal.Fill(FilesAccess.db.teamGames);
                
                query = "SELECT * FROM doublegames";
                cmd = new MySqlCommand(query, conn);
                returnVal = new MySqlDataAdapter(query, conn);
                returnVal.Fill(FilesAccess.db.doubleGames);

                query = "SELECT * FROM playerClubSeason";
                returnVal = new MySqlDataAdapter(query, conn);
                returnVal.Fill(FilesAccess.db.playerclubseason);

                query = "SELECT * FROM tournamentGames";
                cmd = new MySqlCommand(query, conn);
                returnVal = new MySqlDataAdapter(query, conn);
                returnVal.Fill(FilesAccess.db.tournamentGames);

                query = "SELECT * FROM tournamentGroup";
                cmd = new MySqlCommand(query, conn);
                returnVal = new MySqlDataAdapter(query, conn);
                returnVal.Fill(FilesAccess.db.tournamentGroup);

                query = "SELECT * FROM playerTournGroup";
                cmd = new MySqlCommand(query, conn);
                returnVal = new MySqlDataAdapter(query, conn);
                returnVal.Fill(FilesAccess.db.playerTournGroup);

                query = "SELECT * FROM PlayerLeagueTournament";
                cmd = new MySqlCommand(query, conn);
                returnVal = new MySqlDataAdapter(query, conn);
                returnVal.Fill(FilesAccess.db.playerLeagueTournament);

                query = "SELECT * FROM playerclubs";
                cmd = new MySqlCommand(query, conn);
                returnVal = new MySqlDataAdapter(query, conn);
                returnVal.Fill(FilesAccess.db.playerClubs);

                query = "SELECT * FROM Phase";
                cmd = new MySqlCommand(query, conn);
                returnVal = new MySqlDataAdapter(query, conn);
                returnVal.Fill(FilesAccess.db.phase);

                query = "SELECT * FROM games";
                cmd = new MySqlCommand(query, conn);
                returnVal = new MySqlDataAdapter(query, conn);
                returnVal.Fill(FilesAccess.db.games);

                query = "SELECT * FROM pointsbreakdown";
                cmd = new MySqlCommand(query, conn);
                returnVal = new MySqlDataAdapter(query, conn);
                returnVal.Fill(FilesAccess.db.pointsBreakDown);

                query = "SELECT * FROM ratingchart";
                cmd = new MySqlCommand(query, conn);
                returnVal = new MySqlDataAdapter(query, conn);
                returnVal.Fill(FilesAccess.db.ratingChart);
                initializationGood = true;
            }
            catch (MySqlException ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.ToString());
                Console.WriteLine("Error: {0}", ex.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
            if (!initializationGood)
            {
                System.Windows.Forms.MessageBox.Show("Application will now close");
                System.Environment.Exit(0);
            }
        }
    }
}
