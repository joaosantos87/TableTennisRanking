﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TableTennis.Database
{
    public class Game : INotifyPropertyChanged
    {
        private int phaseid;
        private int champid;
        private long playerA_id;
        private long playerB_id;
        public int Id { get; private set; }
        public long PlayerA_Id
        {
            get { return playerA_id; }
            set
            {
                if (playerA_id == value)
                    return;

                playerA_id = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(PlayerA_Id)));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(PlayerA)));
            }
        }
        public string PlayerA
        {
            get
            {
                var player = FilesAccess.db.player.SingleOrDefault(x => x.Id == PlayerA_Id);
                if (player == null)
                    return "null";

                return player.Name;
            }
        }
        public long PlayerB_Id
        {
            get { return playerB_id; }
            set
            {
                if (playerB_id == value)
                    return;

                playerB_id = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(PlayerB_Id)));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(PlayerB)));
            }
        }
        public string PlayerB
        {
            get
            {
                var player = FilesAccess.db.player.SingleOrDefault(x => x.Id == PlayerB_Id);
                if (player == null)
                    return "null";

                return player.Name;
            }
        }
        public int SetsA { get; set; }
        public int SetsB { get; set; }
        public long IdPlayerWinner { get; set; }
        public int ChampId
        {
            get { return champid; }
            set
            {
                if (champid == value)
                    return;

                champid = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(ChampId)));
            }
        }
        public string Champ
        {
            get
            {
                var champ = FilesAccess.db.championships.SingleOrDefault(x => x.Id == ChampId);
                if (champ == null)
                    return "null";

                return champ.Name;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public int PhaseId 
        {
            get { return phaseid; }
            set
            {
                if (phaseid == value)
                    return;

                phaseid = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(PhaseId)));
            }
        }
        public string Phase 
        { 
            get
            {
                var phase = FilesAccess.db.phase.SingleOrDefault(x => x.id == PhaseId);
                if (phase == null)
                    return "null";

                return phase.phase;
            }
        }

        public bool IgnoreOnRankings 
        {
            get;
            set;
        }

        public int TeamGameId { get; set; }
        public DateTime Date { get; set; }

        public Game(int id)
        {
            Id = id;
        }
    }
}
