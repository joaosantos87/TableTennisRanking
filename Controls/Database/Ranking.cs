﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using static TableTennis.Database.DataSet2;

namespace TableTennis.Database
{
    public class Ranking
    {
        public List<Tuple<DateTime, championshipsRow>> championshipsIncluded = new List<Tuple<DateTime, championshipsRow>>();

        private List<playerRow> players;
        private List<gamesRow> allSinglesGames;
        private List<doublegamesRow> allDoublesGames;
        
        private Ranking()
        {
            players = FilesAccess.db.player.ToList();
        }

        private static Ranking instance;
        public static Ranking Instance
        {
            get
            {
                if (instance == null)
                    instance = new Ranking();

                return instance;
            }
        }

        public List<playerRow> Players
        {
            get
            {                
                return players;
            }
        }

        public List<gamesRow> SinglesGames
        {
            get { return allSinglesGames; }
        }

        public List<doublegamesRow> DoublesGames
        {
            get { return allDoublesGames; }
        }

        public List<playerRow> GenerateRanking()
        {
            return FillRanking(new DateTime(1970,1,1), DateTime.Now);
        }

        public List<playerRow> GenerateRanking(DateTime start, DateTime end, List<int> champ = null, List<int> association = null, bool useSinglesGames = true, bool useDoubleGames = true)
        {
            return FillRanking(start, end, champ, association);
        }

        public List<playerRow> UpdateRanking(DateTime start, DateTime end, List<int> champ = null, List<int> association = null, bool useSinglesGames=true, bool useDoubleGames=true)
        {
            players = FillRanking(start, end, champ, association, useSinglesGames: useSinglesGames, useDoubleGames: useDoubleGames);
            return players;
        }

        public void UpdateAllGamesPoints()
        {
            FillRanking(new DateTime(1970, 1, 1), DateTime.Now, null, null, true);
        }

        private List<playerRow> FillRanking(DateTime start, DateTime end, List<int> champ = null, List<int> association = null, bool updateGamesPoints=false, bool useSinglesGames=true, bool useDoubleGames=true)
        {
            string notByePlayer = " IdPlayerA <>" + Utilities.ByePlayer + " AND IdPlayerB <>" + Utilities.ByePlayer + " AND IdPlayerA <>" + Utilities.FCPlayer + " AND IdPlayerB <>" + Utilities.FCPlayer;

            if (useSinglesGames)
                allSinglesGames = ((gamesRow[])FilesAccess.db.games.Select("Date >= '" + start + "' AND Date <= '" + end + "' AND (" + notByePlayer + ")")).ToList();
            else
                allSinglesGames = null;

            if (useDoubleGames)
                allDoublesGames = FilesAccess.db.doubleGames.Where(x => x.Date >= start && x.Date <= end && x.DoubleA_PlayerAId != Utilities.ByePlayer && x.DoubleB_PlayerBId != Utilities.ByePlayer && x.DoubleA_PlayerAId != Utilities.FCPlayer && x.DoubleB_PlayerBId != Utilities.FCPlayer).ToList();
            else
                allDoublesGames = null;

            if (champ != null)
            {
                if (champ.Count > 0)
                {
                    List<gamesRow> allQueries = new List<gamesRow>();
                    List<doublegamesRow> allQueriesDouble = new List<doublegamesRow>();
                    string selectedQuery = "";
                    for (int i = 0; i < champ.Count; i++)
                    {
                        selectedQuery += "IdChamp='";
                        selectedQuery += champ[i] + "' OR ";
                        if(allSinglesGames != null)
                            allQueries.AddRange(allSinglesGames.Where(x => x.IdChamp == champ[i]).ToList());

                        if(allDoublesGames != null)
                            allQueriesDouble.AddRange(allDoublesGames.Where(x => x.IdChampionship == champ[i]).ToList());
                    }
                    selectedQuery += "IdChamp='";
                    selectedQuery += "-1'";

                    allSinglesGames = allQueries.ToList();
                    allDoublesGames = allQueriesDouble.ToList();
                }
            }

            allSinglesGames = allSinglesGames?.OrderBy(x => x.Date).ToList();
            allDoublesGames = allDoublesGames?.OrderBy(x => x.Date).ToList();

            //if (players.Count == 0)
            players = FilesAccess.db.player.ToList();

            var todosCampeonatos = FilesAccess.db.championships.SingleOrDefault(x => x.Name.ToLower() == "todoscampeonatos");
            var todasepocas = FilesAccess.db.seasons.SingleOrDefault(x => x.name.ToLower() == "todasepocas");
            var isTodosCampeonatosAndEpocas = champ!=null ? champ.Contains(todosCampeonatos.Id) && todasepocas.startDate == start && todasepocas.endDate == end : false;
            for (int i = 0; i < players.Count; i++)
            {
                if (isTodosCampeonatosAndEpocas)
                    players[i].currentPoints = players[i].basePoints;
                else
                    players[i].currentPoints = 1000;

                players[i].currentGamesLost = 0;
                players[i].currentGamesWon = 0;
                players[i].currentRank = 0;
            }

            players = CalculateRankGames(allSinglesGames, allDoublesGames, players, updateGamesPoints);

            for (int i = 0; i < players.Count; i++)
            {
                if (players[i].currentGamesLost + players[i].currentGamesWon == 0)
                    players[i].currentPoints = -1;
            }

            if (Utilities.IsChampOpenTournament(champ))
            {
                playerRow[] temp = FilesAccess.db.player.OrderByDescending(x => x.currentPoints).ToArray();
                int rank = 1;
                for (int i = 0; i < temp.Length; i++)
                {
                    //Check if the player is part of the league
                    if (FilesAccess.db.playerInscriptionLeague.Count(x => x.playerId == temp[i].Id) > 0)
                    {
                        players.Single(x => x.Id == temp[i].Id).currentRank = (rank);
                        rank++;
                    }
                }

                if (champ.Count > 1)
                {
                    for (int i = 0; i < FilesAccess.db.playerInscriptionLeague.Count; i++)
                    {
                        playerinscriptionleagueRow plilRow = FilesAccess.db.playerInscriptionLeague[i];
                        FilesAccess.db.playerInscriptionLeague[i].currentPoints = players.Single(x => x.Id == plilRow.playerId).currentPoints;
                        FilesAccess.db.playerInscriptionLeague[i].currentGamesWon = players.Single(x => x.Id == plilRow.playerId).currentGamesWon;
                        FilesAccess.db.playerInscriptionLeague[i].currentGamesLost = players.Single(x => x.Id == plilRow.playerId).currentGamesLost;

                        FilesAccess.db.playerInscriptionLeague[i].currentRank = players.Single(x => x.Id == plilRow.playerId).currentRank;
                    }

                    FilesAccess.db.UpdateLeagueInscription();
                }
                else
                {
                    List<playerleaguetournamentRow> champsRow = ((playerleaguetournamentRow[])FilesAccess.db.playerLeagueTournament.Select("championshipId=" + champ[0])).ToList();
                    for (int i = 0; i < champsRow.Count; i++)
                    {
                        playerleaguetournamentRow plilRow = champsRow[i];
                        champsRow[i].currentPoints = players.Single(x => x.Id == plilRow.playerId).currentPoints;
                        champsRow[i].currentGamesWon = players.Single(x => x.Id == plilRow.playerId).currentGamesWon;
                        champsRow[i].currentGamesLost = players.Single(x => x.Id == plilRow.playerId).currentGamesLost;
                        champsRow[i].currentRank = players.Single(x => x.Id == plilRow.playerId).currentRank;
                    }
                    FilesAccess.db.UpdatePlayerLeagueTournament();
                }
            }
            else
            {
                //Do the generic ranking:
                players = players.OrderByDescending(x => x.currentPoints).ToList();
                for (int i = 0; i < players.Count; i++)
                {
                    //localPlayers.Single(x => x.Id == temp[i].Id).currentRank = (i + 1);
                    players[i].currentRank = (i + 1);
                }
            }
            var output = new List<playerRow>();
            output.AddRange(players.Where(x => x.currentGamesLost + x.currentGamesWon > 0));
            return output;
        }

        private void UpdateGamesPoints(List<ListGames> allGames)
        {
            foreach (var game in allGames)
            {
                var currentGame = FilesAccess.db.games.SingleOrDefault(x => x.id == game.Id && game.IsDouble == false);
                if (currentGame != null)
                {
                    currentGame.pointsPlayerA = game.PointsPlayerA;
                    currentGame.pointsPlayerB = game.PointsPlayerB;
                    currentGame.pointsBeforeGamePlayerA = (int)game.PointsBeforeGamePlayerA;
                    currentGame.pointsBeforeGamePlayerB = (int)game.PointsBeforeGamePlayerB;
                }

                var currentdoubleGame = FilesAccess.db.doubleGames.SingleOrDefault(x => x.Id == game.Id && game.IsDouble == true);
                if (currentdoubleGame != null)
                {
                    currentdoubleGame.PointsDoubleA_PlayerA = game.PointsPlayerA / 2;
                    currentdoubleGame.PointsDoubleA_PlayerB = game.PointsPlayerA / 2;
                    currentdoubleGame.PointsDoubleB_PlayerA = game.PointsPlayerB / 2;
                    currentdoubleGame.PointsDoubleB_PlayerB = game.PointsPlayerB / 2;

                    currentdoubleGame.pointsBeforeGameDoubleA_PlayerA = (int)game.PointsBeforeGamePlayerA >> 16;
                    currentdoubleGame.pointsBeforeGameDoubleA_PlayerB = (int)(game.PointsBeforeGamePlayerA & 8);

                    currentdoubleGame.pointsBeforeGameDoubleB_PlayerA = (int)game.PointsBeforeGamePlayerB >> 16;
                    currentdoubleGame.pointsBeforeGameDoubleB_PlayerB = (int)(game.PointsBeforeGamePlayerB & 8);
                }
            }
            if (!FilesAccess.db.UpdateGames())
                MessageBox.Show("Error while updating players points in games!");

            if (!FilesAccess.db.UpdateDoubleGames())
                MessageBox.Show("Error while updating players points in double games!");
        }

        private List<playerRow> CalculateRankGames(List<gamesRow> allGamesSingle, List<doublegamesRow> allGamesDouble, List<playerRow> localPlayers, bool updateGamePoints=false)
        {
            try
            {
                var allGames = new List<ListGames>();
                var allGames1 = new List<ListGames>();
                var allGames2 = new List<ListGames>();
                //create a class that is the same in both, and use the select, union to allow to have all in the same object
                if (allGamesSingle != null)
                {
                    allGames1 = (from g in allGamesSingle
                                 where !g.ignoreOnRankings
                                select new ListGames()
                                {
                                    Id = g.id,
                                    Date = g.Date,
                                    IdPlayerA = g.IdPlayerA,
                                    IdPlayerB = g.IdPlayerB,
                                    IdPlayerWinner = g.IdPlayerWinner,
                                    IdChamp = g.IdChamp,
                                    PointsPlayerA = g.pointsPlayerA,
                                    PointsPlayerB = g.pointsPlayerB,
                                    IsDouble = false,
                                    IdTeamGame = g.IdTeamGame
                                }).ToList();
                }
                if (allGamesDouble != null)
                {
                    allGames2 = (from g in allGamesDouble
                                where !g.ignoreOnRankings
                                select new ListGames()
                                {
                                    Id = g.Id,
                                    Date = g.Date,
                                    IdPlayerA = g.DoubleA_PlayerAId << 16 | g.DoubleA_PlayerBId,
                                    IdPlayerB = g.DoubleB_PlayerAId << 16 | g.DoubleB_PlayerBId,
                                    IdPlayerWinner = g.IdPlayerAWinner << 16 | g.IdPlayerBWinner,
                                    IdChamp = g.IdChampionship,
                                    PointsPlayerA = g.PointsDoubleA_PlayerA << 16 | g.PointsDoubleA_PlayerB,
                                    PointsPlayerB = g.PointsDoubleB_PlayerA << 16 | g.PointsDoubleB_PlayerB,
                                    IsDouble = true,
                                    IdTeamGame = g.IdTeamGame
                                }).ToList();
                }

                if (allGames1 != null && allGames2 != null)
                    allGames = allGames1.Union(allGames2).OrderBy(x => x.Date).ToList();
                else if (allGames1 != null)
                    allGames = allGames1.OrderBy(x => x.Date).ToList();
                else if (allGames2 != null)
                    allGames = allGames2.OrderBy(x => x.Date).ToList();
                

                for (int i = 0; i < allGames.Count; i++)
                {
                    if (allGames[i].IdPlayerA > -1 && allGames[i].IdPlayerB > -1 && allGames[i].IdChamp > -1)
                    {
                        int clubAId = -1;
                        int clubBId = -1;
                        if (allGames[i].IdTeamGame > 0)
                        {
                            var tg = FilesAccess.db.teamGames.Single(x => x.Id == allGames[i].IdTeamGame);
                            clubAId = FilesAccess.db.clubs.Single(x => x.Id == tg.ClubAId).Id;
                            clubBId = FilesAccess.db.clubs.Single(x => x.Id == tg.ClubBId).Id;
                        }
                        try
                        {
                            if (allGames[i].IsDouble)
                            {
                                playerRow doubleAplayerA = localPlayers.Single(x => x.Id == allGames[i].IdPlayerA >> 16);
                                playerRow doubleAplayerB = localPlayers.Single(x => x.Id == (allGames[i].IdPlayerA & 0xFFFF));

                                playerRow doubleBplayerA = localPlayers.Single(x => x.Id == allGames[i].IdPlayerB >> 16);
                                playerRow doubleBplayerB = localPlayers.Single(x => x.Id == (allGames[i].IdPlayerB & 0xFFFF));
                            
                                DoCalculation(
                                    allGames[i], 
                                    new List<playerRow>() { doubleAplayerA, doubleAplayerB }, 
                                    new List<playerRow>() { doubleBplayerA, doubleBplayerB });

                                var tmp = allGamesDouble.Single(x => x.Id == allGames[i].Id);
                                tmp.PointsDoubleA_PlayerA = allGames[i].PointsPlayerA / 2;
                                tmp.PointsDoubleA_PlayerB = allGames[i].PointsPlayerA / 2;

                                tmp.PointsDoubleB_PlayerA = allGames[i].PointsPlayerB / 2;
                                tmp.PointsDoubleB_PlayerB = allGames[i].PointsPlayerB / 2;

                                tmp.pointsBeforeGameDoubleA_PlayerA = doubleAplayerA.currentPoints;
                                tmp.pointsBeforeGameDoubleA_PlayerB = doubleAplayerB.currentPoints;

                                tmp.pointsBeforeGameDoubleB_PlayerA = doubleBplayerA.currentPoints;
                                tmp.pointsBeforeGameDoubleB_PlayerB = doubleBplayerB.currentPoints;

                                allGames[i].PointsBeforeGamePlayerA = doubleAplayerA.currentPoints << 16 | doubleAplayerB.currentPoints;
                                allGames[i].PointsBeforeGamePlayerB = doubleBplayerA.currentPoints << 16 | doubleBplayerB.currentPoints;

                                if (clubAId > -1)
                                {
                                    doubleAplayerA.IdClub = clubAId;
                                    doubleAplayerB.IdClub = clubAId;
                                }
                                if (clubBId > -1)
                                {
                                    doubleBplayerA.IdClub = clubBId;
                                    doubleBplayerB.IdClub = clubBId;
                                }
                            }
                            else
                            {
                                playerRow playerA = localPlayers.Single(x => x.Id == allGames[i].IdPlayerA);
                                playerRow playerB = localPlayers.Single(x => x.Id == allGames[i].IdPlayerB);
                                DoCalculation(
                                    allGames[i], 
                                    new List<playerRow>() { playerA },
                                    new List<playerRow>() { playerB });

                                var tmp = allGamesSingle.Single(x => x.id == allGames[i].Id);
                                tmp.pointsPlayerA = allGames[i].PointsPlayerA;
                                tmp.pointsPlayerB = allGames[i].PointsPlayerB;

                                tmp.pointsBeforeGamePlayerA = playerA.currentPoints;
                                tmp.pointsBeforeGamePlayerB = playerB.currentPoints;

                                allGames[i].PointsBeforeGamePlayerA = playerA.currentPoints;
                                allGames[i].PointsBeforeGamePlayerB = playerB.currentPoints;

                                if (clubAId > -1)
                                {
                                    playerA.IdClub = clubAId;
                                }
                                if (clubBId > -1)
                                {
                                    playerB.IdClub = clubBId;
                                }
                            }

                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                    }
                }

                if (updateGamePoints)
                    UpdateGamesPoints(allGames);

                return localPlayers;
            }
            catch (Exception ex)
            {
                Trace.TraceError($"[Exception on CalculateRankGames]: {ex.Message}");
                return localPlayers;
            }
        }

        private void DoCalculation(ListGames currentGame, List<playerRow> playerA, List<playerRow> playerB)
        {
            Tuple<int, int> pointsToDistribuite = AwardPoints(currentGame.IdPlayerWinner, playerA, playerB);
            championshipsRow champRow = FilesAccess.db.championships.Single(x => x.Id == currentGame.IdChamp);
            float weight = FilesAccess.db.tournamentType.Single(x => x.id == champRow.idTournamentType).weight;

            float weightA = 0;
            float weightB = 0;
            int numGamesA = 0;
            int numGamesB = 0;
            foreach(var pa in playerA)
            {
                numGamesA += FilesAccess.db.games.Count(x => (x.IdPlayerA == pa.Id || x.IdPlayerB == pa.Id) && x.Date < currentGame.Date);
            }

            foreach (var pb in playerB)
            {
                numGamesB += FilesAccess.db.games.Count(x => (x.IdPlayerA == pb.Id || x.IdPlayerB == pb.Id) && x.Date < currentGame.Date);
            }

            if (numGamesA / playerA.Count > 20)
                numGamesA = 20;
            if (numGamesB / playerB.Count > 20)
                numGamesB = 20;

            weightA = ((4.95f * numGamesB + 1) / 100.0f) * weight;
            weightB = ((4.95f * numGamesA + 1) / 100.0f) * weight;
            //weightA = weight;
            //weightB = weight;

            //if (numGamesA < 5)
            //    weightB = 1;
            //else
            //    weightB = weight;
            //if (numGamesB < 5)
            //    weightA = 1;
            //else
            //    weightA = weight;

            int pointsWinner = Convert.ToInt32(pointsToDistribuite.Item2 * weight);
            int pointsLoser = Convert.ToInt32((pointsToDistribuite.Item2 / 2) * 1); //Before did not have the weight, test with the weight to see how the ranking goes 

            int playerAId = playerA[0].Id;

            if (playerA.Count > 1)
                playerAId = playerA[0].Id << 16 | playerA[1].Id;

            if (playerAId == currentGame.IdPlayerWinner)
            {
                playerA.ForEach(x => x.currentPoints += pointsWinner);
                playerB.ForEach(x => x.currentPoints -= (int)(pointsLoser * weightB));
                currentGame.PointsPlayerA = pointsWinner;
                currentGame.PointsPlayerB = -(int)(pointsLoser * weightB);

                playerA.ForEach(x => x.currentGamesWon++);
                playerB.ForEach(x => x.currentGamesLost++);
            }
            else
            {
                playerB.ForEach(x => x.currentPoints += pointsWinner);
                playerA.ForEach(x => x.currentPoints -= (int)(pointsLoser * weightA));
                currentGame.PointsPlayerB = pointsWinner;
                currentGame.PointsPlayerA = -(int)(pointsLoser * weightA);
                playerB.ForEach(x => x.currentGamesWon++);
                playerA.ForEach(x => x.currentGamesLost++);
            }

            if (!championshipsIncluded.Contains(new Tuple<DateTime, championshipsRow>(currentGame.Date, champRow)))
                championshipsIncluded.Add(new Tuple<DateTime, championshipsRow>(currentGame.Date, champRow));
        }

        private bool IsWinnerExpected(long winnerId, List<playerRow> playerA, List<playerRow> playerB)
        {
            var currentPointsA = playerA.Sum(x => x.currentPoints);
            var currentPointsB = playerB.Sum(x => x.currentPoints);

            int playerAId = playerA[0].Id;

            if(playerA.Count > 1)
                playerAId = playerA[0].Id << 16 | playerA[1].Id;

            int playerBId = playerB[0].Id;

            if (playerB.Count > 1)
                playerBId = playerB[0].Id << 16 | playerB[1].Id;

            if (currentPointsA >= currentPointsB && winnerId == playerAId ||
                currentPointsB >= currentPointsA && winnerId == playerBId)
                return true;
            else
                return false;
        }

        private Tuple<int, int> AwardPoints(long winnerId, List<playerRow> playerA, List<playerRow> playerB)
        {
            List<Tuple<int, int>> pointsExpected = new List<Tuple<int, int>>();
            List<Tuple<int, int>> pointsUnexpected = new List<Tuple<int, int>>();

            List<Tuple<int, int, int>> preConfiguredPoints = new List<Tuple<int, int, int>>();

            #region OriginalPoints
            //preConfiguredPoints.Add(new Tuple<int, int, int>(-1, 8, 8));
            //preConfiguredPoints.Add(new Tuple<int, int, int>(1, 8, 8));
            //preConfiguredPoints.Add(new Tuple<int, int, int>(12, 8, 8));
            //preConfiguredPoints.Add(new Tuple<int, int, int>(37, 7, 10));
            //preConfiguredPoints.Add(new Tuple<int, int, int>(62, 6, 13));
            //preConfiguredPoints.Add(new Tuple<int, int, int>(87, 5, 16));
            //preConfiguredPoints.Add(new Tuple<int, int, int>(112, 4, 20));
            //preConfiguredPoints.Add(new Tuple<int, int, int>(137, 3, 25));
            //preConfiguredPoints.Add(new Tuple<int, int, int>(162, 2, 30));
            //preConfiguredPoints.Add(new Tuple<int, int, int>(187, 2, 35));
            //preConfiguredPoints.Add(new Tuple<int, int, int>(212, 1, 40));
            //preConfiguredPoints.Add(new Tuple<int, int, int>(237, 1, 45));
            //preConfiguredPoints.Add(new Tuple<int, int, int>(238, 0, 50));
            #endregion

            #region CurrentPoints
            //preConfiguredPoints.Add(new Tuple<int, int, int>(-1, 8, 8));
            //preConfiguredPoints.Add(new Tuple<int, int, int>(1, 8, 8));
            //preConfiguredPoints.Add(new Tuple<int, int, int>(20, 7, 12));
            //preConfiguredPoints.Add(new Tuple<int, int, int>(50, 6, 17));
            //preConfiguredPoints.Add(new Tuple<int, int, int>(75, 5, 15));
            //preConfiguredPoints.Add(new Tuple<int, int, int>(100, 4, 17));
            //preConfiguredPoints.Add(new Tuple<int, int, int>(150, 4, 22));
            //preConfiguredPoints.Add(new Tuple<int, int, int>(225, 3, 29));
            //preConfiguredPoints.Add(new Tuple<int, int, int>(300, 2, 36));
            //preConfiguredPoints.Add(new Tuple<int, int, int>(400, 1, 45));
            //preConfiguredPoints.Add(new Tuple<int, int, int>(500, 1, 54));
            //preConfiguredPoints.Add(new Tuple<int, int, int>(800, 1, 82));
            //preConfiguredPoints.Add(new Tuple<int, int, int>(1000, 1, 100));
            #endregion

            #region Points TableTennis365.com
            preConfiguredPoints.Add(new Tuple<int, int, int>(-1, 8, 8));
            preConfiguredPoints.Add(new Tuple<int, int, int>(25, 7, 9));
            preConfiguredPoints.Add(new Tuple<int, int, int>(50, 6, 11));
            preConfiguredPoints.Add(new Tuple<int, int, int>(100, 5, 14));
            preConfiguredPoints.Add(new Tuple<int, int, int>(150, 4, 17));
            preConfiguredPoints.Add(new Tuple<int, int, int>(200, 3, 22));
            preConfiguredPoints.Add(new Tuple<int, int, int>(300, 2, 30));
            preConfiguredPoints.Add(new Tuple<int, int, int>(400, 1, 40));
            preConfiguredPoints.Add(new Tuple<int, int, int>(500, 1, 50));
            #endregion

            for (int i = 0; i < preConfiguredPoints.Count; i++)
            {
                pointsExpected.Add(new Tuple<int, int>(preConfiguredPoints[i].Item1, preConfiguredPoints[i].Item2));
                pointsUnexpected.Add(new Tuple<int, int>(preConfiguredPoints[i].Item1, preConfiguredPoints[i].Item3));
            }

            #region Backup points
            //pointsExpected.Add(new Tuple<int, int>(-1, 9));
            //pointsExpected.Add(new Tuple<int, int>(1, 9));
            //pointsExpected.Add(new Tuple<int, int>(26, 8));
            //pointsExpected.Add(new Tuple<int, int>(51, 7));
            //pointsExpected.Add(new Tuple<int, int>(101, 6));
            //pointsExpected.Add(new Tuple<int, int>(151, 5));
            //pointsExpected.Add(new Tuple<int, int>(201, 4));
            //pointsExpected.Add(new Tuple<int, int>(301, 3));
            //pointsExpected.Add(new Tuple<int, int>(401, 2));
            //pointsExpected.Add(new Tuple<int, int>(501, 1));
            //pointsExpected.Add(new Tuple<int, int>(750, 0));

            //pointsUnexpected.Add(new Tuple<int, int>(-1, 10));
            //pointsUnexpected.Add(new Tuple<int, int>(0, 10));
            //pointsUnexpected.Add(new Tuple<int, int>(25, 12));
            //pointsUnexpected.Add(new Tuple<int, int>(50, 14));
            //pointsUnexpected.Add(new Tuple<int, int>(100, 16));
            //pointsUnexpected.Add(new Tuple<int, int>(150, 20));
            //pointsUnexpected.Add(new Tuple<int, int>(200, 24));
            //pointsUnexpected.Add(new Tuple<int, int>(300, 28));
            //pointsUnexpected.Add(new Tuple<int, int>(400, 32));
            //pointsUnexpected.Add(new Tuple<int, int>(500, 36));
            //pointsUnexpected.Add(new Tuple<int, int>(749, 40));
            #endregion

            var currentPointsPlayerA = playerA.Sum(x => x.currentPoints * (1.0d / playerA.Count));
            var currentPointsPlayerB = playerB.Sum(x => x.currentPoints * (1.0d / playerB.Count));

            double dif = Math.Abs(currentPointsPlayerA - currentPointsPlayerB);
            for (int i = pointsExpected.Count-1; i > -1; i--)
            {
                if (dif > pointsExpected[i].Item1)
                {
                    if (IsWinnerExpected(winnerId, playerA, playerB))
                        return pointsExpected[i];
                    else
                        return pointsUnexpected[i];
                }
            }
            return new Tuple<int, int>(0, 0);
        }
    }
}
