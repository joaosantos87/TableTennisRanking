﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static TableTennis.Database.DataSet2;

namespace TableTennis.Database
{
    public static class rankingRow_equals
    {
        public static bool IsUpdate(this rankingRow rankRow, object obj)
        {
            var obj1 = obj as rankingRow;

            if (obj1.idPlayer != rankRow.idPlayer)
                return true;

            if (obj1.idChamp != rankRow.idChamp)
                return true;

            if (obj1.idSeason != rankRow.idSeason)
                return true;

            if (obj1.numGames != rankRow.numGames)
                return true;

            if (obj1.numWins != rankRow.numWins)
                return true;

            if (obj1.numLosses != rankRow.numLosses)
                return true;

            if (obj1.rank != rankRow.rank)
                return true;

            if (obj1.points != rankRow.points)
                return true;

            return false;
        }
    }
}
