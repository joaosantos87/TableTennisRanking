﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TableTennis.Database
{
    public class ListGames
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public long IdPlayerA { get; set; }
        public long IdPlayerB { get; set; }
        public bool IsDouble { get; set; }
        public int IdChamp { get; set; }
        public long IdPlayerWinner { get; set; }
        public int PointsPlayerA { get; set; }
        public int PointsPlayerB { get; set; }
        public int IdTeamGame { get; set; }
        public long PointsBeforeGamePlayerA { get; set; }
        public long PointsBeforeGamePlayerB { get; set; }
    }
}
