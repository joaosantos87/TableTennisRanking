﻿using System;

namespace TableTennis.Controls.General
{
    internal interface IInsertGame
    {
        void UpdatePlayersCombobox();
        void CreateGame(DateTime date, string champ, string phase, int idTeamGame);

        void UpdateInterface();
    }
}