﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace TableTennis.General.WebScraping
{

    public class Player
    {
        public int Id { get; set; }
        public string Licenca { get; set; }
        public string Nome { get; set; }
        public string Image { get; set; }

        public Bitmap GetImage
        {
            get
            {
                System.Net.WebRequest request = System.Net.WebRequest.Create(Image);
                System.Net.WebResponse response = request.GetResponse();
                System.IO.Stream responseStream =
                    response.GetResponseStream();
                return new Bitmap(responseStream);
            }
        }
        public Player(int id, string licenca, string nome)
        {
            Id = Id;
            Licenca = licenca;
            Nome = nome;
        }

        public Player()
        {
            Nome = "";
            Licenca = "";
            Id = -1;
            Image = "";
        }

        public override bool Equals(object obj)
        {
            var obj1 = obj as Player;

            return obj1.Licenca == Licenca && obj1.Nome == Nome;
        }
    }
}
