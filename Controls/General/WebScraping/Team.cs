﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TableTennis.General.WebScraping
{
    public class Team
    {
        private List<Player> jogadores = new List<Player>();
        public string Nome { get; }
        public string AppendText { get; }
        public IEnumerable<Player> Jogadores { get { return jogadores; } }
        public void AddPlayer(Player player)
        {
            jogadores.Add(player);
        }
        public Team(string name)
        {
            name = name.Trim();
            if (name.EndsWith("\""))
            {
                var value = name.Split('"');
                Nome = value[0].Trim();
                AppendText = value[1].Trim();
                if (AppendText == null)
                    AppendText = "";
            }
            else
                Nome = name;
        }
    }
}
