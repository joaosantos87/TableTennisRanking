﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TableTennis.General.WebScraping
{
    public class DetailedDoublesGame : DetailedGame
    {
        public Player playerA_1 { get; set; }
        public Player playerA_2 { get; set; }
        public Player playerB_1 { get; set; }
        public Player playerB_2 { get; set; }
        public DetailedDoublesGame(Player playerA_1, Player playerA_2, Player playerB_1, Player playerB_2, int setsA, int setsB, string summary)
        {
            this.playerA_1 = playerA_1;
            this.playerA_2 = playerA_2;
            this.playerB_1 = playerB_1;
            this.playerB_2 = playerB_2;
            this.setsA = setsA;
            this.setsB = setsB;
            this.summary = summary;
        }
        public DetailedDoublesGame()
        {
            this.playerA_1 = null;
            this.playerA_2 = null;
            this.playerB_1 = null;
            this.playerB_2 = null;
            this.setsA = -1;
            this.setsB = -1;
            this.summary = "";
        }

        public override string ToString()
        {
            return $"{playerA_1.Nome}/{playerA_2.Nome} {setsA}-{setsB} {playerB_1.Nome}/{playerB_2.Nome}";
        }
    }
}
