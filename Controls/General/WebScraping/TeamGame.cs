﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TableTennis.General.WebScraping
{
    public class TeamGame
    {
        private bool occuredError = false;
        public List<DetailedSinglesGame> SingleGames { get; set; }
        public DetailedDoublesGame DoublesGame { get; set; }
        public DateTime Date { get; set; }
        public Team TeamA { get; set; }
        public Team TeamB { get; set; }
        public int ResultA { get; set; }
        public int ResultB { get; set; }
        public string LinkDetailedResults { get; set; }
        public string MatchDay { get; set; }
        public int GetMatchDayInt
        {
            get
            {
                if (Int32.TryParse(MatchDay, out int day))
                    return day;

                return -1;
            }
        }
        public bool OccuredError { get { return occuredError; } }
        public string Torneio { get; set; }
        public TeamGame()
        {
            Torneio = "";
            ResultA = -1;
            ResultB = -1;
            MatchDay = "";
            Date = DateTime.MinValue;
            SingleGames = new List<DetailedSinglesGame>();
        }
        public void AddError()
        {
            occuredError = true;
        }
        public void RemoveError()
        {
            occuredError = false;
        }

        public override string ToString()
        {
            return $"[{MatchDay}][{Date.ToString("dd-MM-yyyy")}]{TeamA?.Nome}   {ResultA} - {ResultB} {TeamB?.Nome}";
        }

        public bool Inserted
        {
            get
            {
                return SingleGames.Count > 0 && DoublesGame != null;
            }
        }

    }
}