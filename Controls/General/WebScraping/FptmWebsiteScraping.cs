﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;

namespace TableTennis.General.WebScraping
{
    public class FptmWebsiteScraping
    {
        private int doubleGamePosition;
        List<TeamGame> allTeamGames = new List<TeamGame>();
        public IEnumerable<TeamGame> AllTeamGames
        { 
            get
            {
                return allTeamGames;
            }
        }
        public FptmWebsiteScraping(string url, int doublesGamePosition)
        {
            this.doubleGamePosition = doublesGamePosition;
            allTeamGames = GetAllGames(url);
        }
        // Parses the URL and returns HtmlDocument object
        private List<TeamGame> GetAllGames(string url)
        {
            HtmlWeb web = new HtmlWeb();
            HtmlDocument doc = web.Load(url);


            var torneio = doc.DocumentNode.SelectNodes("//h2[@class = 'tournament-title']").FirstOrDefault();

            var nodes = doc.DocumentNode.SelectNodes("//table[@class = 'w-full game-day-table']/tbody");
            var matchDays = doc.DocumentNode.SelectNodes("//h3[@class = 'matchday-title']");
            var generalDates = doc.DocumentNode.SelectNodes("//div[@class = 'w-1/2 md:w-1/3 text-right']");
            List<TeamGame> allTeamGames = new List<TeamGame>();
            int currentIndex = 0;
            foreach (var table in nodes)
            {
                foreach (var row in table.Elements("tr"))
                {
                    TeamGame tg = new TeamGame();

                    if (torneio != null)
                        tg.Torneio = torneio.InnerText.Trim();
                    else
                        tg.Torneio = "N/A";

                    var day = matchDays[currentIndex];
                    var generalDateNode = generalDates[currentIndex].ChildNodes[1].InnerText;

                    DateTime generalDate = DateTime.ParseExact(generalDateNode, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);

                    var d = day.InnerText.Split('ª');
                    if (d.Length == 2)
                        tg.MatchDay = d[0];
                    else
                        tg.MatchDay = day.InnerText;
                    foreach (var cell in row.Elements("td"))
                    {
                        //in the first element, check if it has a element "a", if so check if it has an attribute "href", if so get the value that is the detailed result of that game
                        if (tg.LinkDetailedResults == null && cell.FirstChild != null)
                        {
                            if (cell.FirstChild.Attributes.Contains("href"))
                                tg.LinkDetailedResults = cell.FirstChild.Attributes["href"].Value;
                            continue;
                        }

                        if (tg.TeamA == null)
                        {
                            tg.TeamA = new Team(cell.InnerText.Trim());
                            continue;
                        }

                        if (tg.ResultA == -1)
                        {
                            if (cell.InnerText.Trim() == String.Empty)
                            {
                                tg.ResultA = 0;
                            }
                            else
                            {
                                tg.ResultA = Convert.ToInt32(cell.InnerText.Trim());
                            }
                            continue;
                        }

                        if (tg.ResultB == -1)
                        {
                            if (cell.InnerText.Trim() == String.Empty)
                            {
                                tg.ResultB = 0;
                            }
                            else
                            {
                                tg.ResultB = Convert.ToInt32(cell.InnerText.Trim());
                            }
                            continue;
                        }

                        if (tg.TeamB == null)
                        {
                            tg.TeamB = new Team(cell.InnerText.Trim());
                            continue;
                        }

                        if (tg.Date == DateTime.MinValue)
                        {
                            try
                            {
                                tg.Date = DateTime.ParseExact(cell.InnerText.Trim(), "yyyy-MM-dd HH:mm", System.Globalization.CultureInfo.InvariantCulture);
                            }
                            catch (Exception)
                            {
                                try
                                {
                                    tg.Date = generalDate;
                                }
                                catch (Exception ex)
                                {
                                    Trace.TraceError("Parsing exact date error: " + ex.Message);
                                    tg.Date = DateTime.MinValue;
                                }
                            }
                            continue;
                        }

                    }

                    allTeamGames.Add(tg);
                }
                currentIndex++;
            }
            int currentRetry = 0;
        retry:
            if (currentRetry < 3)
            {
                foreach (var tg in allTeamGames)
                {
                    if ((tg.OccuredError || !tg.Inserted) && tg.Date < DateTime.Now.AddDays(1))
                        GetSpecificGame($"https://fptm.pt{tg.LinkDetailedResults}", tg);

                    if (tg.OccuredError)
                        Thread.Sleep(100);

                    Thread.Sleep(10);
                }

                if (allTeamGames.Count(x => x.OccuredError) > 0)
                {
                    currentRetry++;
                    Trace.TraceInformation($"Errors at {allTeamGames.Count(x => x.OccuredError)} games. Retry #{currentRetry}...");
                    Thread.Sleep(1000);
                    goto retry;
                }
            }
            Trace.TraceInformation($"Finalized scrapping...returning {allTeamGames.Count} team games");
            return allTeamGames;
        }

        private void GetSpecificGame(string url, TeamGame tg)
        {
            HtmlWeb web = new HtmlWeb();
            HtmlDocument doc = web.Load(url);
            List<Player> playersList = new List<Player>();
            bool doneInsertingTeamA = false;
            //Get the player details table
            var nodes = doc.DocumentNode.SelectNodes("//table[starts-with(@class,'player-grid w-full')]/tbody");

            if (nodes == null || nodes.Count == 0)
            {
                Trace.TraceError(tg.ToString());
                tg.AddError();
                return;
            }

            //if(tg.MatchDay != "9")
            //{
            //    return;
            //    Trace.TraceInformation("teste");
            //}

            foreach (var table in nodes)
            {
                foreach (var row in table.Elements("tr"))
                {
                    Player p = new Player();
                    // ...do <tr> stuff
                    foreach (var cell in row.Elements("td"))
                    {
                        // ... do <td> stuff
                        if (p.Id == -1)
                        {
                            p.Id = Convert.ToInt32(cell.InnerText);
                            continue;
                        }
                        if (p.Image == String.Empty)
                        {
                            try
                            {

                                String image = cell.Attributes[1].Value.Substring(23);

                                if (image != null && image.Length != 0)
                                    p.Image = image;
                                else
                                    p.Image = "N/A";
                            }
                            catch (Exception ex)
                            {
                                Trace.TraceError($"Parsing image URL error: {ex.Message}");
                            }
                            continue;
                        }

                        if (p.Nome == String.Empty)
                        {
                            var nome = cell.InnerText;
                            if (nome != String.Empty)
                                p.Nome = cell.InnerText;
                            else
                            {
                                p.Nome = "N/A";
                            }
                            continue;
                        }

                        if (p.Licenca == String.Empty)
                        {
                            var licenca = cell.InnerText;

                            if (licenca != String.Empty)
                                p.Licenca = cell.InnerText;
                            else
                                p.Licenca = "N/A";

                            if (p.Nome == "N/A" && p.Licenca != "N/A")
                            {
                                //search for the name using the license

                            }
                        }
                    }

                    if (playersList.Count(x => x.Nome == p.Nome) == 0 || p.Nome=="N/A")
                        playersList.Add(p);

                    if (!doneInsertingTeamA)
                        tg.TeamA.AddPlayer(p);
                    else
                        tg.TeamB.AddPlayer(p);
                }

                doneInsertingTeamA = true;
            }

            //get the game results details
            //var nodesGameDetails = doc.DocumentNode.SelectNodes("//div[starts-with(@class,'flex partial-row')]");
            //foreach (var node in nodesGameDetails)
            //{
            DetailedDoublesGame doublesGame = new DetailedDoublesGame();
            DetailedSinglesGame singlesGame = new DetailedSinglesGame();
            //Way to get only the names of the games. Gets all the names in the Home VS Away order, and top down order
            foreach (var row in doc.DocumentNode.SelectNodes("//p[@class = 'my-auto']"))
            {
                var value = row.InnerText;

                //doubles game
                if (value.Contains('/'))
                {
                    var players = value.Split('/').ToList();

                    if (players.Count == 2)
                    {
                        var player_1 = players[0].Trim();
                        var player_2 = players[1].Trim();

                        if (doublesGame.playerA_1 == null && doublesGame.playerA_2 == null)
                        {
                            doublesGame.playerA_1 = playersList.SingleOrDefault(x => x.Nome == player_1);
                            doublesGame.playerA_2 = playersList.SingleOrDefault(x => x.Nome == player_2);
                            continue;
                        }
                        if (doublesGame.playerB_1 == null && doublesGame.playerB_2 == null)
                        {
                            doublesGame.playerB_1 = playersList.SingleOrDefault(x => x.Nome == player_1);
                            doublesGame.playerB_2 = playersList.SingleOrDefault(x => x.Nome == player_2);
                            tg.DoublesGame = doublesGame;
                            continue;
                        }
                    }
                    else
                    {
                        throw new Exception($"Number of players in doubles game is not two. Game: {tg}");
                    }
                }
                else
                {

                    var player = value.Trim();

                    if (singlesGame.playerA == null)
                    {
                    retry:
                        singlesGame.playerA = playersList.SingleOrDefault(x => x.Nome == player);

                        if (singlesGame.playerA == null)
                        {
                            playersList.First(x => x.Nome == "N/A").Nome = player;
                            goto retry;
                        }

                        continue;
                    }
                    if (singlesGame.playerB == null)
                    {
                    retry:
                        singlesGame.playerB = playersList.SingleOrDefault(x => x.Nome == player);

                        if (singlesGame.playerB == null)
                        {
                            playersList.First(x => x.Nome == "N/A").Nome = player;
                            goto retry;
                        }
                        tg.SingleGames.Add(singlesGame);
                        singlesGame = new DetailedSinglesGame();
                    }

                }
            }

            //DetailedGame dg = new DetailedGame();

            //Way to get the scores of the games

            int currentGamePosition=0;
            foreach (var row in doc.DocumentNode.SelectNodes("//div[contains(@class,'score')]"))
            {
                var value = row.InnerText.Split('-').ToList();
                if (value.Count == 2)
                {
                    var setsA = Convert.ToInt32(value[0]);
                    var setsB = Convert.ToInt32(value[1]);

                    if (currentGamePosition == doubleGamePosition && tg.DoublesGame != null && !tg.DoublesGame.IsInserted)
                    {
                        tg.DoublesGame.setsA = setsA;
                        tg.DoublesGame.setsB = setsB;
                        currentGamePosition++;
                        continue;
                    }
                    tg.SingleGames.First(x => x.IsInserted == false).setsA = setsA;
                    tg.SingleGames.First(x => x.IsInserted == false).setsB = setsB;
                    currentGamePosition++;
                }
            }
            tg.RemoveError();
        }
    }
}
