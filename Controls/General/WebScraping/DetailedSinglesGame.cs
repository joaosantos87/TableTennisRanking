﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TableTennis.General.WebScraping
{
    public class DetailedSinglesGame : DetailedGame
    {
        public Player playerA { get; set; }
        public Player playerB { get; set; }
        public DetailedSinglesGame(Player playerA, Player playerB, int setsA, int setsB, string summary)
        {
            this.playerA = playerA;
            this.playerB = playerB;
            this.setsA = setsA;
            this.setsB = setsB;
            this.summary = summary;
        }
        public DetailedSinglesGame()
        {
            this.playerA = null;
            this.playerB = null;
        }

        public override string ToString()
        {
            return $"{playerA.Nome} {setsA}-{setsB} {playerB.Nome}";
        }
    }
}
