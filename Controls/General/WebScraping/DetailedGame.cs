﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TableTennis.General.WebScraping
{
    public abstract class DetailedGame
    {
        public int setsA { get; set; }
        public int setsB { get; set; }
        public string summary { get; set; }
        public bool IsInserted
        {
            get { return setsA != -1 && setsB != -1; }
        }
        public DetailedGame()
        {
            this.setsA = -1;
            this.setsB = -1;
            this.summary = "";
        }
    }
}
