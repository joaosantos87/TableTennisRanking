﻿using System;

namespace TableTennis.Controls.General
{
    internal interface IAddGameToTeamgame
    {
        void UpdateGame(int idTeamGame);

        void UpdateList(int idChamp);
    }
}