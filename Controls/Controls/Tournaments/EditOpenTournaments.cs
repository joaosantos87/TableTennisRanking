﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TableTennis.Database;
using static TableTennis.Database.DataSet2;

namespace TableTennis.Controls
{
    public partial class EditOpenTournaments : Form
    {
        public EditOpenTournaments()
        {
            InitializeComponent();
            dataGridView1.RowsAdded += dataGridView1_RowsAdded;
            dataGridView1.DataError += dataGridView1_DataError;
        }

        private void dataGridView1_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            Console.WriteLine(e.Exception.Message);
        }

        private void EditOpenTournaments_Load(object sender, EventArgs e)
        {
            List<championshipsRow> allOpenTournaments = FilesAccess.db.championships.Where(x => x.idTournamentType == Utilities.OpenTournamentId && x.Date > DateTime.Now.Subtract(TimeSpan.FromDays(1))).ToList();

            foreach (championshipsRow row in allOpenTournaments)
            {
                comboBox1.Items.Add(row.Name);
            }

            if (comboBox1.Items.Count > 0)
            {
                comboBox1.Text = comboBox1.Items[0].ToString();
                UpdateInterface();
            }
        }

        private void dataGridView1_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            try
            {
                if (e.RowCount < 1)
                    return;

                for (int i = e.RowIndex; i < e.RowCount + e.RowIndex; i++)
                {
                    int currentId = Convert.ToInt32(dataGridView1["id", i].Value);
                    playerleaguetournamentRow plChampRow = FilesAccess.db.playerLeagueTournament.Single(x => x.id == currentId);
                    playerRow plRow = FilesAccess.db.player.Single(x => x.Id == plChampRow.playerId);
                    clubsRow clRow = FilesAccess.db.clubs.Single(x => x.Id == plRow.IdClub);

                    dataGridView1["Nome", i].Value = plRow.Name;
                    dataGridView1["Clube", i].Value = clRow.Name;
                }
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateInterface();
        }

        private void UpdateInterface()
        {
            try
            {
                championshipsRow champRow = FilesAccess.db.championships.Single(x => x.Name == comboBox1.Text);
                label2.Text = "Data: " + champRow.Date.ToString("dd/MM/yyyy");

                //Update the datagrid view with the info of the guys that have done the inscription for this tournament
                dataGridView1.DataSource = ((playerleaguetournamentRow[])FilesAccess.db.playerLeagueTournament.Select("championshipId="+ champRow.Id)).ToList();
            }
            catch { }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            championshipsRow champRow = FilesAccess.db.championships.Single(x=>x.Name == comboBox1.Text);
            new AddPlayerChampInscription(champRow.Id).ShowDialog();
            UpdateInterface();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FilesAccess.db.UpdatePlayerLeagueTournament();
        }
    }
}