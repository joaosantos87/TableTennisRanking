﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TableTennis.Database;
using TableTennis.Controls.Tournaments;
using static TableTennis.Database.DataSet2;

namespace TableTennis.Controls
{
    public partial class gameScheduleControl : UserControl
    {
        tournamentgamesRow tgRow;
        phaseRow phRow;
        playerRow plA;
        playerRow plB;
        public event EventHandler UpdateAll;

        public gameScheduleControl(int tournamentGameId)
        {
            InitializeComponent();

            tgRow = FilesAccess.db.tournamentGames.Single(x => x.id == tournamentGameId);
            phRow = FilesAccess.db.phase.Single(x => x.id == tgRow.idPhase);
            string roundId = tgRow.roundId != -1 ? FilesAccess.db.tournamentGames.Single(x=>x.id==tournamentGameId).groupId+" - Ronda " + tgRow.roundId : "";
            label2.Text = phRow.phaseShort + "" + roundId;

            if (tgRow.dependPlA!=0)
            {
                try
                {
                    int idGame = FilesAccess.db.tournamentGames.Single(x=>x.id == (-tgRow.dependPlA)).gameId;
                    long idWinner = FilesAccess.db.games.Single(x => x.id == idGame).IdPlayerWinner;
                    plA = FilesAccess.db.player.Single(x => x.Id == idWinner);
                }
                catch { }
            }

            if (tgRow.dependPlB != 0)
            {
                try
                {
                    int idGame = FilesAccess.db.tournamentGames.Single(x => x.id == (-tgRow.dependPlB)).gameId;
                    long idWinner = FilesAccess.db.games.Single(x => x.id == idGame).IdPlayerWinner;
                    plB = FilesAccess.db.player.Single(x => x.Id == idWinner);
                }
                catch { }
            }

            if (plA == null)
            {
                try
                {
                    plA = FilesAccess.db.player.Single(x => x.Id == tgRow.playerAid);
                }
                catch
                {
                    plA = FilesAccess.db.player.Single(x => x.Id == Utilities.PlayerToBeDefined);
                }
            }

            if (plB == null)
            {
                try
                {
                    plB = FilesAccess.db.player.Single(x => x.Id == tgRow.playerBid);
                }
                catch
                {
                    plB = FilesAccess.db.player.Single(x => x.Id == Utilities.PlayerToBeDefined);
                }
            }

            UpdateResult();
        }

        private void UpdateResult()
        {
            int setsA = 0;
            int setsB = 0;
            if (FilesAccess.db.games.Count(x => x.id == tgRow.gameId) > 0)
            {
                DataSet2.gamesRow gameRow = FilesAccess.db.games.Single(x => x.id == tgRow.gameId);

                setsA = gameRow.SetsA;
                setsB = gameRow.SetsB;
            }

            label3.Text = plA.Name + " " + setsA + " - " + setsB + " " + plB.Name;
        }

        private void label3_Click(object sender, EventArgs e)
        {
            new InsertTournGameResult(plA, plB, tgRow).ShowDialog();
            UpdateResult();

            if (UpdateAll != null)
                UpdateAll(this, new EventArgs());
        }

        private void button1_Click(object sender, EventArgs e)
        {
            new ChangePlayer(tgRow).ShowDialog();
            UpdateResult();
            if (UpdateAll != null)
                UpdateAll(this, new EventArgs());
        }
    }
}
