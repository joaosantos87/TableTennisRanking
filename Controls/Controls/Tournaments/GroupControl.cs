﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TableTennis.Database;
using static TableTennis.Database.DataSet2;

namespace TableTennis.Controls
{
    public partial class GroupControl : UserControl
    {
        List<PlayerGroupControl> playersControl;
        List<playerRow> players;
        int champId;
        int groupId;
        List<int> playersOrder = new List<int>();

        public GroupControl(int _champId, int _groupId)
        {
            InitializeComponent();
            champId = _champId;
            groupId = _groupId;

            label1.Text = "Grupo " + groupId;

            //get the number of players in the group
            championshipsRow champRow = FilesAccess.db.championships.Single(x => x.Id == champId);

            List<tournamentgamesRow> tournGamesRows = ((tournamentgamesRow[])FilesAccess.db.tournamentGames.Select("championshipId=" + champId + " and groupId=" + groupId)).ToList();

            players = new List<playerRow>();

            for (int i = 0; i < tournGamesRows.Count; i++)
            {
                playerRow plA = FilesAccess.db.player.Single(x => x.Id == tournGamesRows[i].playerAid);
                if (!players.Contains(plA))
                    players.Add(plA);

                playerRow plB = FilesAccess.db.player.Single(x => x.Id == tournGamesRows[i].playerBid);
                if (!players.Contains(plB))
                    players.Add(plB);
            }

            playersControl = new List<PlayerGroupControl>();
            for (int i = 0; i < players.Count; i++)
            {
                clubsRow club = FilesAccess.db.clubs.Single(x => x.Id == players[i].IdClub);
                playersControl.Add(new PlayerGroupControl(champId, players[i].Name, club.Shortname));
            }

            UpdateControl();
        }

        private bool CheckSamePoints(List<PlayerGroupControl> control)
        {
            for(int i=0; i<control.Count-1;i++)
            {
                if (control[i].PointsTotal == control[i + 1].PointsTotal)
                    return true;
            }
            return false;
        }

        public void UpdateControl()
        {
            foreach (PlayerGroupControl pgc in playersControl)
                pgc.UpdatellGames();

            //playersControl = playersControl.OrderByDescending(x => x.PointsTotal).ThenByDescending(x => x.SetsWonLost).ThenByDescending(x=>x.PointsWonLost).ToList();
            List<int> newOrder = OrderPlayers();

            foreach (PlayerGroupControl pgc in playersControl)
                pgc.UpdatellGames();

            for (int i = 0; i < newOrder.Count; i++)
            {
                flowLayoutPanel1.Controls.Add(playersControl.Single(x=>x.plRow.Id == newOrder[i]));
                playersOrder.Add(newOrder[i]);
                //if(playersControl[i].plRow.Id == newOrder[i])
                //    flowLayoutPanel1.Controls.Add(playersControl[i]);
            }
            UpdateGroupDB();
        }

        private List<int> OrderPlayers()
        {
            playersControl = playersControl.OrderByDescending(x => x.PointsTotal).ToList();
            //if (!CheckSamePoints(playersControl))
            //    return;

            List<int> orderedPlayers = new List<int>();
            List<int> alreadyAnalysedPlayer= new List<int>();

            for (int i = 0; i < playersControl.Count; i++)
            {
                if (!alreadyAnalysedPlayer.Contains(i))
                {
                    List<playerRow> playerEqualPts = new List<playerRow>();
                    for (int j = (i+1); j < playersControl.Count; j++)
                    {
                        if (i == j)
                            break;

                        if (playersControl[i].PointsTotal == playersControl[j].PointsTotal)
                        {
                            if (!alreadyAnalysedPlayer.Contains(i))
                            {
                                playerEqualPts.Add(playersControl[i].plRow);
                                alreadyAnalysedPlayer.Add(i);
                            }

                            if (!alreadyAnalysedPlayer.Contains(j))
                            {
                                playerEqualPts.Add(playersControl[j].plRow);
                                alreadyAnalysedPlayer.Add(j);
                            }
                        }
                    }

                    List<int> newOrder = new List<int>();
                    newOrder = OrderEquals(playerEqualPts);

                    if (newOrder.Count == 0)
                        orderedPlayers.Add(playersControl[i].plRow.Id);
                    else
                    {
                        foreach (int a in newOrder)
                            orderedPlayers.Add(a);
                    }
                }

                //for (int h = 0; h < newOrder.Count; h++)
                //    flowLayoutPanel1.Controls.Add(playersControl.Single(x => x.plRow.Id == newOrder[h]));
            }
            return orderedPlayers;
        }

        private List<int> OrderEquals(List<playerRow> playerEqualPts)
        {
            if (playerEqualPts.Count == 0)
                return new List<int>();

            List<tournamentgamesRow> tournGamesEquals = new List<tournamentgamesRow>();

            for (int i = 0; i < playerEqualPts.Count; i++)
            {
                for (int j = (i+1); j < playerEqualPts.Count; j++)
                {
                    tournGamesEquals.AddRange(((tournamentgamesRow[])FilesAccess.db.tournamentGames.Select("championshipId=" + champId + " and groupId=" + groupId +
                        " and ((playerAid=" + playerEqualPts[i].Id + " and playerBid=" + playerEqualPts[j].Id + ") or (playerAid=" + playerEqualPts[j].Id + " and playerBid=" + playerEqualPts[i].Id + "))")).ToList());
                }
            }

            return Order(tournGamesEquals, playerEqualPts);
        }

        /// <summary>
        /// Receives a list of games and outputs the ordered players id's
        /// </summary>
        /// <param name="games"></param>
        private List<int> Order(List<tournamentgamesRow> games, List<playerRow> playerEqualPts)
        {
            List<PlayerGroupControl> specificPlayersRank = new List<PlayerGroupControl>();

            foreach (PlayerGroupControl playersControl1 in playersControl)
            {
                if (playerEqualPts.Count(x => x.Id == playersControl1.plRow.Id) > 0)
                {
                    playersControl1.UpdateSpecificGames(games);
                    specificPlayersRank.Add(playersControl1);
                }
            }

            specificPlayersRank = specificPlayersRank.OrderByDescending(x => x.PointsTotal).ThenByDescending(x => x.SetsWonLost).ThenByDescending(x => x.PointsWonLost).ToList();
            List<int> outputOrder = new List<int>();
            for (int i = 0; i < specificPlayersRank.Count; i++)
                outputOrder.Add(specificPlayersRank[i].plRow.Id);

            return outputOrder;
        }

        private void UpdateGroupDB()
        {
            bool result;
            if (FilesAccess.db.tournamentGroup.Count(x => x.champId == champId && groupId == x.groupId) == 0)
            {
                FilesAccess.db.tournamentGroup.AddtournamentgroupRow(champId, groupId);
                result = FilesAccess.db.InsertTournamentGroup(FilesAccess.db.tournamentGroup.Last());

                if (result == false)
                {
                    MessageBox.Show("Torneio nao actualizado!");
                    return;
                }
            }

            tournamentgroupRow tgroupRow = FilesAccess.db.tournamentGroup.Single(x => x.champId == champId && groupId == x.groupId);

            for (int i = 0; i < players.Count; i++)
            {
                if (FilesAccess.db.playerTournGroup.Count(x => x.playerId == players[i].Id && x.tournGroupId == tgroupRow.id) == 0)
                {
                    FilesAccess.db.playerTournGroup.AddplayertourngroupRow(players[i].Id, tgroupRow.id, playersOrder.IndexOf(players[i].Id) + 1);
                }
                else
                {
                    FilesAccess.db.playerTournGroup.Single(x => x.tournGroupId == tgroupRow.id && x.playerId == players[i].Id).place = (playersOrder.IndexOf(players[i].Id) + 1);
                }
            }
            result = FilesAccess.db.UpdatePlayerTournamentGroup();

            if (result == false)
            {
                MessageBox.Show("Tabela player tournament group nao actualizado!");
                return;
            }
        }
    }
}
