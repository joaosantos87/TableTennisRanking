﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TableTennis.Database;
using static TableTennis.Database.DataSet2;

namespace TableTennis.Controls.Tournaments
{
    public partial class ChangePlayer : Form
    {
        tournamentgamesRow GameSchRow;

        public ChangePlayer(tournamentgamesRow gameSchRow)
        {
            InitializeComponent();
            GameSchRow = gameSchRow;
            int plA = GameSchRow.playerAid;
            int plB = GameSchRow.playerBid;
            int champId = GameSchRow.championshipId;

            List<playerleaguetournamentRow> allPlayerTournamentRow = ((playerleaguetournamentRow[])FilesAccess.db.playerLeagueTournament.Select("championshipId="+champId)).ToList();

            for (int i = 0; i < allPlayerTournamentRow.Count; i++)
            {
                playerRow plRow = FilesAccess.db.player.Single(x=>x.Id == allPlayerTournamentRow[i].playerId);
                comboBox1.Items.Add(plRow.Name);
                comboBox2.Items.Add(plRow.Name);
            }

            comboBox1.Text = FilesAccess.db.player.Single(x => x.Id == plA).Name;
            comboBox2.Text = FilesAccess.db.player.Single(x => x.Id == plB).Name;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                GameSchRow.playerAid = FilesAccess.db.player.Single(x => x.Name == comboBox1.Text).Id;
                GameSchRow.playerBid = FilesAccess.db.player.Single(x => x.Name == comboBox2.Text).Id;
                FilesAccess.db.UpdateTournamentGames();
                this.Close();
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }
    }
}
