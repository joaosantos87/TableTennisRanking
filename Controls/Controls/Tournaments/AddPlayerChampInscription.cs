﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TableTennis.Database;
using static TableTennis.Database.DataSet2;

namespace TableTennis.Controls
{
    public partial class AddPlayerChampInscription : Form
    {
        playerRow plRow;
        clubsRow clRow;
        championshipsRow champRow;
        int tournamentId;
        public AddPlayerChampInscription(int id)
        {
            InitializeComponent();
            tournamentId = id;
            dataGridView1.RowsAdded += dataGridView1_RowsAdded;
            UpdateInterface();
        }


        private void UpdateInterface()
        {
            try
            {
                champRow = FilesAccess.db.championships.Single(x => x.Id == tournamentId);
                label1.Text = "Torneio: " + champRow.Name;
                string aux = comboBox1.Text;
                comboBox1.Items.Clear();
                for (int i = 0; i < FilesAccess.db.playerInscriptionLeague.Count; i++)
                {
                    playerRow plRow1 = FilesAccess.db.player.Single(x => x.Id == FilesAccess.db.playerInscriptionLeague[i].playerId);
                    comboBox1.Items.Add(plRow1.Name);
                }

                if (comboBox1.Items.Count > 0)
                    comboBox1.Text = aux;
            }
            catch
            {
                MessageBox.Show("Erro ao pesquisar o torneio!");
                this.Close();
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                plRow = FilesAccess.db.player.Single(x=>x.Name == comboBox1.Text);
                clRow = FilesAccess.db.clubs.Single(x=>x.Id == plRow.IdClub);
                playerinscriptionleagueRow pilRow = FilesAccess.db.playerInscriptionLeague.Single(x=>x.playerId == plRow.Id);
                label2.Text = clRow.Name;
                dataGridView1.DataSource = ((playerinscriptionleagueRow[])FilesAccess.db.playerInscriptionLeague.Select("playerId=" + plRow.Id)).ToList();
            }
            catch { }
        }

        private void dataGridView1_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            return;
            try
            {
                if (e.RowCount < 1)
                    return;

                for (int i = e.RowIndex; i < e.RowCount + e.RowIndex; i++)
                {
                    int currentId = Convert.ToInt32(dataGridView1["id", i].Value);
                    playerleaguetournamentRow plChampRow = FilesAccess.db.playerLeagueTournament.Single(x => x.id == currentId);
                    playerRow plRow = FilesAccess.db.player.Single(x => x.Id == plChampRow.playerId);
                    clubsRow clRow = FilesAccess.db.clubs.Single(x => x.Id == plRow.IdClub);

                    dataGridView1["Nome", i].Value = plRow.Name;
                    dataGridView1["Clube", i].Value = clRow.Name;
                }
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(plRow==null || champRow==null)
                return;

            if (FilesAccess.db.playerLeagueTournament.Count(x => x.playerId == plRow.Id && champRow.Id == x.championshipId) > 0)
            {
                MessageBox.Show("Jogador ja inserido!");
                return;
            }

            FilesAccess.db.playerLeagueTournament.AddplayerleaguetournamentRow(plRow.Id, champRow.Id, true,0,0,0,0);
            bool insertedStatus = FilesAccess.db.InsertPlayerChamp(FilesAccess.db.playerLeagueTournament.Last());
            if (insertedStatus)
                //MessageBox.Show("Inserido com sucesso!");
                Console.WriteLine("Inserido com sucesso!");
            else
                MessageBox.Show("Não inserido!");

            //DataSet1.PlayerChampionshipsRow row = FilesAccess.db.playerChamps.AddPlayerChampionshipsRow(plRow.Id, champRow.Id, true);
            //FilesAccess.db.InsertPlayerChamp(row);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            new OpenTournamentInscription().ShowDialog();
            UpdateInterface();
        }
    }
}
