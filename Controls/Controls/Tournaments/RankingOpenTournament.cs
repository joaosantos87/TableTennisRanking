﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TableTennis.Database;
using TableTennis.Reports;
using static TableTennis.Database.DataSet2;

namespace TableTennis.Controls
{
    public partial class RankingOpenTournament : Form
    {
        List<playerRow> players;

        public RankingOpenTournament()
        {
            InitializeComponent();
            dataGridView1.RowsAdded += dataGridView1_RowsAdded;
            UpdateInterface();
        }

        private void dataGridView1_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            try
            {
                if (e.RowCount < 1)
                    return;

                for (int i = e.RowIndex; i < e.RowCount + e.RowIndex; i++)
                {
                    if (comboBox1.Text == "Geral")
                    {
                        AddPlayerLeagueTournamentRanking(i);
                    }
                    else
                    {
                        AddSpecificTournamentRanking(i);
                    }
                }
            }
            catch { }
        }

        private void AddPlayerLeagueTournamentRanking(int i)
        {
            int id = Convert.ToInt32(dataGridView1["id", i].Value);
            playerinscriptionleagueRow pltRow = FilesAccess.db.playerInscriptionLeague.Single(x => x.id == id);

            playerRow playerRow = FilesAccess.db.player.Single(x => x.Id == pltRow.playerId);

            clubsRow clubRow = FilesAccess.db.clubs.Single(x => x.Id == playerRow.IdClub);

            dataGridView1["Nome", i].Value = playerRow.Name;
            dataGridView1["Clube", i].Value = clubRow.Name;

            dataGridView1["currentPoints", i].Value = pltRow.currentPoints;
            dataGridView1["currentGames", i].Value = pltRow.currentGamesWon + "-" + pltRow.currentGamesLost;
            dataGridView1["currentRank", i].Value = pltRow.currentRank;
        }

        private void AddSpecificTournamentRanking(int i)
        {
            int id = Convert.ToInt32(dataGridView1["id", i].Value);
            playerleaguetournamentRow pltRow = FilesAccess.db.playerLeagueTournament.Single(x => x.id == id);

            playerRow playerRow = FilesAccess.db.player.Single(x => x.Id == pltRow.playerId);

            clubsRow clubRow = FilesAccess.db.clubs.Single(x => x.Id == playerRow.IdClub);

            dataGridView1["Nome", i].Value = playerRow.Name;
            dataGridView1["Clube", i].Value = clubRow.Name;

            dataGridView1["currentPoints", i].Value = pltRow.currentPoints;
            dataGridView1["currentGames", i].Value = pltRow.currentGamesWon + "-" + pltRow.currentGamesLost;
            dataGridView1["currentRank", i].Value = pltRow.currentRank;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.Text == "Geral")
            {
                players = Database.Ranking.Instance.UpdateRanking(DateTime.MinValue, DateTime.MaxValue, Utilities.AllOpenTournaments);
                //dataGridView1.DataSource = ((DataSet1.PlayerInscriptionLeagueRow[])(FilesAccess.db.playerInscriptionLeague.Select("currentGames>0"))).OrderByDescending(x => x.currentPoints).ToList();
                dataGridView1.DataSource = ((playerinscriptionleagueRow[])(FilesAccess.db.playerInscriptionLeague.Select())).OrderBy(x => x.currentRank).ToList();
            }
            else
            {
                List<int> a = new List<int>();
                a.Add(FilesAccess.db.championships.Single(x=>x.Name == comboBox1.Text).Id);
                players = Database.Ranking.Instance.UpdateRanking(DateTime.MinValue, DateTime.MaxValue, a);
                //dataGridView1.DataSource = ((DataSet1.playerRow[])(FilesAccess.db.player.Select("currentGames>0"))).OrderByDescending(x => x.currentPoints).ToList();
                dataGridView1.DataSource = ((playerleaguetournamentRow[])(FilesAccess.db.playerLeagueTournament.Select("championshipId=" + a[0]))).OrderBy(x => x.currentRank).ToList();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            UpdateInterface();
        }

        private void UpdateInterface()
        {
            string aux = comboBox1.Text;

            comboBox1.Items.Clear();
            comboBox1.Items.Add("Geral");

            List<championshipsRow> chRow = ((championshipsRow[])FilesAccess.db.championships.Select("idTournamentType="+ Utilities.OpenTournamentId)).ToList();

            for (int i = 0; i < chRow.Count; i++)
            {
                comboBox1.Items.Add(chRow[i].Name);
            }

            if (comboBox1.Items.Count > 0)
                comboBox1.SelectedIndex = 0;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            List<RankPlayer> rankPlayer = new List<RankPlayer>();
            for(int i=0; i<dataGridView1.Rows.Count;i++)
            {
                rankPlayer.Add(new RankPlayer(dataGridView1["currentRank", i].Value, dataGridView1["currentRank", i].Value, "", dataGridView1["nome", i].Value, dataGridView1["clube", i].Value, "", dataGridView1["currentPoints", i].Value, dataGridView1["GamesWon", i].Value, dataGridView1["GamesLost", i].Value));
            }

            new PdfRanking("test1", rankPlayer, Ranking.Instance.championshipsIncluded);
        }
    }
}
