﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TableTennis.Database;
using static TableTennis.Database.DataSet2;

namespace TableTennis.Controls.Tournaments
{
    public partial class ManualTournamentGroup : Form
    {
        public event EventHandler CreateManualTournament;
        
        List<Tuple<int, playerRow>> inscriptions = new List<Tuple<int, playerRow>>();

        
        public ManualTournamentGroup()
        {
            InitializeComponent();
            comboBox1.Items.Clear();

            List<championshipsRow> chRows = ((championshipsRow[])FilesAccess.db.championships.Select("")).ToList();

            for (int i = 0; i < Utilities.AllOpenTournaments.Count; i++)
            {
                comboBox1.Items.Add(FilesAccess.db.championships.Single(x => x.Id == Utilities.AllOpenTournaments[i]).Name);
            }
        }

        public List<Tuple<int, playerRow>> GetInscriptions
        {
            get { return inscriptions; }
        }

        public int NumGroups
        {
            get { return Convert.ToInt32(textBox1.Text); }
        }

        public championshipsRow ChampRow
        {
            get
            {
                try
                {
                    return FilesAccess.db.championships.Single(x => x.Name == comboBox1.Text);
                }
                catch { }
                return null;
            }
        }


        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < listBox1.SelectedItems.Count; i++)
                {
                    inscriptions.Add(new Tuple<int, playerRow>(Convert.ToInt32(textBox2.Text), FilesAccess.db.player.Single(x => x.Name == listBox1.SelectedItems[i])));
                }
            }
            catch { }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (CreateManualTournament != null)
                CreateManualTournament(this, new EventArgs());
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            listBox1.Items.Clear();

            List<playerleaguetournamentRow> pla = ((playerleaguetournamentRow[])FilesAccess.db.playerLeagueTournament.Select("championshipId=" + ChampRow.Id+" and confirmInscription="+1)).ToList();

            for (int i = 0; i < pla.Count; i++)
            {
                playerRow pl = FilesAccess.db.player.Single(x => x.Id == pla[i].playerId);
                listBox1.Items.Add(pl.Name);
            }
        }
    }
}
