﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TableTennis.Database;
using TableTennis.Reports;
using static TableTennis.Database.DataSet2;

namespace TableTennis.Controls
{
    public partial class ViewTournamentGamesForm : Form
    {
        playerRow player;

        public ViewTournamentGamesForm()
        {
            InitializeComponent();
            dataGridView1.RowsAdded += dataGridView1_RowsAdded;
            UpdateInterface();
        }

        private void UpdateInterface()
        {
            string aux = comboBox1.Text;

            comboBox1.Items.Clear();
            comboBox1.Items.Add("Geral");

            List<championshipsRow> chRow = ((championshipsRow[])FilesAccess.db.championships.Select("idTournamentType="+ Utilities.OpenTournamentId)).ToList();

            for (int i = 0; i < chRow.Count; i++)
            {
                comboBox1.Items.Add(chRow[i].Name);
            }

            if (comboBox1.Items.Count > 0)
                comboBox1.SelectedIndex = 0;

            string aux1 = comboBox2.Text;
            comboBox2.Items.Clear();
            comboBox2.Items.Add("Geral");

            if (comboBox2.Items.Contains(aux))
                comboBox2.Text = aux;
            else
            {
                if (comboBox2.Items.Count > 0)
                    comboBox2.SelectedIndex = 0;
            }
        }

        private void dataGridView1_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            try
            {
                if (e.RowCount < 1)
                    return;

                for (int i = e.RowIndex; i < e.RowCount + e.RowIndex; i++)
                {
                    try
                    {
                        //int idGame = Convert.ToInt32(dataGridView1["id", i].Value);
                        int idGame = Convert.ToInt32(dataGridView1["id", i].Value);

                        DataSet2.gamesRow game = FilesAccess.db.games.Single(x => x.id == idGame);

                        long idWinner = game.IdPlayerWinner;
                        long playerAid = game.IdPlayerA;
                        long playerBid = game.IdPlayerB;

                        playerRow plARow = FilesAccess.db.player.Single(x => x.Id == playerAid);
                        playerRow plBRow = FilesAccess.db.player.Single(x => x.Id == playerBid);

                        string res = "";
                        if (player != null)
                        {
                            res = player.Id == game.IdPlayerA ? 
                                plARow.Name + " " + game.SetsA + "-" + game.SetsB + " " + plBRow.Name :
                                plBRow.Name + " " + game.SetsB + "-" + game.SetsA + " " + plARow.Name;


                            dataGridView1["pointsPlayerA", i].Value = (player.Id == game.IdPlayerA ? game.pointsPlayerA : game.pointsPlayerB);
                            dataGridView1["pointsPlayerB", i].Value = (player.Id == game.IdPlayerA ? game.pointsPlayerB : game.pointsPlayerA);
                        }
                        else
                        {
                            res = plARow.Name + " " + game.SetsA + " - " + game.SetsB + " " + plBRow.Name;
                            dataGridView1["pointsPlayerA", i].Value = game.pointsPlayerA;
                            dataGridView1["pointsPlayerB", i].Value = game.pointsPlayerB;
                        }

                        dataGridView1["Resultado", i].Value = res;
                        dataGridView1["Torneio", i].Value = FilesAccess.db.championships.Single(x => x.Id == game.IdChamp).Name;
                        

                        //string opp = game.IdPlayerA == player.Id ? FilesAccess.db.player.Single(x => x.Id == game.IdPlayerB).Name : FilesAccess.db.player.Single(x => x.Id == game.IdPlayerA).Name;

                        //dataGridView1.Rows[i].Cells[dataGridView1.Columns["Opponent"].Index].Value = opp;
                        //dataGridView1.Rows[i].Cells[dataGridView1.Columns["Championship"].Index].Value = FilesAccess.db.championships.Single(x => x.Id == game.IdChamp).Name;

                        //dataGridView1.Rows[i].Cells[dataGridView1.Columns["Points"].Index].Value =
                        //    player.Id == game.IdPlayerA ? game.pointsPlayerA : game.pointsPlayerB;

                        //paint the row according to result
                        if (player != null)
                        {
                            if (idWinner == player.Id)
                                dataGridView1.Rows[i].DefaultCellStyle.BackColor = Color.Green;
                            else
                                dataGridView1.Rows[i].DefaultCellStyle.BackColor = Color.Red;
                        }
                        else
                            dataGridView1.Rows[i].DefaultCellStyle.BackColor = Color.White;
                    }
                    catch { dataGridView1.Rows[i].DefaultCellStyle.BackColor = Color.Black; }
                }

            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
        }

        private void ViewTournamentGamesForm_Load(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.Text == "Geral")
            {
                comboBox2.Items.Clear();
                comboBox2.Items.Add("Geral");
                for(int i=0; i<FilesAccess.db.playerInscriptionLeague.Count;i++)
                {
                    playerRow plRow = FilesAccess.db.player.Single(x => x.Id == FilesAccess.db.playerInscriptionLeague[i].playerId);
                    comboBox2.Items.Add(plRow.Name);
                }
                gamesRow[] allGames;
                if (player != null)
                    allGames = (gamesRow[])FilesAccess.db.games.Select("IdPlayerA=" + player.Id + " Or IdPlayerB=" + player.Id);
                else
                    allGames = (gamesRow[])FilesAccess.db.games.Select();

                List<gamesRow> allQueries = new List<gamesRow>();
                string selectedQuery = "";
                for (int i = 0; i < Utilities.AllOpenTournaments.Count; i++)
                {
                    selectedQuery += "IdChamp='";
                    selectedQuery += Utilities.AllOpenTournaments[i] + "' OR ";
                    allQueries.AddRange(allGames.Where(x => x.IdChamp == Utilities.AllOpenTournaments[i]).ToList());
                }
                selectedQuery += "IdChamp='";
                selectedQuery += "-1'";

                allGames = allQueries.ToArray();
                dataGridView1.DataSource = allGames;
            }
            else
            {
                //here we have the name of tournament
                championshipsRow a = FilesAccess.db.championships.Single(x=>x.Name == comboBox1.Text);
                List<playerleaguetournamentRow> plLeagueTournRows = ((playerleaguetournamentRow[])FilesAccess.db.playerLeagueTournament.Select("championshipId=" + a.Id)).ToList();

                comboBox2.Items.Clear();
                comboBox2.Items.Add("Geral");
                for (int i = 0; i < plLeagueTournRows.Count; i++)
                {
                    playerRow plRow = FilesAccess.db.player.Single(x => x.Id == plLeagueTournRows[i].playerId);
                    comboBox2.Items.Add(plRow.Name);
                }

                if (player != null)
                {
                    gamesRow[] allGames = (gamesRow[])FilesAccess.db.games.Select(
                        "IdChamp=" + a.Id +" AND (IdPlayerA=" + player.Id + " Or IdPlayerB=" + player.Id+")");
                    dataGridView1.DataSource = allGames;
                }
                else
                {
                    gamesRow[] allGames = (gamesRow[])FilesAccess.db.games.Select("IdChamp=" + a.Id);
                    dataGridView1.DataSource = allGames;
                }
            }
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox2.Text == "Geral" || comboBox2.Text.Equals(String.Empty))
                player = null;
            else
            {
                player = FilesAccess.db.player.Single(x => x.Name == comboBox2.Text);
                label3.Text = "Jogos de " + player.Name;
            }

            comboBox1_SelectedIndexChanged(null, new EventArgs());
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string name = comboBox2.Text;

            string title = "Jogos de " + name + " em " + comboBox1.Text;

            List<IPlayerGameReport> playerGames = new List<IPlayerGameReport>();
            for (int i = 0; i < dataGridView1.RowCount; i++)
            {
                playerGames.Add(
                    new PlayerTournamentGame(
                        dataGridView1["Data", i].Value,
                        dataGridView1["Torneio", i].Value, 
                        dataGridView1["Resultado", i].Value,
                        dataGridView1["pointsPlayerA", i].Value,
                        dataGridView1["pointsPlayerB", i].Value, 
                        dataGridView1.Rows[i].DefaultCellStyle.BackColor));
            }

            new PdfPlayerGames(title, playerGames);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Funçãoo ainda nao disponivel", "Informacao");
            return;
            int champId = 0;
            if (comboBox1.Text != "Geral")
            {
                champId = FilesAccess.db.championships.Single(x=>x.Name == comboBox1.Text).Id;
            }

            int playerId = 0;
            if(comboBox2.Text!="Geral")
            {
                playerId = FilesAccess.db.player.Single(x=>x.Name==comboBox2.Text).Id;
            }

            List<tournamentgamesRow> toDelete = new List<tournamentgamesRow>();
            if (champId != 0 && playerId != 0)
            {
                toDelete.AddRange(((tournamentgamesRow[])FilesAccess.db.tournamentGames.Select("championshipId=" + champId + " and (idPlayerA=" + playerId + " or idPlayerB=" + playerId + ")")).ToList());
            }
            else if (champId != 0 && playerId == 0)
            {
                toDelete.AddRange(((tournamentgamesRow[])FilesAccess.db.tournamentGames.Select("championshipId=" + champId)).ToList());
            }
            else if (champId == 0 && playerId != 0)
            {
                toDelete.AddRange(((tournamentgamesRow[])FilesAccess.db.tournamentGames.Select("(idPlayerA=" + playerId + " or idPlayerB=" + playerId + ")")).ToList());
            }

            MessageBox.Show("Ira apagar da base de dados " + toDelete.Count + " jogos\nDeseja continuar?", "Informacao", MessageBoxButtons.YesNo);
            for (int i = 0; i < toDelete.Count; i++)
            {
                //erase from the games table too
                if (toDelete[i].gameId > 0)
                {
                    FilesAccess.db.games.Single(x => x.id == toDelete[i].gameId).Delete();
                }
               // FilesAccess.db.tournamentGames.Single(x=>x==toDelete[i]).Delete();
                //FilesAccess.db.tournamentGames.AcceptChanges();
            }
        }
    }
}
