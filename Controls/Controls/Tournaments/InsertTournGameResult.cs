﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TableTennis.Database;
using static TableTennis.Database.DataSet2;

namespace TableTennis.Controls
{
    public partial class InsertTournGameResult : Form
    {
        playerRow plA;
        playerRow plB;
        tournamentgamesRow tournRow;

        public InsertTournGameResult(playerRow _plA, playerRow _plB, tournamentgamesRow _tournRow)
        {
            InitializeComponent();
            Text = "Inserir resultado do jogo";
            this.SetDesktopLocation(Cursor.Position.X - this.Width/2, Cursor.Position.Y);
            label1.Text = _plA.Name;
            label2.Text = _plB.Name;
            tournRow = _tournRow;
            plA = _plA;
            plB = _plB;
            textBox4.Text = tournRow.set0plA.ToString();
            textBox3.Text = tournRow.set0plB.ToString();

            textBox1.Text = tournRow.set1plA.ToString();
            textBox2.Text = tournRow.set1plB.ToString();

            textBox6.Text = tournRow.set2plA.ToString();
            textBox5.Text = tournRow.set2plB.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                DataSet2.gamesRow rowGameInserted;
                int set0plA = Convert.ToInt32(textBox4.Text);
                int set0plB = Convert.ToInt32(textBox3.Text);

                int set1plA = Convert.ToInt32(textBox1.Text);
                int set1plB = Convert.ToInt32(textBox2.Text);

                int set2plA = Convert.ToInt32(textBox6.Text);
                int set2plB = Convert.ToInt32(textBox5.Text);

                int setsA = 0;
                int setsB = 0;

                if (set0plA > set0plB)
                    setsA++;
                else if (set0plB > set0plA)
                    setsB++;

                if (set1plA > set1plB)
                    setsA++;
                else if (set1plB > set1plA)
                    setsB++;

                if (set2plA > set2plB)
                    setsA++;
                else if (set2plB > set2plA)
                    setsB++;

                int idWinner = setsA>setsB?plA.Id:plB.Id;

                if (FilesAccess.db.games.Count(x=>x.id == tournRow.gameId) > 0)
                {
                    MessageBoxManager.Abort = "Cancelar";
                    MessageBoxManager.Retry = "Substituir";
                    MessageBoxManager.Ignore = "Inserir";
                    try
                    {
                        MessageBoxManager.Register();
                    }
                    catch { }

                    DialogResult dr = MessageBox.Show("Jogo ja inserido, deseja cancelar, substituir ou inserir outro jogo?","Informacao",MessageBoxButtons.AbortRetryIgnore);

                    MessageBoxManager.Abort = "Abort";
                    MessageBoxManager.Retry = "Retry";
                    MessageBoxManager.Ignore = "Ignore";

                    try
                    {
                        MessageBoxManager.Register();
                    }
                    catch { }

                    if(dr == System.Windows.Forms.DialogResult.Abort)
                    {
                        MessageBox.Show("Jogo nao inserido");
                        return;
                    }
                    else if(dr == System.Windows.Forms.DialogResult.Retry)
                    {
                        rowGameInserted = FilesAccess.db.games.Single(x => x.id == tournRow.gameId);
                        rowGameInserted.SetsA = setsA;
                        rowGameInserted.SetsB = setsB;
                        rowGameInserted.IdPlayerWinner = idWinner;
                        
                        FilesAccess.db.UpdateGames();
                        goto updateTournamentGames;
                    }
                    else if (dr == System.Windows.Forms.DialogResult.Ignore)
                    {
                        goto insertTournamentGames;
                    }
                }

                insertTournamentGames:
                rowGameInserted = FilesAccess.db.games.AddgamesRow(plA.Id, plB.Id, setsA, setsB, idWinner, tournRow.Time, tournRow.championshipId, 0, 0, 0, tournRow.idPhase, 0, true, DateTime.Now, false, 0, 0);

                FilesAccess.db.games.Where(x => x.Date > new DateTime(2018, 1, 1) && x.Date < DateTime.Now);

                FilesAccess.db.InsertGame(rowGameInserted);
                
                updateTournamentGames:
                tournRow.set0plA = set0plA;
                tournRow.set1plA = set1plA;
                tournRow.set2plA = set2plA;
                tournRow.set0plB = set0plB;
                tournRow.set1plB = set1plB;
                tournRow.set2plB = set2plB;
                tournRow.gameId = rowGameInserted.id;

                List<tournamentgamesRow> tgRowsToUpdate = ((tournamentgamesRow[])FilesAccess.db.tournamentGames.Select("playerAid=" + (-tournRow.id) + " or playerBid=" + (-tournRow.id))).ToList();

                for (int i = 0; i < tgRowsToUpdate.Count; i++)
                {
                    if (tgRowsToUpdate[i].playerAid == (-tournRow.id))
                        tgRowsToUpdate[i].playerAid = Convert.ToInt32(rowGameInserted.IdPlayerWinner);

                    if(tgRowsToUpdate[i].playerBid == (-tournRow.id))
                        tgRowsToUpdate[i].playerBid = Convert.ToInt32(rowGameInserted.IdPlayerWinner);
                }

                FilesAccess.db.UpdateTournamentGames();

                MessageBox.Show("Jogo inserido");
                this.Close();
            }
            catch { }
        }
    }
}
