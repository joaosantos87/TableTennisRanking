﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TableTennis.Database;
using static TableTennis.Database.DataSet2;

namespace TableTennis.Controls
{
    public partial class PlayerGroupControl : UserControl
    {
        public playerRow plRow;
        int champId;

        int setsWon = 0;
        int setsLost = 0;
        int ptsWon = 0;
        int ptsLost = 0;
        int numGames = 0;
        int ptsFinal = 0;

        public int PointsTotal
        {
            get { return ptsFinal; }
        }

        public float SetsWonLost
        {
            get 
            {
                if (setsWon == 0)
                    return 0.0f;

                if (setsLost == 0)
                    return 1.0f;
                    
                return Convert.ToSingle(setsWon) / (setsLost+setsWon); 
            }
        }

        public float PointsWonLost
        {
            get 
            {
                if (ptsWon == 0)
                    return 0;
                if (ptsLost == 0)
                    return 1.0f;
                return Convert.ToSingle(ptsWon) / (ptsLost+ptsWon); 
            }
        }

        public PlayerGroupControl(int _champId, string name, string clubShortname)
        {
            InitializeComponent();
            label1.Text = name;
            label2.Text = clubShortname;
            champId = _champId;
            plRow = FilesAccess.db.player.Single(x => x.Name == name);
            label7.Text = plRow.Id.ToString();
        }

        public void UpdatellGames()
        {
            //set the correct number of games and points
            List<tournamentgamesRow> tgRows = ((tournamentgamesRow[])FilesAccess.db.tournamentGames.Select("championshipId=" + champId + " AND (playerAid=" + plRow.Id + " OR playerBid=" + plRow.Id + ") and idPhase="+Utilities.Phase_Group)).ToList();

            UpdateSpecificGames(tgRows);
        }

        public void UpdateSpecificGames(List<tournamentgamesRow> tgRows)
        {
            ptsWon = 0;
            ptsLost = 0;
            setsWon = 0;
            setsLost = 0;
            ptsFinal = 0;
            numGames = 0;

            for (int i = 0; i < tgRows.Count; i++)
            {
                Tuple<int, int> points = GetPoints(tgRows[i]);
                Tuple<int, int> sets = GetSets(tgRows[i]);
                Tuple<int, int> ptsAux = GetPointsPerGame(tgRows[i]);
                int temppts = 0;
                if (tgRows[i].playerAid == plRow.Id)
                {
                    temppts = points.Item1;
                    ptsWon += points.Item1;
                    ptsLost += points.Item2;
                    setsWon += sets.Item1;
                    setsLost += sets.Item2;
                    ptsFinal += ptsAux.Item1;
                }
                else if (tgRows[i].playerBid == plRow.Id)
                {
                    temppts = points.Item2;
                    ptsWon += points.Item2;
                    ptsLost += points.Item1;
                    setsWon += sets.Item2;
                    setsLost += sets.Item1;
                    ptsFinal += ptsAux.Item2;
                }

                if (temppts > 0)
                    numGames++;
            }

            label4.Text = setsWon.ToString();
            label6.Text = setsLost.ToString();
            label3.Text = numGames.ToString();
            label5.Text = ptsFinal.ToString();
        }

        private Tuple<int, int> GetPointsPerGame(tournamentgamesRow row)
        {
            //if (FilesAccess.db.games.Count(x => x.id == row.gameId) > 0)
            if(row.gameId==0)
                return new Tuple<int,int>(0,0);

            DataSet2.gamesRow gmRow = FilesAccess.db.games.Single(x => x.id == row.gameId);
            int ptsPla=0;
            int ptsPlb=0;
            if (gmRow.IdPlayerWinner == row.playerAid)
                ptsPla = 3;
            else
                ptsPlb = 3;

            return new Tuple<int, int>(ptsPla, ptsPlb);
        }

        private Tuple<int, int> GetSets(tournamentgamesRow row)
        {
            //if(FilesAccess.db.games.Count(x=>x.id == row.gameId)>0)
            if (row.gameId == 0)
                return new Tuple<int,int>(0,0);

            try
            {
                DataSet2.gamesRow gmRow = FilesAccess.db.games.Single(x => x.id == row.gameId);

                int setsA = gmRow.SetsA;
                int setsB = gmRow.SetsB;

                return new Tuple<int, int>(setsA, setsB);
            }
            catch { return new Tuple<int, int>(0, 0); }
        }

        private Tuple<int,int> GetPoints(tournamentgamesRow row)
        {
            int ptsA = row.set0plA;
            ptsA += row.set1plA;
            ptsA += row.set2plA;

            int ptsB = row.set0plB;
            ptsB += row.set1plB;
            ptsB += row.set2plB;

            return new Tuple<int, int>(ptsA, ptsB);
        }
    }
}