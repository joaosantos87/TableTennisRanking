﻿using static TableTennis.Database.DataSet2;

namespace TableTennis.Controls
{
    partial class PlayersInscritedLeague
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.playerInscGenTournamentDataTableBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nome = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Clube = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.currentPoints = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.currentGames = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.currentRank = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.email = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EstiloJogo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TipoPega = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.madeira = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.borrachadireita = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.borrachaReves = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerInscGenTournamentDataTableBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.Nome,
            this.Clube,
            this.currentPoints,
            this.currentGames,
            this.currentRank,
            this.email,
            this.EstiloJogo,
            this.TipoPega,
            this.madeira,
            this.borrachadireita,
            this.borrachaReves});
            this.dataGridView1.DataSource = this.playerInscGenTournamentDataTableBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(22, 36);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.Size = new System.Drawing.Size(836, 307);
            this.dataGridView1.TabIndex = 0;
            // 
            // playerInscGenTournamentDataTableBindingSource
            // 
            this.playerInscGenTournamentDataTableBindingSource.DataSource = typeof(playerinscriptionleagueDataTable);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(361, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(136, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Jogadores Inscritos na Liga";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(777, 7);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Actualizar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // id
            // 
            this.id.DataPropertyName = "id";
            this.id.HeaderText = "id";
            this.id.Name = "id";
            this.id.Visible = false;
            this.id.Width = 30;
            // 
            // Nome
            // 
            this.Nome.HeaderText = "Nome";
            this.Nome.Name = "Nome";
            this.Nome.Width = 200;
            // 
            // Clube
            // 
            this.Clube.HeaderText = "Clube";
            this.Clube.Name = "Clube";
            this.Clube.Width = 200;
            // 
            // currentPoints
            // 
            this.currentPoints.DataPropertyName = "currentPoints";
            this.currentPoints.HeaderText = "Pontos";
            this.currentPoints.Name = "currentPoints";
            this.currentPoints.Width = 50;
            // 
            // currentGames
            // 
            this.currentGames.DataPropertyName = "currentGames";
            this.currentGames.HeaderText = "Jogos";
            this.currentGames.Name = "currentGames";
            this.currentGames.Width = 40;
            // 
            // currentRank
            // 
            this.currentRank.DataPropertyName = "currentRank";
            this.currentRank.HeaderText = "Ranking";
            this.currentRank.Name = "currentRank";
            this.currentRank.Width = 50;
            // 
            // email
            // 
            this.email.DataPropertyName = "email";
            this.email.HeaderText = "email";
            this.email.Name = "email";
            this.email.Width = 200;
            // 
            // EstiloJogo
            // 
            this.EstiloJogo.HeaderText = "Estilo Jogo";
            this.EstiloJogo.Name = "EstiloJogo";
            this.EstiloJogo.Width = 200;
            // 
            // TipoPega
            // 
            this.TipoPega.HeaderText = "Tipo Pega";
            this.TipoPega.Name = "TipoPega";
            this.TipoPega.Width = 200;
            // 
            // madeira
            // 
            this.madeira.DataPropertyName = "racketwood";
            this.madeira.HeaderText = "Madeira";
            this.madeira.Name = "madeira";
            this.madeira.Width = 200;
            // 
            // borrachadireita
            // 
            this.borrachadireita.DataPropertyName = "racketFH";
            this.borrachadireita.HeaderText = "Borracha Direita";
            this.borrachadireita.Name = "borrachadireita";
            this.borrachadireita.Width = 200;
            // 
            // borrachaReves
            // 
            this.borrachaReves.DataPropertyName = "racketBH";
            this.borrachaReves.HeaderText = "Borracha Reves";
            this.borrachaReves.Name = "borrachaReves";
            this.borrachaReves.Width = 200;
            // 
            // PlayersInscritedLeague
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(864, 355);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridView1);
            this.Name = "PlayersInscritedLeague";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PlayersInscritedTournament";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerInscGenTournamentDataTableBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.BindingSource playerInscGenTournamentDataTableBindingSource;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nome;
        private System.Windows.Forms.DataGridViewTextBoxColumn Clube;
        private System.Windows.Forms.DataGridViewTextBoxColumn currentPoints;
        private System.Windows.Forms.DataGridViewTextBoxColumn currentGames;
        private System.Windows.Forms.DataGridViewTextBoxColumn currentRank;
        private System.Windows.Forms.DataGridViewTextBoxColumn email;
        private System.Windows.Forms.DataGridViewTextBoxColumn EstiloJogo;
        private System.Windows.Forms.DataGridViewTextBoxColumn TipoPega;
        private System.Windows.Forms.DataGridViewTextBoxColumn madeira;
        private System.Windows.Forms.DataGridViewTextBoxColumn borrachadireita;
        private System.Windows.Forms.DataGridViewTextBoxColumn borrachaReves;
    }
}