﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TableTennis.Database;
using static TableTennis.Database.DataSet2;

namespace TableTennis.Controls
{
    public partial class PlayersInscritedLeague : Form
    {
        public PlayersInscritedLeague()
        {
            InitializeComponent();

            dataGridView1.RowsAdded += dataGridView1_RowsAdded;
            button1_Click(this, new EventArgs());
        }

        private void dataGridView1_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            try
            {
                if (e.RowCount < 1)
                    return;

                for (int i = e.RowIndex; i < e.RowCount + e.RowIndex; i++)
                {
                    int id = Convert.ToInt32(dataGridView1["id",i].Value);
                    playerinscriptionleagueRow leagueInscriptionRow = FilesAccess.db.playerInscriptionLeague.Single(x => x.id == id);
                    playerRow plRow = FilesAccess.db.player.Single(x => x.Id == leagueInscriptionRow.playerId);
                    clubsRow clRow = FilesAccess.db.clubs.Single(x => x.Id == plRow.IdClub);
                    gamestyleRow gsRow = FilesAccess.db.gameStyle.Single(x => x.id == leagueInscriptionRow.gameStyleId);
                    handshakeRow hsRow = FilesAccess.db.handShake.Single(x => x.id == leagueInscriptionRow.handshakeId);

                    dataGridView1["Nome", i].Value = plRow.Name;
                    dataGridView1["Clube", i].Value = clRow.Name;
                    dataGridView1["email", i].Value = leagueInscriptionRow.email;
                    dataGridView1["currentPoints", i].Value = leagueInscriptionRow.currentPoints;
                    dataGridView1["currentGames", i].Value = leagueInscriptionRow.currentGamesWon + "-" + leagueInscriptionRow.currentGamesLost;
                    dataGridView1["currentRank", i].Value = leagueInscriptionRow.currentRank;
                    dataGridView1["estiloJogo", i].Value = gsRow.type;
                    dataGridView1["tipoPega", i].Value = hsRow.type;
                    dataGridView1["madeira", i].Value = leagueInscriptionRow.racketwood;
                    dataGridView1["borrachadireita", i].Value = leagueInscriptionRow.racketFH;
                    dataGridView1["borrachaReves", i].Value = leagueInscriptionRow.racketBH;
                }
            }
            catch { }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = ((playerinscriptionleagueRow[])FilesAccess.db.playerInscriptionLeague.Select()).ToList();
        }
    }
}
