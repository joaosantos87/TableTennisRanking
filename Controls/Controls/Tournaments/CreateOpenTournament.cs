﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TableTennis.Database;

namespace TableTennis.Controls
{
    public partial class CreateOpenTournament : Form
    {
        public CreateOpenTournament()
        {
            InitializeComponent();
        }

        private void createPlayerButton_Click(object sender, EventArgs e)
        {
            //check if fields are filled

            //If need to check any conditions check it here

            //enter to the DB
            FilesAccess.db.championships.AddchampionshipsRow(
                nameTextbox.Text,
                Utilities.OpenTournamentId,
                dateTimePicker1.Value,
                Utilities.OpenTournamentId,
                "",
                "");

            bool res = FilesAccess.db.UpdateChamp();

            if(res)
                MessageBox.Show("Torneio inserido com sucesso");
            else
                MessageBox.Show("Erro ao inserir torneio");
           // this.Close();
        }
    }
}
