﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TableTennis.Database;
using static TableTennis.Database.DataSet2;

namespace TableTennis.Controls
{
    public partial class OpenTournamentInscription : Form
    {
        playerRow plRow;
        clubsRow clRow;

        public OpenTournamentInscription()
        {
            InitializeComponent();
            UpdatePlayer();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            new GenericForm(new AddPlayer()).ShowDialog();
            UpdatePlayer();
        }

        private void UpdatePlayer()
        {
            string name = comboBox1.Text;
            string license = textBox1.Text;
            string gameType = comboBox2.Text;
            string handshake = comboBox3.Text;
            comboBox1.Items.Clear();
            comboBox2.Items.Clear();
            comboBox3.Items.Clear();

            List<playerRow> plRows = FilesAccess.db.player.OrderBy(x => x.Name).ToList();

            for (int i = 0; i < plRows.Count; i++)
                comboBox1.Items.Add(plRows[i].Name);

            if (comboBox1.Items.Count > 0)
                comboBox1.Text = name;

            for (int i = 0; i < FilesAccess.db.gameStyle.Count; i++)
                comboBox2.Items.Add(FilesAccess.db.gameStyle[i].type);

            if (comboBox2.Items.Count > 0)
                comboBox2.Text = gameType;

            for (int i = 0; i < FilesAccess.db.handShake.Count; i++)
                comboBox3.Items.Add(FilesAccess.db.handShake[i].type);

            if (comboBox2.Items.Count > 0)
                comboBox2.Text = gameType;

            textBox1.Text = license;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            plRow = FilesAccess.db.player.Single(x => x.Name == comboBox1.Text);
            clRow = FilesAccess.db.clubs.Single(x => x.Id == plRow.IdClub);

            textBox1.Text = plRow.licenseNo;
            textBox2.Text = clRow.Name;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            gamestyleRow gsRow = FilesAccess.db.gameStyle.Single(x => x.type == comboBox2.Text);
            handshakeRow hsRow = FilesAccess.db.handShake.Single(x => x.type == comboBox3.Text);

            //Check if player is already inserted
            if (FilesAccess.db.playerInscriptionLeague.Count(x => x.playerId == plRow.Id) > 0)
            {
                MessageBox.Show("Não inserido!\nJogador ja esta inscrito na liga!","Informacao");
                return;
            }

            FilesAccess.db.playerInscriptionLeague.AddplayerinscriptionleagueRow(plRow.Id, textBox6.Text, gsRow.id, hsRow.id, textBox3.Text,textBox4.Text, textBox5.Text,0,0,0,0);
            bool insertedStatus = FilesAccess.db.InsertTournamentInscription(FilesAccess.db.playerInscriptionLeague.Last());
            if (insertedStatus)
                MessageBox.Show("Inserido com sucesso!");
            else
                MessageBox.Show("Não inserido!");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            new PlayersInscritedLeague().Show();
        }
    }
}
