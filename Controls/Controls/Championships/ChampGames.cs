﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TableTennis.Database;
using static TableTennis.Database.DataSet2;

namespace TableTennis.Controls.Championships
{
    public partial class ChampGames : UserControl
    {
        DateTime startDate = new DateTime(2000, 10, 1);
        DateTime finalDate = DateTime.Now;
        int champ = -1;

        public ChampGames()
        {
            InitializeComponent();
            VisibleChanged += ViewGames_VisibleChanged;
            this.dataGridView1.RowsAdded +=dataGridView1_RowsAdded;
            this.dataGridView1.ColumnHeaderMouseClick += dataGridView1_ColumnHeaderMouseClick;
        }

        private void dataGridView1_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            dataGridView1.DataSource = FilesAccess.db.games.OrderByDescending(x => x.IdPlayerA).ToList();
        }

        private void dataGridView1_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            try
            {
                if (e.RowCount < 1)
                    return;

                for (int i = e.RowIndex; i < e.RowCount + e.RowIndex; i++)
                {
                    int idGame = Convert.ToInt32(dataGridView1["id", i].Value);
                    long idplayerA = FilesAccess.db.games.Single(x => x.id == idGame).IdPlayerA;
                    long idplayerB = FilesAccess.db.games.Single(x => x.id == idGame).IdPlayerB;

                    if (FilesAccess.db.player.Any(x => x.Id == idplayerA))
                    {
                        if (dataGridView1["playerA", i].Value == FilesAccess.db.player.Single(x => x.Id == idplayerA).Name)
                            break;
                        dataGridView1["playerA", i].Value = FilesAccess.db.player.Single(x => x.Id == idplayerA).Name;
                    }
                    if(FilesAccess.db.player.Any(x => x.Id == idplayerB))
                    {
                        dataGridView1["playerB", i].Value = FilesAccess.db.player.Single(x => x.Id == idplayerB).Name;
                    }
                    
                }
            }
            catch (Exception ex) { }
        }

        private void ViewGames_VisibleChanged(object sender, EventArgs e)
        {
            comboBox1.Items.Clear();

            for (int i = 0; i < FilesAccess.db.championships.Count; i++)
            {
                comboBox1.Items.Add(FilesAccess.db.championships[i].Name);
            }

                dataGridView1.DataSource = FilesAccess.db.games.OrderByDescending(x => x.Date).ToList();
            //dataGridView1.DataSource = FilesAccess.db.games.OrderByDescending(x => x.pointsPlayerA).ThenByDescending(x=>x.Date).ToList();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Update DB
            //TODO: commented this line. if any error occours in updating games, uncoment this dataGridView1.EndEdit();
            FilesAccess.db.UpdateGames();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                int actualYear = Convert.ToInt32(seasonTextbox.Text);
                actualYear--;
                seasonTextbox.Text = actualYear.ToString();
            }
            catch { }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                int actualYear = Convert.ToInt32(seasonTextbox.Text);
                actualYear++;
                seasonTextbox.Text = actualYear.ToString();
            }
            catch { }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            champ = FilesAccess.db.championships[comboBox1.SelectedIndex].Id;
            gamesRow[] allGames = (gamesRow[])FilesAccess.db.games.Select("Date >= '" + startDate + "' AND Date <= '" + finalDate + "'");
            
            dataGridView1.DataSource = allGames.OrderByDescending(x => x.IdChamp == champ).ToList();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            try
            {
                int year = Convert.ToInt32(seasonTextbox.Text);
                startDate = new DateTime(year - 1, 7, 1);
                finalDate = new DateTime(year, 6, 30);
                StartDateTextbox.Text = startDate.ToString("yyyy-MM-dd");
                FinalDateTextbox.Text = finalDate.ToString("yyyy-MM-dd");
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
