﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TableTennis.Database;
using static TableTennis.Database.DataSet2;

namespace TableTennis.Controls.Championships
{
    public partial class EditChamp : UserControl
    {
        championshipsRow champ;

        public EditChamp()
        {
            InitializeComponent();
            VisibleChanged += ViewGames_VisibleChanged;
        }

        private void ViewGames_VisibleChanged(object sender, EventArgs e)
        {
            dataGridView1.DataSource = FilesAccess.db.championships;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FilesAccess.db.UpdateChamp();
        }
    }
}
