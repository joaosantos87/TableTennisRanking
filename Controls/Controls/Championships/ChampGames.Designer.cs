﻿namespace TableTennis.Controls.Championships
{
    partial class ChampGames
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.button1 = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idPlayerADataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.playerA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pointsPlayerA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.setsADataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.setsBDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pointsPlayerB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idPlayerBDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.playerB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idPlayerWinnerDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idChampDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gamesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet2 = new TableTennis.Database.DataSet2();
            this.gamesTableAdapter = new TableTennis.Database.DataSet2TableAdapters.gamesTableAdapter();
            this.seasonTextbox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.plusSeasonButton = new System.Windows.Forms.Button();
            this.minusSeasonButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.FinalDateTextbox = new System.Windows.Forms.MaskedTextBox();
            this.StartDateTextbox = new System.Windows.Forms.MaskedTextBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gamesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet2)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button1.Location = new System.Drawing.Point(46, 373);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Update DB";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.dateDataGridViewTextBoxColumn,
            this.idPlayerADataGridViewTextBoxColumn,
            this.playerA,
            this.pointsPlayerA,
            this.setsADataGridViewTextBoxColumn,
            this.setsBDataGridViewTextBoxColumn,
            this.pointsPlayerB,
            this.idPlayerBDataGridViewTextBoxColumn,
            this.playerB,
            this.idPlayerWinnerDataGridViewTextBoxColumn,
            this.idChampDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.gamesBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(9, 65);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(557, 301);
            this.dataGridView1.TabIndex = 2;
            // 
            // id
            // 
            this.id.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.id.DataPropertyName = "id";
            this.id.HeaderText = "id";
            this.id.Name = "id";
            this.id.Width = 40;
            // 
            // dateDataGridViewTextBoxColumn
            // 
            this.dateDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dateDataGridViewTextBoxColumn.DataPropertyName = "Date";
            this.dateDataGridViewTextBoxColumn.HeaderText = "Date";
            this.dateDataGridViewTextBoxColumn.Name = "dateDataGridViewTextBoxColumn";
            this.dateDataGridViewTextBoxColumn.Width = 55;
            // 
            // idPlayerADataGridViewTextBoxColumn
            // 
            this.idPlayerADataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.idPlayerADataGridViewTextBoxColumn.DataPropertyName = "IdPlayerA";
            this.idPlayerADataGridViewTextBoxColumn.HeaderText = "IdPlayerA";
            this.idPlayerADataGridViewTextBoxColumn.Name = "idPlayerADataGridViewTextBoxColumn";
            this.idPlayerADataGridViewTextBoxColumn.Width = 77;
            // 
            // playerA
            // 
            this.playerA.HeaderText = "playerA";
            this.playerA.Name = "playerA";
            // 
            // pointsPlayerA
            // 
            this.pointsPlayerA.DataPropertyName = "pointsPlayerA";
            this.pointsPlayerA.HeaderText = "pointsPlayerA";
            this.pointsPlayerA.Name = "pointsPlayerA";
            // 
            // setsADataGridViewTextBoxColumn
            // 
            this.setsADataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.setsADataGridViewTextBoxColumn.DataPropertyName = "SetsA";
            this.setsADataGridViewTextBoxColumn.HeaderText = "SetsA";
            this.setsADataGridViewTextBoxColumn.Name = "setsADataGridViewTextBoxColumn";
            this.setsADataGridViewTextBoxColumn.Width = 60;
            // 
            // setsBDataGridViewTextBoxColumn
            // 
            this.setsBDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.setsBDataGridViewTextBoxColumn.DataPropertyName = "SetsB";
            this.setsBDataGridViewTextBoxColumn.HeaderText = "SetsB";
            this.setsBDataGridViewTextBoxColumn.Name = "setsBDataGridViewTextBoxColumn";
            this.setsBDataGridViewTextBoxColumn.Width = 60;
            // 
            // pointsPlayerB
            // 
            this.pointsPlayerB.DataPropertyName = "pointsPlayerB";
            this.pointsPlayerB.HeaderText = "pointsPlayerB";
            this.pointsPlayerB.Name = "pointsPlayerB";
            // 
            // idPlayerBDataGridViewTextBoxColumn
            // 
            this.idPlayerBDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.idPlayerBDataGridViewTextBoxColumn.DataPropertyName = "IdPlayerB";
            this.idPlayerBDataGridViewTextBoxColumn.HeaderText = "IdPlayerB";
            this.idPlayerBDataGridViewTextBoxColumn.Name = "idPlayerBDataGridViewTextBoxColumn";
            this.idPlayerBDataGridViewTextBoxColumn.Width = 77;
            // 
            // playerB
            // 
            this.playerB.HeaderText = "playerB";
            this.playerB.Name = "playerB";
            // 
            // idPlayerWinnerDataGridViewTextBoxColumn
            // 
            this.idPlayerWinnerDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.idPlayerWinnerDataGridViewTextBoxColumn.DataPropertyName = "IdPlayerWinner";
            this.idPlayerWinnerDataGridViewTextBoxColumn.HeaderText = "IdPlayerWinner";
            this.idPlayerWinnerDataGridViewTextBoxColumn.Name = "idPlayerWinnerDataGridViewTextBoxColumn";
            this.idPlayerWinnerDataGridViewTextBoxColumn.Width = 104;
            // 
            // idChampDataGridViewTextBoxColumn
            // 
            this.idChampDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.idChampDataGridViewTextBoxColumn.DataPropertyName = "IdChamp";
            this.idChampDataGridViewTextBoxColumn.HeaderText = "IdChamp";
            this.idChampDataGridViewTextBoxColumn.Name = "idChampDataGridViewTextBoxColumn";
            this.idChampDataGridViewTextBoxColumn.Width = 74;
            // 
            // gamesBindingSource
            // 
            this.gamesBindingSource.DataMember = "games";
            this.gamesBindingSource.DataSource = this.dataSet2;
            // 
            // dataSet1
            // 
            this.dataSet2.DataSetName = "DataSet1";
            this.dataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gamesTableAdapter
            // 
            this.gamesTableAdapter.ClearBeforeFill = true;
            // 
            // seasonTextbox
            // 
            this.seasonTextbox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.seasonTextbox.Location = new System.Drawing.Point(186, 27);
            this.seasonTextbox.Name = "seasonTextbox";
            this.seasonTextbox.Size = new System.Drawing.Size(32, 20);
            this.seasonTextbox.TabIndex = 22;
            this.seasonTextbox.Text = "2015";
            this.seasonTextbox.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(212, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 13);
            this.label4.TabIndex = 21;
            this.label4.Text = "Epoca";
            // 
            // plusSeasonButton
            // 
            this.plusSeasonButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.plusSeasonButton.Location = new System.Drawing.Point(261, 27);
            this.plusSeasonButton.Name = "plusSeasonButton";
            this.plusSeasonButton.Size = new System.Drawing.Size(27, 23);
            this.plusSeasonButton.TabIndex = 20;
            this.plusSeasonButton.Text = "+";
            this.plusSeasonButton.UseVisualStyleBackColor = true;
            this.plusSeasonButton.Click += new System.EventHandler(this.button3_Click);
            // 
            // minusSeasonButton
            // 
            this.minusSeasonButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.minusSeasonButton.Location = new System.Drawing.Point(228, 27);
            this.minusSeasonButton.Name = "minusSeasonButton";
            this.minusSeasonButton.Size = new System.Drawing.Size(27, 23);
            this.minusSeasonButton.TabIndex = 19;
            this.minusSeasonButton.Text = "-";
            this.minusSeasonButton.UseVisualStyleBackColor = true;
            this.minusSeasonButton.Click += new System.EventHandler(this.button2_Click);
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(105, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 17;
            this.label2.Text = "Data Fim";
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 16;
            this.label1.Text = "Data Inicio";
            // 
            // FinalDateTextbox
            // 
            this.FinalDateTextbox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.FinalDateTextbox.Location = new System.Drawing.Point(97, 27);
            this.FinalDateTextbox.Mask = "0000-00-00";
            this.FinalDateTextbox.Name = "FinalDateTextbox";
            this.FinalDateTextbox.Size = new System.Drawing.Size(73, 20);
            this.FinalDateTextbox.TabIndex = 15;
            this.FinalDateTextbox.Text = "20160630";
            // 
            // StartDateTextbox
            // 
            this.StartDateTextbox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.StartDateTextbox.Location = new System.Drawing.Point(4, 27);
            this.StartDateTextbox.Mask = "0000-00-00";
            this.StartDateTextbox.Name = "StartDateTextbox";
            this.StartDateTextbox.Size = new System.Drawing.Size(73, 20);
            this.StartDateTextbox.TabIndex = 14;
            this.StartDateTextbox.Text = "20150107";
            // 
            // comboBox1
            // 
            this.comboBox1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(294, 29);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(264, 21);
            this.comboBox1.TabIndex = 23;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(387, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 13);
            this.label3.TabIndex = 24;
            this.label3.Text = "Campeonatos";
            // 
            // ChampGames
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label3);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.seasonTextbox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.plusSeasonButton);
            this.Controls.Add(this.minusSeasonButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.FinalDateTextbox);
            this.Controls.Add(this.StartDateTextbox);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.button1);
            this.Name = "ChampGames";
            this.Size = new System.Drawing.Size(575, 402);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gamesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.BindingSource gamesBindingSource;
        private TableTennis.Database.DataSet2 dataSet2;
        private TableTennis.Database.DataSet2TableAdapters.gamesTableAdapter gamesTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idPlayerADataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn playerA;
        private System.Windows.Forms.DataGridViewTextBoxColumn pointsPlayerA;
        private System.Windows.Forms.DataGridViewTextBoxColumn setsADataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn setsBDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn pointsPlayerB;
        private System.Windows.Forms.DataGridViewTextBoxColumn idPlayerBDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn playerB;
        private System.Windows.Forms.DataGridViewTextBoxColumn idPlayerWinnerDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idChampDataGridViewTextBoxColumn;
        private System.Windows.Forms.TextBox seasonTextbox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button plusSeasonButton;
        private System.Windows.Forms.Button minusSeasonButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MaskedTextBox FinalDateTextbox;
        private System.Windows.Forms.MaskedTextBox StartDateTextbox;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label3;

    }
}
