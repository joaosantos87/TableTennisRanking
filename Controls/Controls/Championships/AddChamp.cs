﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TableTennis.Database;
using TableTennis.Database.DataSet2TableAdapters;

namespace TableTennis.Controls.Championships
{
    public partial class AddChamp : UserControl
    {
        public AddChamp()
        {
            InitializeComponent();
            VisibleChanged += AddChamp_VisibleChanged;
        }

        private void AddChamp_VisibleChanged(object sender, EventArgs e)
        {
            string currentLevel = comboBox1.Text;

            comboBox1.Items.Clear();
            for (int i = 0; i < FilesAccess.db.levels.Count; i++)
                comboBox1.Items.Add(FilesAccess.db.levels[i].Name);

            comboBox1.Text = currentLevel;

            string currentTournType = comboBox2.Text;
            comboBox2.Items.Clear();
            for (int i = 0; i < FilesAccess.db.tournamentType.Count; i++)
                comboBox2.Items.Add(FilesAccess.db.tournamentType[i].name);
            comboBox2.Text = currentTournType;
        }

        private void createChampButton_Click(object sender, EventArgs e)
        {
            try
            {
                new championshipsTableAdapter().Insert(
                    nameTextbox.Text,
                    FilesAccess.db.tournamentType.Single(x => x.name == comboBox2.Text).id,
                    dateTimePicker1.Value,
                    FilesAccess.db.levels.Single(x => x.Name == comboBox1.Text).id,
                    "",
                    "");

                new championshipsTableAdapter().Fill(FilesAccess.db.championships);
                if(FilesAccess.db.championships.Last().Name == nameTextbox.Text)
                    MessageBox.Show("Tournament inserted sucessfully!");
                else
                    MessageBox.Show("Tournament not inserted!");
            }
            catch(Exception ex)
            {
                MessageBox.Show($"Tournament not inserted!. Exception: {ex.Message}");
            }
        }
    }
}
