﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TableTennis.Database;
using static TableTennis.Database.DataSet2;

namespace TableTennis.Controls.Championships
{
    public partial class TeamChampGames : UserControl
    {
        DateTime startDate = new DateTime(2000, 10, 1);
        DateTime finalDate = DateTime.Now;
        int champ = -1;

        public TeamChampGames()
        {
            InitializeComponent();
            VisibleChanged += ViewGames_VisibleChanged;
            this.dataGridView1.RowsAdded +=dataGridView1_RowsAdded;
            this.dataGridView1.ColumnHeaderMouseClick += dataGridView1_ColumnHeaderMouseClick;
            UpdateTeamGames();
        }

        private void dataGridView1_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            dataGridView1.DataSource = FilesAccess.db.games.OrderBy(x => x.Date).ToList();
        }

        private void dataGridView1_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            try
            {
                if (e.RowCount < 1)
                    return;

                var dgv = sender as DataGridView;
                for (int i = e.RowIndex; i < e.RowCount + e.RowIndex; i++)
                {
                    int idTeamGame = Convert.ToInt32(dgv[dgv.Columns["Id"].Index, i].Value);
                    int clubA_id = Convert.ToInt32(dgv[dgv.Columns["ClubAID"].Index, i].Value);
                    int clubB_id = Convert.ToInt32(dgv[dgv.Columns["ClubBID"].Index, i].Value);

                    var addClubA = " " + dgv[dgv.Columns["clubAnameAppend"].Index, i].Value;
                    var addClubB = " " +dgv[dgv.Columns["clubBnameAppend"].Index, i].Value;

                    dgv[dgv.Columns["ClubeA"].Index, i].Value = FilesAccess.db.clubs.Single(x => x.Id == clubA_id).Name + addClubA;
                    dgv[dgv.Columns["ClubeB"].Index, i].Value = FilesAccess.db.clubs.Single(x => x.Id == clubB_id).Name + addClubB;
                    dgv[dgv.Columns["NumGames"].Index, i].Value = FilesAccess.db.games.Count(x => x.IdTeamGame == idTeamGame) + FilesAccess.db.doubleGames.Count(x => x.IdTeamGame == idTeamGame);
                }
            }
            catch (Exception ex) { }
        }

        private void ViewGames_VisibleChanged(object sender, EventArgs e)
        {
            comboBox1.Items.Clear();

            for (int i = 0; i < FilesAccess.db.championships.Count; i++)
            {
                comboBox1.Items.Add(FilesAccess.db.championships[i].Name);
            }

            dataGridView1.DataSource = FilesAccess.db.teamGames.OrderBy(x => x.Day).ToList();
            //dataGridView1.DataSource = FilesAccess.db.games.OrderByDescending(x => x.pointsPlayerA).ThenByDescending(x=>x.Date).ToList();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Update DB
            //TODO: commented this line. if any error occours in updating games, uncoment this dataGridView1.EndEdit();
            FilesAccess.db.UpdateGames();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                int actualYear = Convert.ToInt32(seasonTextbox.Text);
                actualYear--;
                seasonTextbox.Text = actualYear.ToString();
            }
            catch { }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                int actualYear = Convert.ToInt32(seasonTextbox.Text);
                actualYear++;
                seasonTextbox.Text = actualYear.ToString();
            }
            catch { }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateTeamGames();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            try
            {
                int year = Convert.ToInt32(seasonTextbox.Text);
                startDate = new DateTime(year - 1, 7, 1);
                finalDate = new DateTime(year, 6, 30);
                StartDateTextbox.Text = startDate.ToString("yyyy-MM-dd");
                FinalDateTextbox.Text = finalDate.ToString("yyyy-MM-dd");
                UpdateTeamGames();
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void UpdateTeamGames()
        {
            if (comboBox1.Text != "")
            {
                champ = FilesAccess.db.championships.Single(x => x.Name == comboBox1.Text).Id;
                dataGridView1.DataSource = FilesAccess.db.teamGames.Where(x => x.Date >= startDate && x.Date <= finalDate && x.IdChampionship == champ).OrderBy
                    (x => x.Day).ToList();
            }
            else
            {
                dataGridView1.DataSource = FilesAccess.db.teamGames.Where(x => x.Date >= startDate && x.Date <= finalDate).OrderBy
                    (x => x.Day).ToList();
            }
        }
    }
}
