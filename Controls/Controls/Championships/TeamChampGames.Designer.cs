﻿namespace TableTennis.Controls.Championships
{
    partial class TeamChampGames
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.button1 = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.teamgameBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet2 = new TableTennis.Database.DataSet2();
            this.gamesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gamesTableAdapter = new TableTennis.Database.DataSet2TableAdapters.gamesTableAdapter();
            this.seasonTextbox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.plusSeasonButton = new System.Windows.Forms.Button();
            this.minusSeasonButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.FinalDateTextbox = new System.Windows.Forms.MaskedTextBox();
            this.StartDateTextbox = new System.Windows.Forms.MaskedTextBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.teamgameTableAdapter = new TableTennis.Database.DataSet2TableAdapters.teamgameTableAdapter();
            this.Date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Day = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ClubAId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ClubeA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clubAnameAppend = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ClubBId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ClubeB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clubBnameAppend = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NumGames = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teamgameBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gamesBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button1.Location = new System.Drawing.Point(46, 373);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Update DB";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Date,
            this.Day,
            this.ClubAId,
            this.ClubeA,
            this.clubAnameAppend,
            this.ClubBId,
            this.ClubeB,
            this.clubBnameAppend,
            this.NumGames,
            this.Id});
            this.dataGridView1.DataSource = this.teamgameBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(9, 65);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(557, 301);
            this.dataGridView1.TabIndex = 2;
            // 
            // teamgameBindingSource
            // 
            this.teamgameBindingSource.DataMember = "teamgame";
            this.teamgameBindingSource.DataSource = this.dataSet2;
            // 
            // dataSet2
            // 
            this.dataSet2.DataSetName = "DataSet1";
            this.dataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gamesBindingSource
            // 
            this.gamesBindingSource.DataMember = "games";
            this.gamesBindingSource.DataSource = this.dataSet2;
            // 
            // gamesTableAdapter
            // 
            this.gamesTableAdapter.ClearBeforeFill = true;
            // 
            // seasonTextbox
            // 
            this.seasonTextbox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.seasonTextbox.Location = new System.Drawing.Point(186, 27);
            this.seasonTextbox.Name = "seasonTextbox";
            this.seasonTextbox.Size = new System.Drawing.Size(32, 20);
            this.seasonTextbox.TabIndex = 22;
            this.seasonTextbox.Text = "2022";
            this.seasonTextbox.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(212, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 13);
            this.label4.TabIndex = 21;
            this.label4.Text = "Epoca";
            // 
            // plusSeasonButton
            // 
            this.plusSeasonButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.plusSeasonButton.Location = new System.Drawing.Point(261, 27);
            this.plusSeasonButton.Name = "plusSeasonButton";
            this.plusSeasonButton.Size = new System.Drawing.Size(27, 23);
            this.plusSeasonButton.TabIndex = 20;
            this.plusSeasonButton.Text = "+";
            this.plusSeasonButton.UseVisualStyleBackColor = true;
            this.plusSeasonButton.Click += new System.EventHandler(this.button3_Click);
            // 
            // minusSeasonButton
            // 
            this.minusSeasonButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.minusSeasonButton.Location = new System.Drawing.Point(228, 27);
            this.minusSeasonButton.Name = "minusSeasonButton";
            this.minusSeasonButton.Size = new System.Drawing.Size(27, 23);
            this.minusSeasonButton.TabIndex = 19;
            this.minusSeasonButton.Text = "-";
            this.minusSeasonButton.UseVisualStyleBackColor = true;
            this.minusSeasonButton.Click += new System.EventHandler(this.button2_Click);
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(105, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 17;
            this.label2.Text = "Data Fim";
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 16;
            this.label1.Text = "Data Inicio";
            // 
            // FinalDateTextbox
            // 
            this.FinalDateTextbox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.FinalDateTextbox.Location = new System.Drawing.Point(97, 27);
            this.FinalDateTextbox.Mask = "0000-00-00";
            this.FinalDateTextbox.Name = "FinalDateTextbox";
            this.FinalDateTextbox.Size = new System.Drawing.Size(73, 20);
            this.FinalDateTextbox.TabIndex = 15;
            this.FinalDateTextbox.Text = "20220630";
            // 
            // StartDateTextbox
            // 
            this.StartDateTextbox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.StartDateTextbox.Location = new System.Drawing.Point(4, 27);
            this.StartDateTextbox.Mask = "0000-00-00";
            this.StartDateTextbox.Name = "StartDateTextbox";
            this.StartDateTextbox.Size = new System.Drawing.Size(73, 20);
            this.StartDateTextbox.TabIndex = 14;
            this.StartDateTextbox.Text = "20210107";
            // 
            // comboBox1
            // 
            this.comboBox1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(294, 29);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(264, 21);
            this.comboBox1.TabIndex = 23;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(387, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 13);
            this.label3.TabIndex = 24;
            this.label3.Text = "Campeonatos";
            // 
            // teamgameTableAdapter
            // 
            this.teamgameTableAdapter.ClearBeforeFill = true;
            // 
            // Date
            // 
            this.Date.DataPropertyName = "Date";
            this.Date.HeaderText = "Date";
            this.Date.Name = "Date";
            // 
            // Day
            // 
            this.Day.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.Day.DataPropertyName = "Day";
            this.Day.HeaderText = "Day";
            this.Day.Name = "Day";
            this.Day.Width = 51;
            // 
            // ClubAId
            // 
            this.ClubAId.DataPropertyName = "ClubAId";
            this.ClubAId.HeaderText = "ClubAId";
            this.ClubAId.Name = "ClubAId";
            this.ClubAId.Visible = false;
            // 
            // ClubeA
            // 
            this.ClubeA.HeaderText = "Clube A";
            this.ClubeA.Name = "ClubeA";
            // 
            // clubAnameAppend
            // 
            this.clubAnameAppend.DataPropertyName = "clubAnameAppend";
            this.clubAnameAppend.HeaderText = "clubAnameAppend";
            this.clubAnameAppend.Name = "clubAnameAppend";
            this.clubAnameAppend.Visible = false;
            // 
            // ClubBId
            // 
            this.ClubBId.DataPropertyName = "ClubBId";
            this.ClubBId.HeaderText = "ClubBId";
            this.ClubBId.Name = "ClubBId";
            this.ClubBId.Visible = false;
            // 
            // ClubeB
            // 
            this.ClubeB.HeaderText = "Clube B";
            this.ClubeB.Name = "ClubeB";
            // 
            // clubBnameAppend
            // 
            this.clubBnameAppend.DataPropertyName = "clubBnameAppend";
            this.clubBnameAppend.HeaderText = "clubBnameAppend";
            this.clubBnameAppend.Name = "clubBnameAppend";
            this.clubBnameAppend.Visible = false;
            // 
            // NumGames
            // 
            this.NumGames.HeaderText = "Num Games";
            this.NumGames.Name = "NumGames";
            // 
            // Id
            // 
            this.Id.DataPropertyName = "Id";
            this.Id.HeaderText = "Id";
            this.Id.Name = "Id";
            this.Id.Visible = false;
            // 
            // TeamChampGames
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label3);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.seasonTextbox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.plusSeasonButton);
            this.Controls.Add(this.minusSeasonButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.FinalDateTextbox);
            this.Controls.Add(this.StartDateTextbox);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.button1);
            this.Name = "TeamChampGames";
            this.Size = new System.Drawing.Size(575, 402);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teamgameBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gamesBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.BindingSource gamesBindingSource;
        private TableTennis.Database.DataSet2 dataSet2;
        private TableTennis.Database.DataSet2TableAdapters.gamesTableAdapter gamesTableAdapter;
        private System.Windows.Forms.TextBox seasonTextbox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button plusSeasonButton;
        private System.Windows.Forms.Button minusSeasonButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MaskedTextBox FinalDateTextbox;
        private System.Windows.Forms.MaskedTextBox StartDateTextbox;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.BindingSource teamgameBindingSource;
        private Database.DataSet2TableAdapters.teamgameTableAdapter teamgameTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn Date;
        private System.Windows.Forms.DataGridViewTextBoxColumn Day;
        private System.Windows.Forms.DataGridViewTextBoxColumn ClubAId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ClubeA;
        private System.Windows.Forms.DataGridViewTextBoxColumn clubAnameAppend;
        private System.Windows.Forms.DataGridViewTextBoxColumn ClubBId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ClubeB;
        private System.Windows.Forms.DataGridViewTextBoxColumn clubBnameAppend;
        private System.Windows.Forms.DataGridViewTextBoxColumn NumGames;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
    }
}
