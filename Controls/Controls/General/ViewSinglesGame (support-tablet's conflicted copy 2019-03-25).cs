﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using static TableTennis.Database.DataSet2;
using TableTennis.Database;

namespace TableTennis.Controls.General
{
    public partial class ViewSinglesGame : UserControl, IAddGameToTeamgame
    {
        private gamesRow currentGame;

        public ViewSinglesGame(int idChamp)
        {
            InitializeComponent();
            UpdateList(idChamp);
        }

        public void UpdateList(int idChamp)
        {
            if (idChamp != 0)
            {
                var data =
                    from g in FilesAccess.db.games.Where(x => x.IdTeamGame == 0)
                    where g.IdChamp == idChamp && g.IdPlayerA != 0 && g.IdPlayerB != 0
                    select new
                    {
                        output = g.id + "|" + g.Date + " " +
                            FilesAccess.db.player.SingleOrDefault(x => x.Id == g.IdPlayerA)?.Name + " " + g.SetsA + " - " + g.SetsB + " " +
                            FilesAccess.db.player.SingleOrDefault(x => x.Id == g.IdPlayerB)?.Name
                    };

                comboBox1.Items.Clear();
                comboBox1.Items.AddRange(data.Select(x => x.output).ToArray());
            }
        }
        
        public void UpdateGame(int idTeamGame)
        {
            currentGame.IdTeamGame = idTeamGame;
            FilesAccess.db.UpdateGames();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            var gameId = comboBox1.Text.Split('|')[0];

            currentGame = FilesAccess.db.games.SingleOrDefault(x => x.id == Convert.ToInt32(gameId));

            var playerA = FilesAccess.db.player.SingleOrDefault(x => x.Id == currentGame.IdPlayerA);
            var playerB = FilesAccess.db.player.SingleOrDefault(x => x.Id == currentGame.IdPlayerB);

            playerANameTxt.Text = playerA.Name;
            clubPlayerALabel.Text = FilesAccess.db.clubs.Single(x => x.Id == playerA.IdClub).Name;
            pointsPlayerALabel.Text = playerA.currentPoints.ToString();
            rankingPlayerALabel.Text = playerA.currentRank.ToString();
            licPlayerA.Text = playerA.licenseNo;
            setsPlayerA.Text = currentGame.SetsA.ToString();

            playerBNameTxt.Text = playerB.Name;
            clubPlayerBLabel.Text = FilesAccess.db.clubs.Single(x => x.Id == playerB.IdClub).Name;
            pointsPlayerBLabel.Text = playerB.currentPoints.ToString();
            rankingPlayerBLabel.Text = playerB.currentRank.ToString();
            licPlayerB.Text = playerB.licenseNo;
            setsPlayerB.Text = currentGame.SetsB.ToString();
        }
    }
}
