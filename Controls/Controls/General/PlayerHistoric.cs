﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TableTennis.Database;
using static TableTennis.Database.DataSet2;

namespace TableTennis.Controls
{
    public partial class PlayerHistoric : UserControl
    {
        public PlayerHistoric()
        {
            InitializeComponent();
            VisibleChanged += PlayerHistoric_VisibleChanged;
        }

        void PlayerHistoric_VisibleChanged(object sender, EventArgs e)
        {
            comboBox1.Items.Clear();
            comboBox2.Items.Clear();
            comboBox3.Items.Clear();
            comboBox4.Items.Clear();
            comboBox1.Items.AddRange(FilesAccess.db.seasons.Select(x => x.name).ToArray());
            comboBox3.Items.AddRange(FilesAccess.db.player.Select(x => x.Name).OrderBy(x => x).ToArray());
            comboBox2.Items.AddRange(FilesAccess.db.clubs.Select(x => x.Name).OrderBy(x => x).ToArray());
            comboBox4.Items.AddRange(FilesAccess.db.championships.Select(x => x.Name).OrderBy(x => x).ToArray());
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int plId = FilesAccess.db.player.Single(x => x.Name == comboBox3.Text).Id;
            int clId = FilesAccess.db.clubs.Single(x => x.Name == comboBox2.Text).Id;
            int seasonId = FilesAccess.db.seasons.Single(x => x.name == comboBox1.Text).id;
            int champId = FilesAccess.db.championships.Single(x => x.Name == comboBox4.Text).Id;
            FilesAccess.db.playerclubseason.AddplayerclubseasonRow(plId, champId, Convert.ToInt32(seasonId), clId);
            FilesAccess.db.UpdatePlayerClubSeason();
            comboBox3_SelectedIndexChanged(this, new EventArgs());
        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            int plId = FilesAccess.db.player.Single(x => x.Name == comboBox3.Text).Id;
            var playerSeasons = FilesAccess.db.playerclubseason.Select("idPlayer=" + plId);
            string show = "";
            foreach (playerclubseasonRow playerSeason in playerSeasons)
            {
                string club = FilesAccess.db.clubs.Single(x => x.Id == playerSeason.idClub).Name;
                string champ = FilesAccess.db.championships.Single(x => x.Id == playerSeason.idChamp).Name;
                string season = FilesAccess.db.seasons.Single(x => x.id == playerSeason.idSeason).name;
                show += season + " | " + club + " | " + champ + Environment.NewLine;
            }
            textBox1.Text = show;
        }
    }
}
