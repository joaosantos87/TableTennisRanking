﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TableTennis.Database;
using TableTennis.Database.DataSet2TableAdapters;

namespace TableTennis.Controls.General
{
    public partial class AddTeamGame : UserControl
    {
        public AddTeamGame()
        {
            InitializeComponent();
            champCombobox.Items.AddRange(FilesAccess.db.championships.OrderBy(x => x.Name).Select(x => x.Name).ToArray());
            clubACombobox.Items.AddRange(FilesAccess.db.clubs.OrderBy(x => x.Name).Select(x => x.Name).ToArray());
            clubBCombobox.Items.AddRange(FilesAccess.db.clubs.OrderBy(x => x.Name).Select(x => x.Name).ToArray());
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        
        private void createGameButton_Click(object sender, EventArgs e)
        {
            int clubA = FilesAccess.db.clubs.Single(x => x.Name == clubACombobox.Text).Id;
            int clubB = FilesAccess.db.clubs.Single(x => x.Name == clubBCombobox.Text).Id;
            int day = Convert.ToInt32(jornadaTextbox.Text);
            int idChamp = FilesAccess.db.championships.Single(x => x.Name == champCombobox.Text).Id;
            string clubAAppend = clubAappendTextbox.Text;
            string clubBAppend = clubBappendTextbox.Text;

            if (FilesAccess.db.teamGames.Count(x => 
                    x.ClubAId == clubA && 
                    x.clubAnameAppend == clubAAppend &&
                    x.ClubBId == clubB && 
                    x.clubBnameAppend == clubBAppend &&
                    x.Date.ToShortDateString() == gameDate.Value.ToShortDateString() && 
                    x.Day == day && 
                    x.IdChampionship == idChamp) == 0)
            {
                new teamgameTableAdapter().Insert(
                        gameDate.Value,
                        idChamp,
                        day,
                        clubA,
                        clubAAppend,
                        clubB,
                        clubBAppend);

                new teamgameTableAdapter().Fill(FilesAccess.db.teamGames);

                var teamGameRow = FilesAccess.db.teamGames.Last();

                label6.Text = $"Id {teamGameRow.Id} - {FilesAccess.db.clubs.SingleOrDefault(x => x.Id == clubA).Name} {teamGameRow.clubAnameAppend} VS {FilesAccess.db.clubs.SingleOrDefault(x => x.Id == clubB).Name} {teamGameRow.clubBnameAppend}";
            }
            else
            {
                MessageBox.Show($"Team Game already inserted!s");
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            clubAappendTextbox.Text = "";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            clubAappendTextbox.Text = "B";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            clubAappendTextbox.Text = "C";
        }

        private void button4_Click(object sender, EventArgs e)
        {
            clubAappendTextbox.Text = "D";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            clubAappendTextbox.Text = "E";
        }

        private void button10_Click(object sender, EventArgs e)
        {
            clubBappendTextbox.Text = "";
        }

        private void button8_Click(object sender, EventArgs e)
        {
            clubBappendTextbox.Text = "B";
        }

        private void button7_Click(object sender, EventArgs e)
        {
            clubBappendTextbox.Text = "C";
        }

        private void button6_Click(object sender, EventArgs e)
        {
            clubBappendTextbox.Text = "D";
        }

        private void button5_Click(object sender, EventArgs e)
        {
            clubBappendTextbox.Text = "E";
        }


    }
}
