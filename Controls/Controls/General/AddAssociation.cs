﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TableTennis.Database;

namespace TableTennis.Controls
{
    public partial class AddAssociation : Form
    {
        public AddAssociation()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == String.Empty)
                return;

            bool insert = true;
            for (int i = 0; i < FilesAccess.db.associations.Count; i++)
            {
                if (Utilities.ContainsWord(FilesAccess.db.associations[i].name, textBox1.Text))
                {
                    if (Utilities.LevenshteinDistance(FilesAccess.db.associations[i].name, textBox1.Text) == 1)
                    {

                        int clubId = FilesAccess.db.clubs[i].Id;
                        MessageBox.Show("Club already insert in database! [id = " + clubId + "]");
                        insert = false;
                        break;
                    }

                    DialogResult mbRes = MessageBox.Show("Similar club already in database [" + FilesAccess.db.clubs[i].Name + "]" +
                        Environment.NewLine + "Do you still want to enter new club?", "Information", MessageBoxButtons.YesNo);

                    //Insert club
                    if (mbRes == DialogResult.Yes)
                    {
                        insert = true;

                    }
                    else if (mbRes == DialogResult.No)
                        insert = false;
                    break;
                }
            }

            if (insert)
            {
                FilesAccess.db.associations.AddassociationsRow(textBox1.Text, true,$"logos/{textBox1.Text}");
                FilesAccess.db.InsertAssociation();
            }
        }
    }
}
