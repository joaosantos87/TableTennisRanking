﻿namespace TableTennis.Controls
{
    partial class AddToTeamGame
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.createGameButton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.gameInsertedLabel = new System.Windows.Forms.Label();
            this.teamGameCombobox = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.button10 = new System.Windows.Forms.Button();
            this.singlesMatchRadiobutton = new System.Windows.Forms.RadioButton();
            this.doublesGameRadiobutton = new System.Windows.Forms.RadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lusitanoRadiobutton = new System.Windows.Forms.RadioButton();
            this.champTextbox = new System.Windows.Forms.TextBox();
            this.dateTextbox = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.teamGameIdTxtbox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.selectSinglesGameIdBtn = new System.Windows.Forms.Button();
            this.singlesGameIdTxtbox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.selectDoublesGameIdBtn = new System.Windows.Forms.Button();
            this.doubleGameIdTxtbox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(216, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(139, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Add Games To Team Game";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 108);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Championship";
            // 
            // createGameButton
            // 
            this.createGameButton.Location = new System.Drawing.Point(260, 374);
            this.createGameButton.Name = "createGameButton";
            this.createGameButton.Size = new System.Drawing.Size(75, 23);
            this.createGameButton.TabIndex = 9;
            this.createGameButton.Text = "Add Game";
            this.createGameButton.UseVisualStyleBackColor = true;
            this.createGameButton.Click += new System.EventHandler(this.createGameButton_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(23, 137);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(30, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Date";
            // 
            // gameInsertedLabel
            // 
            this.gameInsertedLabel.AutoSize = true;
            this.gameInsertedLabel.Location = new System.Drawing.Point(73, 369);
            this.gameInsertedLabel.Name = "gameInsertedLabel";
            this.gameInsertedLabel.Size = new System.Drawing.Size(0, 13);
            this.gameInsertedLabel.TabIndex = 20;
            // 
            // teamGameCombobox
            // 
            this.teamGameCombobox.FormattingEnabled = true;
            this.teamGameCombobox.Location = new System.Drawing.Point(25, 50);
            this.teamGameCombobox.Name = "teamGameCombobox";
            this.teamGameCombobox.Size = new System.Drawing.Size(416, 21);
            this.teamGameCombobox.TabIndex = 41;
            this.teamGameCombobox.SelectedIndexChanged += new System.EventHandler(this.teamGameCombobox_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(22, 32);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(65, 13);
            this.label8.TabIndex = 40;
            this.label8.Text = "Team Game";
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(455, 50);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(103, 23);
            this.button10.TabIndex = 42;
            this.button10.Text = "Add Team Game";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // singlesMatchRadiobutton
            // 
            this.singlesMatchRadiobutton.AutoSize = true;
            this.singlesMatchRadiobutton.Location = new System.Drawing.Point(27, 168);
            this.singlesMatchRadiobutton.Name = "singlesMatchRadiobutton";
            this.singlesMatchRadiobutton.Size = new System.Drawing.Size(90, 17);
            this.singlesMatchRadiobutton.TabIndex = 43;
            this.singlesMatchRadiobutton.TabStop = true;
            this.singlesMatchRadiobutton.Text = "Singles Game";
            this.singlesMatchRadiobutton.UseVisualStyleBackColor = true;
            this.singlesMatchRadiobutton.CheckedChanged += new System.EventHandler(this.singlesMatchRadiobutton_CheckedChanged);
            // 
            // doublesGameRadiobutton
            // 
            this.doublesGameRadiobutton.AutoSize = true;
            this.doublesGameRadiobutton.Location = new System.Drawing.Point(271, 166);
            this.doublesGameRadiobutton.Name = "doublesGameRadiobutton";
            this.doublesGameRadiobutton.Size = new System.Drawing.Size(95, 17);
            this.doublesGameRadiobutton.TabIndex = 45;
            this.doublesGameRadiobutton.TabStop = true;
            this.doublesGameRadiobutton.Text = "Doubles Game";
            this.doublesGameRadiobutton.UseVisualStyleBackColor = true;
            this.doublesGameRadiobutton.CheckedChanged += new System.EventHandler(this.doublesGameRadiobutton_CheckedChanged);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(21, 267);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(536, 99);
            this.panel1.TabIndex = 46;
            // 
            // lusitanoRadiobutton
            // 
            this.lusitanoRadiobutton.AutoSize = true;
            this.lusitanoRadiobutton.Location = new System.Drawing.Point(418, 137);
            this.lusitanoRadiobutton.Name = "lusitanoRadiobutton";
            this.lusitanoRadiobutton.Size = new System.Drawing.Size(65, 17);
            this.lusitanoRadiobutton.TabIndex = 47;
            this.lusitanoRadiobutton.TabStop = true;
            this.lusitanoRadiobutton.Text = "Lusitano";
            this.lusitanoRadiobutton.UseVisualStyleBackColor = true;
            // 
            // champTextbox
            // 
            this.champTextbox.Location = new System.Drawing.Point(101, 105);
            this.champTextbox.Name = "champTextbox";
            this.champTextbox.ReadOnly = true;
            this.champTextbox.Size = new System.Drawing.Size(456, 20);
            this.champTextbox.TabIndex = 48;
            // 
            // dateTextbox
            // 
            this.dateTextbox.Location = new System.Drawing.Point(101, 134);
            this.dateTextbox.Name = "dateTextbox";
            this.dateTextbox.Size = new System.Drawing.Size(215, 20);
            this.dateTextbox.TabIndex = 49;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(322, 131);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 50;
            this.button1.Text = "Update";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(219, 77);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(97, 23);
            this.button2.TabIndex = 57;
            this.button2.Text = "select game id";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // teamGameIdTxtbox
            // 
            this.teamGameIdTxtbox.Location = new System.Drawing.Point(101, 77);
            this.teamGameIdTxtbox.Name = "teamGameIdTxtbox";
            this.teamGameIdTxtbox.Size = new System.Drawing.Size(112, 20);
            this.teamGameIdTxtbox.TabIndex = 56;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(20, 80);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(79, 13);
            this.label5.TabIndex = 55;
            this.label5.Text = "Team Game ID";
            // 
            // selectSinglesGameIdBtn
            // 
            this.selectSinglesGameIdBtn.Location = new System.Drawing.Point(21, 234);
            this.selectSinglesGameIdBtn.Name = "selectSinglesGameIdBtn";
            this.selectSinglesGameIdBtn.Size = new System.Drawing.Size(132, 23);
            this.selectSinglesGameIdBtn.TabIndex = 60;
            this.selectSinglesGameIdBtn.Text = "select singles game id";
            this.selectSinglesGameIdBtn.UseVisualStyleBackColor = true;
            this.selectSinglesGameIdBtn.Click += new System.EventHandler(this.selectSinglesGameIdBtn_Click);
            // 
            // singlesGameIdTxtbox
            // 
            this.singlesGameIdTxtbox.Location = new System.Drawing.Point(23, 208);
            this.singlesGameIdTxtbox.Name = "singlesGameIdTxtbox";
            this.singlesGameIdTxtbox.Size = new System.Drawing.Size(130, 20);
            this.singlesGameIdTxtbox.TabIndex = 59;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(24, 192);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(86, 13);
            this.label4.TabIndex = 58;
            this.label4.Text = "Singles Game ID";
            // 
            // selectDoublesGameIdBtn
            // 
            this.selectDoublesGameIdBtn.Location = new System.Drawing.Point(271, 234);
            this.selectDoublesGameIdBtn.Name = "selectDoublesGameIdBtn";
            this.selectDoublesGameIdBtn.Size = new System.Drawing.Size(132, 23);
            this.selectDoublesGameIdBtn.TabIndex = 63;
            this.selectDoublesGameIdBtn.Text = "select doubles game id";
            this.selectDoublesGameIdBtn.UseVisualStyleBackColor = true;
            // 
            // doubleGameIdTxtbox
            // 
            this.doubleGameIdTxtbox.Location = new System.Drawing.Point(271, 208);
            this.doubleGameIdTxtbox.Name = "doubleGameIdTxtbox";
            this.doubleGameIdTxtbox.Size = new System.Drawing.Size(102, 20);
            this.doubleGameIdTxtbox.TabIndex = 62;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(268, 192);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(123, 13);
            this.label6.TabIndex = 61;
            this.label6.Text = "Singles Double Game ID";
            // 
            // AddToTeamGame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.selectSinglesGameIdBtn);
            this.Controls.Add(this.selectDoublesGameIdBtn);
            this.Controls.Add(this.doubleGameIdTxtbox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.singlesGameIdTxtbox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.teamGameIdTxtbox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dateTextbox);
            this.Controls.Add(this.champTextbox);
            this.Controls.Add(this.lusitanoRadiobutton);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.doublesGameRadiobutton);
            this.Controls.Add(this.singlesMatchRadiobutton);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.teamGameCombobox);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.gameInsertedLabel);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.createGameButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "AddToTeamGame";
            this.Size = new System.Drawing.Size(575, 402);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.Button createGameButton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label gameInsertedLabel;
        private System.Windows.Forms.ComboBox teamGameCombobox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.RadioButton singlesMatchRadiobutton;
        private System.Windows.Forms.RadioButton doublesGameRadiobutton;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton lusitanoRadiobutton;
        private System.Windows.Forms.TextBox champTextbox;
        private System.Windows.Forms.TextBox dateTextbox;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox teamGameIdTxtbox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button selectSinglesGameIdBtn;
        private System.Windows.Forms.TextBox singlesGameIdTxtbox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button selectDoublesGameIdBtn;
        private System.Windows.Forms.TextBox doubleGameIdTxtbox;
        private System.Windows.Forms.Label label6;
    }
}
