﻿namespace TableTennis.Controls.General
{
    partial class AddTeamGameAutomatically
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.fptmWebsiteLinkTxtbox = new System.Windows.Forms.TextBox();
            this.runBtn = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.automaticClubCbbox = new System.Windows.Forms.ComboBox();
            this.databaseClubCbbox = new System.Windows.Forms.ComboBox();
            this.addConnectionClubsBtn = new System.Windows.Forms.Button();
            this.connectChampsBtn = new System.Windows.Forms.Button();
            this.databaseChampsCbbox = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.automaticChampTxtbox = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.databasePlayerCbbox = new System.Windows.Forms.ComboBox();
            this.automaticPlayerCbbox = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.automaticPlayerLicenceTxtbox = new System.Windows.Forms.TextBox();
            this.databasePlayerLicenceTxtbox = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.doubleGamePositionTxtbox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.clubDatabasePlayerLbl = new System.Windows.Forms.Label();
            this.gamesToInsertListbox = new System.Windows.Forms.ListBox();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(575, 26);
            this.label1.TabIndex = 17;
            this.label1.Text = "Add Team Game Automatically";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 13);
            this.label2.TabIndex = 18;
            this.label2.Text = "Fptm Website Link";
            // 
            // fptmWebsiteLinkTxtbox
            // 
            this.fptmWebsiteLinkTxtbox.Location = new System.Drawing.Point(111, 44);
            this.fptmWebsiteLinkTxtbox.Name = "fptmWebsiteLinkTxtbox";
            this.fptmWebsiteLinkTxtbox.Size = new System.Drawing.Size(373, 20);
            this.fptmWebsiteLinkTxtbox.TabIndex = 19;
            this.fptmWebsiteLinkTxtbox.Text = "https://fptm.pt/campeonatos-nacionais/336/";
            // 
            // runBtn
            // 
            this.runBtn.Location = new System.Drawing.Point(493, 42);
            this.runBtn.Name = "runBtn";
            this.runBtn.Size = new System.Drawing.Size(75, 43);
            this.runBtn.TabIndex = 20;
            this.runBtn.Text = "Run";
            this.runBtn.UseVisualStyleBackColor = true;
            this.runBtn.Click += new System.EventHandler(this.runBtn_Click);
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(10, 89);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 50);
            this.label3.TabIndex = 21;
            this.label3.Text = "Clubs";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // automaticClubCbbox
            // 
            this.automaticClubCbbox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.automaticClubCbbox.FormattingEnabled = true;
            this.automaticClubCbbox.Location = new System.Drawing.Point(49, 91);
            this.automaticClubCbbox.Name = "automaticClubCbbox";
            this.automaticClubCbbox.Size = new System.Drawing.Size(435, 21);
            this.automaticClubCbbox.TabIndex = 22;
            // 
            // databaseClubCbbox
            // 
            this.databaseClubCbbox.FormattingEnabled = true;
            this.databaseClubCbbox.Location = new System.Drawing.Point(49, 118);
            this.databaseClubCbbox.Name = "databaseClubCbbox";
            this.databaseClubCbbox.Size = new System.Drawing.Size(435, 21);
            this.databaseClubCbbox.TabIndex = 23;
            // 
            // addConnectionClubsBtn
            // 
            this.addConnectionClubsBtn.Location = new System.Drawing.Point(493, 89);
            this.addConnectionClubsBtn.Name = "addConnectionClubsBtn";
            this.addConnectionClubsBtn.Size = new System.Drawing.Size(72, 50);
            this.addConnectionClubsBtn.TabIndex = 24;
            this.addConnectionClubsBtn.Text = "Connect";
            this.addConnectionClubsBtn.UseVisualStyleBackColor = true;
            this.addConnectionClubsBtn.Click += new System.EventHandler(this.addConnectionClubsBtn_Click);
            // 
            // connectChampsBtn
            // 
            this.connectChampsBtn.Location = new System.Drawing.Point(493, 163);
            this.connectChampsBtn.Name = "connectChampsBtn";
            this.connectChampsBtn.Size = new System.Drawing.Size(72, 50);
            this.connectChampsBtn.TabIndex = 28;
            this.connectChampsBtn.Text = "Connect";
            this.connectChampsBtn.UseVisualStyleBackColor = true;
            this.connectChampsBtn.Click += new System.EventHandler(this.connectChampsBtn_Click);
            // 
            // databaseChampsCbbox
            // 
            this.databaseChampsCbbox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.databaseChampsCbbox.FormattingEnabled = true;
            this.databaseChampsCbbox.Location = new System.Drawing.Point(100, 165);
            this.databaseChampsCbbox.Name = "databaseChampsCbbox";
            this.databaseChampsCbbox.Size = new System.Drawing.Size(384, 21);
            this.databaseChampsCbbox.TabIndex = 26;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(10, 163);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(84, 50);
            this.label4.TabIndex = 25;
            this.label4.Text = "Championships";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // automaticChampTxtbox
            // 
            this.automaticChampTxtbox.Location = new System.Drawing.Point(101, 191);
            this.automaticChampTxtbox.Name = "automaticChampTxtbox";
            this.automaticChampTxtbox.ReadOnly = true;
            this.automaticChampTxtbox.Size = new System.Drawing.Size(383, 20);
            this.automaticChampTxtbox.TabIndex = 29;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(493, 223);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(72, 50);
            this.button1.TabIndex = 33;
            this.button1.Text = "Connect";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // databasePlayerCbbox
            // 
            this.databasePlayerCbbox.FormattingEnabled = true;
            this.databasePlayerCbbox.Location = new System.Drawing.Point(62, 252);
            this.databasePlayerCbbox.Name = "databasePlayerCbbox";
            this.databasePlayerCbbox.Size = new System.Drawing.Size(343, 21);
            this.databasePlayerCbbox.TabIndex = 32;
            this.databasePlayerCbbox.SelectedIndexChanged += new System.EventHandler(this.databasePlayerCbbox_SelectedIndexChanged);
            // 
            // automaticPlayerCbbox
            // 
            this.automaticPlayerCbbox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.automaticPlayerCbbox.FormattingEnabled = true;
            this.automaticPlayerCbbox.Location = new System.Drawing.Point(62, 225);
            this.automaticPlayerCbbox.Name = "automaticPlayerCbbox";
            this.automaticPlayerCbbox.Size = new System.Drawing.Size(343, 21);
            this.automaticPlayerCbbox.TabIndex = 31;
            this.automaticPlayerCbbox.SelectedIndexChanged += new System.EventHandler(this.automaticPlayerCbbox_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(10, 223);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 50);
            this.label5.TabIndex = 30;
            this.label5.Text = "Players";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(325, 295);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(90, 23);
            this.button2.TabIndex = 34;
            this.button2.Text = "Insert Games";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // automaticPlayerLicenceTxtbox
            // 
            this.automaticPlayerLicenceTxtbox.Location = new System.Drawing.Point(411, 225);
            this.automaticPlayerLicenceTxtbox.Name = "automaticPlayerLicenceTxtbox";
            this.automaticPlayerLicenceTxtbox.ReadOnly = true;
            this.automaticPlayerLicenceTxtbox.Size = new System.Drawing.Size(73, 20);
            this.automaticPlayerLicenceTxtbox.TabIndex = 35;
            // 
            // databasePlayerLicenceTxtbox
            // 
            this.databasePlayerLicenceTxtbox.Location = new System.Drawing.Point(411, 253);
            this.databasePlayerLicenceTxtbox.Name = "databasePlayerLicenceTxtbox";
            this.databasePlayerLicenceTxtbox.ReadOnly = true;
            this.databasePlayerLicenceTxtbox.Size = new System.Drawing.Size(73, 20);
            this.databasePlayerLicenceTxtbox.TabIndex = 36;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(454, 281);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(111, 23);
            this.button3.TabIndex = 38;
            this.button3.Text = "Add Player Manual";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(475, 363);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(90, 23);
            this.button4.TabIndex = 39;
            this.button4.Text = "Add Club";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // doubleGamePositionTxtbox
            // 
            this.doubleGamePositionTxtbox.Location = new System.Drawing.Point(132, 65);
            this.doubleGamePositionTxtbox.Name = "doubleGamePositionTxtbox";
            this.doubleGamePositionTxtbox.Size = new System.Drawing.Size(352, 20);
            this.doubleGamePositionTxtbox.TabIndex = 41;
            this.doubleGamePositionTxtbox.Text = "0";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(10, 68);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(116, 13);
            this.label6.TabIndex = 40;
            this.label6.Text = "Doubles Game position";
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(454, 310);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(111, 23);
            this.button5.TabIndex = 42;
            this.button5.Text = "Add Player Auto";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(325, 363);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(90, 23);
            this.button6.TabIndex = 44;
            this.button6.Text = "Remove game";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // clubDatabasePlayerLbl
            // 
            this.clubDatabasePlayerLbl.AutoSize = true;
            this.clubDatabasePlayerLbl.Location = new System.Drawing.Point(60, 275);
            this.clubDatabasePlayerLbl.Name = "clubDatabasePlayerLbl";
            this.clubDatabasePlayerLbl.Size = new System.Drawing.Size(28, 13);
            this.clubDatabasePlayerLbl.TabIndex = 45;
            this.clubDatabasePlayerLbl.Text = "Club";
            // 
            // gamesToInsertListbox
            // 
            this.gamesToInsertListbox.FormattingEnabled = true;
            this.gamesToInsertListbox.Location = new System.Drawing.Point(13, 295);
            this.gamesToInsertListbox.Name = "gamesToInsertListbox";
            this.gamesToInsertListbox.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.gamesToInsertListbox.Size = new System.Drawing.Size(306, 95);
            this.gamesToInsertListbox.TabIndex = 46;
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(490, 3);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(75, 23);
            this.button7.TabIndex = 47;
            this.button7.Text = "Update";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(431, 335);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(134, 23);
            this.button8.TabIndex = 48;
            this.button8.Text = "Add ALL PLAYER Auto";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // AddTeamGameAutomatically
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.gamesToInsertListbox);
            this.Controls.Add(this.clubDatabasePlayerLbl);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.doubleGamePositionTxtbox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.databasePlayerLicenceTxtbox);
            this.Controls.Add(this.automaticPlayerLicenceTxtbox);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.databasePlayerCbbox);
            this.Controls.Add(this.automaticPlayerCbbox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.automaticChampTxtbox);
            this.Controls.Add(this.connectChampsBtn);
            this.Controls.Add(this.databaseChampsCbbox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.addConnectionClubsBtn);
            this.Controls.Add(this.databaseClubCbbox);
            this.Controls.Add(this.automaticClubCbbox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.runBtn);
            this.Controls.Add(this.fptmWebsiteLinkTxtbox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "AddTeamGameAutomatically";
            this.Size = new System.Drawing.Size(575, 402);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox fptmWebsiteLinkTxtbox;
        private System.Windows.Forms.Button runBtn;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox automaticClubCbbox;
        private System.Windows.Forms.ComboBox databaseClubCbbox;
        private System.Windows.Forms.Button addConnectionClubsBtn;
        private System.Windows.Forms.Button connectChampsBtn;
        private System.Windows.Forms.ComboBox databaseChampsCbbox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox automaticChampTxtbox;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox databasePlayerCbbox;
        private System.Windows.Forms.ComboBox automaticPlayerCbbox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox automaticPlayerLicenceTxtbox;
        private System.Windows.Forms.TextBox databasePlayerLicenceTxtbox;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox doubleGamePositionTxtbox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Label clubDatabasePlayerLbl;
        private System.Windows.Forms.ListBox gamesToInsertListbox;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
    }
}
