﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TableTennis.Database;
using TableTennis.Database.DataSet2TableAdapters;
using TableTennis.General.WebScraping;
using static TableTennis.Database.DataSet2;
using System.Diagnostics;

namespace TableTennis.Controls.General
{
    public partial class AddTeamGameAutomatically : UserControl
    {
        List<TeamGame> allTeamGames;
        List<Player> allPlayersList;
        public AddTeamGameAutomatically()
        {
            InitializeComponent();
            Dock = DockStyle.Fill;
            databaseClubCbbox.Items.AddRange(FilesAccess.db.clubs.Where(x => x.nameAutomaticInsertion=="").Select(x => x.Name).OrderBy(x => x).ToArray());
            
        }


        private void runBtn_Click(object sender, EventArgs e)
        {
            if (!Int32.TryParse(doubleGamePositionTxtbox.Text, out int doubleGamePostion))
            {
                MessageBox.Show("Posicao do jogo de pares nao é um inteiro");
                return;
            }

            var scraping = new FptmWebsiteScraping(fptmWebsiteLinkTxtbox.Text, doubleGamePostion);

            allTeamGames = scraping.AllTeamGames.ToList();

            var allTeams = allTeamGames.Select(x => x.TeamA.Nome).ToList();
            allTeams.AddRange(allTeamGames.Select(x => x.TeamB.Nome));

            allTeams = allTeams.Distinct().ToList();

            var clubsInAutomatic = FilesAccess.db.clubs.Where(x => x.nameAutomaticInsertion != "").Select(x => x.nameAutomaticInsertion);

            foreach (var clubInAutomatic in clubsInAutomatic)
                allTeams.Remove(clubInAutomatic);

            automaticClubCbbox.Items.Clear();
            automaticClubCbbox.Items.AddRange(allTeams.ToArray());

            var champ = allTeamGames.First().Torneio;
            automaticChampTxtbox.Text = champ;

            var champsInAutomatic = FilesAccess.db.championships.Where(x => x.nameAutomaticInsertion != "").Select(x => x.nameAutomaticInsertion);

            if (!champsInAutomatic.Contains(champ))
            {
                databaseChampsCbbox.Items.Clear();
                databaseChampsCbbox.Items.AddRange(FilesAccess.db.championships/*.Where(x => x.nameAutomaticInsertion == "")*/.Select(x => x.Name).ToArray());
            }

            var allPlayers = allTeamGames.Select(x => x.TeamA.Jogadores).ToList();
            allPlayers.AddRange(allTeamGames.Select(x => x.TeamB.Jogadores));
            allPlayers = allPlayers.Distinct().ToList();

            List<string> nomes = new List<string>();
            allPlayersList = new List<Player>();
            foreach (var p in allPlayers)
            {
                foreach (var a in p)
                {
                    if (!nomes.Contains(a.Nome))
                    {
                        nomes.Add(a.Nome);
                        allPlayersList.Add(a);
                    }

                    var pDatabase = FilesAccess.db.player.SingleOrDefault(x => x.licenseNo == a.Licenca);
                    if(pDatabase != null)
                    {
                        if (pDatabase.nameAutomaticInsertion != a.Nome || pDatabase.licenseNo != a.Licenca)
                        {
                            pDatabase.nameAutomaticInsertion = a.Nome;
                            pDatabase.licenseNo = a.Licenca;
                            new playerTableAdapter().Update(pDatabase);
                            new playerTableAdapter().Fill(FilesAccess.db.player);
                        }
                    }
                }
            }

            var playersAlreadyInserted = FilesAccess.db.player.Where(x => x.nameAutomaticInsertion != "").Select(x => x.nameAutomaticInsertion).ToArray();


            foreach(var player in playersAlreadyInserted)
            {
                nomes.Remove(player);
            }

            automaticPlayerCbbox.Items.Clear();
            automaticPlayerCbbox.Items.AddRange(nomes.ToArray());

            databasePlayerCbbox.Items.Clear();
            databasePlayerCbbox.Items.AddRange(FilesAccess.db.player.Where(x => x.nameAutomaticInsertion == "").Select(x => x.Name).OrderBy(x=>x).ToArray());

            gamesToInsertListbox.Items.Clear();

            foreach (var tg in allTeamGames)
                gamesToInsertListbox.Items.Add(tg.ToString());
        }

        private void addConnectionClubsBtn_Click(object sender, EventArgs e)
        {
            var clubRow = FilesAccess.db.clubs.Single(x => x.Name == databaseClubCbbox.Text);
            clubRow.nameAutomaticInsertion = automaticClubCbbox.Text;

            new clubsTableAdapter().Update(clubRow);
            new clubsTableAdapter().Fill(FilesAccess.db.clubs);
        }

        private void connectChampsBtn_Click(object sender, EventArgs e)
        {
            var champRow = FilesAccess.db.championships.Single(x => x.Name == databaseChampsCbbox.Text);
            champRow.nameAutomaticInsertion = automaticChampTxtbox.Text;

            new championshipsTableAdapter().Update(champRow);
            new championshipsTableAdapter().Fill(FilesAccess.db.championships);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                var playerDatabase = FilesAccess.db.player.Single(x => x.Name == databasePlayerCbbox.Text);

                var playerAutomatic = allPlayersList.Single(x => x.Nome == automaticPlayerCbbox.Text);

                playerDatabase.nameAutomaticInsertion = playerAutomatic.Nome;
                playerDatabase.licenseNo = playerAutomatic.Licenca;
                //playerDatabase.Photo = playerAutomatic.GetImage

                new playerTableAdapter().Update(playerDatabase);
                new playerTableAdapter().Fill(FilesAccess.db.player);

                automaticPlayerCbbox.Text = "";
                automaticPlayerCbbox.Items.Remove(playerAutomatic.Nome);
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.Message);
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            foreach(var tg in allTeamGames)
            {
                try
                {

                    var clubAId = FilesAccess.db.clubs.Single(x => x.nameAutomaticInsertion == tg.TeamA.Nome).Id;
                    var clubBId = FilesAccess.db.clubs.Single(x => x.nameAutomaticInsertion == tg.TeamB.Nome).Id;
                    var clubA_appendText = tg.TeamA.AppendText != null ? tg.TeamA.AppendText : "";
                    var clubB_appendText = tg.TeamB.AppendText != null ? tg.TeamB.AppendText : "";

                    var champ = FilesAccess.db.championships.Single(x => x.nameAutomaticInsertion == tg.Torneio);

                    var tgDatabase = FilesAccess.db.teamGames.SingleOrDefault(x => x.Date.Year == tg.Date.Year && x.ClubAId == clubAId && x.clubAnameAppend == clubA_appendText && x.ClubBId == clubBId && x.clubBnameAppend == clubB_appendText && x.Day == tg.GetMatchDayInt && x.IdChampionship == champ.Id);

                    if (tgDatabase == null)
                    {
                        new teamgameTableAdapter().Insert(
                            tg.Date,
                            champ.Id,
                            tg.GetMatchDayInt,
                            clubAId,
                            clubA_appendText,
                            clubBId,
                            clubB_appendText);

                        new teamgameTableAdapter().Fill(FilesAccess.db.teamGames);
                        tgDatabase = FilesAccess.db.teamGames.Last();
                    }
                    else
                    {
                        if(tg.Date!=new DateTime(1,1,1))
                            tgDatabase.Date = tg.Date;
                    }

                    //insert the games
                    if(tg.DoublesGame!=null)
                    {
                        var playerA_1 = FilesAccess.db.player.SingleOrDefault(x => x.licenseNo == tg.DoublesGame.playerA_1.Licenca);
                        if (playerA_1 == null)
                            playerA_1 = FilesAccess.db.player.SingleOrDefault(x => x.nameAutomaticInsertion == tg.DoublesGame.playerA_1.Nome);

                        var playerA_2 = FilesAccess.db.player.SingleOrDefault(x => x.licenseNo == tg.DoublesGame.playerA_2.Licenca);
                        if (playerA_2 == null)
                            playerA_2 = FilesAccess.db.player.SingleOrDefault(x => x.nameAutomaticInsertion == tg.DoublesGame.playerA_2.Nome);

                        var playerB_1 = FilesAccess.db.player.SingleOrDefault(x => x.licenseNo == tg.DoublesGame.playerB_1.Licenca);
                        if (playerB_1 == null)
                            playerB_1 = FilesAccess.db.player.SingleOrDefault(x => x.nameAutomaticInsertion == tg.DoublesGame.playerB_1.Nome);

                        var playerB_2 = FilesAccess.db.player.SingleOrDefault(x => x.licenseNo == tg.DoublesGame.playerB_2.Licenca);
                        if (playerB_2 == null)
                            playerB_2 = FilesAccess.db.player.SingleOrDefault(x => x.nameAutomaticInsertion == tg.DoublesGame.playerB_2.Nome);

                        var winner1_id = tg.DoublesGame.setsA > tg.DoublesGame.setsB ? playerA_1.Id : playerB_1.Id;
                        var winner2_id = tg.DoublesGame.setsA > tg.DoublesGame.setsB ? playerA_2.Id : playerB_2.Id;

                        var dg = FilesAccess.db.doubleGames.SingleOrDefault(x => x.Date.ToShortDateString() == tg.Date.ToShortDateString() && x.IdChampionship == champ.Id && x.DoubleA_PlayerAId == playerA_1.Id && x.DoubleA_PlayerBId == playerA_2.Id && x.DoubleB_PlayerAId == playerB_1.Id && x.DoubleB_PlayerBId == playerB_2.Id && x.IdPlayerAWinner == winner1_id && x.IdPlayerBWinner == winner2_id && x.IdTeamGame == tgDatabase.Id && x.SetsA == tg.DoublesGame.setsA && x.SetsB == tg.DoublesGame.setsB);

                        if (dg == null)
                        {
                            new doublegamesTableAdapter().Insert(
                                playerA_1.Id,
                                playerA_2.Id,
                                playerB_1.Id,
                                playerB_2.Id,
                                tg.DoublesGame.setsA,
                                tg.DoublesGame.setsB,
                                winner1_id,
                                winner2_id,
                                tg.Date,
                                champ.Id,
                                0,
                                0,
                                0,
                                0,
                                1,
                                tgDatabase.Id,
                                0,
                                DateTime.Now,
                                0,
                                0,
                                0,
                                0,
                                0);
                            new doublegamesTableAdapter().Fill(FilesAccess.db.doubleGames);
                        }
                        else
                        {
                            Trace.TraceWarning($"Game already inserted. {tg.DoublesGame} not added to the database");
                        }
                    }

                    foreach(var sg in tg.SingleGames)
                    {
                        var playerA = FilesAccess.db.player.SingleOrDefault(x => x.licenseNo == sg.playerA.Licenca);

                        if (playerA == null)
                            playerA = FilesAccess.db.player.Single(x => x.nameAutomaticInsertion == sg.playerA.Nome);

                        var playerB = FilesAccess.db.player.SingleOrDefault(x => x.licenseNo == sg.playerB.Licenca);

                        if (playerB == null)
                            playerB = FilesAccess.db.player.Single(x => x.nameAutomaticInsertion == sg.playerB.Nome);

                        var winner_id = sg.setsA > sg.setsB ? playerA.Id : playerB.Id;

                        var dg = FilesAccess.db.games.SingleOrDefault(x => x.Date.ToShortDateString() == tg.Date.ToShortDateString() && x.IdChamp == champ.Id && x.IdPlayerA == playerA.Id && x.IdPlayerB == playerB.Id && x.IdPlayerWinner == winner_id && x.IdTeamGame == tgDatabase.Id && x.SetsA == sg.setsA && x.SetsB == sg.setsB);

                        if (dg == null)
                        {
                            new gamesTableAdapter().Insert(
                                playerA.Id,
                                playerB.Id,
                                sg.setsA,
                                sg.setsB,
                                winner_id,
                                tg.Date,
                                champ.Id,
                                0,
                                0,
                                0,
                                1,
                                tgDatabase.Id,
                                0,
                                DateTime.Now,
                                0,
                                0,
                                0);
                                Trace.TraceWarning($"{sg} Added to the database sucessfully");
                            }
                        else
                        {
                            Trace.TraceWarning($"Game already inserted. {sg} not added to the database");
                        }
                    }
                }
                catch (Exception ex)
                {
                    Trace.TraceError($"exception on match {tg}: {ex.Message}");
                }
            }

            new gamesTableAdapter().Fill(FilesAccess.db.games);
        }

        private void automaticPlayerCbbox_SelectedIndexChanged(object sender, EventArgs e)
        {
            var playerAutomatic = allPlayersList.Single(x => x.Nome == automaticPlayerCbbox.Text);
            automaticPlayerLicenceTxtbox.Text = playerAutomatic.Licenca;
        }

        private void databasePlayerCbbox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                var playerDatabase = FilesAccess.db.player.Single(x => x.Name == databasePlayerCbbox.Text);
                databasePlayerLicenceTxtbox.Text = playerDatabase.licenseNo;
                clubDatabasePlayerLbl.Text = FilesAccess.db.clubs.Single(x => x.Id == playerDatabase.IdClub).Name;
            }
            catch (Exception ex)
            {
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            new GenericForm(new AddPlayer()).ShowDialog();
            databasePlayerCbbox.Items.Clear();
            databasePlayerCbbox.Items.AddRange(FilesAccess.db.player.Where(x => x.nameAutomaticInsertion == "").Select(x => x.Name).OrderBy(x => x).ToArray());
        }

        private void button4_Click(object sender, EventArgs e)
        {
            new GenericForm(new AddClub()).ShowDialog();
            databaseClubCbbox.Items.Clear();
            databaseClubCbbox.Items.AddRange(FilesAccess.db.clubs.Where(x => x.nameAutomaticInsertion == "").Select(x => x.Name).OrderBy(x => x).ToArray());
        }

        private void button5_Click(object sender, EventArgs e)
        {
            var playerAutomatic = allPlayersList.Single(x => x.Nome == automaticPlayerCbbox.Text);

            new GenericForm(new AddPlayer(playerAutomatic.Nome, playerAutomatic.Licenca)).ShowDialog();

            //new playerTableAdapter().Insert(playerAutomatic.Nome, new DateTime(1970, 1, 1), FilesAccess.db.clubs.Single(x => x.Name.ToLower() == "nao disponivel").Id, "Male", "", 0, 0, 0, playerAutomatic.Licenca, 0, playerAutomatic.Nome, 1000, 1, 4);
            new playerTableAdapter().Fill(FilesAccess.db.player);

            databasePlayerCbbox.Items.Remove(automaticPlayerCbbox.Text);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            var gamesToRemove = gamesToInsertListbox.SelectedItems;
            for (int i=0; i<gamesToRemove.Count;i++)
            {
                var gm = gamesToRemove[i];
                gamesToInsertListbox.Items.Remove(gm);

                allTeamGames.RemoveAll(x => x.ToString() == gm.ToString());
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            databaseClubCbbox.Items.Clear();
            databaseChampsCbbox.Items.Clear();
            databasePlayerCbbox.Items.Clear();
            new clubsTableAdapter().Fill(FilesAccess.db.clubs);
            new championshipsTableAdapter().Fill(FilesAccess.db.championships);
            new playerTableAdapter().Fill(FilesAccess.db.player);

            databaseClubCbbox.Items.AddRange(FilesAccess.db.clubs.Where(x => x.nameAutomaticInsertion == "").Select(x => x.Name).OrderBy(x => x).ToArray());

            var champsInAutomatic = FilesAccess.db.championships.Where(x => x.nameAutomaticInsertion != "").Select(x => x.nameAutomaticInsertion);

            if (allTeamGames?.Count > 0)
            {
                var champ = allTeamGames.First().Torneio;
                if (!champsInAutomatic.Contains(champ))
                {
                    databaseChampsCbbox.Items.Clear();
                    databaseChampsCbbox.Items.AddRange(FilesAccess.db.championships/*.Where(x => x.nameAutomaticInsertion == "")*/.Select(x => x.Name).ToArray());
                }
            }
            else
            {
                databaseChampsCbbox.Items.Clear();
                databaseChampsCbbox.Items.AddRange(FilesAccess.db.championships/*.Where(x => x.nameAutomaticInsertion == "")*/.Select(x => x.Name).ToArray());
            }

            databasePlayerCbbox.Items.Clear();
            databasePlayerCbbox.Items.AddRange(FilesAccess.db.player.Where(x => x.nameAutomaticInsertion == "").Select(x => x.Name).OrderBy(x => x).ToArray());
        }

        private void button8_Click(object sender, EventArgs e)
        {

            foreach(var name in automaticPlayerCbbox.Items)
            {
                var playerAutomatic = allPlayersList.Single(x => x.Nome == name.ToString());
                new playerTableAdapter().Insert(playerAutomatic.Nome, new DateTime(1970, 1, 1), FilesAccess.db.clubs.Single(x => x.Name.ToLower() == "nao disponivel").Id, "Female", "", 0, 0, 0, playerAutomatic.Licenca, 0, playerAutomatic.Nome, 1000, 1, 4);
            }

            //new playerTableAdapter().Insert(playerAutomatic.Nome, new DateTime(1970, 1, 1), FilesAccess.db.clubs.Single(x => x.Name.ToLower() == "nao disponivel").Id, "Male", "", 0, 0, 0, playerAutomatic.Licenca, 0, playerAutomatic.Nome, 1000, 1, 4);
            new playerTableAdapter().Fill(FilesAccess.db.player);

            databasePlayerCbbox.Items.Remove(automaticPlayerCbbox.Text);
        }
    }
}
