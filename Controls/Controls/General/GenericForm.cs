﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TableTennis.Controls
{
    public partial class GenericForm : Form
    {
        public GenericForm(Control control)
        {
            InitializeComponent();
            Name = control.Name;
            control.Dock = DockStyle.Fill;
            Controls.Add(control);
            this.Size = control.Size;
        }
    }
}
