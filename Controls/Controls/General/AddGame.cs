﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TableTennis.Database;
using TableTennis.Controls.General;
using static TableTennis.Database.DataSet2;
using TableTennis.Database.DataSet2TableAdapters;
using System.Diagnostics;

namespace TableTennis.Controls
{
    public partial class AddGame : UserControl
    {
        private UserControl insertGame;

        public AddGame()
        {
            InitializeComponent();
            Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
            singlesMatchRadiobutton.Checked = true;
            VisibleChanged += AddGame_IsVisibleChanged;
            phaseCombobox.Items.Clear();
            for (int i = 0; i < FilesAccess.db.phase.Count; i++)
            {
                phaseCombobox.Items.Add(FilesAccess.db.phase[i].id+"-"+FilesAccess.db.phase[i].phase);
            }

            teamGameCombobox.Items.AddRange(FilesAccess.db.teamGames.Select(
                x => GetTeamGame(x)).ToArray());

            filterChamp.Items.AddRange(FilesAccess.db.championships.OrderBy(x => x.Name).Select(x => x.Name).ToArray());
        }

        private string GetTeamGame(teamgameRow x)
        {
            return $"{x.Id} - [{x.Date} Jornada " + x.Day + "]" +
                FilesAccess.db.championships.SingleOrDefault(a => a.Id == x.IdChampionship).Name + " " +
                FilesAccess.db.clubs.SingleOrDefault(a => a.Id == x.ClubAId).Name + " " + x.clubAnameAppend + " " + x.GetResult() +" "+
                FilesAccess.db.clubs.SingleOrDefault(a => a.Id == x.ClubBId).Name + " " + x.clubBnameAppend;
        }
        

        private void AddGame_IsVisibleChanged(object sender, EventArgs e)
        {
            UpdateChampionship();
            (insertGame as IInsertGame).UpdatePlayersCombobox();
        }
        
        public void createGameButton_Click(object sender, EventArgs e)
        {
            int selectedIndex = teamGameCombobox.SelectedIndex;
            teamgameRow teamGame = null;
            if (teamGameCombobox.Text != "")
                //teamGame = FilesAccess.db.teamGames.SingleOrDefault(x => GetTeamGame(x) == teamGameCombobox.Text);
                teamGame = FilesAccess.db.teamGames.SingleOrDefault(x => x.Id == Convert.ToInt32(teamGameCombobox.Text.Split('-')[0]));

            if(teamGameCombobox.Text!="" && teamGame == null)
            {
                var dr = MessageBox.Show("Team game is filled but it is null! Do you want to insert 0 in teamGame column?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

                if (dr == DialogResult.No)
                    return;
            }

            (insertGame as IInsertGame).CreateGame(gameDate.Value, champCombobox.Text, phaseCombobox.Text, teamGame!=null?teamGame.Id:0);

            //if (teamGameCombobox.Text != "")
            //{
            //    filterChamp_SelectedIndexChanged(this, new EventArgs());
            //    teamGameCombobox.SelectedIndex = selectedIndex;
            //}
        }
        
        private void UpdateChampionship()
        {
            string champStr = champCombobox.Text;
            champCombobox.Items.Clear();

            champCombobox.Items.AddRange(FilesAccess.db.championships.OrderBy(x => x.Name).Select(x => x.Name).ToArray());
            
            champCombobox.Text = champStr;
        }

        private void champCombobox_SelectedIndexChanged(object sender, EventArgs e)
        {
            (insertGame as IInsertGame).UpdatePlayersCombobox();
            var champ = FilesAccess.db.championships.SingleOrDefault(x => x.Name == champCombobox.Text);
            var tournType = FilesAccess.db.tournamentType.SingleOrDefault(x => x.id == champ.idTournamentType);
            if (!tournType.championship)
                gameDate.Value = champ.Date;
        }

        private void AddGame_Load(object sender, EventArgs e)
        {

        }

        private void button10_Click(object sender, EventArgs e)
        {
            new AddTeamGameForm().ShowDialog();
            teamGameCombobox.Items.Clear();
            teamGameCombobox.Items.AddRange(FilesAccess.db.teamGames.Select( x => GetTeamGame(x)).ToArray());
        }

        private void teamGameCombobox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                var teamGame = FilesAccess.db.teamGames.SingleOrDefault(x => x.Id == Convert.ToInt32(teamGameCombobox.Text.Split('-')[0]));
                champCombobox.Text = FilesAccess.db.championships.Single(a => a.Id == 
                teamGame.IdChampionship).Name;
                gameDate.Value = teamGame.Date;

                //when a team game is selected, the phase should be N/A
                phaseCombobox.Text = Utilities.Phase_NA.id + "-" + Utilities.Phase_NA.phase;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void singlesMatchRadiobutton_CheckedChanged(object sender, EventArgs e)
        {
            if (singlesMatchRadiobutton.Checked)
            {
                insertGame = new SinglesGame();
                panel1.Controls.Clear();
                panel1.Controls.Add(insertGame);
            }
        }

        private void doublesGameRadiobutton_CheckedChanged(object sender, EventArgs e)
        {
            if (doublesGameRadiobutton.Checked)
            {
                insertGame = new DoublesGame();
                panel1.Controls.Clear();
                panel1.Controls.Add(insertGame);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                var teamGame = FilesAccess.db.teamGames.Single(x => GetTeamGame(x) == teamGameCombobox.Text);
                teamGame.IdChampionship = FilesAccess.db.championships.Single(a => a.Name == champCombobox.Text).Id;
                teamGame.Date = gameDate.Value;
                new teamgameTableAdapter().Update(teamGame);
                new teamgameTableAdapter().Fill(FilesAccess.db.teamGames);

                filterChamp_SelectedIndexChanged(this, new EventArgs());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Update Team Game");
            }
        }

        private void filterChamp_SelectedIndexChanged(object sender, EventArgs e)
        {
            teamGameCombobox.Items.Clear();
            if (!filterChamp.Text.Equals(string.Empty))
            {
                var champ = FilesAccess.db.championships.Single(x => x.Name == filterChamp.Text);
                teamGameCombobox.Items.AddRange(FilesAccess.db.teamGames.Where(x => x.IdChampionship == champ.Id).Select(x => GetTeamGame(x)).ToArray());
            }
            else
            {
                teamGameCombobox.Items.AddRange(FilesAccess.db.teamGames.Select(x => GetTeamGame(x)).ToArray());
            }
        }

        private void phaseCombobox_SelectedIndexChanged(object sender, EventArgs e)
        {
            var champ = FilesAccess.db.championships.Single(x => x.Name == champCombobox.Text);
            
        }

        private void fillGamesBtn_Click(object sender, EventArgs e)
        {
            Stopwatch s1 = Stopwatch.StartNew();
            new gamesTableAdapter().Fill(FilesAccess.db.games);
            s1.Stop();
            Stopwatch s2 = Stopwatch.StartNew();
            new doublegamesTableAdapter().Fill(FilesAccess.db.doubleGames);
            s2.Stop();
            Trace.TraceInformation($"Time to fill games: {s1.ElapsedMilliseconds} ms");
            Trace.TraceInformation($"Time to fill double games: {s2.ElapsedMilliseconds} ms");
        }

        private void teamGameIdTxtbox_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (!Int32.TryParse(teamGameIdTxtbox.Text, out int teamGameId))
                return;

            var teamGames = FilesAccess.db.teamGames.Select(x => x.Id);

            if (!teamGames.Contains(teamGameId))
                return;

            var teamGame = FilesAccess.db.teamGames.Single(x => x.Id == teamGameId);

            var tg = GetTeamGame(teamGame);
            teamGameCombobox.SelectedItem = tg;
        }
    }
}