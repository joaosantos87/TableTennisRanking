﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TableTennis.Database;
using static TableTennis.Database.DataSet2;

namespace TableTennis.Controls
{
    public partial class EditPlayer : UserControl
    {
        playerRow player;
        Binding binding;
        Binding binding1;

        public EditPlayer()
        {
            InitializeComponent();
            VisibleChanged += ViewGames_VisibleChanged;
        }

        public DateTime BirthDate
        {
            get
            {
                try
                {
                    return new DateTime(Convert.ToInt32(textBox1.Text), 1, 1);
                }
                catch { return new DateTime(1900, 1, 1); }
            }
        }

        private void ViewGames_VisibleChanged(object sender, EventArgs e)
        {
            comboBox1.Items.Clear();
            for (int i = 0; i < FilesAccess.db.player.Count; i++)
            {
                comboBox1.Items.Add(FilesAccess.db.player[i].Name);
            }
            comboBox1.Sorted = true;

            comboBox2.Items.Clear();
            for (int i = 0; i < FilesAccess.db.clubs.Count; i++)
            {
                comboBox2.Items.Add(FilesAccess.db.clubs[i].Name);
            }
            comboBox2.Sorted = true;

            if(comboBox1.Items.Count>0)
                comboBox1.SelectedIndex = 0;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (FilesAccess.db.player.Count(x => x.Name == comboBox1.Text) > 1)
                {
                    MessageBox.Show($"Encontrado jogador em duplicado: {comboBox1.Text}");
                    return;
                }
                if (player != null)
                {
                    //textBox2.DataBindings.Remove(binding);
                    //textBox1.DataBindings.Remove(binding1);
                }

                //binding = new Binding("text", player, "licenseNo");
                //binding1 = new Binding("text", player, "BirthDateYear");

                player = FilesAccess.db.player.Single(x => x.Name == comboBox1.Text);

                //textBox2.DataBindings.Add(binding);
                //textBox1.DataBindings.Add(binding1);

                textBox1.Text = player.BirthDate.ToString("YYYY");
                textBox2.Text = player.licenseNo;
                comboBox2.Text = FilesAccess.db.clubs.Single(x => x.Id == player.IdClub).Name;
                label2.Text = player.Id.ToString();
            }
            catch(Exception ex) 
            {
                MessageBox.Show(ex.Message, "Seleccionar jogador");

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FilesAccess.db.player.Single(x => x.Id == player.Id).BirthDate = BirthDate;
            clubsRow clubRow = FilesAccess.db.clubs.Single(x=>x.Name == comboBox2.Text);
            FilesAccess.db.player.Single(x => x.Id == player.Id).IdClub = clubRow.Id;
            FilesAccess.db.player.Single(x => x.Id == player.Id).licenseNo = textBox2.Text;
            FilesAccess.db.UpdatePlayer();
        }
    }
}
