﻿namespace TableTennis.Controls.General
{
    partial class SinglesGame
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.licPlayerB = new System.Windows.Forms.Label();
            this.licPlayerA = new System.Windows.Forms.Label();
            this.button9 = new System.Windows.Forms.Button();
            this.pointsPlayerBLabel = new System.Windows.Forms.Label();
            this.rankingPlayerBLabel = new System.Windows.Forms.Label();
            this.pointsPlayerALabel = new System.Windows.Forms.Label();
            this.rankingPlayerALabel = new System.Windows.Forms.Label();
            this.clubPlayerBLabel = new System.Windows.Forms.Label();
            this.clubPlayerALabel = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.setsPlayerB = new System.Windows.Forms.TextBox();
            this.playerBName = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.setsPlayerA = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.playerAName = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.gameInsertedLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // licPlayerB
            // 
            this.licPlayerB.AutoSize = true;
            this.licPlayerB.Location = new System.Drawing.Point(36, 136);
            this.licPlayerB.Name = "licPlayerB";
            this.licPlayerB.Size = new System.Drawing.Size(35, 13);
            this.licPlayerB.TabIndex = 63;
            this.licPlayerB.Text = "label7";
            // 
            // licPlayerA
            // 
            this.licPlayerA.AutoSize = true;
            this.licPlayerA.Location = new System.Drawing.Point(36, 50);
            this.licPlayerA.Name = "licPlayerA";
            this.licPlayerA.Size = new System.Drawing.Size(35, 13);
            this.licPlayerA.TabIndex = 62;
            this.licPlayerA.Text = "label7";
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(464, 62);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(68, 23);
            this.button9.TabIndex = 61;
            this.button9.Text = "Add Player";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // pointsPlayerBLabel
            // 
            this.pointsPlayerBLabel.AutoSize = true;
            this.pointsPlayerBLabel.Location = new System.Drawing.Point(364, 136);
            this.pointsPlayerBLabel.Name = "pointsPlayerBLabel";
            this.pointsPlayerBLabel.Size = new System.Drawing.Size(35, 13);
            this.pointsPlayerBLabel.TabIndex = 60;
            this.pointsPlayerBLabel.Text = "label7";
            // 
            // rankingPlayerBLabel
            // 
            this.rankingPlayerBLabel.AutoSize = true;
            this.rankingPlayerBLabel.Location = new System.Drawing.Point(310, 136);
            this.rankingPlayerBLabel.Name = "rankingPlayerBLabel";
            this.rankingPlayerBLabel.Size = new System.Drawing.Size(35, 13);
            this.rankingPlayerBLabel.TabIndex = 59;
            this.rankingPlayerBLabel.Text = "label7";
            // 
            // pointsPlayerALabel
            // 
            this.pointsPlayerALabel.AutoSize = true;
            this.pointsPlayerALabel.Location = new System.Drawing.Point(364, 52);
            this.pointsPlayerALabel.Name = "pointsPlayerALabel";
            this.pointsPlayerALabel.Size = new System.Drawing.Size(35, 13);
            this.pointsPlayerALabel.TabIndex = 58;
            this.pointsPlayerALabel.Text = "label7";
            // 
            // rankingPlayerALabel
            // 
            this.rankingPlayerALabel.AutoSize = true;
            this.rankingPlayerALabel.Location = new System.Drawing.Point(310, 51);
            this.rankingPlayerALabel.Name = "rankingPlayerALabel";
            this.rankingPlayerALabel.Size = new System.Drawing.Size(35, 13);
            this.rankingPlayerALabel.TabIndex = 57;
            this.rankingPlayerALabel.Text = "label7";
            // 
            // clubPlayerBLabel
            // 
            this.clubPlayerBLabel.AutoSize = true;
            this.clubPlayerBLabel.Location = new System.Drawing.Point(78, 136);
            this.clubPlayerBLabel.Name = "clubPlayerBLabel";
            this.clubPlayerBLabel.Size = new System.Drawing.Size(35, 13);
            this.clubPlayerBLabel.TabIndex = 56;
            this.clubPlayerBLabel.Text = "label7";
            // 
            // clubPlayerALabel
            // 
            this.clubPlayerALabel.AutoSize = true;
            this.clubPlayerALabel.Location = new System.Drawing.Point(78, 50);
            this.clubPlayerALabel.Name = "clubPlayerALabel";
            this.clubPlayerALabel.Size = new System.Drawing.Size(35, 13);
            this.clubPlayerALabel.TabIndex = 55;
            this.clubPlayerALabel.Text = "label7";
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(473, 105);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(29, 23);
            this.button5.TabIndex = 54;
            this.button5.Text = "3";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(431, 105);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(29, 23);
            this.button6.TabIndex = 53;
            this.button6.Text = "2";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(388, 105);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(29, 23);
            this.button7.TabIndex = 52;
            this.button7.Text = "1";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(346, 104);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(29, 23);
            this.button8.TabIndex = 51;
            this.button8.Text = "0";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(473, 20);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(29, 23);
            this.button3.TabIndex = 50;
            this.button3.Text = "3";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(431, 20);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(29, 23);
            this.button4.TabIndex = 49;
            this.button4.Text = "2";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(388, 20);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(29, 23);
            this.button2.TabIndex = 48;
            this.button2.Text = "1";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(346, 19);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(29, 23);
            this.button1.TabIndex = 47;
            this.button1.Text = "0";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // setsPlayerB
            // 
            this.setsPlayerB.Location = new System.Drawing.Point(306, 107);
            this.setsPlayerB.Name = "setsPlayerB";
            this.setsPlayerB.Size = new System.Drawing.Size(25, 20);
            this.setsPlayerB.TabIndex = 46;
            // 
            // playerBName
            // 
            this.playerBName.FormattingEnabled = true;
            this.playerBName.Location = new System.Drawing.Point(78, 107);
            this.playerBName.Name = "playerBName";
            this.playerBName.Size = new System.Drawing.Size(215, 21);
            this.playerBName.Sorted = true;
            this.playerBName.TabIndex = 45;
            this.playerBName.SelectedIndexChanged += new System.EventHandler(this.playerBName_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(4, 110);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 13);
            this.label6.TabIndex = 44;
            this.label6.Text = "Player B";
            // 
            // setsPlayerA
            // 
            this.setsPlayerA.Location = new System.Drawing.Point(306, 20);
            this.setsPlayerA.Name = "setsPlayerA";
            this.setsPlayerA.Size = new System.Drawing.Size(25, 20);
            this.setsPlayerA.TabIndex = 43;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(305, 2);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(28, 13);
            this.label5.TabIndex = 42;
            this.label5.Text = "Sets";
            // 
            // playerAName
            // 
            this.playerAName.FormattingEnabled = true;
            this.playerAName.Location = new System.Drawing.Point(78, 20);
            this.playerAName.Name = "playerAName";
            this.playerAName.Size = new System.Drawing.Size(215, 21);
            this.playerAName.Sorted = true;
            this.playerAName.TabIndex = 41;
            this.playerAName.SelectedIndexChanged += new System.EventHandler(this.playerAName_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(4, 23);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 13);
            this.label4.TabIndex = 40;
            this.label4.Text = "Player A";
            // 
            // gameInsertedLabel
            // 
            this.gameInsertedLabel.AutoSize = true;
            this.gameInsertedLabel.Location = new System.Drawing.Point(36, 82);
            this.gameInsertedLabel.Name = "gameInsertedLabel";
            this.gameInsertedLabel.Size = new System.Drawing.Size(0, 13);
            this.gameInsertedLabel.TabIndex = 64;
            // 
            // SinglesGame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gameInsertedLabel);
            this.Controls.Add(this.licPlayerB);
            this.Controls.Add(this.licPlayerA);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.pointsPlayerBLabel);
            this.Controls.Add(this.rankingPlayerBLabel);
            this.Controls.Add(this.pointsPlayerALabel);
            this.Controls.Add(this.rankingPlayerALabel);
            this.Controls.Add(this.clubPlayerBLabel);
            this.Controls.Add(this.clubPlayerALabel);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.setsPlayerB);
            this.Controls.Add(this.playerBName);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.setsPlayerA);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.playerAName);
            this.Controls.Add(this.label4);
            this.Name = "SinglesGame";
            this.Size = new System.Drawing.Size(539, 156);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label licPlayerB;
        private System.Windows.Forms.Label licPlayerA;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Label pointsPlayerBLabel;
        private System.Windows.Forms.Label rankingPlayerBLabel;
        private System.Windows.Forms.Label pointsPlayerALabel;
        private System.Windows.Forms.Label rankingPlayerALabel;
        private System.Windows.Forms.Label clubPlayerBLabel;
        private System.Windows.Forms.Label clubPlayerALabel;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox setsPlayerB;
        private System.Windows.Forms.ComboBox playerBName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox setsPlayerA;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox playerAName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label gameInsertedLabel;
    }
}
