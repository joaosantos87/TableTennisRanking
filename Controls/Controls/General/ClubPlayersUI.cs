﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TableTennis.Database;
using static TableTennis.Database.DataSet2;

namespace TableTennis.Controls
{
    public partial class ClubPlayersUI : UserControl
    {
        public ClubPlayersUI()
        {
            InitializeComponent();
            VisibleChanged += ClubPlayersUI_VisibleChanged;
            clubCombobox.SelectedIndexChanged += clubCombobox_SelectedIndexChanged;
            dataGridView1.KeyDown += dataGridView1_KeyPress;
        }

        private void dataGridView1_KeyPress(object sender, KeyEventArgs e)
        {
            DataGridView a = sender as DataGridView;
            switch (e.KeyCode)
            {
                case Keys.Delete:
                    int idPlayer = Convert.ToInt32(a["Id",a.CurrentCell.RowIndex].Value);
                    playerRow pl = FilesAccess.db.player.Single(x=>x.Id == idPlayer);
                    DialogResult dr = MessageBox.Show("Deseja apagar o jogador " + pl.Id, "Apagar", MessageBoxButtons.YesNo);
                    if (dr == DialogResult.No)
                        return;

                    pl.Delete();
                    FilesAccess.db.UpdatePlayer();
                    clubCombobox_SelectedIndexChanged(this, new EventArgs());
                    break;
            }
        }

        private void clubCombobox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (clubCombobox.Text == null)
                return;
            int idClub = FilesAccess.db.clubs.Single(x => x.Name == clubCombobox.Text).Id;
            dataGridView1.DataSource = (((playerRow[])FilesAccess.db.player.Select("IdClub=" + idClub)).OrderByDescending(x => x.currentPoints)).ToList();
        }

        private void ClubPlayersUI_VisibleChanged(object sender, EventArgs e)
        {
            clubCombobox.Items.Clear();
            for (int i = 0; i < FilesAccess.db.clubs.Count; i++)
                clubCombobox.Items.Add(FilesAccess.db.clubs[i].Name);

            if (clubCombobox.Items.Count > 0)
                clubCombobox.SelectedIndex = 0;
        }
    }
}
