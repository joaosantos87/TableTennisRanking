﻿namespace TableTennis.Controls
{
    partial class AddGame
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.createGameButton = new System.Windows.Forms.Button();
            this.champCombobox = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.gameDate = new System.Windows.Forms.DateTimePicker();
            this.gameInsertedLabel = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.phaseCombobox = new System.Windows.Forms.ComboBox();
            this.teamGameCombobox = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.button10 = new System.Windows.Forms.Button();
            this.singlesMatchRadiobutton = new System.Windows.Forms.RadioButton();
            this.doublesGameRadiobutton = new System.Windows.Forms.RadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lusitanoRadiobutton = new System.Windows.Forms.RadioButton();
            this.button1 = new System.Windows.Forms.Button();
            this.filterChamp = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.fillGamesBtn = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.teamGameIdTxtbox = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(259, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Add Game";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 96);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Championship";
            // 
            // createGameButton
            // 
            this.createGameButton.Location = new System.Drawing.Point(260, 374);
            this.createGameButton.Name = "createGameButton";
            this.createGameButton.Size = new System.Drawing.Size(75, 23);
            this.createGameButton.TabIndex = 9;
            this.createGameButton.Text = "Create";
            this.createGameButton.UseVisualStyleBackColor = true;
            this.createGameButton.Click += new System.EventHandler(this.createGameButton_Click);
            // 
            // champCombobox
            // 
            this.champCombobox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.champCombobox.FormattingEnabled = true;
            this.champCombobox.Location = new System.Drawing.Point(101, 93);
            this.champCombobox.Name = "champCombobox";
            this.champCombobox.Size = new System.Drawing.Size(388, 21);
            this.champCombobox.TabIndex = 10;
            this.champCombobox.SelectedIndexChanged += new System.EventHandler(this.champCombobox_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(23, 122);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(30, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Date";
            // 
            // gameDate
            // 
            this.gameDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gameDate.Location = new System.Drawing.Point(101, 118);
            this.gameDate.Name = "gameDate";
            this.gameDate.Size = new System.Drawing.Size(388, 20);
            this.gameDate.TabIndex = 12;
            // 
            // gameInsertedLabel
            // 
            this.gameInsertedLabel.AutoSize = true;
            this.gameInsertedLabel.Location = new System.Drawing.Point(73, 369);
            this.gameInsertedLabel.Name = "gameInsertedLabel";
            this.gameInsertedLabel.Size = new System.Drawing.Size(0, 13);
            this.gameInsertedLabel.TabIndex = 20;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(23, 149);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(37, 13);
            this.label7.TabIndex = 36;
            this.label7.Text = "Phase";
            // 
            // phaseCombobox
            // 
            this.phaseCombobox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.phaseCombobox.FormattingEnabled = true;
            this.phaseCombobox.Location = new System.Drawing.Point(101, 145);
            this.phaseCombobox.Name = "phaseCombobox";
            this.phaseCombobox.Size = new System.Drawing.Size(215, 21);
            this.phaseCombobox.Sorted = true;
            this.phaseCombobox.TabIndex = 37;
            this.phaseCombobox.SelectedIndexChanged += new System.EventHandler(this.phaseCombobox_SelectedIndexChanged);
            // 
            // teamGameCombobox
            // 
            this.teamGameCombobox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.teamGameCombobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.teamGameCombobox.FormattingEnabled = true;
            this.teamGameCombobox.Location = new System.Drawing.Point(101, 46);
            this.teamGameCombobox.Name = "teamGameCombobox";
            this.teamGameCombobox.Size = new System.Drawing.Size(340, 21);
            this.teamGameCombobox.TabIndex = 41;
            this.teamGameCombobox.SelectedIndexChanged += new System.EventHandler(this.teamGameCombobox_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(20, 49);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(65, 13);
            this.label8.TabIndex = 40;
            this.label8.Text = "Team Game";
            // 
            // button10
            // 
            this.button10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button10.Location = new System.Drawing.Point(457, 46);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(101, 23);
            this.button10.TabIndex = 42;
            this.button10.Text = "Add Team Game";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // singlesMatchRadiobutton
            // 
            this.singlesMatchRadiobutton.AutoSize = true;
            this.singlesMatchRadiobutton.Location = new System.Drawing.Point(67, 168);
            this.singlesMatchRadiobutton.Name = "singlesMatchRadiobutton";
            this.singlesMatchRadiobutton.Size = new System.Drawing.Size(90, 17);
            this.singlesMatchRadiobutton.TabIndex = 43;
            this.singlesMatchRadiobutton.TabStop = true;
            this.singlesMatchRadiobutton.Text = "Singles Game";
            this.singlesMatchRadiobutton.UseVisualStyleBackColor = true;
            this.singlesMatchRadiobutton.CheckedChanged += new System.EventHandler(this.singlesMatchRadiobutton_CheckedChanged);
            // 
            // doublesGameRadiobutton
            // 
            this.doublesGameRadiobutton.AutoSize = true;
            this.doublesGameRadiobutton.Location = new System.Drawing.Point(167, 168);
            this.doublesGameRadiobutton.Name = "doublesGameRadiobutton";
            this.doublesGameRadiobutton.Size = new System.Drawing.Size(95, 17);
            this.doublesGameRadiobutton.TabIndex = 45;
            this.doublesGameRadiobutton.TabStop = true;
            this.doublesGameRadiobutton.Text = "Doubles Game";
            this.doublesGameRadiobutton.UseVisualStyleBackColor = true;
            this.doublesGameRadiobutton.CheckedChanged += new System.EventHandler(this.doublesGameRadiobutton_CheckedChanged);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(21, 187);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(536, 179);
            this.panel1.TabIndex = 46;
            // 
            // lusitanoRadiobutton
            // 
            this.lusitanoRadiobutton.AutoSize = true;
            this.lusitanoRadiobutton.Location = new System.Drawing.Point(280, 168);
            this.lusitanoRadiobutton.Name = "lusitanoRadiobutton";
            this.lusitanoRadiobutton.Size = new System.Drawing.Size(65, 17);
            this.lusitanoRadiobutton.TabIndex = 47;
            this.lusitanoRadiobutton.TabStop = true;
            this.lusitanoRadiobutton.Text = "Lusitano";
            this.lusitanoRadiobutton.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(497, 88);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(60, 50);
            this.button1.TabIndex = 48;
            this.button1.Text = "Update";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // filterChamp
            // 
            this.filterChamp.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.filterChamp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.filterChamp.FormattingEnabled = true;
            this.filterChamp.Location = new System.Drawing.Point(101, 21);
            this.filterChamp.Name = "filterChamp";
            this.filterChamp.Size = new System.Drawing.Size(340, 21);
            this.filterChamp.TabIndex = 49;
            this.filterChamp.SelectedIndexChanged += new System.EventHandler(this.filterChamp_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(20, 24);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 13);
            this.label4.TabIndex = 50;
            this.label4.Text = "Filter Champ";
            // 
            // fillGamesBtn
            // 
            this.fillGamesBtn.Location = new System.Drawing.Point(20, 372);
            this.fillGamesBtn.Name = "fillGamesBtn";
            this.fillGamesBtn.Size = new System.Drawing.Size(75, 23);
            this.fillGamesBtn.TabIndex = 51;
            this.fillGamesBtn.Text = "Fill Games";
            this.fillGamesBtn.UseVisualStyleBackColor = true;
            this.fillGamesBtn.Click += new System.EventHandler(this.fillGamesBtn_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(20, 73);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(79, 13);
            this.label5.TabIndex = 52;
            this.label5.Text = "Team Game ID";
            // 
            // teamGameIdTxtbox
            // 
            this.teamGameIdTxtbox.Location = new System.Drawing.Point(101, 70);
            this.teamGameIdTxtbox.Name = "teamGameIdTxtbox";
            this.teamGameIdTxtbox.Size = new System.Drawing.Size(112, 20);
            this.teamGameIdTxtbox.TabIndex = 53;
            this.teamGameIdTxtbox.TextChanged += new System.EventHandler(this.teamGameIdTxtbox_TextChanged);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(219, 70);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(97, 23);
            this.button2.TabIndex = 54;
            this.button2.Text = "select game id";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // AddGame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.button2);
            this.Controls.Add(this.teamGameIdTxtbox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.fillGamesBtn);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.filterChamp);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.lusitanoRadiobutton);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.doublesGameRadiobutton);
            this.Controls.Add(this.singlesMatchRadiobutton);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.teamGameCombobox);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.phaseCombobox);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.gameInsertedLabel);
            this.Controls.Add(this.gameDate);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.champCombobox);
            this.Controls.Add(this.createGameButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "AddGame";
            this.Size = new System.Drawing.Size(575, 402);
            this.Load += new System.EventHandler(this.AddGame_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.Button createGameButton;
        private System.Windows.Forms.ComboBox champCombobox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker gameDate;
        private System.Windows.Forms.Label gameInsertedLabel;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox phaseCombobox;
        private System.Windows.Forms.ComboBox teamGameCombobox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.RadioButton singlesMatchRadiobutton;
        private System.Windows.Forms.RadioButton doublesGameRadiobutton;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton lusitanoRadiobutton;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox filterChamp;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.Button fillGamesBtn;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox teamGameIdTxtbox;
        private System.Windows.Forms.Button button2;
    }
}
