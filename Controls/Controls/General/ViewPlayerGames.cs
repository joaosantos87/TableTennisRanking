﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TableTennis.Database;
using static TableTennis.Database.DataSet2;

namespace TableTennis.Controls
{
    public partial class EditGames : UserControl
    {
        public EditGames()
        {
            InitializeComponent();
            VisibleChanged += ViewGames_VisibleChanged;
            dataGridView1.RowHeaderMouseClick += dataGridView1_RowHeaderMouseClick;
        }

        private void dataGridView1_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            //if (e.Button == System.Windows.Forms.MouseButtons.Right)
            //{
            //    DialogResult dr = MessageBox.Show("Are you sure you want to delete row " + dataGridView1.CurrentRow + "?","Information",MessageBoxButtons.YesNo);
            //    if (dr == DialogResult.Yes)
            //    {
            //        DataSet1.gamesRow rowDelete = FilesAccess.db.games.Single(x => x.id == Convert.ToInt32(dataGridView1["id", dataGridView1.CurrentCell.RowIndex].Value));
            //        FilesAccess.db.games.RemovegamesRow(rowDelete);
            //        FilesAccess.db.UpdateGames();
            //        int idPlayer = FilesAccess.db.player.Single(x => x.Name == comboBox1.Text).Id;
            //        dataGridView1.DataSource = ((DataSet1.gamesRow[])(FilesAccess.db.games.Select("IdPlayerA='" + idPlayer + "' OR IdPlayerB='" + idPlayer + "'"))).OrderByDescending(x => x.Date).ToList();
            //    }
            //}
        }

        private void ViewGames_VisibleChanged(object sender, EventArgs e)
        {
            comboBox1.Items.Clear();
            for (int i = 0; i < FilesAccess.db.player.Count; i++)
            {
                comboBox1.Items.Add(FilesAccess.db.player[i].Name);
            }
            comboBox1.Sorted = true;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            int idPlayer = FilesAccess.db.player.Single(x => x.Name == comboBox1.Text).Id;
            dataGridView1.DataSource = ((gamesRow[])(FilesAccess.db.games.Select("IdPlayerA='" + idPlayer + "' OR IdPlayerB='" + idPlayer + "'"))).OrderByDescending(x => x.Date).ToList();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            dataGridView1.EndEdit();
            FilesAccess.db.UpdateGames();
        }
    }
}
