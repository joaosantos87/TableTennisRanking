﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using static TableTennis.Database.DataSet2;
using TableTennis.Database;

namespace TableTennis.Controls.General
{
    public partial class ViewDoublesGame : UserControl, IAddGameToTeamgame
    {
        private doublegamesRow currentGame;

        public ViewDoublesGame(int idChamp)
        {
            InitializeComponent();
            UpdateList(idChamp);
        }

        public void UpdateList(int idChamp)
        {
            if (idChamp != 0)
            {
                var data =
                    from g in FilesAccess.db.doubleGames.Where(x => x.IdTeamGame == 0)
                    where g.IdChampionship == idChamp && g.DoubleA_PlayerAId != 0 && g.DoubleA_PlayerBId != 0 && g.DoubleB_PlayerAId != 0 && g.DoubleB_PlayerBId != 0
                    select new
                    {
                        output = g.Id + "|" + g.Date + " " +
                            FilesAccess.db.player.SingleOrDefault(x => x.Id == g.DoubleA_PlayerAId)?.Name + "/"+
                            FilesAccess.db.player.SingleOrDefault(x => x.Id == g.DoubleA_PlayerBId)?.Name +
                            " " + g.SetsA + " - " + g.SetsB + " " +
                            FilesAccess.db.player.SingleOrDefault(x => x.Id == g.DoubleB_PlayerAId)?.Name + "/" +
                            FilesAccess.db.player.SingleOrDefault(x => x.Id == g.DoubleB_PlayerBId)?.Name
                    };

                comboBox1.Items.Clear();
                comboBox1.Items.AddRange(data.Select(x => x.output).ToArray());
            }
        }


        public void UpdateGame(int idTeamGame)
        {
            currentGame.IdTeamGame = idTeamGame;
            FilesAccess.db.UpdateDoubleGames();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            var gameId = comboBox1.Text.Split('|')[0];

            currentGame = FilesAccess.db.doubleGames.SingleOrDefault(x => x.Id == Convert.ToInt32(gameId));

            var doubleAPlayerA = FilesAccess.db.player.SingleOrDefault(x => x.Id == currentGame.DoubleA_PlayerAId);
            var doubleAPlayerB = FilesAccess.db.player.SingleOrDefault(x => x.Id == currentGame.DoubleA_PlayerBId);
            var doubleBPlayerA = FilesAccess.db.player.SingleOrDefault(x => x.Id == currentGame.DoubleB_PlayerAId);
            var doubleBPlayerB = FilesAccess.db.player.SingleOrDefault(x => x.Id == currentGame.DoubleB_PlayerBId);

            doubleAPlayerANameTxt.Text = doubleAPlayerA.Name;
            doubleAPlayerBNameTxt.Text = doubleAPlayerB.Name;
            doubleBPlayerANameTxt.Text = doubleBPlayerA.Name;
            doubleBPlayerBNameTxt.Text = doubleBPlayerB.Name;

            clubDoubleAPlayerA.Text = FilesAccess.db.clubs.Single(x => x.Id == doubleAPlayerA.IdClub).Name;
            clubDoubleAPlayerB.Text = FilesAccess.db.clubs.Single(x => x.Id == doubleAPlayerB.IdClub).Name;
            clubDoubleBPlayerA.Text = FilesAccess.db.clubs.Single(x => x.Id == doubleBPlayerA.IdClub).Name;
            clubDoubleBPlayerB.Text = FilesAccess.db.clubs.Single(x => x.Id == doubleBPlayerB.IdClub).Name;

            pointsDoubleAPlayerA.Text = doubleAPlayerA.currentPoints.ToString();
            pointsDoubleAPlayerB.Text = doubleAPlayerB.currentPoints.ToString();
            pointsDoubleBPlayerA.Text = doubleBPlayerA.currentPoints.ToString();
            pointsDoubleBPlayerB.Text = doubleBPlayerB.currentPoints.ToString();

            rankingDoubleAPlayerA.Text = doubleAPlayerA.currentRank.ToString();
            rankingDoubleAPlayerB.Text = doubleAPlayerB.currentRank.ToString();
            rankingDoubleBPlayerA.Text = doubleBPlayerA.currentRank.ToString();
            rankingDoubleBPlayerB.Text = doubleBPlayerB.currentRank.ToString();

            licDoubleAPlayerA.Text = doubleAPlayerA.licenseNo.ToString();
            licDoubleAPlayerB.Text = doubleAPlayerB.licenseNo.ToString();
            licDoubleBPlayerA.Text = doubleBPlayerA.licenseNo.ToString();
            licDoubleBPlayerB.Text = doubleBPlayerB.licenseNo.ToString();
            
            setDoubleA.Text = currentGame.SetsA.ToString();

            setDoubleB.Text = currentGame.SetsB.ToString();
        }
    }
}
