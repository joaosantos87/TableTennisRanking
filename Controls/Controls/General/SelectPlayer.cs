﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TableTennis.Database;

namespace TableTennis.Controls
{
    public partial class SelectPlayer : UserControl
    {
        public List<DataSet2.playerRow> selectedPlayers = new List<DataSet2.playerRow>();
        public SelectPlayer()
        {
            InitializeComponent();
            VisibleChanged += SelectPlayer_VisibleChanged;
            dataGridView1.CellContentClick += dataGridView1_CellContentClick;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;
            senderGrid.EndEdit();

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn &&
                e.RowIndex >= 0)
            {

            }
            else if(senderGrid.Columns[e.ColumnIndex] is DataGridViewCheckBoxColumn &&
                e.RowIndex >= 0)
            {
                var columnNome = senderGrid.Columns["Nome"].Index;
                var player = FilesAccess.db.player.Single(x => x.Name == senderGrid[columnNome, e.RowIndex].Value);

                if (Convert.ToBoolean(senderGrid[e.ColumnIndex,e.RowIndex].Value) == true)
                {
                    if (!selectedPlayers.Exists(x => x.Name == player.Name))
                        selectedPlayers.Add(player);
                }
                else
                {
                    if (selectedPlayers.Exists(x => x.Name == player.Name))
                        selectedPlayers.Remove(player);
                }
            }
        }

        private void SelectPlayer_VisibleChanged(object sender, EventArgs e)
        {
            var columnSeleccionar = dataGridView1.Columns["Seleccionar"].Index;
            var filtername = playernameTxtbox.Text.ToLower();
            var players =
                (from p in FilesAccess.db.player
                where filtername != "ALL" ? //Utilities.LevenshteinDistance(p.Name.ToLower(), filtername.ToLower()) == 1
                
                (p.Name.ToLower().Contains(filtername) || p.nameAutomaticInsertion.ToLower().Contains(filtername) || p.licenseNo.Contains(filtername))

                : true
                select new
                {
                    Licenca = p.licenseNo,
                    Nome = p.Name,
                    Clube = FilesAccess.db.clubs.Single(x => x.Id == p.IdClub).Name,
                    Jogos = $"{p.currentGamesWon}-{p.currentGamesLost}",
                    Ranking = p.currentRank,
                    Pontos = p.currentPoints,
                }).ToList();

            dataGridView1.DataSource = players;
            var columnNome = dataGridView1.Columns["Nome"].Index;
            for (int i=0; i< players.Count; i++)
            {
                var selectedPlayer = selectedPlayers.SingleOrDefault(x => x.Name == players[i].Nome);
                if (selectedPlayer == null)
                    continue;

                dataGridView1[columnSeleccionar, i].Value = true;
            }
            dataGridView1.Columns["Seleccionar"].DisplayIndex = dataGridView1.Columns.Count - 1;
        }

        private void playernameTxtbox_TextChanged(object sender, EventArgs e)
        {
            SelectPlayer_VisibleChanged(this, new EventArgs());
        }

        private void button1_Click(object sender, EventArgs e)
        {

            if (this.Parent.GetType().BaseType.Name == typeof(Form).Name)
            {
                (Parent as Form).Close();
            }
        }
    }
}
