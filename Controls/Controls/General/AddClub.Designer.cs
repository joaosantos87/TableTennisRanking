﻿namespace TableTennis.Controls
{
    partial class AddClub
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.nameTextbox = new System.Windows.Forms.TextBox();
            this.createClubButton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.photoTextbox = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.associationChoose1 = new TableTennis.Controls.AssociationChoose();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(266, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Add Club";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 79);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Name";
            // 
            // nameTextbox
            // 
            this.nameTextbox.Location = new System.Drawing.Point(95, 76);
            this.nameTextbox.Name = "nameTextbox";
            this.nameTextbox.Size = new System.Drawing.Size(418, 20);
            this.nameTextbox.TabIndex = 2;
            // 
            // createClubButton
            // 
            this.createClubButton.Location = new System.Drawing.Point(258, 203);
            this.createClubButton.Name = "createClubButton";
            this.createClubButton.Size = new System.Drawing.Size(75, 23);
            this.createClubButton.TabIndex = 9;
            this.createClubButton.Text = "Create";
            this.createClubButton.UseVisualStyleBackColor = true;
            this.createClubButton.Click += new System.EventHandler(this.createClubButton_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(22, 138);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Photo";
            // 
            // photoTextbox
            // 
            this.photoTextbox.Location = new System.Drawing.Point(95, 135);
            this.photoTextbox.Name = "photoTextbox";
            this.photoTextbox.Size = new System.Drawing.Size(418, 20);
            this.photoTextbox.TabIndex = 11;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(519, 132);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(38, 23);
            this.button1.TabIndex = 12;
            this.button1.Text = "...";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // associationChoose1
            // 
            this.associationChoose1.Location = new System.Drawing.Point(17, 161);
            this.associationChoose1.Name = "associationChoose1";
            this.associationChoose1.Size = new System.Drawing.Size(544, 25);
            this.associationChoose1.TabIndex = 13;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(22, 109);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 13);
            this.label4.TabIndex = 14;
            this.label4.Text = "Shortname";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(95, 106);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(57, 20);
            this.textBox1.TabIndex = 15;
            // 
            // AddClub
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.associationChoose1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.photoTextbox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.createClubButton);
            this.Controls.Add(this.nameTextbox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "AddClub";
            this.Size = new System.Drawing.Size(575, 402);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox nameTextbox;
        public System.Windows.Forms.Button createClubButton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox photoTextbox;
        public System.Windows.Forms.Button button1;
        private AssociationChoose associationChoose1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox1;
    }
}
