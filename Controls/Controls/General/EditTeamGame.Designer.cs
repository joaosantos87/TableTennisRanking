﻿namespace TableTennis.Controls.General
{
    partial class EditTeamGame
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.startDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.champCombobox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.jornadaTextbox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.selectTeamGameBtn = new System.Windows.Forms.Button();
            this.gamesDataGridView = new System.Windows.Forms.DataGridView();
            this.button10 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.clubBappendTextbox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.clubAappendTextbox = new System.Windows.Forms.TextBox();
            this.clubBCombobox = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.clubACombobox = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.endDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.teamGamesCbbox = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.gameDate = new System.Windows.Forms.DateTimePicker();
            this.label13 = new System.Windows.Forms.Label();
            this.updateGameBtn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.gamesDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // startDateTimePicker
            // 
            this.startDateTimePicker.Location = new System.Drawing.Point(105, 33);
            this.startDateTimePicker.Name = "startDateTimePicker";
            this.startDateTimePicker.Size = new System.Drawing.Size(212, 20);
            this.startDateTimePicker.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(27, 37);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "Dates";
            // 
            // champCombobox
            // 
            this.champCombobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.champCombobox.FormattingEnabled = true;
            this.champCombobox.Location = new System.Drawing.Point(105, 59);
            this.champCombobox.Name = "champCombobox";
            this.champCombobox.Size = new System.Drawing.Size(456, 21);
            this.champCombobox.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(26, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "Championship";
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(575, 26);
            this.label1.TabIndex = 17;
            this.label1.Text = "Search Team Game to Edit";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(26, 89);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(45, 13);
            this.label8.TabIndex = 44;
            this.label8.Text = "Jornada";
            // 
            // jornadaTextbox
            // 
            this.jornadaTextbox.Location = new System.Drawing.Point(104, 86);
            this.jornadaTextbox.Name = "jornadaTextbox";
            this.jornadaTextbox.Size = new System.Drawing.Size(25, 20);
            this.jornadaTextbox.TabIndex = 3;
            this.jornadaTextbox.Text = "0";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(102, 360);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 13);
            this.label6.TabIndex = 51;
            this.label6.Text = "label6";
            // 
            // selectTeamGameBtn
            // 
            this.selectTeamGameBtn.Location = new System.Drawing.Point(242, 94);
            this.selectTeamGameBtn.Name = "selectTeamGameBtn";
            this.selectTeamGameBtn.Size = new System.Drawing.Size(75, 23);
            this.selectTeamGameBtn.TabIndex = 58;
            this.selectTeamGameBtn.Text = "Search";
            this.selectTeamGameBtn.UseVisualStyleBackColor = true;
            this.selectTeamGameBtn.Click += new System.EventHandler(this.selectTeamGameBtn_Click);
            // 
            // gamesDataGridView
            // 
            this.gamesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gamesDataGridView.Location = new System.Drawing.Point(20, 310);
            this.gamesDataGridView.Name = "gamesDataGridView";
            this.gamesDataGridView.Size = new System.Drawing.Size(529, 79);
            this.gamesDataGridView.TabIndex = 61;
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(255, 272);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(29, 23);
            this.button10.TabIndex = 71;
            this.button10.Text = "A";
            this.button10.UseVisualStyleBackColor = true;
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(255, 216);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(29, 23);
            this.button9.TabIndex = 64;
            this.button9.Text = "A";
            this.button9.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(392, 274);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(29, 23);
            this.button5.TabIndex = 75;
            this.button5.Text = "E";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(357, 274);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(29, 23);
            this.button6.TabIndex = 74;
            this.button6.Text = "D";
            this.button6.UseVisualStyleBackColor = true;
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(322, 272);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(29, 24);
            this.button7.TabIndex = 73;
            this.button7.Text = "C";
            this.button7.UseVisualStyleBackColor = true;
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(288, 272);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(29, 23);
            this.button8.TabIndex = 72;
            this.button8.Text = "B";
            this.button8.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(426, 279);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(102, 13);
            this.label11.TabIndex = 81;
            this.label11.Text = "Example: \"B\" or \"C\"";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(392, 216);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(29, 23);
            this.button3.TabIndex = 68;
            this.button3.Text = "E";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(357, 216);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(29, 23);
            this.button4.TabIndex = 67;
            this.button4.Text = "D";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(322, 215);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(29, 24);
            this.button2.TabIndex = 66;
            this.button2.Text = "C";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(288, 215);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(29, 24);
            this.button1.TabIndex = 65;
            this.button1.Text = "B";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(426, 221);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(102, 13);
            this.label10.TabIndex = 80;
            this.label10.Text = "Example: \"B\" or \"C\"";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(115, 275);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(77, 13);
            this.label9.TabIndex = 79;
            this.label9.Text = "Append club B";
            // 
            // clubBappendTextbox
            // 
            this.clubBappendTextbox.Location = new System.Drawing.Point(200, 272);
            this.clubBappendTextbox.Name = "clubBappendTextbox";
            this.clubBappendTextbox.Size = new System.Drawing.Size(49, 20);
            this.clubBappendTextbox.TabIndex = 70;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(119, 220);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 13);
            this.label7.TabIndex = 78;
            this.label7.Text = "Append club A";
            // 
            // clubAappendTextbox
            // 
            this.clubAappendTextbox.Location = new System.Drawing.Point(200, 216);
            this.clubAappendTextbox.Name = "clubAappendTextbox";
            this.clubAappendTextbox.Size = new System.Drawing.Size(49, 20);
            this.clubAappendTextbox.TabIndex = 63;
            // 
            // clubBCombobox
            // 
            this.clubBCombobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.clubBCombobox.FormattingEnabled = true;
            this.clubBCombobox.Location = new System.Drawing.Point(98, 246);
            this.clubBCombobox.Name = "clubBCombobox";
            this.clubBCombobox.Size = new System.Drawing.Size(258, 21);
            this.clubBCombobox.TabIndex = 69;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(19, 249);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(38, 13);
            this.label5.TabIndex = 77;
            this.label5.Text = "Club B";
            // 
            // clubACombobox
            // 
            this.clubACombobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.clubACombobox.FormattingEnabled = true;
            this.clubACombobox.Location = new System.Drawing.Point(98, 189);
            this.clubACombobox.Name = "clubACombobox";
            this.clubACombobox.Size = new System.Drawing.Size(258, 21);
            this.clubACombobox.TabIndex = 62;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(20, 194);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 13);
            this.label4.TabIndex = 76;
            this.label4.Text = "Club A";
            // 
            // endDateTimePicker
            // 
            this.endDateTimePicker.Location = new System.Drawing.Point(349, 33);
            this.endDateTimePicker.Name = "endDateTimePicker";
            this.endDateTimePicker.Size = new System.Drawing.Size(212, 20);
            this.endDateTimePicker.TabIndex = 82;
            // 
            // teamGamesCbbox
            // 
            this.teamGamesCbbox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.teamGamesCbbox.FormattingEnabled = true;
            this.teamGamesCbbox.Location = new System.Drawing.Point(104, 127);
            this.teamGamesCbbox.Name = "teamGamesCbbox";
            this.teamGamesCbbox.Size = new System.Drawing.Size(457, 21);
            this.teamGamesCbbox.TabIndex = 83;
            this.teamGamesCbbox.SelectedIndexChanged += new System.EventHandler(this.teamGamesCbbox_SelectedIndexChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(25, 130);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(70, 13);
            this.label12.TabIndex = 84;
            this.label12.Text = "Team Games";
            // 
            // gameDate
            // 
            this.gameDate.Location = new System.Drawing.Point(98, 163);
            this.gameDate.Name = "gameDate";
            this.gameDate.Size = new System.Drawing.Size(359, 20);
            this.gameDate.TabIndex = 85;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(20, 167);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(61, 13);
            this.label13.TabIndex = 86;
            this.label13.Text = "Game Date";
            // 
            // updateGameBtn
            // 
            this.updateGameBtn.Location = new System.Drawing.Point(486, 163);
            this.updateGameBtn.Name = "updateGameBtn";
            this.updateGameBtn.Size = new System.Drawing.Size(75, 23);
            this.updateGameBtn.TabIndex = 87;
            this.updateGameBtn.Text = "Update";
            this.updateGameBtn.UseVisualStyleBackColor = true;
            this.updateGameBtn.Click += new System.EventHandler(this.updateGameBtn_Click);
            // 
            // EditTeamGame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.updateGameBtn);
            this.Controls.Add(this.gameDate);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.teamGamesCbbox);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.endDateTimePicker);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.clubBappendTextbox);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.clubAappendTextbox);
            this.Controls.Add(this.clubBCombobox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.clubACombobox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.gamesDataGridView);
            this.Controls.Add(this.selectTeamGameBtn);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.jornadaTextbox);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.startDateTimePicker);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.champCombobox);
            this.Controls.Add(this.label2);
            this.Name = "EditTeamGame";
            this.Size = new System.Drawing.Size(575, 402);
            ((System.ComponentModel.ISupportInitialize)(this.gamesDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker startDateTimePicker;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox champCombobox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox jornadaTextbox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button selectTeamGameBtn;
        private System.Windows.Forms.DataGridView gamesDataGridView;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox clubBappendTextbox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox clubAappendTextbox;
        private System.Windows.Forms.ComboBox clubBCombobox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox clubACombobox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker endDateTimePicker;
        private System.Windows.Forms.ComboBox teamGamesCbbox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.DateTimePicker gameDate;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button updateGameBtn;
    }
}
