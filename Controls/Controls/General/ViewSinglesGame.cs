﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using static TableTennis.Database.DataSet2;
using TableTennis.Database;
using TableTennis.Database.DataSet2TableAdapters;

namespace TableTennis.Controls.General
{
    public partial class ViewSinglesGame : UserControl, IAddGameToTeamgame
    {
        public ViewSinglesGame(int idChamp)
        {
            InitializeComponent();
            UpdateList(idChamp, DateTime.Now);
        }

        public void UpdateList(int idChamp, DateTime date)
        {
            if (idChamp != 0)
            {
                var data =
                    from g in FilesAccess.db.games.Where(x => x.IdTeamGame == 0)
                    where (g.IdTeamGame==0 || g.IdTeamGame==-1) && g.IdChamp == idChamp && g.IdPlayerA != 0 && g.IdPlayerB != 0 && g.Date.ToShortDateString().Equals(date.ToShortDateString())
                    select new
                    {
                        output = g.id + "|" + g.Date + " " +
                            FilesAccess.db.player.SingleOrDefault(x => x.Id == g.IdPlayerA)?.Name + " " + g.SetsA + " - " + g.SetsB + " " +
                            FilesAccess.db.player.SingleOrDefault(x => x.Id == g.IdPlayerB)?.Name
                    };

                listBox1.Items.Clear();
                listBox1.Items.AddRange(data.Select(x => x.output).ToArray());
            }
        }
        
        public void UpdateGame(int idTeamGame)
        {
            //gamesDataTable games = new gamesDataTable();
            foreach(var g in listBox1.SelectedItems)
            {
                var gmId = Convert.ToInt32(g.ToString().Split('|')[0]);
                var currentGame = FilesAccess.db.games.Single(x => x.id == gmId);
                currentGame.IdTeamGame = idTeamGame;

                //games.AddgamesRow(currentGame);
            }
            new gamesTableAdapter().Update(FilesAccess.db.games);
            new gamesTableAdapter().Fill(FilesAccess.db.games);
        }

    }
}
