﻿namespace TableTennis.Controls
{
    partial class CombinePlayers
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.playerAcbbox = new System.Windows.Forms.ComboBox();
            this.playerATextbox = new System.Windows.Forms.TextBox();
            this.playerBcbbox = new System.Windows.Forms.ComboBox();
            this.playerBTextbox = new System.Windows.Forms.TextBox();
            this.licenceTxtbox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(231, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Combine Players";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(486, 57);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "Combine";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // playerAcbbox
            // 
            this.playerAcbbox.FormattingEnabled = true;
            this.playerAcbbox.Location = new System.Drawing.Point(22, 57);
            this.playerAcbbox.Name = "playerAcbbox";
            this.playerAcbbox.Size = new System.Drawing.Size(216, 21);
            this.playerAcbbox.TabIndex = 7;
            this.playerAcbbox.SelectedIndexChanged += new System.EventHandler(this.playerA_SelectedIndexChanged);
            // 
            // playerATextbox
            // 
            this.playerATextbox.Location = new System.Drawing.Point(22, 119);
            this.playerATextbox.Multiline = true;
            this.playerATextbox.Name = "playerATextbox";
            this.playerATextbox.Size = new System.Drawing.Size(216, 129);
            this.playerATextbox.TabIndex = 8;
            // 
            // playerBcbbox
            // 
            this.playerBcbbox.FormattingEnabled = true;
            this.playerBcbbox.Location = new System.Drawing.Point(252, 56);
            this.playerBcbbox.Name = "playerBcbbox";
            this.playerBcbbox.Size = new System.Drawing.Size(216, 21);
            this.playerBcbbox.TabIndex = 9;
            this.playerBcbbox.SelectedIndexChanged += new System.EventHandler(this.playerBcbbox_SelectedIndexChanged);
            // 
            // playerBTextbox
            // 
            this.playerBTextbox.Location = new System.Drawing.Point(252, 119);
            this.playerBTextbox.Multiline = true;
            this.playerBTextbox.Name = "playerBTextbox";
            this.playerBTextbox.Size = new System.Drawing.Size(216, 129);
            this.playerBTextbox.TabIndex = 10;
            // 
            // licenceTxtbox
            // 
            this.licenceTxtbox.Location = new System.Drawing.Point(70, 93);
            this.licenceTxtbox.Name = "licenceTxtbox";
            this.licenceTxtbox.Size = new System.Drawing.Size(71, 20);
            this.licenceTxtbox.TabIndex = 11;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 96);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Licence";
            // 
            // CombinePlayers
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label2);
            this.Controls.Add(this.licenceTxtbox);
            this.Controls.Add(this.playerBTextbox);
            this.Controls.Add(this.playerBcbbox);
            this.Controls.Add(this.playerATextbox);
            this.Controls.Add(this.playerAcbbox);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.Name = "CombinePlayers";
            this.Size = new System.Drawing.Size(575, 402);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox playerAcbbox;
        private System.Windows.Forms.TextBox playerATextbox;
        private System.Windows.Forms.ComboBox playerBcbbox;
        private System.Windows.Forms.TextBox playerBTextbox;
        private System.Windows.Forms.TextBox licenceTxtbox;
        private System.Windows.Forms.Label label2;
    }
}
