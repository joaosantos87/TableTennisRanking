﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using static TableTennis.Database.DataSet2;
using TableTennis.Database;
using TableTennis.Database.DataSet2TableAdapters;

namespace TableTennis.Controls.General
{
    public partial class DoublesGame : UserControl, IInsertGame
    {
        public DoublesGame()
        {
            InitializeComponent();

            var allPlayers = FilesAccess.db.player.Select(x => x.Name).ToArray();
            doubleAplayerAName.Items.AddRange(allPlayers);
            doubleAplayerBName.Items.AddRange(allPlayers);
            doubleBplayerAName.Items.AddRange(allPlayers);
            doubleBplayerBName.Items.AddRange(allPlayers);
        }

        private void doubleAplayerAName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                playerRow player = FilesAccess.db.player.Single(x => x.Name == doubleAplayerAName.Text);
                clubDoubleAPlayerA.Text = FilesAccess.db.clubs.Single(x => x.Id == player.IdClub).Name;
                pointsDoubleAPlayerA.Text = player.currentPoints.ToString();
                rankingDoubleAPlayerA.Text = player.currentRank.ToString();
                licDoubleAPlayerA.Text = player.licenseNo;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void doubleAplayerBName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                playerRow player = FilesAccess.db.player.Single(x => x.Name == doubleAplayerBName.Text);
                clubDoubleAPlayerB.Text = FilesAccess.db.clubs.Single(x => x.Id == player.IdClub).Name;
                pointsDoubleAPlayerB.Text = player.currentPoints.ToString();
                rankingDoubleAPlayerB.Text = player.currentRank.ToString();
                licDoubleAPlayerB.Text = player.licenseNo;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void doubleBplayerAName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                playerRow player = FilesAccess.db.player.Single(x => x.Name == doubleBplayerAName.Text);
                clubDoubleBPlayerA.Text = FilesAccess.db.clubs.Single(x => x.Id == player.IdClub).Name;
                pointsDoubleBPlayerA.Text = player.currentPoints.ToString();
                rankingDoubleBPlayerA.Text = player.currentRank.ToString();
                licDoubleBPlayerA.Text = player.licenseNo;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void doubleBplayerBName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                playerRow player = FilesAccess.db.player.Single(x => x.Name == doubleBplayerBName.Text);
                clubDoubleBPlayerB.Text = FilesAccess.db.clubs.Single(x => x.Id == player.IdClub).Name;
                pointsDoubleBPlayerB.Text = player.currentPoints.ToString();
                rankingDoubleBPlayerB.Text = player.currentRank.ToString();
                licDoubleBPlayerB.Text = player.licenseNo;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            setDoubleA.Text = "0";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            setDoubleA.Text = "1";
        }

        private void button4_Click(object sender, EventArgs e)
        {
            setDoubleA.Text = "2";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            setDoubleA.Text = "3";
        }

        private void button8_Click(object sender, EventArgs e)
        {
            setDoubleB.Text = "0";
        }

        private void button7_Click(object sender, EventArgs e)
        {
            setDoubleB.Text = "1";
        }

        private void button6_Click(object sender, EventArgs e)
        {
            setDoubleB.Text = "2";
        }

        private void button5_Click(object sender, EventArgs e)
        {
            setDoubleB.Text = "3";
        }

        private void button9_Click(object sender, EventArgs e)
        {
            new GenericForm(new AddPlayer()).ShowDialog();
            UpdatePlayersCombobox();
        }

        public void UpdatePlayersCombobox()
        {
            string doubleAplayerAaux = doubleAplayerAName.Text;
            string doubleAplayerBaux = doubleAplayerBName.Text;
            string doubleBplayerAaux = doubleBplayerAName.Text;
            string doubleBplayerBaux = doubleBplayerBName.Text;

            doubleAplayerAName.Items.Clear();
            doubleAplayerBName.Items.Clear();
            doubleBplayerAName.Items.Clear();
            doubleBplayerBName.Items.Clear();

            var allPlayers = FilesAccess.db.player.Select(x => x.Name).ToArray();
            doubleAplayerAName.Items.AddRange(allPlayers);
            doubleAplayerBName.Items.AddRange(allPlayers);
            doubleBplayerAName.Items.AddRange(allPlayers);
            doubleBplayerBName.Items.AddRange(allPlayers);

            doubleAplayerAName.Text = doubleAplayerAaux;
            doubleAplayerBName.Text = doubleAplayerBaux;
            doubleBplayerAName.Text = doubleBplayerAaux;
            doubleBplayerBName.Text = doubleBplayerBaux;
        }

        public void UpdateInterface()
        {
            var players = Ranking.Instance.Players;
            playerRow doubleAplayerA = players.Single(x => x.Name == doubleAplayerAName.Text);
            playerRow doubleAplayerB = players.Single(x => x.Name == doubleAplayerBName.Text);
            playerRow doubleBplayerA = players.Single(x => x.Name == doubleBplayerAName.Text);
            playerRow doubleBplayerB = players.Single(x => x.Name == doubleBplayerBName.Text);

            pointsDoubleAPlayerA.Text = doubleAplayerA.currentPoints.ToString();
            rankingDoubleAPlayerA.Text = doubleAplayerA.currentRank.ToString();

            pointsDoubleAPlayerB.Text = doubleAplayerB.currentPoints.ToString();
            rankingDoubleAPlayerB.Text = doubleAplayerB.currentRank.ToString();

            pointsDoubleBPlayerA.Text = doubleBplayerA.currentPoints.ToString();
            rankingDoubleBPlayerA.Text = doubleBplayerA.currentRank.ToString();

            pointsDoubleBPlayerB.Text = doubleBplayerB.currentPoints.ToString();
            rankingDoubleBPlayerB.Text = doubleBplayerB.currentRank.ToString();

            gameInsertedLabel.Text = "Inserted "+ doubleAplayerAName.Text+ "&"+doubleAplayerBName.Text + setDoubleA.Text+ " - "+ setDoubleB.Text+ " "+doubleBplayerAName.Text + "&" + doubleBplayerBName.Text;
        }
        
        public void CreateGame(DateTime date, string champ, string phase, int idTeamGame)
        {
            int doubleAplayerAId = FilesAccess.db.player.Single(x => x.Name == doubleAplayerAName.Text).Id;
            int doubleAplayerBId = FilesAccess.db.player.Single(x => x.Name == doubleAplayerBName.Text).Id;
            int doubleBplayerAId = FilesAccess.db.player.Single(x => x.Name == doubleBplayerAName.Text).Id;
            int doubleBplayerBId = FilesAccess.db.player.Single(x => x.Name == doubleBplayerBName.Text).Id;
            int phaseId = FilesAccess.db.phase.Single(x => x.id + "-" + x.phase == phase).id;
            
            int setsA = Convert.ToInt32(setDoubleA.Text);
            int setsB = Convert.ToInt32(setDoubleB.Text);
            
            //Check if game already inserted in database
            if (FilesAccess.db.doubleGames.Count(
                    x => ((((x.DoubleA_PlayerAId == doubleAplayerAId || (x.DoubleA_PlayerBId == doubleAplayerAId) && (x.DoubleA_PlayerAId == doubleAplayerBId || x.DoubleA_PlayerBId == doubleAplayerBId))) &&
                    (x.DoubleB_PlayerAId == doubleBplayerAId || (x.DoubleB_PlayerBId == doubleBplayerAId) && (x.DoubleB_PlayerAId == doubleBplayerBId || x.DoubleB_PlayerBId == doubleBplayerBId)))
                        &&
                        x.Date.ToShortDateString() == date.ToShortDateString())) > 0)
            {
                MessageBox.Show("Game Already Entered");
                return;
            }

            new doublegamesTableAdapter().Insert(
                    doubleAplayerAId,
                    doubleAplayerBId,
                    doubleBplayerAId,
                    doubleBplayerBId,
                setsA,
                setsB,
                setsA > setsB ? doubleAplayerAId : doubleBplayerAId,
                setsA > setsB ? doubleAplayerBId : doubleBplayerBId,
                date,
                FilesAccess.db.championships.Single(x => x.Name == champ).Id,
                0,
                0,
                0,
                0,
                phaseId,
                idTeamGame,
                1,
                DateTime.Now,
                0,
                0,
                0,
                0,
                0);
            
            //Ranking.Instance.GenerateRanking();
            new doublegamesTableAdapter().Fill(FilesAccess.db.doubleGames);

            UpdateInterface();
        }

    }
}
