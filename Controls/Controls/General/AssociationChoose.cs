﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TableTennis.Database;

namespace TableTennis.Controls
{
    public partial class AssociationChoose : UserControl
    {
        public bool IsFilled
        {
            get { return !comboBox2.Text.Equals(String.Empty); }
        }

        public AssociationChoose()
        {
            InitializeComponent();
            VisibleChanged += AssociationChoose_VisibleChanged;
        }

        private void AssociationChoose_VisibleChanged(object sender, EventArgs e)
        {
            string text = comboBox2.Text;

            comboBox2.Items.Clear();
            for (int i = 0; i < FilesAccess.db.associations.Count; i++)
            {
                comboBox2.Items.Add(FilesAccess.db.associations[i].name);
            }
            comboBox2.Sorted = true;

            comboBox2.Text = text;
        }

        public string SetAssociation
        {
            set { comboBox2.Text = value; }
        }

        public string GetAssociationName
        {
            get { return comboBox2.Text; }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            new AddAssociation().Show();
        }
    }
}
