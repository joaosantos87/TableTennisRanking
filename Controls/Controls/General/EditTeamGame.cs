﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TableTennis.Database;
using TableTennis.Database.DataSet2TableAdapters;

namespace TableTennis.Controls.General
{
    public partial class EditTeamGame : UserControl
    {
        public EditTeamGame()
        {
            InitializeComponent();
            Dock = DockStyle.Fill;
            clubACombobox.Items.AddRange(FilesAccess.db.clubs.OrderBy(x => x.Name).Select(x => x.Name).ToArray());
            clubBCombobox.Items.AddRange(FilesAccess.db.clubs.OrderBy(x => x.Name).Select(x => x.Name).ToArray());
            champCombobox.Items.AddRange(FilesAccess.db.championships.OrderByDescending(x => FilesAccess.db.tournamentType.Single(a => a.id == x.idTournamentType).weight).Select(x => x.Name).ToArray());
        }

        List<DataSet2.teamgameRow> allTeamGames;
        private void selectTeamGameBtn_Click(object sender, EventArgs e)
        {
            if (champCombobox.Text == String.Empty)
                return;

            var championship = FilesAccess.db.championships.Single(x => x.Name == champCombobox.Text);
            if (!Int32.TryParse(jornadaTextbox.Text, out int jornada))
                return;

            allTeamGames = FilesAccess.db.teamGames.Where(x =>
            x.IdChampionship == championship.Id &&
            x.Date > startDateTimePicker.Value &&
            x.Date < endDateTimePicker.Value &&
            (jornada != 0 ? x.Day == jornada : true)).ToList();

            //var allTeamGames = 
            //    from tg in FilesAccess.db.teamGames
            //    from c in FilesAccess.db.clubs
            //    where tg.IdChampionship == championship.Id &&
            //    tg.Date > startDateTimePicker.Value &&
            //    tg.Date < endDateTimePicker.Value &&
            //    jornada != 0 ? tg.Day == jornada : true &&
            //    tg.cl
            teamGamesCbbox.Items.Clear();
            teamGamesCbbox.Items.AddRange(allTeamGames.Select(
                x => $"[{x.Day}] [{x.Date.ToShortDateString()}] {FilesAccess.db.clubs.Single(a => a.Id == x.ClubAId).Name} {x.clubAnameAppend} - {FilesAccess.db.clubs.Single(b => b.Id == x.ClubBId).Name} {x.clubBnameAppend}").ToArray());
        }

        private void teamGamesCbbox_SelectedIndexChanged(object sender, EventArgs e)
        {
            var currentTeamGame = FilesAccess.db.teamGames.Single(x => x.Id == allTeamGames[teamGamesCbbox.SelectedIndex].Id);
            clubACombobox.Text = FilesAccess.db.clubs.Single(a => a.Id == currentTeamGame.ClubAId).Name;
            clubAappendTextbox.Text = currentTeamGame.clubAnameAppend;
            clubBCombobox.Text = FilesAccess.db.clubs.Single(a => a.Id == currentTeamGame.ClubBId).Name;
            clubBappendTextbox.Text = currentTeamGame.clubBnameAppend;

            gameDate.Value = currentTeamGame.Date;

            var singleGames =
                from g in FilesAccess.db.games
                where g.IdTeamGame == currentTeamGame.Id
                select new
                {
                    Id = g.id,
                    LicencaJogadorA = FilesAccess.db.player.Single(x => x.Id == g.IdPlayerA).licenseNo,
                    JogadorA = FilesAccess.db.player.Single(x => x.Id == g.IdPlayerA).Name,
                    SetsA = g.SetsA,
                    VS = " - ",
                    SetsB = g.SetsB,
                    LicencaJogadorB = FilesAccess.db.player.Single(x => x.Id == g.IdPlayerB).licenseNo,
                    JogadorB = FilesAccess.db.player.Single(x => x.Id == g.IdPlayerB).Name
                };

                FilesAccess.db.games.Where(x => x.IdTeamGame == currentTeamGame.Id);
            var doubleGames = FilesAccess.db.doubleGames.Where(x => x.IdTeamGame == currentTeamGame.Id);

            gamesDataGridView.DataSource = singleGames.ToList();
        }

        private void updateGameBtn_Click(object sender, EventArgs e)
        {
            var currentTeamGame = FilesAccess.db.teamGames.Single(x => x.Id == allTeamGames[teamGamesCbbox.SelectedIndex].Id);
            currentTeamGame.Date = gameDate.Value;

            new teamgameTableAdapter().Update(currentTeamGame);
            new teamgameTableAdapter().Fill(FilesAccess.db.teamGames);
        }
    }
}
