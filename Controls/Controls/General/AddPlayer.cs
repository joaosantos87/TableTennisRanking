﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TableTennis.Database;
using TableTennis.Database.DataSet2TableAdapters;

namespace TableTennis.Controls
{
    public partial class AddPlayer : UserControl
    {
        string playerNameAutomatic = "";
        public AddPlayer()
        {
            Contruct();
        }

        public AddPlayer(string name, string licenca)
        {
            Contruct();
            nameTextbox.Text = name;
            playerNameAutomatic = name;
            licenceTxtbox.Text = licenca;
            clubCombobox.Text = "Nao Disponivel";
        }

        private void Contruct()
        {

            InitializeComponent();

            VisibleChanged += AddPlayer_IsVisibleChanged;
            createPlayerButton.Click += createPlayerButton_Click;
        }

        private void createPlayerButton_Click(object sender, EventArgs e)
        {
            string playerToInsert = nameTextbox.Text;

            List<string> similarPlayers = new List<string>();
            List<string> samePlayers = new List<string>();
            
            for (int i = 0; i < FilesAccess.db.player.Count; i++)
            {
                if (Utilities.LevenshteinDistance(FilesAccess.db.player[i].Name, playerToInsert) == 1)
                {
                    int clubId = FilesAccess.db.player[i].Id;
                    samePlayers.Add("[" + FilesAccess.db.player[i].Name + " - " + FilesAccess.db.clubs.Single(x => x.Id == FilesAccess.db.player[i].IdClub).Name + "]");
                }

                if (Utilities.ContainsWord(FilesAccess.db.player[i].Name, playerToInsert))
                {
                    similarPlayers.Add("[" + FilesAccess.db.player[i].Name + " - " + FilesAccess.db.clubs.Single(x => x.Id == FilesAccess.db.player[i].IdClub).Name + "]");
                }
            }

            string messageShow = "";
            var playerExist = licenceTxtbox.Text!="" ? FilesAccess.db.player.SingleOrDefault(x => x.licenseNo == licenceTxtbox.Text):null;
            if (playerExist != null)
            {
                MessageBox.Show($"Jogador com licensa {playerExist.licenseNo} ja inserido, com o nome {playerExist.Name}", "Information", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (samePlayers.Count > 0)
            {
                messageShow = "Found " + samePlayers.Count + " same players in the database: " + Environment.NewLine;
                for (int i = 0; i < samePlayers.Count; i++)
                    messageShow += samePlayers[i] + Environment.NewLine;
                messageShow += "Do you still want to enter the new player?";
            }

            if (messageShow == "" && similarPlayers.Count > 0)
            {
                messageShow = "Found " + similarPlayers.Count + " similiar players in the database: " + Environment.NewLine;
                for (int i = 0; i < similarPlayers.Count; i++)
                    messageShow += similarPlayers[i] + Environment.NewLine;
                messageShow += "Do you still want to enter the new player?";
            }

            bool insert = true;

            if (messageShow != "")
            {
                
                DialogResult mbRes = MessageBox.Show(messageShow, "Information", MessageBoxButtons.YesNo);

                //Insert player
                if (mbRes == DialogResult.Yes)
                {
                    insert = true;

                }
                else if (mbRes == DialogResult.No)
                    insert = false;
            }

            if (insert)
            {
                new playerTableAdapter().Insert(
                nameTextbox.Text,
                BirthDate,
               FilesAccess.db.clubs.Single(x => x.Name == clubCombobox.Text).Id,
                genderCombobox.Text,
                photoTextbox.Text,
                1000,
                -1,
                0,
                licenceTxtbox.Text!=""?licenceTxtbox.Text:"0",
                -1,
                playerNameAutomatic,
                1000,
                1,
                4);

                //FilesAccess.db.UpdatePlayer();

                new playerTableAdapter().Fill(FilesAccess.db.player);

                //FilesAccess.db.AddPlayer();
            }
        }

        private void AddPlayer_IsVisibleChanged(object sender, EventArgs e)
        {
            clubCombobox.Items.Clear();
            for (int i = 0; i < FilesAccess.db.clubs.Count; i++)
            {
                clubCombobox.Items.Add(FilesAccess.db.clubs[i].Name);
            }
            if (clubCombobox.Items.Count > 0 && clubCombobox.Text.Equals(""))
                clubCombobox.SelectedIndex = 0;

            comboBox1.Items.Clear();
            for (int i = 0; i < FilesAccess.db.player.Count; i++)
            {
                comboBox1.Items.Add(FilesAccess.db.player[i].Name);
            }
            comboBox1.Sorted = true;
        }

        public DateTime BirthDate
        {
            get
            {
                try
                {
                    return new DateTime(Convert.ToInt32(birthTextbox.Text), 1, 1);
                }
                catch { return new DateTime(1900, 1, 1); }
            }
        }

        private void AddPlayer_Load(object sender, EventArgs e)
        {
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            int playerid = FilesAccess.db.player.Single(x => x.Name == comboBox1.Text).Id;
            textBox2.Text = FilesAccess.db.player.Single(x => x.Id == playerid).licenseNo;
            textBox3.Text = FilesAccess.db.clubs.Single(x => x.Id == FilesAccess.db.player.Single(y => y.Id == playerid).IdClub).Name;
        }

        private void createPlayerButton_Click_1(object sender, EventArgs e)
        {

        }
    }
}
