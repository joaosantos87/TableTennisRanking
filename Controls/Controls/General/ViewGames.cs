﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TableTennis.Database;
using static TableTennis.Database.DataSet2;
using TableTennis.Database.DataSet2TableAdapters;
using TableTennis.Controls.General;

namespace TableTennis.Controls
{
    public partial class ViewGames : UserControl
    {
        championshipsRow champ;
        DateTime startTime;
        DateTime endTime;

        BindingList<Game> games = new BindingList<Game>();
        BindingSource source;

        public ViewGames()
        {
            InitializeComponent();
            Dock = DockStyle.Fill;
            VisibleChanged += ViewGames_VisibleChanged;
            this.dataGridView1.RowsAdded +=dataGridView1_RowsAdded;
            this.dataGridView1.ColumnHeaderMouseClick += dataGridView1_ColumnHeaderMouseClick;

            //startDateTimePicker.DataBindings.Add(new Binding("Value", startTime, "Value"));
            //endDateTimePicker.DataBindings.Add(new Binding("Value", endTime, "Value"));

            startTime = DateTimePicker.MinimumDateTime;
            endTime = DateTime.Now;

            startDateTimePicker.Value = startTime;
            endDateTimePicker.Value = endTime;

            champCbbox.Items.Clear();
            champCbbox.Items.Add("Todos");
            champCbbox.Items.AddRange(FilesAccess.db.championships.Select(x => x.Name).ToArray());

            UpdateGames();

            //var gms = from gm in FilesAccess.db.games
            //          where gm.Date > startTime.Date && gm.Date < endTime.Date && (champ != null ? gm.IdChamp == champ.Id : true)
            //          select new Game(gm.id)
            //          {
            //              ChampId = gm.IdChamp,
            //              IdPlayerWinner = gm.IdPlayerWinner,
            //              PhaseId = gm.idPhase,
            //              PlayerA_Id = gm.IdPlayerA,
            //              PlayerB_Id = gm.IdPlayerB,
            //              TeamGameId = gm.IdTeamGame
            //          };

            //games = new BindingList<Game>(gms.ToList());

            ////source = new BindingSource(games, null);
            //dataGridView1.DataSource = games;
        }

        private void dataGridView1_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            dataGridView1.DataSource = FilesAccess.db.games.OrderByDescending(x => x.IdPlayerA).ToList();
        }

        private void dataGridView1_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            return;
            try
            {
                if (e.RowCount < 1)
                    return;

                for (int i = e.RowIndex; i < e.RowCount + e.RowIndex; i++)
                {
                    int idGame = Convert.ToInt32(dataGridView1["id", i].Value);
                    var curGame = FilesAccess.db.games.Single(x => x.id == idGame);
                    long idplayerA = curGame.IdPlayerA;
                    long idplayerB = curGame.IdPlayerB;

                    if (FilesAccess.db.player.Count(x => x.Id == idplayerA) > 0)
                    {
                        /*if (dataGridView1["playerA", i].Value == FilesAccess.db.player.Single(x => x.Id == idplayerA).Name)
                            break;*/
                        dataGridView1["playerA", i].Value = FilesAccess.db.player.Single(x => x.Id == idplayerA).Name;
                    }
                    if (FilesAccess.db.player.Count(x => x.Id == idplayerB) > 0)
                    {
                        dataGridView1["playerB", i].Value = FilesAccess.db.player.Single(x => x.Id == idplayerB).Name;
                    }
                    
                }
            }
            catch (Exception ex) { }
        }

        private void ViewGames_VisibleChanged(object sender, EventArgs e)
        {
            //dataGridView1.DataSource = FilesAccess.db.games.OrderByDescending(x => x.Date).ToList();
            //dataGridView1.DataSource = FilesAccess.db.games.OrderByDescending(x => x.pointsPlayerA).ThenByDescending(x=>x.Date).ToList();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Update DB
            //TODO: commented this line. if any error occours in updating games, uncoment this dataGridView1.EndEdit();
            //FilesAccess.db.UpdateGames();
            var gamesUpdated = new List<gamesRow>();
            var gamesChanged = games.Where(x => gamesToUpdate.Contains(x.Id));
            foreach(var gm in gamesChanged)
            {
                var g = FilesAccess.db.games.SingleOrDefault(x => x.id == gm.Id);

                g.IdChamp = gm.ChampId;
                g.idPhase = gm.PhaseId;
                g.IdPlayerA = gm.PlayerA_Id;
                g.IdPlayerB = gm.PlayerB_Id;
                g.IdPlayerWinner = gm.IdPlayerWinner;
                g.IdTeamGame = gm.TeamGameId;
                g.SetsA = gm.SetsA;
                g.SetsB = gm.SetsB;
                g.Date = gm.Date;
                g.ignoreOnRankings = gm.IgnoreOnRankings;
                gamesUpdated.Add(g);
            }


            new gamesTableAdapter().Update(gamesUpdated.ToArray());
            gamesToUpdate.Clear();
        }
        List<teamgameRow> teamGames = new List<teamgameRow>();
        private void champCbbox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (champCbbox.Text != "Todos")
                champ = FilesAccess.db.championships.Single(x => x.Name == champCbbox.Text);
            else
                champ = null;

            UpdateTeamGames();
            UpdateGames();
        }

        private void UpdateTeamGames()
        {
            teamGames = FilesAccess.db.teamGames.Where(x => (champ != null ? x.IdChampionship == champ.Id : true) && x.Date > startTime.Date && x.Date < endTime.Date).ToList();

            teamGameCbbox.Items.Clear();
            teamGameCbbox.Items.AddRange(teamGames.Select(
                x => $"[{x.Id}]-[{x.Date.ToShortDateString()}] {FilesAccess.db.clubs.Single(a => a.Id == x.ClubAId).Name} {x.clubAnameAppend} - {FilesAccess.db.clubs.Single(a => a.Id == x.ClubBId).Name} {x.clubBnameAppend}").ToArray());
        }

        private void UpdateGames()
        {
            var tgRow = teamGameCbbox.Text != "" ? teamGames[teamGameCbbox.SelectedIndex] : null;

            var gms = from gm in FilesAccess.db.games
                      where gm.Date > startTime.Date && gm.Date < endTime.Date && (champ != null ? gm.IdChamp == champ.Id : true) && (tgRow!=null ? tgRow.Id == gm.IdTeamGame : true)
                      select new Game(gm.id)
                      {
                          ChampId = gm.IdChamp,
                          IdPlayerWinner = gm.IdPlayerWinner,
                          PhaseId = gm.idPhase,
                          PlayerA_Id = gm.IdPlayerA,
                          PlayerB_Id = gm.IdPlayerB,
                          TeamGameId = gm.IdTeamGame,
                          SetsA = gm.SetsA,
                          SetsB = gm.SetsB,
                          Date = gm.Date,
                          IgnoreOnRankings = gm.ignoreOnRankings
                      };

            games = new BindingList<Game>(gms.ToList());
            dataGridView1.DataSource = games;

            datagridviewColumnsCbbox.Items.Clear();
            for (var i = 0; i < dataGridView1.ColumnCount; i++)
                datagridviewColumnsCbbox.Items.Add(dataGridView1.Columns[i].Name);
        }
        private void startDateTimePicker_ValueChanged(object sender, EventArgs e)
        {
            startTime = startDateTimePicker.Value;
            UpdateTeamGames();
            UpdateGames();
        }

        private void endDateTimePicker_ValueChanged(object sender, EventArgs e)
        {
            endTime = endDateTimePicker.Value;
            UpdateTeamGames();
            UpdateGames();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            new GenericForm(new AddTeamGame()).ShowDialog();
            new teamgameTableAdapter().Fill(FilesAccess.db.teamGames);
        }

        private void teamGameCbbox_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateGames();
        }
        List<int> gamesToUpdate = new List<int>();
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void dataGridView1_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            var row = e.RowIndex;

            if (row < 0)
                return;

            var toAdd = games[row];

            if (gamesToUpdate.Count(x => x == toAdd.Id) > 0)
                return;

            gamesToUpdate.Add(toAdd.Id);
        }

        private void changeValueBtn_Click(object sender, EventArgs e)
        {
            var selectedRows = dataGridView1.SelectedRows;

            var column = datagridviewColumnsCbbox.Text;
            var valueToChange = valueToChangeTxtbox.Text;

            var dr = MessageBox.Show($"Change will affect {selectedRows.Count} rows. It will change column {column} value to {valueToChange}.\nDo you want to continue?", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (dr == DialogResult.No)
                return;

            foreach(DataGridViewRow row in selectedRows)
            {
                row.Cells[column].Value = valueToChange;

                var id = row.Index;

                var toAdd = games[id];

                if (gamesToUpdate.Count(x => x == toAdd.Id) > 0)
                    continue;

                gamesToUpdate.Add(toAdd.Id);
            }
        }
    }
}
