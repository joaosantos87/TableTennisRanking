﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TableTennis.Database;
using static TableTennis.Database.DataSet2;

namespace TableTennis.Controls
{
    public partial class EditClub : UserControl
    {
        clubsRow club;

        public EditClub()
        {
            InitializeComponent();
            VisibleChanged += ViewGames_VisibleChanged;
        }

        private void ViewGames_VisibleChanged(object sender, EventArgs e)
        {
            comboBox1.Items.Clear();
            for (int i = 0; i < FilesAccess.db.clubs.Count; i++)
            {
                comboBox1.Items.Add(FilesAccess.db.clubs[i].Name);
            }
            comboBox1.Sorted = true;

            if(comboBox1.Items.Count>0)
                comboBox1.SelectedIndex = 0;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            club = FilesAccess.db.clubs.Single(x => x.Name == comboBox1.Text);
            textBox1.Text = club.Name;
            textBox2.Text = club.Shortname;

            try
            {
                associationChoose1.SetAssociation = FilesAccess.db.associations.Single(x => x.id == club.AssociationId).name;
                //comboBox2.Text = FilesAccess.db.associations.Single(x => x.id == club.AssociationId).name;
            }
            catch { associationChoose1.SetAssociation = ""; }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            clubsRow clubRow = FilesAccess.db.clubs.Single(x => x.Name == comboBox1.Text);
            clubRow.Name = textBox1.Text;
            clubRow.Shortname = textBox2.Text;

            try
            {
                clubRow.AssociationId = FilesAccess.db.associations.Single(x => x.name == associationChoose1.GetAssociationName).id;
            }
            catch { MessageBox.Show("Exception occured. Update not sucessfully!"); return; }

            comboBox1.Items.Clear();
            for (int i = 0; i < FilesAccess.db.clubs.Count; i++)
            {
                comboBox1.Items.Add(FilesAccess.db.clubs[i].Name);
            }
            comboBox1.Sorted = true;

            FilesAccess.db.UpdateClub();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            
        }
    }
}
