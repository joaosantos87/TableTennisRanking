﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TableTennis.Database;
using static TableTennis.Database.DataSet2;

namespace TableTennis.Controls
{
    public partial class CombinePlayers : UserControl
    {
        public CombinePlayers()
        {
            InitializeComponent();
            VisibleChanged += PlayerHistoric_VisibleChanged;
        }

        void PlayerHistoric_VisibleChanged(object sender, EventArgs e)
        {
            playerAcbbox.Items.Clear();
            playerBcbbox.Items.Clear();
            playerAcbbox.Items.AddRange(FilesAccess.db.player.Select(x => x.Name).OrderBy(x => x).ToArray());
            playerBcbbox.Items.AddRange(FilesAccess.db.player.Select(x => x.Name).OrderBy(x => x).ToArray());
        }

        private void playerA_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                playerATextbox.Clear();
                var playerA = FilesAccess.db.player.Single(x => x.Name == playerAcbbox.Text);
                playerATextbox.Text += $"Ranking: {playerA.currentRank} {Environment.NewLine}Points: {playerA.currentPoints}{Environment.NewLine}Record: {playerA.currentGamesWon}-{playerA.currentGamesLost}";

                if (playerA.licenseNo != "")
                    licenceTxtbox.Text = playerA.licenseNo;
            }
            catch
            {

            }
        }

        private void playerBcbbox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                playerBTextbox.Clear();
                var playerB = FilesAccess.db.player.Single(x => x.Name == playerBcbbox.Text);
                playerBTextbox.Text += $"Ranking: {playerB.currentRank} {Environment.NewLine}Points: {playerB.currentPoints}{Environment.NewLine}Record: {playerB.currentGamesWon}-{playerB.currentGamesLost}";

                if (playerB.licenseNo != "")
                    licenceTxtbox.Text = playerB.licenseNo;
                }
            catch
            {
                
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var playerA = FilesAccess.db.player.Single(x => x.Name == playerAcbbox.Text);
            var playerB = FilesAccess.db.player.Single(x => x.Name == playerBcbbox.Text);

            if (playerA.currentPoints > playerB.currentPoints)
                Join(playerA.Id, playerB.Id);
            else
                Join(playerB.Id, playerA.Id);
        }

        private void Join(int idToContinue, int idToRemove)
        {
            //make sure to continue has the correct license

            var playerToContinue = FilesAccess.db.player.Single(x => x.Id == idToContinue);
            var playerToRemove = FilesAccess.db.player.Single(x => x.Id == idToRemove);

            if (playerToContinue.licenseNo.Equals(String.Empty) && !playerToRemove.licenseNo.Equals(String.Empty))
            {
                playerToContinue.licenseNo = playerToRemove.licenseNo;

                FilesAccess.db.UpdatePlayer();
            }

            //Replace all single and doubles id that has the idToRemove for the idToContinue
            var games = FilesAccess.db.games.Where(x => x.IdPlayerA == idToRemove || x.IdPlayerB == idToRemove || x.IdPlayerWinner == idToRemove);

            var doubleGames = FilesAccess.db.doubleGames.Where(x => x.DoubleA_PlayerAId == idToRemove || x.DoubleA_PlayerBId == idToRemove || x.DoubleB_PlayerAId == idToRemove || x.DoubleB_PlayerBId == idToRemove);

            foreach(var game in games)
            {
                if (game.IdPlayerA == idToRemove)
                    game.IdPlayerA = idToContinue;

                if (game.IdPlayerB == idToRemove)
                    game.IdPlayerB = idToContinue;

                if (game.IdPlayerWinner == idToRemove)
                    game.IdPlayerWinner = idToContinue;

                FilesAccess.db.UpdateGames();
            }

            foreach(var doubleGame in doubleGames)
            {
                if (doubleGame.DoubleA_PlayerAId == idToRemove)
                    doubleGame.DoubleA_PlayerAId = idToContinue;

                if (doubleGame.DoubleA_PlayerBId == idToRemove)
                    doubleGame.DoubleA_PlayerBId = idToContinue;

                if (doubleGame.DoubleB_PlayerAId == idToRemove)
                    doubleGame.DoubleB_PlayerAId = idToContinue;

                if (doubleGame.DoubleB_PlayerBId == idToRemove)
                    doubleGame.DoubleB_PlayerBId = idToContinue;

                if (doubleGame.IdPlayerAWinner == idToRemove)
                    doubleGame.IdPlayerAWinner = idToContinue;

                if (doubleGame.IdPlayerBWinner == idToRemove)
                    doubleGame.IdPlayerBWinner = idToContinue;

                FilesAccess.db.UpdateDoubleGames();
            }

        }
    }
}
