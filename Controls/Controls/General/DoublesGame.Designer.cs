﻿namespace TableTennis.Controls.General
{
    partial class DoublesGame
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.licDoubleBPlayerA = new System.Windows.Forms.Label();
            this.licDoubleAPlayerA = new System.Windows.Forms.Label();
            this.button9 = new System.Windows.Forms.Button();
            this.pointsDoubleBPlayerA = new System.Windows.Forms.Label();
            this.rankingDoubleBPlayerA = new System.Windows.Forms.Label();
            this.pointsDoubleAPlayerA = new System.Windows.Forms.Label();
            this.rankingDoubleAPlayerA = new System.Windows.Forms.Label();
            this.clubDoubleBPlayerA = new System.Windows.Forms.Label();
            this.clubDoubleAPlayerA = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.setDoubleB = new System.Windows.Forms.TextBox();
            this.doubleBplayerAName = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.setDoubleA = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.doubleAplayerAName = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.gameInsertedLabel = new System.Windows.Forms.Label();
            this.licDoubleAPlayerB = new System.Windows.Forms.Label();
            this.clubDoubleAPlayerB = new System.Windows.Forms.Label();
            this.doubleAplayerBName = new System.Windows.Forms.ComboBox();
            this.licDoubleBPlayerB = new System.Windows.Forms.Label();
            this.clubDoubleBPlayerB = new System.Windows.Forms.Label();
            this.doubleBplayerBName = new System.Windows.Forms.ComboBox();
            this.pointsDoubleAPlayerB = new System.Windows.Forms.Label();
            this.rankingDoubleAPlayerB = new System.Windows.Forms.Label();
            this.pointsDoubleBPlayerB = new System.Windows.Forms.Label();
            this.rankingDoubleBPlayerB = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // licDoubleBPlayerA
            // 
            this.licDoubleBPlayerA.AutoSize = true;
            this.licDoubleBPlayerA.Location = new System.Drawing.Point(46, 123);
            this.licDoubleBPlayerA.Name = "licDoubleBPlayerA";
            this.licDoubleBPlayerA.Size = new System.Drawing.Size(35, 13);
            this.licDoubleBPlayerA.TabIndex = 63;
            this.licDoubleBPlayerA.Text = "label7";
            // 
            // licDoubleAPlayerA
            // 
            this.licDoubleAPlayerA.AutoSize = true;
            this.licDoubleAPlayerA.Location = new System.Drawing.Point(48, 50);
            this.licDoubleAPlayerA.Name = "licDoubleAPlayerA";
            this.licDoubleAPlayerA.Size = new System.Drawing.Size(35, 13);
            this.licDoubleAPlayerA.TabIndex = 62;
            this.licDoubleAPlayerA.Text = "label7";
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(464, 62);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(68, 23);
            this.button9.TabIndex = 61;
            this.button9.Text = "Add Player";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // pointsDoubleBPlayerA
            // 
            this.pointsDoubleBPlayerA.AutoSize = true;
            this.pointsDoubleBPlayerA.Location = new System.Drawing.Point(364, 123);
            this.pointsDoubleBPlayerA.Name = "pointsDoubleBPlayerA";
            this.pointsDoubleBPlayerA.Size = new System.Drawing.Size(35, 13);
            this.pointsDoubleBPlayerA.TabIndex = 60;
            this.pointsDoubleBPlayerA.Text = "label7";
            // 
            // rankingDoubleBPlayerA
            // 
            this.rankingDoubleBPlayerA.AutoSize = true;
            this.rankingDoubleBPlayerA.Location = new System.Drawing.Point(310, 123);
            this.rankingDoubleBPlayerA.Name = "rankingDoubleBPlayerA";
            this.rankingDoubleBPlayerA.Size = new System.Drawing.Size(35, 13);
            this.rankingDoubleBPlayerA.TabIndex = 59;
            this.rankingDoubleBPlayerA.Text = "label7";
            // 
            // pointsDoubleAPlayerA
            // 
            this.pointsDoubleAPlayerA.AutoSize = true;
            this.pointsDoubleAPlayerA.Location = new System.Drawing.Point(364, 50);
            this.pointsDoubleAPlayerA.Name = "pointsDoubleAPlayerA";
            this.pointsDoubleAPlayerA.Size = new System.Drawing.Size(35, 13);
            this.pointsDoubleAPlayerA.TabIndex = 58;
            this.pointsDoubleAPlayerA.Text = "label7";
            // 
            // rankingDoubleAPlayerA
            // 
            this.rankingDoubleAPlayerA.AutoSize = true;
            this.rankingDoubleAPlayerA.Location = new System.Drawing.Point(310, 48);
            this.rankingDoubleAPlayerA.Name = "rankingDoubleAPlayerA";
            this.rankingDoubleAPlayerA.Size = new System.Drawing.Size(35, 13);
            this.rankingDoubleAPlayerA.TabIndex = 57;
            this.rankingDoubleAPlayerA.Text = "label7";
            // 
            // clubDoubleBPlayerA
            // 
            this.clubDoubleBPlayerA.AutoSize = true;
            this.clubDoubleBPlayerA.Location = new System.Drawing.Point(88, 124);
            this.clubDoubleBPlayerA.Name = "clubDoubleBPlayerA";
            this.clubDoubleBPlayerA.Size = new System.Drawing.Size(35, 13);
            this.clubDoubleBPlayerA.TabIndex = 56;
            this.clubDoubleBPlayerA.Text = "label7";
            // 
            // clubDoubleAPlayerA
            // 
            this.clubDoubleAPlayerA.AutoSize = true;
            this.clubDoubleAPlayerA.Location = new System.Drawing.Point(90, 50);
            this.clubDoubleAPlayerA.Name = "clubDoubleAPlayerA";
            this.clubDoubleAPlayerA.Size = new System.Drawing.Size(35, 13);
            this.clubDoubleAPlayerA.TabIndex = 55;
            this.clubDoubleAPlayerA.Text = "label7";
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(473, 96);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(29, 23);
            this.button5.TabIndex = 54;
            this.button5.Text = "3";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(431, 96);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(29, 23);
            this.button6.TabIndex = 53;
            this.button6.Text = "2";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(388, 96);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(29, 23);
            this.button7.TabIndex = 52;
            this.button7.Text = "1";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(346, 95);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(29, 23);
            this.button8.TabIndex = 51;
            this.button8.Text = "0";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(473, 20);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(29, 23);
            this.button3.TabIndex = 50;
            this.button3.Text = "3";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(431, 20);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(29, 23);
            this.button4.TabIndex = 49;
            this.button4.Text = "2";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(388, 20);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(29, 23);
            this.button2.TabIndex = 48;
            this.button2.Text = "1";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(346, 19);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(29, 23);
            this.button1.TabIndex = 47;
            this.button1.Text = "0";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // setDoubleB
            // 
            this.setDoubleB.Location = new System.Drawing.Point(306, 98);
            this.setDoubleB.Name = "setDoubleB";
            this.setDoubleB.Size = new System.Drawing.Size(25, 20);
            this.setDoubleB.TabIndex = 46;
            // 
            // doubleBplayerAName
            // 
            this.doubleBplayerAName.FormattingEnabled = true;
            this.doubleBplayerAName.Location = new System.Drawing.Point(78, 98);
            this.doubleBplayerAName.Name = "doubleBplayerAName";
            this.doubleBplayerAName.Size = new System.Drawing.Size(106, 21);
            this.doubleBplayerAName.Sorted = true;
            this.doubleBplayerAName.TabIndex = 45;
            this.doubleBplayerAName.SelectedIndexChanged += new System.EventHandler(this.doubleBplayerAName_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(4, 101);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(51, 13);
            this.label6.TabIndex = 44;
            this.label6.Text = "Double B";
            // 
            // setDoubleA
            // 
            this.setDoubleA.Location = new System.Drawing.Point(306, 20);
            this.setDoubleA.Name = "setDoubleA";
            this.setDoubleA.Size = new System.Drawing.Size(25, 20);
            this.setDoubleA.TabIndex = 43;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(305, 2);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(28, 13);
            this.label5.TabIndex = 42;
            this.label5.Text = "Sets";
            // 
            // doubleAplayerAName
            // 
            this.doubleAplayerAName.FormattingEnabled = true;
            this.doubleAplayerAName.Location = new System.Drawing.Point(78, 20);
            this.doubleAplayerAName.Name = "doubleAplayerAName";
            this.doubleAplayerAName.Size = new System.Drawing.Size(106, 21);
            this.doubleAplayerAName.Sorted = true;
            this.doubleAplayerAName.TabIndex = 41;
            this.doubleAplayerAName.SelectedIndexChanged += new System.EventHandler(this.doubleAplayerAName_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(4, 23);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 13);
            this.label4.TabIndex = 40;
            this.label4.Text = "Double A";
            // 
            // gameInsertedLabel
            // 
            this.gameInsertedLabel.AutoSize = true;
            this.gameInsertedLabel.Location = new System.Drawing.Point(36, 82);
            this.gameInsertedLabel.Name = "gameInsertedLabel";
            this.gameInsertedLabel.Size = new System.Drawing.Size(0, 13);
            this.gameInsertedLabel.TabIndex = 64;
            // 
            // licDoubleAPlayerB
            // 
            this.licDoubleAPlayerB.AutoSize = true;
            this.licDoubleAPlayerB.Location = new System.Drawing.Point(48, 72);
            this.licDoubleAPlayerB.Name = "licDoubleAPlayerB";
            this.licDoubleAPlayerB.Size = new System.Drawing.Size(35, 13);
            this.licDoubleAPlayerB.TabIndex = 67;
            this.licDoubleAPlayerB.Text = "label7";
            // 
            // clubDoubleAPlayerB
            // 
            this.clubDoubleAPlayerB.AutoSize = true;
            this.clubDoubleAPlayerB.Location = new System.Drawing.Point(90, 72);
            this.clubDoubleAPlayerB.Name = "clubDoubleAPlayerB";
            this.clubDoubleAPlayerB.Size = new System.Drawing.Size(35, 13);
            this.clubDoubleAPlayerB.TabIndex = 66;
            this.clubDoubleAPlayerB.Text = "label7";
            // 
            // doubleAplayerBName
            // 
            this.doubleAplayerBName.FormattingEnabled = true;
            this.doubleAplayerBName.Location = new System.Drawing.Point(190, 21);
            this.doubleAplayerBName.Name = "doubleAplayerBName";
            this.doubleAplayerBName.Size = new System.Drawing.Size(106, 21);
            this.doubleAplayerBName.Sorted = true;
            this.doubleAplayerBName.TabIndex = 65;
            this.doubleAplayerBName.SelectedIndexChanged += new System.EventHandler(this.doubleAplayerBName_SelectedIndexChanged);
            // 
            // licDoubleBPlayerB
            // 
            this.licDoubleBPlayerB.AutoSize = true;
            this.licDoubleBPlayerB.Location = new System.Drawing.Point(46, 138);
            this.licDoubleBPlayerB.Name = "licDoubleBPlayerB";
            this.licDoubleBPlayerB.Size = new System.Drawing.Size(35, 13);
            this.licDoubleBPlayerB.TabIndex = 70;
            this.licDoubleBPlayerB.Text = "label7";
            // 
            // clubDoubleBPlayerB
            // 
            this.clubDoubleBPlayerB.AutoSize = true;
            this.clubDoubleBPlayerB.Location = new System.Drawing.Point(86, 140);
            this.clubDoubleBPlayerB.Name = "clubDoubleBPlayerB";
            this.clubDoubleBPlayerB.Size = new System.Drawing.Size(35, 13);
            this.clubDoubleBPlayerB.TabIndex = 69;
            this.clubDoubleBPlayerB.Text = "label7";
            // 
            // doubleBplayerBName
            // 
            this.doubleBplayerBName.FormattingEnabled = true;
            this.doubleBplayerBName.Location = new System.Drawing.Point(194, 98);
            this.doubleBplayerBName.Name = "doubleBplayerBName";
            this.doubleBplayerBName.Size = new System.Drawing.Size(106, 21);
            this.doubleBplayerBName.Sorted = true;
            this.doubleBplayerBName.TabIndex = 68;
            this.doubleBplayerBName.SelectedIndexChanged += new System.EventHandler(this.doubleBplayerBName_SelectedIndexChanged);
            // 
            // pointsDoubleAPlayerB
            // 
            this.pointsDoubleAPlayerB.AutoSize = true;
            this.pointsDoubleAPlayerB.Location = new System.Drawing.Point(364, 71);
            this.pointsDoubleAPlayerB.Name = "pointsDoubleAPlayerB";
            this.pointsDoubleAPlayerB.Size = new System.Drawing.Size(35, 13);
            this.pointsDoubleAPlayerB.TabIndex = 72;
            this.pointsDoubleAPlayerB.Text = "label7";
            // 
            // rankingDoubleAPlayerB
            // 
            this.rankingDoubleAPlayerB.AutoSize = true;
            this.rankingDoubleAPlayerB.Location = new System.Drawing.Point(310, 71);
            this.rankingDoubleAPlayerB.Name = "rankingDoubleAPlayerB";
            this.rankingDoubleAPlayerB.Size = new System.Drawing.Size(35, 13);
            this.rankingDoubleAPlayerB.TabIndex = 71;
            this.rankingDoubleAPlayerB.Text = "label7";
            // 
            // pointsDoubleBPlayerB
            // 
            this.pointsDoubleBPlayerB.AutoSize = true;
            this.pointsDoubleBPlayerB.Location = new System.Drawing.Point(364, 139);
            this.pointsDoubleBPlayerB.Name = "pointsDoubleBPlayerB";
            this.pointsDoubleBPlayerB.Size = new System.Drawing.Size(35, 13);
            this.pointsDoubleBPlayerB.TabIndex = 74;
            this.pointsDoubleBPlayerB.Text = "label7";
            // 
            // rankingDoubleBPlayerB
            // 
            this.rankingDoubleBPlayerB.AutoSize = true;
            this.rankingDoubleBPlayerB.Location = new System.Drawing.Point(310, 139);
            this.rankingDoubleBPlayerB.Name = "rankingDoubleBPlayerB";
            this.rankingDoubleBPlayerB.Size = new System.Drawing.Size(35, 13);
            this.rankingDoubleBPlayerB.TabIndex = 73;
            this.rankingDoubleBPlayerB.Text = "label7";
            // 
            // DoublesGame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pointsDoubleBPlayerB);
            this.Controls.Add(this.rankingDoubleBPlayerB);
            this.Controls.Add(this.pointsDoubleAPlayerB);
            this.Controls.Add(this.rankingDoubleAPlayerB);
            this.Controls.Add(this.licDoubleBPlayerB);
            this.Controls.Add(this.clubDoubleBPlayerB);
            this.Controls.Add(this.doubleBplayerBName);
            this.Controls.Add(this.licDoubleAPlayerB);
            this.Controls.Add(this.clubDoubleAPlayerB);
            this.Controls.Add(this.doubleAplayerBName);
            this.Controls.Add(this.gameInsertedLabel);
            this.Controls.Add(this.licDoubleBPlayerA);
            this.Controls.Add(this.licDoubleAPlayerA);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.pointsDoubleBPlayerA);
            this.Controls.Add(this.rankingDoubleBPlayerA);
            this.Controls.Add(this.pointsDoubleAPlayerA);
            this.Controls.Add(this.rankingDoubleAPlayerA);
            this.Controls.Add(this.clubDoubleBPlayerA);
            this.Controls.Add(this.clubDoubleAPlayerA);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.setDoubleB);
            this.Controls.Add(this.doubleBplayerAName);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.setDoubleA);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.doubleAplayerAName);
            this.Controls.Add(this.label4);
            this.Name = "DoublesGame";
            this.Size = new System.Drawing.Size(539, 156);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label licDoubleBPlayerA;
        private System.Windows.Forms.Label licDoubleAPlayerA;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Label pointsDoubleBPlayerA;
        private System.Windows.Forms.Label rankingDoubleBPlayerA;
        private System.Windows.Forms.Label pointsDoubleAPlayerA;
        private System.Windows.Forms.Label rankingDoubleAPlayerA;
        private System.Windows.Forms.Label clubDoubleBPlayerA;
        private System.Windows.Forms.Label clubDoubleAPlayerA;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox setDoubleB;
        private System.Windows.Forms.ComboBox doubleBplayerAName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox setDoubleA;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox doubleAplayerAName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label gameInsertedLabel;
        private System.Windows.Forms.Label licDoubleAPlayerB;
        private System.Windows.Forms.Label clubDoubleAPlayerB;
        private System.Windows.Forms.ComboBox doubleAplayerBName;
        private System.Windows.Forms.Label licDoubleBPlayerB;
        private System.Windows.Forms.Label clubDoubleBPlayerB;
        private System.Windows.Forms.ComboBox doubleBplayerBName;
        private System.Windows.Forms.Label pointsDoubleAPlayerB;
        private System.Windows.Forms.Label rankingDoubleAPlayerB;
        private System.Windows.Forms.Label pointsDoubleBPlayerB;
        private System.Windows.Forms.Label rankingDoubleBPlayerB;
    }
}
