﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TableTennis.Database;
using TableTennis.Controls.General;
using static TableTennis.Database.DataSet2;

namespace TableTennis.Controls
{
    public partial class AddToTeamGame : UserControl
    {
        private UserControl singlesGame;

        public AddToTeamGame()
        {
            InitializeComponent();
            singlesMatchRadiobutton.Checked = true;

            teamGameCombobox.Items.AddRange(FilesAccess.db.teamGames.Select(x => GetTeamGame(x)).ToArray());
        }

        private string GetTeamGame(teamgameRow x)
        {
            return "[Jornada " + x.Day + "]" +
                FilesAccess.db.championships.SingleOrDefault(a => a.Id == x.IdChampionship).Name + " " +
                FilesAccess.db.clubs.SingleOrDefault(a => a.Id == x.ClubAId).Name + " " + x.clubAnameAppend + " " + x.GetResult() +
                FilesAccess.db.clubs.SingleOrDefault(a => a.Id == x.ClubBId).Name + " " + x.clubBnameAppend;
        }
        
        public void createGameButton_Click(object sender, EventArgs e)
        {
            int selectedIndex = teamGameCombobox.SelectedIndex;
            var teamGame = FilesAccess.db.teamGames.SingleOrDefault(x => GetTeamGame(x) == teamGameCombobox.Text);

            if(teamGameCombobox.Text!="" && teamGame == null)
            {
                var dr = MessageBox.Show("Team game is filled but it is null! Team Game needs to be defined", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
                
                return;
            }
            (singlesGame as IAddGameToTeamgame).UpdateGame(teamGame.Id);
            
            teamGameCombobox.Items.Clear();
            teamGameCombobox.Items.AddRange(FilesAccess.db.teamGames.Select(
                x => GetTeamGame(x)).ToArray());

            teamGameCombobox.SelectedIndex = selectedIndex;
        }
        
        private void button10_Click(object sender, EventArgs e)
        {
            new AddTeamGameForm().ShowDialog();
            teamGameCombobox.Items.Clear();
            teamGameCombobox.Items.AddRange(FilesAccess.db.teamGames.Select(
                x => "[Jornada " + x.Day + " ]" +
                FilesAccess.db.championships.SingleOrDefault(a => a.Id == x.IdChampionship).Name + " " +
                FilesAccess.db.clubs.SingleOrDefault(a => a.Id == x.ClubAId).Name + " " + x.clubAnameAppend + " " + x.GetResult() +
                FilesAccess.db.clubs.SingleOrDefault(a => a.Id == x.ClubBId).Name + " " + x.clubBnameAppend).ToArray());
        }

        private void teamGameCombobox_SelectedIndexChanged(object sender, EventArgs e)
        {
            var teamGame = FilesAccess.db.teamGames.Single(x => GetTeamGame(x) == teamGameCombobox.Text);
            champTextbox.Text = FilesAccess.db.championships.Single(a => a.Id == teamGame.IdChampionship).Name;
            dateTextbox.Text = teamGame.Date.ToString("dd/MM/yyyy");

            (singlesGame as IAddGameToTeamgame)?.UpdateList(teamGame.IdChampionship, teamGame.Date);
        }

        private void singlesMatchRadiobutton_CheckedChanged(object sender, EventArgs e)
        {
            if (singlesMatchRadiobutton.Checked)
            {
                var teamGame = FilesAccess.db.teamGames.SingleOrDefault(x => GetTeamGame(x) == teamGameCombobox.Text);
                singlesGame = new ViewSinglesGame(teamGame!=null ? teamGame.IdChampionship:0);
                panel1.Controls.Clear();
                panel1.Controls.Add(singlesGame);
            }
        }

        private void doublesGameRadiobutton_CheckedChanged(object sender, EventArgs e)
        {
            if (doublesGameRadiobutton.Checked)
            {
                var teamGame = FilesAccess.db.teamGames.SingleOrDefault(x => GetTeamGame(x) == teamGameCombobox.Text);
                singlesGame = new ViewDoublesGame(teamGame != null ? teamGame.IdChampionship : 0);
                panel1.Controls.Clear();
                panel1.Controls.Add(singlesGame);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var teamGame = FilesAccess.db.teamGames.SingleOrDefault(x => GetTeamGame(x) == teamGameCombobox.Text);
            teamGame.Date = DateTime.ParseExact(dateTextbox.Text, "dd/MM/yyyy", null);
            FilesAccess.db.UpdateTeamGame(teamGame);
            
            (singlesGame as IAddGameToTeamgame)?.UpdateList(teamGame.IdChampionship, teamGame.Date);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (!Int32.TryParse(teamGameIdTxtbox.Text, out int teamGameId))
                return;

            var teamGames = FilesAccess.db.teamGames.Select(x => x.Id);

            if (!teamGames.Contains(teamGameId))
                return;

            var teamGame = FilesAccess.db.teamGames.Single(x => x.Id == teamGameId);

            var tg = GetTeamGame(teamGame);
            teamGameCombobox.SelectedItem = tg;
        }

        private void selectSinglesGameIdBtn_Click(object sender, EventArgs e)
        {
            var teamGame = FilesAccess.db.teamGames.SingleOrDefault(x => GetTeamGame(x) == teamGameCombobox.Text);
            singlesGame = new ViewSinglesGame(teamGame != null ? teamGame.IdChampionship : 0);
            panel1.Controls.Clear();
            panel1.Controls.Add(singlesGame);
        }
    }
}