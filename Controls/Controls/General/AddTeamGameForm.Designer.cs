﻿namespace TableTennis.Controls.General
{
    partial class AddTeamGameForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.addTeamGame1 = new TableTennis.Controls.General.AddTeamGame();
            this.SuspendLayout();
            // 
            // addTeamGame1
            // 
            this.addTeamGame1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.addTeamGame1.Location = new System.Drawing.Point(0, 0);
            this.addTeamGame1.Name = "addTeamGame1";
            this.addTeamGame1.Size = new System.Drawing.Size(584, 406);
            this.addTeamGame1.TabIndex = 0;
            // 
            // AddTeamGameForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 406);
            this.Controls.Add(this.addTeamGame1);
            this.Name = "AddTeamGameForm";
            this.Text = "AddTeamGameForm";
            this.ResumeLayout(false);

        }

        #endregion

        private AddTeamGame addTeamGame1;
    }
}