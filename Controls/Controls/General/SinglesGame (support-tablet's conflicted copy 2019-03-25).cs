﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using static TableTennis.Database.DataSet2;
using TableTennis.Database;

namespace TableTennis.Controls.General
{
    public partial class SinglesGame : UserControl, IInsertGame
    {
        public SinglesGame()
        {
            InitializeComponent();
            var allPlayers = FilesAccess.db.player.Select(x => x.Name).ToArray();
            playerAName.Items.AddRange(allPlayers);
            playerBName.Items.AddRange(allPlayers);
        }

        private void playerAName_SelectedIndexChanged(object sender, EventArgs e)
        {
            playerRow playerA = FilesAccess.db.player.Single(x => x.Name == playerAName.Text);
            clubPlayerALabel.Text = FilesAccess.db.clubs.Single(x => x.Id == playerA.IdClub).Name;
            pointsPlayerALabel.Text = playerA.currentPoints.ToString();
            rankingPlayerALabel.Text = playerA.currentRank.ToString();
            licPlayerA.Text = playerA.licenseNo;
        }

        private void playerBName_SelectedIndexChanged(object sender, EventArgs e)
        {
            playerRow playerB = FilesAccess.db.player.Single(x => x.Name == playerBName.Text);
            clubPlayerBLabel.Text = FilesAccess.db.clubs.Single(x => x.Id == playerB.IdClub).Name;
            pointsPlayerBLabel.Text = playerB.currentPoints.ToString();
            rankingPlayerBLabel.Text = playerB.currentRank.ToString();
            licPlayerB.Text = playerB.licenseNo;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            setsPlayerA.Text = "0";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            setsPlayerA.Text = "1";
        }

        private void button4_Click(object sender, EventArgs e)
        {
            setsPlayerA.Text = "2";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            setsPlayerA.Text = "3";
        }

        private void button8_Click(object sender, EventArgs e)
        {
            setsPlayerB.Text = "0";
        }

        private void button7_Click(object sender, EventArgs e)
        {
            setsPlayerB.Text = "1";
        }

        private void button6_Click(object sender, EventArgs e)
        {
            setsPlayerB.Text = "2";
        }

        private void button5_Click(object sender, EventArgs e)
        {
            setsPlayerB.Text = "3";
        }

        private void button9_Click(object sender, EventArgs e)
        {
            new AddPlayerForm().ShowDialog();
            UpdatePlayersCombobox();
        }

        public void UpdatePlayersCombobox()
        {
            string playerAaux = playerAName.Text;
            string playerBaux = playerBName.Text;

            playerAName.Items.Clear();
            playerBName.Items.Clear();
            //try
            //{
            //    championshipsRow chRow = FilesAccess.db.championships.Single(x => x.Name == champ);
            //    tournamenttypeRow ttRow = FilesAccess.db.tournamentType.Single(x => x.id == chRow.idTournamentType);

            //    if (ttRow.id == Utilities.OpenTournamentId)
            //    {
            //        List<playerleaguetournamentRow> champsRow = ((playerleaguetournamentRow[])FilesAccess.db.playerLeagueTournament.Select("championshipId=" + chRow.Id)).ToList();
            //        for (int i = 0; i < champsRow.Count; i++)
            //        {
            //            playerRow plRow = FilesAccess.db.player.Single(x => x.Id == champsRow[i].playerId);
            //            playerAName.Items.Add(plRow.Name);
            //            playerBName.Items.Add(plRow.Name);
            //        }
            //        return;
            //    }

            //}
            //catch { }

            for (int i = 0; i < FilesAccess.db.player.Count; i++)
            {
                playerAName.Items.Add(FilesAccess.db.player[i].Name);
                playerBName.Items.Add(FilesAccess.db.player[i].Name);
            }

            playerAName.Text = playerAaux;
            playerBName.Text = playerBaux;
        }

        public void UpdateInterface()
        {
            var players = Ranking.Instance.UpdateRanking(new DateTime(1970, 1, 1), DateTime.Now);
            playerRow playerA = players.Single(x => x.Name == playerAName.Text);
            playerRow playerB = players.Single(x => x.Name == playerBName.Text);

            pointsPlayerALabel.Text = playerA.currentPoints.ToString();
            rankingPlayerALabel.Text = playerA.currentRank.ToString();

            pointsPlayerBLabel.Text = playerB.currentPoints.ToString();
            rankingPlayerBLabel.Text = playerB.currentRank.ToString();

            gameInsertedLabel.Text = "Inserted "+ playerAName.Text+ " " + setsPlayerA.Text+ " - "+ setsPlayerB.Text+ " "+playerBName.Text;
        }
        
        public void CreateGame(DateTime date, string champ, string phase, int idTeamGame)
        {
            int playerAId = FilesAccess.db.player.Single(x => x.Name == playerAName.Text).Id;
            int playerBId = FilesAccess.db.player.Single(x => x.Name == playerBName.Text).Id;
            int phaseId = FilesAccess.db.phase.Single(x => x.id + "-" + x.phase == phase).id;

            int setsA = Convert.ToInt32(setsPlayerA.Text);
            int setsB = Convert.ToInt32(setsPlayerB.Text);

            if (FilesAccess.db.games.Count(
                    x => (((x.IdPlayerA == playerAId && x.IdPlayerB == playerBId) ||
                        (x.IdPlayerA == playerBId && x.IdPlayerB == playerAId)) &&
                        x.Date.ToShortDateString() == date.ToShortDateString())) > 0)
            {
                MessageBox.Show("Game Already Entered");
                return;
            }

            DataSet2.gamesRow rowInserted =
                FilesAccess.db.games.AddgamesRow(
                playerAId,
                playerBId,
                setsA,
                setsB,
                setsA > setsB ? playerAId:playerBId,
                date,
                FilesAccess.db.championships.Single(x => x.Name == champ).Id,
                0,
                0,
                0,
                phaseId,
                idTeamGame);

            //Ranking.Instance.GenerateRanking();
            FilesAccess.db.InsertGame(rowInserted);
           
            UpdateInterface();
        }
    }
}
