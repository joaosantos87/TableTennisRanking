﻿namespace TableTennis.Controls.General
{
    partial class ViewDoublesGame
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.licDoubleBPlayerA = new System.Windows.Forms.Label();
            this.licDoubleAPlayerA = new System.Windows.Forms.Label();
            this.pointsDoubleBPlayerA = new System.Windows.Forms.Label();
            this.rankingDoubleBPlayerA = new System.Windows.Forms.Label();
            this.pointsDoubleAPlayerA = new System.Windows.Forms.Label();
            this.rankingDoubleAPlayerA = new System.Windows.Forms.Label();
            this.clubDoubleBPlayerA = new System.Windows.Forms.Label();
            this.clubDoubleAPlayerA = new System.Windows.Forms.Label();
            this.setDoubleB = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.setDoubleA = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.gameInsertedLabel = new System.Windows.Forms.Label();
            this.licDoubleAPlayerB = new System.Windows.Forms.Label();
            this.clubDoubleAPlayerB = new System.Windows.Forms.Label();
            this.licDoubleBPlayerB = new System.Windows.Forms.Label();
            this.clubDoubleBPlayerB = new System.Windows.Forms.Label();
            this.pointsDoubleAPlayerB = new System.Windows.Forms.Label();
            this.rankingDoubleAPlayerB = new System.Windows.Forms.Label();
            this.pointsDoubleBPlayerB = new System.Windows.Forms.Label();
            this.rankingDoubleBPlayerB = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.doubleAPlayerANameTxt = new System.Windows.Forms.TextBox();
            this.doubleAPlayerBNameTxt = new System.Windows.Forms.TextBox();
            this.doubleBPlayerBNameTxt = new System.Windows.Forms.TextBox();
            this.doubleBPlayerANameTxt = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // licDoubleBPlayerA
            // 
            this.licDoubleBPlayerA.AutoSize = true;
            this.licDoubleBPlayerA.Location = new System.Drawing.Point(46, 123);
            this.licDoubleBPlayerA.Name = "licDoubleBPlayerA";
            this.licDoubleBPlayerA.Size = new System.Drawing.Size(35, 13);
            this.licDoubleBPlayerA.TabIndex = 63;
            this.licDoubleBPlayerA.Text = "label7";
            // 
            // licDoubleAPlayerA
            // 
            this.licDoubleAPlayerA.AutoSize = true;
            this.licDoubleAPlayerA.Location = new System.Drawing.Point(48, 59);
            this.licDoubleAPlayerA.Name = "licDoubleAPlayerA";
            this.licDoubleAPlayerA.Size = new System.Drawing.Size(35, 13);
            this.licDoubleAPlayerA.TabIndex = 62;
            this.licDoubleAPlayerA.Text = "label7";
            // 
            // pointsDoubleBPlayerA
            // 
            this.pointsDoubleBPlayerA.AutoSize = true;
            this.pointsDoubleBPlayerA.Location = new System.Drawing.Point(364, 123);
            this.pointsDoubleBPlayerA.Name = "pointsDoubleBPlayerA";
            this.pointsDoubleBPlayerA.Size = new System.Drawing.Size(35, 13);
            this.pointsDoubleBPlayerA.TabIndex = 60;
            this.pointsDoubleBPlayerA.Text = "label7";
            // 
            // rankingDoubleBPlayerA
            // 
            this.rankingDoubleBPlayerA.AutoSize = true;
            this.rankingDoubleBPlayerA.Location = new System.Drawing.Point(310, 123);
            this.rankingDoubleBPlayerA.Name = "rankingDoubleBPlayerA";
            this.rankingDoubleBPlayerA.Size = new System.Drawing.Size(35, 13);
            this.rankingDoubleBPlayerA.TabIndex = 59;
            this.rankingDoubleBPlayerA.Text = "label7";
            // 
            // pointsDoubleAPlayerA
            // 
            this.pointsDoubleAPlayerA.AutoSize = true;
            this.pointsDoubleAPlayerA.Location = new System.Drawing.Point(364, 59);
            this.pointsDoubleAPlayerA.Name = "pointsDoubleAPlayerA";
            this.pointsDoubleAPlayerA.Size = new System.Drawing.Size(35, 13);
            this.pointsDoubleAPlayerA.TabIndex = 58;
            this.pointsDoubleAPlayerA.Text = "label7";
            // 
            // rankingDoubleAPlayerA
            // 
            this.rankingDoubleAPlayerA.AutoSize = true;
            this.rankingDoubleAPlayerA.Location = new System.Drawing.Point(310, 57);
            this.rankingDoubleAPlayerA.Name = "rankingDoubleAPlayerA";
            this.rankingDoubleAPlayerA.Size = new System.Drawing.Size(35, 13);
            this.rankingDoubleAPlayerA.TabIndex = 57;
            this.rankingDoubleAPlayerA.Text = "label7";
            // 
            // clubDoubleBPlayerA
            // 
            this.clubDoubleBPlayerA.AutoSize = true;
            this.clubDoubleBPlayerA.Location = new System.Drawing.Point(88, 124);
            this.clubDoubleBPlayerA.Name = "clubDoubleBPlayerA";
            this.clubDoubleBPlayerA.Size = new System.Drawing.Size(35, 13);
            this.clubDoubleBPlayerA.TabIndex = 56;
            this.clubDoubleBPlayerA.Text = "label7";
            // 
            // clubDoubleAPlayerA
            // 
            this.clubDoubleAPlayerA.AutoSize = true;
            this.clubDoubleAPlayerA.Location = new System.Drawing.Point(90, 59);
            this.clubDoubleAPlayerA.Name = "clubDoubleAPlayerA";
            this.clubDoubleAPlayerA.Size = new System.Drawing.Size(35, 13);
            this.clubDoubleAPlayerA.TabIndex = 55;
            this.clubDoubleAPlayerA.Text = "label7";
            // 
            // setDoubleB
            // 
            this.setDoubleB.Location = new System.Drawing.Point(319, 98);
            this.setDoubleB.Name = "setDoubleB";
            this.setDoubleB.ReadOnly = true;
            this.setDoubleB.Size = new System.Drawing.Size(25, 20);
            this.setDoubleB.TabIndex = 46;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(4, 101);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(51, 13);
            this.label6.TabIndex = 44;
            this.label6.Text = "Double B";
            // 
            // setDoubleA
            // 
            this.setDoubleA.Location = new System.Drawing.Point(319, 30);
            this.setDoubleA.Name = "setDoubleA";
            this.setDoubleA.ReadOnly = true;
            this.setDoubleA.Size = new System.Drawing.Size(25, 20);
            this.setDoubleA.TabIndex = 43;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(4, 33);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 13);
            this.label4.TabIndex = 40;
            this.label4.Text = "Double A";
            // 
            // gameInsertedLabel
            // 
            this.gameInsertedLabel.AutoSize = true;
            this.gameInsertedLabel.Location = new System.Drawing.Point(36, 91);
            this.gameInsertedLabel.Name = "gameInsertedLabel";
            this.gameInsertedLabel.Size = new System.Drawing.Size(0, 13);
            this.gameInsertedLabel.TabIndex = 64;
            // 
            // licDoubleAPlayerB
            // 
            this.licDoubleAPlayerB.AutoSize = true;
            this.licDoubleAPlayerB.Location = new System.Drawing.Point(48, 81);
            this.licDoubleAPlayerB.Name = "licDoubleAPlayerB";
            this.licDoubleAPlayerB.Size = new System.Drawing.Size(35, 13);
            this.licDoubleAPlayerB.TabIndex = 67;
            this.licDoubleAPlayerB.Text = "label7";
            // 
            // clubDoubleAPlayerB
            // 
            this.clubDoubleAPlayerB.AutoSize = true;
            this.clubDoubleAPlayerB.Location = new System.Drawing.Point(90, 81);
            this.clubDoubleAPlayerB.Name = "clubDoubleAPlayerB";
            this.clubDoubleAPlayerB.Size = new System.Drawing.Size(35, 13);
            this.clubDoubleAPlayerB.TabIndex = 66;
            this.clubDoubleAPlayerB.Text = "label7";
            // 
            // licDoubleBPlayerB
            // 
            this.licDoubleBPlayerB.AutoSize = true;
            this.licDoubleBPlayerB.Location = new System.Drawing.Point(46, 138);
            this.licDoubleBPlayerB.Name = "licDoubleBPlayerB";
            this.licDoubleBPlayerB.Size = new System.Drawing.Size(35, 13);
            this.licDoubleBPlayerB.TabIndex = 70;
            this.licDoubleBPlayerB.Text = "label7";
            // 
            // clubDoubleBPlayerB
            // 
            this.clubDoubleBPlayerB.AutoSize = true;
            this.clubDoubleBPlayerB.Location = new System.Drawing.Point(86, 140);
            this.clubDoubleBPlayerB.Name = "clubDoubleBPlayerB";
            this.clubDoubleBPlayerB.Size = new System.Drawing.Size(35, 13);
            this.clubDoubleBPlayerB.TabIndex = 69;
            this.clubDoubleBPlayerB.Text = "label7";
            // 
            // pointsDoubleAPlayerB
            // 
            this.pointsDoubleAPlayerB.AutoSize = true;
            this.pointsDoubleAPlayerB.Location = new System.Drawing.Point(364, 80);
            this.pointsDoubleAPlayerB.Name = "pointsDoubleAPlayerB";
            this.pointsDoubleAPlayerB.Size = new System.Drawing.Size(35, 13);
            this.pointsDoubleAPlayerB.TabIndex = 72;
            this.pointsDoubleAPlayerB.Text = "label7";
            // 
            // rankingDoubleAPlayerB
            // 
            this.rankingDoubleAPlayerB.AutoSize = true;
            this.rankingDoubleAPlayerB.Location = new System.Drawing.Point(310, 80);
            this.rankingDoubleAPlayerB.Name = "rankingDoubleAPlayerB";
            this.rankingDoubleAPlayerB.Size = new System.Drawing.Size(35, 13);
            this.rankingDoubleAPlayerB.TabIndex = 71;
            this.rankingDoubleAPlayerB.Text = "label7";
            // 
            // pointsDoubleBPlayerB
            // 
            this.pointsDoubleBPlayerB.AutoSize = true;
            this.pointsDoubleBPlayerB.Location = new System.Drawing.Point(364, 139);
            this.pointsDoubleBPlayerB.Name = "pointsDoubleBPlayerB";
            this.pointsDoubleBPlayerB.Size = new System.Drawing.Size(35, 13);
            this.pointsDoubleBPlayerB.TabIndex = 74;
            this.pointsDoubleBPlayerB.Text = "label7";
            // 
            // rankingDoubleBPlayerB
            // 
            this.rankingDoubleBPlayerB.AutoSize = true;
            this.rankingDoubleBPlayerB.Location = new System.Drawing.Point(310, 139);
            this.rankingDoubleBPlayerB.Name = "rankingDoubleBPlayerB";
            this.rankingDoubleBPlayerB.Size = new System.Drawing.Size(35, 13);
            this.rankingDoubleBPlayerB.TabIndex = 73;
            this.rankingDoubleBPlayerB.Text = "label7";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(10, 5);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(389, 21);
            this.comboBox1.TabIndex = 75;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // doubleAPlayerANameTxt
            // 
            this.doubleAPlayerANameTxt.Location = new System.Drawing.Point(61, 32);
            this.doubleAPlayerANameTxt.Name = "doubleAPlayerANameTxt";
            this.doubleAPlayerANameTxt.ReadOnly = true;
            this.doubleAPlayerANameTxt.Size = new System.Drawing.Size(123, 20);
            this.doubleAPlayerANameTxt.TabIndex = 76;
            // 
            // doubleAPlayerBNameTxt
            // 
            this.doubleAPlayerBNameTxt.Location = new System.Drawing.Point(190, 32);
            this.doubleAPlayerBNameTxt.Name = "doubleAPlayerBNameTxt";
            this.doubleAPlayerBNameTxt.ReadOnly = true;
            this.doubleAPlayerBNameTxt.Size = new System.Drawing.Size(123, 20);
            this.doubleAPlayerBNameTxt.TabIndex = 77;
            // 
            // doubleBPlayerBNameTxt
            // 
            this.doubleBPlayerBNameTxt.Location = new System.Drawing.Point(190, 98);
            this.doubleBPlayerBNameTxt.Name = "doubleBPlayerBNameTxt";
            this.doubleBPlayerBNameTxt.ReadOnly = true;
            this.doubleBPlayerBNameTxt.Size = new System.Drawing.Size(123, 20);
            this.doubleBPlayerBNameTxt.TabIndex = 79;
            // 
            // doubleBPlayerANameTxt
            // 
            this.doubleBPlayerANameTxt.Location = new System.Drawing.Point(61, 98);
            this.doubleBPlayerANameTxt.Name = "doubleBPlayerANameTxt";
            this.doubleBPlayerANameTxt.ReadOnly = true;
            this.doubleBPlayerANameTxt.Size = new System.Drawing.Size(123, 20);
            this.doubleBPlayerANameTxt.TabIndex = 78;
            // 
            // ViewDoublesGame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.doubleBPlayerBNameTxt);
            this.Controls.Add(this.doubleBPlayerANameTxt);
            this.Controls.Add(this.doubleAPlayerBNameTxt);
            this.Controls.Add(this.doubleAPlayerANameTxt);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.pointsDoubleBPlayerB);
            this.Controls.Add(this.rankingDoubleBPlayerB);
            this.Controls.Add(this.pointsDoubleAPlayerB);
            this.Controls.Add(this.rankingDoubleAPlayerB);
            this.Controls.Add(this.licDoubleBPlayerB);
            this.Controls.Add(this.clubDoubleBPlayerB);
            this.Controls.Add(this.licDoubleAPlayerB);
            this.Controls.Add(this.clubDoubleAPlayerB);
            this.Controls.Add(this.gameInsertedLabel);
            this.Controls.Add(this.licDoubleBPlayerA);
            this.Controls.Add(this.licDoubleAPlayerA);
            this.Controls.Add(this.pointsDoubleBPlayerA);
            this.Controls.Add(this.rankingDoubleBPlayerA);
            this.Controls.Add(this.pointsDoubleAPlayerA);
            this.Controls.Add(this.rankingDoubleAPlayerA);
            this.Controls.Add(this.clubDoubleBPlayerA);
            this.Controls.Add(this.clubDoubleAPlayerA);
            this.Controls.Add(this.setDoubleB);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.setDoubleA);
            this.Controls.Add(this.label4);
            this.Name = "ViewDoublesGame";
            this.Size = new System.Drawing.Size(420, 156);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label licDoubleBPlayerA;
        private System.Windows.Forms.Label licDoubleAPlayerA;
        private System.Windows.Forms.Label pointsDoubleBPlayerA;
        private System.Windows.Forms.Label rankingDoubleBPlayerA;
        private System.Windows.Forms.Label pointsDoubleAPlayerA;
        private System.Windows.Forms.Label rankingDoubleAPlayerA;
        private System.Windows.Forms.Label clubDoubleBPlayerA;
        private System.Windows.Forms.Label clubDoubleAPlayerA;
        private System.Windows.Forms.TextBox setDoubleB;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox setDoubleA;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label gameInsertedLabel;
        private System.Windows.Forms.Label licDoubleAPlayerB;
        private System.Windows.Forms.Label clubDoubleAPlayerB;
        private System.Windows.Forms.Label licDoubleBPlayerB;
        private System.Windows.Forms.Label clubDoubleBPlayerB;
        private System.Windows.Forms.Label pointsDoubleAPlayerB;
        private System.Windows.Forms.Label rankingDoubleAPlayerB;
        private System.Windows.Forms.Label pointsDoubleBPlayerB;
        private System.Windows.Forms.Label rankingDoubleBPlayerB;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TextBox doubleAPlayerANameTxt;
        private System.Windows.Forms.TextBox doubleAPlayerBNameTxt;
        private System.Windows.Forms.TextBox doubleBPlayerBNameTxt;
        private System.Windows.Forms.TextBox doubleBPlayerANameTxt;
    }
}
