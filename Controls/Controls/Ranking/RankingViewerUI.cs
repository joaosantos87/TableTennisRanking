﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TableTennis.Database;
using TableTennis.Reports;
using static TableTennis.Database.DataSet2;
using System.Diagnostics;
using TableTennis.Database.DataSet2TableAdapters;
using System.Threading.Tasks;

namespace TableTennis.Controls
{
    public partial class RankingViewerUI : UserControl
    {
        DateTime startDate = new DateTime(2000, 10, 1);
        DateTime finalDate = DateTime.Now;
        List<playerRow> players;
        List<playerRow> filteredplayers;

        List<string> playerNames = new List<string>();

        public string CurrentStartDate
        {
            get { return startDate.ToString("yyyy-MM-dd"); }
        }

        public string CurrentEndDate
        {
            get { return finalDate.ToString("yyyy-MM-dd"); }
        }

        public void FilterPlayers(List<playerRow> filteredplayers)
        {
            this.filteredplayers = filteredplayers;
            button1_Click(this, new EventArgs());
        }

        public RankingViewerUI()
        {
            InitializeComponent();
            this.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            StartDateTextbox.TextChanged += StartDateTextbox_TextChanged;
            FinalDateTextbox.TextChanged += FinalDateTextbox_TextChanged;

            StartDateTextbox.DataBindings.Add("text", this, "CurrentStartDate");
            FinalDateTextbox.DataBindings.Add("text", this, "CurrentEndDate");

            dataGridView1.RowsAdded += dataGridView1_RowsAdded;
            VisibleChanged += RankingViewerUI_VisibleChanged;
            nameCombobox.SelectedValueChanged += nameCombobox_SelectedIndexChanged;
        }

        private void nameCombobox_SelectedIndexChanged(object sender, EventArgs e)
        {
            button1_Click(this, new EventArgs());
        }

        private void RankingViewerUI_VisibleChanged(object sender, EventArgs e)
        {
            return;
            if (Visible)
            {
                players = Database.Ranking.Instance.UpdateRanking(startDate, finalDate, null);
                
                //dataGridView1.DataSource = FilesAccess.db.player.OrderByDescending(x => x.currentRank).ToList();
                button1_Click(this, EventArgs.Empty);
                if (listBox1.Items.Count == 0)
                {
                    for (int i = 0; i < FilesAccess.db.championships.Count; i++)
                    {
                        listBox1.Items.Add(FilesAccess.db.championships[i].Name);
                    }
                }

                if (listBox2.Items.Count == 0)
                {
                    for (int i = 0; i < FilesAccess.db.associations.Count; i++)
                    {
                        listBox2.Items.Add(FilesAccess.db.associations[i].name);
                    }
                }

                if (nameCombobox.Items.Count == 0)
                {
                    playerNames = playerNames.OrderByDescending(x => x).ToList();
                    for (int i = 0; i < playerNames.Count; i++)
                    {
                        nameCombobox.Items.Add(playerNames[i]);
                    }
                }
                if (clubCombobox.Items.Count == 0)
                {
                    clubCombobox.Items.Add("All");
                    clubCombobox.Text = "All";
                    for (int i = 0; i < FilesAccess.db.clubs.Count; i++)
                    {
                        clubCombobox.Items.Add(FilesAccess.db.clubs[i].Name);
                    }
                }
            }
        }

        private void FinalDateTextbox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                finalDate = new DateTime(Convert.ToInt32(FinalDateTextbox.Text.Split('-')[0]), Convert.ToInt32(FinalDateTextbox.Text.Split('-')[1]), Convert.ToInt32(FinalDateTextbox.Text.Split('-')[2]));
            }
            catch { }
        }

        private void StartDateTextbox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                startDate = new DateTime(Convert.ToInt32(StartDateTextbox.Text.Split('-')[0]), Convert.ToInt32(StartDateTextbox.Text.Split('-')[1]), Convert.ToInt32(StartDateTextbox.Text.Split('-')[2]));
            }
            catch { }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var useSinglesGames = singleGamesCheckbox.Checked;
            var useDoublesGames = doubleGamesCheckbox.Checked;

            List<int> champ = new List<int>();
            try
            {
                for(int i=0; i<listBox1.SelectedItems.Count;i++)
                {
                    champ.Add(FilesAccess.db.championships.Single(x=>x.Name==listBox1.SelectedItems[i].ToString()).Id);
                }
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }

            List<int> associations = new List<int>();
            try
            {
                for (int i = 0; i < listBox2.SelectedItems.Count; i++)
                {
                    associations.Add(FilesAccess.db.associations.Single(x => x.name == listBox2.SelectedItems[i].ToString()).id);
                }
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }

            players = Database.Ranking.Instance.UpdateRanking(startDate, finalDate, champ, associations, useSinglesGames, useDoublesGames);
            string searchName = "";
            if (nameCombobox.Text != "")
            {
                searchName = "and name ='" + nameCombobox.Text+"'";
                dataGridView1.DataSource = players.Where(x => x.currentGamesWon+x.currentGamesLost > 0 && x.Name == nameCombobox.Text).OrderByDescending(a => a.currentPoints).ToList();
            }

            if (associations.Count > 0)
            {
                List<playerRow> players1 = new List<playerRow>();
                foreach (int assoId in associations)
                {
                    List<clubsRow> clubs = (((clubsRow[])FilesAccess.db.clubs.Select("AssociationId=" + assoId)).ToList());
                    foreach(clubsRow club in clubs)
                    {
                        players1.AddRange(players.Where(x => x.currentGamesWon + x.currentGamesLost > 0 && x.IdClub == club.Id).ToList());
                    }
                }
                dataGridView1.DataSource = players1.OrderByDescending(x => x.currentPoints).ToList();
            }
            else if (clubCombobox.Text != "All")
            {
                try
                {
                    int idClub = FilesAccess.db.clubs.Single(x => x.Name == clubCombobox.Text).Id;
                    dataGridView1.DataSource = players.Where(x => x.IdClub == idClub).OrderByDescending(x => x.currentPoints).ToList();
                }
                catch { }
            }
            else if(nameCombobox.Text!="")
            {
                dataGridView1.DataSource = players.Where(x => x.currentGamesWon + x.currentGamesLost > 0 && x.Name == nameCombobox.Text).OrderByDescending(x => x.currentPoints).ToList();
            }
            else
            {
                dataGridView1.DataSource = players.Where(x => x.currentGamesWon + x.currentGamesLost > 0).OrderByDescending(x => x.currentPoints).ToList();
            }

            if (filteredplayers != null)
            {
                dataGridView1.DataSource = filteredplayers.OrderByDescending(x => x.currentPoints).ToArray();
            }
            //dataGridView1.DataSource = players.OrderByDescending(x => x.currentPoints).ToArray();
        }

        /// <summary>
        /// Add the Ranking place and club
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView1_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            try
            {
                if (e.RowCount < 1)
                    return;

                for (int i = e.RowIndex; i < e.RowCount+e.RowIndex; i++)
                {

                    //dataGridView1.Rows[i].Cells[dataGridView1.Columns["Ranking"].Index].Value = (i+1).ToString();

                    string namePl = dataGridView1.Rows[i].Cells["NamePlayer"].Value?.ToString();
                    if (namePl == null)
                        continue;

                    var pl = FilesAccess.db.player.FirstOrDefault(x => x.Name == namePl);

                    if (pl == null)
                        continue;

                    if (pl.nameAutomaticInsertion != "")
                        dataGridView1.Rows[i].Cells["NamePlayer"].Value = pl.nameAutomaticInsertion;

                    dataGridView1.Rows[i].Cells[dataGridView1.Columns["Ranking"].Index].Value = pl.currentRank;
                    int idclub = Convert.ToInt32(dataGridView1.Rows[i].Cells[dataGridView1.Columns["IdClub"].Index].Value);
                    clubsRow clubRow = FilesAccess.db.clubs.Single(x => x.Id == idclub);
                    string clubAssociation = "N/A";

                    if(clubRow.AssociationId>0)
                        clubAssociation = FilesAccess.db.associations.Single(x => x.id == clubRow.AssociationId).name;
                    
                    dataGridView1.Rows[i].Cells[dataGridView1.Columns["Club"].Index].Value = clubRow.nameAutomaticInsertion != String.Empty ? clubRow.nameAutomaticInsertion : clubRow.Name;
                    dataGridView1.Rows[i].Cells[dataGridView1.Columns["Association"].Index].Value = clubAssociation;

                    double points = (Convert.ToDouble(dataGridView1.Rows[i].Cells[dataGridView1.Columns["Points"].Index].Value) - 1000);
                    double games = pl.currentGamesLost + pl.currentGamesWon;
                    dataGridView1.Rows[i].Cells[dataGridView1.Columns["WonLost"].Index].Value = pl.currentGamesWon +"-"+ pl.currentGamesLost;

                    string ppg = (points / games).ToString("0.00");
                    dataGridView1.Rows[i].Cells[dataGridView1.Columns["PPG"].Index].Value = ppg;
                    //nameCombobox.Items.Add(dataGridView1.Rows[i].Cells[dataGridView1.Columns["NamePlayer"].Index].Value);
                    playerNames.Add(namePl);
                }

                if (nameCombobox.Items.Count == 0 && playerNames.Count > 1)
                {
                    playerNames.Add("");
                    playerNames = playerNames.OrderByDescending(x => x).ToList();
                    nameCombobox.Items.AddRange(playerNames.ToArray());
                }
            }
            catch { }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                int actualYear = Convert.ToInt32(textBox1.Text);
                actualYear--;
                textBox1.Text = actualYear.ToString();
            }
            catch { }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                int actualYear = Convert.ToInt32(textBox1.Text);
                actualYear++;
                textBox1.Text = actualYear.ToString();
            }
            catch { }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            try
            {
                StartDateTextbox.Text = textBox1.Text + "-07-01";
                FinalDateTextbox.Text = Convert.ToInt32(textBox1.Text) + 1 + "-06-30";
                button1_Click(this, new EventArgs());
            }
            catch { }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            button1_Click(this, new EventArgs());
        }

        private void button4_Click(object sender, EventArgs e)
        {
            List<RankPlayer> playerRanks = new List<RankPlayer>();

            string title = "";

            title = StartDateTextbox.Text + "-" + FinalDateTextbox.Text+"   ";

            for (int i = 0; i < listBox1.SelectedItems.Count; i++)
            {
                if(i==listBox1.SelectedItems.Count-1)
                    title += " " + listBox1.SelectedItems[i];
                else
                    title += " " + listBox1.SelectedItems[i] + "&";
            }
            int newplace = 1;
            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                string clubAssociation = "N/A";
                try
                {
                    clubAssociation = FilesAccess.db.associations.Single(x => x.id == FilesAccess.db.clubs.Single(y => y.Name == dataGridView1["Club", i].Value).AssociationId).name;
                }
                catch { }
                if (listBox2.SelectedItems.Contains(clubAssociation) || listBox2.SelectedItems.Count == 0)
                {
                    var player = players.Single(x => x.Name == dataGridView1["NamePlayer", i].Value);
                    playerRanks.Add(new RankPlayer(dataGridView1["Ranking", i].Value, newplace++, dataGridView1["licenseNo", i].Value, dataGridView1["NamePlayer", i].Value, dataGridView1["Club", i].Value, clubAssociation, dataGridView1["Points", i].Value, player.currentGamesWon, player.currentGamesLost));
                }
            }

            if (textBox2.Text != "")
                title = textBox2.Text;

            //new PdfRanking(title, playerRanks);
            new PdfRanking(title, playerRanks, Database.Ranking.Instance.championshipsIncluded);
        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            button1_Click(this, new EventArgs());
        }

        private void button5_Click(object sender, EventArgs e)
        {
            FilesAccess.db.UpdatePlayer();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Stopwatch s1 = Stopwatch.StartNew();
            var seasons = FilesAccess.db.seasons.OrderBy(x => x.endDate);

            var championships =
                from c in FilesAccess.db.championships
                from tt in FilesAccess.db.tournamentType
                where c.idTournamentType == tt.id && tt.championship == true && tt.id != Utilities.OpenTournamentId
                select new
                {
                    Id = c.Id,
                    Name = c.Name
                };

            //generate a ranking that has all the champs and all the seasons and that is used to fill the points for each game
            Database.Ranking.Instance.UpdateAllGamesPoints();

            var generatedRankings = new List<Tuple<List<playerRow>, championshipsRow, seasonRow>>();
            Task.Factory.StartNew(() =>
            {
                foreach (var champ in championships)
                {
                    var c = FilesAccess.db.championships.Single(x => x.Id == champ.Id);
                    foreach (var season in seasons)
                    {
                        var champId = champ.Name == "TodosCampeonatos" ?
                                FilesAccess.db.championships.Where(x => x.idTournamentType != Utilities.OpenTournamentId).Select(x => x.Id).ToList() :
                                new List<int>() { champ.Id };
                        Trace.TraceInformation($"[{DateTime.Now.ToLongTimeString()}] Generating ranking for {champ.Name} season {season.name}...");
                        var generatedRank = Database.Ranking.Instance.GenerateRanking(season.startDate, season.endDate, champId).OrderBy(x => x.currentRank).ToList();

                        Trace.TraceInformation($"[{DateTime.Now.ToLongTimeString()}] done");

                        var singleGamesTeamGames = Database.Ranking.Instance.SinglesGames.OrderByDescending(a => a.Date).Where(x => x.IdTeamGame != 0 && x.IdTeamGame > -1);
                        var doubleGamesTeamGames = Database.Ranking.Instance.DoublesGames.OrderByDescending(a => a.Date).Where(x => x.IdTeamGame != 0 && x.IdTeamGame > -1);
                        int clubIdNA = FilesAccess.db.clubs.Single(x => x.Name == "Nao Disponivel").Id;

                        //int count = generatedRankings.Count;
                        int i = 0; 
                        int updatedRank = 0;
                        int sameRank = 0;
                        int newRank = 0;

                        Trace.TraceInformation($"[{DateTime.Now.ToLongTimeString()}] Started analyzing generated ranking for {champ.Name} season {season.name}...");
                        foreach (var r in generatedRank)
                        {
                            int clubId = clubIdNA;

                            //get the player's team on that season
                            var singlesGame = singleGamesTeamGames.FirstOrDefault(x => x.IdPlayerA == r.Id || x.IdPlayerB == r.Id);

                            if (singlesGame != null)
                            {
                                clubId = r.Id == singlesGame.IdPlayerA ? FilesAccess.db.teamGames.Single(x => x.Id == singlesGame.IdTeamGame).ClubAId :
    FilesAccess.db.teamGames.Single(x => x.Id == singlesGame.IdTeamGame).ClubBId;
                            }
                            else
                            {
                                var doublesGame = doubleGamesTeamGames.FirstOrDefault(x => (x.DoubleA_PlayerAId == r.Id || x.DoubleA_PlayerBId == r.Id || x.DoubleB_PlayerAId == r.Id || x.DoubleB_PlayerBId == r.Id));
                                if (doublesGame != null)
                                {
                                    clubId = (r.Id == doublesGame.DoubleA_PlayerAId || r.Id == doublesGame.DoubleA_PlayerBId) ? FilesAccess.db.teamGames.Single(x => x.Id == doublesGame.IdTeamGame).ClubAId : FilesAccess.db.teamGames.Single(x => x.Id == doublesGame.IdTeamGame).ClubBId;
                                }
                            }
                        retry:
                            var rankPlayers = FilesAccess.db.ranking.Where(x => x.idPlayer == r.Id && x.idSeason == season.id && x.idChamp == champ.Id).ToList();

                            if (rankPlayers.Count == 1)
                            {
                                //update the data
                                var rp = rankPlayers[0];

                                if (!PlayerRankingComparisonEqual(rp, r, clubId))
                                {
                                    //Console.WriteLine($"Player {rankPlayers.First().idPlayer} ranking data will be updated");
                                    rp.numGames = r.currentGamesLost + r.currentGamesWon;
                                    rp.numWins = r.currentGamesWon;
                                    rp.numLosses = r.currentGamesLost;
                                    rp.rank = r.currentRank;
                                    rp.points = r.currentPoints;
                                    rp.idClub = clubId;
                                
                                    new rankingTableAdapter().Update(rp);
                                    updatedRank++;
                                }
                                else
                                {
                                    sameRank++;
                                    //Console.WriteLine($"Player {rankPlayers.First().idPlayer} ranking data will not be updated");
                                }
                                //FilesAccess.db.UpdateRanking(rp);
                            }
                            else if (rankPlayers.Count == 0)
                            {
                                //var rankRow = 
                                //   FilesAccess.db.ranking.AddrankingRow(
                                //       r.Id,
                                //       champ.Id,
                                //       season.id,
                                //       r.currentGamesLost + r.currentGamesWon,
                                //       r.currentGamesWon,
                                //       r.currentGamesLost,
                                //       r.currentRank,
                                //       r.currentPoints,
                                //       clubId);
                                //Console.WriteLine($"Player {r.Id} ranking data will be created");
                                new rankingTableAdapter().Insert(r.Id,
                                        champ.Id,
                                        season.id,
                                        r.currentGamesLost + r.currentGamesWon,
                                        r.currentGamesWon,
                                        r.currentGamesLost,
                                        r.currentRank,
                                        r.currentPoints,
                                        clubId);
                                newRank++;
                                //FilesAccess.db.InsertRanking(rankRow);
                            }
                            else
                            {
                                Console.WriteLine($"[{DateTime.Now.ToLongTimeString()}]Player {rankPlayers.First().idPlayer} ranking data found {rankPlayers.Count} times. Data will be deleted!");
                                foreach (var rank in rankPlayers)
                                {
                                    FilesAccess.db.DeleteRanking(rank);
                                }
                                goto retry;
                            }

                            if (champ.Id == Utilities.TodosCampeonatos.Id && season.id == Utilities.TodasEpocas.id)
                            {
                                //Update the data on the main player table
                                var player = FilesAccess.db.player.Single(x => x.Id == r.Id);
                                player.IdClub = r.IdClub;
                                player.currentPoints = r.currentPoints;
                                player.currentRank = r.currentRank;
                                player.currentGamesWon = r.currentGamesWon;
                                player.currentGamesLost = r.currentGamesLost;
                                FilesAccess.db.UpdatePlayer();
                            }
                        }

                        Trace.TraceInformation($"[{DateTime.Now.ToLongTimeString()}] Done {i}. New: {newRank} Updated: {updatedRank} NotChanged: {sameRank} done");
                        i++;
                    }
                }

                new rankingTableAdapter().Fill(FilesAccess.db.ranking);
                //update ranking datatable
                //FilesAccess.db.UpdateRanking();
                s1.Stop();
                Console.WriteLine($"Time to create the ranking: {s1.Elapsed.TotalMinutes}:{s1.Elapsed.TotalSeconds}");
            });
        }

        private bool PlayerRankingComparisonEqual(rankingRow r1, playerRow p1, int p1IdClub)
        {
            if (r1.numGames != p1.currentGamesLost + p1.currentGamesWon)
                return false;

            if (r1.numWins != p1.currentGamesWon)
                return false;

            if (r1.numLosses != p1.currentGamesLost)
                return false;

            if (r1.rank != p1.currentRank)
                return false;

            if (r1.points != p1.currentPoints)
                return false;

            if (r1.idClub != p1IdClub)
                return false;

            return true;
        }

        private void deleteRankingButton_Click(object sender, EventArgs e)
        {
            FilesAccess.db.DeleteRanking();
        }

        private void singleGamesCheckbox_CheckedChanged(object sender, EventArgs e)
        {

        }
    }
}
