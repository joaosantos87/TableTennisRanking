﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TableTennis.Database;
using static TableTennis.Database.DataSet2;

namespace TableTennis.Controls
{
    public partial class ListPlayersLicence : UserControl
    {
        public ListPlayersLicence()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            List<playerRow> players = new List<playerRow>();
            foreach (string aux in textBox1.Lines)
            {
                try
                {
                    players.Add(FilesAccess.db.player.Single(x => x.licenseNo == aux));
                }
                catch (Exception)
                {

                }
            }
            rankingViewerUI1.FilterPlayers(players);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            rankingViewerUI1.FilterPlayers(null);
        }

        private void searchBtn_Click(object sender, EventArgs e)
        {
            var selectPlayerUserControl = new SelectPlayer();
            var genericForm = new GenericForm(selectPlayerUserControl);

            genericForm.ShowDialog();

            foreach(var p in selectPlayerUserControl.selectedPlayers)
                textBox1.Text += $"{ Environment.NewLine}{p.licenseNo}";
        }
    }
}
