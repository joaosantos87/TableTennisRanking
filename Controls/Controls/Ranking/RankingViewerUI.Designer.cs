﻿namespace TableTennis.Controls
{
    partial class RankingViewerUI
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Ranking = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.licenseNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IdClub = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NamePlayer = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Club = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Association = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WonLost = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Points = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PPG = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.playerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet2 = new TableTennis.Database.DataSet2();
            this.StartDateTextbox = new System.Windows.Forms.MaskedTextBox();
            this.FinalDateTextbox = new System.Windows.Forms.MaskedTextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.playerTableAdapter = new TableTennis.Database.DataSet2TableAdapters.playerTableAdapter();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.label3 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button4 = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.listBox2 = new System.Windows.Forms.ListBox();
            this.nameCombobox = new System.Windows.Forms.ComboBox();
            this.button5 = new System.Windows.Forms.Button();
            this.clubCombobox = new System.Windows.Forms.ComboBox();
            this.button6 = new System.Windows.Forms.Button();
            this.deleteRankingButton = new System.Windows.Forms.Button();
            this.singleGamesCheckbox = new System.Windows.Forms.CheckBox();
            this.doubleGamesCheckbox = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet2)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Ranking,
            this.licenseNo,
            this.IdClub,
            this.NamePlayer,
            this.Club,
            this.Association,
            this.WonLost,
            this.Points,
            this.PPG});
            this.dataGridView1.DataSource = this.playerBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(18, 149);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.Size = new System.Drawing.Size(542, 221);
            this.dataGridView1.TabIndex = 1;
            // 
            // Ranking
            // 
            this.Ranking.HeaderText = "Ranking";
            this.Ranking.Name = "Ranking";
            this.Ranking.Width = 30;
            // 
            // licenseNo
            // 
            this.licenseNo.DataPropertyName = "licenseNo";
            this.licenseNo.HeaderText = "licenseNo";
            this.licenseNo.Name = "licenseNo";
            this.licenseNo.Width = 50;
            // 
            // IdClub
            // 
            this.IdClub.DataPropertyName = "IdClub";
            this.IdClub.HeaderText = "IdClub";
            this.IdClub.Name = "IdClub";
            this.IdClub.Visible = false;
            // 
            // NamePlayer
            // 
            this.NamePlayer.DataPropertyName = "Name";
            this.NamePlayer.HeaderText = "Name";
            this.NamePlayer.Name = "NamePlayer";
            this.NamePlayer.Width = 150;
            // 
            // Club
            // 
            this.Club.HeaderText = "Club";
            this.Club.Name = "Club";
            this.Club.Width = 150;
            // 
            // Association
            // 
            this.Association.HeaderText = "Association";
            this.Association.Name = "Association";
            this.Association.Width = 70;
            // 
            // WonLost
            // 
            this.WonLost.HeaderText = "Won-Lost";
            this.WonLost.Name = "WonLost";
            // 
            // Points
            // 
            this.Points.DataPropertyName = "currentPoints";
            this.Points.HeaderText = "Points";
            this.Points.Name = "Points";
            this.Points.Width = 70;
            // 
            // PPG
            // 
            this.PPG.HeaderText = "PPG";
            this.PPG.Name = "PPG";
            // 
            // playerBindingSource
            // 
            this.playerBindingSource.DataMember = "player";
            this.playerBindingSource.DataSource = this.dataSet2;
            // 
            // dataSet2
            // 
            this.dataSet2.DataSetName = "DataSet1";
            this.dataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // StartDateTextbox
            // 
            this.StartDateTextbox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.StartDateTextbox.Location = new System.Drawing.Point(37, 38);
            this.StartDateTextbox.Mask = "0000-00-00";
            this.StartDateTextbox.Name = "StartDateTextbox";
            this.StartDateTextbox.Size = new System.Drawing.Size(73, 20);
            this.StartDateTextbox.TabIndex = 2;
            this.StartDateTextbox.Text = "20150107";
            // 
            // FinalDateTextbox
            // 
            this.FinalDateTextbox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.FinalDateTextbox.Location = new System.Drawing.Point(113, 38);
            this.FinalDateTextbox.Mask = "0000-00-00";
            this.FinalDateTextbox.Name = "FinalDateTextbox";
            this.FinalDateTextbox.Size = new System.Drawing.Size(73, 20);
            this.FinalDateTextbox.TabIndex = 3;
            this.FinalDateTextbox.Text = "20160630";
            // 
            // button1
            // 
            this.button1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.button1.Location = new System.Drawing.Point(419, 27);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(47, 95);
            this.button1.TabIndex = 4;
            this.button1.Text = "Filter";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // playerTableAdapter
            // 
            this.playerTableAdapter.ClearBeforeFill = true;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(38, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Data Inicio";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(125, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Data Fim";
            // 
            // listBox1
            // 
            this.listBox1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(191, 27);
            this.listBox1.Name = "listBox1";
            this.listBox1.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.listBox1.Size = new System.Drawing.Size(222, 43);
            this.listBox1.TabIndex = 8;
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(311, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Championships";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(125, 68);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(27, 23);
            this.button2.TabIndex = 10;
            this.button2.Text = "-";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(158, 68);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(27, 23);
            this.button3.TabIndex = 11;
            this.button3.Text = "+";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(25, 72);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Epoca";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(77, 69);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(32, 20);
            this.textBox1.TabIndex = 13;
            this.textBox1.Text = "2015";
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // button4
            // 
            this.button4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.button4.Location = new System.Drawing.Point(472, 79);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(88, 27);
            this.button4.TabIndex = 14;
            this.button4.Text = "Export";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // textBox2
            // 
            this.textBox2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textBox2.Location = new System.Drawing.Point(472, 38);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(88, 32);
            this.textBox2.TabIndex = 15;
            // 
            // listBox2
            // 
            this.listBox2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.listBox2.FormattingEnabled = true;
            this.listBox2.Location = new System.Drawing.Point(191, 79);
            this.listBox2.Name = "listBox2";
            this.listBox2.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.listBox2.Size = new System.Drawing.Size(222, 43);
            this.listBox2.TabIndex = 16;
            this.listBox2.SelectedIndexChanged += new System.EventHandler(this.listBox2_SelectedIndexChanged);
            // 
            // nameCombobox
            // 
            this.nameCombobox.FormattingEnabled = true;
            this.nameCombobox.Location = new System.Drawing.Point(261, 125);
            this.nameCombobox.Name = "nameCombobox";
            this.nameCombobox.Size = new System.Drawing.Size(222, 21);
            this.nameCombobox.TabIndex = 17;
            // 
            // button5
            // 
            this.button5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button5.Location = new System.Drawing.Point(18, 376);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 18;
            this.button5.Text = "Update DB";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // clubCombobox
            // 
            this.clubCombobox.FormattingEnabled = true;
            this.clubCombobox.Location = new System.Drawing.Point(18, 125);
            this.clubCombobox.Name = "clubCombobox";
            this.clubCombobox.Size = new System.Drawing.Size(222, 21);
            this.clubCombobox.TabIndex = 19;
            // 
            // button6
            // 
            this.button6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button6.Location = new System.Drawing.Point(143, 376);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(104, 23);
            this.button6.TabIndex = 20;
            this.button6.Text = "Update Ranking";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // deleteRankingButton
            // 
            this.deleteRankingButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.deleteRankingButton.Location = new System.Drawing.Point(456, 376);
            this.deleteRankingButton.Name = "deleteRankingButton";
            this.deleteRankingButton.Size = new System.Drawing.Size(104, 23);
            this.deleteRankingButton.TabIndex = 21;
            this.deleteRankingButton.Text = "Delete Ranking";
            this.deleteRankingButton.UseVisualStyleBackColor = true;
            this.deleteRankingButton.Click += new System.EventHandler(this.deleteRankingButton_Click);
            // 
            // singleGamesCheckbox
            // 
            this.singleGamesCheckbox.AutoSize = true;
            this.singleGamesCheckbox.Checked = true;
            this.singleGamesCheckbox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.singleGamesCheckbox.Location = new System.Drawing.Point(21, 101);
            this.singleGamesCheckbox.Name = "singleGamesCheckbox";
            this.singleGamesCheckbox.Size = new System.Drawing.Size(60, 17);
            this.singleGamesCheckbox.TabIndex = 22;
            this.singleGamesCheckbox.Text = "Singles";
            this.singleGamesCheckbox.UseVisualStyleBackColor = true;
            this.singleGamesCheckbox.CheckedChanged += new System.EventHandler(this.singleGamesCheckbox_CheckedChanged);
            // 
            // doubleGamesCheckbox
            // 
            this.doubleGamesCheckbox.AutoSize = true;
            this.doubleGamesCheckbox.Checked = true;
            this.doubleGamesCheckbox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.doubleGamesCheckbox.Location = new System.Drawing.Point(104, 101);
            this.doubleGamesCheckbox.Name = "doubleGamesCheckbox";
            this.doubleGamesCheckbox.Size = new System.Drawing.Size(65, 17);
            this.doubleGamesCheckbox.TabIndex = 23;
            this.doubleGamesCheckbox.Text = "Doubles";
            this.doubleGamesCheckbox.UseVisualStyleBackColor = true;
            // 
            // RankingViewerUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.doubleGamesCheckbox);
            this.Controls.Add(this.singleGamesCheckbox);
            this.Controls.Add(this.deleteRankingButton);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.clubCombobox);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.nameCombobox);
            this.Controls.Add(this.listBox2);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.FinalDateTextbox);
            this.Controls.Add(this.StartDateTextbox);
            this.Controls.Add(this.dataGridView1);
            this.Name = "RankingViewerUI";
            this.Size = new System.Drawing.Size(575, 402);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.MaskedTextBox StartDateTextbox;
        private System.Windows.Forms.MaskedTextBox FinalDateTextbox;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.BindingSource playerBindingSource;
        private TableTennis.Database.DataSet2 dataSet2;
        private TableTennis.Database.DataSet2TableAdapters.playerTableAdapter playerTableAdapter;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.ListBox listBox2;
        private System.Windows.Forms.ComboBox nameCombobox;
        private System.Windows.Forms.DataGridViewTextBoxColumn Games;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Ranking;
        private System.Windows.Forms.DataGridViewTextBoxColumn licenseNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdClub;
        private System.Windows.Forms.DataGridViewTextBoxColumn NamePlayer;
        private System.Windows.Forms.DataGridViewTextBoxColumn Club;
        private System.Windows.Forms.DataGridViewTextBoxColumn Association;
        private System.Windows.Forms.DataGridViewTextBoxColumn WonLost;
        private System.Windows.Forms.DataGridViewTextBoxColumn Points;
        private System.Windows.Forms.DataGridViewTextBoxColumn PPG;
        private System.Windows.Forms.ComboBox clubCombobox;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button deleteRankingButton;
        private System.Windows.Forms.CheckBox singleGamesCheckbox;
        private System.Windows.Forms.CheckBox doubleGamesCheckbox;
    }
}
