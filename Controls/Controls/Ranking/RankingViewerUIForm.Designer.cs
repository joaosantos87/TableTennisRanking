﻿namespace TableTennis.Controls
{
    partial class RankingViewerUIForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rankingViewerUI1 = new TableTennis.Controls.RankingViewerUI();
            this.SuspendLayout();
            // 
            // rankingViewerUI1
            // 
            this.rankingViewerUI1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rankingViewerUI1.Location = new System.Drawing.Point(12, 2);
            this.rankingViewerUI1.Name = "rankingViewerUI1";
            this.rankingViewerUI1.Size = new System.Drawing.Size(728, 470);
            this.rankingViewerUI1.TabIndex = 0;
            // 
            // RankingViewerUIForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(803, 518);
            this.Controls.Add(this.rankingViewerUI1);
            this.Name = "RankingViewerUIForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "RankingViewerUIForm";
            this.ResumeLayout(false);

        }

        #endregion

        private RankingViewerUI rankingViewerUI1;

    }
}