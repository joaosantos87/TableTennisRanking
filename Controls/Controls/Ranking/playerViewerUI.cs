﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TableTennis.Database;
using TableTennis.Reports;
using static TableTennis.Database.DataSet2;

namespace TableTennis.Controls
{
    public partial class PlayerViewerUI : UserControl
    {
        DateTime startDate = new DateTime(2000, 10, 1);
        DateTime finalDate = DateTime.Now;
        playerRow player;
        int gamesCount = 0;
        int gameWon = 0;

        public string CurrentStartDate
        {
            get { return startDate.ToString("yyyy-MM-dd"); }
        }

        public string CurrentEndDate
        {
            get { return finalDate.ToString("yyyy-MM-dd"); }
        }

        public playerRow OppVS
        {
            get;
            set;
        }

        public PlayerViewerUI()
        {
            InitializeComponent();
            this.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            StartDateTextbox.TextChanged += StartDateTextbox_TextChanged;
            FinalDateTextbox.TextChanged += FinalDateTextbox_TextChanged;
            StartDateTextbox.DataBindings.Add("text", this, "CurrentStartDate");
            FinalDateTextbox.DataBindings.Add("text", this, "CurrentEndDate");

            VisibleChanged += RankingViewerUI_VisibleChanged;
        }


        private void RankingViewerUI_VisibleChanged(object sender, EventArgs e)
        {
            if (Visible)
            {
                comboBox1.Items.Clear();
                comboBox2.Items.Clear();
                comboBox2.Items.Add("All");
                comboBox2.SelectedIndex = 0;

                for (int i = 0; i < FilesAccess.db.player.Count; i++)
                {
                    comboBox1.Items.Add(FilesAccess.db.player[i].Name);
                    comboBox2.Items.Add(FilesAccess.db.player[i].Name);
                }

                comboBox1.Sorted = true;
                comboBox2.Sorted = true;

                if (listBox1.Items.Count == 0)
                {
                    for (int i = 0; i < FilesAccess.db.championships.Count; i++)
                    {
                        listBox1.Items.Add(FilesAccess.db.championships[i].Name);
                    }
                }

                if (comboBox1.Items.Count > 0)
                {
                    comboBox1.SelectedIndex = 0;
                }
            }
        }

        private void FinalDateTextbox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                finalDate = new DateTime(Convert.ToInt32(FinalDateTextbox.Text.Split('-')[0]), Convert.ToInt32(FinalDateTextbox.Text.Split('-')[1]), Convert.ToInt32(FinalDateTextbox.Text.Split('-')[2]));
            }
            catch { }
        }

        private void StartDateTextbox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                startDate = new DateTime(Convert.ToInt32(StartDateTextbox.Text.Split('-')[0]), Convert.ToInt32(StartDateTextbox.Text.Split('-')[1]), Convert.ToInt32(StartDateTextbox.Text.Split('-')[2]));
            }
            catch { }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            gamesCount = 0;
            gameWon = 0;
            List<int> champ = new List<int>();
            try
            {
                for(int i=0; i<listBox1.SelectedItems.Count;i++)
                {
                    champ.Add(FilesAccess.db.championships.Single(x=>x.Name==listBox1.SelectedItems[i]).Id);
                }
            }
            catch { }


            gamesRow[] allGames;

            allGames = (gamesRow[])FilesAccess.db.games.Select("Date >= '" + startDate + "' AND Date <= '" + finalDate + "'");

            if(player!=null)
                allGames = allGames.Where(x => x.IdPlayerA == player.Id || x.IdPlayerB == player.Id).ToArray();

            //only games with the selected opp
            if (OppVS!=null)
            {
                allGames = allGames.Where(x => x.IdPlayerA == OppVS.Id || x.IdPlayerB == OppVS.Id).ToArray();
            }

            if (champ != null)
            {
                if (champ.Count > 0)
                {
                    List<gamesRow> allQueries = new List<gamesRow>();
                    string selectedQuery = "";
                    for (int i = 0; i < champ.Count; i++)
                    {
                        selectedQuery += "IdChamp='";
                        selectedQuery += champ[i] + "' OR ";
                        allQueries.AddRange(allGames.Where(x => x.IdChamp == champ[i]).ToList());
                    }
                    selectedQuery += "IdChamp='";
                    selectedQuery += "-1'";

                    allGames = allQueries.ToArray();
                }
            }

            allGames = allGames.OrderBy(x => x.Date).ToArray();
            dataGridView1.DataSource = allGames;
        }

        /// <summary>
        /// Add the Ranking place and club
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void datagridview1_rowadded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            if (player == null)
                return;

            try
            {
                if (e.RowCount < 1)
                    return;
                
                for (int i = e.RowIndex; i < e.RowCount+e.RowIndex; i++)
                {
                    try
                    {
                        int idGame = Convert.ToInt32(dataGridView1["id", i].Value);
                        DataSet2.gamesRow game = FilesAccess.db.games.Single(x => x.id == idGame);

                        long idWinner = game.IdPlayerWinner;

                        gamesCount++;
                        if (idWinner == player.Id) gameWon++;

                        dataGridView1.Rows[i].Cells[dataGridView1.Columns["Championship"].Index].Value = FilesAccess.db.championships.Single(x => x.Id == game.IdChamp).Name;

                        string res = player.Id==game.IdPlayerA ? game.SetsA + "-" + game.SetsB : game.SetsB + "-" + game.SetsA;

                        dataGridView1.Rows[i].Cells[dataGridView1.Columns["Result"].Index].Value = res;

                        playerRow oppPlayer = game.IdPlayerA == player.Id ? FilesAccess.db.player.Single(x => x.Id == game.IdPlayerB) : FilesAccess.db.player.Single(x => x.Id == game.IdPlayerA);

                        string opp = "[" + oppPlayer.currentRank + "] " + oppPlayer.Name;

                        dataGridView1.Rows[i].Cells[dataGridView1.Columns["Opponent"].Index].Value = opp;
                        
                        dataGridView1.Rows[i].Cells[dataGridView1.Columns["Points"].Index].Value =
                            player.Id==game.IdPlayerA ? game.pointsPlayerA : game.pointsPlayerB;

                        dataGridView1.Rows[i].Cells[dataGridView1.Columns["oppPoints"].Index].Value =
                            oppPlayer.currentPoints;

                        //paint the row according to result

                        if (idWinner == player.Id)
                            dataGridView1.Rows[i].DefaultCellStyle.BackColor = Color.Green;
                        else
                            dataGridView1.Rows[i].DefaultCellStyle.BackColor = Color.Red;
                    }
                    catch { dataGridView1.Rows[i].DefaultCellStyle.BackColor = Color.Black; }
                }

                label6.Text = gamesCount.ToString();
                label7.Text = gameWon.ToString();
                label9.Text = (gamesCount - gameWon).ToString();

                
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                int actualYear = Convert.ToInt32(textBox1.Text);
                actualYear--;
                textBox1.Text = actualYear.ToString();
            }
            catch { }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                int actualYear = Convert.ToInt32(textBox1.Text);
                actualYear++;
                textBox1.Text = actualYear.ToString();
            }
            catch { }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            try
            {
                StartDateTextbox.Text = textBox1.Text + "-07-01";
                FinalDateTextbox.Text = Convert.ToInt32(textBox1.Text) + 1 + "-06-30";
                button1_Click(this, new EventArgs());
            }
            catch { }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            button1_Click(this, new EventArgs());
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            player = FilesAccess.db.player.Single(x => x.Name == comboBox1.Text);
            button1_Click(this, new EventArgs());
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox2.Text == "All")
                OppVS = null;
            else
                OppVS = FilesAccess.db.player.Single(x => x.Name == comboBox2.Text);
            
            button1_Click(this, new EventArgs());
        }

        /// <summary>
        /// Export games from the player
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click_1(object sender, EventArgs e)
        {
            string name = comboBox1.Text;
            string startDate = StartDateTextbox.Text;
            string finalDate = FinalDateTextbox.Text;

            string title = "Games of "+name+ " "+startDate+"-"+finalDate;

            List<IPlayerGameReport> playerGames = new List<IPlayerGameReport>();
            for (int i = 0; i < dataGridView1.RowCount; i++)
            {
                playerGames.Add(new PlayerGame(dataGridView1["Date", i].Value, name, dataGridView1["Championship", i].Value, dataGridView1["Opponent", i].Value, dataGridView1["Result", i].Value, dataGridView1["Points", i].Value, dataGridView1.Rows[i].DefaultCellStyle.BackColor));
            }

            new PdfPlayerGames(title, playerGames);
            
        }
    }
}
