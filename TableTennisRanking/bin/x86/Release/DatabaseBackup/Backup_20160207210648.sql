-- MySqlBackup.NET 2.0.8
-- Dump Time: 2016-02-07 21:06:48
-- --------------------------------------
-- Server version 5.1.73-log MySQL Community Server (GPL)


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES latin1 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- 
-- Definition of calculations
-- 

DROP TABLE IF EXISTS `calculations`;
CREATE TABLE IF NOT EXISTS `calculations` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `type` text NOT NULL,
  `weight` float NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- 
-- Dumping data for table calculations
-- 

/*!40000 ALTER TABLE `calculations` DISABLE KEYS */;

/*!40000 ALTER TABLE `calculations` ENABLE KEYS */;

-- 
-- Definition of championships
-- 

DROP TABLE IF EXISTS `championships`;
CREATE TABLE IF NOT EXISTS `championships` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` text NOT NULL,
  `weight` float NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- 
-- Dumping data for table championships
-- 

/*!40000 ALTER TABLE `championships` DISABLE KEYS */;
INSERT INTO `championships`(`Id`,`Name`,`weight`) VALUES
(1,'Campeonato Regional Madeira 1ª Divisão',1.75),
(2,'Campeonato Regional de Singulares Seniores',2),
(3,'Campeonato Nacional 2ª Divisão Honra',2),
(4,'Campeonato Nacional 1ª Divisão',2.25),
(5,'Torneio Regional',1),
(6,'Campeonato Nacional Singulares Seniores',2.25),
(7,'Treino',0),
(8,'Torneio Nacional',1);
/*!40000 ALTER TABLE `championships` ENABLE KEYS */;

-- 
-- Definition of clubs
-- 

DROP TABLE IF EXISTS `clubs`;
CREATE TABLE IF NOT EXISTS `clubs` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` text NOT NULL,
  `Photo` text NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;

-- 
-- Dumping data for table clubs
-- 

/*!40000 ALTER TABLE `clubs` DISABLE KEYS */;
INSERT INTO `clubs`(`Id`,`Name`,`Photo`) VALUES
(1,'Clube Desportivo Santa Rita',''),
(2,'Clube Desportivo São Roque',''),
(3,'Grupo Desportivo Estreito',''),
(4,'Desportivo Machico',''),
(5,'ACM Madeira',''),
(6,'ADC Ponta do Pargo',''),
(7,'AD Galomar',''),
(8,'CD 1º Maio',''),
(9,'Sporting C. Madeira',''),
(10,'CTM Funchal',''),
(11,'AD Caramanchao',''),
(13,'ACD São João',''),
(14,'CTM Ponta Sol',''),
(15,'CDE Santa Cruz',''),
(16,'CS Maritimo',''),
(17,'CSD Camara de Lobos',''),
(18,'Sporting C. Porto Santo',''),
(19,'Guilhabreu',''),
(20,'AR Novelense',''),
(21,'Sporting Clube de Portugal',''),
(22,'Sport Lisboa e Benfica',''),
(23,'CTM Mirandela',''),
(24,'GD Toledos',''),
(25,'GD Juncal',''),
(26,'CP Oliveirinha',''),
(27,'São Cosme',''),
(28,'AATM Região Centro',''),
(29,'Estrangeiro',''),
(30,'JS Campinense',''),
(31,'CP Alvito',''),
(32,'Juventude Sanguedo',''),
(33,'CC Currelos',''),
(34,'Top Spin',''),
(35,'Campismo SJ Madeira',''),
(36,'UR Dafundo',''),
(38,'Dragoes Valbonenses',''),
(39,'Academico de Viseu FC',''),
(40,'GD Viso',''),
(41,'ADC Ega',''),
(42,'AHBV Seia',''),
(43,'aaaa',''),
(44,'abc',''),
(45,'abc',''),
(46,'testing',''),
(47,'Vitoria FC',''),
(48,'CTM Santa Teresinha','');
/*!40000 ALTER TABLE `clubs` ENABLE KEYS */;

-- 
-- Definition of destinationlenght
-- 

DROP TABLE IF EXISTS `destinationlenght`;
CREATE TABLE IF NOT EXISTS `destinationlenght` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Lenght` text NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

