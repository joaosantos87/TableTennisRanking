﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using TableTennis.Database;
using TableTennis.Controls;
using TableTennis.Reports;
using iTextSharp.text;
using TableTennis.Controls;
using TableTennis.Controls.Championships;
using TableTennis.Controls.General;

namespace TableTennisRanking
{
    public partial class Form1 : Form
    {
        UserControlGUI playerUI = new UserControlGUI();
        UserControlGUI clubUI = new UserControlGUI();
        UserControlGUI champUI = new UserControlGUI();
        UserControlGUI gamesUI = new UserControlGUI();
        UserControlGUI rankingUI = new UserControlGUI();

        AddPlayer addPlayerUI = new AddPlayer();
        AddChamp addChamp = new AddChamp();
        AddClub addClubUI = new AddClub();
        ClubPlayersUI viewClubPlayers = new ClubPlayersUI();
        RankingViewerUI rankViewer = new RankingViewerUI();
        ListPlayersLicence listPlayersLicense = new ListPlayersLicence();
        EditGames editGames = new EditGames();
        EditPlayer editPlayerUI = new EditPlayer();
        PlayerViewerUI playerViewer = new PlayerViewerUI();
        EditClub editClub = new EditClub();
        EditChamp editChamp = new EditChamp();
        ChampGames viewPlayerChampGames = new ChampGames();
        TeamChampGames viewTeamChampGames = new TeamChampGames();
        PlayerHistoric playerHistoric = new PlayerHistoric();
        CombinePlayers combinePlayers = new CombinePlayers();

        public Form1()
        {
            try
            {
                InitializeComponent();
                Enabled = false;
                MySqlDatabase.Instance.ToString();
                MySqlDatabase.Instance.DatabaseInitialized += Instance_DatabaseInitialized;

                FormClosing += Form1_FormClosing;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void InitializeForm()
        {
            try
            {
                //update the players rank and points
                var ranking = FilesAccess.db.ranking.Where(x => x.idSeason == TableTennis.Utilities.TodasEpocas.id && x.idChamp == TableTennis.Utilities.TodosCampeonatos.Id);

                foreach (var r in ranking)
                {
                    FilesAccess.db.player.Single(x => x.Id == r.idPlayer).currentRank = r.rank;
                    FilesAccess.db.player.Single(x => x.Id == r.idPlayer).currentPoints = r.points;
                }

                playerUI.SetName("Players");
                playerUI.SetButton(0, "Add Player", Environment.CurrentDirectory + @"\plus.png");
                playerUI.SetButton(1, "Edit Games", Environment.CurrentDirectory + @"\list.png");
                playerUI.SetButton(2, "Edit Player", Environment.CurrentDirectory + @"\list.png");
                playerUI.SetButton(3, "View Games", Environment.CurrentDirectory + @"\list.png");
                playerUI.SetButton(4, "Historic", Environment.CurrentDirectory + @"\list.png");
                playerUI.SetButton(5, "Combine Players", Environment.CurrentDirectory + @"\list.png");
                playerUI.AddMainInterface(addPlayerUI);
                playerUI.AddMainInterface(editGames);
                playerUI.AddMainInterface(editPlayerUI);
                playerUI.AddMainInterface(playerViewer);
                playerUI.AddMainInterface(playerHistoric);
                playerUI.AddMainInterface(combinePlayers);

                clubUI.SetName("Clubs");
                clubUI.SetButton(0, "Add Club", Environment.CurrentDirectory + @"\plus.png");
                clubUI.SetButton(1, "View Players", Environment.CurrentDirectory + @"\list.png");
                clubUI.SetButton(2, "Edit Club", Environment.CurrentDirectory + @"\list.png");
                clubUI.AddMainInterface(addClubUI);
                clubUI.AddMainInterface(viewClubPlayers);
                clubUI.AddMainInterface(editClub);

                champUI.SetName("Champs");
                champUI.SetButton(0, "Add Champ", Environment.CurrentDirectory + @"\plus.png");
                champUI.SetButton(1, "View Champs", Environment.CurrentDirectory + @"\list.png");
                champUI.SetButton(2, "View Games", Environment.CurrentDirectory + @"\list.png");
                champUI.SetButton(3, "View Team Games", Environment.CurrentDirectory + @"\list.png");
                viewPlayerChampGames.Dock = DockStyle.Fill;
                viewTeamChampGames.Dock = DockStyle.Fill;
                champUI.Dock = DockStyle.Fill;
                champUI.AddMainInterface(addChamp);
                champUI.AddMainInterface(editChamp);
                champUI.AddMainInterface(viewPlayerChampGames);
                champUI.AddMainInterface(viewTeamChampGames);

                gamesUI.SetName("Games");
                gamesUI.SetButton(0, "Add Game", Environment.CurrentDirectory + @"\plus.png");
                gamesUI.SetButton(1, "View Games", Environment.CurrentDirectory + @"\list.png");
                gamesUI.SetButton(2, "Add To Team Game", Environment.CurrentDirectory + @"\list.png");
                gamesUI.SetButton(3, "Edit Team Game", Environment.CurrentDirectory + @"\list.png");
                gamesUI.SetButton(4, "Add Team Games Automatically", Environment.CurrentDirectory + @"\list.png");
                gamesUI.AddMainInterface(new AddGame());
                gamesUI.AddMainInterface(new ViewGames());
                gamesUI.AddMainInterface(new AddToTeamGame());
                gamesUI.AddMainInterface(new EditTeamGame());
                gamesUI.AddMainInterface(new AddTeamGameAutomatically());

                rankingUI.SetName("Ranking");
                rankingUI.SetButton(0, "View Ranking", Environment.CurrentDirectory + @"\list.png");
                rankingUI.SetButton(1, "List Players", Environment.CurrentDirectory + @"\list.png");
                rankingUI.AddMainInterface(rankViewer);
                rankingUI.AddMainInterface(listPlayersLicense);
                //gamesUI.AddMainInterface(addGame);

                panel3.Controls.Add(playerUI);
                panel3.Controls.Add(clubUI);
                panel3.Controls.Add(champUI);
                panel3.Controls.Add(gamesUI);
                panel3.Controls.Add(rankingUI);
                playerUI.Visible = false;
                clubUI.Visible = false;
                champUI.Visible = false;
                gamesUI.Visible = false;
                rankingUI.Visible = false;
                Enabled = true;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Instance_DatabaseInitialized(object sender, EventArgs e)
        {
            if(InvokeRequired)
            {
                Invoke(new Action(() => Instance_DatabaseInitialized(sender, e)));
            }
            else
            {
                InitializeForm();
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                FilesAccess.db.BackupDatabase();
            }
            catch
            {
                MessageBox.Show("Error performing database backup");
            }
        }
        
        private void button1_Click(object sender, EventArgs e)
        {
            playerUI.Visible = true;
            clubUI.Visible = false;
            champUI.Visible = false;
            gamesUI.Visible = false;
            rankingUI.Visible = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            playerUI.Visible = false;
            clubUI.Visible = true;
            champUI.Visible = false;
            gamesUI.Visible = false;
            rankingUI.Visible = false;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            playerUI.Visible = false;
            clubUI.Visible = false;
            champUI.Visible = true;
            gamesUI.Visible = false;
            rankingUI.Visible = false;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            playerUI.Visible = false;
            clubUI.Visible = false;
            champUI.Visible = false;
            gamesUI.Visible = true;
            rankingUI.Visible = false;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            playerUI.Visible = false;
            clubUI.Visible = false;
            champUI.Visible = false;
            gamesUI.Visible = false;
            rankingUI.Visible = true;
        }

    }
}
