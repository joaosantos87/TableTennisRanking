﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using TableTennis.Database;

//namespace TableTennis.Ranking
//{
//    public class Ranking
//    {
//        public Ranking(DateTime start, DateTime end, List<int> champ = null, List<int> association = null)
//        {
//            string preventNegative = " IdPlayerA > -1 AND IdPlayerB >-1";

//            DataSet1.gamesRow[] allGames;

//            allGames =
//    (DataSet1.gamesRow[])FilesAccess.db.games.Select(
//    "Date >= '" + start + "' AND Date <= '" + end + "'");

//            if (champ != null)
//            {
//                if (champ.Count > 0)
//                {
//                    List<DataSet1.gamesRow> allQueries = new List<DataSet1.gamesRow>();
//                    string selectedQuery = "";
//                    for (int i = 0; i < champ.Count; i++)
//                    {
//                        selectedQuery += "IdChamp='";
//                        selectedQuery += champ[i] + "' OR ";
//                        allQueries.AddRange(allGames.Where(x => x.IdChamp == champ[i]).ToList());
//                    }
//                    selectedQuery += "IdChamp='";
//                    selectedQuery += "-1'";

//                    allGames = allQueries.ToArray();

//                    //allGames = (DataSet1.gamesRow[])FilesAccess.db.games.Select(
//                    //        selectedQuery + "' AND Date >= '" + start + "' AND Date <= '" + end + "'");
//                }
//            }

//            if (association != null)
//            {

//            }

//            allGames = allGames.OrderBy(x => x.Date).ToArray();

//            for (int i = 0; i < FilesAccess.db.player.Count; i++)
//            {
//                FilesAccess.db.player[i].currentPoints = 1000;
//                FilesAccess.db.player[i].currentGames = 0;
//                FilesAccess.db.player[i].currentRank = 0;
//            }

//            //if (champ == null)
//            //    return;

//            //if (champ.Count==0)
//            //    return;

//            //FilesAccess.db.UpdatePlayer();
//            for (int i = 0; i < allGames.Length; i++)
//            {
//                if (allGames[i].IdPlayerA > -1 && allGames[i].IdPlayerB > -1)
//                {
//                    try
//                    {
//                        DataSet1.playerRow playerA = FilesAccess.db.player.Single(x => x.Id == allGames[i].IdPlayerA);
//                        DataSet1.playerRow playerB = FilesAccess.db.player.Single(x => x.Id == allGames[i].IdPlayerB);

//                        if ((playerA.Name == "Pedro Vieira" || playerA.Name == "João Seduvem") && (playerB.Name == "Pedro Vieira" || playerB.Name == "João Seduvem"))
//                        {
//                            Console.WriteLine("break");
//                        }

//                        Tuple<int, int> pointsToDistribuite = AwardPoints(allGames[i].IdPlayerWinner, playerA, playerB);
//                        DataSet1.championshipsRow champRow = FilesAccess.db.championships.Single(x => x.Id == allGames[i].IdChamp);
//                        float weight = FilesAccess.db.tournamentType.Single(x => x.id == champRow.idTournamentType).weight;

//                        int pointsWinner = Convert.ToInt32(pointsToDistribuite.Item2 * weight);
//                        int pointsLoser = Convert.ToInt32(pointsToDistribuite.Item2 / 2 * 1);

//                        if (playerA.Id == allGames[i].IdPlayerWinner)
//                        {

//                            playerA.currentPoints += pointsWinner;
//                            playerB.currentPoints -= pointsLoser;
//                            allGames[i].pointsPlayerA = pointsWinner;
//                            allGames[i].pointsPlayerB = -pointsLoser;
//                        }
//                        else
//                        {
//                            playerB.currentPoints += pointsWinner;
//                            playerA.currentPoints -= pointsLoser;
//                            allGames[i].pointsPlayerB = pointsWinner;
//                            allGames[i].pointsPlayerA = -pointsLoser;
//                        }
//                        playerA.currentGames++;
//                        playerB.currentGames++;
//                    }
//                    catch (Exception ex)
//                    {
//                        Console.WriteLine(ex.Message);
//                    }
//                }
//            }

//            DataSet1.playerRow[] temp = FilesAccess.db.player.OrderByDescending(x => x.currentPoints).ToArray();
//            for (int i = 0; i < temp.Length; i++)
//            {
//                FilesAccess.db.player.Single(x => x.Id == temp[i].Id).currentRank = (i + 1);
//            }

//        }

//        private bool IsWinnerExpected(int winnerId, DataSet1.playerRow playerA, DataSet1.playerRow playerB)
//        {
//            if (playerA.currentPoints >= playerB.currentPoints && winnerId == playerA.Id ||
//                playerB.currentPoints >= playerA.currentPoints && winnerId == playerB.Id)
//                return true;
//            else
//                return false;
//        }

//        private Tuple<int, int> AwardPoints(int winnerId, DataSet1.playerRow playerA, DataSet1.playerRow playerB)
//        {
//            List<Tuple<int, int>> pointsExpected = new List<Tuple<int, int>>();
//            List<Tuple<int, int>> pointsUnexpected = new List<Tuple<int, int>>();

//            List<Tuple<int, int, int>> preConfiguredPoints = new List<Tuple<int, int, int>>();
//            preConfiguredPoints.Add(new Tuple<int, int, int>(-1, 8, 8));
//            preConfiguredPoints.Add(new Tuple<int, int, int>(1, 8, 8));
//            preConfiguredPoints.Add(new Tuple<int, int, int>(12, 8, 8));
//            preConfiguredPoints.Add(new Tuple<int, int, int>(37, 7, 10));
//            preConfiguredPoints.Add(new Tuple<int, int, int>(62, 6, 13));
//            preConfiguredPoints.Add(new Tuple<int, int, int>(87, 5, 16));
//            preConfiguredPoints.Add(new Tuple<int, int, int>(112, 4, 20));
//            preConfiguredPoints.Add(new Tuple<int, int, int>(137, 3, 25));
//            preConfiguredPoints.Add(new Tuple<int, int, int>(162, 2, 30));
//            preConfiguredPoints.Add(new Tuple<int, int, int>(187, 2, 35));
//            preConfiguredPoints.Add(new Tuple<int, int, int>(212, 1, 40));
//            preConfiguredPoints.Add(new Tuple<int, int, int>(237, 1, 45));
//            preConfiguredPoints.Add(new Tuple<int, int, int>(238, 0, 50));

//            for (int i = 0; i < preConfiguredPoints.Count; i++)
//            {
//                pointsExpected.Add(new Tuple<int, int>(preConfiguredPoints[i].Item1, preConfiguredPoints[i].Item2 * 2));
//                pointsUnexpected.Add(new Tuple<int, int>(preConfiguredPoints[i].Item1, preConfiguredPoints[i].Item3 * 2));
//            }

//            #region Backup points
//            //pointsExpected.Add(new Tuple<int, int>(-1, 9));
//            //pointsExpected.Add(new Tuple<int, int>(1, 9));
//            //pointsExpected.Add(new Tuple<int, int>(26, 8));
//            //pointsExpected.Add(new Tuple<int, int>(51, 7));
//            //pointsExpected.Add(new Tuple<int, int>(101, 6));
//            //pointsExpected.Add(new Tuple<int, int>(151, 5));
//            //pointsExpected.Add(new Tuple<int, int>(201, 4));
//            //pointsExpected.Add(new Tuple<int, int>(301, 3));
//            //pointsExpected.Add(new Tuple<int, int>(401, 2));
//            //pointsExpected.Add(new Tuple<int, int>(501, 1));
//            //pointsExpected.Add(new Tuple<int, int>(750, 0));

//            //pointsUnexpected.Add(new Tuple<int, int>(-1, 10));
//            //pointsUnexpected.Add(new Tuple<int, int>(0, 10));
//            //pointsUnexpected.Add(new Tuple<int, int>(25, 12));
//            //pointsUnexpected.Add(new Tuple<int, int>(50, 14));
//            //pointsUnexpected.Add(new Tuple<int, int>(100, 16));
//            //pointsUnexpected.Add(new Tuple<int, int>(150, 20));
//            //pointsUnexpected.Add(new Tuple<int, int>(200, 24));
//            //pointsUnexpected.Add(new Tuple<int, int>(300, 28));
//            //pointsUnexpected.Add(new Tuple<int, int>(400, 32));
//            //pointsUnexpected.Add(new Tuple<int, int>(500, 36));
//            //pointsUnexpected.Add(new Tuple<int, int>(749, 40));
//            #endregion

//            int dif = Math.Abs(playerA.currentPoints - playerB.currentPoints);
//            for (int i = pointsExpected.Count - 1; i > -1; i--)
//            {
//                if (dif > pointsExpected[i].Item1)
//                {
//                    if (IsWinnerExpected(winnerId, playerA, playerB))
//                    {
//                        return pointsExpected[i];
//                    }
//                    else
//                        return pointsUnexpected[i];
//                }
//            }
//            return new Tuple<int, int>(0, 0);
//        }
//    }
//}
