﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using iTextSharp;
//using iTextSharp.text;
//using iTextSharp.text.pdf;
//using System.IO;

//namespace TableTennis.Controls
//{
//    public class PdfReport
//    {
//        BaseFont bfTimes;
//        iTextSharp.text.Font p1;
//        iTextSharp.text.Font p2;
//        iTextSharp.text.Font p3;

//        iTextSharp.text.Font p1b;
//        iTextSharp.text.Font p2b;
//        iTextSharp.text.Font p3b;

//        Paragraph titleSegment;
//        Paragraph bodyLabel;
//        Paragraph bodyText;
//        Paragraph bodyText2;
//        PdfPCell cell;
//        PdfPTable table;

//        Document pdfDoc;

//        public PdfReport(string title, List<RankPlayer> rankPlayers)
//        {
//            bfTimes = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
//            p1 = new iTextSharp.text.Font(bfTimes, 16, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
//            p2 = new iTextSharp.text.Font(bfTimes, 14, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
//            p3 = new iTextSharp.text.Font(bfTimes, 12, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
//            p1b = new iTextSharp.text.Font(bfTimes, 16, iTextSharp.text.Font.UNDERLINE | iTextSharp.text.Font.BOLD, BaseColor.BLACK);
//            p2b = new iTextSharp.text.Font(bfTimes, 14, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
//            p3b = new iTextSharp.text.Font(bfTimes, 12, iTextSharp.text.Font.BOLD, BaseColor.BLACK);

//            //Check if report Folder exists

//            if (!Directory.Exists("Reports"))
//                Directory.CreateDirectory("Ranking");
//            string filename = title;
//            if(title.Count()>248)
//                filename = title.Split(' ')[0];

//            filename += "_"+DateTime.Now.ToString("[yyyy-MM-dd_HH-mm-ss]");
//            pdfDoc = new Document();
//            string path = "Ranking" + Path.DirectorySeparatorChar + filename + ".pdf";

//            PdfWriter writer = PdfWriter.GetInstance(pdfDoc, new FileStream(path, FileMode.OpenOrCreate));
//            pdfDoc.Open();

//            Paragraph titleP = new Paragraph("Ranking - " + title, p1b);
//            titleP.Alignment = Element.ALIGN_CENTER;

//            pdfDoc.Add(titleP);
//            pdfDoc.Add(new Paragraph("\n"));

//            InsertTableRow(rankPlayers[0].Header, p3b);

//            //Get players that have X PPG
//            //rankPlayers = rankPlayers.Where(x => x.NumGames > 30).ToList();

//            for (int i = 0; i < rankPlayers.Count; i++)
//            {
//                InsertTableRow(rankPlayers[i].Data, p3);
//            }

//            pdfDoc.Close();
//        }

//        private void InsertTableRow(string[] data, Font font)
//        {
//            if (data.Length != 5)
//                throw new Exception("Table number columns does not match");
//            //cell.Border = Rectangle.NO_BORDER;
//            table = new PdfPTable(5);
            
//            table.SetWidths(new int[] { 80, 200, 200, 80, 80});
//            table.DefaultCell.Border = Rectangle.NO_BORDER;

//            for (int i = 0; i < data.Length; i++)
//            {
//                bodyText = new Paragraph(data[i], font);
//                cell = new PdfPCell(bodyText);
//                cell.HorizontalAlignment = Element.ALIGN_CENTER;
//                table.AddCell(cell);
//            }
//            pdfDoc.Add(table);
//        }
//    }

//    public class RankPlayer
//    {
//        string name;
//        string club;
//        string points;
//        string numGames;
//        string ranking;
//        string association;

//        public string[] Data
//        {
//            get { return new string[] { ranking, name, club, association, points, numGames }; }
//        }

//        public int PPG
//        {
//            get { return (Convert.ToInt32(points) - 1000) / Convert.ToInt32(numGames); }
//        }

//        public int NumGames
//        {
//            get { return Convert.ToInt32(numGames); }
//        }

//        public string[] Header
//        {
//            get { return new string[] { "Ranking", "Name", "Club", "Points", "Games Played" }; }
//        }

//        public RankPlayer(object _ranking, object _name, object _club, object _association, object _points, object _numGames)
//        {
//            name = _name.ToString();
//            club = _club.ToString();
//            points = _points.ToString();
//            numGames = _numGames.ToString();
//            ranking = _ranking.ToString();
//            association = _association.ToString();
//        }
//    }
//}
