﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;


namespace TableTennis.Reports
{
    public class PdfRanking : PdfReport
    {
        public PdfRanking(string title, List<RankPlayer> rankPlayers)
        {
            if (!Directory.Exists("Reports"))
                Directory.CreateDirectory("Ranking");

            string filename = title;
            if (title.Count() > 248)
                filename = title.Split(' ')[0];

            filename += "_" + DateTime.Now.ToString("[yyyy-MM-dd_HH-mm-ss]");
            string path = "Ranking" + Path.DirectorySeparatorChar + filename + ".pdf";

            CreateDocument(path, "Ranking - " + title, rankPlayers[0].WidthSize);

            InsertRow(rankPlayers[0].Header, P3b);

            for (int i = 0; i < rankPlayers.Count; i++)
                InsertRow(rankPlayers[i].Data, P3);

            Close();
        }
    }

    public class RankPlayer
    {
        string name;
        string club;
        string points;
        string numGames;
        string ranking;
        string association;

        public string[] Data
        {
            get { return new string[] { ranking, name, club, points, numGames }; }
        }

        public int PPG
        {
            get { return (Convert.ToInt32(points) - 1000) / Convert.ToInt32(numGames); }
        }

        public int NumGames
        {
            get { return Convert.ToInt32(numGames); }
        }

        public string[] Header
        {
            get { return new string[] { "Ranking", "Name", "Club", "Points", "Games Played" }; }
        }

        public int[] WidthSize
        {
            get
            {
                return new int[] { 80, 200, 200, 80, 80 };
            }
        }

        public RankPlayer(object _ranking, object _name, object _club, object _association, object _points, object _numGames)
        {
            name = _name.ToString();
            club = _club.ToString();
            points = _points.ToString();
            numGames = _numGames.ToString();
            ranking = _ranking.ToString();
            association = _association.ToString();
        }
    }
}
