﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;

namespace TableTennis.Reports
{
    public class PdfReport
    {
        BaseFont bfTimes;
        Font p1;
        Font p2;
        Font p3;

        Font p1b;
        Font p2b;
        Font p3b;

        Paragraph bodyText;
        PdfPCell cell;
        PdfPTable table;

        Document pdfDoc;

        #region Properties
        public Font P1
        {
            get { return p1; }
        }

        public Font P2
        {
            get { return p2; }
        }

        public Font P3
        {
            get { return p3; }
        }

        public Font P1b
        {
            get { return p1b; }
        }

        public Font P2b
        {
            get { return p2b; }
        }

        public Font P3b
        {
            get { return p3b; }
        }
        #endregion

        int[] ColumnsWidth;

        public PdfReport()
        {
            bfTimes = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
            p1 = new Font(bfTimes, 16, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
            p2 = new Font(bfTimes, 14, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
            p3 = new Font(bfTimes, 12, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
            p1b = new Font(bfTimes, 16, iTextSharp.text.Font.UNDERLINE | iTextSharp.text.Font.BOLD, BaseColor.BLACK);
            p2b = new Font(bfTimes, 14, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
            p3b = new Font(bfTimes, 12, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
        }

        public void CreateDocument(string path, string title, int[] columnsWidth)
        {
            pdfDoc = new Document();

            PdfWriter writer = PdfWriter.GetInstance(pdfDoc, new FileStream(path, FileMode.OpenOrCreate));
            pdfDoc.Open();

            Paragraph titleP = new Paragraph(title, p1b);
            titleP.Alignment = Element.ALIGN_CENTER;

            pdfDoc.Add(titleP);
            pdfDoc.Add(new Paragraph("\n"));
            ColumnsWidth = columnsWidth;
        }

        public void Close()
        {
            pdfDoc.Close();
        }

        public void InsertRow(string[] data, Font font, BaseColor color=null)
        {
            CreateTable();
            if (data.Length != table.NumberOfColumns)
                throw new Exception("Table number columns does not match");


            InsertTableRow(data, font, color);
        }

        private void CreateTable()
        {
            table = new PdfPTable(ColumnsWidth.Length);

            table.SetWidths(ColumnsWidth);
            table.DefaultCell.Border = Rectangle.NO_BORDER;
        }

        private void InsertTableRow(string[] data, Font font, BaseColor color = null)
        {
            for (int i = 0; i < data.Length; i++)
            {
                bodyText = new Paragraph(data[i], font);
                cell = new PdfPCell(bodyText);
                cell.HorizontalAlignment = Element.ALIGN_CENTER;

                if (color != null)
                    cell.BackgroundColor = color;

                table.AddCell(cell);
            }
            pdfDoc.Add(table);
        }
    }
}
