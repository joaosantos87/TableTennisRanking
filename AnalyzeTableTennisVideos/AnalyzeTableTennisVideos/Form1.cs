﻿using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Emgu.CV;
using Emgu.CV.Cvb;
using Emgu.CV.CvEnum;
using Emgu.CV.Util;

namespace AnalyzeTableTennisVideos
{
    public partial class Form1 : Form, INotifyPropertyChanged
    {
        private VideoCapture video;
        private BackgroundSubtractorMOG2 fgDetector;
        private CvBlobDetector blobDetector;
        private CvTracks tracker;
        private System.Timers.Timer t1;
        int currentFrame;

        Dictionary<uint, string> sceneObjects = new Dictionary<uint, string>();
        public int CurrentFrame
        {
            get { return currentFrame; }
            set { currentFrame = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(CurrentFrame))); }
        }

        public Form1()
        {
            InitializeComponent();
            trackBar1.DataBindings.Add(new Binding("Value", this, nameof(CurrentFrame), true, DataSourceUpdateMode.OnPropertyChanged));
            pictureBox1.MouseUp += PictureBox1_MouseUp;
            pictureBox1.Paint += pictureBox1_Paint;
            pictureBox1.MouseMove += pictureBox1_MouseMove;
            pictureBox1.MouseDown += pictureBox1_MouseDown;
        }
        
        private void button1_Click(object sender, EventArgs e)
        {
            //video = new VideoCapture(@"D:\\jsto\\My Videos\\Tenis de Mesa\\VID_20180613_194902.mp4");
            video = new VideoCapture(@"D:\\jsto\\My Videos\\Tenis de Mesa\\ZHANG Jike vs HARIMOTO Tomokazu 張本 智和 - MS R32 - China Open 2018 (youtubemp4.to).mp4");
            trackBar1.Maximum = (int)video.GetCaptureProperty(CapProp.FrameCount);

            trackBar1.Value = 5000;
            Run();
        }

        void Run()
        {
            fgDetector = new BackgroundSubtractorMOG2();
            blobDetector = new CvBlobDetector();
            tracker = new CvTracks();

            t1 = new System.Timers.Timer(1);
            t1.Elapsed += ProcessFrame;
            //t1.Start();
        }
        object locker = new object();

        public event PropertyChangedEventHandler PropertyChanged;

        private void ProcessFrame(object sender, EventArgs e)
        {
            lock (locker)
            {
                video.SetCaptureProperty(CapProp.PosFrames, CurrentFrame);
                Mat frame = video.QueryFrame();
                imgInput = frame.ToImage<Bgr, byte>();
                Mat smoothedFrame = new Mat();
                CvInvoke.GaussianBlur(frame, smoothedFrame, new Size(3, 3), 1);

                pictureBox3.Image = GetObjects(frame.Clone()).Bitmap;

                #region use the BG/FG detector to find the forground mask
                Mat forgroundMask = new Mat();
                fgDetector.Apply(smoothedFrame, forgroundMask);
                #endregion

                CvBlobs blobs = new CvBlobs();
                blobDetector.Detect(forgroundMask.ToImage<Gray, byte>(), blobs);
                blobs.FilterByArea(100, int.MaxValue);

                float scale = (frame.Width + frame.Width) / 2.0f;
                tracker.Update(blobs, 0.01 * scale, 5, 5);
                pictureBox1.Invoke(new Action(() => textBox1.Text = ""));
                foreach (var pair in tracker)
                {
                    CvTrack b = pair.Value;
                    CvInvoke.Rectangle(frame, b.BoundingBox, new MCvScalar(255.0, 255.0, 255.0), 2);
                    CvInvoke.PutText(frame, b.Id.ToString(), new Point((int)Math.Round(b.Centroid.X), (int)Math.Round(b.Centroid.Y)), FontFace.HersheyPlain, 10.0, new MCvScalar(0.0, 0.0, 0.0));
                    var insert = pair.Value.Centroid.X.ToString("0.00") + "," + pair.Value.Centroid.Y.ToString("0.00") + " ";
                    if (sceneObjects.ContainsKey(pair.Key))
                        sceneObjects[pair.Key] += insert;
                    else
                        sceneObjects.Add(pair.Key, insert);

                    StringBuilder sb = new StringBuilder();

                    foreach (var obj in sceneObjects)
                        sb.Append(obj.Key + " " + obj.Value + " " + Environment.NewLine);

                    pictureBox1.Invoke(new Action(() =>
                    {
                        textBox1.Text = sb.ToString();
                    }));

                }
                pictureBox1.Invoke(new Action(() => pictureBox1.Image = frame.Bitmap));
                //pictureBox2.Invoke(new Action(() => pictureBox2.Image = forgroundMask.Bitmap));
                //trackBar1.BeginInvoke(new Action(() => { CurrentFrame += 100; }));
            }
        }
        private Mat GetObjects(Mat inputImage)
        {
            var smoothedImage = inputImage.ToImage<Bgr, byte>().SmoothGaussian(5).Convert<Gray, byte>().ThresholdBinaryInv(new Gray(130), new Gray(255));
            
            VectorOfVectorOfPoint countours = new VectorOfVectorOfPoint();
            Mat m = new Mat();
            CvInvoke.FindContours(smoothedImage, countours, m, RetrType.External, ChainApproxMethod.ChainApproxSimple);

            for (int i = 0; i < countours.Size; i++)
            {
                double perimeter = CvInvoke.ArcLength(countours[i], true);
                VectorOfPoint approx = new VectorOfPoint();
                CvInvoke.ApproxPolyDP(countours[i], approx, 0.04 * perimeter, true);
                CvInvoke.DrawContours(inputImage, countours, i, new MCvScalar(0, 255, 255), 2);

            }
            return inputImage;
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            ProcessFrame(this, new EventArgs());
        }

        Image<Bgr, byte> imgInput;
        Rectangle rect;
        Point StartLocation;
        Point EndLcation;
        bool IsMouseDown = false;

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            IsMouseDown = true;
            StartLocation = e.Location;
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            if (IsMouseDown == true)
            {
                EndLcation = e.Location;
                pictureBox1.Invalidate();
            }
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            if (rect != null)
            {
                e.Graphics.DrawRectangle(Pens.Red, GetRectangle());
            }
        }

        private Rectangle GetRectangle()
        {
            rect = new Rectangle();
            rect.X = Math.Min(StartLocation.X, EndLcation.X);
            rect.Y = Math.Min(StartLocation.Y, EndLcation.Y);
            rect.Width = Math.Abs(StartLocation.X - EndLcation.X);
            rect.Height = Math.Abs(StartLocation.Y - EndLcation.Y);

            return rect;
        }

        private void PictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            if (IsMouseDown == true)
            {
                EndLcation = e.Location;
                IsMouseDown = false;
                if (rect != null)
                {
                    imgInput.ROI = rect;
                    Image<Bgr, byte> temp = imgInput.CopyBlank();
                    imgInput.CopyTo(temp);
                    imgInput.ROI = Rectangle.Empty;
                    pictureBox2.Image = temp.Bitmap;
                }
            }
        }
    }
}
