﻿//using Emgu.CV;
//using Emgu.CV.Structure;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace AnalyzeTableTennisVideos
//{
//    public class DifferenceImage
//    {
//        Image<Bgr, Byte> Frame; //current Frame from camera
//        Image<Bgr, Byte> Previous_Frame; //Previiousframe aquired
//        Image<Bgr, Byte> Difference; //Difference between the two frames

//        public DifferenceImage(VideoCapture capture)
//        {
//            double ContourThresh = 0.003; //stores alpha for thread access
//            int Threshold = 60; //stores threshold for thread access
            
//            if (Frame == null) //we need at least one fram to work out running average so acquire one before doing anything
//            {
//                //display the frame aquired
//                Frame = capture.RetrieveBgrFrame(); //we could use RetrieveGrayFrame if we didn't care about displaying colour image
//                DisplayImage(Frame.ToBitmap(), CurrentFrame); //display the image using thread safe call
//                Previous_Frame = Frame.Copy(); //copy the frame to act as the previous
//            }
//            else
//            {
//                //acquire the frame
//                Frame = _capture.RetrieveBgrFrame(); //aquire a frame

//                Difference = Previous_Frame.AbsDiff(Frame); //find the absolute difference 
//                /*Play with the value 60 to set a threshold for movement*/
//                Difference = Difference.ThresholdBinary(new Bgr(Threshold, Threshold, Threshold), new Bgr(255, 255, 255)); //if value > 60 set to 255, 0 otherwise 
//                DisplayImage(Difference.ToBitmap(), resultbox); //display the absolute difference 

//                Previous_Frame = Frame.Copy(); //copy the frame to act as the previous frame

//                #region Draw the contours of difference
//                //this is tasken from the ShapeDetection Example
//                using (MemStorage storage = new MemStorage()) //allocate storage for contour approximation
//                                                              //detect the contours and loop through each of them
//                    for (Contour<Point> contours = Difference.Convert<Gray, Byte>().FindContours(
//                          Emgu.CV.CvEnum.CHAIN_APPROX_METHOD.CV_CHAIN_APPROX_SIMPLE,
//                          Emgu.CV.CvEnum.RETR_TYPE.CV_RETR_LIST,
//                          storage);
//                       contours != null;
//                       contours = contours.HNext)
//                    {
//                        //Create a contour for the current variable for us to work with
//                        Contour<Point> currentContour = contours.ApproxPoly(contours.Perimeter * 0.05, storage);

//                        //Draw the detected contour on the image
//                        if (currentContour.Area > ContourThresh) //only consider contours with area greater than 100 as default then take from form control
//                        {
//                            Frame.Draw(currentContour.BoundingRectangle, new Bgr(Color.Red), 2);
//                        }
//                    }
//                #endregion

//                DisplayImage(Frame.ToBitmap(), CurrentFrame); //display the image using thread safe call
//                DisplayImage(Previous_Frame.ToBitmap(), PreviousFrame); //display the previous image using thread safe call



//            }
//        }
//    }
//}
