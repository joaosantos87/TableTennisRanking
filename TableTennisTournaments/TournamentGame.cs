﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TableTennis.Database;
using static TableTennis.Database.DataSet2;

namespace TableTennis.Tournaments
{
    public class TournamentResume
    {
        public championshipsRow tournamentRow;
        public List<TournamentGroup> Groups;

        public TournamentResume(championshipsRow _tournamentRow, List<TournamentGroup> groups)
        {
            Groups = groups;
            tournamentRow = _tournamentRow;
        }

        public string Details
        {
            get 
            {
                if (Groups == null)
                    return "Não existem grupos inseridos";

                string output = tournamentRow.Name + "\n";
                output += "Data "+tournamentRow.Date.ToString("dd-MM-yyyy hh:mm")+"\n";

                output += "Numero de grupos: " + NumberOfGroups + "\n";
                for (int i = 0; i < Groups.Count; i++)
                    output += Groups[i].Details;

                return output;
            }
        }

        public int NumberOfGroups
        {
            get
            {
                if (Groups == null)
                    return -1;
                return Groups.Count;
            }
        }
    }

    public class TournamentGroup
    {
        public List<TournamentGame> Games;
        int groupId;

        public string Details
        {
            get 
            {
                if (Games == null)
                    return "Grupo "+groupId+" não tem jogos inseridos";

                string output = "Detalhes do grupo " + groupId+": \n";

                output += "Jogadores que constituem o grupo: \n";
                output += Players;

                for (int i = 0; i < Games.Count; i++)
                {
                    output += Games[i].Details+"\n";
                }
                return output;
            }
        }

        List<playerRow> players = new List<playerRow>();

        public string Players
        {
            get
            {
                string output = "";
                players = players.OrderBy(x => x.currentRank).ToList();
                for (int i = 0; i < players.Count; i++)
                {
                    output += "Rank "+players[i].currentRank+" " + players[i].Name+"\n";
                }
                return output;
            }
        }

        public int GroupId
        {
            get { return groupId; }
        }

        public int PhaseId
        {
            get { return Utilities.Phase_Group; }
        }

        public TournamentGroup(int _groupId, List<TournamentGame> games)
        {
            groupId = _groupId;
            Games = games;
            for (int i = 0; i < games.Count; i++)
            {
                if (!players.Contains(games[i].PlayerA) && games[i].PlayerA!=null)
                    players.Add(games[i].PlayerA);

                if (!players.Contains(games[i].PlayerB) && games[i].PlayerB != null)
                    players.Add(games[i].PlayerB);
            }
        }
    }

    public class TournamentGame
    {
        public int Round;
        public DateTime Time { get; set; }
        private playerleaguetournamentRow playerA { get; set; }
        private playerleaguetournamentRow playerB { get; set; }

        public TournamentGame(DateTime time, int round, playerleaguetournamentRow plA, playerleaguetournamentRow plB)
        {
            Time = time;
            playerA = plA;
            playerB = plB;
            Round = round;
        }

        public playerRow PlayerA
        {
            get
            {
                if (playerA != null)
                    return FilesAccess.db.player.Single(x => x.Id == playerA.playerId);
                else
                    return null;
            }
        }

        public playerRow PlayerB
        {
            get
            {
                if (playerB != null)
                    return FilesAccess.db.player.Single(x => x.Id == playerB.playerId);
                else
                    return null;
            }
        }

        private string GetPlayerA
        {
            get
            {
                if (PlayerA != null)
                    return PlayerA.Name;
                else
                    return "Bye";
            }
        }

        private string GetPlayerB
        {
            get
            {
                if (PlayerB != null)
                    return PlayerB.Name;
                else
                    return "Bye";
            }
        }

        public string Details
        {
            get { return "Ronda " + Round + " as " + Time.ToString("HH:mm") + " " + GetPlayerA + " vs " + GetPlayerB; }
        }
    }

    public class GroupPosition
    {
        public int groupId;
        public playerleaguetournamentRow player;

        public GroupPosition(int _groupId, playerleaguetournamentRow _player)
        {
            groupId = _groupId;
            player = _player;
        }
    }
}