﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TableTennis.Controls;
using TableTennis.Database;
using static TableTennis.Database.DataSet2;

namespace TableTennis.Tournaments
{
    public partial class Tournament : Form
    {
        List<playerRow> players;
        public Tournament()
        {
            InitializeComponent();

            ///To know the general ranking
            players = Ranking.Instance.UpdateRanking(new DateTime(2000, 1, 1), DateTime.Now, Utilities.AllOpenTournaments);

            //Put the general ranking and stuff on the correct table
            //for (int i = 0; i < FilesAccess.db.playerInscriptionLeague.Count; i++)
            //{
            //    DataSet1.PlayerInscriptionLeagueRow plilRow = FilesAccess.db.playerInscriptionLeague[i];
            //    FilesAccess.db.playerInscriptionLeague[i].currentPoints = FilesAccess.db.player.Single(x=>x.Id == plilRow.playerId).currentPoints;
            //    FilesAccess.db.playerInscriptionLeague[i].currentGames = FilesAccess.db.player.Single(x => x.Id == plilRow.playerId).currentGames;
            //    FilesAccess.db.playerInscriptionLeague[i].currentRank = FilesAccess.db.player.Single(x => x.Id == plilRow.playerId).currentRank;
            //}

            for (int i = 0; i < Utilities.AllOpenTournaments.Count; i++)
            {
                List<int> champs = new List<int>();
                champs.Add(Utilities.AllOpenTournaments[i]);
                players = Ranking.Instance.UpdateRanking(new DateTime(2000, 1, 1), DateTime.Now, champs);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            new CreateOpenTournament().ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            new EditOpenTournaments().ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            new OpenTournamentInscription().ShowDialog();
        }
        AddGameForm addGame;
        private void button5_Click(object sender, EventArgs e)
        {
            addGame = new AddGameForm();
            addGame.addGame1.createGameButton.Click +=createGameButton_Click;
            addGame.Show();
        }

        private void createGameButton_Click(object sender, EventArgs e)
        {
            players = Ranking.Instance.UpdateRanking(new DateTime(2000, 1, 1), DateTime.Now, Utilities.AllOpenTournaments);
            //addGame.addGame1.UpdateInterface();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            new RankingOpenTournament().Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            new TorneioView().Show();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            new ViewTournamentGamesForm().Show();
        }
    }
}
