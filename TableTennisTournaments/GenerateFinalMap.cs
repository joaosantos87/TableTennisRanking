﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TableTennis.Database;
using System.Windows.Forms;
using static TableTennis.Database.DataSet2;

namespace TableTennis.Tournaments
{
    /// <summary>
    /// In this class it will be generated some games from the group phase
    /// Check the amount of groups that we have, the rules in passing 2/3, and then create the map according to those rules
    /// Map of 8/16/32...
    /// </summary>
    public class GenerateFinalMap
    {
        int champId;
        int numPlayersPass;

        List<int> validPlayerMaps = new List<int> { 4, 8, 16, 32, 64, 128 };

        public GenerateFinalMap(int tournId, int numPlayers)
        {
            champId = tournId;
            numPlayersPass = numPlayers;
            //check if the tournament has all games entered
            int numGames = FilesAccess.db.tournamentGames.Count(x => x.championshipId == tournId);

            int numGamesDone = FilesAccess.db.tournamentGames.Count(x => x.championshipId == tournId && x.gameId!=0);

            if (numGames != numGamesDone)
            {
                DialogResult dr = MessageBox.Show("Inseridos " + numGamesDone + "/" + numGames + " jogos. Deseja apagar os jogos do mapa final inseridos e gerar novo mapa final?","Informacao",MessageBoxButtons.YesNo);

                if (dr == DialogResult.No)
                    return;

                List<tournamentgamesRow> trDelete = ((tournamentgamesRow[])FilesAccess.db.tournamentGames.Select("championshipId=" + tournId + " and roundId='-1'")).ToList();


                for (int i = 0; i < trDelete.Count; i++)
                {
                    tournamentgamesRow aux = FilesAccess.db.tournamentGames.Single(x => x == trDelete[i]);
                    if (aux.gameId > 0)
                    {
                        FilesAccess.db.games.Single(x => x.id == aux.gameId).Delete();

                        if (!FilesAccess.db.UpdateGames())
                        {
                            MessageBox.Show("Erro ao apagar as entradas de jogos anteriores. \nNovos mapas nao inseridos!", "Informacao");
                            return;
                        }
                        FilesAccess.db.games.AcceptChanges();
                    }
                    FilesAccess.db.tournamentGames.Single(x => x == trDelete[i]).Delete();
                }

                if (!FilesAccess.db.UpdateTournamentGames())
                {
                    MessageBox.Show("Erro ao apagar as entradas de jogos anteriores. \nNovos mapas finais nao inseridos!", "Informacao");
                    return;
                }
                FilesAccess.db.tournamentGames.AcceptChanges();

                
            }

            ///Map is generated correctly until 16 player's map
            List<FinalMapGame> finalMap = Generate();

            //add to the DB
            for (int i = 0; i < finalMap.Count; i++)
            {

                //need to add the winner of game Id instead of just the index
                long plA = finalMap[i].PlayerAId;
                long plB = finalMap[i].PlayerBId;
                long dependPrevA=0;
                long dependPrevB=0;

                if (plA <= 0)
                {
                    //add winner of game
                    //need to get the tournamentGameId by using the value of WinnerGame-3 and WinnerGame-4. Search the players that were in the game and compare to the tournamentGames row, and then use that row!

                    //need to check when it has the gameId, don't need to find it
                    FinalMapGame prevFinalGameMap = finalMap[(int)-plA];
                    plA = -prevFinalGameMap.GameId_database;
                    dependPrevA = plA;
                    if (FilesAccess.db.tournamentGames.Single(x => x.id == (-plA)).gameId > 0)
                    {
                        plA = FilesAccess.db.games.Single(x => x.id == FilesAccess.db.tournamentGames.Single(a => a.id == (-plA)).gameId).IdPlayerWinner;
                    }
                }

                if (plB <= 0)
                {
                    //add winner of game
                    FinalMapGame prevFinalGameMap = finalMap[(int)-plB];
                    plB = -prevFinalGameMap.GameId_database;
                    dependPrevB = plB;
                    if (FilesAccess.db.tournamentGames.Single(x => x.id == (-plB)).gameId > 0)
                    {
                        plB = FilesAccess.db.games.Single(x => x.id == FilesAccess.db.tournamentGames.Single(a => a.id == (-plB)).gameId).IdPlayerWinner;
                    }
                }

                //round 0->initial round of final map
                phaseRow phRow = GetPhase(finalMap,i);
                tournamentgamesRow tgRow = FilesAccess.db.tournamentGames.AddtournamentgamesRow(champId, -1, DateTime.Now, (int)plA, (int)plB, -1, 0, 0, 0, 0, 0, 0, 0, phRow.id, (int)dependPrevA, (int)dependPrevB);
                FilesAccess.db.InsertTournamentGame(tgRow);

                if (plA == Utilities.ByePlayer || plB == Utilities.ByePlayer)
                {
                    long idWinner = plA == Utilities.ByePlayer ? plB : plA;
                    DataSet2.gamesRow gameRow = FilesAccess.db.games.AddgamesRow(plA, plB, 0, 0, idWinner, tgRow.Time, tgRow.championshipId, 0, 0, 0, phRow.id, 0, true);
                    FilesAccess.db.InsertGame(gameRow);

                    tgRow.gameId = gameRow.id;

                    FilesAccess.db.UpdateTournamentGames();
                }

                finalMap[i].GameId_database = FilesAccess.db.tournamentGames.Last().id;
            }

        }

        private phaseRow GetPhase(List<FinalMapGame> finalMap, int i)
        {
            int roundAnalyse = finalMap[i].Round;

            int countGames = finalMap.Count(x => x.Round == roundAnalyse);

            switch (countGames)
            {
                case 32:
                    return FilesAccess.db.phase.Single(x => x.phase == "Last 64");
                case 16:
                    return FilesAccess.db.phase.Single(x => x.phase == "Last 32");
                case 8:
                    return FilesAccess.db.phase.Single(x => x.phase == "Last 16");
                case 4:
                    return FilesAccess.db.phase.Single(x => x.phase == "Quarter-Final");
                case 2:
                    return FilesAccess.db.phase.Single(x => x.phase == "Semi-Final");
                case 1:
                    return FilesAccess.db.phase.Single(x => x.phase == "Final");
                default:
                    return FilesAccess.db.phase.Single(x => x.phase == "N/A");
            }
        }

        private List<FinalMapGame> Generate()
        {
            int numPlayersPassing = numPlayersPass * (FilesAccess.db.tournamentGames.Last(x => x.championshipId == champId && x.groupId>-1).groupId+1);
            
            //get first and put at first position
            List<tournamentgroupRow> tgRows = ((tournamentgroupRow[])FilesAccess.db.tournamentGroup.Select("champId=" + champId)).ToList();
            List<playertourngroupRow> ptgRows = new List<playertourngroupRow>();
            for (int i = 0; i < tgRows.Count; i++)
            {
                ptgRows.AddRange(((playertourngroupRow[])FilesAccess.db.playerTournGroup.Select("tournGroupId=" + tgRows[i].id)).ToList());
            }

            ptgRows = ptgRows.OrderBy(x => x.place).ThenBy(x => x.tournGroupId).ToList();

            List<Tuple<int, playerRow>> positions = new List<Tuple<int, playerRow>>();
            for (int i = 0; i < ptgRows.Count; i++)
            {
                positions.Add(new Tuple<int, playerRow>(i, FilesAccess.db.player.Single(x => x.Id == ptgRows[i].playerId)));
            }

            positions = positions.Take(numPlayersPassing).ToList();


            while (!validPlayerMaps.Contains(numPlayersPassing))
                numPlayersPassing++;

            while (positions.Count < numPlayersPassing)
            {
                positions.Add(new Tuple<int, playerRow>(positions.Count, FilesAccess.db.player.Single(x=>x.Id== Utilities.ByePlayer)));
            }

            List<FinalMapGame> finalMapGame = FillRoundZero(positions);

            int numGames=0;

            int temp=positions.Count/2;
            int round=1;
            int prev = 0;

            while(temp > 1)
            {
                temp/=2;
                numGames += temp;

                int startIndexGame = 0;
                int gamesPrevLevel = finalMapGame.Count(x => x.Round == round - 1);
                if (gamesPrevLevel > 0)
                {
                    startIndexGame = finalMapGame.First(x => x.Round == round - 1).GameNumber;
                    prev = startIndexGame;
                }

                for (int i = prev; i < prev + gamesPrevLevel; i += 2)
                {
                    //finalMapGame.Add(new FinalMapGame(finalMapGame.Count, round, finalMapGame[i].GameNumber, finalMapGame[i + 1].GameNumber));
                    finalMapGame.Add(new FinalMapGame(finalMapGame.Count, round, i, i+1));
                }
                round++;
            }

            return finalMapGame;
        }

        /// <summary>
        /// It is only working for maps until 16 players
        /// </summary>
        /// <param name="positions"></param>
        /// <returns></returns>
        private List<FinalMapGame> FillRoundZero (List<Tuple<int, playerRow>> positions)
        {
            int numGames = positions.Count / 2;
            FinalMapGame[] round0Map = new FinalMapGame[numGames];
            string[] auxPositions = new string[positions.Count];
            for (int i = 0; i < positions.Count; i++)
            {
                auxPositions[i]=("Pl" + (i+1));
            }

            int currentIndex = positions.Count-1;

            int indexBottom = 0;

            int indexTop = numGames - 1;

            bool swap = true;

            int divider = 0;
            switch (numGames)
            {
                case 2:
                case 4:
                case 8:
                    divider = 2;
                    break;

                case 16:
                case 32:
                    divider = 2;
                    break;
            }
            int tmp = 0;
            for (int i = 0; i < numGames;)
            {
                int indexA = i;
                int indexB = (currentIndex--);

                round0Map[indexBottom] = (new FinalMapGame(i, 0, positions[indexA].Item2, positions[indexB].Item2));
                //round0Map[indexBottom] = (new FinalMapGame(i, 0, auxPositions[indexA], auxPositions[indexB]));
                i++;
                if (i == numGames)
                    break;

                indexA = i;
                indexB = (currentIndex--);

                round0Map[indexTop] = (new FinalMapGame(i, 0, positions[indexA].Item2, positions[indexB].Item2));
                //round0Map[indexTop] = (new FinalMapGame(i, 0, auxPositions[i], auxPositions[indexB]));
                i++;
                if (i == numGames)
                    break;

                if (!swap)
                {
                    indexBottom = indexBottom - (numGames / (divider*2));
                    indexTop = indexTop + (numGames / (divider * 2));
                }
                else
                {
                    indexBottom = indexBottom + (numGames / divider);
                    indexTop = indexTop - (numGames / divider);
                }
                swap = !swap;
            }

            return round0Map.ToList();
        }
    }

    public class FinalMapGame
    {
        public int GameId_database;

        int gameNumber;
        int round;
        int winnerGame1;
        int winnerGame2;
        int idWinner;
        playerRow plA;
        playerRow plB;

        public int GameWinner { get { return idWinner; } }
        public int GameNumber { get { return gameNumber; } }
        public int Round { get { return round; } }

        string testA;
        string testB;

        public int PlayerAId
        {
            get 
            {
                if (plA == null)
                    return -winnerGame1;
                return plA.Id;
            }
        }

        public int PlayerBId
        {
            get
            {
                if (plB == null)
                    return -winnerGame2;
                return plB.Id;
            }
        }

        public FinalMapGame(int gmNumber, int _round, int _winnerGame1, int _winnerGame2)
        {
            gameNumber = gmNumber;
            round = _round;
            winnerGame1 = _winnerGame1;
            winnerGame2 = _winnerGame2;


        }

        public FinalMapGame(int gmNumber, int _round, playerRow _plA, playerRow _plB)
        {
            gameNumber = gmNumber;
            round = _round;
            plA = _plA;
            plB = _plB;
        }

        public FinalMapGame(int gmNumber, int _round, string _plA, string _plB)
        {
            gameNumber = gmNumber;
            round = _round;
            testA = _plA;
            testB = _plB;
        }

        public void SetPlayers(playerRow _plA, playerRow _plB)
        {
            plA = _plA;
            plB = _plB;
        }

        public void SetWinner(int idPlayerWinner)
        {
            idWinner = idPlayerWinner;
        }
    }
}
