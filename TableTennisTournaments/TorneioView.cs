﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TableTennis.Database;
using TableTennis.Controls;
using TableTennis.Controls.Tournaments;
using static TableTennis.Database.DataSet2;

namespace TableTennis.Tournaments
{
    public partial class TorneioView : Form
    {
        ManualTournamentGroup mtg;
        public TorneioView()
        {
            InitializeComponent();

            flowLayoutPanel2.VerticalScroll.Enabled = true;
            flowLayoutPanel2.HorizontalScroll.Enabled = false;

            flowLayoutPanel1.VerticalScroll.Enabled = true;
            flowLayoutPanel1.HorizontalScroll.Enabled = false;

            for (int i = 0; i < Utilities.AllOpenTournaments.Count; i++)
                comboBox1.Items.Add(FilesAccess.db.championships.Single(x=>x.Id == Utilities.AllOpenTournaments[i]).Name);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                championshipsRow champRow = FilesAccess.db.championships.Single(x => x.Name == comboBox1.Text);   
                GenerateDraw a = new GenerateDraw(champRow.Id, Convert.ToInt32(textBox1.Text));
                comboBox1_SelectedIndexChanged(this, new EventArgs());
            }
                
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            flowLayoutPanel1.Controls.Clear();
            flowLayoutPanel2.Controls.Clear();
            List<GroupControl> groupControls = new List<GroupControl>();
            List<gameScheduleControl> gameSchl = new List<gameScheduleControl>();
            championshipsRow champRow = FilesAccess.db.championships.Single(x => x.Name == comboBox1.Text);

            List<playerleaguetournamentRow> pltRows = ((playerleaguetournamentRow[])FilesAccess.db.playerLeagueTournament.Select("championshipId="+champRow.Id +" and confirmInscription=1")).ToList();

            label4.Text = pltRows.Count.ToString();
            listBox1.Items.Clear();
            for (int i = 0; i < pltRows.Count; i++)
            {
                playerRow pl = FilesAccess.db.player.Single(x=>x.Id == pltRows[i].playerId);
                listBox1.Items.Add(pl.Name);
            }

            if (FilesAccess.db.tournamentGames.Select("championshipId=" + champRow.Id).Count() > 0)
            {
               int numGroups = ((tournamentgamesRow[])FilesAccess.db.tournamentGames.Select("championshipId=" + champRow.Id)).Max(x=>x.groupId) +1;
                for (int i = 0; i < numGroups; i++)
                {
                    groupControls.Add(new GroupControl(champRow.Id, i));
                    flowLayoutPanel1.Controls.Add(groupControls.Last());
                }
            }

            List<tournamentgamesRow> tgRows = ((tournamentgamesRow[])FilesAccess.db.tournamentGames.Select("championshipId=" + champRow.Id)).OrderBy(x=>x.roundId).ToList();

            for (int i = 0; i < tgRows.Count; i++)
            {
                gameSchl.Add(new gameScheduleControl(tgRows[i].id));
                gameSchl.Last().UpdateAll += TorneioView_UpdateAll;
                flowLayoutPanel2.Controls.Add(gameSchl.Last());
            }
        }

        private void TorneioView_UpdateAll(object sender, EventArgs e)
        {
            //foreach (Control con in flowLayoutPanel1.Controls)
            //{
            //    ((GroupControl)con).UpdateControl();
            //}

            //foreach (Control con in flowLayoutPanel2.Controls)
            //{
            //    ((gameScheduleControl)con).UpdateControl();
            //}
            comboBox1_SelectedIndexChanged(this, new EventArgs());
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                new GenerateFinalMap(FilesAccess.db.championships.Single(x => x.Name == comboBox1.Text).Id, Convert.ToInt32(textBox2.Text));
                comboBox1_SelectedIndexChanged(this, new EventArgs());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            mtg = new ManualTournamentGroup();
            mtg.CreateManualTournament +=mtg_CreateManualTournament;
            mtg.Show();
        }

        private void mtg_CreateManualTournament(object sender, EventArgs e)
        {
            List<Tuple<int, playerRow>> inscriptions = mtg.GetInscriptions;
            List<TournamentGroup> groups = new List<TournamentGroup>();

            GenerateGames genGames = new GenerateGames();
            List<playerleaguetournamentRow> pltRow = new List<playerleaguetournamentRow>();

            int a = 0;
            for (int i = 0; i < inscriptions.Count+1; i++)
            {
                if (i < inscriptions.Count)
                {
                    if (inscriptions[i].Item1 == a)
                    {
                        pltRow.Add(FilesAccess.db.playerLeagueTournament.Single(x => x.playerId == inscriptions[i].Item2.Id && x.championshipId == mtg.ChampRow.Id));
                    }
                    else
                    {
                        List<TournamentGame> tournamentGames = genGames.AllGainstAll(mtg.ChampRow, pltRow, inscriptions[i-1].Item1);
                        groups.Add(new TournamentGroup(inscriptions[i-1].Item1, tournamentGames));
                        a++;
                        pltRow = new List<playerleaguetournamentRow>();
                    }
                }
                else
                {
                    List<TournamentGame> tournamentGames = genGames.AllGainstAll(mtg.ChampRow, pltRow, inscriptions[i-1].Item1);
                    groups.Add(new TournamentGroup(inscriptions[i-1].Item1, tournamentGames));
                    a++;
                    pltRow = new List<playerleaguetournamentRow>();
                    break;
                }
            }

            for(int j=0; j<groups.Count;j++)
            {
                for (int i = 0; i < groups[j].Games.Count; i++)
                {
                    if (groups[j].Games[i].PlayerA != null && groups[j].Games[i].PlayerB != null)
                    {
                        tournamentgamesRow tgRow = FilesAccess.db.tournamentGames.AddtournamentgamesRow(
                            mtg.ChampRow.Id, groups[j].Games[i].Round, groups[j].Games[i].Time, groups[j].Games[i].PlayerA.Id, groups[j].Games[i].PlayerB.Id, groups[j].GroupId, 0, 0, 0, 0, 0, 0, 0, groups[j].PhaseId, 0, 0);
                        FilesAccess.db.InsertTournamentGame(tgRow);
                    }
                }
            }
        }
    }
}
