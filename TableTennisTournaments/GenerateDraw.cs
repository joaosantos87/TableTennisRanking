﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TableTennis.Database;
using System.Windows.Forms;
using static TableTennis.Database.DataSet2;

namespace TableTennis.Tournaments
{
    public class ResumeDraw
    {
        championshipsRow tournament;
        public TournamentResume tournamentResume;

        public ResumeDraw(int tournamentId)
        {
            //display the tournament

            List<tournamentgamesRow> tgRows =
            ((tournamentgamesRow[])FilesAccess.db.tournamentGames.Select("championshipId=" + tournament.Id)).ToList();
            List<TournamentGroup> tgroupRows = new List<TournamentGroup>();
            List<TournamentGame> allGames = new List<TournamentGame>();

            int groupid = tgRows[0].groupId;
            //check the tournament game
            for (int i = 0; i < tgRows.Count; i++)
            {
                playerleaguetournamentRow pltRowA = FilesAccess.db.playerLeagueTournament.Single(x => x.playerId == tgRows[i].playerAid && x.championshipId == tgRows[i].championshipId);
                playerleaguetournamentRow pltRowB = FilesAccess.db.playerLeagueTournament.Single(x => x.playerId == tgRows[i].playerBid && x.championshipId == tgRows[i].championshipId);
                TournamentGame tg = new TournamentGame(tgRows[i].Time, tgRows[i].roundId, pltRowA, pltRowB);
                allGames.Add(tg);

                if (groupid != tgRows[i].groupId || i == tgRows.Count - 1)
                {
                    tgroupRows.Add(new TournamentGroup(tgRows[i].groupId, allGames));
                    allGames = new List<TournamentGame>();
                    groupid++;
                }
            }

            tournamentResume = new TournamentResume(tournament, tgroupRows);
        }
    }

    public class GenerateDraw
    {
        championshipsRow tournament;
        public TournamentResume tournamentResume;
        List<playerRow> players;
        GenerateGames genGames = new GenerateGames();

        public GenerateDraw(int tournamentId, int idealPlayerPerGroup)
        {
            //Create the general ranking based on the League Ranking
            List<int> allOpenChamps = new List<int>();
            allOpenChamps.AddRange(Utilities.AllOpenTournaments);
            players = Ranking.Instance.UpdateRanking(new DateTime(2000, 1, 1), DateTime.Now, allOpenChamps);

            tournament = FilesAccess.db.championships.Single(x => x.Id == tournamentId);

            //In here all players ranked in the league
            List<playerleaguetournamentRow> playerInTournament = ((playerleaguetournamentRow[])FilesAccess.db.playerLeagueTournament.Select("championshipId=" + tournamentId + " AND confirmInscription=true")).ToList();

            int previousEntries = FilesAccess.db.tournamentGames.Count(x => x.championshipId == tournamentId);

            DialogResult dr = MessageBox.Show("Ja estão inseridos " + previousEntries + " jogos deste torneio.\nDeseja apagar e gerar novos mapas?", "Information", MessageBoxButtons.YesNo);

            if (dr == System.Windows.Forms.DialogResult.No)
            {
                return;
            }

            ///If there exists, then delete them
            List<tournamentgamesRow> trDelete = 
                ((tournamentgamesRow[])FilesAccess.db.tournamentGames.Select("championshipId=" + tournament.Id)).ToList();

            List<tournamentgroupRow> tgDelete = ((tournamentgroupRow[])FilesAccess.db.tournamentGroup.Select("champId=" + tournament.Id)).ToList();
            List<playertourngroupRow> ptgDelete= new List<playertourngroupRow>();
            for (int i = 0; i < tgDelete.Count; i++)
            {
                ptgDelete.AddRange(((playertourngroupRow[])FilesAccess.db.playerTournGroup.Select("tournGroupId=" + tgDelete[i].id)).ToList());
            }

            for (int i = 0; i < trDelete.Count; i++)
            {
                if (FilesAccess.db.tournamentGames.Single(x => x == trDelete[i]).gameId > 0)
                    FilesAccess.db.games.Single(x => x.id == FilesAccess.db.tournamentGames.Single(a => a == trDelete[i]).gameId).Delete();
                FilesAccess.db.tournamentGames.Single(x => x == trDelete[i]).Delete();
            }

            for (int i = 0; i < tgDelete.Count; i++)
                FilesAccess.db.tournamentGroup.Single(x => x == tgDelete[i]).Delete();

            for (int i = 0; i < ptgDelete.Count; i++)
                FilesAccess.db.playerTournGroup.Single(x => x == ptgDelete[i]).Delete();

            if (!FilesAccess.db.UpdateTournamentGames())
            {
                MessageBox.Show("Erro ao apagar as entradas de jogos anteriores. \nNovos mapas nao inseridos!", "Informacao");
                return;
            }
            FilesAccess.db.tournamentGames.AcceptChanges();

            if (!FilesAccess.db.UpdateTournamentGroup())
            {
                MessageBox.Show("Erro ao apagar as entradas de jogos anteriores. \nNovos mapas nao inseridos!", "Informacao");
                return;
            }
            FilesAccess.db.tournamentGroup.AcceptChanges();

            if (!FilesAccess.db.UpdatePlayerTournamentGroup())
            {
                MessageBox.Show("Erro ao apagar as entradas de jogos anteriores. \nNovos mapas nao inseridos!", "Informacao");
                return;
            }
            FilesAccess.db.playerTournGroup.AcceptChanges();

            if (!FilesAccess.db.UpdateGames())
            {
                MessageBox.Show("Erro ao apagar as entradas de jogos anteriores. \nNovos mapas nao inseridos!", "Informacao");
                return;
            }
            FilesAccess.db.games.AcceptChanges();

            //Generate new games
            List<TournamentGroup> tg = ChooseGroups(playerInTournament, idealPlayerPerGroup);
            tournamentResume = new TournamentResume(tournament, tg);

            for (int i = 0; i < tournamentResume.Groups.Count; i++)
            {
                for (int j = 0; j < tournamentResume.Groups[i].Games.Count; j++)
                {
                    if (tournamentResume.Groups[i].Games[j].PlayerA != null && tournamentResume.Groups[i].Games[j].PlayerB != null)
                    {
                        FilesAccess.db.tournamentGames.AddtournamentgamesRow(
                            tournamentResume.tournamentRow.Id,
                            tournamentResume.Groups[i].Games[j].Round,
                                tournamentResume.Groups[i].Games[j].Time,
                        tournamentResume.Groups[i].Games[j].PlayerA.Id,
                            tournamentResume.Groups[i].Games[j].PlayerB.Id,
                            tournamentResume.Groups[i].GroupId,
                            0,0,0,0,0,0,0,
                            Utilities.Phase_Group,
                            0,0);

                        FilesAccess.db.UpdateTournamentGames();
                    }
                }
            }
        }

        private List<TournamentGroup> ChooseGroups(List<playerleaguetournamentRow> players, int playersPerGroupDesired)
        {
            List<TournamentGroup> allGroups = new List<TournamentGroup>();
            int currentPlayers = players.Count;
            int numberOfGroups = TryGroups(players, playersPerGroupDesired);
            int playersPerGroup = currentPlayers / numberOfGroups;

            //need to order the players according to its ranking
            List<GroupPosition> playersPosition = GetPlayersPerRanking(players, numberOfGroups);

            for (int i = 0; i < numberOfGroups; i++)
            {
                ///If the data is not even, then need to do a "add more" value
                int addMore=0;
                if (i == numberOfGroups - 1)
                {
                    if(currentPlayers != playersPerGroup*numberOfGroups)
                        addMore = currentPlayers-(playersPerGroup*numberOfGroups);
                }
                List<TournamentGame> allGames = new List<TournamentGame>();

                //skip and take need to be done taking in account the ranking

                //List<DataSet1.PlayerLeagueTournamentRow> pltRows = players.Skip(i * playersPerGroup).Take(playersPerGroup + addMore).ToList();
                List<playerleaguetournamentRow> pltRows = new List<playerleaguetournamentRow>();
                for(int h=0; h<playersPosition.Count;h++)
                {
                    if(i==playersPosition[h].groupId)
                        pltRows.Add(playersPosition[h].player);
                }

                allGames.AddRange(genGames.AllGainstAll(tournament, pltRows, i));

                allGroups.Add(new TournamentGroup(i, allGames));
            }

            return allGroups;
        }

        private List<GroupPosition> GetPlayersPerRanking(List<playerleaguetournamentRow> playerInTournament, int numGroups)
        {
            int i = 0;
            for (i = 0; i < playerInTournament.Count; i++)
            {
                playerinscriptionleagueRow pilRow = FilesAccess.db.playerInscriptionLeague.Single(x=>x.playerId == playerInTournament[i].playerId);
                playerInTournament[i].currentPoints = pilRow.currentPoints;
                playerInTournament[i].currentGamesWon = pilRow.currentGamesWon;
                playerInTournament[i].currentGamesLost = pilRow.currentGamesLost;
                playerInTournament[i].currentRank = pilRow.currentRank;
            }

            playerInTournament = playerInTournament.OrderBy(x => x.currentRank).ToList();

            List<GroupPosition> groupPosition = new List<GroupPosition>();

            int j = 0;
            i = 0;
            int min = 0;
            int max = numGroups-1;
            bool isTopDown = true;
            while (j < playerInTournament.Count)
            {
                for (i = min; isTopDown == true ? i <= max : i >= max; )
                {
                    if (j == playerInTournament.Count)
                    {
                        j++;
                        break;
                    }
                    groupPosition.Add(new GroupPosition(i, playerInTournament[j]));
                    j++;
                    if (isTopDown)
                        i++;
                    else
                        i--;
                }
                if (isTopDown)
                {
                    min = numGroups-1;
                    max = 0;
                }
                else
                {
                    min = 0;
                    max = numGroups-1;
                }
                isTopDown = !isTopDown;
            }
            groupPosition = groupPosition.OrderBy(x => x.player.currentRank).OrderBy(x => x.groupId).ToList();

            return groupPosition;
        }

        /// <summary>
        /// return the number of ideal groups to be made
        /// </summary>
        /// <param name="aux"></param>
        /// <param name="playersPerGroup"></param>
        /// <returns></returns>
        private int TryGroups(List<playerleaguetournamentRow> aux, int idealNumber)
        {
            List<int> numberOfPlayersPerGroup = new List<int>();

            for (int h = 0; h < aux.Count; h++)
            {
                List<Tuple<int, int>> organization = new List<Tuple<int, int>>();
                for (int i = 1, j = 0; j < aux.Count; i++, j++)
                {
                    organization.Add(new Tuple<int, int>(i, aux[j].playerId));
                    if (i == h)
                        i = 0;
                }
                numberOfPlayersPerGroup.Add(organization.Count(x => x.Item1 == 1));
            }

            while (true)
            {
                if(numberOfPlayersPerGroup.Contains(idealNumber))
                    return numberOfPlayersPerGroup.IndexOf(idealNumber);

                idealNumber--;
            }
        }

        //private List<TournamentGame> AllGainstAll(List<DataSet1.PlayerLeagueTournamentRow> players, int groupId)
        //{
        //    DateTime currentTournamentTime = new DateTime(tournament.Date.Year,tournament.Date.Month,tournament.Date.Day,20,0,0);

        //    int max = players.Count - 1;

        //    //for each round, the number 1 statys in the same position, and the only rotate clockwise

        //    List<TournamentGame> tournamentGame = new List<TournamentGame>();

        //    List<int> indexes = new List<int>();
        //    for (int i = 0; i < players.Count; i++)
        //    {
        //        indexes.Add(players[i].playerId);
        //    }

        //    List<Tuple<int, int, int>> gamesSchedule = ListMatches(indexes);

        //    for (int i = 0; i < gamesSchedule.Count; i++)
        //    {
        //        DataSet1.PlayerLeagueTournamentRow plA;
        //        DataSet1.PlayerLeagueTournamentRow plB;

        //        if(gamesSchedule[i].Item1>-1)
        //            plA = players.Single(x=>x.playerId == gamesSchedule[i].Item1);
        //        else
        //            plA = null;

        //        if(gamesSchedule[i].Item2>-1)
        //            plB = players.Single(x=>x.playerId == gamesSchedule[i].Item2);
        //        else
        //            plB = null;

        //        if(i>0)
        //        {
        //            if (gamesSchedule[i].Item3 != gamesSchedule[i - 1].Item3)
        //                currentTournamentTime = currentTournamentTime.AddMinutes(15);
        //        }

        //        tournamentGame.Add(new TournamentGame(currentTournamentTime, gamesSchedule[i].Item3, plA, plB));
        //    }

        //    return tournamentGame;
        //}

        

        private List<playerleaguetournamentRow> OrderByRank(List<playerleaguetournamentRow> players)
        {
            return players.OrderBy(x => x.currentRank).ToList();
        }

        private List<playerleaguetournamentRow> RandomPlayers(List<playerleaguetournamentRow> players)
        {
            return null;
        }
    }

    public class GenerateGames
    {
        public GenerateGames()
        {

        }

        public List<TournamentGame> AllGainstAll(championshipsRow tournament, List<playerleaguetournamentRow> players, int groupId)
        {
            DateTime currentTournamentTime = new DateTime(tournament.Date.Year,tournament.Date.Month,tournament.Date.Day,20,0,0);

            int max = players.Count - 1;

            //for each round, the number 1 statys in the same position, and the only rotate clockwise

            List<TournamentGame> tournamentGame = new List<TournamentGame>();

            List<int> indexes = new List<int>();
            for (int i = 0; i < players.Count; i++)
            {
                indexes.Add(players[i].playerId);
            }

            List<Tuple<int, int, int>> gamesSchedule = ListMatches(indexes);

            for (int i = 0; i < gamesSchedule.Count; i++)
            {
                playerleaguetournamentRow plA;
                playerleaguetournamentRow plB;

                if(gamesSchedule[i].Item1>-1)
                    plA = players.Single(x=>x.playerId == gamesSchedule[i].Item1);
                else
                    plA = null;

                if(gamesSchedule[i].Item2>-1)
                    plB = players.Single(x=>x.playerId == gamesSchedule[i].Item2);
                else
                    plB = null;

                if(i>0)
                {
                    if (gamesSchedule[i].Item3 != gamesSchedule[i - 1].Item3)
                        currentTournamentTime = currentTournamentTime.AddMinutes(15);
                }

                tournamentGame.Add(new TournamentGame(currentTournamentTime, gamesSchedule[i].Item3, plA, plB));
            }

            return tournamentGame;
        }

        private List<Tuple<int, int, int>> ListMatches(List<int> ListPlayers)
        {
            if (ListPlayers.Count % 2 != 0)
            {
                ListPlayers.Add(-1);
            }

            int numTeams = ListPlayers.Count;
            int numGames = (numTeams - 1);
            int halfSize = numTeams / 2;

            List<Tuple<int, int, int>> order = new List<Tuple<int, int, int>>();

            List<int> teams = new List<int>();

            teams.AddRange(ListPlayers.Skip(halfSize).Take(halfSize));
            teams.AddRange(ListPlayers.Skip(1).Take(halfSize - 1).ToArray().Reverse());

            int teamsSize = teams.Count;

            int round = 0;

            for (int i = 0; i < numGames; i++)
            {
                round = i + 1;

                int teamIdx = i % teamsSize;

                order.Add(new Tuple<int, int, int>(teams[teamIdx], ListPlayers[0], round));

                for (int idx = 1; idx < halfSize; idx++)
                {
                    int firstTeam = (i + idx) % teamsSize;
                    int secondTeam = (i + teamsSize - idx) % teamsSize;

                    order.Add(new Tuple<int, int, int>(teams[firstTeam], teams[secondTeam], round));
                }
            }
            return order;
        }
    }
}
