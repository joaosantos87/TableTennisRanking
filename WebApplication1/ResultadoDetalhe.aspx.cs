﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using TableTennis;
using TableTennis.Database;
using static TableTennis.Database.DataSet2;

namespace WebApplication1
{
    public partial class ResultadoDetalhe : System.Web.UI.Page
    {
        int idTeamGame;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack && Request.UrlReferrer!=null)
                ViewState["RefUrl"] = Request.UrlReferrer.ToString();
            
            idTeamGame = Page.RouteData.Values.ContainsKey("Id") ?  Int32.TryParse(Page.RouteData.Values["Id"].ToString(), out idTeamGame)? idTeamGame : 0 : 0;

            seasonRow season = Page.RouteData.Values.ContainsKey("Epoca") ? FilesAccess.db.seasons.Single(x => x.name == Page.RouteData.Values["Epoca"].ToString()) : FilesAccess.db.seasons.Single(x => x.name == "TodasEpocas");


            if (idTeamGame > 0)
            {
                var teamGame = FilesAccess.db.teamGames.SingleOrDefault(x => x.Id == idTeamGame);
                nomeTorneio.InnerText = FilesAccess.db.championships.SingleOrDefault(x => x.Id == teamGame.IdChampionship).Name;
                jornada.InnerText = teamGame.Day != 0 ? "Jornada " + teamGame.Day : "";
                gameName.InnerText = FilesAccess.db.clubs.SingleOrDefault(x => x.Id == teamGame.ClubAId).Name + " " + teamGame.GetScoreA() + " - " + teamGame.GetScoreB() + " " + FilesAccess.db.clubs.SingleOrDefault(x => x.Id == teamGame.ClubBId).Name;
                
                var dataSingles = from g in FilesAccess.db.games
                           where (g.IdTeamGame == teamGame.Id)
                           select new ListDetailGame()
                           {
                               Id = g.id,
                               Champ = FilesAccess.db.championships.SingleOrDefault(x => x.Id == teamGame.IdChampionship).Name,
                               ChampId = teamGame.IdChampionship,
                               Season = season.name,
                               IdPlayerA = g.IdPlayerA,
                               IdPlayerB = g.IdPlayerB,
                               JogadorA = FilesAccess.db.player.SingleOrDefault(x => x.Id == g.IdPlayerA).Name,
                               JogadorB = FilesAccess.db.player.SingleOrDefault(x => x.Id == g.IdPlayerB).Name,
                               Resultado = g.SetsA + " - " + g.SetsB,
                               PontosA = Utilities.GetRankPoints(FilesAccess.db.ranking.FirstOrDefault(x => x.idChamp == g.IdChamp && x.idPlayer == g.IdPlayerA && x.idSeason == season.id)),
                               PontosB = Utilities.GetRankPoints(FilesAccess.db.ranking.FirstOrDefault(x => x.idChamp == g.IdChamp && x.idPlayer == g.IdPlayerB && x.idSeason == season.id)),
                               RankA = Utilities.GetRankPlace(FilesAccess.db.ranking.FirstOrDefault(x => x.idChamp == g.IdChamp && x.idPlayer == g.IdPlayerA && x.idSeason == season.id)),
                               RankB = Utilities.GetRankPlace(FilesAccess.db.ranking.FirstOrDefault(x => x.idChamp == g.IdChamp && x.idPlayer == g.IdPlayerB && x.idSeason == season.id)),
                               VerHistorico = FilesAccess.db.player.SingleOrDefault(x => x.Id == g.IdPlayerA).Name + " VS "+FilesAccess.db.player.SingleOrDefault(x => x.Id == g.IdPlayerB).Name
                           };

                if (FilesAccess.db.doubleGames.Count(x => x.IdTeamGame == teamGame.Id) > 0)
                {
                    var dataDoubles = from dg in FilesAccess.db.doubleGames
                                      where (dg.IdTeamGame == teamGame.Id)
                                      select new ListDetailGame()
                                      {
                                          Id = dg.Id,
                                          Champ = FilesAccess.db.championships.SingleOrDefault(x => x.Id == teamGame.IdChampionship).Name,
                                          ChampId = teamGame.IdChampionship,
                                          IdPlayerA = dg.DoubleA_PlayerAId << 16 | dg.DoubleA_PlayerBId,
                                          IdPlayerB = dg.DoubleB_PlayerAId << 16 | dg.DoubleB_PlayerBId,
                                          JogadorA = FilesAccess.db.player.SingleOrDefault(x => x.Id == dg.DoubleA_PlayerAId).Name + "-" + FilesAccess.db.player.SingleOrDefault(x => x.Id == dg.DoubleA_PlayerBId).Name,
                                          JogadorB = FilesAccess.db.player.SingleOrDefault(x => x.Id == dg.DoubleB_PlayerAId).Name + "-" + FilesAccess.db.player.SingleOrDefault(x => x.Id == dg.DoubleB_PlayerBId).Name,
                                          Resultado = dg.SetsA + " - " + dg.SetsB,
                                          PontosA = (Utilities.GetRankPoints(FilesAccess.db.ranking.SingleOrDefault(x => x.idChamp == dg.IdChampionship && x.idPlayer == dg.DoubleA_PlayerAId && x.idSeason == season.id)) + Utilities.GetRankPoints(FilesAccess.db.ranking.SingleOrDefault(x => x.idChamp == dg.IdChampionship && x.idPlayer == dg.DoubleA_PlayerBId && x.idSeason == season.id))) / 2,
                                          PontosB = (Utilities.GetRankPoints(FilesAccess.db.ranking.SingleOrDefault(x => x.idChamp == dg.IdChampionship && x.idPlayer == dg.DoubleB_PlayerAId && x.idSeason == season.id)) + Utilities.GetRankPoints(FilesAccess.db.ranking.SingleOrDefault(x => x.idChamp == dg.IdChampionship && x.idPlayer == dg.DoubleB_PlayerBId && x.idSeason == season.id))) / 2,

                                          RankA = (Utilities.GetRankPlace(FilesAccess.db.ranking.SingleOrDefault(x => x.idChamp == dg.IdChampionship && x.idPlayer == dg.DoubleA_PlayerAId && x.idSeason == season.id)) + Utilities.GetRankPlace(FilesAccess.db.ranking.SingleOrDefault(x => x.idChamp == dg.IdChampionship && x.idPlayer == dg.DoubleA_PlayerBId && x.idSeason == season.id))) / 2,

                                          RankB = (Utilities.GetRankPlace(FilesAccess.db.ranking.SingleOrDefault(x => x.idChamp == dg.IdChampionship && x.idPlayer == dg.DoubleB_PlayerAId && x.idSeason == season.id)) + Utilities.GetRankPlace(FilesAccess.db.ranking.SingleOrDefault(x => x.idChamp == dg.IdChampionship && x.idPlayer == dg.DoubleB_PlayerBId && x.idSeason == season.id))) / 2,

                                      };
                
                    GridView1.DataSource = dataDoubles.Union(dataSingles);
                    GridView1.DataBind();
                }
                else
                {
                    GridView1.DataSource = dataSingles;
                    GridView1.DataBind();
                }
            }
        }
        

        protected void Button1_Click(object sender, EventArgs e)
        {
            object refUrl = ViewState["RefUrl"];
            if (refUrl != null)
                Response.Redirect((string)refUrl);
        }
    }
}