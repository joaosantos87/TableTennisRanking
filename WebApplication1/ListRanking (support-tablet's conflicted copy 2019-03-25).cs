﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1
{
    public class ListRanking
    {
        public string startDateField { get; set; }
        public string endDateField { get; set; }
        public string campeonatosField { get; set; }
        public int Id { get; set; }
        public int IdClub { get; set; }
        public int Ranking { get; set; }
        public string Licenca { get; set; }
        public string Nome { get; set; }
        public string Clube { get; set; }
        public int Pontos { get; set; }
        public string Record { get; set; }
        public double WinPercentage { get; set; }
    }
}