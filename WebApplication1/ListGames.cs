﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1
{
    public class ListGamesDoubles : ListGames
    {
        public string Par { get; set; }
        public string ParAdversario { get; set; }
    }
    public class ListGames
    {
        public string Nome { get; set; }
        public int GameType { get; set; }
        public int Id { get; set; }
        public long IdAdv { get; set; }
        public string Data { get; set; }
        public string Adversario { get; set; }
        public string Resultado { get; set; }
        public int Pontos { get; set; }
        public int IdChamp { get; set; }
        public string Campeonato { get; set; }
        public int graphData { get;set; }
        public string VerHistorico { get; set; }
        public int IdSeason { get; set; }
        public string Season { get; set; }
        public DateTime DateGame { get; set; }
        public bool IsWon { get; set; }
        public int IdTeamGame { get; set; }
    }
}