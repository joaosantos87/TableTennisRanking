﻿<%@ Page Title="Jogadores" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Players.aspx.cs" Inherits="WebApplication1.Players" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <br />
    <p>
        &nbsp;
    </p>
    <p>Loading Time: <Label ID="Label1" runat="server"> test </Label></p>
    <asp:Panel ID="Panel1" runat="server" DefaultButton="Button1">
    Procurar novo jogador:
    <asp:TextBox ID="TextBox1" runat="server" Width="210px"></asp:TextBox>
    <asp:Button ID="Button1" runat="server" Text="Pesquisar" OnClick="Button1_Click" />
    <div class="table-responsive">    
        <asp:GridView ID="GridView2" runat="server" Visible="False" AutoGenerateColumns="false" CssClass="table table-sm">
            <Columns>
            <asp:BoundField DataField="Id" HeaderText="Id" Visible="false" InsertVisible="False" ReadOnly="True" SortExpression="Id" />   
            <asp:BoundField DataField="IdClub" HeaderText="IdClub" Visible="false" InsertVisible="False" ReadOnly="True" SortExpression="IdClub" />
                <asp:BoundField DataField="Licenca" HeaderText="Licenca" SortExpression="licenseNo" ItemStyle-HorizontalAlign="Center" />
            <asp:BoundField DataField="CurrentRank" HeaderText="Ranking" SortExpression="currentRank"  ItemStyle-HorizontalAlign="Center"/>
            <asp:HyperLinkField ItemStyle-HorizontalAlign="Center" DataTextField="Nome" HeaderText="Nome" SortExpression="Name" DataNavigateUrlFields="Id, Nome"  DataNavigateUrlFormatString="~/Jogador/{0}-{1}"/>
            <asp:HyperLinkField ItemStyle-HorizontalAlign="Center" DataTextField="Club" HeaderText="Clube" SortExpression="Name" DataNavigateUrlFields="IdClub, Club"  DataNavigateUrlFormatString="~/Clube/{0}-{1}"/>
            <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Record" HeaderText="Record" SortExpression="Record" />
            <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Pontos" HeaderText="Pontos" SortExpression="Pontos" />
            <asp:BoundField Visible="false" DataField="GraphData" />
        </Columns>
        </asp:GridView>
        </div>
    </asp:Panel>
    
        <h3>Ver historico jogos contra:</h3>
        <asp:TextBox ID="TextBox2" runat="server" Width="210px"></asp:TextBox>
        <asp:Button ID="Button2" runat="server" Text="Procurar" OnClick="Button2_Click" />    
    <h1 ID="nomeJogador" runat="server"></h1>
    
    <div class="col-sm-8">
    <table ID="list" runat="server" style="width:100%">
        <tr>
            <td class="auto-style1" style="width: 284px">Licenca</td>
            <td>
                <asp:Label ID="Label10" runat="server" Text="Label" Font-Bold="True"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style1" style="width: 284px">Ranking Global</td>
            <td>
                <asp:Label ID="Label3" runat="server" Text="Label" Font-Bold="True"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style1" style="width: 284px">Record Global</td>
            <td>
                <asp:Label ID="Label4" runat="server" Text="Label" Font-Bold="True"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style1" style="width: 284px">Pontos Global</td>
            <td>
                <asp:Label ID="Label5" runat="server" Text="Label" Font-Bold="True"></asp:Label>
            </td>
        </tr>
        <tr id="detalhesPesquisa"><td> Pesquisa: </td>
            <td><asp:Label ID="Label9" runat="server" Text="Label" Font-Bold="True"></asp:Label>
            </td>
        </tr>
        <tr id="rankingActual">
            <td class="auto-style1" style="width: 284px">Ranking</td>
            <td>
                <asp:Label ID="Label6" runat="server" Text="Label" Font-Bold="True"></asp:Label>
            </td>
        </tr>
        <tr id="recordActual">
            <td class="auto-style1" style="width: 284px">Record</td>
            <td>
                <asp:Label ID="Label7" runat="server" Text="Label" Font-Bold="True"></asp:Label>
            </td>
        </tr>
        
        <tr id="pontosActual">
            <td class="auto-style1" style="width: 284px">Pontos</td>
            <td>
                <asp:Label ID="Label8" runat="server" Text="Label" Font-Bold="True"></asp:Label>
            </td>
        </tr>
        <tr id="forma">
            <td class="auto-style1" style="width: 284px">Forma</td>
            <td id="formaTd" runat="server"></td>
        </tr>
    </table>
        <asp:Panel ID="Panel3" runat="server">
        <asp:Panel ID="PanelJogosIndividuais" runat="server">
        <h3>Jogos individuais</h3>
        <div class="table-responsive">
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" onrowdatabound="GridView1_RowDataBound" AllowSorting="true" onsorting="GridView1_Sorting" GridLines="Vertical" CssClass="table">
                <Columns>
                    <asp:BoundField DataField="nome" HeaderText="nome" InsertVisible="False" ReadOnly="True" SortExpression="nome" Visible="false" />                
                    <asp:BoundField DataField="Id" HeaderText="Id" InsertVisible="False" ReadOnly="True" SortExpression="Id" Visible="false" />
                    <asp:BoundField DataField="IdSeason" HeaderText="IdSeason" InsertVisible="False" ReadOnly="True" Visible="false" />
                    <asp:BoundField DataField="Season" HeaderText="Season" InsertVisible="False" ReadOnly="True" Visible="false" />
                    <asp:BoundField DataField="IdChamp" HeaderText="IdChamp" InsertVisible="False" ReadOnly="True" SortExpression="IdChamp" Visible="false" />
                    <asp:BoundField DataField="IdAdv" HeaderText="IdAdv" InsertVisible="False" ReadOnly="True" SortExpression="IdAdv" Visible="false" />
                    <asp:BoundField DataField="Data" HeaderText="Data" SortExpression="Data" />
                    <asp:HyperLinkField DataTextField="Campeonato" HeaderText="Campeonato" SortExpression="Campeonato" DataNavigateUrlFields="Season, IdChamp, Campeonato"  DataNavigateUrlFormatString="~/torneio/{0}/{1}-{2}"/>
                    <asp:HyperLinkField DataTextField="Adversario" HeaderText="Adversario" SortExpression="Adversario" DataNavigateUrlFields="IdAdv, Adversario"  DataNavigateUrlFormatString="~/jogador/{0}-{1}"/>
                    <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Resultado" HeaderText="Resultado" SortExpression="Resultado" />
                    <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Pontos" HeaderText="Pontos" SortExpression="Pontos" />
                    <asp:HyperLinkField DataTextField="VerHistorico" HeaderText="Ver Historico" SortExpression="VerHistorico" DataNavigateUrlFields="Id, IdAdv, nome, Adversario"  DataNavigateUrlFormatString="~/Jogador/{0}-{2}-vs-{1}-{3}"/>
                    <asp:BoundField DataField="WinPercentage" HeaderText="WinPercentage" InsertVisible="False" ReadOnly="True" SortExpression="WinPercentage" Visible="false" />
                </Columns>
            </asp:GridView>
        </div>
    </asp:Panel>
    <asp:Panel ID="jogosParesPanel" runat="server">
        <h3>Jogos Pares</h3>
        <div class="table-responsive">
            <asp:GridView ID="GridView4" runat="server" AutoGenerateColumns="False" onrowdatabound="GridView4_RowDataBound" AllowSorting="true" CssClass="table" >
                <Columns>
                    <asp:BoundField DataField="Id" HeaderText="Id" InsertVisible="False" ReadOnly="True" SortExpression="Id" Visible="false" />
                    <asp:BoundField DataField="IdSeason" HeaderText="IdSeason" InsertVisible="False" ReadOnly="True" Visible="false" />
                    <asp:BoundField DataField="Season" HeaderText="Season" InsertVisible="False" ReadOnly="True" Visible="false" />
                    <asp:BoundField DataField="IdAdv" HeaderText="IdAdv" InsertVisible="False" ReadOnly="True" SortExpression="IdAdv" Visible="false" />
                    <asp:BoundField DataField="Data" HeaderText="Data" SortExpression="Data" />
                    <asp:HyperLinkField DataTextField="Campeonato" HeaderText="Campeonato" SortExpression="Campeonato" DataNavigateUrlFields="IdChamp, Season, Campeonato"  DataNavigateUrlFormatString="~/torneio/{1}/{0}-{2}"/>
                    <asp:BoundField DataField="IdChamp" HeaderText="IdChamp" InsertVisible="False" ReadOnly="True" Visible="false" />
                    <asp:HyperLinkField DataTextField="Par" HeaderText="Par" SortExpression="Par" DataNavigateUrlFields="Id, Season, Campeonato, Par"  DataNavigateUrlFormatString="~/jogador/{1}/{2}/{0}-{3}"/>
                    <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Resultado" HeaderText="Resultado" SortExpression="Resultado" />
                    <asp:HyperLinkField DataTextField="ParAdversario" HeaderText="ParAdversario" SortExpression="ParAdversario" DataNavigateUrlFields="IdAdv, Season, Campeonato,ParAdversario"  DataNavigateUrlFormatString="~/jogador/{1}/{2}/{0}-{3}"/>
                    <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Pontos" HeaderText="Pontos" SortExpression="Pontos" />
                    <asp:HyperLinkField DataTextField="VerHistorico" HeaderText="Ver Historico" SortExpression="VerHistorico" DataNavigateUrlFields="Id, Season, Campeonato, IdAdv, Par, ParAdversario"  DataNavigateUrlFormatString="~/Jogador/{0}-{4}-vs-{3}-{5}"/>
                    <asp:BoundField DataField="WinPercentage" HeaderText="WinPercentage" InsertVisible="False" ReadOnly="True" SortExpression="WinPercentage" Visible="false" />
                </Columns>
            </asp:GridView>
        </div>
    </asp:Panel>
    </div>
    <div class="col-sm-4">
        <h3>Historico</h3>
        
                <div class="table-responsive">
                    <asp:GridView ID="GridView5" runat="server" Visible="True" AutoGenerateColumns="false" CssClass="table table-striped">
                        <Columns>  
                            <asp:BoundField DataField="Season" HeaderText="Epoca" Visible="true" InsertVisible="False" SortExpression="epoca" />
                            
                            <asp:BoundField DataField="Id" Visible="False"/>

                            <asp:BoundField DataField="IdChamp" Visible="False" />
                            <asp:BoundField DataField="Nome" HeaderText="Nome" Visible="False" InsertVisible="False"/>

                            <asp:HyperLinkField ItemStyle-HorizontalAlign="Center" DataTextField="Campeonato" HeaderText="Campeonato" SortExpression="Campeonato" DataNavigateUrlFields="IdChamp, Campeonato"  DataNavigateUrlFormatString="~/torneio/{0}-{1}"/>
            
                            <asp:BoundField DataField="IdClub" HeaderText="IdClub" Visible="false" InsertVisible="False" ReadOnly="True" SortExpression="IdClub" />
                            <asp:HyperLinkField ItemStyle-HorizontalAlign="Center" DataTextField="Clube" HeaderText="Clube" Visible="true" InsertVisible="False" SortExpression="Clube" DataNavigateUrlFields="IdClub, Clube"  DataNavigateUrlFormatString="~/clube/{0}-{1}"/>
                            <asp:HyperLinkField ItemStyle-HorizontalAlign="Center" DataTextField="Ranking" HeaderText="Ranking" Visible="true" InsertVisible="False" SortExpression="Ranking" DataNavigateUrlFields="IdChamp, Campeonato, Season"  DataNavigateUrlFormatString="~/ranking/{2}/{0}-{1}"/>
                            <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Pontos" HeaderText="Pontos" Visible="true" InsertVisible="False" ReadOnly="True" SortExpression="pontos" />
                            <asp:HyperLinkField ItemStyle-HorizontalAlign="Center" DataTextField="Record" HeaderText="Record" Visible="true" InsertVisible="False" SortExpression="Record" DataNavigateUrlFields="Campeonato, Id, Season, Nome"  DataNavigateUrlFormatString="~/jogador/{2}/{0}/{1}-{3}"/>
                        </Columns>
                    </asp:GridView>
                </div>
        </div>
    <asp:Panel ID="Panel2" runat="server" DefaultButton="Button2" >
        <asp:GridView ID="GridView3" runat="server" Visible="False" AutoGenerateColumns="false">
            <Columns>            
                <asp:BoundField DataField="campeonatosField" HeaderText="campeonatosField" Visible="false" InsertVisible="False" ReadOnly="True" SortExpression="campeonatosField" />
                <asp:BoundField DataField="Id" HeaderText="Id" Visible="false" InsertVisible="False" ReadOnly="True" SortExpression="Id" />  
                <asp:BoundField DataField="IdAdv" HeaderText="IdAdv" Visible="false" InsertVisible="False" ReadOnly="True" SortExpression="IdAdv" />    
                <asp:BoundField DataField="IdClub" HeaderText="IdClub" Visible="false" InsertVisible="False" ReadOnly="True" SortExpression="IdClub" />
                    <asp:BoundField DataField="Licenca" HeaderText="Licenca" SortExpression="licenseNo" ItemStyle-HorizontalAlign="Center" />
                <asp:BoundField DataField="CurrentRank" HeaderText="Ranking" SortExpression="currentRank"  ItemStyle-HorizontalAlign="Center"/>
                <asp:HyperLinkField ItemStyle-HorizontalAlign="Center" DataTextField="Nome" HeaderText="Nome" SortExpression="Name" DataNavigateUrlFields="Id,startDateField, endDateField, campeonatosField,IdAdv"  DataNavigateUrlFormatString="~/Jogador/{0}"/>
                <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Club" HeaderText="Clube" SortExpression="Name" />
                <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Record" HeaderText="Record" SortExpression="Record" />
                <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Pontos" HeaderText="Pontos" SortExpression="Pontos" />
                <asp:BoundField Visible="false" DataField="GraphData" />
            </Columns>
        </asp:GridView>
    </asp:Panel>
    
    <asp:Panel ID="resultadosGraficoPanel" runat="server" Visible="false">
        <h3> Resultados</h3>
            <asp:Chart ID="Chart1" runat="server" BackColor="White">
                <ChartAreas>
                    <asp:ChartArea Name="ChartArea1" BackColor="White">
                    </asp:ChartArea>
                </ChartAreas>
                <Legends>
                    <asp:Legend BackColor="White" Name="Legenda" LegendStyle="Column" Alignment="Center" Docking="Right" >
                    </asp:Legend>
            </Legends>
            </asp:Chart>
    </asp:Panel>
</asp:Panel>
</asp:Content>
