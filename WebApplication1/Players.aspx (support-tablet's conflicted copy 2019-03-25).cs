﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Helpers;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using TableTennis;
using TableTennis.Database;
using static TableTennis.Database.DataSet2;

namespace WebApplication1
{
    public partial class Players : Page
    {
        int playerId = -1;
        DateTime startDate;
        DateTime endDate;
        List<int> championships = new List<int>();
        playerRow playerGlobal;
        int idAdv;
        List<playerRow> players;
        IEnumerable<ListGames> listaJogos = new List<ListGames>();
        IEnumerable<ListGames> listaJogosDouble = new List<ListGames>();

        static object locker = new object();
        
        protected void Page_Load(object sender, EventArgs e)
        {
            MetaKeywords = "Tenis de mesa, Jogos, resultados, ";
            Stopwatch s1 = Stopwatch.StartNew();
            idAdv = Request.QueryString["idAdv"] != "0" ? Convert.ToInt32(Request.QueryString["idAdv"]) : -1;
            
            playerId = Page.RouteData.Values.ContainsKey("Id") ? Convert.ToInt32(Page.RouteData.Values["Id"].ToString()) : -1;

            //playerId = Request.QueryString["ID"] != "0" ? Convert.ToInt32(Request.QueryString["ID"]) : -1;
            var lista = Request.QueryString["campeonatos"] != null ? Request.QueryString["campeonatos"] != "" ? Request.QueryString["campeonatos"].Split(',').ToList() : new List<string>() : new List<string>();

            championships = lista.Count != 0 ? lista.Select(int.Parse).ToList() : null;
            startDate = Request.QueryString["startDate"] != null ? DateTime.ParseExact(Request.QueryString["startDate"], "dd/MM/yyyy", CultureInfo.InvariantCulture) : new DateTime(1970, 1, 1);
            endDate = Request.QueryString["endDate"] != null ? DateTime.ParseExact(Request.QueryString["endDate"], "dd/MM/yyyy", CultureInfo.InvariantCulture) : DateTime.Today;
                
            //Players have the last ranking that was created
            List<playerRow> playersActual = Ranking.Instance.Players;

            players = Ranking.Instance.GenerateRanking();

            list.Visible = false;

            //check if it is to view double games
            bool isDoubles = playerId >> 16 > 0 ? true : false;
            
            if (players.Count(x => x.Id == playerId) == 1 || isDoubles)
            {
                list.Visible = true;
                if (isDoubles)
                {
                    var doublePlayerA = players.Single(x => x.Id == (playerId >> 16));
                    var doublePlayerB = players.Single(x => x.Id == (playerId & 0xFF));

                    HyperLink playerAlink = new HyperLink()
                    {
                        Text = doublePlayerA.Name,
                        NavigateUrl = "Players.aspx?ID="+doublePlayerA.Id
                    };

                    HyperLink playerBlink = new HyperLink()
                    {
                        Text = doublePlayerB.Name,
                        NavigateUrl = "Players.aspx?ID=" + doublePlayerB.Id
                    };

                    nomeJogador.InnerHtml = $"<a href='{playerAlink.NavigateUrl}'>{playerAlink.Text}</a>" + " / " + $"<a href='{playerBlink.NavigateUrl}'>{playerBlink.Text}</a>";
                    Label2.Text = FilesAccess.db.clubs.Single(x => x.Id == doublePlayerA.IdClub).Name + " / " + FilesAccess.db.clubs.Single(x => x.Id == doublePlayerB.IdClub).Name;
                    Label3.Text = doublePlayerA.currentRank.ToString() + " / " + doublePlayerB.currentRank.ToString();

                    Label4.Text = Utilities.DoubleNumberGamesTogetherWon(doublePlayerA.Id, doublePlayerB.Id).ToString() + " - " + Utilities.DoubleNumberGamesTogetherLost(doublePlayerA.Id, doublePlayerB.Id).ToString();
                        
                    Label5.Text = doublePlayerA.currentPoints+ " / "+ doublePlayerB.currentPoints;
                    Label10.Text = doublePlayerA.licenseNo + " / "+ doublePlayerB.licenseNo;

                    Title = doublePlayerA.Name + " / " + doublePlayerB.Name;
                    MetaKeywords += $"Pares, {doublePlayerA.Name},{doublePlayerB.Name},{FilesAccess.db.clubs.Single(x => x.Id == doublePlayerA.IdClub).Name},{FilesAccess.db.clubs.Single(x => x.Id == doublePlayerB.IdClub).Name}, {doublePlayerA.licenseNo}, {doublePlayerB.licenseNo} ";
                }
                else
                {
                    playerGlobal = players.Single(x => x.Id == playerId);
                    nomeJogador.InnerText = playerGlobal.Name;
                    Label2.Text = FilesAccess.db.clubs.Single(x => x.Id == playerGlobal.IdClub).Name;
                    Label3.Text = playerGlobal.currentRank.ToString();
                    Label4.Text = $"{playerGlobal.currentGamesWon} - {playerGlobal.currentGamesLost}";
                    Label5.Text = playerGlobal.currentPoints.ToString();
                    Label10.Text = playerGlobal.licenseNo.ToString();

                    Title = playerGlobal.Name;
                    MetaKeywords += $"{playerGlobal.Name}, {Label2.Text}, {Label10.Text}";
                }

                var playerPesquisa = playersActual?.SingleOrDefault(x => x.Id == playerId);
                
                if (playerGlobal == playerPesquisa || playerPesquisa == null)
                {
                    rankingActual.Visible = false;
                    pontosActual.Visible = false;
                    recordActual.Visible = false;
                    detalhesPesquisa.Visible = false;
                    historicoJogadorPanel.Visible = false;
                }
                else
                {
                    rankingActual.Visible = true;
                    pontosActual.Visible = true;
                    recordActual.Visible = true;
                    detalhesPesquisa.Visible = true;
                    historicoJogadorPanel.Visible = true;

                    Label6.Text = playerPesquisa.currentRank.ToString();
                    Label7.Text = $"{playerPesquisa.currentGamesWon} - {playerPesquisa.currentGamesLost}";
                    Label8.Text = playerPesquisa.currentPoints.ToString();
                    Label9.Text = $"{startDate.ToString("dd/MM/yyyy")} - {endDate.ToString("dd/MM/yyyy")}";
                }

                if (!isDoubles)
                {
                    PanelJogosIndividuais.Visible = true;
                    listaJogos = GetSinglesGamesListQuery();
                    lock (locker)
                    {
                        GridView1.DataSource = listaJogos;
                        GridView1.DataBind();
                    }
                }
                else
                {
                    PanelJogosIndividuais.Visible = false;
                }
                
                listaJogosDouble = GetDoubleGamesListQuery();

                if (listaJogosDouble.Count() == 0)
                {
                    jogosParesPanel.Visible = false;
                }
                else
                {
                    jogosParesPanel.Visible = true;
                    lock (locker)
                    {
                        GridView4.DataSource = listaJogosDouble;
                        GridView4.DataBind();
                    }
                }
                /*
                Chart1.Series.Add("Individuais Ganhos");
                var gamesWon = Chart1.Series.Last();
                gamesWon.ChartType = System.Web.UI.DataVisualization.Charting.SeriesChartType.Point;
                gamesWon.Color = System.Drawing.Color.Blue;

                var combined = listaJogos.Where(x => x.graphData > 0);
                gamesWon.Points.DataBindXY(
                    combined.Select(x => DateTime.ParseExact(x.Data, "dd/MM/yyyy", CultureInfo.InvariantCulture)).ToList(),
                    combined.Select(x => x.graphData).ToList());

                Chart1.Series.Add("Pares Ganhos");
                var gamesDoubleWon = Chart1.Series.Last();
                gamesDoubleWon.ChartType = System.Web.UI.DataVisualization.Charting.SeriesChartType.Point;
                gamesDoubleWon.Color = System.Drawing.Color.Green;
                var combinedDoubles = listaJogosDouble.Where(x => x.graphData > 0);
                gamesDoubleWon.Points.DataBindXY(
                    combinedDoubles.Select(x => DateTime.ParseExact(x.Data, "dd/MM/yyyy", CultureInfo.InvariantCulture)).ToList(),
                    combinedDoubles.Select(x => x.graphData).ToList());

                Chart1.Series.Add("Individuais Perdidos");
                var gamesLost = Chart1.Series.Last();
                gamesLost.ChartType = System.Web.UI.DataVisualization.Charting.SeriesChartType.Point;
                gamesLost.Color = System.Drawing.Color.Red;
                var combined1 = listaJogos.Where(x => x.graphData < 0);
                gamesLost.Points.DataBindXY(
                    combined1.Select(x => DateTime.ParseExact(x.Data, "dd/MM/yyyy", CultureInfo.InvariantCulture)).ToList(),
                    combined1.Select(x => x.graphData).ToList());
                
                Chart1.Series.Add("Pares Perdidos");
                var gamesDoubleLost = Chart1.Series.Last();
                gamesDoubleLost.ChartType = System.Web.UI.DataVisualization.Charting.SeriesChartType.Point;
                gamesDoubleWon.Color = System.Drawing.Color.Yellow;
                var combined1Doubles = listaJogosDouble.Where(x => x.graphData < 0);
                gamesDoubleLost.Points.DataBindXY(
                    combined1Doubles.Select(x => DateTime.ParseExact(x.Data, "dd/MM/yyyy", CultureInfo.InvariantCulture)).ToList(),
                    combined1Doubles.Select(x => x.graphData).ToList());

                Chart1.Width = 1000;
                */
            }
            else
            {
                Button1_Click(this, new EventArgs());
            }
            
            s1.Stop();
            Label1.InnerText = s1.ElapsedMilliseconds.ToString();
        }
        
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            FillGridViewColor(e.Row.Cells[9].Text.Split('-'), e);
        }

        protected void GridView4_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            FillGridViewColor(e.Row.Cells[10].Text.Split('-'), e);
        }

        private void FillGridViewColor(string[] parseResult, GridViewRowEventArgs e)
        {
            if (parseResult.Count() == 2)
            {
                int value0 = 0;
                int value1 = 0;
                if (!Int32.TryParse(parseResult[0], out value0))
                    return;

                if (!Int32.TryParse(parseResult[1], out value1))
                    return;

                if (value0 > value1)
                    e.Row.BackColor = System.Drawing.Color.LightGreen;
                else
                    e.Row.BackColor = System.Drawing.Color.IndianRed;
            }
        }


        /// <summary>
        /// Search players button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Button1_Click(object sender, EventArgs e)
        {
            var search = HttpUtility.UrlDecode(HttpUtility.UrlEncode(TextBox1.Text.ToLower(), Encoding.GetEncoding("iso-8859-7")));
            var players =
                (from p in FilesAccess.db.player
                from c in FilesAccess.db.clubs
                orderby p.currentPoints descending
                where p.IdClub == c.Id &&
                    ((HttpUtility.UrlDecode(HttpUtility.UrlEncode(p.Name.ToLower(), Encoding.GetEncoding("iso-8859-7"))).Contains(search)) ||
                    HttpUtility.UrlDecode(HttpUtility.UrlEncode(c.Name.ToLower(), Encoding.GetEncoding("iso-8859-7"))).Contains(search) ||
                    p.licenseNo.Contains(search))
                select new
                {
                    Id = p.Id,
                    Licenca = p.licenseNo,
                    CurrentRank = p.currentRank,
                    Nome = p.Name,
                    Club = c.Name,
                    Record = $"{p.currentGamesWon}-{p.currentGamesLost}",
                    Pontos = p.currentPoints,
                    IdClub = c.Id
                }).ToList();

            if (players.Count() == 1)
            {
                var url = String.Format("~/Players.aspx?ID={0}",
                    players[0].Id);
                Response.Redirect(url);
            }
            else
            {
                GridView2.DataSource = players;
                GridView2.DataBind();
                GridView2.Visible = true;
            }
        }

        /// <summary>
        /// Compare players button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Button2_Click(object sender, EventArgs e)
        {
            GridView2.Visible = false;
            var search = HttpUtility.UrlDecode(HttpUtility.UrlEncode(TextBox2.Text.ToLower(), Encoding.GetEncoding("iso-8859-7")));
            var players =
                (from p in FilesAccess.db.player
                from c in FilesAccess.db.clubs
                orderby p.currentPoints descending
                where p.IdClub == c.Id &&
                    ((HttpUtility.UrlDecode(HttpUtility.UrlEncode(p.Name.ToLower(), Encoding.GetEncoding("iso-8859-7"))).Contains(search)) ||
                    HttpUtility.UrlDecode(HttpUtility.UrlEncode(c.Name.ToLower(), Encoding.GetEncoding("iso-8859-7"))).Contains(search) ||
                    p.licenseNo.Contains(search))
                select new
                {
                    startDateField = startDate.ToString("dd/MM/yyyy"),
                    endDateField = endDate.ToString("dd/MM/yyyy"),
                    campeonatosField = championships != null && championships.Count > 0 ? string.Join(",", championships.Select(x => x.ToString())) : "",
                    Id = playerId,
                    IdAdv = p.Id,
                    Licenca = p.licenseNo,
                    CurrentRank = p.currentRank,
                    Nome = p.Name,
                    Club = c.Name,
                    Record = $"{p.currentGamesWon}-{p.currentGamesLost}",
                    Pontos = p.currentPoints,
                    IdClub = c.Id
                }).ToList();

            if (players.Count() == 1)
            {
                var url = String.Format("~/Players.aspx?ID={0}&startDate={1}&endDate={2}&campeonatos={3}&idAdv={4}", 
                    players[0].Id,
                    players[0].startDateField, 
                    players[0].endDateField,
                    players[0].campeonatosField,
                    players[0].IdAdv
                    );
                Response.Redirect(url);
            }
            else
            {
                GridView3.DataSource = players;
                GridView3.DataBind();
                GridView3.Visible = true;
            }
        }

        protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
        {
            var sortDirection = GetSortDirection("GridView1"+e.SortExpression);
            switch (e.SortExpression)
            {
                case "Data":
                    switch (sortDirection)
                    {
                        case System.Web.UI.WebControls.SortDirection.Ascending:
                            listaJogos=listaJogos.OrderBy(x => DateTime.ParseExact(x.Data, "dd/MM/yyyy", CultureInfo.InvariantCulture));
                            break;
                        case System.Web.UI.WebControls.SortDirection.Descending:
                            listaJogos = listaJogos.OrderByDescending(x => DateTime.ParseExact(x.Data, "dd/MM/yyyy", CultureInfo.InvariantCulture));
                            break;
                        default:
                            break;
                    }
                    break;
                case "Campeonato":
                    switch (sortDirection)
                    {
                        case System.Web.UI.WebControls.SortDirection.Ascending:
                            listaJogos = listaJogos.OrderBy(x => x.Campeonato);
                            break;
                        case System.Web.UI.WebControls.SortDirection.Descending:
                            listaJogos = listaJogos.OrderByDescending(x => x.Campeonato);
                            break;
                        default:
                            break;
                    }
                    break;
                case "Adversario":
                    switch (sortDirection)
                    {
                        case System.Web.UI.WebControls.SortDirection.Ascending:
                            listaJogos = listaJogos.OrderBy(x => x.Adversario);
                            break;
                        case System.Web.UI.WebControls.SortDirection.Descending:
                            listaJogos = listaJogos.OrderByDescending(x => x.Adversario);
                            break;
                        default:
                            break;
                    }
                    break;
                case "Resultado":
                    switch (sortDirection)
                    {
                        case System.Web.UI.WebControls.SortDirection.Ascending:
                            listaJogos = listaJogos.OrderBy(x => x.Resultado);
                            break;
                        case System.Web.UI.WebControls.SortDirection.Descending:
                            listaJogos = listaJogos.OrderByDescending(x => x.Resultado);
                            break;
                        default:
                            break;
                    }
                    break;
                case "Pontos":
                    switch (sortDirection)
                    {
                        case System.Web.UI.WebControls.SortDirection.Ascending:
                            listaJogos=listaJogos.OrderBy(x => x.Pontos);
                            break;
                        case System.Web.UI.WebControls.SortDirection.Descending:
                            listaJogos=listaJogos.OrderByDescending(x => x.Pontos);
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }

            GridView1.DataSource = listaJogos.ToList();
            GridView1.DataBind();
            
            GridView4.DataSource = listaJogosDouble;
            GridView4.DataBind();
        }

        private System.Web.UI.WebControls.SortDirection GetSortDirection(string column)
        {
            System.Web.UI.WebControls.SortDirection nextDir = System.Web.UI.WebControls.SortDirection.Ascending; // Default next sort expression behaviour.
            if (ViewState["sort"] != null && ViewState["sort"].ToString() == column)
            {   // Exists... DESC.
                nextDir = System.Web.UI.WebControls.SortDirection.Descending;
                ViewState["sort"] = null;
            }
            else
            {   // Doesn't exists, set ViewState.
                ViewState["sort"] = column;
            }
            return nextDir;
        }

        private IEnumerable<ListGamesDoubles> GetDoubleGamesListQuery()
        {
            List<doublegamesRow> allGames = new List<doublegamesRow>();

            bool isDoubles = playerId >> 16 > 0 ? true : false;
            List<int> playersId = new List<int>();
            int doublePlayerAId = 0;
            int doublePlayerBId = 0;
            if (isDoubles)
            {
                var doublePlayerA = players.Single(x => x.Id == (playerId >> 16));
                var doublePlayerB = players.Single(x => x.Id == (playerId & 0xFF));

                doublePlayerAId = doublePlayerA.Id;
                doublePlayerBId = doublePlayerB.Id;
                playerId = doublePlayerAId;

                playersId.Add(doublePlayerA.Id);
                playersId.Add(doublePlayerB.Id);
            }
            else
            {
                playersId.Add(playerId);
            }

            if (championships != null && championships.Count != 0)
            {
                foreach (var playerId in playersId)
                {
                    allGames.AddRange(FilesAccess.db.doubleGames.Where(x => championships.Contains(x.IdChampionship) &&
                    (x.DoubleA_PlayerAId == playerId || x.DoubleA_PlayerBId == playerId ||
                    x.DoubleB_PlayerAId == playerId || x.DoubleB_PlayerBId == playerId)).ToList());
                }
            }
            else
                allGames = FilesAccess.db.doubleGames.ToList();
            
            if (playerId > 0)
            {
                if (!isDoubles)
                {
                    allGames = allGames.Where(x => x.DoubleA_PlayerAId == playerId || x.DoubleA_PlayerBId == playerId ||
                        x.DoubleB_PlayerAId == playerId || x.DoubleB_PlayerBId == playerId).ToList();
                }
                else
                {
                    allGames = Utilities.DoublesGamesTogether(allGames, doublePlayerAId, doublePlayerBId);
                }
            }
            
            var champs = championships != null && championships.Count > 0 ? string.Join(",", championships.Select(x => x.ToString())) : "";
            
            var data = from g in allGames
                       orderby g.Date descending
                       where 
                       !isDoubles?
                            ((g.DoubleA_PlayerAId == playerId || g.DoubleA_PlayerBId == playerId ||
                    g.DoubleB_PlayerAId == playerId || g.DoubleB_PlayerBId == playerId)) 
                       :
                            (g.DoubleA_PlayerAId == doublePlayerAId && g.DoubleA_PlayerBId == doublePlayerBId) ||
                            (g.DoubleA_PlayerAId == doublePlayerBId && g.DoubleA_PlayerBId == doublePlayerAId) ||
                            (g.DoubleB_PlayerAId == doublePlayerAId && g.DoubleB_PlayerBId == doublePlayerBId) ||
                            (g.DoubleB_PlayerAId == doublePlayerBId && g.DoubleB_PlayerBId == doublePlayerAId)

                        &&
                       g.Date >= startDate && g.Date <= endDate
                       select new ListGamesDoubles()
                       {
                           Id = (g.DoubleA_PlayerAId == playerId || g.DoubleA_PlayerBId == playerId) ? g.DoubleA_PlayerAId << 16 | g.DoubleA_PlayerBId : g.DoubleB_PlayerAId << 16 | g.DoubleB_PlayerBId,
                           startDateField = startDate.ToString("dd/MM/yyyy"),
                           endDateField = endDate.ToString("dd/MM/yyyy"),
                           campeonatosField = champs,
                           IdAdv = 
                                !isDoubles ?
                                    (g.DoubleA_PlayerAId == playerId || g.DoubleA_PlayerBId == playerId) ? g.DoubleB_PlayerAId <<16 | g.DoubleB_PlayerBId : g.DoubleA_PlayerAId << 16 | g.DoubleA_PlayerBId
                                :
                                    (g.DoubleA_PlayerAId == doublePlayerAId || g.DoubleA_PlayerBId == doublePlayerAId) ? g.DoubleB_PlayerAId << 16 | g.DoubleB_PlayerBId :
                                    g.DoubleA_PlayerAId << 16 | g.DoubleA_PlayerBId
                                ,
                           Data = g.Date.ToString("dd/MM/yyyy"),
                           Campeonato = FilesAccess.db.championships.SingleOrDefault(x => x.Id == g.IdChampionship)?.Name,
                           Par =
                                !isDoubles ?
                                (g.DoubleA_PlayerAId == playerId || g.DoubleA_PlayerBId == playerId) ?
                                    players.SingleOrDefault(x => x.Id == g.DoubleA_PlayerAId)?.Name + "/" + players.SingleOrDefault(x => x.Id == g.DoubleA_PlayerBId)?.Name :
                                    players.SingleOrDefault(x => x.Id == g.DoubleB_PlayerAId)?.Name + "/" + players.SingleOrDefault(x => x.Id == g.DoubleB_PlayerBId)?.Name
                                :
                                players.SingleOrDefault(x => x.Id == doublePlayerAId)?.Name + "/" + players.SingleOrDefault(x => x.Id == doublePlayerBId)?.Name,

                           ParAdversario =
                                !isDoubles ?
                                (g.DoubleA_PlayerAId == playerId || g.DoubleA_PlayerBId == playerId) ? 
                                
                                    players.SingleOrDefault(x => x.Id == g.DoubleB_PlayerAId)?.Name+"/"+ players.SingleOrDefault(x => x.Id == g.DoubleB_PlayerBId)?.Name 
                                    :
                                    players.SingleOrDefault(x => x.Id == g.DoubleA_PlayerAId)?.Name + "/" + players.SingleOrDefault(x => x.Id == g.DoubleA_PlayerBId)?.Name
                                :
                                (g.DoubleA_PlayerAId == doublePlayerAId || g.DoubleA_PlayerBId == doublePlayerAId) ?
                                    players.SingleOrDefault(x => x.Id == g.DoubleB_PlayerAId)?.Name + "/" + players.SingleOrDefault(x => x.Id == g.DoubleB_PlayerBId)?.Name
                                    :
                                    players.SingleOrDefault(x => x.Id == g.DoubleA_PlayerAId)?.Name + "/" + players.SingleOrDefault(x => x.Id == g.DoubleA_PlayerBId)?.Name,

                           Resultado = (g.DoubleA_PlayerAId == playerId || g.DoubleA_PlayerBId == playerId) ? $"{g.SetsA}-{g.SetsB}" : $"{g.SetsB}-{g.SetsA}",
                           Pontos = (g.DoubleA_PlayerAId == playerId || g.DoubleA_PlayerBId == playerId) ? g.PointsDoubleA_PlayerA+g.PointsDoubleA_PlayerB : g.PointsDoubleB_PlayerA + g.PointsDoubleB_PlayerB,
                           IdChamp = g.IdChampionship,
                           graphData = (g.DoubleA_PlayerAId == playerId || g.DoubleA_PlayerBId == playerId) ? g.SetsA - g.SetsB : g.SetsB - g.SetsA,

                           VerHistorico =
                                (g.DoubleA_PlayerAId == playerId || g.DoubleA_PlayerBId == playerId) ?

                                players.SingleOrDefault(x => x.Id == g.DoubleA_PlayerAId)?.Name + "/" + players.SingleOrDefault(x => x.Id == g.DoubleA_PlayerBId)?.Name + " VS " + players.SingleOrDefault(x => x.Id == g.DoubleB_PlayerAId)?.Name + "/" + players.SingleOrDefault(x => x.Id == g.DoubleB_PlayerBId)?.Name :

                                players.SingleOrDefault(x => x.Id == g.DoubleB_PlayerAId)?.Name + "/" + players.SingleOrDefault(x => x.Id == g.DoubleB_PlayerBId)?.Name + " VS "+ players.SingleOrDefault(x => x.Id == g.DoubleA_PlayerAId)?.Name + "/" + players.SingleOrDefault(x => x.Id == g.DoubleA_PlayerBId)?.Name,

                           GameType = 2
                       };
            return data;
        }

        private IEnumerable<ListGames> GetSinglesGamesListQuery()
        {
            List<gamesRow> allGames = new List<gamesRow>();

            if (championships != null && championships.Count != 0)
                allGames = FilesAccess.db.games.Where(x => championships.Contains(x.IdChamp) && (x.IdPlayerA == playerGlobal.Id || x.IdPlayerB == playerGlobal.Id)).ToList();
            else
                allGames = FilesAccess.db.games.ToList();

            if (playerId > 0)
                allGames = allGames.Where(x => x.IdPlayerA == playerId || x.IdPlayerB == playerId).ToList();

            if (idAdv > 0)
                allGames = allGames.Where(x => x.IdPlayerA == idAdv || x.IdPlayerB == idAdv).ToList();
            var champs = championships != null && championships.Count > 0 ? string.Join(",", championships.Select(x => x.ToString())) : "";
            var data = from g in allGames
                       from p in players
                       orderby g.Date descending
                       where ((g.IdPlayerA == playerGlobal.Id && g.IdPlayerA == p.Id) || (g.IdPlayerB == playerGlobal.Id && g.IdPlayerB == p.Id))
                       && g.Date >= startDate
                            && g.Date <= endDate
                       select new ListGames()
                       {
                           Id = playerId,
                           startDateField = startDate.ToString("dd/MM/yyyy"),
                           endDateField = endDate.ToString("dd/MM/yyyy"),
                           campeonatosField = champs,
                           IdAdv = g.IdPlayerA == playerGlobal.Id ? g.IdPlayerB : g.IdPlayerA,
                           Data = g.Date.ToString("dd/MM/yyyy"),
                           Campeonato = FilesAccess.db.championships.SingleOrDefault(x => x.Id == g.IdChamp)?.Name,
                           Adversario = g.IdPlayerA == playerGlobal.Id ? players.SingleOrDefault(x => x.Id == g.IdPlayerB)?.Name : players.SingleOrDefault(x => x.Id == g.IdPlayerA)?.Name,
                           Resultado = g.IdPlayerA == playerGlobal.Id ? $"{g.SetsA}-{g.SetsB}" : $"{g.SetsB}-{g.SetsA}",
                           Pontos = g.IdPlayerA == playerGlobal.Id ? g.pointsPlayerA : g.pointsPlayerB,
                           IdChamp = g.IdChamp,
                           graphData = g.IdPlayerA == playerGlobal.Id ? g.SetsA - g.SetsB : g.SetsB - g.SetsA,
                           VerHistorico = FilesAccess.db.player.SingleOrDefault(x => x.Id == playerId).Name + " VS " + (g.IdPlayerA == playerGlobal.Id ? players.SingleOrDefault(x => x.Id == g.IdPlayerB)?.Name : players.SingleOrDefault(x => x.Id == g.IdPlayerA)?.Name),
                           GameType = 1
                       };
            
            return data;
        }
    }
}