﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Helpers;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using TableTennis;
using TableTennis.Database;
using static TableTennis.Database.DataSet2;

namespace WebApplication1
{
    public partial class PlayersVs : Page
    {
        int playerAId;
        int playerBId;

        playerRow playerA;
        playerRow playerB;
        List<seasonRow> seasons;
        IEnumerable<ListGames> listaJogos = new List<ListGames>();
        IEnumerable<ListGames> listaJogosDouble = new List<ListGames>();
        List<ListRanking> playerHistoric = new List<ListRanking>();
        championshipsRow championship;
        seasonRow season;

        static object locker = new object();
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack && Request.UrlReferrer != null)
            {
                ViewState["RefUrl"] = Request.UrlReferrer.ToString();
            }

            Stopwatch s1 = Stopwatch.StartNew();

            //initial keywords, then other keywords are inserted below in the code
            MetaKeywords = "Tenis de mesa, Jogos, resultados, ";

            //get the routeData from the URL
            playerAId = Page.RouteData.Values.ContainsKey("Id") ? Int32.TryParse(Page.RouteData.Values["Id"] as string, out playerAId) ? playerAId : -1 : -1;

            playerBId = Page.RouteData.Values.ContainsKey("IdAdv") ? Int32.TryParse(Page.RouteData.Values["IdAdv"] as string, out playerBId) ? playerBId : -1 : -1;

            season = Page.RouteData.Values.ContainsKey("Epoca") ? FilesAccess.db.seasons.Single(x => x.name == Page.RouteData.Values["Epoca"].ToString()) : FilesAccess.db.seasons.Single(x => x.name == "TodasEpocas");

            if (season.name == "TodasEpocas")
                season.endDate = DateTime.Today;

            championship = Page.RouteData.Values.ContainsKey("Campeonato") ? FilesAccess.db.championships.Single(x => x.Name == Page.RouteData.Values["Campeonato"].ToString()) : FilesAccess.db.championships.Single(x => x.Name == "TodosCampeonatos");


            //check if it is to view double games
            bool isDoubles = playerAId >> 16 > 0 ? true : false;
            
            if (FilesAccess.db.player.Count(x => x.Id == playerAId) == 1 || isDoubles)
            {
                if (isDoubles)
                {
                    var doublePlayerA = FilesAccess.db.player.Single(x => x.Id == (playerAId >> 16));
                    var doublePlayerB = FilesAccess.db.player.Single(x => x.Id == (playerAId & 0xFF));

                    var rankDataGlobalPlayerA = FilesAccess.db.ranking.SingleOrDefault(x => x.idPlayer == doublePlayerA.Id && x.idChamp == Utilities.TodosCampeonatos.Id && x.idSeason == Utilities.TodasEpocas.id);
                    var rankDataGlobalPlayerB = FilesAccess.db.ranking.SingleOrDefault(x => x.idPlayer == doublePlayerB.Id && x.idChamp == Utilities.TodosCampeonatos.Id && x.idSeason == Utilities.TodasEpocas.id);

                    HyperLink playerAlink = new HyperLink()
                    {
                        Text = doublePlayerA.Name,
                        NavigateUrl = $"/Jogador/{doublePlayerA.Id}-{doublePlayerA.Name}"
                    };

                    HyperLink playerBlink = new HyperLink()
                    {
                        Text = doublePlayerB.Name,
                        NavigateUrl = $"/Jogador/{doublePlayerB.Id}-{doublePlayerB.Name}"
                    };


                    jogadorA.InnerHtml = $"<a href='{playerAlink.NavigateUrl}'>{playerAlink.Text}</a>" + " / " + $"<a href='{playerBlink.NavigateUrl}'>{playerBlink.Text}</a>";
                    rankingA.Text = rankDataGlobalPlayerA?.rank.ToString() + " / " + rankDataGlobalPlayerB.rank.ToString();

                    recordA.Text = Utilities.DoubleNumberGamesTogetherWon(championship.Id, doublePlayerA.Id, doublePlayerB.Id).ToString() + " - " + Utilities.DoubleNumberGamesTogetherLost(championship.Id, doublePlayerA.Id, doublePlayerB.Id).ToString();
                    
                    pontosA.Text = rankDataGlobalPlayerA?.points+ " / "+ rankDataGlobalPlayerB?.points;
                    licensaA.Text = doublePlayerA.licenseNo + " / "+ doublePlayerB.licenseNo;

                    Title = doublePlayerA.Name + " / " + doublePlayerB.Name;
                    MetaKeywords += $"Pares, {doublePlayerA.Name},{doublePlayerB.Name},{FilesAccess.db.clubs.Single(x => x.Id == doublePlayerA.IdClub).Name},{FilesAccess.db.clubs.Single(x => x.Id == doublePlayerB.IdClub).Name}, {doublePlayerA.licenseNo}, {doublePlayerB.licenseNo} ";
                }
                else
                {
                    var rankDataGlobalA = FilesAccess.db.ranking.SingleOrDefault(x => x.idPlayer == playerAId && x.idChamp == Utilities.TodosCampeonatos.Id && x.idSeason == Utilities.TodasEpocas.id);
                    var rankDataGlobalB = FilesAccess.db.ranking.SingleOrDefault(x => x.idPlayer == playerBId && x.idChamp == Utilities.TodosCampeonatos.Id && x.idSeason == Utilities.TodasEpocas.id);

                    playerA = FilesAccess.db.player.Single(x => x.Id == playerAId);
                    playerB = FilesAccess.db.player.Single(x => x.Id == playerBId);
                    
                    jogadorA.InnerText = playerA.Name;
                    rankingA.Text = rankDataGlobalA?.rank.ToString();
                    recordA.Text = $"{rankDataGlobalA?.numWins} - {rankDataGlobalA?.numLosses}";
                    pontosA.Text = rankDataGlobalA?.points.ToString();
                    licensaA.Text = playerA.licenseNo.ToString();


                    jogadorB.InnerText = playerB.Name;
                    rankingB.Text = rankDataGlobalB?.rank.ToString();
                    recordB.Text = $"{rankDataGlobalB?.numWins} - {rankDataGlobalB?.numLosses}";
                    pontosB.Text = rankDataGlobalB?.points.ToString();
                    licensaB.Text = playerB.licenseNo.ToString();

                    var lastGamesA = GetSinglesGamesListQuery(playerAId).ToList();
                    lastGamesA.AddRange(GetDoubleGamesListQuery(playerAId));
                    lastGamesA = lastGamesA.OrderByDescending(x => x.DateGame).Take(5).ToList();


                    var lastGamesB = GetSinglesGamesListQuery(playerBId).ToList();
                    lastGamesB.AddRange(GetDoubleGamesListQuery(playerBId));
                    lastGamesB = lastGamesB.OrderByDescending(x => x.DateGame).Take(5).ToList();

                    foreach (var game in lastGamesA)
                    {
                        var teamGame = FilesAccess.db.teamGames.SingleOrDefault(x => x.Id == game.IdTeamGame);
                        string link = "";
                        if (teamGame != null)
                            link = $"/detalhes/{FilesAccess.db.seasons.Single(x => x.id == game.IdSeason).name}/{FilesAccess.db.championships.Single(x => x.Id == game.IdChamp).Name}/{teamGame.Id}-{FilesAccess.db.clubs.Single(x => x.Id == teamGame.ClubAId).Name}-vs-{FilesAccess.db.clubs.Single(x => x.Id == teamGame.ClubBId).Name}";
                        else
                            link = $"/detalhes/0";
                        if (game.IsWon)
                            formaATd.InnerHtml +=
                                $"<a href='{link}' class='sign win' style='display: inline-block'>V</a>";
                        else
                            formaATd.InnerHtml +=
                                $"<a href='{link}' class='sign lost' style='display: inline-block'>D</a>";
                    }

                    foreach (var game in lastGamesB)
                    {
                        var teamGame = FilesAccess.db.teamGames.SingleOrDefault(x => x.Id == game.IdTeamGame);
                        string link = "";
                        if (teamGame != null)
                            link = $"/detalhes/{FilesAccess.db.seasons.Single(x => x.id == game.IdSeason).name}/{FilesAccess.db.championships.Single(x => x.Id == game.IdChamp).Name}/{teamGame.Id}-{FilesAccess.db.clubs.Single(x => x.Id == teamGame.ClubAId).Name}-vs-{FilesAccess.db.clubs.Single(x => x.Id == teamGame.ClubBId).Name}";
                        else
                            link = $"/detalhes/0";
                        if (game.IsWon)
                            formaBTd.InnerHtml +=
                                $"<a href='{link}' class='sign win' style='display: inline-block'>V</a>";
                        else
                            formaBTd.InnerHtml +=
                                $"<a href='{link}' class='sign lost' style='display: inline-block'>D</a>";
                    }

                    Title = "Historico "+ playerA.Name +" VS "+playerB;
                    MetaKeywords += $"{playerA.Name}, Historico, {playerB.Name}";
                }
                
                //put the historic of the player in datagridview5

                if (ViewState["SeasonFilter"] == null || ViewState["SeasonFilter"].ToString() == "ALL")
                {
                    seasons = FilesAccess.db.seasons.OrderByDescending(x => x.startDate).ToList();
                }
                else if(ViewState["SeasonFilter"] != null)
                {
                    seasons = FilesAccess.db.seasons.Where(a=>a.name== ViewState["SeasonFilter"].ToString()).OrderByDescending(x => x.startDate).ToList(); 
                }

                var championships =
                    from c in FilesAccess.db.championships
                    from tt in FilesAccess.db.tournamentType
                    where c.idTournamentType == tt.id && tt.championship == true
                    select new
                    {
                        Id = c.Id,
                        Name = c.Name
                    };

                if (!isDoubles)
                {
                    PanelJogosIndividuais.Visible = true;
                    listaJogos = GetSinglesGamesListQuery(playerAId);
                    GridView1.DataSource = listaJogos;
                    GridView1.DataBind();
                }
                else
                {
                    PanelJogosIndividuais.Visible = false;
                }
                
                listaJogosDouble = GetDoubleGamesListQuery(playerAId);

                if (listaJogosDouble.Count() == 0)
                {
                    jogosParesPanel.Visible = false;
                }
                else
                {
                    jogosParesPanel.Visible = true;
                    //lock (locker)
                    //{
                        GridView4.DataSource = listaJogosDouble;
                        GridView4.DataBind();
                    //}
                }
                jogosA.InnerHtml = (listaJogos.Count(x => x.IsWon) + listaJogosDouble.Count(x => x.IsWon)).ToString();
                jogosB.InnerHtml = (listaJogos.Count(x => !x.IsWon) + listaJogosDouble.Count(x => !x.IsWon)).ToString();
            }
        }
        
        protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
        {
            var sortDirection = GetSortDirection("GridView1"+e.SortExpression);
            switch (e.SortExpression)
            {
                case "Data":
                    switch (sortDirection)
                    {
                        case System.Web.UI.WebControls.SortDirection.Ascending:
                            listaJogos=listaJogos.OrderBy(x => DateTime.ParseExact(x.Data, "dd/MM/yyyy", CultureInfo.InvariantCulture));
                            break;
                        case System.Web.UI.WebControls.SortDirection.Descending:
                            listaJogos = listaJogos.OrderByDescending(x => DateTime.ParseExact(x.Data, "dd/MM/yyyy", CultureInfo.InvariantCulture));
                            break;
                        default:
                            break;
                    }
                    break;
                case "Campeonato":
                    switch (sortDirection)
                    {
                        case System.Web.UI.WebControls.SortDirection.Ascending:
                            listaJogos = listaJogos.OrderBy(x => x.Campeonato);
                            break;
                        case System.Web.UI.WebControls.SortDirection.Descending:
                            listaJogos = listaJogos.OrderByDescending(x => x.Campeonato);
                            break;
                        default:
                            break;
                    }
                    break;
                case "Adversario":
                    switch (sortDirection)
                    {
                        case System.Web.UI.WebControls.SortDirection.Ascending:
                            listaJogos = listaJogos.OrderBy(x => x.Adversario);
                            break;
                        case System.Web.UI.WebControls.SortDirection.Descending:
                            listaJogos = listaJogos.OrderByDescending(x => x.Adversario);
                            break;
                        default:
                            break;
                    }
                    break;
                case "Resultado":
                    switch (sortDirection)
                    {
                        case System.Web.UI.WebControls.SortDirection.Ascending:
                            listaJogos = listaJogos.OrderBy(x => x.Resultado);
                            break;
                        case System.Web.UI.WebControls.SortDirection.Descending:
                            listaJogos = listaJogos.OrderByDescending(x => x.Resultado);
                            break;
                        default:
                            break;
                    }
                    break;
                case "Pontos":
                    switch (sortDirection)
                    {
                        case System.Web.UI.WebControls.SortDirection.Ascending:
                            listaJogos=listaJogos.OrderBy(x => x.Pontos);
                            break;
                        case System.Web.UI.WebControls.SortDirection.Descending:
                            listaJogos=listaJogos.OrderByDescending(x => x.Pontos);
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }

            GridView1.DataSource = listaJogos.ToList();
            GridView1.DataBind();
            
            GridView4.DataSource = listaJogosDouble;
            GridView4.DataBind();
        }

        private System.Web.UI.WebControls.SortDirection GetSortDirection(string column)
        {
            System.Web.UI.WebControls.SortDirection nextDir = System.Web.UI.WebControls.SortDirection.Ascending; // Default next sort expression behaviour.
            if (ViewState["sort"] != null && ViewState["sort"].ToString() == column)
            {   // Exists... DESC.
                nextDir = System.Web.UI.WebControls.SortDirection.Descending;
                ViewState["sort"] = null;
            }
            else
            {   // Doesn't exists, set ViewState.
                ViewState["sort"] = column;
            }
            return nextDir;
        }

        private IEnumerable<ListGamesDoubles> GetDoubleGamesListQuery(int id)
        {
            List<doublegamesRow> allGames = new List<doublegamesRow>();

            bool isDoubles = playerAId >> 16 > 0 ? true : false;
            List<int> playersId = new List<int>();
            int doublePlayerAId = 0;
            int doublePlayerBId = 0;
            if (isDoubles)
            {
                var doublePlayerA = FilesAccess.db.player.Single(x => x.Id == (playerAId >> 16));
                var doublePlayerB = FilesAccess.db.player.Single(x => x.Id == (playerAId & 0xFF));

                doublePlayerAId = doublePlayerA.Id;
                doublePlayerBId = doublePlayerB.Id;
                playerAId = doublePlayerAId;

                playersId.Add(doublePlayerA.Id);
                playersId.Add(doublePlayerB.Id);
            }
            else
            {
                playersId.Add(playerAId);
            }

            if (championship.Id != FilesAccess.db.championships.Single(x=>x.Name=="TodosCampeonatos").Id)
            {
                foreach (var playerId in playersId)
                {
                    allGames.AddRange(FilesAccess.db.doubleGames.Where(x => x.IdChampionship == championship.Id &&
                    (x.DoubleA_PlayerAId == playerId || x.DoubleA_PlayerBId == playerId ||
                    x.DoubleB_PlayerAId == playerId || x.DoubleB_PlayerBId == playerId)).ToList());
                }
                allGames = allGames.Distinct().ToList();
            }
            else
                allGames = FilesAccess.db.doubleGames.ToList();
            
            if (playerAId > 0)
            {
                if (!isDoubles)
                {
                    allGames = allGames.Where(x => x.DoubleA_PlayerAId == playerAId || x.DoubleA_PlayerBId == playerAId ||
                        x.DoubleB_PlayerAId == playerAId || x.DoubleB_PlayerBId == playerAId).ToList();
                }
                else
                {
                    allGames = Utilities.DoublesGamesTogether(allGames, doublePlayerAId, doublePlayerBId);
                }
            }
            var players = FilesAccess.db.player;
            var data = from g in allGames
                      // from ph in FilesAccess.db.phase
                       orderby g.Date descending
                       where 
                       !isDoubles?
                            (((g.DoubleA_PlayerAId == playerAId || g.DoubleA_PlayerBId == playerAId ||
                    g.DoubleB_PlayerAId == playerAId || g.DoubleB_PlayerBId == playerAId)) && playerBId == -1 ? true : 
                    (g.DoubleA_PlayerAId == playerBId || g.DoubleA_PlayerBId == playerBId ||
                    g.DoubleB_PlayerAId == playerBId || g.DoubleB_PlayerBId == playerBId))

                       :
                            (g.DoubleA_PlayerAId == doublePlayerAId && g.DoubleA_PlayerBId == doublePlayerBId) ||
                            (g.DoubleA_PlayerAId == doublePlayerBId && g.DoubleA_PlayerBId == doublePlayerAId) ||
                            (g.DoubleB_PlayerAId == doublePlayerAId && g.DoubleB_PlayerBId == doublePlayerBId) ||
                            (g.DoubleB_PlayerAId == doublePlayerBId && g.DoubleB_PlayerBId == doublePlayerAId)

                        &&
                       g.Date >= season.startDate && g.Date <= season.endDate //&& ph.id == g.phaseId
                       select new ListGamesDoubles()
                       {
                           Id = (g.DoubleA_PlayerAId == playerAId || g.DoubleA_PlayerBId == playerAId) ? g.DoubleA_PlayerAId << 16 | g.DoubleA_PlayerBId : g.DoubleB_PlayerAId << 16 | g.DoubleB_PlayerBId,
                           IdSeason = season.id,
                           Season = season.name,
                           Campeonato = FilesAccess.db.championships.Single(x => x.Id == g.IdChampionship).Name,// + (ph.order != 0 ? " - " + ph.phaseShort : ""),
                           IdAdv = 
                                !isDoubles ?
                                    (g.DoubleA_PlayerAId == playerAId || g.DoubleA_PlayerBId == playerAId) ? g.DoubleB_PlayerAId <<16 | g.DoubleB_PlayerBId : g.DoubleA_PlayerAId << 16 | g.DoubleA_PlayerBId
                                :
                                    (g.DoubleA_PlayerAId == doublePlayerAId || g.DoubleA_PlayerBId == doublePlayerAId) ? g.DoubleB_PlayerAId << 16 | g.DoubleB_PlayerBId :
                                    g.DoubleA_PlayerAId << 16 | g.DoubleA_PlayerBId
                                ,
                           Data = g.Date.ToString("dd/MM/yyyy"),
                           Par =
                                !isDoubles ?
                                (g.DoubleA_PlayerAId == playerAId || g.DoubleA_PlayerBId == playerAId) ?
                                    players.SingleOrDefault(x => x.Id == g.DoubleA_PlayerAId)?.Name + "|" + players.SingleOrDefault(x => x.Id == g.DoubleA_PlayerBId)?.Name :
                                    players.SingleOrDefault(x => x.Id == g.DoubleB_PlayerAId)?.Name + "|" + players.SingleOrDefault(x => x.Id == g.DoubleB_PlayerBId)?.Name
                                :
                                players.SingleOrDefault(x => x.Id == doublePlayerAId)?.Name + "|" + players.SingleOrDefault(x => x.Id == doublePlayerBId)?.Name,

                           ParAdversario =
                                !isDoubles ?
                                (g.DoubleA_PlayerAId == playerAId || g.DoubleA_PlayerBId == playerAId) ? 
                                
                                    players.SingleOrDefault(x => x.Id == g.DoubleB_PlayerAId)?.Name+"|"+ players.SingleOrDefault(x => x.Id == g.DoubleB_PlayerBId)?.Name 
                                    :
                                    players.SingleOrDefault(x => x.Id == g.DoubleA_PlayerAId)?.Name + "|" + players.SingleOrDefault(x => x.Id == g.DoubleA_PlayerBId)?.Name
                                :
                                (g.DoubleA_PlayerAId == doublePlayerAId || g.DoubleA_PlayerBId == doublePlayerAId) ?
                                    players.SingleOrDefault(x => x.Id == g.DoubleB_PlayerAId)?.Name + "|" + players.SingleOrDefault(x => x.Id == g.DoubleB_PlayerBId)?.Name
                                    :
                                    players.SingleOrDefault(x => x.Id == g.DoubleA_PlayerAId)?.Name + "|" + players.SingleOrDefault(x => x.Id == g.DoubleA_PlayerBId)?.Name,

                           Resultado = (g.DoubleA_PlayerAId == playerAId || g.DoubleA_PlayerBId == playerAId) ? $"{g.SetsA}-{g.SetsB}" : $"{g.SetsB}-{g.SetsA}",
                           Pontos = (g.DoubleA_PlayerAId == playerAId || g.DoubleA_PlayerBId == playerAId) ? g.PointsDoubleA_PlayerA+g.PointsDoubleA_PlayerB : g.PointsDoubleB_PlayerA + g.PointsDoubleB_PlayerB,
                           IdChamp = g.IdChampionship,
                           graphData = (g.DoubleA_PlayerAId == playerAId || g.DoubleA_PlayerBId == playerAId) ? g.SetsA - g.SetsB : g.SetsB - g.SetsA,

                           VerHistorico =
                                (g.DoubleA_PlayerAId == playerAId || g.DoubleA_PlayerBId == playerAId) ?

                                players.SingleOrDefault(x => x.Id == g.DoubleA_PlayerAId)?.Name + "|" + players.SingleOrDefault(x => x.Id == g.DoubleA_PlayerBId)?.Name + " VS " + players.SingleOrDefault(x => x.Id == g.DoubleB_PlayerAId)?.Name + "/" + players.SingleOrDefault(x => x.Id == g.DoubleB_PlayerBId)?.Name :

                                players.SingleOrDefault(x => x.Id == g.DoubleB_PlayerAId)?.Name + "|" + players.SingleOrDefault(x => x.Id == g.DoubleB_PlayerBId)?.Name + " VS "+ players.SingleOrDefault(x => x.Id == g.DoubleA_PlayerAId)?.Name + "/" + players.SingleOrDefault(x => x.Id == g.DoubleA_PlayerBId)?.Name,

                           GameType = 2,
                           DateGame = g.Date,
                           IsWon = (id == g.DoubleA_PlayerAId || id == g.DoubleA_PlayerBId) 
                                        ? (g.SetsA > g.SetsB ? true : false) : (g.SetsB > g.SetsA ? true : false),
                           IdTeamGame = g.IdTeamGame
                       };
            return data;
        }

        private IEnumerable<ListGames> GetSinglesGamesListQuery(int id)
        {
            List<gamesRow> allGames = new List<gamesRow>();
            if (championship.Id != FilesAccess.db.championships.Single(x => x.Name == "TodosCampeonatos").Id)
                allGames = FilesAccess.db.games.Where(x => x.IdChamp == championship.Id && 
                (x.IdPlayerA == playerA.Id || x.IdPlayerB == playerA.Id) && (x.IdPlayerA == playerB.Id || x.IdPlayerB == playerB.Id)).ToList();
            else
                allGames = FilesAccess.db.games.Where(x => !Utilities.AllOpenTournaments.Contains(x.IdChamp)).ToList();
            
            if (playerAId > 0)
                allGames = allGames.Where(x => x.IdPlayerA == playerAId || x.IdPlayerB == playerAId).ToList();
            
            if (playerBId > 0)
                allGames = allGames.Where(x => x.IdPlayerA == playerBId || x.IdPlayerB == playerBId).ToList();
            
            var data = from g in allGames
                       from p in FilesAccess.db.player
                       from ph in FilesAccess.db.phase
                       orderby g.Date descending, ph.order descending
                       where ((g.IdPlayerA == playerA.Id && g.IdPlayerA == p.Id) || (g.IdPlayerB == playerA.Id && g.IdPlayerB == p.Id))
                       && g.Date >= season.startDate
                            && g.Date <= season.endDate && ph.id == g.idPhase
                       select new ListGames()
                       {
                           Nome = playerA.Name,
                           Id = playerAId,
                           IdSeason = season.id,
                           Season = season.name,
                           IdChamp = g.IdChamp,
                           Campeonato = FilesAccess.db.championships.Single(x => x.Id == g.IdChamp).Name + (ph.order!=0 ? " - " + ph.phaseShort : ""),
                           Data = g.Date.ToString("dd/MM/yyyy"),
                           Resultado = id == g.IdPlayerA ? $"{g.SetsA}-{g.SetsB}" : $"{g.SetsB}-{g.SetsA}",
                           GameType = 1,
                           DateGame = g.Date,
                           IsWon = id == g.IdPlayerA ? (g.SetsA > g.SetsB ? true : false) : (g.SetsB > g.SetsA ? true : false),
                           IdTeamGame = g.IdTeamGame
                       };
            
            return data;
        }

        protected void ButtonBack_Click(object sender, EventArgs e)
        {
            object refUrl = ViewState["RefUrl"];
            if (refUrl != null)
                Response.Redirect((string)refUrl);
        }
    }
}