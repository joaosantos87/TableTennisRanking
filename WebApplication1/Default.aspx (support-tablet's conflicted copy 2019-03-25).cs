﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TableTennis.Database;

namespace WebApplication1
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Stopwatch s1 = Stopwatch.StartNew();
            var players = Ranking.Instance.GenerateRanking();
            var lastGames =
                    from g in FilesAccess.db.games
                    select new
                    {
                        Id = g.id,
                        IdChamp = g.IdChamp,
                        IdPlayerA = g.IdPlayerA,
                        IdPlayerB = g.IdPlayerB,
                        Data = g.Date.ToString("dd/MM/yyyy"),
                        Campeonato = FilesAccess.db.championships.SingleOrDefault(x => x.Id == g.IdChamp)?.Name,
                        JogadorA = players.SingleOrDefault(x => x.Id == g.IdPlayerA)?.Name,
                        JogadorB = players.SingleOrDefault(x => x.Id == g.IdPlayerB)?.Name,
                        Resultado = $"{g.SetsA}-{g.SetsB}",
                        PontosA = g.pointsPlayerA,
                        PontosB = g.pointsPlayerB
                    };

            GridView2.DataSource = lastGames.OrderByDescending(x => x.Id).Take(20);
            GridView2.DataBind();
            
            var bigestUpsetResultsA =
                    from g in FilesAccess.db.games
                    orderby g.pointsPlayerA descending
                    where Math.Abs(g.pointsPlayerA) < 100 && Math.Abs(g.pointsPlayerB) < 100
                    select new
                    {
                        IdChamp = g.IdChamp,
                        IdPlayerA = g.IdPlayerA,
                        IdPlayerB = g.IdPlayerB,
                        Data = g.Date.ToString("dd/MM/yyyy"),
                        Campeonato = FilesAccess.db.championships.SingleOrDefault(x => x.Id == g.IdChamp)?.Name,
                        JogadorA = players.SingleOrDefault(x => x.Id == g.IdPlayerA)?.Name,
                        JogadorB = players.SingleOrDefault(x => x.Id == g.IdPlayerB)?.Name,
                        Resultado = $"{g.SetsA}-{g.SetsB}",
                        PontosA = g.pointsPlayerA,
                        PontosB = g.pointsPlayerB,
                        PontosBaixos = g.pointsPlayerA < g.pointsPlayerB ? g.pointsPlayerA : g.pointsPlayerB
                    };

            var bigestUpsetResultsB =
                    from g in FilesAccess.db.games
                    orderby g.pointsPlayerB descending
                    where Math.Abs(g.pointsPlayerA) < 100 && Math.Abs(g.pointsPlayerB) < 100
                    select new
                    {
                        IdChamp = g.IdChamp,
                        IdPlayerA = g.IdPlayerA,
                        IdPlayerB = g.IdPlayerB,
                        Data = g.Date.ToString("dd/MM/yyyy"),
                        Campeonato = FilesAccess.db.championships.SingleOrDefault(x => x.Id == g.IdChamp)?.Name,
                        JogadorA = players.SingleOrDefault(x => x.Id == g.IdPlayerA)?.Name,
                        JogadorB = players.SingleOrDefault(x => x.Id == g.IdPlayerB)?.Name,
                        Resultado = $"{g.SetsA}-{g.SetsB}",
                        PontosA = g.pointsPlayerA,
                        PontosB = g.pointsPlayerB,
                        PontosBaixos = g.pointsPlayerA < g.pointsPlayerB ? g.pointsPlayerA : g.pointsPlayerB
                    };

            GridView1.DataSource = bigestUpsetResultsA.Union(bigestUpsetResultsB).OrderBy(x => x.PontosBaixos).Take(20);
            GridView1.DataBind();
            
            GridView3.DataSource = lastGames.OrderByDescending(x => DateTime.ParseExact(x.Data,"dd/MM/yyyy", CultureInfo.InvariantCulture)).Take(20);
            GridView3.DataBind();

            var lastPlayers =
                from p in players
                from c in FilesAccess.db.clubs
                where p.IdClub == c.Id
                select new
                {
                    Id = p.Id,
                    Licenca = p.licenseNo,
                    CurrentRank = p.currentRank,
                    Nome = p.Name,
                    Club = c.Name,
                    Record = $"{p.currentGamesWon}-{p.currentGamesLost}",
                    Pontos = p.currentPoints,
                    IdClub = c.Id
                };

            GridView4.DataSource = lastPlayers.OrderByDescending(x => x.Id).Take(20);
            GridView4.DataBind();

            s1.Stop();
            Label1.InnerText = s1.ElapsedMilliseconds.ToString();
        }
    }
}