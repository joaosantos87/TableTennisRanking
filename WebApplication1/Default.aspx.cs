﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using TableTennis;
using TableTennis.Database;

namespace WebApplication1
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            MySqlDatabase.Instance.CheckForceInitialization();
            Stopwatch s1 = Stopwatch.StartNew();
            var players = FilesAccess.db.player;
            var a = Task.Factory.StartNew(() =>
            {
                var lastGames =
                        from g in FilesAccess.db.games
                        select new
                        {
                            Id = g.id,
                            IdChamp = g.IdChamp,
                            IdPlayerA = g.IdPlayerA,
                            IdPlayerB = g.IdPlayerB,
                            Data = g.Date.ToString("dd/MM/yyyy"),
                            Campeonato = FilesAccess.db.championships.SingleOrDefault(x => x.Id == g.IdChamp)?.Name,
                            JogadorA = players.SingleOrDefault(x => x.Id == g.IdPlayerA)?.Name,
                            JogadorB = players.SingleOrDefault(x => x.Id == g.IdPlayerB)?.Name,
                            Resultado = $"{g.SetsA}-{g.SetsB}",
                            PontosA = g.pointsPlayerA,
                            PontosB = g.pointsPlayerB,
                            Epoca = Utilities.EpocaActual.name
                        };

                GridView2.DataSource = lastGames.OrderByDescending(x => x.Id).Take(20);
                GridView2.DataBind();

                GridView3.DataSource = lastGames.OrderByDescending(x => DateTime.ParseExact(x.Data, "dd/MM/yyyy", CultureInfo.InvariantCulture)).Take(20);
                GridView3.DataBind();
            });

            var b = Task.Factory.StartNew(() =>
            {
                var bigestUpsetResultsA =
                    from g in FilesAccess.db.games
                    orderby g.pointsPlayerA descending
                    where Math.Abs(g.pointsPlayerA) > 50 && Math.Abs(g.pointsPlayerB) > 50 && g.Date > g.Date.Subtract(TimeSpan.FromDays(100))
                    select new
                    {
                        IdChamp = g.IdChamp,
                        IdPlayerA = g.IdPlayerA,
                        IdPlayerB = g.IdPlayerB,
                        Data = g.Date.ToString("dd/MM/yyyy"),
                        Date = g.Date,
                        Campeonato = FilesAccess.db.championships.SingleOrDefault(x => x.Id == g.IdChamp)?.Name + (g.idPhase != Utilities.Phase_NA ? " "+FilesAccess.db.phase.SingleOrDefault(x => x.id == g.idPhase)?.phase : ""),
                        JogadorA = players.SingleOrDefault(x => x.Id == g.IdPlayerA)?.Name,
                        JogadorB = players.SingleOrDefault(x => x.Id == g.IdPlayerB)?.Name,
                        Resultado = $"{g.SetsA}-{g.SetsB}",
                        PontosA = g.pointsPlayerA,
                        PontosB = g.pointsPlayerB,
                        PontosBaixos = g.pointsPlayerA < g.pointsPlayerB ? g.pointsPlayerA : g.pointsPlayerB
                    };

                var bigestUpsetResultsB =
                    from g in FilesAccess.db.games
                    orderby g.pointsPlayerB descending
                    where Math.Abs(g.pointsPlayerA) > 50 && Math.Abs(g.pointsPlayerB) < 50 && g.Date > g.Date.Subtract(TimeSpan.FromDays(100))
                    select new
                    {
                        IdChamp = g.IdChamp,
                        IdPlayerA = g.IdPlayerA,
                        IdPlayerB = g.IdPlayerB,
                        Data = g.Date.ToString("dd/MM/yyyy"),
                        Date = g.Date,
                        Campeonato = FilesAccess.db.championships.SingleOrDefault(x => x.Id == g.IdChamp)?.Name + (g.idPhase != Utilities.Phase_NA ? " " + FilesAccess.db.phase.SingleOrDefault(x => x.id == g.idPhase)?.phase : ""),
                        JogadorA = players.SingleOrDefault(x => x.Id == g.IdPlayerA)?.Name,
                        JogadorB = players.SingleOrDefault(x => x.Id == g.IdPlayerB)?.Name,
                        Resultado = $"{g.SetsA}-{g.SetsB}",
                        PontosA = g.pointsPlayerA,
                        PontosB = g.pointsPlayerB,
                        PontosBaixos = g.pointsPlayerA < g.pointsPlayerB ? g.pointsPlayerA : g.pointsPlayerB
                    };

            GridView1.DataSource = bigestUpsetResultsA.Union(bigestUpsetResultsB).OrderByDescending(x => x.Date).Take(20);
            GridView1.DataBind();
            
            });

            var d = Task.Factory.StartNew(() =>
            {
                var lastPlayers =
                from p in players
                from c in FilesAccess.db.clubs
                where p.IdClub == c.Id
                select new
                {
                    Id = p.Id,
                    Licenca = p.licenseNo,
                    CurrentRank = p.currentRank,
                    Nome = p.Name,
                    Club = c.Name,
                    Record = $"{p.currentGamesWon}-{p.currentGamesLost}",
                    Pontos = p.currentPoints,
                    IdClub = c.Id
                };

                GridView4.DataSource = lastPlayers.OrderByDescending(x => x.Id).Take(20);
                GridView4.DataBind();
            });
            Task.WaitAll(a,b,d);
            s1.Stop();
            Label1.InnerText = s1.ElapsedMilliseconds.ToString();
        }
    }
}