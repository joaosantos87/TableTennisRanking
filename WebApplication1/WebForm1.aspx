﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="WebApplication1.WebForm1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h3> Ultimos jogos inseridos
    </h3>
    <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="false">
        <Columns>
            <asp:BoundField DataField="IdChamp" HeaderText="IdChamp" InsertVisible="False" ReadOnly="True" SortExpression="IdChamp" Visible="false" />
            <asp:BoundField DataField="IdPlayerA" HeaderText="IdPlayerA" InsertVisible="False" ReadOnly="True" SortExpression="IdPlayerA" Visible="false" />
            <asp:BoundField DataField="IdPlayerB" HeaderText="IdPlayerB" InsertVisible="False" ReadOnly="True" SortExpression="IdPlayerB" Visible="false" />
            <asp:BoundField DataField="Data" HeaderText="Data" SortExpression="Data" />
            <asp:HyperLinkField DataTextField="JogadorA" HeaderText="Jogador" SortExpression="JogadorA" DataNavigateUrlFields="IdPlayerA, IdChamp"  DataNavigateUrlFormatString="~/Players.aspx?ID={0}&campeonatos={1}"/>
            <asp:BoundField DataField="PontosA" HeaderText="Pontos Rating" InsertVisible="False" ReadOnly="True" SortExpression="Pontos" />
            <asp:BoundField DataField="Resultado" HeaderText="Resultado" SortExpression="Resultado" ReadOnly="false" />
            <asp:HyperLinkField DataTextField="JogadorB" HeaderText="Jogador" SortExpression="JogadorB" DataNavigateUrlFields="IdPlayerB, IdChamp"  DataNavigateUrlFormatString="~/Players.aspx?ID={0}&campeonatos={1}"/>
            <asp:BoundField DataField="PontosB" HeaderText="Pontos Rating" InsertVisible="False" ReadOnly="True" SortExpression="Pontos" />
        </Columns>
    </asp:GridView>
</asp:Content>
