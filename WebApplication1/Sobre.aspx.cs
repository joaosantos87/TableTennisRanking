﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using TableTennis.Database;

namespace WebApplication1
{
    public partial class About : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            numeroJogos.InnerText = FilesAccess.db.games.Count.ToString();
            numeroTorneios.InnerText = FilesAccess.db.tournamentType.Count.ToString();
        }
    }
}