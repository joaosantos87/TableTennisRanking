﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Helpers;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using TableTennis;
using TableTennis.Database;
using static TableTennis.Database.DataSet2;

namespace WebApplication1
{
    public partial class Players : Page
    {
        int playerId = -1;
        playerRow playerGlobal;
        int idAdv;
        List<playerRow> players;
        List<seasonRow> seasons;
        IEnumerable<ListGames> listaJogos = new List<ListGames>();
        IEnumerable<ListGames> listaJogosDouble = new List<ListGames>();
        List<ListRanking> playerHistoric = new List<ListRanking>();
        championshipsRow championship;
        seasonRow season;

        static object locker = new object();
        
        protected void Page_Load(object sender, EventArgs e)
        {
            Stopwatch s1 = Stopwatch.StartNew();

            //initial keywords, then other keywords are inserted below in the code
            MetaKeywords = "Tenis de mesa, Jogos, resultados, ";

            //get the routeData from the URL
            playerId = Page.RouteData.Values.ContainsKey("Id") ? Int32.TryParse(Page.RouteData.Values["Id"] as string, out playerId) ? playerId : -1 : -1;

            season = Page.RouteData.Values.ContainsKey("Epoca") ? FilesAccess.db.seasons.Single(x => x.name == Page.RouteData.Values["Epoca"].ToString()) : FilesAccess.db.seasons.Single(x => x.name == "TodasEpocas");

            if (season.name == "TodasEpocas")
                season.endDate = DateTime.Today;

            championship = Page.RouteData.Values.ContainsKey("Campeonato") ? FilesAccess.db.championships.Single(x => x.Name == Page.RouteData.Values["Campeonato"].ToString()) : FilesAccess.db.championships.Single(x => x.Name == "TodosCampeonatos");
            
            idAdv = Page.RouteData.Values.ContainsKey("IdAdv") ? Int32.TryParse(Page.RouteData.Values["IdAdv"] as string, out idAdv) ? idAdv : -1 : -1;
                            
            //Players have the last ranking that was created
            List<playerRow> playersActual = Ranking.Instance.Players;

            players = playersActual;

            list.Visible = false;

            //check if it is to view double games
            bool isDoubles = playerId >> 16 > 0 ? true : false;
            
            if (players.Count(x => x.Id == playerId) == 1 || isDoubles)
            {
                list.Visible = true;
                if (isDoubles)
                {
                    var doublePlayerA = players.Single(x => x.Id == (playerId >> 16));
                    var doublePlayerB = players.Single(x => x.Id == (playerId & 0xFF));

                    var rankDataGlobalPlayerA = FilesAccess.db.ranking.SingleOrDefault(x => x.idPlayer == doublePlayerA.Id && x.idChamp == Utilities.TodosCampeonatos.Id && x.idSeason == Utilities.TodasEpocas.id);
                    var rankDataGlobalPlayerB = FilesAccess.db.ranking.SingleOrDefault(x => x.idPlayer == doublePlayerB.Id && x.idChamp == Utilities.TodosCampeonatos.Id && x.idSeason == Utilities.TodasEpocas.id);

                    HyperLink playerAlink = new HyperLink()
                    {
                        Text = doublePlayerA.Name,
                        NavigateUrl = $"/Jogador/{doublePlayerA.Id}-{doublePlayerA.Name}"
                    };

                    HyperLink playerBlink = new HyperLink()
                    {
                        Text = doublePlayerB.Name,
                        NavigateUrl = $"/Jogador/{doublePlayerB.Id}-{doublePlayerB.Name}"
                    };

                    nomeJogador.InnerHtml = $"<a href='{playerAlink.NavigateUrl}'>{playerAlink.Text}</a>" + " / " + $"<a href='{playerBlink.NavigateUrl}'>{playerBlink.Text}</a>";
                    Label3.Text = rankDataGlobalPlayerA?.rank.ToString() + " / " + rankDataGlobalPlayerB.rank.ToString();

                    Label4.Text = Utilities.DoubleNumberGamesTogetherWon(championship.Id, doublePlayerA.Id, doublePlayerB.Id).ToString() + " - " + Utilities.DoubleNumberGamesTogetherLost(championship.Id, doublePlayerA.Id, doublePlayerB.Id).ToString();
                    
                    Label5.Text = rankDataGlobalPlayerA?.points+ " / "+ rankDataGlobalPlayerB?.points;
                    Label10.Text = doublePlayerA.licenseNo + " / "+ doublePlayerB.licenseNo;

                    Title = doublePlayerA.Name + " / " + doublePlayerB.Name;
                    MetaKeywords += $"Pares, {doublePlayerA.Name},{doublePlayerB.Name},{FilesAccess.db.clubs.Single(x => x.Id == doublePlayerA.IdClub).Name},{FilesAccess.db.clubs.Single(x => x.Id == doublePlayerB.IdClub).Name}, {doublePlayerA.licenseNo}, {doublePlayerB.licenseNo} ";
                }
                else
                {
                    var rankDataGlobal = FilesAccess.db.ranking.SingleOrDefault(x => x.idPlayer == playerId && x.idChamp == Utilities.TodosCampeonatos.Id && x.idSeason == Utilities.TodasEpocas.id);

                    playerGlobal = players.Single(x => x.Id == playerId);
                    nomeJogador.InnerText = playerGlobal.Name;
                    Label3.Text = rankDataGlobal?.rank.ToString();
                    Label4.Text = $"{rankDataGlobal?.numWins} - {rankDataGlobal?.numLosses}";
                    Label5.Text = rankDataGlobal?.points.ToString();
                    Label10.Text = playerGlobal.licenseNo.ToString();

                    var lastGames = GetSinglesGamesListQuery().ToList();
                    lastGames.AddRange(GetDoubleGamesListQuery());
                    lastGames = lastGames.OrderByDescending(x => x.DateGame).Take(5).ToList();

                    foreach (var game in lastGames)
                    {
                        var teamGame = FilesAccess.db.teamGames.SingleOrDefault(x => x.Id == game.IdTeamGame);
                        string link = "";
                        if (teamGame != null)
                            link = $"/detalhes/{FilesAccess.db.seasons.Single(x => x.id == game.IdSeason).name}/{FilesAccess.db.championships.Single(x => x.Id == game.IdChamp).Name}/{teamGame.Id}-{FilesAccess.db.clubs.Single(x => x.Id == teamGame.ClubAId).Name}-vs-{FilesAccess.db.clubs.Single(x => x.Id == teamGame.ClubBId).Name}";
                        else
                            link = $"/detalhes/0";
                        if (game.IsWon)
                            formaTd.InnerHtml +=
                                $"<a href='{link}' class='sign win' style='display: inline-block'>V</a>";
                        else
                            formaTd.InnerHtml +=
                                $"<a href='{link}' class='sign lost' style='display: inline-block'>D</a>";
                    }
                    Title = playerGlobal.Name;
                    MetaKeywords += $"{playerGlobal.Name}, {Label10.Text}";
                }

                var playerPesquisa = playersActual?.SingleOrDefault(x => x.Id == playerId);
                
                if (playerGlobal == playerPesquisa || playerPesquisa == null)
                {
                    rankingActual.Visible = false;
                    pontosActual.Visible = false;
                    recordActual.Visible = false;
                    detalhesPesquisa.Visible = false;
                }
                else
                {
                    rankingActual.Visible = true;
                    pontosActual.Visible = true;
                    recordActual.Visible = true;
                    detalhesPesquisa.Visible = true;

                    Label6.Text = playerPesquisa.currentRank.ToString();
                    Label7.Text = $"{playerPesquisa.currentGamesWon} - {playerPesquisa.currentGamesLost}";
                    Label8.Text = playerPesquisa.currentPoints.ToString();
                    Label9.Text = $"{season.startDate.ToString("dd/MM/yyyy")} - {season.endDate.ToString("dd/MM/yyyy")}";
                }

                //put the historic of the player in datagridview5

                if (ViewState["SeasonFilter"] == null || ViewState["SeasonFilter"].ToString() == "ALL")
                {
                    seasons = FilesAccess.db.seasons.OrderByDescending(x => x.startDate).ToList();
                }
                else if(ViewState["SeasonFilter"] != null)
                {
                    seasons = FilesAccess.db.seasons.Where(a=>a.name== ViewState["SeasonFilter"].ToString()).OrderByDescending(x => x.startDate).ToList(); 
                }

                var championships =
                    from c in FilesAccess.db.championships
                    from tt in FilesAccess.db.tournamentType
                    where c.idTournamentType == tt.id && tt.championship == true
                    select new
                    {
                        Id = c.Id,
                        Name = c.Name
                    };
                
                if (!isDoubles)
                {
                    foreach (var season in seasons)
                    {
                        foreach (var champ in championships)
                        {
                            var data =
                                from r in FilesAccess.db.ranking
                                where r.idChamp == champ.Id && r.idSeason == season.id && r.idPlayer == playerGlobal.Id
                                select new ListRanking()
                                {
                                    Season = FilesAccess.db.seasons.Single(x => x.id == r.idSeason).name,
                                    Campeonato = FilesAccess.db.championships.Single(x => x.Id == r.idChamp).Name,
                                    Clube = FilesAccess.db.clubs.Single(x => x.Id == r.idClub).Name,
                                    Ranking = r.rank,
                                    Pontos = r.points,
                                    Record = r.numWins + " - " + r.numLosses,
                                    IdClub = r.idClub,
                                    IdChamp = r.idChamp,
                                    Nome = FilesAccess.db.player.Single(x => x.Id == r.idPlayer).Name,
                                    Id = r.idPlayer,
                                    Won = r.numWins,
                                    Lost = r.numLosses,
                                    EndSeasonDate = FilesAccess.db.seasons.Single(x => x.id == r.idSeason).endDate
                                };
                            playerHistoric.AddRange(data);
                        }
                    }

                    GridView5.DataSource = playerHistoric;
                    GridView5.DataBind();

                    //DropDownList seasonFilter = (DropDownList)GridView5.HeaderRow.FindControl("seasonFilter");
                    //seasonFilter.DataSource = seasons.Select(x=>x.name);
                    //seasonFilter.DataBind();

                    //divide all games per champ
                    var allChamps = playerHistoric.Select(x => x.IdChamp).Distinct();
                    var allSeasons = playerHistoric.OrderByDescending(x => x.EndSeasonDate).Select(b => b.IdSeason).Distinct();
                    
                    var allChampsOrdered = from c in allChamps
                                        from c1 in FilesAccess.db.championships
                                        from tt in FilesAccess.db.tournamentType
                                        where c1.Id == c && c1.idTournamentType == tt.id
                                        orderby tt.weight descending
                                        select new
                                        {
                                            id = c
                                        };
                    /*
                    foreach(var champ in allChampsOrdered)
                    {
                        var allGamesChamp = playerHistoric.Where(x => x.IdChamp == champ.id);
                        string name = allGamesChamp.First().Campeonato;

                        HtmlGenericControl li = new HtmlGenericControl("li");
                        evolucaoAnual.Controls.Add(li);
                        HtmlGenericControl anchor = new HtmlGenericControl("a");
                        anchor.Attributes.Add("href", "#"+ name);
                        anchor.Attributes.Add("data-toggle", "tab");
                        anchor.InnerText = name;
                        li.Controls.Add(anchor);

                        //show all seasons

                        GridView objGV = new GridView();
                        objGV.AutoGenerateColumns = false;
                        objGV.CssClass = "table table-striped";

                        BoundField idCamp = new BoundField();
                        idCamp.DataField = "idCamp";
                        idCamp.Visible = false;
                        objGV.Columns.Add(idCamp);

                        BoundField idClube = new BoundField();
                        idClube.DataField = "idClube";
                        idClube.Visible = false;
                        objGV.Columns.Add(idClube);

                        BoundField epoca = new BoundField();
                        epoca.HeaderText = "Epoca";
                        epoca.DataField = "Epoca";
                        epoca.SortExpression = "Epoca";
                        epoca.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                        objGV.Columns.Add(epoca);

                        HyperLinkField campeonato = new HyperLinkField();
                        campeonato.HeaderText = "Campeonato";
                        campeonato.DataTextField = "Campeonato";
                        campeonato.DataNavigateUrlFields = new string[] { "IdCampeonato", "Campeonato" };
                        campeonato.DataNavigateUrlFormatString = "~/torneio/{0}-{1}";
                        campeonato.SortExpression = "Campeonato";
                        campeonato.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                        objGV.Columns.Add(campeonato);

                        HyperLinkField clube = new HyperLinkField();
                        clube.HeaderText = "Clube";
                        clube.DataTextField = "Clube";
                        clube.DataNavigateUrlFields = new string[] { "idClube", "Clube" };
                        clube.DataNavigateUrlFormatString = "~/clube/{0}-{1}";
                        clube.SortExpression = "Clube";
                        clube.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                        objGV.Columns.Add(clube);


                        HyperLinkField ranking = new HyperLinkField();
                        ranking.HeaderText = "Ranking";
                        ranking.DataTextField = "Ranking";
                        ranking.DataNavigateUrlFields = new string[] { "idClube", "Clube" };
                        ranking.DataNavigateUrlFormatString = "~/clube/{0}-{1}";
                        ranking.SortExpression = "Ranking";
                        ranking.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                        objGV.Columns.Add(ranking);
                    }*/

                }
                if (!isDoubles)
                {
                    PanelJogosIndividuais.Visible = true;
                    listaJogos = GetSinglesGamesListQuery();
                    GridView1.DataSource = listaJogos;
                    GridView1.DataBind();
                }
                else
                {
                    PanelJogosIndividuais.Visible = false;
                }
                
                listaJogosDouble = GetDoubleGamesListQuery();

                if (listaJogosDouble.Count() == 0)
                {
                    jogosParesPanel.Visible = false;
                }
                else
                {
                    jogosParesPanel.Visible = true;
                    //lock (locker)
                    //{
                        GridView4.DataSource = listaJogosDouble;
                        GridView4.DataBind();
                    //}
                }
                /*
                Chart1.Series.Add("Individuais Ganhos");
                var gamesWon = Chart1.Series.Last();
                gamesWon.ChartType = System.Web.UI.DataVisualization.Charting.SeriesChartType.Point;
                gamesWon.Color = System.Drawing.Color.Blue;

                var combined = listaJogos.Where(x => x.graphData > 0);
                gamesWon.Points.DataBindXY(
                    combined.Select(x => DateTime.ParseExact(x.Data, "dd/MM/yyyy", CultureInfo.InvariantCulture)).ToList(),
                    combined.Select(x => x.graphData).ToList());

                Chart1.Series.Add("Pares Ganhos");
                var gamesDoubleWon = Chart1.Series.Last();
                gamesDoubleWon.ChartType = System.Web.UI.DataVisualization.Charting.SeriesChartType.Point;
                gamesDoubleWon.Color = System.Drawing.Color.Green;
                var combinedDoubles = listaJogosDouble.Where(x => x.graphData > 0);
                gamesDoubleWon.Points.DataBindXY(
                    combinedDoubles.Select(x => DateTime.ParseExact(x.Data, "dd/MM/yyyy", CultureInfo.InvariantCulture)).ToList(),
                    combinedDoubles.Select(x => x.graphData).ToList());

                Chart1.Series.Add("Individuais Perdidos");
                var gamesLost = Chart1.Series.Last();
                gamesLost.ChartType = System.Web.UI.DataVisualization.Charting.SeriesChartType.Point;
                gamesLost.Color = System.Drawing.Color.Red;
                var combined1 = listaJogos.Where(x => x.graphData < 0);
                gamesLost.Points.DataBindXY(
                    combined1.Select(x => DateTime.ParseExact(x.Data, "dd/MM/yyyy", CultureInfo.InvariantCulture)).ToList(),
                    combined1.Select(x => x.graphData).ToList());
                
                Chart1.Series.Add("Pares Perdidos");
                var gamesDoubleLost = Chart1.Series.Last();
                gamesDoubleLost.ChartType = System.Web.UI.DataVisualization.Charting.SeriesChartType.Point;
                gamesDoubleWon.Color = System.Drawing.Color.Yellow;
                var combined1Doubles = listaJogosDouble.Where(x => x.graphData < 0);
                gamesDoubleLost.Points.DataBindXY(
                    combined1Doubles.Select(x => DateTime.ParseExact(x.Data, "dd/MM/yyyy", CultureInfo.InvariantCulture)).ToList(),
                    combined1Doubles.Select(x => x.graphData).ToList());

                Chart1.Width = 1000;
                */
            }
            else
            {
                Button1_Click(this, new EventArgs());
            }
            
            s1.Stop();
            Label1.InnerText = s1.ElapsedMilliseconds.ToString();
        }

        private void SeasonFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            MessageBox.Show("test");
        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            FillGridViewColor(e.Row.Cells[9].Text.Split('-'), e);
        }

        protected void GridView4_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            FillGridViewColor(e.Row.Cells[8].Text.Split('-'), e);
        }

        private void FillGridViewColor(string[] parseResult, GridViewRowEventArgs e)
        {
            if (parseResult.Count() == 2)
            {
                int value0 = 0;
                int value1 = 0;
                if (!Int32.TryParse(parseResult[0], out value0))
                    return;

                if (!Int32.TryParse(parseResult[1], out value1))
                    return;

                if (value0 > value1)
                    e.Row.BackColor = System.Drawing.Color.LightGreen;
                else
                    e.Row.BackColor = System.Drawing.Color.IndianRed;
            }
        }


        /// <summary>
        /// Search players button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Button1_Click(object sender, EventArgs e)
        {
            var search = HttpUtility.UrlDecode(HttpUtility.UrlEncode(TextBox1.Text.ToLower(), Encoding.GetEncoding("iso-8859-7")));
            var players =
                (from p in FilesAccess.db.player
                from c in FilesAccess.db.clubs
                orderby p.currentPoints descending
                where p.IdClub == c.Id &&
                    ((HttpUtility.UrlDecode(HttpUtility.UrlEncode(p.Name.ToLower(), Encoding.GetEncoding("iso-8859-7"))).Contains(search)) ||
                    HttpUtility.UrlDecode(HttpUtility.UrlEncode(c.Name.ToLower(), Encoding.GetEncoding("iso-8859-7"))).Contains(search) ||
                    p.licenseNo.Contains(search))
                select new
                {
                    Id = p.Id,
                    Licenca = p.licenseNo,
                    CurrentRank = p.currentRank,
                    Nome = p.Name,
                    Club = c.Name,
                    Record = $"{p.currentGamesWon}-{p.currentGamesLost}",
                    Pontos = p.currentPoints,
                    IdClub = c.Id
                }).ToList();

            if (players.Count() == 1)
            {
                var url = String.Format("~/jogador/{0}-{1}",
                    players[0].Id,
                    players[0].Nome);
                Response.Redirect(url);
            }
            else
            {
                GridView2.DataSource = players;
                GridView2.DataBind();
                GridView2.Visible = true;
            }
        }

        /// <summary>
        /// Compare players button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Button2_Click(object sender, EventArgs e)
        {
            GridView2.Visible = false;
            var search = HttpUtility.UrlDecode(HttpUtility.UrlEncode(TextBox2.Text.ToLower(), Encoding.GetEncoding("iso-8859-7")));
            var players =
                (from p in FilesAccess.db.player
                from c in FilesAccess.db.clubs
                orderby p.currentPoints descending
                where p.IdClub == c.Id &&
                    ((HttpUtility.UrlDecode(HttpUtility.UrlEncode(p.Name.ToLower(), Encoding.GetEncoding("iso-8859-7"))).Contains(search)) ||
                    HttpUtility.UrlDecode(HttpUtility.UrlEncode(c.Name.ToLower(), Encoding.GetEncoding("iso-8859-7"))).Contains(search) ||
                    p.licenseNo.Contains(search))
                select new
                {
                    Id = playerId,
                    IdAdv = p.Id,
                    Licenca = p.licenseNo,
                    CurrentRank = p.currentRank,
                    Nome = p.Name,
                    Club = c.Name,
                    Record = $"{p.currentGamesWon}-{p.currentGamesLost}",
                    Pontos = p.currentPoints,
                    IdClub = c.Id,
                    IdSeason = season.id,
                    Season = season.name,
                    IdChamp = championship.Id,
                    Campeonato = championship.Name
                }).ToList();

            if (players.Count() == 1)
            {
                var url = String.Format("~/jogador/{0}-{2}-vs-{1}-{3}", 
                    players[0].Id,
                    players[0].IdAdv,
                    FilesAccess.db.player.Single(x=>x.Id == players[0].Id).Name,
                    players[0].Nome
                    );
                Response.Redirect(url);
            }
            else
            {
                GridView3.DataSource = players;
                GridView3.DataBind();
                GridView3.Visible = true;
            }
        }

        protected void GridView5_Sorting(object sender, GridViewSortEventArgs e)
        {
            var sortDirection = GetSortDirection("GridView5" + e.SortExpression);
            switch (e.SortExpression)
            {
                case "epoca":
                    switch (sortDirection)
                    {
                        case System.Web.UI.WebControls.SortDirection.Ascending:
                            playerHistoric = playerHistoric.OrderBy(x => x.IdSeason).ToList();
                            break;
                        case System.Web.UI.WebControls.SortDirection.Descending:
                            playerHistoric = playerHistoric.OrderByDescending(x => x.IdSeason).ToList();
                            break;
                        default:
                            break;
                    }
                    break;

                case "Ranking":
                    switch (sortDirection)
                    {
                        case System.Web.UI.WebControls.SortDirection.Ascending:
                            playerHistoric = playerHistoric.OrderBy(x => x.Ranking).ToList();
                            break;
                        case System.Web.UI.WebControls.SortDirection.Descending:
                            playerHistoric = playerHistoric.OrderByDescending(x => x.Ranking).ToList();
                            break;
                        default:
                            break;
                    }
                    break;

            }
            GridView5.DataSource = playerHistoric;
            GridView5.DataBind();
        }
        protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
        {
            var sortDirection = GetSortDirection("GridView1"+e.SortExpression);
            switch (e.SortExpression)
            {
                case "Data":
                    switch (sortDirection)
                    {
                        case System.Web.UI.WebControls.SortDirection.Ascending:
                            listaJogos=listaJogos.OrderBy(x => DateTime.ParseExact(x.Data, "dd/MM/yyyy", CultureInfo.InvariantCulture));
                            break;
                        case System.Web.UI.WebControls.SortDirection.Descending:
                            listaJogos = listaJogos.OrderByDescending(x => DateTime.ParseExact(x.Data, "dd/MM/yyyy", CultureInfo.InvariantCulture));
                            break;
                        default:
                            break;
                    }
                    break;
                case "Campeonato":
                    switch (sortDirection)
                    {
                        case System.Web.UI.WebControls.SortDirection.Ascending:
                            listaJogos = listaJogos.OrderBy(x => x.Campeonato);
                            break;
                        case System.Web.UI.WebControls.SortDirection.Descending:
                            listaJogos = listaJogos.OrderByDescending(x => x.Campeonato);
                            break;
                        default:
                            break;
                    }
                    break;
                case "Adversario":
                    switch (sortDirection)
                    {
                        case System.Web.UI.WebControls.SortDirection.Ascending:
                            listaJogos = listaJogos.OrderBy(x => x.Adversario);
                            break;
                        case System.Web.UI.WebControls.SortDirection.Descending:
                            listaJogos = listaJogos.OrderByDescending(x => x.Adversario);
                            break;
                        default:
                            break;
                    }
                    break;
                case "Resultado":
                    switch (sortDirection)
                    {
                        case System.Web.UI.WebControls.SortDirection.Ascending:
                            listaJogos = listaJogos.OrderBy(x => x.Resultado);
                            break;
                        case System.Web.UI.WebControls.SortDirection.Descending:
                            listaJogos = listaJogos.OrderByDescending(x => x.Resultado);
                            break;
                        default:
                            break;
                    }
                    break;
                case "Pontos":
                    switch (sortDirection)
                    {
                        case System.Web.UI.WebControls.SortDirection.Ascending:
                            listaJogos=listaJogos.OrderBy(x => x.Pontos);
                            break;
                        case System.Web.UI.WebControls.SortDirection.Descending:
                            listaJogos=listaJogos.OrderByDescending(x => x.Pontos);
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }

            GridView1.DataSource = listaJogos.ToList();
            GridView1.DataBind();
            
            GridView4.DataSource = listaJogosDouble;
            GridView4.DataBind();
        }

        private System.Web.UI.WebControls.SortDirection GetSortDirection(string column)
        {
            System.Web.UI.WebControls.SortDirection nextDir = System.Web.UI.WebControls.SortDirection.Ascending; // Default next sort expression behaviour.
            if (ViewState["sort"] != null && ViewState["sort"].ToString() == column)
            {   // Exists... DESC.
                nextDir = System.Web.UI.WebControls.SortDirection.Descending;
                ViewState["sort"] = null;
            }
            else
            {   // Doesn't exists, set ViewState.
                ViewState["sort"] = column;
            }
            return nextDir;
        }

        private IEnumerable<ListGamesDoubles> GetDoubleGamesListQuery()
        {
            List<doublegamesRow> allGames = new List<doublegamesRow>();

            bool isDoubles = playerId >> 16 > 0 ? true : false;
            List<int> playersId = new List<int>();
            int doublePlayerAId = 0;
            int doublePlayerBId = 0;
            if (isDoubles)
            {
                var doublePlayerA = players.Single(x => x.Id == (playerId >> 16));
                var doublePlayerB = players.Single(x => x.Id == (playerId & 0xFF));

                doublePlayerAId = doublePlayerA.Id;
                doublePlayerBId = doublePlayerB.Id;
                playerId = doublePlayerAId;

                playersId.Add(doublePlayerA.Id);
                playersId.Add(doublePlayerB.Id);
            }
            else
            {
                playersId.Add(playerId);
            }

            if (championship.Id != FilesAccess.db.championships.Single(x=>x.Name=="TodosCampeonatos").Id)
            {
                foreach (var playerId in playersId)
                {
                    allGames.AddRange(FilesAccess.db.doubleGames.Where(x => x.IdChampionship == championship.Id &&
                    (x.DoubleA_PlayerAId == playerId || x.DoubleA_PlayerBId == playerId ||
                    x.DoubleB_PlayerAId == playerId || x.DoubleB_PlayerBId == playerId)).ToList());
                }
                allGames = allGames.Distinct().ToList();
            }
            else
                allGames = FilesAccess.db.doubleGames.ToList();
            
            if (playerId > 0)
            {
                if (!isDoubles)
                {
                    allGames = allGames.Where(x => x.DoubleA_PlayerAId == playerId || x.DoubleA_PlayerBId == playerId ||
                        x.DoubleB_PlayerAId == playerId || x.DoubleB_PlayerBId == playerId).ToList();
                }
                else
                {
                    allGames = Utilities.DoublesGamesTogether(allGames, doublePlayerAId, doublePlayerBId);
                }
            }

            var data = from g in allGames
                      // from ph in FilesAccess.db.phase
                       orderby g.Date descending
                       where 
                       (!isDoubles?
                            (((g.DoubleA_PlayerAId == playerId || g.DoubleA_PlayerBId == playerId ||
                    g.DoubleB_PlayerAId == playerId || g.DoubleB_PlayerBId == playerId)) && idAdv == -1 ? true : 
                    (g.DoubleA_PlayerAId == idAdv || g.DoubleA_PlayerBId == idAdv ||
                    g.DoubleB_PlayerAId == idAdv || g.DoubleB_PlayerBId == idAdv))

                       :
                            ((g.DoubleA_PlayerAId == doublePlayerAId && g.DoubleA_PlayerBId == doublePlayerBId) ||
                            (g.DoubleA_PlayerAId == doublePlayerBId && g.DoubleA_PlayerBId == doublePlayerAId) ||
                            (g.DoubleB_PlayerAId == doublePlayerAId && g.DoubleB_PlayerBId == doublePlayerBId) ||
                            (g.DoubleB_PlayerAId == doublePlayerBId && g.DoubleB_PlayerBId == doublePlayerAId))

                        ) &&
                        g.Date >= season.startDate && g.Date <= season.endDate //&& ph.id == g.phaseId
                       select new ListGamesDoubles()
                       {
                           Id = (g.DoubleA_PlayerAId == playerId || g.DoubleA_PlayerBId == playerId) ? g.DoubleA_PlayerAId << 16 | g.DoubleA_PlayerBId : g.DoubleB_PlayerAId << 16 | g.DoubleB_PlayerBId,
                           IdSeason = season.id,
                           Season = season.name,
                           Campeonato = FilesAccess.db.championships.Single(x => x.Id == g.IdChampionship).Name,// + (ph.order != 0 ? " - " + ph.phaseShort : ""),
                           IdAdv = 
                                !isDoubles ?
                                    (g.DoubleA_PlayerAId == playerId || g.DoubleA_PlayerBId == playerId) ? g.DoubleB_PlayerAId <<16 | g.DoubleB_PlayerBId : g.DoubleA_PlayerAId << 16 | g.DoubleA_PlayerBId
                                :
                                    (g.DoubleA_PlayerAId == doublePlayerAId || g.DoubleA_PlayerBId == doublePlayerAId) ? g.DoubleB_PlayerAId << 16 | g.DoubleB_PlayerBId :
                                    g.DoubleA_PlayerAId << 16 | g.DoubleA_PlayerBId
                                ,
                           Data = g.Date.ToString("dd/MM/yyyy"),
                           Par =
                                !isDoubles ?
                                (g.DoubleA_PlayerAId == playerId || g.DoubleA_PlayerBId == playerId) ?
                                    players.SingleOrDefault(x => x.Id == g.DoubleA_PlayerAId)?.Name + "|" + players.SingleOrDefault(x => x.Id == g.DoubleA_PlayerBId)?.Name :
                                    players.SingleOrDefault(x => x.Id == g.DoubleB_PlayerAId)?.Name + "|" + players.SingleOrDefault(x => x.Id == g.DoubleB_PlayerBId)?.Name
                                :
                                players.SingleOrDefault(x => x.Id == doublePlayerAId)?.Name + "|" + players.SingleOrDefault(x => x.Id == doublePlayerBId)?.Name,

                           ParAdversario =
                                !isDoubles ?
                                (g.DoubleA_PlayerAId == playerId || g.DoubleA_PlayerBId == playerId) ? 
                                
                                    players.SingleOrDefault(x => x.Id == g.DoubleB_PlayerAId)?.Name+"|"+ players.SingleOrDefault(x => x.Id == g.DoubleB_PlayerBId)?.Name 
                                    :
                                    players.SingleOrDefault(x => x.Id == g.DoubleA_PlayerAId)?.Name + "|" + players.SingleOrDefault(x => x.Id == g.DoubleA_PlayerBId)?.Name
                                :
                                (g.DoubleA_PlayerAId == doublePlayerAId || g.DoubleA_PlayerBId == doublePlayerAId) ?
                                    players.SingleOrDefault(x => x.Id == g.DoubleB_PlayerAId)?.Name + "|" + players.SingleOrDefault(x => x.Id == g.DoubleB_PlayerBId)?.Name
                                    :
                                    players.SingleOrDefault(x => x.Id == g.DoubleA_PlayerAId)?.Name + "|" + players.SingleOrDefault(x => x.Id == g.DoubleA_PlayerBId)?.Name,

                           Resultado = (g.DoubleA_PlayerAId == playerId || g.DoubleA_PlayerBId == playerId) ? $"{g.SetsA}-{g.SetsB}" : $"{g.SetsB}-{g.SetsA}",
                           Pontos = (g.DoubleA_PlayerAId == playerId || g.DoubleA_PlayerBId == playerId) ? g.PointsDoubleA_PlayerA+g.PointsDoubleA_PlayerB : g.PointsDoubleB_PlayerA + g.PointsDoubleB_PlayerB,
                           IdChamp = g.IdChampionship,
                           graphData = (g.DoubleA_PlayerAId == playerId || g.DoubleA_PlayerBId == playerId) ? g.SetsA - g.SetsB : g.SetsB - g.SetsA,

                           VerHistorico =
                                (g.DoubleA_PlayerAId == playerId || g.DoubleA_PlayerBId == playerId) ?

                                players.SingleOrDefault(x => x.Id == g.DoubleA_PlayerAId)?.Name + "|" + players.SingleOrDefault(x => x.Id == g.DoubleA_PlayerBId)?.Name + " VS " + players.SingleOrDefault(x => x.Id == g.DoubleB_PlayerAId)?.Name + "/" + players.SingleOrDefault(x => x.Id == g.DoubleB_PlayerBId)?.Name :

                                players.SingleOrDefault(x => x.Id == g.DoubleB_PlayerAId)?.Name + "|" + players.SingleOrDefault(x => x.Id == g.DoubleB_PlayerBId)?.Name + " VS "+ players.SingleOrDefault(x => x.Id == g.DoubleA_PlayerAId)?.Name + "/" + players.SingleOrDefault(x => x.Id == g.DoubleA_PlayerBId)?.Name,

                           GameType = 2,
                           DateGame = g.Date,
                           IsWon = (playerId == g.DoubleA_PlayerAId || playerId == g.DoubleA_PlayerBId) 
                                        ? (g.SetsA > g.SetsB ? true : false) : (g.SetsB > g.SetsA ? true : false),
                           IdTeamGame = g.IdTeamGame
                       };
            return data;
        }

        private IEnumerable<ListGames> GetSinglesGamesListQuery()
        {
            List<gamesRow> allGames = new List<gamesRow>();
            if (championship.Id != FilesAccess.db.championships.Single(x => x.Name == "TodosCampeonatos").Id)
                allGames = FilesAccess.db.games.Where(x => x.IdChamp == championship.Id && (x.IdPlayerA == playerGlobal.Id || x.IdPlayerB == playerGlobal.Id)).ToList();
            else
                allGames = FilesAccess.db.games.Where(x => !Utilities.AllOpenTournaments.Contains(x.IdChamp)).ToList();
            
            if (playerId > 0)
                allGames = allGames.Where(x => x.IdPlayerA == playerId || x.IdPlayerB == playerId).ToList();

            if (idAdv > 0)
                allGames = allGames.Where(x => x.IdPlayerA == idAdv || x.IdPlayerB == idAdv).ToList();

            var data = from g in allGames
                       from p in players
                       from ph in FilesAccess.db.phase
                       orderby g.Date descending, ph.order descending
                       where ((g.IdPlayerA == playerGlobal.Id && g.IdPlayerA == p.Id) || (g.IdPlayerB == playerGlobal.Id && g.IdPlayerB == p.Id))
                       && g.Date >= season.startDate
                            && g.Date <= season.endDate && ph.id == g.idPhase
                       select new ListGames()
                       {
                           Nome = playerGlobal.Name,
                           Id = playerId,
                           IdSeason = season.id,
                           Season = FilesAccess.db.seasons.SingleOrDefault(x=>x.startDate < g.Date && x.endDate > g.Date && x.id != Utilities.TodasEpocas.id)?.name,
                           IdChamp = g.IdChamp,
                           Campeonato = FilesAccess.db.championships.Single(x => x.Id == g.IdChamp).Name + (ph.order!=0 ? " - " + ph.phaseShort : ""),
                           IdAdv = g.IdPlayerA == playerGlobal.Id ? g.IdPlayerB : g.IdPlayerA,
                           Data = g.Date.ToString("dd/MM/yyyy"),
                           Adversario = g.IdPlayerA == playerGlobal.Id ? players.SingleOrDefault(x => x.Id == g.IdPlayerB)?.Name : players.SingleOrDefault(x => x.Id == g.IdPlayerA)?.Name,
                           Resultado = g.IdPlayerA == playerGlobal.Id ? $"{g.SetsA}-{g.SetsB}" : $"{g.SetsB}-{g.SetsA}",
                           Pontos = g.IdPlayerA == playerGlobal.Id ? g.pointsPlayerA : g.pointsPlayerB,
                           graphData = g.IdPlayerA == playerGlobal.Id ? g.SetsA - g.SetsB : g.SetsB - g.SetsA,
                           VerHistorico = FilesAccess.db.player.SingleOrDefault(x => x.Id == playerId).Name + " VS " + (g.IdPlayerA == playerGlobal.Id ? players.SingleOrDefault(x => x.Id == g.IdPlayerB)?.Name : players.SingleOrDefault(x => x.Id == g.IdPlayerA)?.Name),
                           GameType = 1,
                           DateGame = g.Date,
                           IsWon = playerId == g.IdPlayerA ? (g.SetsA > g.SetsB ? true : false) : (g.SetsB > g.SetsA ? true : false),
                           IdTeamGame = g.IdTeamGame
                       };
            
            return data;
        }

        protected void seasonFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            MessageBox.Show("test");
            DropDownList list = (DropDownList)sender;
            string value = (string)list.SelectedValue;
            ViewState["SeasonFilter"] = value;
        }
    }
}