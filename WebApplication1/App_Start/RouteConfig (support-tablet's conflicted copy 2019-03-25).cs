using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Routing;
using Microsoft.AspNet.FriendlyUrls;

namespace WebApplication1
{
    public static class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            var settings = new FriendlyUrlSettings();
            settings.AutoRedirectMode = RedirectMode.Permanent;
            routes.EnableFriendlyUrls(settings);
            routes.MapPageRoute("JogadoresBase", "Jogadores", "~/Players.aspx");
            routes.MapPageRoute("JogadoresIdNome", "Jogadores/{Id}-{Nome}", "~/Players.aspx");
            routes.MapPageRoute("JogadoresId", "Jogadores/{Id}", "~/Players.aspx");
            
            routes.MapPageRoute("ClubesBase", "Clubes", "~/Clubes.aspx");
            routes.MapPageRoute("ClubesIdNome", "Clubes/{IdClub}-{Nome}", "~/Clubes.aspx");
            routes.MapPageRoute("ClubesId", "Clubes/{IdClub}", "~/Clubes.aspx");
        }
    }
}
