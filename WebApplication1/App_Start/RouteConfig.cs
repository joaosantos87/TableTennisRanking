using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Routing;
using Microsoft.AspNet.FriendlyUrls;

namespace WebApplication1
{
    public static class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            var settings = new FriendlyUrlSettings();
            settings.AutoRedirectMode = RedirectMode.Permanent;
            routes.EnableFriendlyUrls(settings);
            
            routes.MapPageRoute("jogadorIdNomeEpocaCampeonato", "jogador/{Epoca}/{Campeonato}/{Id}-{Nome}", "~/Players.aspx");
            routes.MapPageRoute("jogadorIdNomeEpoca", "jogador/{Epoca}/{Id}-{Nome}", "~/Players.aspx");
            routes.MapPageRoute("jogadorIdNomeVS", "jogador/{Id}-{Nome}-vs-{IdAdv}-{NomeAdv}", "~/PlayersVs.aspx");
            routes.MapPageRoute("jogadorIdVS", "jogador/{Id}-vs-{IdAdv}", "~/Players.aspx");
            routes.MapPageRoute("jogadorIdNomeNome1", "jogador/{Id}-{Nome}-{Nome1}", "~/Players.aspx");
            routes.MapPageRoute("jogadorIdNome", "jogador/{Id}-{Nome}", "~/Players.aspx");
            routes.MapPageRoute("jogadorId", "jogador/{Id}", "~/Players.aspx");
            routes.MapPageRoute("jogadorBase", "jogador", "~/Players.aspx");
            
            routes.MapPageRoute("clubeIdNome", "clube/{IdClube}-{NomeClube}", "~/Clubes.aspx");
            routes.MapPageRoute("clubeId", "clube/{IdClube}", "~/Clubes.aspx");
            routes.MapPageRoute("clubeBase", "clube", "~/Clubes.aspx");

            routes.MapPageRoute("torneioEpocaIdNome", "torneio/{Epoca}/{Id}-{Nome}", "~/Torneios.aspx");
            routes.MapPageRoute("torneioIdNome", "torneio/{Id}-{Nome}", "~/Torneios.aspx");
            routes.MapPageRoute("torneioEpocaId", "torneio/{Epoca}/{Id}", "~/Torneios.aspx");
            routes.MapPageRoute("torneio", "torneio", "~/Torneios.aspx");
            
            routes.MapPageRoute("torneioIndividualEpocaIdNome", "torneioDetalhe/{Epoca}/{Id}-{Nome}", "~/TorneioIndividual.aspx");
            routes.MapPageRoute("torneioIndividualBase", "TorneioIndividual", "~/Torneios.aspx");

            routes.MapPageRoute("detalhesEpocaCampeonatoId", "detalhes/{Epoca}/{Campeonato}/{Id}-{ClubA}-vs-{ClubB}", "~/ResultadoDetalhe.aspx");
            routes.MapPageRoute("detalhesId", "detalhes/{Id}", "~/ResultadoDetalhe.aspx");

            routes.MapPageRoute("rankingEpocaIdEvento", "ranking/{Epoca}/{IdTorneio}-{NomeTorneio}", "~/Rankings.aspx");
            routes.MapPageRoute("rankingEpoca", "ranking/{Epoca}", "~/Rankings.aspx");
            routes.MapPageRoute("ranking", "ranking", "~/Rankings.aspx");
        }
    }
}
