﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1
{
    public class ListDetailGame
    {
        public int Id { get; set; }
        public long IdPlayerA { get; set; }
        public long IdPlayerB { get; set; }
        public string JogadorA { get; set; }
        public string JogadorB { get; set; }
        public string Resultado { get; set; }

        public int PontosA { get; set; }
        public int PontosB { get; set; }
        public int RankA { get; set; }
        public int RankB { get; set; }

        public string VerHistorico { get; set; }

        public string Season { get; set; }
        public int ChampId { get; set; }
        public string Champ { get; set; }
    }
}