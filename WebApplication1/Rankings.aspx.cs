﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using TableTennis;
using TableTennis.Database;
using static TableTennis.Database.DataSet2;

namespace WebApplication1
{
    public partial class RankingPage : System.Web.UI.Page
    {
        championshipsRow champ;
        seasonRow season;

        DateTime startDate;
        DateTime endDate;
        string search;
        IEnumerable<ListRanking> rankingListGridView = new List<ListRanking>();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                Page.RouteData.Values["Epoca"] = epocaDropDownList.SelectedValue;
                Page.RouteData.Values["IdTorneio"] = FilesAccess.db.championships.Single(x => x.Name == CampeonatoListBox1.SelectedValue).Id;
            }
            if (Page.RouteData.Values.ContainsKey("IdTorneio") && Page.RouteData.Values["IdTorneio"].ToString().Split('-').Count() > 0)
                Page.RouteData.Values["IdTorneio"] = Page.RouteData.Values["IdTorneio"].ToString().Split('-')[0];

            season = Page.RouteData.Values.ContainsKey("Epoca") ? FilesAccess.db.seasons.Single(x => x.name == Page.RouteData.Values["Epoca"].ToString()) : FilesAccess.db.seasons.Single(x => x.name == "TodasEpocas");
            int champId = 0;
            champId = Page.RouteData.Values.ContainsKey("IdTorneio") && Int32.TryParse(Page.RouteData.Values["IdTorneio"].ToString(), out champId) ? champId : FilesAccess.db.championships.Single(x => x.Name == "TodosCampeonatos").Id;
            champ = FilesAccess.db.championships.Single(x => x.Id == champId);
            
            if (!IsPostBack)
            {
                if (CampeonatoListBox1.Items.Count == 0)
                {
                    var champs = from c in FilesAccess.db.championships
                                 from tt in FilesAccess.db.tournamentType
                                 where tt.championship && c.idTournamentType == tt.id
                                 select new
                                 {
                                     c.Name
                                 };

                    foreach (var c in champs)
                        CampeonatoListBox1.Items.Add(c.Name);
                }

                if (epocaDropDownList.Items.Count == 0)
                {
                    foreach (var n in FilesAccess.db.seasons.OrderByDescending(a => a.startDate).Select(x => x.name))
                        epocaDropDownList.Items.Add(n);
                }
                epocaDropDownList.SelectedValue = season.name;
                
                CampeonatoListBox1.SelectedValue = champ.Name;
            }
            
            Stopwatch s1 = Stopwatch.StartNew();
            rankingListGridView = RankingQuery();

            rankingList.DataSource = rankingListGridView;
            rankingList.DataBind();

            s1.Stop();
            Label1.InnerText = s1.ElapsedMilliseconds.ToString();
        }

        protected void FilterButton_Click(object sender, EventArgs e)
        {
            Session["search"] = HttpUtility.UrlDecode(HttpUtility.UrlEncode(searchTextbox.Text.ToLower(), Encoding.GetEncoding("iso-8859-7")));
            var url = String.Format(@"~/ranking/{0}/{1}-{2}",
                        season.name, champ.Id, champ.Name);
            Response.Redirect(url);
        }

        private IEnumerable<ListRanking> RankingQuery()
        {
            startDate = season.startDate;
            endDate = season.endDate;

            search = Session["search"] ==null ? "" : Session["search"].ToString();

            var rank = FilesAccess.db.ranking.Where(x => x.idSeason == season.id && x.idChamp == champ.Id);
            var data = from r in rank
                       from p in FilesAccess.db.player
                       orderby r.points descending
                       where (search != "" ?
                       ((HttpUtility.UrlDecode(HttpUtility.UrlEncode(p.Name.ToLower(), Encoding.GetEncoding("iso-8859-7"))).Contains(search)) ||
                        /*(HttpUtility.UrlDecode(HttpUtility.UrlEncode(FilesAccess.db.clubs.Single(x => x.Id == r.idClub).Name.ToLower(), Encoding.GetEncoding("iso-8859-7"))).Contains(search)) ||*/
                       p.licenseNo.Contains(search)) : true) && p.Id == r.idPlayer
                       select new ListRanking()
                       {
                           IdChamp = r.idChamp,
                           IdSeason = r.idSeason,
                           Season = FilesAccess.db.seasons.Single(x => x.id == r.idSeason).name,
                           Campeonato = FilesAccess.db.championships.Single(x => x.Id == r.idChamp).Name,
                           Id = p.Id,
                           Licenca = p.licenseNo,
                           Ranking = r.rank,
                           Nome = p.Name,
                           Clube = FilesAccess.db.clubs.Single(x=>x.Id == r.idClub).Name,
                           Pontos = r.points,
                           PontosPorJogo = ((r.points-1000)*1.0d / r.numGames).ToString("0.00"),
                           Record = $"{r.numWins}-{r.numLosses}",
                           IdClub = FilesAccess.db.clubs.Single(x => x.Id == p.IdClub).Id,
                           WinPercentage = (1.0d * (r.points)) / r.numGames
                       };
            return data;
        }
        
        protected void rankingList_Sorting(object sender, GridViewSortEventArgs e)
        {
            RankingQuery();
            var sortDirection = GetSortDirection("GridView1" + e.SortExpression);
            switch (e.SortExpression)
            {
                case "Ranking":
                    switch (sortDirection)
                    {
                        case SortDirection.Ascending:
                            rankingListGridView = rankingListGridView.OrderBy(x => x.Ranking).ToList();
                            break;
                        case SortDirection.Descending:
                            rankingListGridView = rankingListGridView.OrderByDescending(x => x.Ranking).ToList();
                            break;
                        default:
                            break;
                    }
                    break;
                case "Licenca":
                    switch (sortDirection)
                    {
                        case SortDirection.Ascending:
                            rankingListGridView = rankingListGridView.OrderBy(x => x.Licenca).ToList();
                            break;
                        case SortDirection.Descending:
                            rankingListGridView = rankingListGridView.OrderByDescending(x => x.Licenca).ToList();
                            break;
                        default:
                            break;
                    }
                    break;
                case "Nome":
                    switch (sortDirection)
                    {
                        case SortDirection.Ascending:
                            rankingListGridView = rankingListGridView.OrderBy(x => x.Nome).ToList();
                            break;
                        case SortDirection.Descending:
                            rankingListGridView = rankingListGridView.OrderByDescending(x => x.Nome).ToList();
                            break;
                        default:
                            break;
                    }
                    break;
                case "Clube":
                    switch (sortDirection)
                    {
                        case SortDirection.Ascending:
                            rankingListGridView = rankingListGridView.OrderBy(x => x.Clube).ToList();
                            break;
                        case SortDirection.Descending:
                            rankingListGridView = rankingListGridView.OrderByDescending(x => x.Clube).ToList();
                            break;
                        default:
                            break;
                    }
                    break;
                case "Pontos":
                    switch (sortDirection)
                    {
                        case SortDirection.Ascending:
                            rankingListGridView = rankingListGridView.OrderByDescending(x => x.Pontos).ToList();
                            break;
                        case SortDirection.Descending:
                            rankingListGridView = rankingListGridView.OrderBy(x => x.Pontos).ToList();
                            break;
                        default:
                            break;
                    }
                    break;
                case "PontosPorJogo":
                    switch (sortDirection)
                    {
                        case SortDirection.Ascending:
                            rankingListGridView = rankingListGridView.OrderByDescending(x => Convert.ToDouble(x.PontosPorJogo)).ToList();
                            break;
                        case SortDirection.Descending:
                            rankingListGridView = rankingListGridView.OrderBy(x => Convert.ToDouble(x.PontosPorJogo)).ToList();
                            break;
                        default:
                            break;
                    }
                    break;
                case "Record":
                    switch (sortDirection)
                    {
                        case SortDirection.Ascending:
                            rankingListGridView = rankingListGridView.OrderByDescending(x => x.WinPercentage).ToList();
                            break;
                        case SortDirection.Descending:
                            rankingListGridView = rankingListGridView.OrderBy(x => x.WinPercentage).ToList();
                            break;
                        default:
                            break;
                    }
                    break;
            }
            rankingList.DataSource = rankingListGridView;
            rankingList.DataBind();
        }

        private SortDirection GetSortDirection(string column)
        {
            System.Web.UI.WebControls.SortDirection nextDir = System.Web.UI.WebControls.SortDirection.Ascending; // Default next sort expression behaviour.
            if (ViewState["sort"] != null && ViewState["sort"].ToString() == column)
            {   // Exists... DESC.
                nextDir = System.Web.UI.WebControls.SortDirection.Descending;
                ViewState["sort"] = null;
            }
            else
            {   // Doesn't exists, set ViewState.
                ViewState["sort"] = column;
            }
            return nextDir;
        }

    }
}