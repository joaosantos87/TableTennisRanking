﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ResultadoDetalhe.aspx.cs" Inherits="WebApplication1.ResultadoDetalhe" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <br>
    <asp:Button ID="Button1" runat="server" Text="Voltar" OnClick="Button1_Click" />
    <h2 id="nomeTorneio" runat="server"></h2>
    <h3 id="jornada" runat="server"> </h3>
    <h2 id="gameName" runat="server"> </h2>
    
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false">
            <Columns>
                <asp:BoundField DataField="Id" HeaderText="Id" Visible="false" InsertVisible="False" ReadOnly="True" SortExpression="Id"/>
                <asp:BoundField DataField="season" Visible="False"/>
                <asp:BoundField DataField="champId" Visible="False"/>
                <asp:BoundField DataField="champ" Visible="False"/>
                <asp:BoundField DataField="IdPlayerA" HeaderText="IdPlayerA" Visible="false" InsertVisible="False" ReadOnly="True" SortExpression="IdPlayerA" />  
                <asp:BoundField DataField="IdPlayerB" HeaderText="IdPlayerB" Visible="false" InsertVisible="False" ReadOnly="True" SortExpression="IdPlayerB" /> 
                <asp:HyperLinkField ItemStyle-HorizontalAlign="Center" DataTextField="RankA" HeaderText="Ranking" SortExpression="RankingA" DataNavigateUrlFields="season, champId,champ"  DataNavigateUrlFormatString="~/ranking/{0}/{1}-{2}"/>
                <asp:HyperLinkField ItemStyle-HorizontalAlign="Center" DataTextField="JogadorA" HeaderText="" SortExpression="JogadorA" DataNavigateUrlFields="IdPlayerA, JogadorA"  DataNavigateUrlFormatString="~/jogador/{0}-{1}"/>
                 <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="PontosA" HeaderText="Pontos Rating" SortExpression="Pontos" />
                <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Resultado" HeaderText="Resultado" SortExpression="Resultado" />
                
                <asp:HyperLinkField ItemStyle-HorizontalAlign="Center" DataTextField="RankB" HeaderText="Ranking" SortExpression="RankingB" DataNavigateUrlFields="season, champId,champ"  DataNavigateUrlFormatString="~/ranking/{0}/{1}-{2}"/>

                <asp:HyperLinkField ItemStyle-HorizontalAlign="Center" DataTextField="JogadorB" HeaderText="" SortExpression="JogadorB" DataNavigateUrlFields="IdPlayerB, JogadorB"  DataNavigateUrlFormatString="~/jogador/{0}-{1}"/>
                <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="PontosB" HeaderText="Pontos Rating" SortExpression="Pontos" />
                <asp:HyperLinkField DataTextField="VerHistorico" HeaderText="Ver Historico" SortExpression="VerHistorico" DataNavigateUrlFields="IdPlayerA,JogadorA, IdPlayerB,JogadorB"  DataNavigateUrlFormatString="~/jogador/{0}-{1}-vs-{2}-{3}"/>
        </Columns>
    </asp:GridView>
</asp:Content>
