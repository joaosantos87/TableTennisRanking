﻿<%@ Page Title="JogadoresVS" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PlayersVs.aspx.cs" Inherits="WebApplication1.PlayersVs" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <br />
    <p>
        &nbsp;
    </p>
    <div class="col-xs-12"> 
    <asp:Button ID="ButtonBack" runat="server" Text="Voltar" OnClick="ButtonBack_Click" />
        </div>
    <div class="col-xs-12"> 
        <div class="col-xs-4"> 
            <h1 ID="jogadorA" runat="server"></h1>
        </div>
         <div class="col-xs-1"> 
              <h1 id="jogosA" runat="server">VS</h1>
         </div>
         <div class="col-xs-2"> 
              <h1>VS</h1>
         </div>
         <div class="col-xs-1"> 
              <h1 id="jogosB" runat="server">VS</h1>
         </div>
        <div class="col-xs-4"> 
            <h1 ID="jogadorB" runat="server"></h1>
        </div>
    </div>
    <div class="col-xs-5">
        <table ID="tableA" runat="server" style="width:100%">
            <tr>
                <td class="auto-style1" style="width: 284px">Licenca</td>
                <td>
                    <asp:Label ID="licensaA" runat="server" Text="Label" Font-Bold="True"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style1" style="width: 284px">Ranking Global</td>
                <td>
                    <asp:Label ID="rankingA" runat="server" Text="Label" Font-Bold="True"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style1" style="width: 284px">Record Global</td>
                <td>
                    <asp:Label ID="recordA" runat="server" Text="Label" Font-Bold="True"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style1" style="width: 284px">Pontos Global</td>
                <td>
                    <asp:Label ID="pontosA" runat="server" Text="Label" Font-Bold="True"></asp:Label>
                </td>
            </tr>
            <tr id="formaA">
                <td class="auto-style1" style="width: 284px">Forma</td>
                <td id="formaATd" runat="server"></td>
            </tr>
        </table>
    </div>
    
    <div class="col-xs-2"></div>
    <div class="col-xs-5">
        <table ID="tableB" runat="server" style="width:100%">
            <tr>
                <td class="auto-style1" style="width: 284px">Licenca</td>
                <td>
                    <asp:Label ID="licensaB" runat="server" Text="Label" Font-Bold="True"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style1" style="width: 284px">Ranking Global</td>
                <td>
                    <asp:Label ID="rankingB" runat="server" Text="Label" Font-Bold="True"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style1" style="width: 284px">Record Global</td>
                <td>
                    <asp:Label ID="recordB" runat="server" Text="Label" Font-Bold="True"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style1" style="width: 284px">Pontos Global</td>
                <td>
                    <asp:Label ID="pontosB" runat="server" Text="Label" Font-Bold="True"></asp:Label>
                </td>
            </tr>
            <tr id="formaB">
                <td class="auto-style1" style="width: 284px">Forma</td>
                <td id="formaBTd" runat="server"></td>
            </tr>
        </table>
    </div>
        
    <asp:Panel ID="Panel3" runat="server">
        <asp:Panel ID="PanelJogosIndividuais" runat="server">
        <h3>Jogos individuais</h3>
        <div class="table-responsive">
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" AllowSorting="true" onsorting="GridView1_Sorting" GridLines="Vertical" CssClass="table">
                <Columns>
                    <asp:BoundField DataField="nome" HeaderText="nome" InsertVisible="False" ReadOnly="True" SortExpression="nome" Visible="false" />                
                    <asp:BoundField DataField="Id" HeaderText="Id" InsertVisible="False" ReadOnly="True" SortExpression="Id" Visible="false" />
                    <asp:BoundField DataField="IdSeason" HeaderText="IdSeason" InsertVisible="False" ReadOnly="True" Visible="false" />
                    <asp:BoundField DataField="Season" HeaderText="Season" InsertVisible="False" ReadOnly="True" Visible="false" />
                    <asp:BoundField DataField="IdChamp" HeaderText="IdChamp" InsertVisible="False" ReadOnly="True" SortExpression="IdChamp" Visible="false" />
                    <asp:BoundField DataField="Data" HeaderText="Data" SortExpression="Data" />
                    <asp:HyperLinkField DataTextField="Campeonato" HeaderText="Campeonato" SortExpression="Campeonato" DataNavigateUrlFields="IdChamp, Campeonato,Season"  DataNavigateUrlFormatString="~/torneio/{2}/{0}-{1}"/>
                    <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Resultado" HeaderText="Resultado" SortExpression="Resultado" />
                </Columns>
            </asp:GridView>
        </div>
    </asp:Panel>
    <asp:Panel ID="jogosParesPanel" runat="server">
        <h3>Jogos Pares</h3>
        <div class="table-responsive">
            <asp:GridView ID="GridView4" runat="server" AutoGenerateColumns="False"  AllowSorting="true" CssClass="table" >
                <Columns>
                    <asp:BoundField DataField="Id" HeaderText="Id" InsertVisible="False" ReadOnly="True" SortExpression="Id" Visible="false" />
                    <asp:BoundField DataField="IdSeason" HeaderText="IdSeason" InsertVisible="False" ReadOnly="True" Visible="false" />
                    <asp:BoundField DataField="Season" HeaderText="Season" InsertVisible="False" ReadOnly="True" Visible="false" />
                    <asp:BoundField DataField="IdAdv" HeaderText="IdAdv" InsertVisible="False" ReadOnly="True" SortExpression="IdAdv" Visible="false" />
                    <asp:BoundField DataField="Data" HeaderText="Data" SortExpression="Data" />
                    <asp:HyperLinkField DataTextField="Campeonato" HeaderText="Campeonato" SortExpression="Campeonato" DataNavigateUrlFields="IdChamp, Season, Campeonato"  DataNavigateUrlFormatString="~/torneio/{1}/{0}-{2}"/>
                    <asp:BoundField DataField="IdChamp" HeaderText="IdChamp" InsertVisible="False" ReadOnly="True" Visible="false" />
                    <asp:HyperLinkField DataTextField="Par" HeaderText="Par" SortExpression="Par" DataNavigateUrlFields="Id, Season, Campeonato, Par"  DataNavigateUrlFormatString="~/jogador/{1}/{2}/{0}-{3}"/>
                    <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Resultado" HeaderText="Resultado" SortExpression="Resultado" />
                    <asp:HyperLinkField DataTextField="ParAdversario" HeaderText="ParAdversario" SortExpression="ParAdversario" DataNavigateUrlFields="IdAdv, Season, Campeonato,ParAdversario"  DataNavigateUrlFormatString="~/jogador/{1}/{2}/{0}-{3}"/>
                    <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Pontos" HeaderText="Pontos" SortExpression="Pontos" />
                    <asp:HyperLinkField DataTextField="VerHistorico" HeaderText="Ver Historico" SortExpression="VerHistorico" DataNavigateUrlFields="Id, Season, Campeonato, IdAdv, Par, ParAdversario"  DataNavigateUrlFormatString="~/Jogador/{0}-{4}-vs-{3}-{5}"/>
                    <asp:BoundField DataField="WinPercentage" HeaderText="WinPercentage" InsertVisible="False" ReadOnly="True" SortExpression="WinPercentage" Visible="false" />
                </Columns>
            </asp:GridView>
        </div>
    </asp:Panel>
</asp:Panel>
</asp:Content>
