﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using TableTennis;
using TableTennis.Database;
using static TableTennis.Database.DataSet2;

namespace WebApplication1
{
    public partial class TorneioIndividual : System.Web.UI.Page
    {
        private int idTorneio = 0;
        private championshipsRow champ;
        private clubsRow clubA;
        private clubsRow clubB;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.RouteData.Values.ContainsKey("Id") && Page.RouteData.Values["Id"].ToString().Split('-').Count() > 0)
                Page.RouteData.Values["Id"] = Page.RouteData.Values["Id"].ToString().Split('-')[0];

            idTorneio = 0;
            idTorneio = Page.RouteData.Values.ContainsKey("Id") && Int32.TryParse(Page.RouteData.Values["Id"].ToString(), out idTorneio) ? idTorneio : FilesAccess.db.championships.Single(x => x.Name == "TodosCampeonatos").Id;
            champ = FilesAccess.db.championships.Single(x => x.Id == idTorneio);

            nomeTorneio.InnerText = champ.Name;
            subNomeTorneio.InnerText = champ.Date.ToString("dd-MM-yyyy");

            var games = FilesAccess.db.games.Where(x => x.IdChamp == idTorneio).OrderBy(x => FilesAccess.db.phase.Single(a => a.id == x.idPhase).order);

            var gamesPerPhase = games.GroupBy(x => FilesAccess.db.phase.Single(a => a.id == x.idPhase).phase);
            var differentPhases = gamesPerPhase.SelectMany(x => x).Select(x => x.idPhase).Distinct();

            List<Tuple<int, int, gamesRow>> gamesToAdd = new List<Tuple<int, int, gamesRow>>();
            foreach (var game in gamesPerPhase)
            {
                var phase = FilesAccess.db.phase.Single(x => x.id == game.First().idPhase);
                if (phase.order < 2)
                    continue;

                var expectedNumGames = phase.numGames;
                if(game.Count() != expectedNumGames)
                {
                    //discover players that are on the next phase, but not on this one
                    var gamesThisPhase = games.Where(x => x.idPhase == phase.id).ToList();
                    var playersThisPhase = gamesThisPhase.Select(x => x.IdPlayerA);
                    playersThisPhase = playersThisPhase.Union(gamesThisPhase.Select(x => x.IdPlayerB));

                    var gamesNextPhase = games.Where(x => x.idPhase == FilesAccess.db.phase.SingleOrDefault(a => a.order == phase.order + 1)?.id);
                    var playersNextPhase = gamesNextPhase.Select(x => x.IdPlayerA);
                    playersNextPhase = playersNextPhase.Union(gamesNextPhase.Select(x => x.IdPlayerB));
                    
                    var byePlayers = playersNextPhase.Except(playersThisPhase);
                    var gameExample = games.First();
                    foreach (var pl in byePlayers)
                    {
                        FilesAccess.db.games.AddgamesRow(
                            0, 0, 0, 0, 0, gameExample.Date, gameExample.IdChamp, gameExample.IdCalculation, 0, 0, gameExample.idPhase, gameExample.IdTeamGame, true);
                        var tmpRow = FilesAccess.db.games.Last();

                        var gm = gamesNextPhase.Single(x => x.IdPlayerA == pl || x.IdPlayerB == pl);

                        var index = gamesNextPhase.ToList().IndexOf(gm) * 2;

                        if (pl == gm.IdPlayerB)
                            index++;
                        
                        tmpRow.IdPlayerA = pl;
                        tmpRow.IdPlayerB = Utilities.ByePlayer;
                        tmpRow.IdPlayerWinner = pl;
                        tmpRow.SetsA = 3;
                        tmpRow.SetsB = 0;
                        gamesToAdd.Add(new Tuple<int, int, gamesRow>(phase.id, index, tmpRow));
                    }
                }
            }

            gamesToAdd = gamesToAdd.OrderBy(x => x.Item1).ThenBy(x => x.Item2).ToList();

            eliminationStage.InnerHtml += $@"<div class='tournament-bracket tournament-bracket--rounded'>";

            foreach (var difPhase in differentPhases)
            {
                var phase = FilesAccess.db.phase.Single(x => x.id == difPhase);
                if (phase.order < 2)
                    continue;
                
                eliminationStage.InnerHtml +=
                           $@"<div class='tournament-bracket__round tournament-bracket__round--{phase.phase}'>
                                <h3 class='tournament-bracket__round-title'>{phase.phase}</h3>
                                <ul class='tournament-bracket__list'>";

                int index = 0;
                var eliminationPhase = gamesPerPhase.SelectMany(x => x).Where(x => x.idPhase == phase.id).ToList();

                foreach (var a in gamesToAdd)
                {
                    if (a.Item1 != phase.id)
                        continue;

                    eliminationPhase.Insert(a.Item2, a.Item3);
                }

                foreach (var game in eliminationPhase)
                {
                    string winnerA = game.SetsA > game.SetsB ? "winner" : "";
                    string winnerB = game.SetsB > game.SetsA ? "winner" : "";

                    var playerA = FilesAccess.db.player.Single(x => x.Id == game.IdPlayerA);
                    var playerB = FilesAccess.db.player.Single(x => x.Id == game.IdPlayerB);

                    var season = FilesAccess.db.seasons.First(x => x.startDate < game.Date && x.endDate > game.Date);

                    string clubAImageUrl = "";
                    string clubBImageUrl = "";
                    if (playerA.Id != Utilities.ByePlayer)
                    {
                        clubA = FilesAccess.db.clubs.Single(a => a.Id == FilesAccess.db.ranking.First(x => x.idPlayer == playerA.Id && x.idSeason == season.id).idClub);
                        var relativePath1 = "/" + clubA.Photo;
                        var absolutePath1 = HttpContext.Current.Server.MapPath(relativePath1);
                        if (File.Exists(absolutePath1))
                            clubAImageUrl = "/"+clubA.Photo;
                        else
                            clubAImageUrl = "/logos/no-image.png";
                    }
                    else
                    {
                        clubA = FilesAccess.db.clubs.Single(x => x.Id == Utilities.ByeClub);
                        clubAImageUrl = "/logos/no-image.png";
                    }

                    if (playerB.Id != Utilities.ByePlayer)
                    {
                        clubB = FilesAccess.db.clubs.Single(a => a.Id == FilesAccess.db.ranking.First(x => x.idPlayer == playerB.Id && x.idSeason == season.id).idClub);

                        var relativePath1 = "/" + clubB.Photo;
                        var absolutePath1 = HttpContext.Current.Server.MapPath(relativePath1);
                        if (File.Exists(absolutePath1))
                            clubBImageUrl = "/" + clubB.Photo;
                        else
                            clubBImageUrl = "/logos/no-image.png";
                    }
                    else
                    {
                        clubB = FilesAccess.db.clubs.Single(x => x.Id == Utilities.ByeClub);
                        clubBImageUrl = "/logos/no-image.png";
                    }

                    eliminationStage.InnerHtml +=
                        $@"<li class='tournament-bracket__item'>
                            <div class='tournament-bracket__match' tabindex={index}'>
                                <table class='tournament-bracket__table'>
                                    <caption class='tournament-bracket__caption'/>
                                       <thead class='sr-only'>
                                        <tr>
                                            <th>Jogador</th>
                                            <th>Sets</th>
                                        </tr>
                                    </thead>
                                    <tbody class='tournament-bracket__content'>
                                        <tr class='tournament-bracket__team tournament-bracket__team--{winnerA}'>
                                            <td class='tournament-bracket__country'>
                                                <abbr class='tournament-bracket__code'>{playerA.Name}</abbr>
                                                <img src = '{clubAImageUrl}' width = '32' height = '32' alt= '{clubA.Name}' Title= '{clubA.Name}'/>
                                            </td>
                                            <td class='tournament-bracket__score'>
                                                <span class='tournament-bracket__number'>{game.SetsA}</span>
                                            </td>
                                        </tr>
                                        <tr class='tournament-bracket__team tournament-bracket__team--{winnerB}'>
                                            <td class='tournament-bracket__country'>
                                                <abbr class='tournament-bracket__code'>{playerB.Name}</abbr>
                                                <img src = '{clubBImageUrl}' width = '32' height = '32' alt= '{clubB.Name}' Title= '{clubB.Name}' />
                                            </td>
                                            <td class='tournament-bracket__score'>
                                                <span class='tournament-bracket__number'>{game.SetsB}</span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </li>";
                    index++;
                }
                eliminationStage.InnerHtml += "</ul></div>";
            }
            eliminationStage.InnerHtml += "</div>";
        }
        
    }
}