﻿<%@ Page Title="Jogadores" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Players.aspx.cs" Inherits="WebApplication1.Players" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <br />
    <p>
        &nbsp;
    </p>
    <p>Loading Time: <Label ID="Label1" runat="server"> test </Label></p>
    <asp:Panel ID="Panel1" runat="server" DefaultButton="Button1">
    Procurar novo jogador:
        <asp:TextBox ID="TextBox1" runat="server" Width="210px"></asp:TextBox>
            <asp:Button ID="Button1" runat="server" Text="Pesquisar" OnClick="Button1_Click" />    
            <asp:GridView ID="GridView2" runat="server" Visible="False" AutoGenerateColumns="false">
                <Columns>
                <asp:BoundField DataField="Id" HeaderText="Id" Visible="false" InsertVisible="False" ReadOnly="True" SortExpression="Id" />      
                <asp:BoundField DataField="IdClub" HeaderText="IdClub" Visible="false" InsertVisible="False" ReadOnly="True" SortExpression="IdClub" />            
                    <asp:BoundField DataField="Licenca" HeaderText="Licenca" SortExpression="licenseNo" ItemStyle-HorizontalAlign="Center" />
                <asp:BoundField DataField="CurrentRank" HeaderText="Ranking" SortExpression="currentRank"  ItemStyle-HorizontalAlign="Center"/>
                <asp:HyperLinkField ItemStyle-HorizontalAlign="Center" DataTextField="Nome" HeaderText="Nome" SortExpression="Name" DataNavigateUrlFields="Id, Nome"  DataNavigateUrlFormatString="~/Jogadores/{0}-{1}"/>
                <asp:HyperLinkField ItemStyle-HorizontalAlign="Center" DataTextField="Club" HeaderText="Clube" SortExpression="Name" DataNavigateUrlFields="IdClub, Club"  DataNavigateUrlFormatString="~/Clubes/{0}-{1}"/>
                <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Record" HeaderText="Record" SortExpression="Record" />
                <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Pontos" HeaderText="Pontos" SortExpression="Pontos" />
                <asp:BoundField Visible="false" DataField="GraphData" />
            </Columns>
            </asp:GridView>
    </asp:Panel>
    <h2 ID="nomeJogador" runat="server"></h2>
    <table ID="list" runat="server" style="width:100%;">
        <tr>
            <td class="auto-style1" style="width: 284px">Licenca</td>
            <td>
                <asp:Label ID="Label10" runat="server" Text="Label" Font-Bold="True"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style1" style="width: 284px">Clube</td>
            <td>
                <asp:Label ID="Label2" runat="server" Text="Label" Font-Bold="True"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style1" style="width: 284px">Ranking Global</td>
            <td>
                <asp:Label ID="Label3" runat="server" Text="Label" Font-Bold="True"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style1" style="width: 284px">Record Global</td>
            <td>
                <asp:Label ID="Label4" runat="server" Text="Label" Font-Bold="True"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style1" style="width: 284px">Pontos Global</td>
            <td>
                <asp:Label ID="Label5" runat="server" Text="Label" Font-Bold="True"></asp:Label>
            </td>
        </tr>
        <tr id="detalhesPesquisa"><td> Pesquisa: </td>
            <td><asp:Label ID="Label9" runat="server" Text="Label" Font-Bold="True"></asp:Label>
            </td>
        </tr>
        <tr id="rankingActual">
            <td class="auto-style1" style="width: 284px">Ranking</td>
            <td>
                <asp:Label ID="Label6" runat="server" Text="Label" Font-Bold="True"></asp:Label>
            </td>
        </tr>
        <tr id="recordActual">
            <td class="auto-style1" style="width: 284px">Record</td>
            <td>
                <asp:Label ID="Label7" runat="server" Text="Label" Font-Bold="True"></asp:Label>
            </td>
        </tr>
        
        <tr id="pontosActual">
            <td class="auto-style1" style="width: 284px">Pontos</td>
            <td>
                <asp:Label ID="Label8" runat="server" Text="Label" Font-Bold="True"></asp:Label>
            </td>
        </tr>
    </table>

    <asp:Panel ID="historicoJogadorPanel" runat="server" Visible="true">
        historico
    </asp:Panel>
    <p>
        &nbsp;
    </p>

    <asp:Panel ID="Panel2" runat="server" DefaultButton="Button2" >
        <h3>Historico contra:</h3>
        <asp:TextBox ID="TextBox2" runat="server" Width="210px"></asp:TextBox>
        <asp:Button ID="Button2" runat="server" Text="Procurar" OnClick="Button2_Click" />    
        <asp:GridView ID="GridView3" runat="server" Visible="False" AutoGenerateColumns="false">
            <Columns>            
                <asp:BoundField DataField="startDateField" HeaderText="startDate" Visible="false" InsertVisible="False" ReadOnly="True" SortExpression="startDate" />
                <asp:BoundField DataField="endDateField" HeaderText="endDate" Visible="false" InsertVisible="False" ReadOnly="True" SortExpression="endDate" />
                <asp:BoundField DataField="campeonatosField" HeaderText="campeonatosField" Visible="false" InsertVisible="False" ReadOnly="True" SortExpression="campeonatosField" />
                <asp:BoundField DataField="Id" HeaderText="Id" Visible="false" InsertVisible="False" ReadOnly="True" SortExpression="Id" />  
                <asp:BoundField DataField="IdAdv" HeaderText="IdAdv" Visible="false" InsertVisible="False" ReadOnly="True" SortExpression="IdAdv" />    
                <asp:BoundField DataField="IdClub" HeaderText="IdClub" Visible="false" InsertVisible="False" ReadOnly="True" SortExpression="IdClub" />            
                    <asp:BoundField DataField="Licenca" HeaderText="Licenca" SortExpression="licenseNo" ItemStyle-HorizontalAlign="Center" />
                <asp:BoundField DataField="CurrentRank" HeaderText="Ranking" SortExpression="currentRank"  ItemStyle-HorizontalAlign="Center"/>
                <asp:HyperLinkField ItemStyle-HorizontalAlign="Center" DataTextField="Nome" HeaderText="Nome" SortExpression="Name" DataNavigateUrlFields="Id,startDateField, endDateField, campeonatosField,IdAdv"  DataNavigateUrlFormatString="~/Players.aspx?ID={0}&startDate={1}&endDate={2}&campeonatos={3}&idAdv={4}"/>
                <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Club" HeaderText="Clube" SortExpression="Name" />
                <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Record" HeaderText="Record" SortExpression="Record" />
                <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Pontos" HeaderText="Pontos" SortExpression="Pontos" />
                <asp:BoundField Visible="false" DataField="GraphData" />
            </Columns>
        </asp:GridView>
    </asp:Panel>
    <asp:Panel ID="Panel3" runat="server">
        <asp:Panel ID="PanelJogosIndividuais" runat="server">
            <h3>Jogos individuais</h3>
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" onrowdatabound="GridView1_RowDataBound" AllowSorting="true" onsorting="GridView1_Sorting" GridLines="Vertical">
            <Columns>
                <asp:BoundField DataField="startDateField" HeaderText="startDate" Visible="false" InsertVisible="False" ReadOnly="True" SortExpression="startDate" />
                <asp:BoundField DataField="endDateField" HeaderText="endDate" Visible="false" InsertVisible="False" ReadOnly="True" SortExpression="endDate" />
                <asp:BoundField DataField="campeonatosField" HeaderText="campeonatosField" Visible="false" InsertVisible="False" ReadOnly="True" SortExpression="campeonatosField" />
                <asp:BoundField DataField="IdChamp" HeaderText="IdChamp" InsertVisible="False" ReadOnly="True" SortExpression="IdChamp" Visible="false" />
                <asp:BoundField DataField="Id" HeaderText="Id" InsertVisible="False" ReadOnly="True" SortExpression="Id" Visible="false" />
                <asp:BoundField DataField="IdAdv" HeaderText="IdAdv" InsertVisible="False" ReadOnly="True" SortExpression="IdAdv" Visible="false" />
                <asp:BoundField DataField="Data" HeaderText="Data" SortExpression="Data" />
                <asp:HyperLinkField DataTextField="Campeonato" HeaderText="Campeonato" SortExpression="Campeonato" DataNavigateUrlFields="IdChamp, startDateField, endDateField"  DataNavigateUrlFormatString="~/Torneios.aspx?ID={0}&startDate={1}&endDate={2}"/>
                <asp:HyperLinkField DataTextField="Adversario" HeaderText="Adversario" SortExpression="Adversario" DataNavigateUrlFields="IdAdv, startDateField, endDateField, campeonatosField"  DataNavigateUrlFormatString="~/Players.aspx?ID={0}&startDate={1}&endDate={2}&campeonatos={3}"/>
                <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Resultado" HeaderText="Resultado" SortExpression="Resultado" />
                <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Pontos" HeaderText="Pontos" SortExpression="Pontos" />
                <asp:HyperLinkField DataTextField="VerHistorico" HeaderText="Ver Historico" SortExpression="VerHistorico" DataNavigateUrlFields="Id, startDateField, endDateField, campeonatosField, IdAdv"  DataNavigateUrlFormatString="~/Players.aspx?ID={0}&startDate={1}&endDate={2}&campeonatos={3}&IdAdv={4}"/>
                <asp:BoundField DataField="WinPercentage" HeaderText="WinPercentage" InsertVisible="False" ReadOnly="True" SortExpression="WinPercentage" Visible="false" />
            </Columns>
        </asp:GridView>
    </asp:Panel>
    <asp:Panel ID="jogosParesPanel" runat="server">
        <h3>Jogos Pares</h3>
        <asp:GridView ID="GridView4" runat="server" AutoGenerateColumns="False" onrowdatabound="GridView4_RowDataBound" AllowSorting="true"  >
            <Columns>
                <asp:BoundField DataField="startDateField" HeaderText="startDate" Visible="false" InsertVisible="False" ReadOnly="True" SortExpression="startDate" />
                <asp:BoundField DataField="endDateField" HeaderText="endDate" Visible="false" InsertVisible="False" ReadOnly="True" SortExpression="endDate" />
                <asp:BoundField DataField="campeonatosField" HeaderText="campeonatosField" Visible="false" InsertVisible="False" ReadOnly="True" SortExpression="campeonatosField" />
                <asp:BoundField DataField="IdChamp" HeaderText="IdChamp" InsertVisible="False" ReadOnly="True" SortExpression="IdChamp" Visible="false" />
                <asp:BoundField DataField="Id" HeaderText="Id" InsertVisible="False" ReadOnly="True" SortExpression="Id" Visible="false" />
                <asp:BoundField DataField="IdAdv" HeaderText="IdAdv" InsertVisible="False" ReadOnly="True" SortExpression="IdAdv" Visible="false" />
                <asp:BoundField DataField="Data" HeaderText="Data" SortExpression="Data" />
                <asp:HyperLinkField DataTextField="Campeonato" HeaderText="Campeonato" SortExpression="Campeonato" DataNavigateUrlFields="IdChamp, startDateField, endDateField"  DataNavigateUrlFormatString="~/Torneios.aspx?ID={0}&startDate={1}&endDate={2}"/>
                <asp:HyperLinkField DataTextField="Par" HeaderText="Par" SortExpression="Par" DataNavigateUrlFields="Id, startDateField, endDateField, campeonatosField"  DataNavigateUrlFormatString="~/Players.aspx?ID={0}&startDate={1}&endDate={2}&campeonatos={3}"/>

                <asp:HyperLinkField DataTextField="ParAdversario" HeaderText="ParAdversario" SortExpression="v" DataNavigateUrlFields="IdAdv, startDateField, endDateField, campeonatosField"  DataNavigateUrlFormatString="~/Players.aspx?ID={0}&startDate={1}&endDate={2}&campeonatos={3}"/>
                <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Resultado" HeaderText="Resultado" SortExpression="Resultado" />
                <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Pontos" HeaderText="Pontos" SortExpression="Pontos" />
                <asp:HyperLinkField DataTextField="VerHistorico" HeaderText="Ver Historico" SortExpression="VerHistorico" DataNavigateUrlFields="Id, startDateField, endDateField, campeonatosField, IdAdv"  DataNavigateUrlFormatString="~/Players.aspx?ID={0}&startDate={1}&endDate={2}&campeonatos={3}&IdAdv={4}"/>
                <asp:BoundField DataField="WinPercentage" HeaderText="WinPercentage" InsertVisible="False" ReadOnly="True" SortExpression="WinPercentage" Visible="false" />
            </Columns>
        </asp:GridView>
    </asp:Panel>
    <asp:Panel ID="resultadosGraficoPanel" runat="server" Visible="false">
        <h3> Resultados</h3>
            <asp:Chart ID="Chart1" runat="server" BackColor="White">
                <ChartAreas>
                    <asp:ChartArea Name="ChartArea1" BackColor="White">
                    </asp:ChartArea>
                </ChartAreas>
                <Legends>
                    <asp:Legend BackColor="White" Name="Legenda" LegendStyle="Column" Alignment="Center" Docking="Right" >
                    </asp:Legend>
            </Legends>
            </asp:Chart>
    </asp:Panel>
</asp:Panel>
</asp:Content>
