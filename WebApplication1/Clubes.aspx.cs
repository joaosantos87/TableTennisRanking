﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using TableTennis;
using TableTennis.Database;
using static TableTennis.Database.DataSet2;

namespace WebApplication1
{
    public partial class Clubes : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Panel1.Visible = false;
            Panel2.Visible = false;

            int idClub = 0;

            //get the routeData from the URL
            idClub = Page.RouteData.Values.ContainsKey("IdClube") ? Int32.TryParse(Page.RouteData.Values["IdClube"] as string, out idClub) ? idClub : 0 : 0;

            if (!IsPostBack)
            {
                //List all the clubs per association
                if (idClub == 0)
                {
                    //var clubes = from c in FilesAccess.db.clubs
                    //           from a in FilesAccess.db.associations
                    //           where c.AssociationId == a.id
                    //           select new
                    //           {
                    //               Id = c.Id,
                    //               AssociacaoId = a.id,
                    //               Nome = c.Name,
                    //               Associacao = a.name,
                    //               NumeroAtletas = FilesAccess.db.player.Count(x => x.IdClub == c.Id),
                    //               NumeroJogos = FilesAccess.db.teamGames.Count(x => x.ClubAId == c.Id || x.ClubBId == c.Id),
                    //               Vitorias = FilesAccess.db.teamGames.Count(x => x.ClubAId == c.Id && x.GetScoreA() > x.GetScoreB() || x.ClubBId == c.Id && x.GetScoreB() > x.GetScoreA()),
                    //               Derrotas = FilesAccess.db.teamGames.Count(x => x.ClubAId == c.Id && x.GetScoreA() < x.GetScoreB() || x.ClubBId == c.Id && x.GetScoreB() < x.GetScoreA())
                    //           };
                    foreach (var a in FilesAccess.db.associations.Where(x => x.isReal == true))
                    {
                        string assocImageUrl = "";

                        var relativePath = "~/" + a.logoUrl;
                        var absolutePath = HttpContext.Current.Server.MapPath(relativePath);
                        if (File.Exists(absolutePath))
                            assocImageUrl = a.logoUrl;
                        else
                            assocImageUrl = "logos/no-image.png";

                        baseClubes.InnerHtml += $"<br><div class='tituloClube'><img src='{assocImageUrl}' alt='{a.name}' style='width: 60px; height: 60px; margin-right: 20px;'/><h2 style='display: inline; margin-bottom:-10px'> {a.name}</h2></div>";
                        foreach (var c in FilesAccess.db.clubs.Where(x => x.AssociationId == a.id))
                        {
                            //check if image exists, if not, load the no-image.png image string assocImageUrl = "";

                            string clubImageUrl = "";
                            var relativePath1 = "~/" + c.Photo;
                            var absolutePath1 = HttpContext.Current.Server.MapPath(relativePath1);
                            if (File.Exists(absolutePath1))
                                clubImageUrl = c.Photo;
                            else
                                clubImageUrl = "logos/no-image.png";

                            baseClubes.InnerHtml += $"<a href='/clube/{c.Id}-{c.Name}'><div class='btn' style='text-align:center; width:160px; height:180px'> <img src='{clubImageUrl}' alt='{c.Name}' style='width: 100px; height: 100px; margin-top:20px; padding-bottom: 10px'/><br> {c.Name} </div></a>";
                        }
                    }
                    Panel1.Visible = true;
                }
                else
                {
                    //display specific club and players
                    Panel2.Visible = true;
                    var clube = FilesAccess.db.clubs.SingleOrDefault(x => x.Id == idClub);
                    string baseUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/";
                    
                    var relativePath1 = clube.Photo;
                    //string baseURL = HttpContext.Current.Request.Url.Host;
                    string clubImageUrl = baseUrl + relativePath1;

                    if (!URLExists(clubImageUrl))
                        clubImageUrl = baseUrl + "/logos/no-image.png";
                    
                    headerClube.InnerHtml += $"<div class='row'>";
                    headerClube.InnerHtml += $@"<img src='{clubImageUrl}' alt='{clube.Name}' style='width: 60px; height: 60px;'/><h2 style='display: inline; margin-bottom:-10px'> {clube.Name}</h2>";
                    headerClube.InnerHtml += $"</div>";

                    var dataJogos =
                                from tg in FilesAccess.db.teamGames
                                where (tg.ClubAId == idClub || tg.ClubBId == idClub)
                                select new
                                {
                                    Id = tg.Id,
                                    IdClub = idClub,
                                    IdClubAdv = tg.ClubAId == idClub ? FilesAccess.db.clubs.SingleOrDefault(x => x.Id == tg.ClubBId).Id : FilesAccess.db.clubs.SingleOrDefault(x => x.Id == tg.ClubAId).Id,
                                    Clube = FilesAccess.db.clubs.SingleOrDefault(x => x.Id == idClub).Name,
                                    ClubeAdv = tg.ClubAId == idClub ? FilesAccess.db.clubs.SingleOrDefault(x => x.Id == tg.ClubBId).Name : FilesAccess.db.clubs.SingleOrDefault(x => x.Id == tg.ClubAId).Name,
                                    IdTorneio = tg.IdChampionship,
                                    Epoca = FilesAccess.db.seasons.Single(x => x.name != "TodasEpocas" && x.startDate < tg.Date && x.endDate > tg.Date).name,
                                    Data = tg.Date.ToString("dd/MM/yyyy"),
                                    Torneio = FilesAccess.db.championships.SingleOrDefault(x => x.Id == tg.IdChampionship).Name,
                                    Jornada = tg.Day,
                                    Adversario =
                                         tg.ClubAId == idClub ?
                                             FilesAccess.db.clubs.SingleOrDefault(x => x.Id == tg.ClubBId).Name + " " + tg.clubBnameAppend :
                                             FilesAccess.db.clubs.SingleOrDefault(x => x.Id == tg.ClubAId).Name + " " + tg.clubAnameAppend,
                                    Resultado = tg.GetResult(idClub),
                                    Equipa = tg.ClubAId == idClub ?
                                         tg.clubAnameAppend == "" ? "\"A\"" : tg.clubAnameAppend :
                                         tg.clubBnameAppend == "" ? "\"A\"" : tg.clubBnameAppend
                               };

                    dataJogos = dataJogos.OrderByDescending(x => DateTime.ParseExact(x.Data, "dd/MM/yyyy", CultureInfo.InvariantCulture));
                    GridView2.DataSource = dataJogos;
                    GridView2.DataBind();
                    
                    List<ListDetailGame> allData = new List<ListDetailGame>();
                    foreach (var tg in dataJogos)
                    {
                        var teamGame = FilesAccess.db.teamGames.SingleOrDefault(x => x.Id == tg.Id);
                        /*nomeTorneio.InnerText = FilesAccess.db.championships.SingleOrDefault(x => x.Id == teamGame.IdChampionship).Name;
                        jornada.InnerText = teamGame.Day != 0 ? "Jornada " + teamGame.Day : "";
                        gameName.InnerText = FilesAccess.db.clubs.SingleOrDefault(x => x.Id == teamGame.ClubAId).Name + " " + teamGame.GetScoreA() + " - " + teamGame.GetScoreB() + " " + FilesAccess.db.clubs.SingleOrDefault(x => x.Id == teamGame.ClubBId).Name;*/

                        var dataSingles = from g in FilesAccess.db.games
                                          where (g.IdTeamGame == teamGame.Id)
                                          select new ListDetailGame()
                                          {
                                              Id = g.id,
                                              IdPlayerA = g.IdPlayerA,
                                              IdPlayerB = g.IdPlayerB,
                                              JogadorA = FilesAccess.db.player.SingleOrDefault(x => x.Id == g.IdPlayerA).Name,
                                              JogadorB = FilesAccess.db.player.SingleOrDefault(x => x.Id == g.IdPlayerB).Name,
                                              Resultado = g.SetsA + " - " + g.SetsB,
                                              PontosA = Utilities.GetRankPoints(FilesAccess.db.ranking.SingleOrDefault(x => x.idChamp == g.IdChamp && x.idPlayer == g.IdPlayerA && x.idSeason == GetSeasonId(g.Date))),
                                              PontosB = Utilities.GetRankPoints(FilesAccess.db.ranking.SingleOrDefault(x => x.idChamp == g.IdChamp && x.idPlayer == g.IdPlayerB && x.idSeason == GetSeasonId(g.Date))),
                                              RankA = Utilities.GetRankPlace(FilesAccess.db.ranking.SingleOrDefault(x => x.idChamp == g.IdChamp && x.idPlayer == g.IdPlayerA && x.idSeason == GetSeasonId(g.Date))),
                                              RankB = Utilities.GetRankPlace(FilesAccess.db.ranking.SingleOrDefault(x => x.idChamp == g.IdChamp && x.idPlayer == g.IdPlayerB && x.idSeason == GetSeasonId(g.Date))),
                                              VerHistorico = FilesAccess.db.player.SingleOrDefault(x => x.Id == g.IdPlayerA).Name + " VS " + FilesAccess.db.player.SingleOrDefault(x => x.Id == g.IdPlayerB).Name
                                          };

                        var dataDoubles = from dg in FilesAccess.db.doubleGames
                                          where (dg.IdTeamGame == teamGame.Id)
                                          select new ListDetailGame()
                                          {
                                              Id = dg.Id,
                                              IdPlayerA = dg.DoubleA_PlayerAId << 16 | dg.DoubleA_PlayerBId,
                                              IdPlayerB = dg.DoubleB_PlayerAId << 16 | dg.DoubleB_PlayerBId,
                                              JogadorA = FilesAccess.db.player.SingleOrDefault(x => x.Id == dg.DoubleA_PlayerAId).Name + "/" + FilesAccess.db.player.SingleOrDefault(x => x.Id == dg.DoubleA_PlayerBId).Name,
                                              JogadorB = FilesAccess.db.player.SingleOrDefault(x => x.Id == dg.DoubleB_PlayerAId).Name + "/" + FilesAccess.db.player.SingleOrDefault(x => x.Id == dg.DoubleB_PlayerBId).Name,
                                              Resultado = dg.SetsA + " - " + dg.SetsB,
                                              PontosA = (Utilities.GetRankPoints(FilesAccess.db.ranking.SingleOrDefault(x => x.idChamp == dg.IdChampionship && x.idPlayer == dg.DoubleA_PlayerAId && x.idSeason == GetSeasonId(dg.Date))) + Utilities.GetRankPoints(FilesAccess.db.ranking.SingleOrDefault(x => x.idChamp == dg.IdChampionship && x.idPlayer == dg.DoubleA_PlayerBId && x.idSeason == GetSeasonId(dg.Date)))) / 2,
                                              PontosB = (Utilities.GetRankPoints(FilesAccess.db.ranking.SingleOrDefault(x => x.idChamp == dg.IdChampionship && x.idPlayer == dg.DoubleB_PlayerAId && x.idSeason == GetSeasonId(dg.Date))) + Utilities.GetRankPoints(FilesAccess.db.ranking.SingleOrDefault(x => x.idChamp == dg.IdChampionship && x.idPlayer == dg.DoubleB_PlayerBId && x.idSeason == GetSeasonId(dg.Date)))) / 2,

                                              RankA = (Utilities.GetRankPlace(FilesAccess.db.ranking.SingleOrDefault(x => x.idChamp == dg.IdChampionship && x.idPlayer == dg.DoubleA_PlayerAId && x.idSeason == GetSeasonId(dg.Date))) + Utilities.GetRankPlace(FilesAccess.db.ranking.SingleOrDefault(x => x.idChamp == dg.IdChampionship && x.idPlayer == dg.DoubleA_PlayerBId && x.idSeason == GetSeasonId(dg.Date)))) / 2,

                                              RankB = (Utilities.GetRankPlace(FilesAccess.db.ranking.SingleOrDefault(x => x.idChamp == dg.IdChampionship && x.idPlayer == dg.DoubleB_PlayerAId && x.idSeason == GetSeasonId(dg.Date))) + Utilities.GetRankPlace(FilesAccess.db.ranking.SingleOrDefault(x => x.idChamp == dg.IdChampionship && x.idPlayer == dg.DoubleB_PlayerBId && x.idSeason == GetSeasonId(dg.Date)))) / 2,
                                              VerHistorico =
                                                FilesAccess.db.player.SingleOrDefault(x => x.Id == dg.DoubleA_PlayerAId).Name + "/" +
                                                FilesAccess.db.player.SingleOrDefault(x => x.Id == dg.DoubleA_PlayerBId).Name + " VS " +
                                                FilesAccess.db.player.SingleOrDefault(x => x.Id == dg.DoubleB_PlayerAId).Name + "/" +
                                                FilesAccess.db.player.SingleOrDefault(x => x.Id == dg.DoubleB_PlayerBId).Name
                                          };

                        var data  = dataDoubles.Union(dataSingles);
                        
                        var h4 = new HtmlGenericControl("h4");
                        h4.InnerText = "\n"+
                            teamGame.Date.ToString("dd/MM/yyyy")+ " - " +
                            FilesAccess.db.clubs.SingleOrDefault(x => x.Id == teamGame.ClubAId).Name + " "+ teamGame.clubAnameAppend +" "+ teamGame.GetScoreA() + " - " + teamGame.GetScoreB() + " " + FilesAccess.db.clubs.SingleOrDefault(x => x.Id == teamGame.ClubBId).Name+ " "+ teamGame.clubBnameAppend +" - "+
                            FilesAccess.db.championships.SingleOrDefault(x => x.Id == teamGame.IdChampionship).Name;

                        var objGV = GenerateGameGridView();
                        
                        objGV.ID = "GV" + h4.InnerText;
                        objGV.Columns[4].HeaderText = FilesAccess.db.clubs.SingleOrDefault(x => x.Id == teamGame.ClubAId).Shortname;
                        objGV.Columns[4].ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                        objGV.Columns[8].HeaderText = FilesAccess.db.clubs.SingleOrDefault(x => x.Id == teamGame.ClubBId).Shortname;

                        objGV.Columns[8].ItemStyle.HorizontalAlign = HorizontalAlign.Center;

                        objGV.DataSource = data;
                        objGV.DataBind();

                        Panel3.Controls.Add(h4);
                        Panel3.Controls.Add(objGV);
                    }
                    Panel3.Visible = true;
                    var dataJogadores =
                               from p in FilesAccess.db.player
                               from r in FilesAccess.db.ranking
                               where 
                                   r.idClub == idClub && 
                                   p.Id == r.idPlayer && 
                                   r.idChamp == Utilities.TodosCampeonatos.Id &&
                                   r.idSeason != Utilities.TodasEpocas.id
                               select new
                               {
                                   Id = p.Id,
                                   Pontos = r.points,
                                   Record = r.numWins + " - " + r.numLosses,
                                   Ranking = r.rank,
                                   Nome = p.Name,
                                   Licenca = p.licenseNo,
                                   DataInicioEpoca = FilesAccess.db.seasons.Single(x => x.id == r.idSeason).startDate,
                                   Epoca = FilesAccess.db.seasons.Single(x=>x.id == r.idSeason).name,
                                   Campeonato = FilesAccess.db.championships.Single(x=>x.Id == r.idChamp).Name,
                                   IdCampeonato = r.idChamp
                               };

                    GridView3.DataSource = dataJogadores.OrderByDescending(x => x.DataInicioEpoca).ThenByDescending(x => x.Pontos);
                    GridView3.DataBind();
                }
            }
        }
        private int GetSeasonId(DateTime date)
        {
            return FilesAccess.db.seasons.Single(x => x.startDate < date && x.endDate > date && x.name != "TodasEpocas").id;
        }
        private GridView GenerateGameGridView()
        {
            GridView objGV = new GridView();
            objGV.AutoGenerateColumns = false;

            BoundField Id = new BoundField();
            Id.Visible = false;
            Id.DataField = "Id";
            Id.SortExpression = "Id";
            objGV.Columns.Add(Id);

            BoundField IdPlayerA = new BoundField();
            IdPlayerA.Visible = false;
            IdPlayerA.DataField = "IdPlayerA";
            IdPlayerA.SortExpression = "IdPlayerA";
            objGV.Columns.Add(IdPlayerA);

            BoundField IdPlayerB = new BoundField();
            IdPlayerB.Visible = false;
            IdPlayerB.DataField = "IdPlayerB";
            IdPlayerB.SortExpression = "IdPlayerB";
            objGV.Columns.Add(IdPlayerB);

            BoundField rankA = new BoundField();
            rankA.HeaderText = "Ranking Actual";
            rankA.DataField = "RankA";
            rankA.SortExpression = "RankA";
            rankA.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
            objGV.Columns.Add(rankA);

            HyperLinkField jogadorA = new HyperLinkField();
            jogadorA.HeaderText = "";
            jogadorA.DataTextField = "JogadorA";
            jogadorA.DataNavigateUrlFields = new string[] { "IdPlayerA", "JogadorA" };
            jogadorA.DataNavigateUrlFormatString = "~/jogador/{0}-{1}";
            jogadorA.SortExpression = "JogadorA";
            jogadorA.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
            objGV.Columns.Add(jogadorA);
            
            BoundField PontosA = new BoundField();
            PontosA.HeaderText = "Pontos Rating";
            PontosA.DataField = "PontosA";
            PontosA.SortExpression = "PontosA";
            PontosA.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
            objGV.Columns.Add(PontosA);

            BoundField Resultado = new BoundField();
            Resultado.HeaderText = "Resultado";
            Resultado.DataField = "Resultado";
            Resultado.SortExpression = "Resultado";
            Resultado.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
            objGV.Columns.Add(Resultado);

            BoundField rankB = new BoundField();
            rankB.HeaderText = "Ranking Actual";
            rankB.DataField = "RankB";
            rankB.SortExpression = "RankB";
            rankB.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
            objGV.Columns.Add(rankB);

            HyperLinkField jogadorB = new HyperLinkField();
            jogadorB.HeaderText = "";
            jogadorB.DataTextField = "JogadorB";
            jogadorB.DataNavigateUrlFields = new string[] { "IdPlayerB", "JogadorB" };
            jogadorB.DataNavigateUrlFormatString = "~/jogador/{0}-{1}";
            jogadorB.SortExpression = "JogadorB";
            jogadorB.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
            objGV.Columns.Add(jogadorB);
            
            BoundField PontosB = new BoundField();
            PontosB.HeaderText = "Pontos Rating";
            PontosB.DataField = "PontosB";
            PontosB.SortExpression = "PontosB";
            PontosB.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
            objGV.Columns.Add(PontosB);

            HyperLinkField Historico = new HyperLinkField();
            Historico.HeaderText = "Ver Historico";
            Historico.DataTextField = "VerHistorico";
            Historico.DataNavigateUrlFields = new string[] { "IdPlayerA","JogadorA", "IdPlayerB", "JogadorB" };
            Historico.DataNavigateUrlFormatString = "~/jogador/{0}-{1}-vs-{2}-{3}";
            Historico.SortExpression = "VerHistorico";
            Historico.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
            objGV.Columns.Add(Historico);

            return objGV;
        }
        
        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Clube");
        }

        public bool URLExists(string url)
        {
            System.Net.WebRequest webRequest = System.Net.WebRequest.Create(url);
            webRequest.Method = "HEAD";
            try
            {
                using (System.Net.HttpWebResponse response = (System.Net.HttpWebResponse)webRequest.GetResponse())
                {
                    if (response.StatusCode.ToString() == "OK")
                    {
                        return true;
                    }
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }
    }
}