﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Clubes.aspx.cs" Inherits="WebApplication1.Clubes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <br>
    <asp:Panel ID="Panel1" runat="server" Visible="false">
        Associacao: <asp:DropDownList ID="DropDownList1" runat="server" Height="30px" Width="449px" AutoPostBack="true"></asp:DropDownList>
        <br>
        <br>
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" >
            <Columns>
                <asp:BoundField DataField="Id" HeaderText="Id" Visible="false" InsertVisible="False" ReadOnly="True" SortExpression="Id" />
                <asp:HyperLinkField ItemStyle-HorizontalAlign="Center" DataTextField="Nome" HeaderText="Nome" SortExpression="Nome" DataNavigateUrlFields="Id"  DataNavigateUrlFormatString="~/Clubes.aspx?Id={0}"/>
                <asp:BoundField  DataField="AssociacaoId" HeaderText="AssociacaoId" SortExpression="AssociacaoId" Visible="false" />
                <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="NumeroAtletas" HeaderText="Atletas" SortExpression="NumeroAtletas" />
                <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="NumeroJogos" HeaderText="Jogos" SortExpression="NumeroJogos" />
                <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Vitorias" HeaderText="Vitorias" SortExpression="Vitorias" />
                <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Derrotas" HeaderText="Derrotas" SortExpression="Derrotas" />
                </Columns>
        </asp:GridView>
    </asp:Panel>
    <asp:Panel ID="Panel2" runat="server" Visible="false">
        <asp:Button ID="Button1" runat="server" Text="Todos os clubes" OnClick="Button1_Click" />
         <h2 id="nomeClube" runat="server"> &nbsp;</h2>
        
         <h3>Jogos</h3>
        <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="false">
            <Columns>
                <asp:BoundField DataField="Id" HeaderText="Id" SortExpression="Id" Visible="false" />
                <asp:BoundField DataField="IdClub" HeaderText="IdClub" SortExpression="IdClub" Visible="false" />
                <asp:BoundField DataField="IdTorneio" HeaderText="IdTorneio" SortExpression="IdTorneio" Visible="false" />
                <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Data" HeaderText="Data" SortExpression="Data" /> 
                <asp:HyperLinkField ItemStyle-HorizontalAlign="Center" DataTextField="Torneio" HeaderText="Torneio" SortExpression="Torneio" DataNavigateUrlFields="IdTorneio"  DataNavigateUrlFormatString="~/Torneios.aspx?Id={0}"/> 
                <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Jornada" HeaderText="Jornada" SortExpression="Jornada" />
                <asp:HyperLinkField ItemStyle-HorizontalAlign="Center" DataTextField="Adversario" HeaderText="Adversario" SortExpression="Adversario" DataNavigateUrlFields="IdClub"  DataNavigateUrlFormatString="~/Clubes.aspx?Id={0}"/>
                <asp:HyperLinkField ItemStyle-HorizontalAlign="Center" DataTextField="Resultado" HeaderText="Resultado" SortExpression="Resultado" DataNavigateUrlFields="Id"  DataNavigateUrlFormatString="~/ResultadoDetalhe.aspx?Id={0}"/>
            </Columns>
        </asp:GridView>
        <asp:Panel ID="Panel3" runat="server">
                
        </asp:Panel>

         <h3>Jogadores</h3>
        <asp:GridView ID="GridView3" runat="server" AutoGenerateColumns="False" >
            <Columns>
                <asp:BoundField DataField="Id" HeaderText="Id" InsertVisible="False" ReadOnly="True" SortExpression="Id" Visible="false" />
                <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Ranking" HeaderText="Ranking" SortExpression="Ranking" />
                <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Licenca" HeaderText="Licenca" SortExpression="Licenca" />
                <asp:HyperLinkField DataTextField="Nome" HeaderText="Nome" SortExpression="Nome" DataNavigateUrlFields="Id"  DataNavigateUrlFormatString="~/Players.aspx?ID={0}"/>
                <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Pontos" HeaderText="Pontos Rating" SortExpression="Pontos" />
                <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Record" HeaderText="Record" SortExpression="Record" />
            </Columns>
        </asp:GridView>
    </asp:Panel>
</asp:Content>
