﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TableTennis;
using TableTennis.Database;

namespace WebApplication1
{
    public partial class ResultadoDetalhe : System.Web.UI.Page
    {
        int idTeamGame;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
                ViewState["RefUrl"] = Request.UrlReferrer.ToString();

            idTeamGame = Request.QueryString["Id"] != "" ? Convert.ToInt32(Request.QueryString["Id"]) : -1;

            if (idTeamGame > 0)
            {
                var teamGame = FilesAccess.db.teamGames.SingleOrDefault(x => x.Id == idTeamGame);
                nomeTorneio.InnerText = FilesAccess.db.championships.SingleOrDefault(x => x.Id == teamGame.IdChampionship).Name;
                jornada.InnerText = teamGame.Day != 0 ? "Jornada " + teamGame.Day : "";
                gameName.InnerText = FilesAccess.db.clubs.SingleOrDefault(x => x.Id == teamGame.ClubAId).Name + " " + teamGame.GetScoreA() + " - " + teamGame.GetScoreB() + " " + FilesAccess.db.clubs.SingleOrDefault(x => x.Id == teamGame.ClubBId).Name;
                
                var dataSingles = from g in FilesAccess.db.games
                           where (g.IdTeamGame == teamGame.Id)
                           select new ListDetailGame()
                           {
                               Id = g.id,
                               IdPlayerA = g.IdPlayerA,
                               IdPlayerB = g.IdPlayerB,
                               JogadorA = FilesAccess.db.player.SingleOrDefault(x => x.Id == g.IdPlayerA).Name,
                               JogadorB = FilesAccess.db.player.SingleOrDefault(x => x.Id == g.IdPlayerB).Name,
                               Resultado = g.SetsA + " - " + g.SetsB,
                               PontosA = g.pointsPlayerA,
                               PontosB = g.pointsPlayerB,
                               RankA = FilesAccess.db.player.SingleOrDefault(x => x.Id == g.IdPlayerA).currentRank,
                               RankB = FilesAccess.db.player.SingleOrDefault(x => x.Id == g.IdPlayerB).currentRank,
                               VerHistorico = FilesAccess.db.player.SingleOrDefault(x => x.Id == g.IdPlayerA).Name + " VS "+FilesAccess.db.player.SingleOrDefault(x => x.Id == g.IdPlayerB).Name
                           };

                var dataDoubles = from dg in FilesAccess.db.doubleGames
                                  where (dg.IdTeamGame == teamGame.Id)
                                  select new ListDetailGame()
                                  {
                                      Id = dg.Id,
                                      IdPlayerA = dg.DoubleA_PlayerAId <<16 | dg.DoubleA_PlayerBId,
                                      IdPlayerB = dg.DoubleB_PlayerAId << 16 | dg.DoubleB_PlayerBId,
                                      JogadorA = FilesAccess.db.player.SingleOrDefault(x => x.Id == dg.DoubleA_PlayerAId).Name+"/"+ FilesAccess.db.player.SingleOrDefault(x => x.Id == dg.DoubleA_PlayerBId).Name,
                                      JogadorB = FilesAccess.db.player.SingleOrDefault(x => x.Id == dg.DoubleB_PlayerAId).Name + "/" + FilesAccess.db.player.SingleOrDefault(x => x.Id == dg.DoubleB_PlayerBId).Name,
                                      Resultado = dg.SetsA + " - " + dg.SetsB,
                                  };

                GridView1.DataSource = dataDoubles.Union(dataSingles);
                GridView1.DataBind();
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            object refUrl = ViewState["RefUrl"];
            if (refUrl != null)
                Response.Redirect((string)refUrl);
        }
    }
}