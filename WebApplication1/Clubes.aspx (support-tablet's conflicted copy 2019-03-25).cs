﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using TableTennis;
using TableTennis.Database;
using static TableTennis.Database.DataSet2;

namespace WebApplication1
{
    public partial class Clubes : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Panel1.Visible = false;
            Panel2.Visible = false;

            int idClub = Page.RouteData.Values.ContainsKey("IdClub") ? Convert.ToInt32(Page.RouteData.Values["IdClub"].ToString()) : 0;
            MessageBox.Show(idClub.ToString());
            MessageBox.Show(Page.RouteData.Values.ContainsKey("Nome").ToString());
            if (!IsPostBack)
            {
                for(int i=0; i < FilesAccess.db.associations.Count;i++)
                    DropDownList1.Items.Add(FilesAccess.db.associations[i].name);

                if (idClub == 0)
                {
                    var data = from c in FilesAccess.db.clubs
                               from a in FilesAccess.db.associations
                               where c.AssociationId == a.id
                               select new
                               {
                                   Id = c.Id,
                                   AssociacaoId = a.id,
                                   Nome = c.Name,
                                   Associacao = a.name,
                                   NumeroAtletas = FilesAccess.db.player.Count(x => x.IdClub == c.Id),
                                   NumeroJogos = FilesAccess.db.teamGames.Count(x => x.ClubAId == c.Id || x.ClubBId == c.Id),
                                   Vitorias = FilesAccess.db.teamGames.Count(x => x.ClubAId == c.Id && x.GetScoreA() > x.GetScoreB() || x.ClubBId == c.Id && x.GetScoreB() > x.GetScoreA()),
                                   Derrotas = FilesAccess.db.teamGames.Count(x => x.ClubAId == c.Id && x.GetScoreA() < x.GetScoreB() || x.ClubBId == c.Id && x.GetScoreB() < x.GetScoreA())
                               };

                    GridView1.DataSource = data.OrderByDescending(x => x.NumeroAtletas);
                    GridView1.DataBind();
                    Panel1.Visible = true;
                }
                else
                {
                    nomeClube.InnerHtml = FilesAccess.db.clubs.SingleOrDefault(x => x.Id == idClub).Name;
                    var dataJogos = 
                                from tg in FilesAccess.db.teamGames
                               where  (tg.ClubAId == idClub || tg.ClubBId == idClub)
                               select new
                               {
                                   Id = tg.Id,
                                   IdClub = tg.ClubAId == idClub ? FilesAccess.db.clubs.SingleOrDefault(x => x.Id == tg.ClubBId).Id : FilesAccess.db.clubs.SingleOrDefault(x => x.Id == tg.ClubAId).Id,
                                   IdTorneio = tg.IdChampionship,
                                   Data = tg.Date.ToString("dd/MM/yyyy"),
                                   Torneio = FilesAccess.db.championships.SingleOrDefault(x => x.Id == tg.IdChampionship).Name,
                                   Jornada = tg.Day,
                                   Adversario = 
                                        tg.ClubAId == idClub?
                                            FilesAccess.db.clubs.SingleOrDefault(x => x.Id == tg.ClubBId).Name + " "+tg.clubAnameAppend  : 
                                            FilesAccess.db.clubs.SingleOrDefault(x => x.Id == tg.ClubAId).Name + " " + tg.clubBnameAppend,
                                   Resultado = tg.GetResult()
                               };
                    dataJogos = dataJogos.OrderByDescending(x => DateTime.ParseExact(x.Data, "dd/MM/yyyy", CultureInfo.InvariantCulture));
                    GridView2.DataSource = dataJogos;
                GridView2.DataBind();
                    
                    List<ListDetailGame> allData = new List<ListDetailGame>();
                    foreach (var tg in dataJogos)
                    {
                        var teamGame = FilesAccess.db.teamGames.SingleOrDefault(x => x.Id == tg.Id);
                        /*nomeTorneio.InnerText = FilesAccess.db.championships.SingleOrDefault(x => x.Id == teamGame.IdChampionship).Name;
                        jornada.InnerText = teamGame.Day != 0 ? "Jornada " + teamGame.Day : "";
                        gameName.InnerText = FilesAccess.db.clubs.SingleOrDefault(x => x.Id == teamGame.ClubAId).Name + " " + teamGame.GetScoreA() + " - " + teamGame.GetScoreB() + " " + FilesAccess.db.clubs.SingleOrDefault(x => x.Id == teamGame.ClubBId).Name;*/

                        var dataSingles = from g in FilesAccess.db.games
                                          where (g.IdTeamGame == teamGame.Id)
                                          select new ListDetailGame()
                                          {
                                              Id = g.id,
                                              IdPlayerA = g.IdPlayerA,
                                              IdPlayerB = g.IdPlayerB,
                                              JogadorA = FilesAccess.db.player.SingleOrDefault(x => x.Id == g.IdPlayerA).Name,
                                              JogadorB = FilesAccess.db.player.SingleOrDefault(x => x.Id == g.IdPlayerB).Name,
                                              Resultado = g.SetsA + " - " + g.SetsB,
                                              PontosA = g.pointsPlayerA,
                                              PontosB = g.pointsPlayerB,
                                              RankA = FilesAccess.db.player.SingleOrDefault(x => x.Id == g.IdPlayerA).currentRank,
                                              RankB = FilesAccess.db.player.SingleOrDefault(x => x.Id == g.IdPlayerB).currentRank,
                                              VerHistorico = FilesAccess.db.player.SingleOrDefault(x => x.Id == g.IdPlayerA).Name + " VS " + FilesAccess.db.player.SingleOrDefault(x => x.Id == g.IdPlayerB).Name
                                          };

                        var dataDoubles = from dg in FilesAccess.db.doubleGames
                                          where (dg.IdTeamGame == teamGame.Id)
                                          select new ListDetailGame()
                                          {
                                              Id = dg.Id,
                                              IdPlayerA = dg.DoubleA_PlayerAId << 16 | dg.DoubleA_PlayerBId,
                                              IdPlayerB = dg.DoubleB_PlayerAId << 16 | dg.DoubleB_PlayerBId,
                                              JogadorA = FilesAccess.db.player.SingleOrDefault(x => x.Id == dg.DoubleA_PlayerAId).Name + "/" + FilesAccess.db.player.SingleOrDefault(x => x.Id == dg.DoubleA_PlayerBId).Name,
                                              JogadorB = FilesAccess.db.player.SingleOrDefault(x => x.Id == dg.DoubleB_PlayerAId).Name + "/" + FilesAccess.db.player.SingleOrDefault(x => x.Id == dg.DoubleB_PlayerBId).Name,
                                              Resultado = dg.SetsA + " - " + dg.SetsB,
                                              RankA = (FilesAccess.db.player.SingleOrDefault(x => x.Id == dg.DoubleA_PlayerAId).currentRank + FilesAccess.db.player.SingleOrDefault(x => x.Id == dg.DoubleA_PlayerBId).currentRank) / 2,
                                              RankB = (FilesAccess.db.player.SingleOrDefault(x => x.Id == dg.DoubleB_PlayerAId).currentRank + FilesAccess.db.player.SingleOrDefault(x => x.Id == dg.DoubleB_PlayerBId).currentRank) / 2,
                                              PontosA = (dg.PointsDoubleA_PlayerA + dg.PointsDoubleA_PlayerB) / 2,
                                              PontosB = (dg.PointsDoubleB_PlayerA + dg.PointsDoubleB_PlayerB) / 2,
                                              VerHistorico = 
                                                FilesAccess.db.player.SingleOrDefault(x => x.Id == dg.DoubleA_PlayerAId).Name + "/" +
                                                FilesAccess.db.player.SingleOrDefault(x => x.Id == dg.DoubleA_PlayerBId).Name + " VS " +
                                                FilesAccess.db.player.SingleOrDefault(x => x.Id == dg.DoubleB_PlayerAId).Name + "/" +
                                                FilesAccess.db.player.SingleOrDefault(x => x.Id == dg.DoubleB_PlayerBId).Name
                                          };

                        var data  = dataDoubles.Union(dataSingles);
                        
                        var h4 = new HtmlGenericControl("h4");
                        h4.InnerText = "\n"+
                            teamGame.Date.ToString("dd/MM/yyyy")+ " - " +
                            FilesAccess.db.clubs.SingleOrDefault(x => x.Id == teamGame.ClubAId).Name + " "+ teamGame.clubAnameAppend +" "+ teamGame.GetScoreA() + " - " + teamGame.GetScoreB() + " " + FilesAccess.db.clubs.SingleOrDefault(x => x.Id == teamGame.ClubBId).Name+ " "+ teamGame.clubBnameAppend +" - "+
                            FilesAccess.db.championships.SingleOrDefault(x => x.Id == teamGame.IdChampionship).Name;

                        var objGV = GenerateGameGridView();
                        
                        objGV.ID = "GV" + h4.InnerText;
                        objGV.Columns[4].HeaderText = FilesAccess.db.clubs.SingleOrDefault(x => x.Id == teamGame.ClubAId).Shortname;
                        objGV.Columns[4].ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                        objGV.Columns[8].HeaderText = FilesAccess.db.clubs.SingleOrDefault(x => x.Id == teamGame.ClubBId).Shortname;

                        objGV.Columns[8].ItemStyle.HorizontalAlign = HorizontalAlign.Center;

                        objGV.DataSource = data;
                        objGV.DataBind();

                        Panel3.Controls.Add(h4);
                        Panel3.Controls.Add(objGV);
                    }
                    Panel3.Visible = true;
                    var dataJogadores =
                               from p in FilesAccess.db.player
                               where p.IdClub == idClub
                               select new
                               {
                                   Id = p.Id,
                                   Pontos = p.currentPoints,
                                   Record = p.currentGamesWon + " - " + p.currentGamesLost,
                                   Ranking = p.currentRank,
                                   Nome = p.Name,
                                   Licenca = p.licenseNo
                               };

                    GridView3.DataSource = dataJogadores.OrderBy(x => x.Ranking);
                    GridView3.DataBind();

                    Panel2.Visible = true;
                }
            }
        }

        private GridView GenerateGameGridView()
        {
            GridView objGV = new GridView();
            objGV.AutoGenerateColumns = false;

            BoundField Id = new BoundField();
            Id.Visible = false;
            Id.DataField = "Id";
            Id.SortExpression = "Id";
            objGV.Columns.Add(Id);

            BoundField IdPlayerA = new BoundField();
            IdPlayerA.Visible = false;
            IdPlayerA.DataField = "IdPlayerA";
            IdPlayerA.SortExpression = "IdPlayerA";
            objGV.Columns.Add(IdPlayerA);

            BoundField IdPlayerB = new BoundField();
            IdPlayerB.Visible = false;
            IdPlayerB.DataField = "IdPlayerB";
            IdPlayerB.SortExpression = "IdPlayerB";
            objGV.Columns.Add(IdPlayerB);

            BoundField rankA = new BoundField();
            rankA.HeaderText = "Ranking Actual";
            rankA.DataField = "RankA";
            rankA.SortExpression = "RankA";
            rankA.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
            objGV.Columns.Add(rankA);

            HyperLinkField jogadorA = new HyperLinkField();
            jogadorA.HeaderText = "";
            jogadorA.DataTextField = "JogadorA";
            jogadorA.DataNavigateUrlFields = new string[] { "IdPlayerA" };
            jogadorA.DataNavigateUrlFormatString = "~/Players.aspx?Id={0}";
            jogadorA.SortExpression = "JogadorA";
            jogadorA.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
            objGV.Columns.Add(jogadorA);
            
            BoundField PontosA = new BoundField();
            PontosA.HeaderText = "Pontos Rating";
            PontosA.DataField = "PontosA";
            PontosA.SortExpression = "PontosA";
            PontosA.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
            objGV.Columns.Add(PontosA);

            BoundField Resultado = new BoundField();
            Resultado.HeaderText = "Resultado";
            Resultado.DataField = "Resultado";
            Resultado.SortExpression = "Resultado";
            Resultado.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
            objGV.Columns.Add(Resultado);

            BoundField rankB = new BoundField();
            rankB.HeaderText = "Ranking Actual";
            rankB.DataField = "RankB";
            rankB.SortExpression = "RankB";
            rankB.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
            objGV.Columns.Add(rankB);

            HyperLinkField jogadorB = new HyperLinkField();
            jogadorB.HeaderText = "";
            jogadorB.DataTextField = "JogadorB";
            jogadorB.DataNavigateUrlFields = new string[] { "IdPlayerB" };
            jogadorB.DataNavigateUrlFormatString = "~/Players.aspx?Id={0}";
            jogadorB.SortExpression = "JogadorB";
            jogadorB.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
            objGV.Columns.Add(jogadorB);
            
            BoundField PontosB = new BoundField();
            PontosB.HeaderText = "Pontos Rating";
            PontosB.DataField = "PontosB";
            PontosB.SortExpression = "PontosB";
            PontosB.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
            objGV.Columns.Add(PontosB);

            HyperLinkField Historico = new HyperLinkField();
            Historico.HeaderText = "Ver Historico";
            Historico.DataTextField = "VerHistorico";
            Historico.DataNavigateUrlFields = new string[] { "IdPlayerA", "IdPlayerB" };
            Historico.DataNavigateUrlFormatString = "~/Players.aspx?ID={0}&IdAdv={1}";
            Historico.SortExpression = "VerHistorico";
            Historico.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
            objGV.Columns.Add(Historico);

            return objGV;
        }
        
        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Clubes.aspx");
        }
    }
}