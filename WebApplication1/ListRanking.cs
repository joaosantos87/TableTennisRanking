﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1
{
    public class ListRanking
    {
        public int Id { get; set; }
        public int IdClub { get; set; }
        public int Ranking { get; set; }
        public string Licenca { get; set; }
        public string Nome { get; set; }
        public string Clube { get; set; }
        public int Pontos { get; set; }
        public string PontosPorJogo { get; set; }
        public string Record { get; set; }
        public double WinPercentage { get; set; }
        public int IdSeason { get; set; }
        public string Season { get; set; }
        public int IdChamp { get; set; }
        public string Campeonato { get; set; }
        public int Won { get; set; }
        public int Lost { get; set; }
        public DateTime EndSeasonDate { get; internal set; }
    }
}