﻿using Microsoft.Ajax.Utilities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using TableTennis;
using TableTennis.Database;
using static TableTennis.Database.DataSet2;

namespace WebApplication1
{
    public partial class Torneios : System.Web.UI.Page
    {
        private int idTorneio = 0;
        private seasonRow season;
        private championshipsRow champ;
        private IEnumerable<ListRanking> rankingListGridView = new List<ListRanking>();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.RouteData.Values.ContainsKey("Id") && Page.RouteData.Values["Id"].ToString().Split('-').Count() > 0)
                Page.RouteData.Values["Id"] = Page.RouteData.Values["Id"].ToString().Split('-')[0];

            if (IsPostBack)
            {
                Page.RouteData.Values["Epoca"] = epocaDropDownList.SelectedValue;
                Page.RouteData.Values["Id"] = FilesAccess.db.championships.Single(x => x.Name == CampeonatoListBox1.SelectedValue).Id;
            }
            season = Page.RouteData.Values.ContainsKey("Epoca") ? FilesAccess.db.seasons.Single(x => x.name == Page.RouteData.Values["Epoca"].ToString()) : Utilities.EpocaActual;

            idTorneio = 0;
            idTorneio = Page.RouteData.Values.ContainsKey("Id") && Int32.TryParse(Page.RouteData.Values["Id"].ToString(), out idTorneio) ? idTorneio : FilesAccess.db.championships.Single(x => x.Name == "TodosCampeonatos").Id;
            champ = FilesAccess.db.championships.Single(x => x.Id == idTorneio);
            
            if (!IsPostBack)
            {
                if (CampeonatoListBox1.Items.Count == 0)
                {
                    var champs = from c in FilesAccess.db.championships
                                 from tt in FilesAccess.db.tournamentType
                                 where tt.championship && c.idTournamentType == tt.id
                                 select new
                                 {
                                     c.Name
                                 };

                    foreach (var c in champs)
                        CampeonatoListBox1.Items.Add(c.Name);
                }

                if (epocaDropDownList.Items.Count == 0)
                {
                    foreach (var n in FilesAccess.db.seasons.OrderByDescending(a => a.startDate).Select(x => x.name))
                        epocaDropDownList.Items.Add(n);
                }

                epocaDropDownList.SelectedValue = season.name;
                CampeonatoListBox1.SelectedValue = champ.Name;

                TextBox1.Text = Request.QueryString["search"];

                //means that the user is showing the generic page of tournaments
                if (champ.Name=="TodosCampeonatos")
                {
                    Show(0);
                    string search = "";
                    if (TextBox1.Text != "")
                    {
                        search = HttpUtility.UrlDecode(HttpUtility.UrlEncode(TextBox1.Text.ToLower(), Encoding.GetEncoding("iso-8859-7")));
                    }

                    var tmpStartDate = season.startDate;
                    var tmpEndDate = season.endDate;
                    try
                    {

                    var torneios =
                        from c in FilesAccess.db.championships
                        from l in FilesAccess.db.levels
                        from tt in FilesAccess.db.tournamentType
                        where
                            (tt.allYears && c.idTournamentType == tt.id && c.IdLevel == l.id && tt.weight > 0) ||
                            c.IdLevel == l.id && 
                            c.idTournamentType == tt.id  && 
                            tt.weight > 0 &&
                            c.Date >= tmpStartDate && c.Date <= tmpEndDate &&
                            (search != "" ?
                                ((HttpUtility.UrlDecode(HttpUtility.UrlEncode(c.Name.ToLower(), Encoding.GetEncoding("iso-8859-7"))).Contains(search)) ||
                                HttpUtility.UrlDecode(HttpUtility.UrlEncode(l.Name.ToLower(), Encoding.GetEncoding("iso-8859-7"))).Contains(search) ||
                                HttpUtility.UrlDecode(HttpUtility.UrlEncode(tt.name.ToLower(), Encoding.GetEncoding("iso-8859-7"))).Contains(search)) : true)
                        select new
                        {
                            startDateField = tmpStartDate.ToString("dd/MM/yyyy"),
                            endDateField = tmpEndDate.ToString("dd/MM/yyyy"),
                            Id = c.Id,
                            Epoca = season.name,
                            Data = tt.allYears ? FilesAccess.db.games.Count(a => a.IdChamp == c.Id) > 0 ? FilesAccess.db.games.Where(a => a.IdChamp == c.Id).OrderByDescending(x =>x.Date).First().Date.ToString("dd/MM/yyyy") : c.Date.ToString("dd/MM/yyyy") : c.Date.ToString("dd/MM/yyyy"),
                            Nome = c.Name,
                            NumeroJogos = FilesAccess.db.games.Count(a => a.IdChamp == c.Id && a.Date >= tmpStartDate && a.Date <= tmpEndDate),
                            NumeroAtletas = Enumerable.Union(FilesAccess.db.games.Where(x => x.IdChamp == c.Id && x.Date >= tmpStartDate && x.Date <= tmpEndDate).Select(a => a.IdPlayerA), FilesAccess.db.games.Where(x => x.IdChamp == c.Id && x.Date >= tmpStartDate && x.Date <= tmpEndDate).Select(a => a.IdPlayerB)).Distinct().Count(),
                            Nivel = l.Name,
                            TipoTorneio = tt.name,
                            Peso = tt.weight
                        };
                    torneios = torneios.Where(x => x.NumeroJogos > 0);
                    GridView1.DataSource = torneios;
                    GridView1.DataBind();

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
                else
                {
                    var isChampionship = FilesAccess.db.tournamentType.Single(x => x.id == FilesAccess.db.championships.Single(a => a.Id == idTorneio).idTournamentType).championship;

                    //if it is a tournament, then go to TorneioIndividual
                    if (!isChampionship)
                    {
                        var url = String.Format("~/torneioDetalhe/{0}/{1}-{2}", season.name, champ.Id, champ.Name);
                        Response.Redirect(url);
                    }
                    Show(1);

                    rankingListGridView = RankingQuery();

                    rankingList.DataSource = rankingListGridView;
                    rankingList.DataBind();

                    var tmpStartDate = season.startDate;
                    var tmpEndDate = season.endDate;

                    nomeTorneio.InnerText = FilesAccess.db.championships.Single(x => x.Id == idTorneio).Name;
                    PanelClassificacao.Visible = false;
                    if (isChampionship)
                    {
                        PanelClassificacao.Visible = true;
                        var jogos = FilesAccess.db.teamGames.Where(x => x.Date >= tmpStartDate && x.Date <= tmpEndDate && x.IdChampionship == idTorneio && x.Day!=0);

                        var allTeamsId = jogos.Select(x => x.ClubAId).Union(jogos.Select(x => x.ClubBId)).Distinct();
                        var listaClassificacao = new List<ListClassificacao>();

                        foreach (var idClub in allTeamsId)
                        {
                            var jogosEquipa = jogos.Where(x => (x.ClubAId == idClub || x.ClubBId == idClub) && (x.GetScoreA()!=0 || x.GetScoreB()!=0));

                            listaClassificacao.Add(new ListClassificacao()
                            {
                                Pos = 0,
                                IdClub = idClub,
                                Clube = FilesAccess.db.clubs.SingleOrDefault(x => x.Id == idClub).Name + " " +
                                    FilesAccess.db.teamGames.Select(x => x.ClubAId == idClub && x.IdChampionship == idTorneio ? x.clubAnameAppend : x.ClubBId == idClub && x.IdChampionship == idTorneio ? x.clubBnameAppend : "").First(),
                                Jogos = jogosEquipa.Count(x=> x.GetScoreA() != 0 || x.GetScoreB() != 0),
                                Vitorias = jogosEquipa.Where(x => x.GetWinner() == idClub).Count(),
                                Derrotas = jogosEquipa.Where(x => x.GetWinner() != idClub && (x.GetScoreA()!=0 || x.GetScoreB()!=0)).Count(),
                                SetGanPer = jogosEquipa.Sum(x => x.ClubAId == idClub ? x.GetScoreA() : x.GetScoreB()) + " - " +
                                         jogosEquipa.Sum(x => x.ClubAId == idClub ? x.GetScoreB() : x.GetScoreA()),

                                Pontos = jogosEquipa.Count(x => x.GetWinner() == idClub && x.ClubAId == idClub && x.GetScoreA() - x.GetScoreB() >= 2) * 4 +
                                        jogosEquipa.Count(x => x.GetWinner() == idClub && x.ClubAId == idClub && x.GetScoreA() - x.GetScoreB() == 1) * 3 +
                                        jogosEquipa.Count(x => x.GetWinner() == idClub && x.ClubBId == idClub && x.GetScoreB() - x.GetScoreA() >= 2) * 4 +
                                        jogosEquipa.Count(x => x.GetWinner() == idClub && x.ClubBId == idClub && x.GetScoreB() - x.GetScoreA() == 1) * 3 +
                                        jogosEquipa.Count(x => x.ClubAId == idClub && x.GetScoreB() - x.GetScoreA() == 1) * 1 +
                                        jogosEquipa.Count(x => x.ClubBId == idClub && x.GetScoreA() - x.GetScoreB() == 1) * 1,

                                SetDif = jogosEquipa.Sum(x => x.ClubAId == idClub ? x.GetScoreA() : x.GetScoreB()) - jogosEquipa.Sum(x => x.ClubAId == idClub ? x.GetScoreB() : x.GetScoreA())
                            });
                        }

                        //tabela classificativa
                        listaClassificacao = listaClassificacao.OrderByDescending(x => x.Pontos).ThenByDescending(x => x.SetDif).ToList();

                        for (int i = 0; i < listaClassificacao.Count; i++)
                            listaClassificacao[i].Pos = (i + 1);

                        GridView4.DataSource = listaClassificacao;
                        GridView4.DataBind();
                    }

                    //jogos individuais jogadores
                    var dataJogadores =
                        from g in FilesAccess.db.games
                        where g.IdChamp == idTorneio &&
                            g.Date >= tmpStartDate && g.Date <= tmpEndDate
                        orderby g.Date descending
                        select new
                        {
                            startDateField = tmpStartDate.ToString("dd/MM/yyyy"),
                            endDateField = tmpEndDate.ToString("dd/MM/yyyy"),
                            IdChamp = idTorneio,
                            IdPlayerA = g.IdPlayerA,
                            IdPlayerB = g.IdPlayerB,
                            Data = g.Date.ToString("dd/MM/yyyy"),
                            Fase = FilesAccess.db.phase.SingleOrDefault(x => x.id == g.idPhase)?.phase,
                            JogadorA = FilesAccess.db.player.SingleOrDefault(x => x.Id == g.IdPlayerA)?.Name,
                            JogadorB = FilesAccess.db.player.SingleOrDefault(x => x.Id == g.IdPlayerB)?.Name,
                            Resultado = $"{g.SetsA}-{g.SetsB}",
                            PontosA = g.pointsPlayerA,
                            PontosB = g.pointsPlayerB
                        };

                    GridView2.DataSource = dataJogadores;
                    GridView2.DataBind();

                    //jornadas

                    var dataJogosEquipa =
                        from tg in FilesAccess.db.teamGames
                        where tg.Date >= tmpStartDate && tg.Date <= tmpEndDate && tg.IdChampionship == idTorneio
                        select new
                        {
                            startDateField = tmpStartDate.ToString("dd/MM/yyyy"),
                            endDateField = tmpEndDate.ToString("dd/MM/yyyy"),
                            IdChamp = idTorneio,
                            Epoca = season.name,
                            Campeonato = FilesAccess.db.championships.Single(x=>x.Id == idTorneio).Name,
                            Id = tg.Id,
                            IdClubA = tg.ClubAId,
                            IdClubB = tg.ClubBId,
                            Data = tg.Date.ToString("dd/MM/yyyy"),
                            Jornada = tg.Day,
                            ClubA = FilesAccess.db.clubs.SingleOrDefault(x => x.Id == tg.ClubAId).Name + " " + tg.clubAnameAppend,
                            ClubB = FilesAccess.db.clubs.SingleOrDefault(x => x.Id == tg.ClubBId).Name + " " + tg.clubBnameAppend,
                            Resultado = $"{tg.GetScoreA()}-{tg.GetScoreB()}",
                            date = tg.Date
                        };

                    dataJogosEquipa = dataJogosEquipa.OrderByDescending(x => x.Jornada);

                    var jogosPorJornada = dataJogosEquipa.GroupBy(x => x.Jornada).ToList();
                    
                    foreach (var jornada in jogosPorJornada)
                    {
                        listaJornadas.InnerHtml += $@"<div class='col-xs-12' style='text-align: center;'><h3 style='border-bottom:solid;margin-left:-15px;margin-right:-15px;'>Jornada {jornada.First().Jornada}</h3>";
                        
                        for (int i = 0; i < jornada.Count(); i++)
                        {
                            var jogo = jornada.ToList()[i];
                            listaJornadas.InnerHtml += "<div class='row' style='display: flex'>";
                            string addEnd = "border-bottom:solid";

                            listaJornadas.InnerHtml += $@"
                                                        <div class='col-xs-2' style='text-align: center;min-height:30px;{IsLast(i, jornada.Count(),addEnd)}'>{jogo.Data}</div>
                                                        <div class='col-xs-3' style='text-align: center;min-height:30px;{IsLast(i, jornada.Count(), addEnd)}'>
                                                             <a href='/clube/{jogo.IdClubA}-{jogo.ClubA}'>{jogo.ClubA}</a>
                                                        </div>
                                                        <div class='col-xs-2' style='text-align: center;min-height:30px;{IsLast(i, jornada.Count(), addEnd)}'>
                                                            <a href='javascript: toggleShowResult({jogo.Id})'>{jogo.Resultado}</a>
                                                        </div>
                                                        <div class='col-xs-5' style='text-align: center;min-height:30px;{IsLast(i, jornada.Count(), addEnd)}'>
                                                            <a href='/clube/{jogo.IdClubB}-{jogo.ClubB}'>{jogo.ClubB}</a>
                                                        </div>";

                            listaJornadas.InnerHtml += "</div>";
                            listaJornadas.InnerHtml += $@"<div id='detalhes{jogo.Id}' class='col-xs-12' style='text-align: center; display: none;'>";
                            listaJornadas.InnerHtml += GetResult(jogo.Id);
                            listaJornadas.InnerHtml += "</div>";
                        }
                        listaJornadas.InnerHtml += "</div>";
                    }
                    //GridView3.DataSource = dataJogosEquipa;
                    //GridView3.DataBind();

                    if (dataJogosEquipa.Count() > 0)
                        PanelEquipas.Visible = true;
                    else
                        GridView2.Visible = true;
                }
            }
        }

        private string GetResult(int idTeamGame)
        {
            var teamGame = FilesAccess.db.teamGames.SingleOrDefault(x => x.Id == idTeamGame);
            List < ListDetailGame> gameResult = new List<ListDetailGame>();
            int allChamp = FilesAccess.db.championships.SingleOrDefault(x => x.Name == "TodosCampeonatos").Id;
            int allSeason = FilesAccess.db.seasons.SingleOrDefault(x => x.name == "TodasEpocas").id;
            try
            {

            var dataSingles = from g in FilesAccess.db.games
                                where (g.IdTeamGame == teamGame.Id)
                                select new ListDetailGame()
                                {
                                    Id = g.id,
                                    IdPlayerA = g.IdPlayerA,
                                    IdPlayerB = g.IdPlayerB,
                                    JogadorA = FilesAccess.db.player.SingleOrDefault(x => x.Id == g.IdPlayerA).Name,
                                    JogadorB = FilesAccess.db.player.SingleOrDefault(x => x.Id == g.IdPlayerB).Name,
                                    Resultado = g.SetsA + " - " + g.SetsB,
                                    PontosA = Utilities.GetRankPoints(FilesAccess.db.ranking.SingleOrDefault(x => x.idChamp == allChamp && x.idPlayer == g.IdPlayerA && x.idSeason == allSeason)),
                                    PontosB = Utilities.GetRankPoints(FilesAccess.db.ranking.SingleOrDefault(x => x.idChamp == allChamp && x.idPlayer == g.IdPlayerB && x.idSeason == allSeason)),
                                    RankA = Utilities.GetRankPlace(FilesAccess.db.ranking.SingleOrDefault(x => x.idChamp == allChamp && x.idPlayer == g.IdPlayerA && x.idSeason == allSeason)),
                                    RankB = Utilities.GetRankPlace(FilesAccess.db.ranking.SingleOrDefault(x => x.idChamp == allChamp && x.idPlayer == g.IdPlayerB && x.idSeason == allSeason)),
                                    VerHistorico = FilesAccess.db.player.SingleOrDefault(x => x.Id == g.IdPlayerA).Name + " VS " + FilesAccess.db.player.SingleOrDefault(x => x.Id == g.IdPlayerB).Name
                                };
                
            if (FilesAccess.db.doubleGames.Count(x => x.IdTeamGame == teamGame.Id) > 0)
            {
                try
                {
                    var dataDoubles = from dg in FilesAccess.db.doubleGames
                                      where (dg.IdTeamGame == teamGame.Id)
                                      select new ListDetailGame()
                                      {
                                          Id = dg.Id,
                                          IdPlayerA = dg.DoubleA_PlayerAId << 16 | dg.DoubleA_PlayerBId,
                                          IdPlayerB = dg.DoubleB_PlayerAId << 16 | dg.DoubleB_PlayerBId,
                                          JogadorA = FilesAccess.db.player.SingleOrDefault(x => x.Id == dg.DoubleA_PlayerAId).Name + "-" + FilesAccess.db.player.SingleOrDefault(x => x.Id == dg.DoubleA_PlayerBId).Name,
                                          JogadorB = FilesAccess.db.player.SingleOrDefault(x => x.Id == dg.DoubleB_PlayerAId).Name + "-" + FilesAccess.db.player.SingleOrDefault(x => x.Id == dg.DoubleB_PlayerBId).Name,
                                          Resultado = dg.SetsA + " - " + dg.SetsB,
                                          PontosA = (Utilities.GetRankPoints(FilesAccess.db.ranking.SingleOrDefault(x => x.idChamp == allChamp && x.idPlayer == dg.DoubleA_PlayerAId && x.idSeason == allSeason)) + Utilities.GetRankPoints(FilesAccess.db.ranking.SingleOrDefault(x => x.idChamp == allChamp && x.idPlayer == dg.DoubleA_PlayerBId && x.idSeason == allSeason))) / 2,
                                          PontosB = (Utilities.GetRankPoints(FilesAccess.db.ranking.SingleOrDefault(x => x.idChamp == allChamp && x.idPlayer == dg.DoubleB_PlayerAId && x.idSeason == allSeason)) + Utilities.GetRankPoints(FilesAccess.db.ranking.SingleOrDefault(x => x.idChamp == allChamp && x.idPlayer == dg.DoubleB_PlayerBId && x.idSeason == allSeason))) / 2,

                                          RankA = (Utilities.GetRankPlace(FilesAccess.db.ranking.SingleOrDefault(x => x.idChamp == allChamp && x.idPlayer == dg.DoubleA_PlayerAId && x.idSeason == allSeason)) + Utilities.GetRankPlace(FilesAccess.db.ranking.SingleOrDefault(x => x.idChamp == allChamp && x.idPlayer == dg.DoubleA_PlayerBId && x.idSeason == allSeason))) / 2,

                                          RankB = (Utilities.GetRankPlace(FilesAccess.db.ranking.SingleOrDefault(x => x.idChamp == allChamp && x.idPlayer == dg.DoubleB_PlayerAId && x.idSeason == allSeason)) + Utilities.GetRankPlace(FilesAccess.db.ranking.SingleOrDefault(x => x.idChamp == allChamp && x.idPlayer == dg.DoubleB_PlayerBId && x.idSeason == allSeason))) / 2,
                                      };
                    gameResult.AddRange(dataDoubles);
                }
                catch(Exception ex)
                {
                    MessageBox.Show($"double game {teamGame.Id}. {FilesAccess.db.doubleGames.Count(x => x.IdTeamGame == teamGame.Id)}\n{ex.Message}");
                }
            }
            gameResult.AddRange(dataSingles);

            }
            catch (Exception ex)
            {
                MessageBox.Show($"single game {teamGame.Id}. {FilesAccess.db.games.Count(x => x.IdTeamGame == teamGame.Id)}\n{ex.Message}");

            }
            StringBuilder sr = new StringBuilder();
            sr.Append(@"<div class='row' style='display: flex'>");
            sr.Append($@"<div class='col-xs-1' style='text-align: center;border-top:solid;border-bottom:dashed;min-height:25px'>Ranking</div>
                                <div class='col-xs-4' style='text-align: center;border-top:solid;border-bottom:dashed;min-height:25px'>{FilesAccess.db.clubs.Single(x => x.Id == teamGame.ClubAId).Shortname} {teamGame.clubAnameAppend}</div>
                                <div class='col-xs-2' style='text-align: center;border-top:solid;border-bottom:dashed;min-height:25px'>Resultado</div>
                                <div class='col-xs-4' style='text-align: center;border-top:solid;border-bottom:dashed;min-height:25px'>{FilesAccess.db.clubs.Single(x => x.Id == teamGame.ClubBId).Shortname} {teamGame.clubBnameAppend}</div>
                                <div class='col-xs-1' style='text-align: center;border-top:solid;border-bottom:dashed;min-height:25px'>Ranking</div>");
            sr.Append(@"</div>");
            for (int i=0; i < gameResult.Count;i++)
            {
                string addEnd = "border-bottom:solid;margin-bottom:10px";
                sr.Append(@"<div class='row' style='display: flex'>");
                    sr.Append($@"<div class='col-xs-1' style='text-align: center;min-height:25px;{IsLast(i, gameResult.Count, addEnd)}'>
                                    <a href='/ranking/TodasEpocas/{allChamp}-TodosCampeonatos'>{gameResult[i].RankA}</a>
                                </div>
                                <div class='col-xs-4' style='text-align: center;min-height:25px;{IsLast(i, gameResult.Count, addEnd)}'>
                                    <a href='/jogador/{gameResult[i].IdPlayerA}-{gameResult[i].JogadorA}'>{gameResult[i].JogadorA}</a>
                                </div>
                                <div class='col-xs-2' style='text-align: center;min-height:25px;{IsLast(i, gameResult.Count, addEnd)}'>{gameResult[i].Resultado}</div>
                                <div class='col-xs-4' style='text-align: center;min-height:25px;{IsLast(i, gameResult.Count, addEnd)}'>
                                    <a href='/jogador/{gameResult[i].IdPlayerB}-{gameResult[i].JogadorB}'>{gameResult[i].JogadorB}</a>
                                </div>
                                <div class='col-xs-1' style='text-align: center;min-height:25px;{IsLast(i, gameResult.Count, addEnd)}'>
                                    <a href='/ranking/TodasEpocas/{allChamp}-TodosCampeonatos'>{gameResult[i].RankB}</a>
                                </div>");
                sr.Append(@"</div>");
            }
            return sr.ToString();

        }

        private string IsLast(int actual, int total, string input)
        {
            if (actual == total - 1)
                return input;
            return "";
        }

        private IEnumerable<ListRanking> RankingQuery()
        {
            var rank = FilesAccess.db.ranking.Where(x => x.idSeason == season.id && x.idChamp == champ.Id);
            var data = from r in rank
                       from p in FilesAccess.db.player
                       orderby r.points descending
                       where p.Id == r.idPlayer
                       select new ListRanking()
                       {
                           IdChamp = r.idChamp,
                           IdSeason = r.idSeason,
                           Season = FilesAccess.db.seasons.Single(x => x.id == r.idSeason).name,
                           Campeonato = FilesAccess.db.championships.Single(x => x.Id == r.idChamp).Name,
                           Id = p.Id,
                           Licenca = p.licenseNo,
                           Ranking = r.rank,
                           Nome = p.Name,
                           Clube = FilesAccess.db.clubs.Single(x => x.Id == r.idClub).Name,
                           Pontos = r.points,
                           Record = $"{r.numWins}-{r.numLosses}",
                           IdClub = FilesAccess.db.clubs.Single(x => x.Id == p.IdClub).Id,
                           WinPercentage = 1D * (r.points) / (r.numWins + r.numLosses)
                       };
            return data;
        }

        private SortDirection GetSortDirection(string column)
        {
            SortDirection nextDir = SortDirection.Ascending; // Default next sort expression behaviour.
            if (ViewState["sort"] != null && ViewState["sort"].ToString() == column)
            {   // Exists... DESC.
                nextDir = System.Web.UI.WebControls.SortDirection.Descending;
                ViewState["sort"] = null;
            }
            else
            {   // Doesn't exists, set ViewState.
                ViewState["sort"] = column;
            }
            return nextDir;
        }

        protected void rankingList_Sorting(object sender, GridViewSortEventArgs e)
        {
            //RankingQuery();
            var sortDirection = GetSortDirection("GridView1" + e.SortExpression);
            switch (e.SortExpression)
            {
                case "Ranking":
                    switch (sortDirection)
                    {
                        case SortDirection.Ascending:
                            rankingListGridView = rankingListGridView.OrderBy(x => x.Ranking).ToList();
                            break;
                        case SortDirection.Descending:
                            rankingListGridView = rankingListGridView.OrderByDescending(x => x.Ranking).ToList();
                            break;
                        default:
                            break;
                    }
                    break;
                case "Licenca":
                    switch (sortDirection)
                    {
                        case SortDirection.Ascending:
                            rankingListGridView = rankingListGridView.OrderBy(x => x.Licenca).ToList();
                            break;
                        case SortDirection.Descending:
                            rankingListGridView = rankingListGridView.OrderByDescending(x => x.Licenca).ToList();
                            break;
                        default:
                            break;
                    }
                    break;
                case "Nome":
                    switch (sortDirection)
                    {
                        case SortDirection.Ascending:
                            rankingListGridView = rankingListGridView.OrderBy(x => x.Nome).ToList();
                            break;
                        case SortDirection.Descending:
                            rankingListGridView = rankingListGridView.OrderByDescending(x => x.Nome).ToList();
                            break;
                        default:
                            break;
                    }
                    break;
                case "Clube":
                    switch (sortDirection)
                    {
                        case SortDirection.Ascending:
                            rankingListGridView = rankingListGridView.OrderBy(x => x.Clube).ToList();
                            break;
                        case SortDirection.Descending:
                            rankingListGridView = rankingListGridView.OrderByDescending(x => x.Clube).ToList();
                            break;
                        default:
                            break;
                    }
                    break;
                case "Pontos":
                    switch (sortDirection)
                    {
                        case System.Web.UI.WebControls.SortDirection.Ascending:
                            rankingListGridView = rankingListGridView.OrderBy(x => x.Pontos).ToList();
                            break;
                        case System.Web.UI.WebControls.SortDirection.Descending:
                            rankingListGridView = rankingListGridView.OrderByDescending(x => x.Pontos).ToList();
                            break;
                        default:
                            break;
                    }
                    break;
                case "Record":
                    switch (sortDirection)
                    {
                        case SortDirection.Ascending:
                            rankingListGridView = rankingListGridView.OrderByDescending(x => x.WinPercentage).ToList();
                            break;
                        case SortDirection.Descending:
                            rankingListGridView = rankingListGridView.OrderBy(x => x.WinPercentage).ToList();
                            break;
                        default:
                            break;
                    }
                    break;
            }
            rankingList.DataSource = rankingListGridView;
            rankingList.DataBind();
        }

        private void Show(int id)
        {
            switch(id)
            {
                case 0:

                    GridView1.Visible = true;
                    backButton.Visible = false;
                    nomeTorneio.Visible = false;
                    GridView2.Visible = false;
                    PanelClassificacao.Visible = false;
                    break;

                case 1:

                    GridView1.Visible = false;
                    backButton.Visible = true;
                    nomeTorneio.Visible = true;
                    GridView2.Visible = true;
                    PanelClassificacao.Visible = true;
                    break;
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            var url = String.Format("~/torneio/{0}/{1}-{2}", season.name, champ.Id, champ.Name);
            Response.Redirect(url);
        }

        protected void backButton_Click(object sender, EventArgs e)
        {
            var url = String.Format("~/torneio/{0}", season.name);
            Response.Redirect(url);
        }

        protected void GridView2_RowEditing(object sender, GridViewEditEventArgs e)
        {
            //Set the edit index.
            GridView2.EditIndex = e.NewEditIndex;
            //Bind data to the GridView control.
            MessageBox.Show(e.NewEditIndex.ToString());
        }

        protected void GridView2_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {

        }

        protected void GridView2_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridView1.EditIndex = -1; //swicth back to default mode
        }
        
    }
}