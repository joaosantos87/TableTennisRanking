﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1
{
    public class ListClassificacao
    {
        public int Pos { get; set; }
        public int IdClub { get; set; }
        public string Clube { get; set; }
        public int Jogos { get; set; }
        public int Vitorias { get; set; }
        public int Derrotas { get; set; }
        public string SetGanPer { get; set; }
        public int SetDif { get; set; }
        public int Pontos { get; set; }
    }
}