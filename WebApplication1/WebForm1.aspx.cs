﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using TableTennis.Database;

namespace WebApplication1
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var lastGames =
                 from g in FilesAccess.db.games
                 orderby g.id descending
                 select new
                 {
                     IdChamp = g.IdChamp,
                     IdPlayerA = g.IdPlayerA,
                     IdPlayerB = g.IdPlayerB,
                     Data = g.Date.ToString("dd/MM/yyyy"),
                     JogadorA = FilesAccess.db.player.SingleOrDefault(x => x.Id == g.IdPlayerA)?.Name,
                     JogadorB = FilesAccess.db.player.SingleOrDefault(x => x.Id == g.IdPlayerB)?.Name,
                     //JogadorA = "a",
                     //JogadorB = "b",
                     Resultado = $"{g.SetsA}-{g.SetsB}",
                     PontosA = g.pointsPlayerA,
                     PontosB = g.pointsPlayerB
                 };
            GridView2.DataSource = lastGames;
            //GridView2.DataBind();
        }
    }
}