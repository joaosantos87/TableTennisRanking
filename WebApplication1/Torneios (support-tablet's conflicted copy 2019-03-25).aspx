﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Torneios.aspx.cs" Inherits="WebApplication1.Torneios" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <br />
    <asp:Button Visible="false" ID="backButton" runat="server" Text="Voltar ao inicio" OnClick ="backButton_Click"/>
    <br />
    <h1 id="h1Torneio" runat="server"> Torneios </h1>
    <h3 runat="server"> Pesquisa </h3>
    <br />
    
    <h5 runat="server"> Epoca
        <asp:DropDownList ID="DropDownList1" runat="server" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged"></asp:DropDownList>
        </h5>
    Data:
    <asp:TextBox ID="startDateTextbox" runat="server" ReadOnly="true"></asp:TextBox>
    <asp:TextBox ID="endDateTextbox" runat="server" ReadOnly="true"></asp:TextBox>
    <br />
    Pesquisa:
    <asp:TextBox ID="TextBox1" runat="server" Width="210px"></asp:TextBox>
    <br />
     <asp:Button ID="Button1" runat="server" Text="Pesquisar" OnClick="Button1_Click" />
     <asp:GridView ID="GridView1" runat="server" Visible="False" AutoGenerateColumns="false">
          <Columns>
            <asp:BoundField DataField="startDateField" HeaderText="startDate" Visible="false" InsertVisible="False" ReadOnly="True" SortExpression="startDate" />
            <asp:BoundField DataField="endDateField" HeaderText="endDate" Visible="false" InsertVisible="False" ReadOnly="True" SortExpression="endDate" />
            <asp:BoundField DataField="Id" HeaderText="Id" Visible="false" InsertVisible="False" ReadOnly="True" SortExpression="Id" />
            <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Data" HeaderText="Data" SortExpression="Data" />
            <asp:HyperLinkField ItemStyle-HorizontalAlign="Center" DataTextField="Nome" HeaderText="Nome" SortExpression="Name" DataNavigateUrlFields="Id, startDateField, endDateField"  DataNavigateUrlFormatString="~/Torneios.aspx?ID={0}&startDate={1}&endDate={2}"/>
            <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="NumeroJogos" HeaderText="Numero Jogos" SortExpression="NumeroJogos" />
            <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="NumeroAtletas" HeaderText="Numero Atletas" SortExpression="NumeroAtletas" />
            <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Nivel" HeaderText="Nivel" SortExpression="Nivel" />
              <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="TipoTorneio" HeaderText="Tipo Torneio" SortExpression="TipoTorneio" />
              <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Peso" HeaderText="Peso" SortExpression="Peso" />
        </Columns>
     </asp:GridView>
         <br />
    
    <h2 id="nomeTorneio" runat="server"></h2>
    <asp:Panel ID="PanelClassificacao" runat="server" Visible="false">
        <asp:GridView ID="GridView4" runat="server" AutoGenerateColumns="false" Width="500">
            <Columns>
                <asp:BoundField  ItemStyle-HorizontalAlign="Center" DataField="Pos" HeaderText="Pos." SortExpression="Pos" />
                <asp:BoundField DataField="IdClub" HeaderText="IdClub" Visible="false" />
                <asp:HyperLinkField DataTextField="Clube" HeaderText="Clube" SortExpression="Clube" DataNavigateUrlFields="IdClub"  DataNavigateUrlFormatString="~/Clubes.aspx?Id={0}"/>
                <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Jogos" HeaderText="Jogos" SortExpression="Jogos" />
                <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Vitorias" HeaderText="Vit." SortExpression="Vitorias" />
                <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Derrotas" HeaderText="Der." SortExpression="Derrotas" />
                <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="SetGanPer" HeaderText="Set/Gan-Per" SortExpression="Set/Gan-Per" />
                <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="SetDif" HeaderText="Set/Dif" SortExpression="Set/Dif" />
                <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Pontos" HeaderText="Pontos" SortExpression="Pontos" />
            </Columns>
        </asp:GridView>
    </asp:Panel>
    <asp:Panel ID="PanelEquipas" runat="server" Visible="false">
        <h3 runat="server">Jogos</h3>
        <asp:GridView ID="GridView3" runat="server" AutoGenerateColumns="false">
            <Columns>
                <asp:BoundField DataField="startDateField" HeaderText="startDate" Visible="false" InsertVisible="False" ReadOnly="True" SortExpression="startDate" />
                <asp:BoundField DataField="endDateField" HeaderText="endDate" Visible="false" InsertVisible="False" ReadOnly="True" SortExpression="endDate" />
                <asp:BoundField DataField="IdChamp" HeaderText="IdChamp" InsertVisible="False" ReadOnly="True" SortExpression="IdChamp" Visible="false" />
                <asp:BoundField DataField="Id" HeaderText="Id" SortExpression="Id" Visible="false" />
                <asp:BoundField DataField="IdClubA" HeaderText="IdClubA" SortExpression="IdClubA" Visible="false" />
                <asp:BoundField DataField="IdClubB" HeaderText="IdClubB" SortExpression="IdClubB" Visible="false" />
                <asp:BoundField DataField="Data" HeaderText="Data" SortExpression="Data" /> 
                <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Jornada" HeaderText="Jornada" SortExpression="Jornada" />
                <asp:HyperLinkField ItemStyle-HorizontalAlign="Center" DataTextField="ClubA" HeaderText="Clube" SortExpression="Clube" DataNavigateUrlFields="IdClubA"  DataNavigateUrlFormatString="~/Clubes.aspx?Id={0}"/>
                <asp:HyperLinkField ItemStyle-HorizontalAlign="Center" DataTextField="ClubB" HeaderText="Clube" SortExpression="Clube" DataNavigateUrlFields="IdClubB"  DataNavigateUrlFormatString="~/Clubes.aspx?Id={0}"/>
                <asp:HyperLinkField ItemStyle-HorizontalAlign="Center" DataTextField="Resultado" HeaderText="Resultado" SortExpression="Resultado" DataNavigateUrlFields="Id"  DataNavigateUrlFormatString="~/ResultadoDetalhe.aspx?Id={0}"/>
            </Columns>
        </asp:GridView>
    </asp:Panel>

    <h3 runat="server">Resultados Detalhados</h3>
        <asp:GridView ID="GridView2" runat="server" Visible="False" AutoGenerateColumns="false" AutoGenerateEditButton="false" OnRowEditing="GridView2_RowEditing" OnRowUpdating="GridView2_RowUpdating" OnRowCancelingEdit="GridView2_RowCancelingEdit">
            <Columns>
                <asp:BoundField DataField="startDateField" HeaderText="startDate" Visible="false" InsertVisible="False" ReadOnly="True" SortExpression="startDate" />
                <asp:BoundField DataField="endDateField" HeaderText="endDate" Visible="false" InsertVisible="False" ReadOnly="True" SortExpression="endDate" />
                <asp:BoundField DataField="IdChamp" HeaderText="IdChamp" InsertVisible="False" ReadOnly="True" SortExpression="IdChamp" Visible="false" />
                <asp:BoundField DataField="IdPlayerA" HeaderText="IdPlayerA" InsertVisible="False" ReadOnly="True" SortExpression="IdPlayerA" Visible="false" />
                <asp:BoundField DataField="IdPlayerB" HeaderText="IdPlayerB" InsertVisible="False" ReadOnly="True" SortExpression="IdPlayerB" Visible="false" />
                <asp:BoundField DataField="Data" HeaderText="Data" SortExpression="Data" />
                <asp:HyperLinkField DataTextField="JogadorA" HeaderText="Jogador" SortExpression="JogadorA" DataNavigateUrlFields="IdPlayerA, IdChamp, startDateField, endDateField"  DataNavigateUrlFormatString="~/Players.aspx?ID={0}&campeonatos={1}&startDate={2}&endDate={3}"/>
                <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="PontosA" HeaderText="Pontos Rating" InsertVisible="False" ReadOnly="True" SortExpression="Pontos" />
                <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Resultado" HeaderText="Resultado" SortExpression="Resultado" ReadOnly="false" />
                <asp:HyperLinkField DataTextField="JogadorB" HeaderText="Jogador" SortExpression="JogadorB" DataNavigateUrlFields="IdPlayerB, IdChamp, startDateField, endDateField"  DataNavigateUrlFormatString="~/Players.aspx?ID={0}&campeonatos={1}&startDate={2}&endDate={3}"/>
                <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="PontosB" HeaderText="Pontos Rating" InsertVisible="False" ReadOnly="True" SortExpression="Pontos" />
            </Columns>
        </asp:GridView>
</asp:Content>
