﻿using Microsoft.Ajax.Utilities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using TableTennis;
using TableTennis.Database;

namespace WebApplication1
{
    public partial class Torneios : System.Web.UI.Page
    {
        int idTorneio = 0;
        //DateTime startDate;
        //DateTime endDate;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (DropDownList1.Items.Count == 0)
                {
                    var minDate = FilesAccess.db.games.Min(x => x.Date.Year);
                    for (int i = DateTime.Now.Year; i > minDate; i--)
                        DropDownList1.Items.Add(i.ToString());
                }

                DropDownList1_SelectedIndexChanged(this, new EventArgs());
                
                startDateTextbox.Text = Request.QueryString["startDate"] != null ? Request.QueryString["startDate"] : startDateTextbox.Text;
                endDateTextbox.Text = Request.QueryString["endDate"] != null ? Request.QueryString["endDate"] : endDateTextbox.Text;
                
                TextBox1.Text = Request.QueryString["search"];

                var idQuest = Request.QueryString["ID"];
                idTorneio = idQuest == "" ? 0 : Convert.ToInt32(idQuest);

                //means that the user is showing the generic page of tournaments
                if (idTorneio == 0)
                {
                    Show(0);
                    string search = "";
                    if (TextBox1.Text != "")
                    {
                        search = HttpUtility.UrlDecode(HttpUtility.UrlEncode(TextBox1.Text.ToLower(), Encoding.GetEncoding("iso-8859-7")));
                    }

                    var tmpStartDate = DateTime.ParseExact(startDateTextbox.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    var tmpEndDate = DateTime.ParseExact(endDateTextbox.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                    var torneios =
                        from c in FilesAccess.db.championships
                        from l in FilesAccess.db.levels
                        from tt in FilesAccess.db.tournamentType
                        where
                            (tt.allYears && c.idTournamentType == tt.id && c.IdLevel == l.id && tt.weight > 0) ||
                            c.IdLevel == l.id && 
                            c.idTournamentType == tt.id  && 
                            tt.weight > 0 &&
                            c.Date >= tmpStartDate && c.Date <= tmpEndDate &&
                            (search != "" ?
                                ((HttpUtility.UrlDecode(HttpUtility.UrlEncode(c.Name.ToLower(), Encoding.GetEncoding("iso-8859-7"))).Contains(search)) ||
                                HttpUtility.UrlDecode(HttpUtility.UrlEncode(l.Name.ToLower(), Encoding.GetEncoding("iso-8859-7"))).Contains(search) ||
                                HttpUtility.UrlDecode(HttpUtility.UrlEncode(tt.name.ToLower(), Encoding.GetEncoding("iso-8859-7"))).Contains(search)) : true)
                        select new
                        {
                            startDateField = startDateTextbox.Text,
                            endDateField = endDateTextbox.Text,
                            Id = c.Id,
                            Data = tt.allYears ? FilesAccess.db.games.Where(a => a.IdChamp == c.Id).OrderByDescending(x =>x.Date).First().Date.ToString("dd/MM/yyyy") : c.Date.ToString("dd/MM/yyyy"),
                            Nome = c.Name,
                            NumeroJogos = FilesAccess.db.games.Count(a => a.IdChamp == c.Id),
                            NumeroAtletas = Enumerable.Union(FilesAccess.db.games.Where(x => x.IdChamp == c.Id).Select(a => a.IdPlayerA), FilesAccess.db.games.Where(x => x.IdChamp == c.Id).Select(a => a.IdPlayerB)).Distinct().Count(),
                            Nivel = l.Name,
                            TipoTorneio = tt.name,
                            Peso = tt.weight
                        };

                    GridView1.DataSource = torneios;
                    GridView1.DataBind();
                }
                else
                {
                    Show(1);

                    var tmpStartDate = DateTime.ParseExact(startDateTextbox.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    var tmpEndDate = DateTime.ParseExact(endDateTextbox.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                    nomeTorneio.InnerText = FilesAccess.db.championships.Single(x => x.Id == idTorneio).Name;
                    PanelClassificacao.Visible = false;
                    if (FilesAccess.db.tournamentType.Single(x => x.id == FilesAccess.db.championships.Single(a => a.Id == idTorneio).idTournamentType).championship)
                    {
                        PanelClassificacao.Visible = true;
                        var jogos = FilesAccess.db.teamGames.Where(x => x.Date >= tmpStartDate && x.Date <= tmpEndDate && x.IdChampionship == idTorneio);

                        var allTeamsId = jogos.Select(x => x.ClubAId).Union(jogos.Select(x => x.ClubBId)).Distinct();
                        var listaClassificacao = new List<ListClassificacao>();

                        foreach (var idClub in allTeamsId)
                        {
                            var jogosEquipa = jogos.Where(x => x.ClubAId == idClub || x.ClubBId == idClub);
                            listaClassificacao.Add(new ListClassificacao()
                            {
                                Pos = 0,
                                IdClub = idClub,
                                Clube = FilesAccess.db.clubs.SingleOrDefault(x => x.Id == idClub).Name + " " +
                                    FilesAccess.db.teamGames.Select(x => x.ClubAId == idClub && x.IdChampionship == idTorneio ? x.clubAnameAppend : x.ClubBId == idClub && x.IdChampionship == idTorneio ? x.clubBnameAppend : "").First(),
                                Jogos = jogosEquipa.Count(x => x.GetScoreA() != 0 || x.GetScoreB() != 0),
                                Vitorias = jogosEquipa.Where(x => x.GetWinner() == idClub).Count(),
                                Derrotas = jogosEquipa.Where(x => x.GetWinner() != idClub).Count(),
                                SetGanPer = jogosEquipa.Sum(x => x.ClubAId == idClub ? x.GetScoreA() : x.GetScoreB()) + " - " +
                                         jogosEquipa.Sum(x => x.ClubAId == idClub ? x.GetScoreB() : x.GetScoreA()),

                                Pontos = jogosEquipa.Count(x => x.GetWinner() == idClub && x.ClubAId == idClub && x.GetScoreA() - x.GetScoreB() > 2) * 4 +
                                        jogosEquipa.Count(x => x.GetWinner() == idClub && x.ClubAId == idClub && x.GetScoreA() - x.GetScoreB() == 1) * 3 +
                                        jogosEquipa.Count(x => x.GetWinner() == idClub && x.ClubBId == idClub && x.GetScoreB() - x.GetScoreA() > 2) * 4 +
                                        jogosEquipa.Count(x => x.GetWinner() == idClub && x.ClubBId == idClub && x.GetScoreB() - x.GetScoreA() == 1) * 3 +
                                        jogosEquipa.Count(x => x.ClubAId == idClub && x.GetScoreB() - x.GetScoreA() == 1) * 1 +
                                        jogosEquipa.Count(x => x.ClubBId == idClub && x.GetScoreA() - x.GetScoreB() == 1) * 1,

                                SetDif = jogosEquipa.Sum(x => x.ClubAId == idClub ? x.GetScoreA() : x.GetScoreB()) - jogosEquipa.Sum(x => x.ClubAId == idClub ? x.GetScoreB() : x.GetScoreA())
                            });
                        }
                        listaClassificacao = listaClassificacao.OrderByDescending(x => x.Pontos).ThenByDescending(x => x.SetDif).ToList();

                        for (int i = 0; i < listaClassificacao.Count; i++)
                            listaClassificacao[i].Pos = (i + 1);

                        GridView4.DataSource = listaClassificacao;
                        GridView4.DataBind();
                    }
                    var dataJogadores =
                        from g in FilesAccess.db.games
                        where g.IdChamp == idTorneio &&
                            g.Date >= tmpStartDate && g.Date <= tmpEndDate
                        orderby g.Date descending
                        select new
                        {
                            startDateField = startDateTextbox.Text,
                            endDateField = endDateTextbox.Text,
                            IdChamp = idTorneio,
                            IdPlayerA = g.IdPlayerA,
                            IdPlayerB = g.IdPlayerB,
                            Data = g.Date.ToString("dd/MM/yyyy"),
                            JogadorA = FilesAccess.db.player.SingleOrDefault(x => x.Id == g.IdPlayerA)?.Name,
                            JogadorB = FilesAccess.db.player.SingleOrDefault(x => x.Id == g.IdPlayerB)?.Name,
                            Resultado = $"{g.SetsA}-{g.SetsB}",
                            PontosA = g.pointsPlayerA,
                            PontosB = g.pointsPlayerB
                        };

                    GridView2.DataSource = dataJogadores;
                    GridView2.DataBind();

                    var dataJogosEquipa =
                        from tg in FilesAccess.db.teamGames
                        where tg.Date >= tmpStartDate && tg.Date <= tmpEndDate && tg.IdChampionship == idTorneio
                        select new
                        {
                            startDateField = startDateTextbox.Text,
                            endDateField = endDateTextbox.Text,
                            IdChamp = idTorneio,
                            Id = tg.Id,
                            IdClubA = tg.ClubAId,
                            IdClubB = tg.ClubBId,
                            Data = tg.Date.ToString("dd/MM/yyyy"),
                            Jornada = tg.Day,
                            ClubA = FilesAccess.db.clubs.SingleOrDefault(x => x.Id == tg.ClubAId).Name + " " + tg.clubAnameAppend,
                            ClubB = FilesAccess.db.clubs.SingleOrDefault(x => x.Id == tg.ClubBId).Name + " " + tg.clubBnameAppend,
                            Resultado = $"{tg.GetScoreA()}-{tg.GetScoreB()}"
                        };

                    GridView3.DataSource = dataJogosEquipa;
                    GridView3.DataBind();

                    if (dataJogosEquipa.Count() > 0)
                        PanelEquipas.Visible = true;
                }
            }
        }

        private void Show(int id)
        {
            switch(id)
            {
                case 0:

                    GridView1.Visible = true;
                    backButton.Visible = false;
                    nomeTorneio.Visible = false;
                    GridView2.Visible = false;
                    PanelClassificacao.Visible = false;
                    break;

                case 1:

                    GridView1.Visible = false;
                    backButton.Visible = true;
                    nomeTorneio.Visible = true;
                    GridView2.Visible = true;
                    PanelClassificacao.Visible = true;
                    break;
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            int idTorn = FilesAccess.db.championships.SingleOrDefault(x => x.Name == nomeTorneio.InnerText).Id;
            var url = String.Format("~/Torneios?startDate={0}&endDate={1}&search={2}&ID={3}", startDateTextbox.Text, endDateTextbox.Text, TextBox1.Text, idTorn != 0? idTorn.ToString():"");
            Response.Redirect(url);
        }

        protected void backButton_Click(object sender, EventArgs e)
        {
            var url = String.Format("~/Torneios?startDate={0}&endDate={1}", startDateTextbox.Text, endDateTextbox.Text);
            Response.Redirect(url);
        }

        protected void GridView2_RowEditing(object sender, GridViewEditEventArgs e)
        {
            //Set the edit index.
            GridView2.EditIndex = e.NewEditIndex;
            //Bind data to the GridView control.
            MessageBox.Show(e.NewEditIndex.ToString());
        }

        protected void GridView2_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {

        }

        protected void GridView2_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridView1.EditIndex = -1; //swicth back to default mode
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            int year = Convert.ToInt32(DropDownList1.Text);

            startDateTextbox.Text = $"01/07/{year}";
            endDateTextbox.Text = $"30/06/{year + 1}";
        }
    }
}