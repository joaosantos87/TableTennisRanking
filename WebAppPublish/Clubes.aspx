﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Clubes.aspx.cs" Inherits="WebApplication1.Clubes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <br>
    <asp:Panel ID="Panel1" runat="server" Visible="false">
        <div class="col-sm-12" id="baseClubes" runat="server"></div>
        <!--
        <div class="table-responsive">
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" CssClass="table table-striped" >
                <Columns>
                    <asp:BoundField DataField="Id" HeaderText="Id" Visible="false" InsertVisible="False" ReadOnly="True" SortExpression="Id" />
                    <asp:HyperLinkField ItemStyle-HorizontalAlign="Center" DataTextField="Nome" HeaderText="Nome" SortExpression="Nome" DataNavigateUrlFields="Id, Nome"  DataNavigateUrlFormatString="~/Clube/{0}-{1}"/>
                    <asp:BoundField  DataField="AssociacaoId" HeaderText="AssociacaoId" SortExpression="AssociacaoId" Visible="false" />
                    <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="NumeroAtletas" HeaderText="Atletas" SortExpression="NumeroAtletas" />
                    <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="NumeroJogos" HeaderText="Jogos" SortExpression="NumeroJogos" />
                    <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Vitorias" HeaderText="Vitorias" SortExpression="Vitorias" />
                    <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Derrotas" HeaderText="Derrotas" SortExpression="Derrotas" />
                    </Columns>
            </asp:GridView>
        </div>-->
    </asp:Panel>
    <asp:Panel ID="Panel2" runat="server" Visible="false">
        <asp:Button ID="Button1" runat="server" Text="Todos os clubes" OnClick="Button1_Click" />
        
        <div class="col-xs-12" id="headerClube" runat="server"></div>
        
        <div class="col-sm-7" id="Div1" runat="server">
            <h3>Jogos</h3>
            
            <div class="table-responsive">
                <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="false"  CssClass="table table-striped">
                    <Columns>
                        <asp:BoundField DataField="Id" Visible="false" />
                        <asp:BoundField DataField="IdClub" Visible="false" />
                        <asp:BoundField DataField="IdClubAdv" Visible="false" />
                        <asp:BoundField DataField="Clube" Visible="false" />
                        <asp:BoundField DataField="ClubeAdv" Visible="false" />
                        <asp:BoundField DataField="IdTorneio" Visible="false" />
                        <asp:BoundField DataField="Epoca" Visible="false" />
                        <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Data" HeaderText="Data" SortExpression="Data" /> 
                        <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Equipa" HeaderText="Equipa" SortExpression="Data" /> 
                        <asp:HyperLinkField ItemStyle-HorizontalAlign="Center" DataTextField="Torneio" HeaderText="Campeonato" SortExpression="Torneio" DataNavigateUrlFields="IdTorneio, Torneio, Epoca"  DataNavigateUrlFormatString="~/torneio/{2}/{0}-{1}"/>
                        <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Jornada" HeaderText="Jornada" SortExpression="Jornada" />
                        <asp:HyperLinkField ItemStyle-HorizontalAlign="Center" DataTextField="Adversario" HeaderText="Adversario" SortExpression="Adversario" DataNavigateUrlFields="IdClubAdv"  DataNavigateUrlFormatString="Clube/{0}"/>
                        <asp:HyperLinkField ItemStyle-HorizontalAlign="Center" DataTextField="Resultado" HeaderText="Resultado" SortExpression="Resultado" DataNavigateUrlFields="Id, Clube, ClubeAdv, Torneio, Epoca"  DataNavigateUrlFormatString="~/detalhes/{4}/{3}/{0}-{1}-vs-{2}"/>
                    </Columns>
                </asp:GridView>
            </div>
            <asp:Panel ID="Panel3" runat="server">
        
            </asp:Panel>
        </div>
        
        <div class="col-sm-5" id="Div2" runat="server">
             <h3>Jogadores</h3>
            <div class="table-responsive">
                <asp:GridView ID="GridView3" runat="server" AutoGenerateColumns="False" CssClass="table table-striped">
                    <Columns>
                        <asp:BoundField DataField="Id" HeaderText="Id" Visible="False" />
                        <asp:BoundField DataField="Campeonato" HeaderText="Campeonato" Visible="False" />
                        <asp:BoundField DataField="IdCampeonato" HeaderText="IdCampeonato" Visible="False" />
                        <asp:BoundField DataField="DataInicioEpoca" HeaderText="DataInicioEpoca" Visible="false" />
                        <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Epoca" HeaderText="Epoca" SortExpression="Epoca" />
                        <asp:HyperLinkField DataTextField="Ranking" HeaderText="Ranking" SortExpression="Ranking" DataNavigateUrlFields="Epoca,IdCampeonato, Campeonato"  DataNavigateUrlFormatString="ranking/{0}/{1}-{2}"/>
                        <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Licenca" HeaderText="Licenca" SortExpression="Licenca" />
                        <asp:HyperLinkField DataTextField="Nome" HeaderText="Nome" SortExpression="Nome" DataNavigateUrlFields="Id, Nome"  DataNavigateUrlFormatString="Jogador/{0}-{1}"/>
                        <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Pontos" HeaderText="Pontos Rating" SortExpression="Pontos" />
                        <asp:HyperLinkField DataTextField="Record" HeaderText="Record" SortExpression="Record" DataNavigateUrlFields="Epoca, Campeonato, Id, Nome"  DataNavigateUrlFormatString="jogador/{0}/{1}/{2}-{3}"/>
                    </Columns>
                </asp:GridView>
            </div>
        </div>

    </asp:Panel>
</asp:Content>
