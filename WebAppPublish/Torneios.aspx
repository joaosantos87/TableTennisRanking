﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Torneios.aspx.cs" Inherits="WebApplication1.Torneios" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <br />
    <asp:Button Visible="false" ID="backButton" runat="server" Text="Voltar ao inicio" OnClick ="backButton_Click"/>
    <br />
    <h1 id="h1Torneio" runat="server"> Torneios </h1>
    Epoca:
    <asp:DropDownList ID="epocaDropDownList" runat="server" Height="30px" Width="458px"></asp:DropDownList>
    <br>
    Campeonato: <asp:DropDownList ID="CampeonatoListBox1" runat="server" Height="30px" Width="453px"></asp:DropDownList>
    <br />
    Pesquisa:
    <asp:TextBox ID="TextBox1" runat="server" Width="210px"></asp:TextBox>
    <br />
     <asp:Button ID="Button1" runat="server" Text="Pesquisar" OnClick="Button1_Click" />
     
    <div class="table-responsive">
         <asp:GridView ID="GridView1" runat="server" Visible="False" AutoGenerateColumns="false" CssClass="table table-striped">
              <Columns>
                <asp:BoundField DataField="startDateField" HeaderText="startDate" Visible="false" InsertVisible="False" ReadOnly="True" SortExpression="startDate" />
                <asp:BoundField DataField="endDateField" HeaderText="endDate" Visible="false" InsertVisible="False" ReadOnly="True" SortExpression="endDate" />
                <asp:BoundField DataField="Id" HeaderText="Id" Visible="false" InsertVisible="False" ReadOnly="True" SortExpression="Id" />
                  <asp:BoundField DataField="Epoca" Visible="false" />
                <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Data" HeaderText="Data" SortExpression="Data" />
                <asp:HyperLinkField ItemStyle-HorizontalAlign="Center" DataTextField="Nome" HeaderText="Nome" SortExpression="Name" DataNavigateUrlFields="Id, Epoca, Nome"  DataNavigateUrlFormatString="~/torneio/{1}/{0}-{2}"/>
                <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="NumeroJogos" HeaderText="Numero Jogos" SortExpression="NumeroJogos" />
                <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="NumeroAtletas" HeaderText="Numero Atletas" SortExpression="NumeroAtletas" />
                <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Nivel" HeaderText="Nivel" SortExpression="Nivel" />
                  <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="TipoTorneio" HeaderText="Tipo Torneio" SortExpression="TipoTorneio" />
                  <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Peso" HeaderText="Peso" SortExpression="Peso" />
            </Columns>
         </asp:GridView>
    </div>
    
    <h2 id="nomeTorneio" runat="server"></h2>
    
    <div class="col-sm-7" id="Div1" runat="server">
        <asp:Panel ID="PanelClassificacao" runat="server" Visible="false">
        <div class="table-responsive">
            <asp:GridView ID="GridView4" runat="server" AutoGenerateColumns="false" CssClass="table table-striped">
                <Columns>
                    <asp:BoundField  ItemStyle-HorizontalAlign="Center" DataField="Pos" HeaderText="Pos." SortExpression="Pos" />
                    <asp:BoundField DataField="IdClub" HeaderText="IdClub" Visible="false" />
                    <asp:HyperLinkField DataTextField="Clube" HeaderText="Clube" SortExpression="Clube" DataNavigateUrlFields="IdClub, Clube"  DataNavigateUrlFormatString="~/clube/{0}-{1}"/>
                    <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Jogos" HeaderText="Jogos" SortExpression="Jogos" />
                    <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Vitorias" HeaderText="Vit." SortExpression="Vitorias" />
                    <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Derrotas" HeaderText="Der." SortExpression="Derrotas" />
                    <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="SetGanPer" HeaderText="Set/Gan-Per" SortExpression="Set/Gan-Per" />
                    <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="SetDif" HeaderText="Set/Dif" SortExpression="Set/Dif" />
                    <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Pontos" HeaderText="Pontos" SortExpression="Pontos" />
                </Columns>
            </asp:GridView>
        </div>
        </asp:Panel>
    
        <asp:Panel ID="PanelEquipas" runat="server" Visible="false">
        
            <div class="col-sm-12" id="listaJornadas" runat="server"></div>

            <asp:GridView ID="GridView3" runat="server" AutoGenerateColumns="false">
                <Columns>
                    <asp:BoundField DataField="startDateField" HeaderText="startDate" Visible="false" InsertVisible="False" ReadOnly="True" SortExpression="startDate" />
                    <asp:BoundField DataField="endDateField" HeaderText="endDate" Visible="false" InsertVisible="False" ReadOnly="True" SortExpression="endDate" />
                    <asp:BoundField DataField="IdChamp" HeaderText="IdChamp" InsertVisible="False" ReadOnly="True" SortExpression="IdChamp" Visible="false" />
                    <asp:BoundField DataField="Epoca" Visible="false" />

                    <asp:BoundField DataField="Campeonato" HeaderText="Campeonato" InsertVisible="False" ReadOnly="True" SortExpression="Campeonato" Visible="false" />
                    <asp:BoundField DataField="Id" HeaderText="Id" SortExpression="Id" Visible="false" />
                    <asp:BoundField DataField="IdClubA" HeaderText="IdClubA" SortExpression="IdClubA" Visible="false" />
                    <asp:BoundField DataField="IdClubB" HeaderText="IdClubB" SortExpression="IdClubB" Visible="false" />
                    <asp:BoundField DataField="Data" HeaderText="Data" SortExpression="Data" /> 
                    <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Jornada" HeaderText="Jornada" SortExpression="Jornada" />
                    <asp:HyperLinkField ItemStyle-HorizontalAlign="Center" DataTextField="ClubA" HeaderText="Clube" SortExpression="Clube" DataNavigateUrlFields="IdClubA, ClubA"  DataNavigateUrlFormatString="~/clube/{0}-{1}"/>
                    <asp:HyperLinkField ItemStyle-HorizontalAlign="Center" DataTextField="ClubB" HeaderText="Clube" SortExpression="Clube" DataNavigateUrlFields="IdClubB, ClubB"  DataNavigateUrlFormatString="~/clube/{0}-{1}"/>
                    <asp:HyperLinkField ItemStyle-HorizontalAlign="Center" DataTextField="Resultado" HeaderText="Resultado" SortExpression="Resultado" DataNavigateUrlFields="Id, ClubA, ClubB, Campeonato, Epoca"  DataNavigateUrlFormatString="~/detalhes/{4}/{3}/{0}-{1}-vs-{2}"/>
                </Columns>
            </asp:GridView>
        </asp:Panel>
    </div>
    <div class="col-sm-5" id="Div2" runat="server">
        <div class="table-responsive">
            <asp:GridView HeaderStyle-HorizontalAlign="Center" ID = "rankingList" runat="server" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="Id" ShowHeaderWhenEmpty="True" onsorting="rankingList_Sorting" AutoPostback = "False" CssClass="table table-sm table-hover" GridLines="Horizontal" BorderStyle="None" HeaderStyle-BorderWidth="2px" HorizontalAlign="Center" HeaderStyle-CssClass="gridviewHeader">
                <Columns>
                    <asp:BoundField DataField="IdSeason" HeaderText="IdSeason" Visible="false" InsertVisible="False" ReadOnly="True" SortExpression="IdSeason" />
                    <asp:BoundField DataField="Season" HeaderText="Season" Visible="false" InsertVisible="False" ReadOnly="True" SortExpression="Season" />
                    <asp:BoundField DataField="IdChamp" HeaderText="IdChamp" Visible="false" InsertVisible="False" ReadOnly="True" SortExpression="IdChamp" />
                    <asp:BoundField DataField="Campeonato" HeaderText="Campeonato" Visible="false" InsertVisible="False" ReadOnly="True" SortExpression="Campeonato" />
                    <asp:BoundField DataField="Id" HeaderText="Id" Visible="false" InsertVisible="False" ReadOnly="True" />
                    <asp:BoundField DataField="IdClub" HeaderText="IdClub" Visible="false" InsertVisible="False" ReadOnly="True" />
                    <asp:BoundField DataField="Ranking" HeaderText="Ranking" SortExpression="Ranking"  ItemStyle-HorizontalAlign="Center"/>
                    <asp:BoundField DataField="Licenca" HeaderText="Licenca" SortExpression="Licenca" ItemStyle-HorizontalAlign="Center" Visible="false" />
                    <asp:HyperLinkField  DataTextField="Nome" HeaderText="Nome" SortExpression="Nome" DataNavigateUrlFields="Id, Nome"  DataNavigateUrlFormatString="~/Jogador/{0}-{1}" ItemStyle-HorizontalAlign="Center"/>
                    <asp:HyperLinkField  DataTextField="Clube" HeaderText="Clube" SortExpression="Clube" DataNavigateUrlFields="IdClub, Clube"  DataNavigateUrlFormatString="~/Clube/{0}-{1}" ItemStyle-HorizontalAlign="Center"/>
                    <asp:HyperLinkField ItemStyle-HorizontalAlign="Center" DataTextField="Record" HeaderText="Record" SortExpression="Record" DataNavigateUrlFields="Id,Season, Campeonato, Nome"  DataNavigateUrlFormatString="~/Jogador/{1}/{2}/{0}-{3}" />
                    <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Pontos" HeaderText="Pontos" SortExpression="Pontos" />
                </Columns>
            </asp:GridView>
        </div>
    </div>
    <!--
    <div class="row">
        
    <div class="col-sm-12">
    <h3 runat="server">Resultados Detalhados</h3>
        <asp:GridView ID="GridView2" runat="server" Visible="False" AutoGenerateColumns="false" AutoGenerateEditButton="false" OnRowEditing="GridView2_RowEditing" OnRowUpdating="GridView2_RowUpdating" OnRowCancelingEdit="GridView2_RowCancelingEdit">
            <Columns>
                <asp:BoundField DataField="startDateField" HeaderText="startDate" Visible="false" InsertVisible="False" ReadOnly="True" SortExpression="startDate" />
                <asp:BoundField DataField="endDateField" HeaderText="endDate" Visible="false" InsertVisible="False" ReadOnly="True" SortExpression="endDate" />
                <asp:BoundField DataField="IdChamp" HeaderText="IdChamp" InsertVisible="False" ReadOnly="True" SortExpression="IdChamp" Visible="false" />
                <asp:BoundField DataField="IdPlayerA" HeaderText="IdPlayerA" InsertVisible="False" ReadOnly="True" SortExpression="IdPlayerA" Visible="false" />
                <asp:BoundField DataField="IdPlayerB" HeaderText="IdPlayerB" InsertVisible="False" ReadOnly="True" SortExpression="IdPlayerB" Visible="false" />
                <asp:BoundField DataField="Data" HeaderText="Data" SortExpression="Data" />
                <asp:BoundField DataField="Fase" HeaderText="Fase" SortExpression="Fase" />
                <asp:HyperLinkField DataTextField="JogadorA" HeaderText="Jogador" SortExpression="JogadorA" DataNavigateUrlFields="IdPlayerA, IdChamp, startDateField, endDateField, JogadorA"  DataNavigateUrlFormatString="~/jogador/{0}-{4}"/>
                <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="PontosA" HeaderText="Pontos Rating" InsertVisible="False" ReadOnly="True" SortExpression="Pontos" />
                <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Resultado" HeaderText="Resultado" SortExpression="Resultado" ReadOnly="false" />
                <asp:HyperLinkField DataTextField="JogadorB" HeaderText="Jogador" SortExpression="JogadorB" DataNavigateUrlFields="IdPlayerB, IdChamp, startDateField, endDateField, JogadorB"  DataNavigateUrlFormatString="~/jogador/{0}-{4}"/>
                <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="PontosB" HeaderText="Pontos Rating" InsertVisible="False" ReadOnly="True" SortExpression="Pontos" />
            </Columns>
        </asp:GridView>
        </div>
    </div>-->
    <script type="text/javascript">
        function toggleShowResult(id) {

            var x = document.getElementById("detalhes"+id);
            if (x.style.display === "none") {
                x.style.display = "block";
            } else {
                x.style.display = "none";
            }
        }
    </script>
</asp:Content>
