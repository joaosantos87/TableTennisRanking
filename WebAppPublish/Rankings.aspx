﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Rankings.aspx.cs" Inherits="WebApplication1.RankingPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <br>
    <br />
    <p>Loading Time: <Label ID="Label1" runat="server"> test </Label>
    </p>
    <div class="row">
        <div class="col-xs-3">Epoca</div>
        <div class="col-xs-9">Campeonato</div>
    </div>
    <div class="row">
        <div class="col-xs-3">
            <asp:DropDownList ID="epocaDropDownList" runat="server"></asp:DropDownList>
        </div>
        <div class="col-xs-9">
            <asp:DropDownList ID="CampeonatoListBox1" runat="server"></asp:DropDownList>
        </div>
    </div>
    <br />
    Pesquisa:<asp:TextBox ID="searchTextbox" runat="server"></asp:TextBox>
    <br />
    <br />
    <asp:Button ID="FilterButton" runat="server" Text="Pesquisar" OnClick="FilterButton_Click" AutoPostback = "False" />
    <br />
    
    <div class="table-responsive">
        <asp:GridView HeaderStyle-HorizontalAlign="Center" ID = "rankingList" runat="server" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="Id" ShowHeaderWhenEmpty="True" onsorting="rankingList_Sorting" AutoPostback = "False" CssClass="table table-sm table-hover" GridLines="Horizontal" BorderStyle="None" HeaderStyle-BorderWidth="2px" HorizontalAlign="Center" HeaderStyle-CssClass="gridviewHeader">
            <Columns>
                <asp:BoundField DataField="IdSeason" HeaderText="IdSeason" Visible="false" InsertVisible="False" ReadOnly="True" SortExpression="IdSeason" />
                <asp:BoundField DataField="Season" HeaderText="Season" Visible="false" InsertVisible="False" ReadOnly="True" SortExpression="Season" />
                <asp:BoundField DataField="IdChamp" HeaderText="IdChamp" Visible="false" InsertVisible="False" ReadOnly="True" SortExpression="IdChamp" />
                <asp:BoundField DataField="Campeonato" HeaderText="Campeonato" Visible="false" InsertVisible="False" ReadOnly="True" SortExpression="Campeonato" />
                <asp:BoundField DataField="Id" HeaderText="Id" Visible="false" InsertVisible="False" ReadOnly="True" />
                <asp:BoundField DataField="IdClub" HeaderText="IdClub" Visible="false" InsertVisible="False" ReadOnly="True" />
                <asp:BoundField DataField="Ranking" HeaderText="Ranking" SortExpression="Ranking"  ItemStyle-HorizontalAlign="Center"/>
                <asp:BoundField DataField="Licenca" HeaderText="Licenca" SortExpression="Licenca" ItemStyle-HorizontalAlign="Center" Visible="false" />
                <asp:HyperLinkField  DataTextField="Nome" HeaderText="Nome" SortExpression="Nome" DataNavigateUrlFields="Id, Nome"  DataNavigateUrlFormatString="~/Jogador/{0}-{1}" ItemStyle-HorizontalAlign="Center"/>
                <asp:HyperLinkField  DataTextField="Clube" HeaderText="Clube" SortExpression="Clube" DataNavigateUrlFields="IdClub, Clube"  DataNavigateUrlFormatString="~/Clube/{0}-{1}" ItemStyle-HorizontalAlign="Center"/>
                <asp:HyperLinkField ItemStyle-HorizontalAlign="Center" DataTextField="Record" HeaderText="Record" SortExpression="Record" DataNavigateUrlFields="Id,Season, Campeonato, Nome"  DataNavigateUrlFormatString="~/Jogador/{1}/{2}/{0}-{3}" />
                <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Pontos" HeaderText="Pontos" SortExpression="Pontos" />
                <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="PontosPorJogo" HeaderText="Pts p/jogo" SortExpression="PontosPorJogo" />
            </Columns>
        </asp:GridView>
    </div>
    </asp:Content>