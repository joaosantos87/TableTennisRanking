﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebApplication1.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <br>
    <br />
    <p>Loading Time: <Label ID="Label1" runat="server"> test </Label></p>
    <h3> Ultimos jogos inseridos</h3>
    <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="false">
        <Columns>
            <asp:BoundField DataField="IdChamp" HeaderText="IdChamp" InsertVisible="False" ReadOnly="True" SortExpression="IdChamp" Visible="false" />
            <asp:BoundField DataField="IdPlayerA" HeaderText="IdPlayerA" InsertVisible="False" ReadOnly="True" SortExpression="IdPlayerA" Visible="false" />
            <asp:BoundField DataField="IdPlayerB" HeaderText="IdPlayerB" InsertVisible="False" ReadOnly="True" SortExpression="IdPlayerB" Visible="false" />
            <asp:BoundField DataField="Epoca" Visible="false" />
            <asp:BoundField DataField="Data" HeaderText="Data" SortExpression="Data" />
            <asp:HyperLinkField DataTextField="Campeonato" HeaderText="Campeonato" SortExpression="Campeonato" DataNavigateUrlFields="Epoca, IdChamp, Campeonato"  DataNavigateUrlFormatString="~/torneio/{0}/{1}-{2}"/>
            <asp:HyperLinkField DataTextField="JogadorA" HeaderText="Jogador" SortExpression="JogadorA" DataNavigateUrlFields="IdPlayerA, IdChamp, JogadorA"  DataNavigateUrlFormatString="~/jogador/{0}-{2}"/>
            <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="PontosA" HeaderText="Pontos Rating" InsertVisible="False" ReadOnly="True" SortExpression="Pontos" />
            <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Resultado" HeaderText="Resultado" SortExpression="Resultado" ReadOnly="false" />
            <asp:HyperLinkField DataTextField="JogadorB" HeaderText="Jogador" SortExpression="JogadorB" DataNavigateUrlFields="IdPlayerB, IdChamp, JogadorB"  DataNavigateUrlFormatString="~/jogador/{0}-{2}"/>
            <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="PontosB" HeaderText="Pontos Rating" InsertVisible="False" ReadOnly="True" SortExpression="Pontos" />
        </Columns>
    </asp:GridView>
    <h3> Resultados surpreendentes</h3>
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false">
        <Columns>
            <asp:BoundField DataField="IdChamp" HeaderText="IdChamp" InsertVisible="False" ReadOnly="True" SortExpression="IdChamp" Visible="false" />
            <asp:BoundField DataField="IdPlayerA" HeaderText="IdPlayerA" InsertVisible="False" ReadOnly="True" SortExpression="IdPlayerA" Visible="false" />
            <asp:BoundField DataField="IdPlayerB" HeaderText="IdPlayerB" InsertVisible="False" ReadOnly="True" SortExpression="IdPlayerB" Visible="false" />
            <asp:BoundField DataField="Data" HeaderText="Data" SortExpression="Data" />
            <asp:HyperLinkField DataTextField="Campeonato" HeaderText="Campeonato" SortExpression="Campeonato" DataNavigateUrlFields="IdChamp, Campeonato"  DataNavigateUrlFormatString="~/evento/{0}-{1}"/>
            <asp:HyperLinkField DataTextField="JogadorA" HeaderText="Jogador" SortExpression="JogadorA" DataNavigateUrlFields="IdPlayerA, IdChamp, JogadorA"  DataNavigateUrlFormatString="~/jogador/{0}-{2}"/>
            <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="PontosA" HeaderText="Pontos Rating" InsertVisible="False" ReadOnly="True" SortExpression="Pontos" />
            <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Resultado" HeaderText="Resultado" SortExpression="Resultado" ReadOnly="false" />
            <asp:HyperLinkField DataTextField="JogadorB" HeaderText="Jogador" SortExpression="JogadorB" DataNavigateUrlFields="IdPlayerB, IdChamp, JogadorB"  DataNavigateUrlFormatString="~/jogador/{0}-{2}"/>
            <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="PontosB" HeaderText="Pontos Rating" InsertVisible="False" ReadOnly="True" SortExpression="Pontos" />
        </Columns>
    </asp:GridView>
    <h3> Jogos mais recentes</h3>
    <asp:GridView ID="GridView3" runat="server" AutoGenerateColumns="false">
        <Columns>
            <asp:BoundField DataField="IdChamp" HeaderText="IdChamp" InsertVisible="False" ReadOnly="True" SortExpression="IdChamp" Visible="false" />
            <asp:BoundField DataField="IdPlayerA" HeaderText="IdPlayerA" InsertVisible="False" ReadOnly="True" SortExpression="IdPlayerA" Visible="false" />
            <asp:BoundField DataField="IdPlayerB" HeaderText="IdPlayerB" InsertVisible="False" ReadOnly="True" SortExpression="IdPlayerB" Visible="false" />
            <asp:BoundField DataField="Data" HeaderText="Data" SortExpression="Data" />
            <asp:HyperLinkField DataTextField="Campeonato" HeaderText="Campeonato" SortExpression="Campeonato" DataNavigateUrlFields="IdChamp"  DataNavigateUrlFormatString="~/Torneios.aspx?ID={0}"/>
            <asp:HyperLinkField DataTextField="JogadorA" HeaderText="Jogador" SortExpression="JogadorA" DataNavigateUrlFields="IdPlayerA, IdChamp, JogadorA"  DataNavigateUrlFormatString="~/jogador/{0}-{2}"/>
            <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="PontosA" HeaderText="Pontos Rating" InsertVisible="False" ReadOnly="True" SortExpression="Pontos" />
            <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Resultado" HeaderText="Resultado" SortExpression="Resultado" ReadOnly="false" />
            <asp:HyperLinkField DataTextField="JogadorB" HeaderText="Jogador" SortExpression="JogadorB" DataNavigateUrlFields="IdPlayerB, IdChamp, JogadorB"  DataNavigateUrlFormatString="~/jogador/{0}-{2}"/>
            <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="PontosB" HeaderText="Pontos Rating" InsertVisible="False" ReadOnly="True" SortExpression="Pontos" />
        </Columns>
    </asp:GridView>
    
    <h3> Ultimos jogadores adicionados</h3>
    <asp:GridView ID="GridView4" runat="server" AutoGenerateColumns="false">
        <Columns>
            <asp:BoundField DataField="Id" HeaderText="Id" Visible="false" InsertVisible="False" ReadOnly="True" SortExpression="Id" />      
            <asp:BoundField DataField="IdClub" HeaderText="IdClub" Visible="false" InsertVisible="False" ReadOnly="True" SortExpression="IdClub" />            
              <asp:BoundField DataField="Licenca" HeaderText="Licenca" SortExpression="licenseNo" ItemStyle-HorizontalAlign="Center" />
            <asp:BoundField DataField="CurrentRank" HeaderText="Ranking" SortExpression="currentRank"  ItemStyle-HorizontalAlign="Center"/>
            <asp:HyperLinkField ItemStyle-HorizontalAlign="Center" DataTextField="Nome" HeaderText="Nome" SortExpression="Name" DataNavigateUrlFields="Id, Nome"  DataNavigateUrlFormatString="~/jogador/{0}-{1}"/>
            <asp:HyperLinkField ItemStyle-HorizontalAlign="Center" DataTextField="Club" HeaderText="Clube" SortExpression="Name" DataNavigateUrlFields="IdClub, Club"  DataNavigateUrlFormatString="~/clube/{0}-{1}"/>
            <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Record" HeaderText="Record" SortExpression="Record" />
            <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Pontos" HeaderText="Pontos" SortExpression="Pontos" />
        </Columns>
    </asp:GridView>
</asp:Content>
